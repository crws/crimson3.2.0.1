
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3GRAPH_HPP
	
#define	INCLUDE_G3GRAPH_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hpp>

#include <g3prims.hpp>

#include <g3gdi.hpp>

#include <g3comms.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3graph.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3GRAPH

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3graph.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Init and Term Hooks
//

extern void DLLAPI G3GraphInit(void);

extern void DLLAPI G3GraphTerm(void);

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPageEditorWnd;
class CTextEditorWnd;
class CGhostBar;
class CScratchPadWnd;

//////////////////////////////////////////////////////////////////////////
//
// Handle Data
//

struct CHand
{
	UINT	  m_Code;
	UINT      m_uPos;
	CRect	  m_Rect;
	CPoint    m_Old;
	CPrimHand m_Hand;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

class DLLAPI CPageEditorWnd : public CViewWnd, public IDropSource, public IDropTarget
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPageEditorWnd(void);

		// Destructor
		~CPageEditorWnd(void);

	protected:
		// Timer IDs
		static UINT m_timerHover;
		static UINT m_timerGhost;
		static UINT m_timerKeys;
		static UINT m_timerScroll;

		// Options
		static BOOL const m_fCoalesceMove;
		static BOOL const m_fCoalesceSize;
		static BOOL const m_fCoalesceHand;

		// Keyboard Hook
		static HHOOK m_hKeyHook;

		// Commands
		class CCmdBase;
		class CCmdMove;
		class CCmdSize;
		class CCmdHandle;
		class CCmdDelete;
		class CCmdPaste;
		class CCmdGroup;
		class CCmdUngroup;
		class CCmdOrder;

		// Accelerator
		CAccelerator m_Accel;

		// Cursors
		CCursor	m_CopyCursor;
		CCursor	m_MoveCursor;
		CCursor m_LineCursor;
		CCursor	m_GrabOpenCursor;
		CCursor	m_GrabCloseCursor;
		CCursor	m_ZoomInCursor;
		CCursor	m_ZoomOutCursor;
		CCursor m_PickCursor;
		CCursor m_MissCursor;

		// Bound Items
		CDispPage      * m_pPage;
		CDispPageProps * m_pProps;
		CPrimList      * m_pList;
		CUISystem      * m_pSystem;
		CLangManager   * m_pLang;
		CFontManager   * m_pFonts;
		CEventMap      * m_pGlobal;
		CEventMap      * m_pLocal;
		BOOL	         m_fRead;

		// System Proxy
		CSysProxy m_System;

		// View Options
		BOOL m_fGridShow;
		BYTE m_bGridBlend;
		UINT m_uGridSnap;
		BYTE m_bItemBlend;
		BYTE m_bBackBlend;
		BYTE m_bMastBlend;
		BOOL m_fMastShow;
		BOOL m_fShowError;
		INT  m_nItemEdge;
		INT  m_nItemHalf;
		BOOL m_fAlignEdge;
		BOOL m_fAlignCenter;
		BOOL m_fLockUI;

		// Layout Data
		CSize m_DispSize;
		CRect m_FrameRect;
		CSize m_TotalSize;

		// GDI Object
		IGdiWindows * m_pWin;
		IGdi        * m_pGDI;

		// Scroll Data
		CStatic    * m_pScrollC;
		CScrollBar * m_pScrollH;
		CScrollBar * m_pScrollV;
		BOOL	     m_fScrollH;
		BOOL	     m_fScrollV;

		// Scaling Data
		int	 m_nScale;
		int	 m_nScaleMin;
		int	 m_nScaleMax;
		CDC    * m_pMap;
		CPoint   m_ViewOrg;
		CSize	 m_ViewExt;

		// Error List
		CArray <CRect> m_Error;

		// Drag Handles
		CArray <CHand>	m_Handles;

		// Command History
		CStringTree m_LastCopy;
		CString     m_LastFind;

		// Working Set
		CPrimList * m_pWorkList;
		CPrimSet  * m_pWorkSet;
		CPrimSet  * m_pPendSet;
		CRect       m_WorkRect;
		
		// Selection Data
		CRect		m_SelRect;
		CRect           m_SelText;
		CArray <INDEX>  m_SelList;
		INDEX		m_SelRef;
		CGhostBar     * m_pGhost;
		CArray <INDEX>  m_BuryList;

		// Position Data
		BOOL	m_fPosValid;
		BOOL    m_fPosMove;
		CPoint  m_ClientPos;
		CPoint	m_Pos;
		UINT	m_uHandle;
		BOOL    m_fHover;
		INDEX	m_nHover;
		CPrim *	m_pHover;
		INDEX	m_nPick;
		CPrim * m_pPick;

		// Tracking Data
		UINT	m_uMode;
		UINT	m_uCapture;
		UINT    m_uPick;
		CString m_PickNav;
		CPoint	m_TrackPos;
		CRect	m_TrackRect;
		CSize   m_TrackMin;
		int	m_TrackData;
		CLASS	m_Create;

		// Drag Context
		BOOL    m_fDrag;
		WORD	m_cfData;
		WORD	m_cfSpec;
		CPoint	m_DragPos;
		CSize	m_DragOffset;
		CSize	m_DragSize;
		CBitmap	m_DragImage;
		CString m_DragNav;

		// Drop Context
		CDropHelper  m_DropHelp;
		BOOL	     m_fDrop;
		BOOL	     m_fDropMove;
		UINT         m_uDropType;

		// Accepted Data
		CPrimList  * m_pAcceptList;
		CStringArray m_AcceptOrder;
		CRect        m_AcceptRect;
		CPoint	     m_AcceptPos;

		// Scratch Pad
		BOOL             m_fScratch;
		CScratchPadWnd * m_pScratch;
		CPageEditorWnd * m_pBuddy;

	// editor.cpp

		// Overridables
		void    OnAttach(void);
		void    OnDetach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Core Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnPostCreate(void);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnGoingIdle(void);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnSize(UINT uType, CSize Size);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnCancelMode(void);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnKeyUp(UINT uCode, DWORD dwFlags);
		void OnKeyChange(UINT uCode, DWORD dwFlags);

		// Mode Management
		BOOL SetMode(UINT uMode);
		BOOL SetMode(UINT uMode, BOOL fAbort);
		BOOL SetTextMode(BOOL fMouse, BOOL fAdd);
		void CheckMode(void);

		// Status Bar
		void ShowDefaultStatus(void);
		void ShowTrackStatus(void);
		void ShowPickStatus(void);
		void ShowStatus(CRect const &Rect, PCTXT pMore);

		// Key Status
		BOOL IsDown(UINT uCode);

		// Implementation
		void LoadCursors(void);
		void LoadConfig(void);
		void SaveConfig(void);
		void SendRewind(void);
		void RestoreSelect(void);
		BOOL RestoreSelect(CString Nav);

	// cmds.cpp

		// Command Handling
		void OnExec(CCmd *pCmd);
		void OnUndo(CCmd *pCmd);
		void ExecMove(CCmdMove *pCmd);
		void UndoMove(CCmdMove *pCmd);
		void ExecSize(CCmdSize *pCmd);
		void UndoSize(CCmdSize *pCmd);
		void ExecHandle(CCmdHandle *pCmd);
		void UndoHandle(CCmdHandle *pCmd);
		void ExecDelete(CCmdDelete *pCmd);
		void UndoDelete(CCmdDelete *pCmd);
		void ExecPaste(CCmdPaste *pCmd);
		void UndoPaste(CCmdPaste *pCmd);
		void ExecGroup(CCmdGroup *pCmd);
		void UndoGroup(CCmdGroup *pCmd);
		void ExecUngroup(CCmdUngroup *pCmd);
		void UndoUngroup(CCmdUngroup *pCmd);
		void ExecOrder(CCmdOrder *pCmd);
		void UndoOrder(CCmdOrder *pCmd);
		void ExecItem(CCmdItem *pCmd);
		void UndoItem(CCmdItem *pCmd);

	// edit.cpp

		// Edit Menu
		BOOL OnEditGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL CanEditPaste(void);
		BOOL CanEditPasteSpecial(void);
		BOOL CanEditDelete(void);
		BOOL CanEditItemProps(void);
		BOOL CanEditItemPos(void);
		void OnEditDelete(CString Verb);
		void OnEditCut(void);
		BOOL OnEditCopy(BOOL fCut);
		void OnEditPaste(CString Verb, UINT uMode);
		void OnEditPasteSpecial(void);
		void OnEditDuplicate(void);
		void OnEditSelectAll(void);
		void OnEditCopySizeInit(void);
		void OnEditCopySizeDone(void);
		void OnEditCopyPropsInit(UINT uID);
		void OnEditCopyPropsDone(void);
		void OnEditCopyProps(CPrim *pPick, CStringArray &Props, CString Name, UINT uID);
		BOOL OnEditSummary(void);
		BOOL OnEditItemProps(void);
		BOOL OnEditItemProps(CString Tab);
		BOOL OnEditItemProps(CString Tab, HANDLE hItem);
		BOOL OnEditPageProps(void);
		BOOL OnEditItemPos(void);
		BOOL OnEditFind(void);
		void OnEditCopyPage(BOOL fAll);

		// Property Manipulation
		BOOL HasProp(CStringArray &Props, PCTXT pProp);
		void StripMatch(CStringArray *pProps, ...);
		void StripOther(CStringArray *pProps, ...);
		void FlipTextAndData(CStringArray *pProps);

		// Background Color
		COLOR FindBackColor(void);
		COLOR FindTextColor(void);

		// Compilation
		BOOL RecompileSelList(void);
		BOOL RecompilePrimList(CPrimList *pList);
		BOOL RecompilePrim(CPrim *pPrim);
		BOOL RecompileFrom(CMetaItem *pItem);

	// block.cpp

		// Block Menu
		BOOL OnBlockGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnBlockControl(UINT uID, CCmdSource &Src);
		BOOL OnBlockCommand(UINT uID);
		void MakeBlockCmd(CPrim *pPrim, CString Menu);
		void KillBlockCmd(void);
		BOOL SaveBlockCmd(void);
		void ShowNewFont(UINT uFont);

	// text.cpp

		// Text Data
		CTextEditorWnd * m_pTextCtrl;
		BOOL             m_fTextMouse;
		BOOL             m_fTextAdd;
		CPrimWithText  * m_pTextPrim;
		HGLOBAL          m_hTextPrev;
		CBitmap          m_TextBitmap;
		CString          m_TextMenu;

		// Text Menu
		BOOL OnTextGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnTextControl(UINT uID, CCmdSource &Src);
		BOOL OnTextCommand(UINT uID);
		void OnTextAdd(void);
		void OnTextEdit(void);
		void OnTextCreated(void);
		void OnTextRemove(void);
		BOOL IsTextZoomOkay(BOOL fMouse);
		BOOL AllowTextMode(void);
		void EnterTextMode(void);
		void LeaveTextMode(BOOL fAbort);
		BOOL CheckCreateText(void);
		void ExposeText(void);

	// data.cpp

		// Data Menu
		BOOL OnDataGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnDataControl(UINT uID, CCmdSource &Src);
		BOOL OnDataCommand(UINT uID);
		BOOL OnDataAdd(void);
		void OnDataCreated(void);
		void OnDataRemove(void);

	// mode.cpp

		// Mode Menu
		BOOL OnModeGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnModeControl(UINT uID, CCmdSource &Src);
		BOOL OnModeCommand(UINT uID);

	// view.cpp

		// View Menu
		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

	// behave.cpp

		// Behavior Menu
		BOOL OnBehaveGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnBehaveControl(UINT uID, CCmdSource &Src);
		BOOL OnBehaveCommand(UINT uID);
		BOOL HasAction(CPrim *pPrim);
		BOOL CanAddMove(void);
		BOOL CanRemMove(void);
		BOOL CanAddAction(void);
		void OnBehaveAddMove(CLASS Class);
		void OnBehaveRemMove(void);
		BOOL OnBehaveAddAction(void);

	// trans.cpp

		// Transform Menu
		BOOL OnTransformGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnTransformControl(UINT uID, CCmdSource &Src);
		BOOL OnTransformCommand(UINT uID);

	// arrange.cpp

		// Arrange Menu
		BOOL OnArrangeGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnArrangeControl(UINT uID, CCmdSource &Src);
		BOOL OnArrangeCommand(UINT uID);
		void ArrangeUndo(CStringArray &Pos);
		void ArrangeRedo(UINT uID);
		void ArrangeSave(UINT uID);
		void OnArrangeFront(BOOL fRedo);
		void OnArrangeBack(BOOL fRedo);
		void OnArrangeToFront(BOOL fRedo);
		void OnArrangeToBack(BOOL fRedo);
		void OnArrangeEqual(BOOL fVert);
		void OnArrangeAlign(void);
		void OnArrangeAlignInit(UINT uID);
		void OnArrangeAlignDone(void);
		void PerformAlign(BOOL fPage, UINT uHorz, UINT uVert, INDEX Ref, BOOL fMulti);
		int  Align(UINT uCode, int nStart, int nEnd, int nPos, long nSize);

		// Arrange Context
		CCmdOrder ** m_ppCmdOrder;

		// Sort Pointer
		static CPrimList * m_pSort;

		// Sort Helpers
		static int SortPrimsHorz(PCVOID i1, PCVOID i2);
		static int SortPrimsVert(PCVOID i1, PCVOID i2);

	// organize.cpp

		// Organize Menu
		BOOL OnOrganizeGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnOrganizeControl(UINT uID, CCmdSource &Src);
		BOOL OnOrganizeCommand(UINT uID);
		BOOL CanGroup(void);
		BOOL CanWidget(void);
		BOOL CanUngroup(void);
		BOOL CanNormalize(void);
		BOOL CanExpand(void);
		BOOL CanBindWidget(void);
		BOOL CanClearBinding(void);
		BOOL CanSaveWidget(void);
		void OnOrganizeGroup(void);
		BOOL OnOrganizeWidget(void);
		void OnOrganizeAddMove(CLASS Class);
		void OnOrganizeUngroup(void);
		BOOL OnOrganizeNormalize(void);
		BOOL OnOrganizeExpand(void);
		void OnOrganizeBindWidget(void);
		void OnOrganizeClearBinding(void);
		BOOL OnOrganizeSaveWidget(void);

		// Widget Details
		BOOL    UsesDetails(CPrim *pPrim);
		BOOL    FindDetails(CPrim *pPrim, CStringArray &Zoom);
		BOOL    KillDetails(CPrim *pPrim, CStringArray &Zoom);
		BOOL    MakeDetails(CPrim *pPrim, CString Tag);
		BOOL    BindDetails(CPrim *pPrim, CString Base, CArray <CDispPage *> &Pages);
		BOOL    BindDetails(CPrimWidgetPropList *pList, CString Name, CString Code);
		BOOL    MakePage   (CPrimWidgetProp *pProp, CString Name, CDispPage * &pPage);
		BOOL    MakeWidget (CString Widget);
		CString MakePageName(UINT n, UINT c, CString Base);
		CString MakePropName(UINT n, UINT c);

	// gomenu.cpp

		// Jump List
		CStringTree m_JumpList;

		// Go Menu
		BOOL OnGoGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);

		// Jump List Handling
		void AddJumpCommands(CMenu &Menu);
		void BuildJumpList(void);
		void BuildJumpList(CMetaItem *pItem);
		void BuildJumpList(CItem *pItem);
		void BuildJumpList(CCodedItem *pCoded);
		BOOL AddToJumpList(UINT uHandle);
		BOOL AddToJumpList(CMetaItem *pMeta);
		BOOL AddToJumpList(CTag *pTag);

	// scale.cpp

		// Scaling and Scrolling
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		void OnHScroll(UINT uCode, int nPos, CWnd &Ctrl);
		void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);
		BOOL OnScroll(UINT uCode, int nPos, CScrollBar *pBar, long &nValue);
		void ScrollBy(CSize Step);
		BOOL ScrollIntoView(CPoint Pos, BOOL fDrop);
		void SetScale(int nScale, CPoint Pos1, CPoint Pos2);
		void SetScale(int nScale, CPoint Pos);
		void SetScale(int nScale);
		void CreateScrollBars(void);
		void PlaceScrollBars(void);
		void ShowScrollBar(CWnd *pWnd);
		void AdjustLayout(BOOL fSize);
		void FindLayout(void);
		void FindScaleLimits(void);
		void ConfigDC(CDC &DC);

		// Scaling Helpers
		CRect  PPtoDP(CRect Rect);
		CPoint PPtoDP(CPoint Pos);
		CRect  LPtoDP(CRect Rect);
		CPoint LPtoDP(CPoint Pos);
		CRect  DPtoLP(CRect Rect);
		CPoint DPtoLP(CPoint Pos);
		CRect  DPtoPP(CRect Rect);
		CPoint DPtoPP(CPoint Pos);

		// GDI Management
		void CreateGDI(void);
		void DestroyGDI(void);

		// Rectangle Clipping
		BOOL ClipMoveRect(CRect &Rect);
		void ClipSizeRect(CRect &Rect, CSize Size);

	// frame.cpp

		// Key Data
		struct CKey
		{
			CRect m_Rect;
			UINT  m_uCode;
			};

		// Key Undo
		struct CKeyUndo
		{
			HGLOBAL m_hPrev1;
			HGLOBAL m_hPrev2;
			};

		// Frame Data
		IFramer     * m_pFramer;
		CArray <CKey> m_Keys;
		UINT          m_EditPos;
		UINT          m_EditKey;
		CColor        m_EditCol;

		// Display Frame
		void MakeFramer(void);
		void FindMetrics(void);
		void DrawFrame(CDC &DC);
		void DrawKey(CDC &DC, CRect Rect, UINT uCode);
		void InvalidateKey(UINT uPos);
		void InvalidateKey(CKey const &Key);

		// Key Mouse Hooks
		BOOL CheckKeyHover(void);
		void CheckKeyTimer(void);
		BOOL KeyMenu(void);
		BOOL EditKey(void);

		// Key Data
		CColor  GetKeyColor(UINT uCode);
		CString GetKeyName(UINT uCode, BOOL fLocal);

		// Key Menu
		BOOL OnKeyGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnKeyControl(UINT uID, CCmdSource &Src);
		BOOL OnKeyCommand(UINT uID);
		void OnKeyMakeLocal(void);
		void OnKeyMakeGlobal(void);
		void OnKeyClear(void);
		void OnKeyEdit(void);

		// Undo Helpers
		void KeyInitUndo(CKeyUndo &Undo, CKey const &Key);
		BOOL KeySaveUndo(CKeyUndo &Undo, CString Menu);
		void KeyKillUndo(CKeyUndo &Undo);

	// mouse.cpp

		// Mouse Messages
		void OnRButtonDown(UINT uFlags, CPoint Pos);
		void OnMButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnRButtonDblClk(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Tracking Helpers
		BOOL TrackStart(void);
		void TrackUpdate(void);
		void TrackSetCursor(void);
		void TrackDraw(void);
		void TrackDraw(CDC &DC);
		void TrackEnd(BOOL fAbort);

		// Default Helpers
		BOOL DefaultStart(BOOL fRight);
		void DefaultUpdate(void);
		void DefaultSetCursor(void);
		void DefaultDraw(CDC &DC);

		// Selection Helpers
		BOOL SelectStart(void);
		void SelectUpdate(void);
		void SelectDraw(CDC &DC);
		void SelectEnd(BOOL fAbort);

		// Sizing Helpers
		BOOL SizeStart(void);
		void SizeUpdate(void);
		BOOL SizeUpdateItem(CHand const &Hand);
		void SizeUpdateLine(CHand const &Hand);
		void SizeUpdateRect(CHand const &Hand);
		void SizeSetCursor(void);
		void SizeDraw(void);
		void SizeDraw(CDC &DC);
		void SizeEnd(BOOL fAbort);

		// Grab Helpers
		void GrabSetCursor(void);

		// Zoom Helpers
		void ZoomSetCursor(void);

		// Pick Helpers
		BOOL InPickMode(void);
		BOOL CanPick(void);
		void PickUpdate(void);
		void PickSetCursor(void);
		BOOL PickDone(void);

		// Position Testing
		BOOL  UpdatePos(void);
		BOOL  UpdatePos(CPoint Pos);
		INDEX HitTestPrim(CPoint Pos);
		INDEX HitTestPrim(CPrimList *pList, CPoint Pos, BOOL fTree);
		UINT  HitTestHandle(CPoint Pos);

		// Grid Snapping
		void SnapToGrid(CPoint &Pos, UINT uMode);

		// Mouse Position
		CPoint GetClientPos(void);

		// Hook Support
		void InstallHook(void);
		void RemoveHook(void);

		// Hook Procedure
		static LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);

	// draw.cpp

		// Drawing Messages
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);

		// Frame Dumping
		void OnEditWebFrame(void);

		// Drawing Helpers
		BOOL ShowTempItems(void);
		void DrawErrors(CDC &DC);
		void DrawError(CDC &DC, CRect Rect);
		void DrawGrid(CDC &DC);
		BOOL DrawCurrentPos(CDC &DC);
		void DrawSelect(CDC &DC);
		void DrawHandles(CDC &DC);
		void DrawHandle (CDC &DC, CHand const &Hand, BOOL fOver);
		void DrawHandle (CDC &DC, CHand const &Hand, CRect const &Rect);
		void UpdateAll(void);
		BOOL UpdateImage(void);
		void UpdateImage(CPrimList *pList, UINT uCode);
		void UpdateBack(void);
		void SaveImage(void);
		void RestoreImage(void);

		// Line-Up List
		CTree <CPrim *> m_LinedUp;

		// Line-Up Checks
		void ShowLineUp(CDC &DC, BOOL fSelect);
		BOOL ShowLineUp(CPrim *pPrim, BOOL fSelect);
		void ShowLineUpBottom(CDC &DC, BOOL fSelect);
		void ShowLineUpTop(CDC &DC, BOOL fSelect);
		void ShowLineUpLeft(CDC &DC, BOOL fSelect);
		void ShowLineUpRight(CDC &DC, BOOL fSelect);
		void ShowLineUpCenter(CDC &DC, BOOL fSelect);
		void ShowLineUpMiddle(CDC &DC, BOOL fSelect);

		// Alpha Blending
		void BlendItemRect(CDC &DC, CRect Rect, CColor Color);
		void BlendItemLine(CDC &DC, CRect Rect, CColor Color);
		BOOL BlendImage(BYTE bBlend);

	// select.cpp

		// Working List
		void ClearWorkList(void);
		void SelectWorkList(void);
		BOOL ClimbWorkList(BOOL fEscape);

		// Selection Access
		CPrim * GetLoneSelect(void);

		// Selection List
		BOOL  HasSelect(void);
		BOOL  HasLoneSelect(void);
		BOOL  HasMultiSelect(void);
		BOOL  HasZoomPages(void);
		BOOL  HasZoomPages(CPrim *pPrim);
		BOOL  HasZoomPages(CPrimList *pList);
		BOOL  IsPickable(INDEX nPrim);
		BOOL  IsHoverable(INDEX nPrim);
		BOOL  IsSelected(INDEX nPrim);
		BOOL  IsSelected(CPrim *pPrim);
		BOOL  IsPosLocked(void);
		BOOL  IsListLocked(void);
		BOOL  ArePropsLocked(void);
		BOOL  HasUnbrokenSelect(void);
		BOOL  ClearHover(void);
		void  KillHover(void);
		BOOL  ClearSelect(BOOL fGone);
		BOOL  SetSelect(INDEX nPrim);
		BOOL  AddSelect(INDEX nPrim, BOOL fPend);
		BOOL  RemSelect(INDEX nPrim, BOOL fPend);
		void  NewSelection(void);
		void  UpdateSelectData(BOOL fGhost = FALSE);
		void  BuildHandles(void);
		void  BuildRectHandles(void);
		void  BuildMoveHandles(void);
		void  BuildLineHandles(CPrim *pPrim);
		void  BuildItemHandles(CPrim *pPrim);
		void  BuildItemHandle (CPrim *pPrim, CHand &Hand);
		CSize GetHandleSize(void);
		void  DrawSelection(void);
		void  NudgeSelection(int dx, int dy, BOOL fNudge);
		BOOL  MoveSelection(int &dx, int &dy, BOOL fDirty);
		BOOL  MoveSelection(CRect Rect, BOOL fDirty);
		BOOL  SizeSelection(CRect Rect, BOOL fDirty);
		BOOL  DeleteSelection(void);
		void  SetSelectionHandle(CString Tag, CSize Clip, CPoint Pos);
		BOOL  SetItemHandle(CPrim *pPrim, CString Tag, CSize Clip, CPoint Pos);
		void  FindSelectMinSize(void);
		BOOL  CheckOverlay(void);
		BOOL  FindTop(CPrim *pPrim, CPoint &Pos, CSize Size);
		void  GroupSelection(CLASS Class);
		void  GroupSelection(HANDLE hData);
		void  GroupSelection(CPrimSet *pSet);
		void  UngroupSelection(void);
		void  SetSelectionTextColor(void);

		// Ghost Bar
		BOOL MakeGhost(void);
		BOOL ShowGhost(void);
		BOOL PollGhost(void);
		BOOL KillGhost(void);

	// selmenu.cpp

		// Select Menu
		BOOL OnSelectGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnSelectControl(UINT uID, CCmdSource &Src);
		BOOL OnSelectCommand(UINT uID);

		// Buried Selection
		void BuildBuriedList(void);
		void AddBuriedCommands(CMenu &Menu);

	// drag.cpp

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropSource
		HRESULT METHOD QueryContinueDrag(BOOL fEscape, DWORD dwKeys);
		HRESULT METHOD GiveFeedback(DWORD dwEffect);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Drag Support
		void DragItem(void);
		void FindDragMetrics(void);
		void MakeDragImage(void);

		// Drop Support
		BOOL IsDropMove(DWORD dwKeys, DWORD *pEffect);
		void SetDropMove(BOOL fMove);

		// Mouse Helpers
		BOOL DragStart(void);
		void DropUpdate(void);
		void DropDraw(void);
		void DropDraw(CDC &DC);

		// Command Helpers
		void LocalExecCmd(CCmd *pCmd);
		void LocalSaveCmd(CCmd *pCmd);
		void SaveMultiCmd(CString Nav, CString Verb);
		BOOL SaveDropCmd(CString Verb);

	// dataobj.cpp

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData, BOOL fRich, BOOL fFixup);
		BOOL AddPrimsToDataObject(CDataObject *pData, CPoint Pos, BOOL fRich);
		BOOL AddPrimsToStream(ITextStream &Stream, CPoint Pos, BOOL fRich);
		void AddPrimsToTreeFile(CTreeFile &Tree, BOOL fRich);

		// Data Object Testing
		BOOL CanAcceptDataObject(IDataObject *pData, UINT &uType);
		BOOL CanAcceptOther(IDataObject *pData, UINT &uType);
		BOOL CanAcceptPrims(IDataObject *pData, UINT &uType);
		BOOL CanAcceptPrims(ITextStream &Stream, UINT &uType);
		BOOL GetStreamInfo(ITextStream &Stream, UINT &uType, CPoint &Pos);

		// Data Object Acceptance
		BOOL AcceptDataObject(IDataObject *pData, UINT uMode);
		BOOL AcceptOther(IDataObject *pData, UINT uMode);
		BOOL AcceptPrims(IDataObject *pData, UINT uMode);
		BOOL AcceptPrims(ITextStream &Stream, UINT uType, UINT uMode);

		// Accepted Data Operations
		void AcceptSelection(void);
		void FixAcceptList(CPrimRefMap &Refs);
		void MoveAcceptList(CRect OldRect, CRect NewRect);
		void SizeAcceptList(CRect OldRect, CRect NewRect);
		void CopyAcceptList(BOOL fOrder);
		void FreeAcceptList(void);

		// Adopter Control
		void SetAdopter(void);
		void ClearAdopter(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Base Command
//

class DLLAPI CPageEditorWnd::CCmdBase : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdBase(void);

		// Operations
		void MakeMenu(BOOL fForce);

		// Data Members
		CString m_Verb;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Move Command
//

class DLLAPI CPageEditorWnd::CCmdMove : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdMove(CString Item, BOOL fNudge, int dx, int dy);

		// Data Members
		int m_dx;
		int m_dy;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Size Command
//

class DLLAPI CPageEditorWnd::CCmdSize : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdSize(CString Item, CRect OldRect, CRect NewRect);

		// Data Members
		CRect m_OldRect;
		CRect m_NewRect;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Handle Command
//

class DLLAPI CPageEditorWnd::CCmdHandle : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdHandle(CString Item, CString Tag, CSize Clip, CPoint Old, CPoint Pos);

		// Data Members
		CString m_Tag;
		CSize   m_Clip;
		CPoint  m_Old;
		CPoint  m_Pos;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Delete Command
//

class DLLAPI CPageEditorWnd::CCmdDelete : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdDelete(CString Verb, CString Item, IDataObject *pData);

		// Destructor
		~CCmdDelete(void);

		// Data Members
		IDataObject *m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Paste Command
//

class DLLAPI CPageEditorWnd::CCmdPaste : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPaste(CString Verb, IDataObject *pData, UINT uMode);

		// Destructor
		~CCmdPaste(void);

		// Data Members
		IDataObject * m_pData;
		UINT	      m_uMode;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Group Command
//

class DLLAPI CPageEditorWnd::CCmdGroup : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdGroup(CString Fixed, CLASS Class);

		// Data Members
		CString m_Fixed;
		CLASS   m_Class;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Ungroup Command
//

class DLLAPI CPageEditorWnd::CCmdUngroup : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdUngroup(CPrimGroup *pGroup);

		// Data Members
		HANDLE m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor -- Order Command
//

class DLLAPI CPageEditorWnd::CCmdOrder : public CPageEditorWnd::CCmdBase
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdOrder(CString Item, UINT uID);

		// Data Members
		UINT         m_uID;
		CStringArray m_Pos;
	};

//////////////////////////////////////////////////////////////////////////
//
// Alignment Dialog
//

class DLLAPI CAlignDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CAlignDialog(BOOL fPage);
		
		// Attributes
		UINT GetHorz(void) const;
		UINT GetVert(void) const;
		BOOL UseRef (void) const;
		
	protected:
		// Data Members
		BOOL m_fPage;
		UINT m_uHorz;
		UINT m_uVert;
		BOOL m_fRef;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);

		// Implementation
		void LoadConfig(void);
		void SaveConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Keys Dialog
//

class DLLAPI CKeysDialog : public CItemDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CKeysDialog(CDualEvent *pDual, CString Title);
		
	protected:
		// Static Data
		static UINT m_uTab;

		// Data Members
		CDualEvent *m_pDual;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCancel(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text Editor
//

class DLLAPI CTextEditorWnd : public CCtrlWnd, public IDropSource, public IDropTarget
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTextEditorWnd(CPrimText *pText, int nScale, CPoint Click, BOOL fAdd, CBitmap &Back);

		// Destructor
		~CTextEditorWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropSource
		HRESULT METHOD QueryContinueDrag(BOOL fEscape, DWORD dwKeys);
		HRESULT METHOD GiveFeedback(DWORD dwEffect);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Attributes
		CString GetText(void) const;

		// Operations
		BOOL WriteBack(void);

	protected:
		// Timer IDs
		static UINT m_timerCheck;
		static UINT m_timerDouble;

		// Accelerator
		CAccelerator m_Accel;

		// Config Data
		CPoint    m_Click;
		BOOL      m_fAdd;
		CBitmap * m_pBack;

		// Bound Items
		CUISystem    * m_pSystem;
		CLangManager * m_pLang;
		CFontManager * m_pFonts;

		// Text Object
		CPrimText * m_pText;
		CString     m_Text;
		UINT        m_Font;
		INT         m_AlignH;
		INT         m_AlignV;
		INT	    m_Lead;
		CRect       m_Margin;

		// Line Data
		CArray <CRange> m_Lines;
		CArray <CPoint> m_Inits;

		// Layout
		int   m_yLine;
		int   m_nScale;
		CSize m_TextSize;
		CSize m_WorkSize;
		int   m_nLines;
		int   m_nShown;

		// Selection
		BOOL   m_fSelect;
		CPoint m_SelStart;
		CPoint m_SelEnd;
		CPoint m_SelAnchor;
		
		// States
		BOOL   m_fDouble;
		BOOL   m_fKeyb;

		// Tracking Data
		UINT	m_uCapture;

		// Drag Context
		CCursor m_MoveCursor;
		CCursor m_CopyCursor;

		// Drop Context
		CDropHelper m_DropHelp;
		BOOL        m_fDrop;
		BOOL        m_fDropMove;

		// 1D Position
		int    m_nFrom;
		int    m_nTo;
		int    m_nSize;
		int    m_nPos;
		BOOL   m_fHard;

		// 2D Position
		CPoint m_Pos;
		CSize  m_CaretSize;
		CPoint m_CaretPos;

		// GDI Object
		IGdiWindows * m_pWin;
		IGdi        * m_pGDI;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Core Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnPostCreate(void);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnSysKeyDown(UINT uCode, DWORD dwFlags);
		void OnSysKeyUp(UINT uCode, DWORD dwFlags);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnKeyUp(UINT uCode, DWORD dwFlags);
		void OnKeyChange(UINT uCode, DWORD dwFlags);
		void OnChar(UINT uCode, DWORD dwFlags);
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnRButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnRButtonDblClk(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnRButtonUp(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Edit Menu
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL CanEditPaste(void);
		BOOL CanEditDelete(void);
		void OnEditSelectAll(void);
		void OnEditDelete(CString Verb);
		void OnEditCut(void);
		void OnEditCopy(void);
		void OnEditPaste(CString Verb);

		// Text Menu
		BOOL OnTextControl(UINT uID, CCmdSource &Src);
		BOOL OnTextCommand(UINT uID);
		void OnTextProperties(void);
		void SendParent(UINT uCode);

		// Implementation
		BOOL IsMoveKey(UINT uCode);
		BOOL PerformMove(CPoint &Pos, UINT uCode);
		BOOL IsBreak(WCHAR cData);
		void MoveThisWordStart(CPoint &Pos);
		void MoveThisWordEnd(CPoint &Pos);
		void MoveNextWordStart(CPoint &Pos);
		void MovePrevWordEnd(CPoint &Pos);
		BOOL EnterChar(TCHAR cData);
		void AbortChar(void);
		void EnterText(CString Text);
		BOOL Backspace(void);
		BOOL DeleteChar(void);
		BOOL DeleteSelect(void);
		BOOL ReflowFromHere(void);
		BOOL ReflowAll(void);
		BOOL DoesNotFit(void);
		void UpdateFont(void);
		void UpdateAll(void);
		void UpdateImage(void);
		void GetLineInfo(void);
		void GetLineInfo(int nLine);
		void SyncFrom1D(void);
		void SyncFrom2D(void);
		int  CalcFrom2D(CPoint Pos);
		void SyncRange(void);
		void AdjustFromHere(int nDelta);
		void FindCaretSize(void);
		void MoveCaret(void);
		void RemakeCaret(void);
		int  GetLineWidth(void);
		int  GetCharWidth(TCHAR cData);
		BOOL MouseToText(CPoint &Pos);
		int  FindCharFromPos(int nLine, int xPos);
		int  FindPosFromChar(int nLine, int xChar);
		void FakeMouse(CPoint Pos);
		BOOL IsDown(UINT uCode);
		void StartSelect(void);
		void CheckSelect(void);
		BOOL ClearSelect(BOOL fUpdate);
		BOOL HitTestSelect(CPoint Pos);
		BOOL HitTestSelect(void);
		BOOL DefaultStart(void);
		void DefaultUpdate(void);
		BOOL SelectStart(void);
		void SelectUpdate(void);
		void SelectLine(void);
		void SelectWord(void);
		void SelectText(void);
		void DecodeEOL(CString &Text);
		void EncodeEOL(CString &Text);		
		BOOL DragStart(void);
		void DragItem(void);		
		BOOL IsDropMove(DWORD dwKeys, DWORD *pEffect);
		void DropTrack(CPoint Pos);
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL CanAcceptDataObject(IDataObject *pData);
		BOOL AcceptDataObject(IDataObject *pData);
		BOOL AcceptText(IDataObject *pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Prim Paste Special Dialog
//

class DLLAPI CPrimPasteSpecialDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CPrimPasteSpecialDialog(void);

		// Attributes
		UINT GetMode(void) const;
		
	protected:
		// Data Members
		UINT m_uMode;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Prim Position Dialog
//

class DLLAPI CPrimPositionDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CPrimPositionDialog(CPrim *pPrim, BOOL fRead, CSize Min, CRect Work);
		
	protected:
		// Data Members
		CPrim * m_pPrim;
		BOOL    m_fRead;
		CSize   m_Min;
		CRect   m_Work;
		BOOL	m_fRel;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnChangeRel(UINT uID);

		// Implementation
		void LoadConfig(void);
		void SaveConfig(void);
	};	

// End of File

#endif
