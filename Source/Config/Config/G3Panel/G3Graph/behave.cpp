
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Behavior Menu

BOOL CPageEditorWnd::OnBehaveGetInfo(UINT uID, CCmdInfo &Info)
{
	return FALSE;
	}

BOOL CPageEditorWnd::OnBehaveControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_BEHAVE_MOVE_2D:
		case IDM_BEHAVE_MOVE_POLAR:

			Src.EnableItem(CanAddMove());
			
			break;

		case IDM_BEHAVE_MOVE_REMOVE:

			Src.EnableItem(CanRemMove());
			
			break;

		case IDM_BEHAVE_ADD_ACTION:

			Src.EnableItem(CanAddAction());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnBehaveCommand(UINT uID)
{
	switch( uID ) {

		case IDM_BEHAVE_MOVE_2D:
			
			OnBehaveAddMove(AfxRuntimeClass(CPrimMove2D));
			
			break;

		case IDM_BEHAVE_MOVE_POLAR:
			
			OnBehaveAddMove(AfxRuntimeClass(CPrimMovePolar));
			
			break;

		case IDM_BEHAVE_MOVE_REMOVE:
			
			OnBehaveRemMove();
			
			break;

		case IDM_BEHAVE_ADD_ACTION:

			OnBehaveAddAction();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::HasAction(CPrim *pPrim)
{
	// REV3 -- This is a little too permissive, in that
	// it lets you move items that have, for example, data
	// entry enabled. We also need to restrict the selection
	// of data entry mode for items within move groups.

	if( pPrim->IsSet() ) {

		CPrimSet  *pSet  = (CPrimSet *) pPrim;

		CPrimList *pList = pSet->m_pList;

		UINT      uCount = pList->GetItemCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pScan = pList->GetItem(n);

			if( HasAction(pScan) ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

		CPrimWithText *pHost = (CPrimWithText *) pPrim;

		if( pHost->m_pDataItem ) {

			if( pHost->m_pDataItem->m_Entry ) {

				return TRUE;
				}
			}

		if( pHost->m_pAction ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanAddMove(void)
{
	if( !m_fScratch && !m_fRead ) {

		UINT uCount = m_SelList.GetCount();

		if( uCount ) {

			for( UINT n = 0; n < uCount; n++ ) {

				INDEX   nPrim = m_SelList[n];

				CPrim * pPrim = m_pWorkList->GetItem(nPrim);

				if( HasAction(pPrim) ) {

					return FALSE;
					}
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanRemMove(void)
{
	if( !m_fScratch && !m_fRead ) {

		if( HasLoneSelect() ) {
			
			CPrim *pPrim = GetLoneSelect();
			
			if( pPrim->IsMove() ) {

				CPrimSet *pSet = (CPrimSet *) pPrim;

				return !pSet->m_LockList;
				}
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanAddAction(void)
{
	if( HasSelect() ) {

		if( ArePropsLocked() ) {

			return FALSE;
			}

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				CPrimWithText *pHost = (CPrimWithText *) pPrim;

				if( pHost->m_pDataItem ) {

					if( pHost->m_pDataItem->m_Entry ) {

						return FALSE;
						}
					}

				if( pHost->GetActionMode() > actNone ) {
						
					CItem *pScan = pPrim;

					for(;;) {

						pScan = pScan->GetParent(2);

						if( pScan->IsKindOf(AfxRuntimeClass(CPrim)) ) {

							pPrim = (CPrim *) pScan;

							if( pPrim->IsMove() ) {

								return FALSE;
								}

							continue;
							}

						break;
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CPageEditorWnd::OnBehaveAddMove(CLASS Class)
{
	if( CanAddMove() ) {

		CString Fixed = m_pItem->GetDatabase()->AllocFixedString();

		CCmd *  pCmd  = New CCmdGroup(Fixed, Class);

		LocalExecCmd(pCmd);
		}
	}

void CPageEditorWnd::OnBehaveRemMove(void)
{
	if( CanRemMove() ) {

		CPrimGroup *pGroup = (CPrimGroup *) GetLoneSelect();

		CCmd       *pCmd   = New CCmdUngroup(pGroup);

		LocalExecCmd(pCmd);
		}
	}

BOOL CPageEditorWnd::OnBehaveAddAction(void)
{
	if( CanAddAction() ) {

		CPrim         *pPrim = GetLoneSelect();

		CPrimWithText *pHost = (CPrimWithText *) pPrim;

		HGLOBAL        hPrev = pPrim->TakeSnapshot();

		if( !pHost->HasAction() ) {
		
			pHost->AddAction();
			}

		if( OnEditItemProps(CString(IDS_ACTION_1), hPrev) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
