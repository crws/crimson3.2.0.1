
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Frame Layout

void CPageEditorWnd::FindMetrics(void)
{
	if( !m_fScratch ) {

		m_pFramer = m_pSystem->m_pFramer;

		m_pFramer->FindLayout(m_pPage->m_Size, m_pSystem->m_SoftKeys);

		m_DispSize  = m_pPage->m_Size;

		m_FrameRect = m_pFramer->GetFrameRect();

		m_TotalSize = m_pFramer->GetTotalSize();

		m_Keys.Empty();

		int nKeys = m_pFramer->GetTotalKeys();

		for( int nKey = 0; nKey < nKeys; nKey++ ) {

			CKey Key;

			Key.m_Rect  = m_pFramer->GetKeyRect(nKey);

			Key.m_uCode = m_pFramer->GetKeyCode(nKey);

			m_Keys.Append(Key);
			}
		}
	else {
		m_pFramer   = NULL;

		m_DispSize  = m_pPage->m_Size;

		m_FrameRect = m_DispSize;

		m_TotalSize = m_DispSize;

		m_Keys.Empty();
		}

	m_EditKey = 0;

	m_EditCol = 0;
	}

void CPageEditorWnd::DrawFrame(CDC &DC)
{
	if( m_pFramer ) {

		m_pFramer->DrawFrame( DC,
				      m_nScale == 0,
				      FALSE
				      );

		for( UINT n = 0; n < m_Keys.GetCount(); n++ ) {

			CKey const &Key = m_Keys[n];

			DrawKey(DC, Key.m_Rect, Key.m_uCode);
			}
		}
	}

void CPageEditorWnd::DrawKey(CDC &DC, CRect Rect, UINT uCode)
{
	CString Text = GetKeyName(uCode, FALSE);	

	if( Text.StartsWith(L"Icon") ) {

		if( uCode == m_EditKey ) {

			DC.SetTextColor(CColor(128,128,224));
			}
		else {
			DC.SetTextColor(GetKeyColor(uCode));
			}

		DC.SetBkColor(afxColor(BLACK));

		DC.SetBkMode(OPAQUE);

		m_pFramer->DrawKeyIcon(DC, Rect, uCode);
			
		return;
		}

	if( uCode == m_EditKey ) {

		CBrush Brush(CColor(128,128,224));

		DC.Select(CGdiObject(WHITE_PEN));

		DC.Select(Brush);

		m_pFramer->DrawKeyBorder(DC, Rect);

		DC.Deselect();

		DC.Deselect();
		}
	else {
		DC.Select(CGdiObject(NULL_PEN));

		DC.Select(afxBrush(WHITE));

		m_pFramer->DrawKeyBorder(DC, Rect);

		DC.Deselect();

		DC.Deselect();
		}

	CColor Color = GetKeyColor(uCode);

	if( uCode == COPS_VK_MENU ) {

		DC.SetBkMode(TRANSPARENT);

		DC.SetTextColor(Color);

		m_pFramer->DrawKeyIcon(DC, Rect, uCode);

		return;
		}	

	if( Text.StartsWith(L"Soft") ) {

		CBrush Brush(Color);

		Rect -= Rect.GetSize() / 4;

		DC.Select(CGdiObject(NULL_PEN));

		DC.Select(Brush);

		DC.RoundRect(Rect, 2);

		DC.Deselect();

		DC.Deselect();

		return;
		}

	DC.SetBkMode(TRANSPARENT);

	DC.SetTextColor(Color);

	m_pFramer->DrawKeyText(DC, Rect, Text);
	}

void CPageEditorWnd::InvalidateKey(UINT uPos)
{
	InvalidateKey(m_Keys[uPos]);
	}

void CPageEditorWnd::InvalidateKey(CKey const &Key)
{
	CRect Rect = LPtoDP(Key.m_Rect);

	Invalidate(Rect, FALSE);
	}

// Key Mouse Hooks

BOOL CPageEditorWnd::CheckKeyHover(void)
{
	if( !m_pSystem->GetDatabase()->HasFlag(L"IconLedsOnly") ) {

		CPoint Pos = DPtoLP(m_ClientPos);

		for( UINT n = 0; n < m_Keys.GetCount(); n++ ) {

			CKey const &Key = m_Keys[n];

			if( Key.m_Rect.PtInRect(Pos) ) {

				if( m_EditKey - Key.m_uCode ) {

					if( m_EditKey ) {

						InvalidateKey(m_EditPos);
						}

					m_EditKey = Key.m_uCode;

					m_EditPos = n;

					InvalidateKey(m_EditPos);

					SetTimer(m_timerKeys, 50);
					}

				return TRUE;
				}
			}

		if( m_EditKey ) {
	
			InvalidateKey(m_EditPos);
			}

		m_EditKey = 0;

		KillTimer(m_timerKeys);
		}

	return FALSE;
	}

void CPageEditorWnd::CheckKeyTimer(void)
{
	if( m_EditKey ) {

		CPoint Pos = GetClientPos();

		CRect  R1  = GetClientRect();

		if( !R1.PtInRect(Pos) ) {

			InvalidateKey(m_EditPos);

			m_EditKey = 0;

			KillTimer(m_timerKeys);
			}

		return;
		}

	KillTimer(m_timerKeys);
	}

BOOL CPageEditorWnd::KeyMenu(void)
{
	if( CheckKeyHover() ) {

		CString Name = L"KeyEditCtx";

		CMenu   Load = CMenu(PCTXT(Name));

		if( Load.IsValid() ) {

			CMenu &Menu = Load.GetSubMenu(0);

			m_JumpList.Empty();

			CEventEntry * pGlobal = m_pGlobal->GetEntry(m_EditKey);

			CEventEntry * pLocal  = m_pLocal ->GetEntry(m_EditKey);

			BuildJumpList(pGlobal);

			BuildJumpList(pLocal);

			AddJumpCommands(Menu);

			Menu.MakeOwnerDraw(FALSE);
			
			Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
					     GetCursorPos(),
					     afxMainWnd->GetHandle()
					     );

			Menu.FreeOwnerDraw();

			InvalidateKey(m_EditPos);

			KillTimer(m_timerKeys);

			m_EditKey = 0;

			m_EditCol = 0;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::EditKey(void)
{
	if( CheckKeyHover() ) {

		OnKeyEdit();

		return TRUE;
		}

	return FALSE;
	}

// Key Data

CColor CPageEditorWnd::GetKeyColor(UINT uCode)
{
	if( !m_pSystem->GetDatabase()->HasFlag(L"IconLedsOnly") ) {

		if( m_EditKey == uCode ) {

			if( m_EditCol ) {

				return m_EditCol;
				}
			}

		if( m_pGlobal->IsBroken(uCode) ) {
		
			return afxColor(RED);
			}

		if( m_pLocal->IsBroken(uCode) ) {

			return afxColor(RED);
			}

		if( m_pGlobal->HasEntry(uCode) ) {

			if( m_pLocal->HasEntry(uCode) ) {

				return CColor(0x00, 0x00, 0x80);
				}

			return CColor(0x00, 0x80, 0x00);
			}

		if( m_pLocal->HasEntry(uCode) ) {

			return CColor(0x80, 0x00, 0x80);
			}
		}

	return m_pFramer->GetKeyColor(uCode);
	}

CString CPageEditorWnd::GetKeyName(UINT uCode, BOOL fLocal)
{
	if( C3OemFeature(L"OemSD", FALSE) ) {

		if( m_pFramer->GetTotalKeys() == 10 ) {

			switch( uCode ) {

				case COPS_VK_SOFT3: return L"F3";
				case COPS_VK_SOFT4: return L"F4";
				}
			}

		switch( uCode ) {

			case COPS_VK_SOFT1: return L"F1";
			case COPS_VK_SOFT2: return L"F2";
			}
		}	

	if( uCode >= COPS_VK_SOFT1 && uCode <= COPS_VK_SOFT9 ) {

		UINT uKey = uCode - COPS_VK_SOFT1 + 1;

		if( m_pSystem->GetDatabase()->HasFlag(L"IconLeds") ) {
			
			return CPrintf(IDS("Icon Key %u"), uKey);
			}
		else {
			if( fLocal ) {

				return CPrintf(IDS_SOFT_KEY_FMT, uKey);
				}

			return CPrintf(IDS("Soft Key %u"), uKey);
			}
		}

	if( uCode == COPS_VK_MENU ) {

		if( m_pSystem->GetDatabase()->HasFlag(L"IconLeds") ) {
			
			return CPrintf(IDS("Icon Key (Home)"));
			}

		return IDS_MENU_KEY;
		}

	return L"";
	}

// Key Menu

BOOL CPageEditorWnd::OnKeyGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_KEY_CLEAR,	MAKELONG(0x0008, 0x1000),
		IDM_KEY_EDIT,	MAKELONG(0x0012, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnKeyControl(UINT uID, CCmdSource &Src)
{
	UINT uCode   = m_Keys[m_EditPos].m_uCode;

	BOOL fGlobal = m_pGlobal->HasEntry(uCode);

	BOOL fLocal  = m_pLocal ->HasEntry(uCode);

	switch( uID ) {

		case IDM_KEY_MAKE_GLOBAL:

			Src.EnableItem(!m_fScratch && fLocal && !fGlobal);

			break;

		case IDM_KEY_MAKE_LOCAL:

			Src.EnableItem(!m_fScratch && fGlobal && !fLocal);

			break;

		case IDM_KEY_CLEAR:

			Src.EnableItem(!m_fScratch && fLocal || fGlobal);

			break;

		case IDM_KEY_EDIT:

			Src.EnableItem(!m_fScratch && TRUE);

			break;

		default:

			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnKeyCommand(UINT uID)
{
	switch( uID ) {

		case IDM_KEY_MAKE_GLOBAL:

			OnKeyMakeGlobal();

			break;

		case IDM_KEY_MAKE_LOCAL:

			OnKeyMakeLocal();

			break;

		case IDM_KEY_CLEAR:

			OnKeyClear();

			break;

		case IDM_KEY_EDIT:

			OnKeyEdit();

			break;

		default:

			return FALSE;
		}

	return TRUE;
	}

void CPageEditorWnd::OnKeyMakeGlobal(void)
{
	CKey const &Key = m_Keys[m_EditPos];

	CKeyUndo Undo;

	KeyInitUndo(Undo, Key);

	m_pGlobal->ForceEntry(Key.m_uCode);

	CEventEntry * pGlobal = m_pGlobal->GetEntry(Key.m_uCode);

	CEventEntry * pLocal  = m_pLocal ->GetEntry(Key.m_uCode);

	HGLOBAL hMem = pLocal->TakeSnapshot();

	pGlobal->LoadSnapshot(hMem);

	pLocal->m_Mode = 0;

	m_pLocal->CleanEntry(Key.m_uCode);

	KeySaveUndo(Undo, CString(IDS_MAKE_GLOBAL));

	InvalidateKey(Key);
	}

void CPageEditorWnd::OnKeyMakeLocal(void)
{
	CKey const &Key = m_Keys[m_EditPos];

	CKeyUndo Undo;

	KeyInitUndo(Undo, Key);

	m_pLocal->ForceEntry(Key.m_uCode);

	CEventEntry * pGlobal = m_pGlobal->GetEntry(Key.m_uCode);

	CEventEntry * pLocal  = m_pLocal ->GetEntry(Key.m_uCode);

	HGLOBAL hMem = pGlobal->TakeSnapshot();

	pLocal->LoadSnapshot(hMem);

	pGlobal->m_Mode = 0;

	m_pGlobal->CleanEntry(Key.m_uCode);

	KeySaveUndo(Undo, CString(IDS_MAKE_LOCAL));

	InvalidateKey(Key);
	}

void CPageEditorWnd::OnKeyClear(void)
{
	CKey const &Key = m_Keys[m_EditPos];

	CKeyUndo Undo;

	KeyInitUndo(Undo, Key);

	CEventEntry * pGlobal = m_pGlobal->GetEntry(Key.m_uCode);

	CEventEntry * pLocal  = m_pLocal ->GetEntry(Key.m_uCode);

	if( pGlobal ) {

		pGlobal->m_Mode = 0;

		m_pGlobal->CleanEntry(Key.m_uCode);
		}

	if( pLocal ) {

		pLocal->m_Mode = 0;

		m_pLocal->CleanEntry(Key.m_uCode);
		}

	KeySaveUndo(Undo, CString(IDS_CLEAR_KEY));

	InvalidateKey(Key);
	}

void CPageEditorWnd::OnKeyEdit(void)
{
	CKey const &Key = m_Keys[m_EditPos];

	CKeyUndo Undo;

	KeyInitUndo(Undo, Key);

	m_EditCol = GetKeyColor(Key.m_uCode);

	InvalidateKey(m_EditPos);

	m_pGlobal->ForceEntry(Key.m_uCode);

	m_pLocal ->ForceEntry(Key.m_uCode);

	CDualEvent Dual;

	Dual.SimpleInit();

	Dual.SetParent(m_pItem);
		
	Dual.m_pGlobal = m_pGlobal->GetEntry(Key.m_uCode);

	Dual.m_pLocal  = m_pLocal ->GetEntry(Key.m_uCode);

	CString Title  = GetKeyName(Key.m_uCode, TRUE);

	CKeysDialog Dialog(&Dual, Title);

	if( m_System.ExecSemiModal(Dialog) ) {

		m_pGlobal->CleanEntry(Key.m_uCode);

		m_pLocal ->CleanEntry(Key.m_uCode);

		CString Menu = CFormat(CString(IDS_EDIT_FMT), Title);

		KeySaveUndo(Undo, Menu);
		}
	else {
		Dual.m_pGlobal->SetParent(m_pGlobal);

		Dual.m_pLocal ->SetParent(m_pLocal);

		m_pGlobal->CleanEntry(Key.m_uCode);

		m_pLocal ->CleanEntry(Key.m_uCode);

		KeyKillUndo(Undo);
		}

	InvalidateKey(Key);

	KillTimer(m_timerKeys);

	m_EditKey = 0;

	m_EditCol = 0;
	}

// Undo Helpers

void CPageEditorWnd::KeyInitUndo(CKeyUndo &Undo, CKey const &Key)
{
	Undo.m_hPrev1 = m_pGlobal->TakeSnapshot();

	Undo.m_hPrev2 = m_pLocal ->TakeSnapshot();
	}

BOOL CPageEditorWnd::KeySaveUndo(CKeyUndo &Undo, CString Menu)
{
	if( !m_fScratch ) {

		// OPTIM -- The Undo mechanism here is ridiculously expensive
		// as it saves a copy of the whole of each event list! We need
		// to add specific CCmd's for event list editing...

		CCmdItem * pCmd1 = New CCmdItem(Menu, m_pGlobal, Undo.m_hPrev1);

		CCmdItem * pCmd2 = New CCmdItem(Menu, m_pLocal,  Undo.m_hPrev2);

		if( pCmd1->IsNull() && pCmd2->IsNull() ) {

			delete pCmd1;

			delete pCmd2;

			GlobalFree(Undo.m_hPrev1);

			GlobalFree(Undo.m_hPrev2);

			return FALSE;
			}

		CString Nav   = m_System.GetNavPos();

		CCmd  * pCmd0 = New CCmdMulti(Nav, Menu, navOne);

		CCmd  * pCmd3 = New CCmdMulti(Nav, Menu, navOne);

		LocalSaveCmd(pCmd0);

		LocalSaveCmd(pCmd1);
	
		LocalSaveCmd(pCmd2);
	
		LocalSaveCmd(pCmd3);
		}
	else {
		#pragma warning(suppress: 6001)

		GlobalFree(Undo.m_hPrev1);

		#pragma warning(suppress: 6001)

		GlobalFree(Undo.m_hPrev2);
		}

	return TRUE;
	}

void CPageEditorWnd::KeyKillUndo(CKeyUndo &Undo)
{
	#pragma warning(suppress: 6001)

	GlobalFree(Undo.m_hPrev1);

	#pragma warning(suppress: 6001)

	GlobalFree(Undo.m_hPrev2);
	}

//////////////////////////////////////////////////////////////////////////
//
// Keys Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CKeysDialog, CItemDialog);

// Static Data

UINT CKeysDialog::m_uTab = 0;
		
// Constructor

CKeysDialog::CKeysDialog(CDualEvent *pDual, CString Title) : CItemDialog(pDual, Title)
{
	m_pDual = pDual;
	}
		
// Message Map

AfxMessageMap(CKeysDialog, CItemDialog)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxDispatchCommand(IDOK,     OnOkay  )
	AfxDispatchCommand(IDCANCEL, OnCancel)

	AfxMessageEnd(CKeysDialog)
	};

// Message Handlers

void CKeysDialog::OnPostCreate(void)
{
	CItemDialog::OnPostCreate();

	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		CMultiViewWnd *pView = (CMultiViewWnd *) m_pView;

		if( m_pDual->m_pGlobal->m_Mode ) {

			if( !m_pDual->m_pLocal->m_Mode ) {

				pView->SelectTab(1);

				return;
				}
			}

		pView->SelectTab(m_uTab);
		}
	}

// Command Handlers

BOOL CKeysDialog::OnOkay(UINT uID)
{
	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		CMultiViewWnd *pView = (CMultiViewWnd *) m_pView;

		m_uTab = pView->GetTabIndex();
		}

	return CItemDialog::OnOkay(uID);
	}

BOOL CKeysDialog::OnCancel(UINT uID)
{
	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		CMultiViewWnd *pView = (CMultiViewWnd *) m_pView;

		m_uTab = pView->GetTabIndex();
		}

	return CItemDialog::OnCancel(uID);
	}

// End of File
