
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Textor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Data Menu

BOOL CPageEditorWnd::OnDataGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_DATA_ADD,          MAKELONG(0x000E, 0x3001),
		IDM_DATA_REM,	       MAKELONG(0x0008, 0x1000),
		IDM_DATA_MODE_DISPLAY, MAKELONG(0x0010, 0x3001),
		IDM_DATA_MODE_ENTRY,   MAKELONG(0x0011, 0x3001),
		IDM_DATA_MODE_FLIP,    MAKELONG(0x0011, 0x3001),
		IDM_DATA_PROPERTIES,   MAKELONG(0x0012, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			return FALSE;
			}
		}

	uID -= IDM_DATA_TITLE;

	uID += IDM_BLOCK_TITLE;

	return OnBlockGetInfo(uID, Info);
	}

BOOL CPageEditorWnd::OnDataControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	CPrimWithText *pHost = NULL;

	CPrimData     *pData = NULL;

	BOOL           fText = FALSE;

	if( !ArePropsLocked() ) {

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				pHost = (CPrimWithText *) pPrim;

				if( pHost->HasData() ) {

					pData = pHost->m_pDataItem;
					}

				if( pHost->HasText() ) {

					fText = TRUE;
					}
				}
			}
		}

	switch( uID ) {

		case IDM_DATA_ADD:

			Src.EnableItem(pHost && !fText && !pData);

			break;

		case IDM_DATA_REM:

			Src.EnableItem(pData ? TRUE : FALSE);

			break;

		case IDM_DATA_EDIT:

			Src.EnableItem(pHost && !fText);

			break;

		case IDM_DATA_MODE_DISPLAY:

			if( pData ) {

				Src.EnableItem(pData->CanWrite());

				Src.CheckItem (!pData->m_Entry);
				}

			break;

		case IDM_DATA_MODE_ENTRY:
		case IDM_DATA_MODE_FLIP:

			if( pData ) {

				Src.EnableItem(pData->CanWrite());

				Src.CheckItem (pData->m_Entry);
				}

			break;
		
		case IDM_DATA_SHOW_DATA:
		case IDM_DATA_SHOW_BOTH:
		case IDM_DATA_SHOW_LABEL:

			if( pData ) {

				Src.EnableItem(TRUE);

				Src.CheckItem (pData->m_Content == uID - IDM_DATA_SHOW_DATA);
				}

			break;

		case IDM_DATA_PROPERTIES:

			Src.EnableItem(pData ? TRUE : FALSE);

			break;

		default:
			if( pData ) {
				
				uID -= IDM_DATA_TITLE;

				uID += IDM_BLOCK_TITLE;
				
				return OnBlockControl(uID, Src);
				}

			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnDataCommand(UINT uID)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	CPrim         *pPrim = NULL;

	CPrimWithText *pHost = NULL;

	CPrimData     *pData = NULL;

	BOOL           fText = FALSE;

	if( !ArePropsLocked() ) {

		if( HasLoneSelect() ) {

			pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				pHost = (CPrimWithText *) pPrim;

				if( pHost->HasData() ) {

					pData = pHost->m_pDataItem;
					}

				if( pHost->HasText() ) {

					fText = TRUE;
					}
				}
			}
		}

	switch( uID ) {

		case IDM_DATA_ADD:

			OnDataAdd();

			break;

		case IDM_DATA_EDIT:

			if( pData ) {

				OnEditItemProps(CString(IDS_DATA));
				}
			else
				OnDataAdd();

			break;

		case IDM_DATA_REM:
			
			OnDataRemove();

			break;

		case IDM_DATA_MODE_DISPLAY:
		case IDM_DATA_MODE_ENTRY:

			if( pData ) {

				if( pData->m_Entry != uID - IDM_DATA_MODE_DISPLAY ) {
			
					MakeBlockCmd(pPrim, CString(IDS_CHANGE_DATA_2));

					pData->SetEntry(uID - IDM_DATA_MODE_DISPLAY);

					SaveBlockCmd();
					}
				}

			break;

		case IDM_DATA_MODE_FLIP:

			if( pData ) {

				MakeBlockCmd(pPrim, CString(IDS_CHANGE_DATA_2));

				pData->SetEntry(!pData->m_Entry);

				SaveBlockCmd();
				}

			break;

		case IDM_DATA_SHOW_DATA:
		case IDM_DATA_SHOW_BOTH:
		case IDM_DATA_SHOW_LABEL:

			if( pData ) {

				if( pData->m_Content != uID - IDM_DATA_SHOW_DATA ) {
			
					MakeBlockCmd(pPrim, CString(IDS_CHANGE_DATA_3));

					pData->SetContent(uID - IDM_DATA_SHOW_DATA);

					SaveBlockCmd();
					}
				}

			break;

		case IDM_DATA_PROPERTIES:

			if( pData ) {

				OnEditItemProps(CString(IDS_DATA));
				}

			break;

		default:
			if( pData ) {
				
				uID -= IDM_DATA_TITLE;

				uID += IDM_BLOCK_TITLE;
				
				return OnBlockCommand(uID);
				}

			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnDataAdd(void)
{
	CPrim         * pPrim = GetLoneSelect();

	CPrimWithText * pHost = (CPrimWithText *) pPrim;

	MakeBlockCmd(pPrim, CString(IDS_ADD_DATA));

	pHost->AddData();

	UpdateSelectData();

	SaveBlockCmd();

	if( !OnEditItemProps(CString(IDS_DATA)) ) {

		SendRewind();

		return FALSE;
		}

	return TRUE;
	}

void CPageEditorWnd::OnDataRemove(void)
{
	CPrim         * pPrim = GetLoneSelect();

	CPrimWithText * pHost = (CPrimWithText *) pPrim;

	MakeBlockCmd(pPrim, CString(IDS_REMOVE_DATA));

	pHost->RemData();

	UpdateSelectData();

	SaveBlockCmd();
	}

// End of File
