
#include "intern.hpp"

#include "../G3Prims/DispCatWnd.hpp"

#include "../G3Prims/ScratchPadWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Dynamic Class

AfxImplementDynamicClass(CPageEditorWnd, CViewWnd);

// Timer IDs

UINT CPageEditorWnd::m_timerHover  = CWnd::AllocTimerID();

UINT CPageEditorWnd::m_timerGhost  = CWnd::AllocTimerID();

UINT CPageEditorWnd::m_timerKeys   = CWnd::AllocTimerID();

UINT CPageEditorWnd::m_timerScroll = CWnd::AllocTimerID();

// Options

BOOL const CPageEditorWnd::m_fCoalesceMove = TRUE;

BOOL const CPageEditorWnd::m_fCoalesceSize = TRUE;

BOOL const CPageEditorWnd::m_fCoalesceHand = TRUE;

// Keyboard Hook

HHOOK CPageEditorWnd::m_hKeyHook = NULL;

// Constructor

CPageEditorWnd::CPageEditorWnd(void)
{
	m_fRecycle     = TRUE;

	m_pScrollC     = New CStatic;

	m_pScrollH     = New CScrollBar;

	m_pScrollV     = New CScrollBar;

	m_pGhost       = NULL;

	m_nScale       = -1;

	m_uMode        = modeSelect;

	m_uCapture     = captNone;

	m_Pos.x        = -1;

	m_Pos.y        = -1;

	m_fHover       = TRUE;

	m_pHover       = NULL;
	
	m_nHover       = NULL;

	m_cfData       = WORD(RegisterClipboardFormat(L"C3.1 Graphics"));

	m_cfSpec       = WORD(RegisterClipboardFormat(L"C3.1 Prim Special"));

	m_fDrag        = FALSE;

	m_fDrop        = FALSE;

	m_pAcceptList  = NULL;

	m_pWorkList    = NULL;

	m_pWorkSet     = NULL;
	
	m_pTextCtrl    = NULL;

	m_fLockUI      = FALSE;

	m_fScratch     = FALSE;

	m_pScratch     = NULL;

	m_pBuddy       = NULL;

	m_Accel.Create(L"PageEditorMenu");

	LoadCursors();

	LoadConfig();
	}

// Destructor

CPageEditorWnd::~CPageEditorWnd(void)
{
	DestroyGDI();

	SaveConfig();
	}

// Overridables

void CPageEditorWnd::OnAttach(void)
{
	m_pPage   = (CDispPage *) m_pItem;

	m_fRead   = m_pPage->GetDatabase()->IsReadOnly();

	m_pProps  = m_pPage->m_pProps;

	m_pList   = m_pPage->m_pList;

	m_pSystem = (CUISystem *) m_pPage->GetDatabase()->GetSystemItem();

	m_pLang   = m_pSystem->m_pLang;

	m_pFonts  = m_pSystem->m_pUI->m_pFonts;

	m_pGlobal = m_pSystem->m_pUI->m_pEvents;

	m_pLocal  = m_pPage->m_pEvents;

	if( m_hWnd ) {
	
		FindMetrics();

		CreateGDI();

		FindScaleLimits();

		FindLayout();

		ClearWorkList();

		ClearSelect(TRUE);

		RestoreSelect();

		if( m_uMode == modePickLoc ) {

			SetMode(modeSelect, TRUE);
			}

		if( !m_fLockUI ) {

			UpdateImage();

			Invalidate(FALSE);
			}
		}

	CViewWnd::OnAttach();
	}

void CPageEditorWnd::OnDetach(void)
{
	if( m_uMode == modeText ) {

		SetMode(modeSelect, TRUE);
		}

	ClearSelect(TRUE);

	DestroyGDI();

	CViewWnd::OnDetach();
	}

CString CPageEditorWnd::OnGetNavPos(void)
{
	CString Nav    = m_pWorkList->GetFixedPath();

	UINT    uCount = m_SelList.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pWorkList->GetItem(m_SelList[n]);

		Nav += n ? L'-' : L'/';

		Nav += pPrim->GetFixedName();
		}

	return Nav;
	}

BOOL CPageEditorWnd::OnNavigate(CString const &Nav)
{
	CString Base = m_pList->GetFixedPath();

	if( Nav.StartsWith(Base) ) {

		ClearWorkList();

		ClearSelect(TRUE);

		CString Rest = Nav.Mid(Base.GetLength() + 1);

		if( !Rest.IsEmpty() ) {

			Rest = Rest.StripToken(':');

			for(;;) {

				CString Item = Rest.StripToken('/');

				if( !Rest.IsEmpty() ) {

					INDEX Index = m_pWorkList->FindFixedName(Item);

					if( m_pWorkList->Failed(Index) ) {

						return FALSE;
						}

					CPrim *pPrim = m_pWorkList->GetItem(Index);

					if( pPrim->IsSet() ) {

						m_pWorkSet  = (CPrimSet *) pPrim;

						m_pWorkList = m_pWorkSet->m_pList;

						m_WorkRect  = m_pWorkSet->GetRect();

						if( pPrim->IsWidget() ) {

							CPrimWidget *pWidget = (CPrimWidget *) pPrim;

							CString      Next    = Rest.StripToken('/');

							if( pWidget->m_pData->GetFixedName() == Next ) {

								ClimbWorkList(FALSE);

								return TRUE;
								}

							continue;
							}
					
						Rest.StripToken('/');

						continue;
						}
					}

				if( m_uMode == modePickLoc ) {

					SetMode(modeSelect, TRUE);
					}

				CStringArray List;

				Item.Tokenize(List, '-');

				for( UINT n = 0; n < List.GetCount(); n++ ) {

					INDEX Index = m_pWorkList->FindFixedName(List[n]);

					AddSelect(Index, TRUE);
					}

				UpdateAll();

				return TRUE;
				}
			}

		return TRUE;
		}
	else {
		if( !m_fScratch ) {

			return m_pScratch->Navigate(Nav);
			}

		return FALSE;
		}
	}

// Routing Control

BOOL CPageEditorWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( !m_fScratch ) {

		if( m_pScratch->IsActive() ) {

			return m_pScratch->RouteMessage(Message, lResult);
			}
		}

	if( m_uMode == modeText ) {

		if( m_pTextCtrl ) {
			
			if( m_pTextCtrl->RouteMessage(Message, lResult) ) {

				return TRUE;
				}
			}
		}
	
	return CViewWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CPageEditorWnd, CViewWnd)
{
	// TODO -- Cancel pick mode on menu display???

	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_GOINGIDLE)
	AfxDispatchMessage(WM_GETMETRIC)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)

	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_HSCROLL)
	AfxDispatchMessage(WM_VSCROLL)

	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_MBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_RBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchGetInfoType(IDM_EDIT, OnEditGetInfo)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxDispatchGetInfoType(IDM_BLOCK, OnBlockGetInfo)
	AfxDispatchControlType(IDM_BLOCK, OnBlockControl)
	AfxDispatchCommandType(IDM_BLOCK, OnBlockCommand)

	AfxDispatchGetInfoType(IDM_TEXT, OnTextGetInfo)
	AfxDispatchControlType(IDM_TEXT, OnTextControl)
	AfxDispatchCommandType(IDM_TEXT, OnTextCommand)

	AfxDispatchGetInfoType(IDM_DATA, OnDataGetInfo)
	AfxDispatchControlType(IDM_DATA, OnDataControl)
	AfxDispatchCommandType(IDM_DATA, OnDataCommand)

	AfxDispatchGetInfoType(IDM_MODE, OnModeGetInfo)
	AfxDispatchControlType(IDM_MODE, OnModeControl)
	AfxDispatchCommandType(IDM_MODE, OnModeCommand)

	AfxDispatchGetInfoType(IDM_VIEW, OnViewGetInfo)
	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxDispatchGetInfoType(IDM_BEHAVE, OnBehaveGetInfo)
	AfxDispatchControlType(IDM_BEHAVE, OnBehaveControl)
	AfxDispatchCommandType(IDM_BEHAVE, OnBehaveCommand)

	AfxDispatchGetInfoType(IDM_ARRANGE, OnArrangeGetInfo)
	AfxDispatchControlType(IDM_ARRANGE, OnArrangeControl)
	AfxDispatchCommandType(IDM_ARRANGE, OnArrangeCommand)

	AfxDispatchGetInfoType(IDM_TRANS, OnTransformGetInfo)
	AfxDispatchControlType(IDM_TRANS, OnTransformControl)
	AfxDispatchCommandType(IDM_TRANS, OnTransformCommand)

	AfxDispatchGetInfoType(IDM_ORG, OnOrganizeGetInfo)
	AfxDispatchControlType(IDM_ORG, OnOrganizeControl)
	AfxDispatchCommandType(IDM_ORG, OnOrganizeCommand)

	AfxDispatchGetInfoType(IDM_KEY, OnKeyGetInfo)
	AfxDispatchControlType(IDM_KEY, OnKeyControl)
	AfxDispatchCommandType(IDM_KEY, OnKeyCommand)

	AfxDispatchGetInfoType(IDM_GO, OnGoGetInfo)
	AfxDispatchControlType(IDM_GO, OnGoControl)
	AfxDispatchCommandType(IDM_GO, OnGoCommand)

	AfxDispatchGetInfoType(IDM_SELECT, OnSelectGetInfo)
	AfxDispatchControlType(IDM_SELECT, OnSelectControl)
	AfxDispatchCommandType(IDM_SELECT, OnSelectCommand)

	AfxMessageEnd(CPageEditorWnd)
	};

// Accelerators

BOOL CPageEditorWnd::OnAccelerator(MSG &Msg)
{
	if( HasFocus() ) {

		return m_Accel.Translate(Msg);
		}

	return FALSE;
	}

// Core Message Handlers

UINT CPageEditorWnd::OnCreate(CREATESTRUCT &Create)
{
	if( m_pItem ) {

		FindMetrics();

		CreateGDI();

		m_pList->Draw(m_pGDI, drawWhole);
		}

	return 0;
	}

void CPageEditorWnd::OnPostCreate(void)
{
	if( m_fScratch ) {

		m_System.Bind();

		CDispCatWnd *pCatView = (CDispCatWnd *) m_System.GetCatView(AfxRuntimeClass(CDispCatWnd));

		m_pScratch = pCatView->GetScratchPadWnd();

		m_pBuddy   = pCatView->GetMainEditor();
		}
	else {
		m_System.Bind(this);

		CDispCatWnd *pCatView = (CDispCatWnd *) m_System.GetCatView(AfxRuntimeClass(CDispCatWnd));

		pCatView->SetMainEditor(this);

		m_pScratch = pCatView->GetScratchPadWnd();

		m_pBuddy   = pCatView->GetScratchPadEditor();
		}

	m_DropHelp.Bind(m_hWnd, this);

	CreateScrollBars();

	FindScaleLimits();

	FindLayout();

	ClearWorkList();

	ClearSelect(TRUE);

	UpdateImage();
	}

void CPageEditorWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	if( m_uMode == modeText ) {

		Menu.MergeMenu(CMenu(L"TextEditorMenu"));

		return;
		}

	Menu.MergeMenu(CMenu(L"PageEditorMenu"));
	}

void CPageEditorWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( m_uMode == modeText ) {

		Menu.AppendMenu(CMenu(L"TextEditorTool"));

		return;
		}

	Menu.AppendMenu(CMenu(L"PageEditorTool"));
	}

void CPageEditorWnd::OnGoingIdle(void)
{
	PollGhost();
	}

void CPageEditorWnd::OnGetMetric(UINT uCode, CSize &Size)
{
	if( uCode == MC_ACTIVE_POSITION || uCode == MC_ACTIVE_SIZE ) {

		// TODO -- This is quite expensive to keep doing. We should
		// probably only do it when the scale or size changes...

		CClientDC DC(ThisObject);

		ConfigDC(DC);

		CRect Disp = CRect(m_FrameRect.GetTopLeft(), m_DispSize);

		DC.LPtoDP(Disp);

		CRect Used = Disp & GetClientRect();

		if( uCode == MC_ACTIVE_POSITION ) {

			Size = Used.GetTopLeft();
			}

		if( uCode == MC_ACTIVE_SIZE ) {

			Size = Used.GetSize();
			}
		}
	}

void CPageEditorWnd::OnSize(UINT uType, CSize Size)
{
	if( uType == SIZE_RESTORED || uType == SIZE_MAXIMIZED ) {

		if( m_pItem ) {

			AdjustLayout(TRUE);
		
			if( m_uMode == modeText ) {

				SetMode(modeSelect);
				}
			}
		}
	}

void CPageEditorWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_uMode == modeText ) {

		m_pTextCtrl->SetFocus();
		}

	Invalidate(FALSE);
	}

void CPageEditorWnd::OnKillFocus(CWnd &Wnd)
{
	KillGhost();

	Invalidate(FALSE);
	}

void CPageEditorWnd::OnCancelMode(void)
{
	m_PickNav.Empty();

	SetMode(modeSelect, FALSE);
	}

void CPageEditorWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	OnKeyChange(uCode, dwFlags);

	if( uCode == VK_RETURN ) {

		if( m_uMode == modeText ) {

			SetMode(modeSelect);
			}
		}

	if( uCode == VK_ESCAPE ) {

		if( m_uCapture ) {

			m_uCapture = captNone;

			ReleaseCapture();

			TrackEnd(TRUE);

			UpdateImage();

			Invalidate(FALSE);

			ShowDefaultStatus();
			}

		if( SetMode(modeSelect, TRUE) ) {

			return;
			}

		if( HasSelect() ) {

			ClearSelect(FALSE);

			return;
			}

		ClimbWorkList(TRUE);
		}

	if( m_uMode == modeSelect ) {

		if( HasSelect() ) {

			switch( uCode ) {

				case VK_TAB:
					
					if( HasLoneSelect() ) {

						INDEX n = m_SelList[0];

						if( IsDown(VK_SHIFT) ) {

							if( !m_pWorkList->GetPrev(n) ) {

								n = m_pWorkList->GetTail();
								}
							}
						else {
							if( !m_pWorkList->GetNext(n) ) {

								n = m_pWorkList->GetHead();
								}
							}

						SetSelect(n);
						}
					break;

				case VK_LEFT:
					
					NudgeSelection(-1,  0, TRUE);
					
					break;

				case VK_RIGHT:
					
					NudgeSelection(+1,  0, TRUE);
					
					break;

				case VK_UP:
					
					NudgeSelection( 0, -1, TRUE);
					
					break;

				case VK_DOWN:
					
					NudgeSelection( 0, +1, TRUE);
					
					break;
				}
			}
		}
	}

void CPageEditorWnd::OnKeyUp(UINT uCode, DWORD dwFlags)
{
	OnKeyChange(uCode, dwFlags);
	}

void CPageEditorWnd::OnKeyChange(UINT uCode, DWORD dwFlags)
{
	if( uCode == VK_SHIFT ) {

		switch( m_uCapture ) {

			case captNone:
			case captCreate:

				TrackUpdate();
				
				break;
			}

		TrackSetCursor();
		}

	if( uCode == VK_CONTROL ) {

		switch( m_uCapture ) {

			case captSize:

				TrackUpdate();
				
				break;
			}

		TrackSetCursor();
		}
	}

// Mode Management

BOOL CPageEditorWnd::SetMode(UINT uMode)
{
	return SetMode(uMode, FALSE);
	}

BOOL CPageEditorWnd::SetMode(UINT uMode, BOOL fAbort)
{
	if( m_uMode != uMode ) {

		BOOL fFocus = HasFocus();

		m_pHover = NULL;

		m_nHover = NULL;

		switch( m_uMode ) {

			case modePickAny:

				if( !m_PickNav.IsEmpty() ) {

					m_System.Navigate(m_PickNav);
					}

				UpdateImage();

				break;
			
			case modeText:

				fFocus = m_pTextCtrl->HasFocus();

				LeaveTextMode(fAbort);

				break;
			}

		if( !m_pItem ) {

			m_uMode = uMode;
			}
		else {
			switch( m_uMode = uMode ) {

				case modeText:

					UpdateImage();

					EnterTextMode();

					break;
				
				case modePickLoc:

					m_PickNav.Empty();

					break;

				case modePickAny:

					m_PickNav = m_System.GetNavPos();

					UpdateImage();

					break;

				default:

					UpdateImage();

					break;
				}
			}

		KillGhost();

		Invalidate(FALSE);

		TrackSetCursor();

		if( fFocus ) {

			SetFocus();
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::SetTextMode(BOOL fMouse, BOOL fAdd)
{
	if( AllowTextMode() ) {

		// LATER -- Make sure the whole text area is visible?

		m_fTextMouse = fMouse;

		m_fTextAdd   = fAdd;

		return SetMode(modeText);
		}

	return FALSE;
	}

void CPageEditorWnd::CheckMode(void)
{
	if( m_uMode == modeGrab ) {

		if( !m_fScrollH && !m_fScrollV ) {

			SetMode(modeSelect);
			}
		}
	}

// Status Display

void CPageEditorWnd::ShowDefaultStatus(void)
{
	CString Text;

	if( HasFocus() && m_fPosValid ) {

		UINT uCount = m_SelList.GetCount();

		if( uCount ) {

			if( uCount > 1 ) {

				Text.Printf(IDS_FMT_ITEMS, uCount);
				}
			else {
				CPrim * pPrim = GetLoneSelect();

				CString Name  = pPrim->GetHumanName();

				BOOL    fHit  = FALSE;

				Text.Printf(IDS_FMT_SELECTED, Name);

				// TODO -- Should this be a method on the primitive?

				if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) { 

					CPrimWithText *pHost = (CPrimWithText *) pPrim;

					if( pHost->m_pDataItem ) {

						if( pHost->m_pDataItem->m_pValue ) {

							Text += L"  (";

							Text += pHost->m_pDataItem->m_pValue->GetSource(TRUE);

							Text += L")";

							fHit = TRUE;
							}
						}
					}

				if( !fHit ) {

					CMetaData const *pMeta = NULL;
					
					// cppcheck-suppress knownConditionTrueFalse

					if( !pMeta ) {
						
						pMeta = pPrim->FindMetaData(L"Value");
						}

					if( !pMeta ) {
						
						pMeta = pPrim->FindMetaData(L"State");
						}

					if( pMeta ) {

						CCodedItem *pCode = (CCodedItem *) pMeta->GetObject(pPrim);

						if( pCode ) {
							
							Text += L"  (";

							Text += pCode->GetSource(TRUE);

							Text += L")";
							}
						}
					}
				}

			ShowStatus(m_SelRect, Text);

			return;
			}

		CString Zoom = m_nScale ? CPrintf(L"%u:1", m_nScale) : CString(IDS_FULL);

		Text.Printf( IDS_ZOOM_FMT_POSITION_1,
			     Zoom,
			     m_Pos.x,
			     m_Pos.y
			     );
		}

	afxThread->SetStatusText(Text);
	}

void CPageEditorWnd::ShowTrackStatus(void)
{
	ShowStatus(m_TrackRect, L"");
	}

void CPageEditorWnd::ShowPickStatus(void)
{
	CString Text = IDS_CLICK_ON_SOURCE;

	if( m_uMode == modePickAny ) {

		Text += L' ';

		Text += CString(IDS_YOU_MAY_NAVIGATE);
		}

	afxThread->SetStatusText(Text);
	}

void CPageEditorWnd::ShowStatus(CRect const &Rect, PCTXT pMore)
{
	CString Text;

	CString Zoom = m_nScale ? CPrintf(L"%u:1", m_nScale) : L"FULL";

	Text += CPrintf( IDS_ZOOM_FMT_POSITION_2,
			 Zoom,
			 Rect.left,
			 Rect.top,
			 Rect.right -1,
			 Rect.bottom-1
			 );
	
	Text += CPrintf( IDS_SIZE_FMT,
			 abs(Rect.cx()),
			 abs(Rect.cy()),
			 pMore
			 );

	afxThread->SetStatusText(Text);
	}

// Key Status

BOOL CPageEditorWnd::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000) ? TRUE : FALSE;
	}

// Implementation

void CPageEditorWnd::LoadCursors(void)
{
	m_MoveCursor     .Create(L"DispMove");

	m_CopyCursor     .Create(L"DispCopy");

	m_LineCursor     .Create(L"DispLine");

	m_GrabOpenCursor .Create(L"DispOpenHand");

	m_GrabCloseCursor.Create(L"DispCloseHand");

	m_ZoomInCursor   .Create(L"DispZoomIn");

	m_ZoomOutCursor  .Create(L"DispZoomOut");

	m_PickCursor     .Create(L"DispPick");

	m_MissCursor     .Create(L"DispMiss");
	}

void CPageEditorWnd::LoadConfig(void)
{
	if( m_fScratch ) {

		m_fAlignEdge   = TRUE;
		m_fAlignCenter = TRUE;
		m_fGridShow    = FALSE;
		m_bGridBlend   = 96;
		m_uGridSnap    = 0;
		m_bItemBlend   = 150;
		m_bBackBlend   = 64;
		m_bMastBlend   = 128;
		m_fMastShow    = FALSE;
		m_fShowError   = TRUE;
		m_nItemEdge    = 5;
		}
	else {
		CRegKey Key = afxModule->GetApp()->GetUserRegKey();

		Key.MoveTo(L"G3Graph");

		Key.MoveTo(L"General");

		BYTE bMaster   = BYTE(C3OemFeature(L"OemSD", FALSE) ? 255 : 128);

		m_fAlignEdge   = BOOL(Key.GetValue(L"AlignEdge",   UINT(1)));

		m_fAlignCenter = BOOL(Key.GetValue(L"AlignCenter", UINT(1)));

		m_fGridShow    = BOOL(Key.GetValue(L"GridShow",    UINT(0)));

		m_bGridBlend   = BYTE(Key.GetValue(L"GridBlend",   UINT(96)));

		m_uGridSnap    = UINT(Key.GetValue(L"GridSnap",    UINT(7)));

		m_bItemBlend   = BYTE(Key.GetValue(L"ItemBlend",   UINT(150)));

		m_bBackBlend   = BYTE(Key.GetValue(L"BackBlend",   UINT(64)));

		m_bMastBlend   = BYTE(Key.GetValue(L"MastBlend",   UINT(bMaster)));

		m_fMastShow    = BOOL(Key.GetValue(L"ShowMast",    UINT(1)));

		m_fShowError   = BOOL(Key.GetValue(L"ShowError",   UINT(1)));

		m_nItemEdge    = INT (Key.GetValue(L"ItemEdge",    UINT(5)));

		if( !Key.GetValue(L"Version", UINT(0)) ) {

			m_bGridBlend = 96;

			Key.SetValue(L"Version", UINT(1));
			}
		}

	if( !afxCanBlend ) {

		m_bGridBlend = 0;

		m_bItemBlend = 0;

		m_bBackBlend = 0;

		m_bMastBlend = 0;
		}

	m_nItemHalf = (m_nItemEdge + 1) / 2;
	}

void CPageEditorWnd::SaveConfig(void)
{
	if( !m_fScratch ) {

		CRegKey Key = afxModule->GetApp()->GetUserRegKey();

		Key.MoveTo(L"G3Graph");

		Key.MoveTo(L"General");

		Key.SetValue(L"AlignEdge",   UINT(m_fAlignEdge));

		Key.SetValue(L"AlignCenter", UINT(m_fAlignCenter));

		Key.SetValue(L"GridShow",    UINT(m_fGridShow));

		Key.SetValue(L"GridSnap",    UINT(m_uGridSnap));

		Key.SetValue(L"ItemEdge",    UINT(m_nItemEdge));

		Key.SetValue(L"ShowMast",    UINT(m_fMastShow));

		Key.SetValue(L"ShowError",   UINT(m_fShowError));

		if( afxCanBlend ) {

			Key.SetValue(L"GridBlend", UINT(m_bGridBlend));

			Key.SetValue(L"ItemBlend", UINT(m_bItemBlend));

			Key.SetValue(L"BackBlend", UINT(m_bBackBlend));

			Key.SetValue(L"MastBlend", UINT(m_bMastBlend));
			}
		}
	}

void CPageEditorWnd::SendRewind(void)
{
	afxMainWnd->PostMessage(WM_COMMAND, IDM_EDIT_BACK);
	}

void CPageEditorWnd::RestoreSelect(void)
{
	RestoreSelect(m_DragNav);

	RestoreSelect(m_PickNav);
	}

BOOL CPageEditorWnd::RestoreSelect(CString Nav)
{
	if( !Nav.IsEmpty() ) {

		CString Match = m_pItem->GetFixedPath();

		if( Nav.StartsWith(Match) ) {

			OnNavigate(Nav);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
