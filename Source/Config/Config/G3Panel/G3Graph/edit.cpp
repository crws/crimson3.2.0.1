
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Edit Menu

BOOL CPageEditorWnd::OnEditGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_EDIT_COPY_SELECT, MAKELONG(0x0017, 0x3000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		switch( uID ) {

			case IDM_EDIT_UNDO:
			case IDM_EDIT_REDO:

				return TRUE;
			}

		return FALSE;
		}

	switch( uID ) {

		case IDM_EDIT_DELETE:
		case IDM_EDIT_CUT:

			Src.EnableItem(CanEditDelete());

			break;

		case IDM_EDIT_SMART_DUP:
			
			Src.EnableItem(HasSelect() && !IsListLocked());

			break;

		case IDM_EDIT_COPY:
			
			Src.EnableItem(HasSelect());
			
			break;

		case IDM_EDIT_LOAD_DEFS:
			
			Src.EnableItem(!m_fScratch && HasSelect() && !ArePropsLocked());
			
			break;

		case IDM_EDIT_PASTE:
			
			Src.EnableItem(CanEditPaste());
			
			break;

		case IDM_EDIT_PASTE_SPECIAL:
			
			Src.EnableItem(CanEditPasteSpecial());
			
			break;

		case IDM_EDIT_SELECT_ALL:
			
			Src.EnableItem(m_pWorkList->GetItemCount());
			
			break;

		case IDM_EDIT_COPY_SIZE:

			Src.EnableItem(HasSelect() && !IsPosLocked());

			break;

		case IDM_EDIT_COPY_ALL:
		case IDM_EDIT_COPY_FORMAT:
		case IDM_EDIT_COPY_FONT:
		case IDM_EDIT_COPY_FIGURE:
		case IDM_EDIT_COPY_ACTION:
		case IDM_EDIT_COPY_SELECT:

			Src.EnableItem(HasSelect() && !IsPosLocked());

			break;

		case IDM_EDIT_PROPERTIES:
			
			Src.EnableItem(CanEditItemProps());

			break;

		case IDM_EDIT_POSITION:
			
			Src.EnableItem(CanEditItemPos());

			break;

		case IDM_EDIT_SUMMARY:

			Src.EnableItem(FALSE && HasLoneSelect());

			break;

		case IDM_EDIT_FIND:

			Src.EnableItem(TRUE);

			break;

		case IDM_EDIT_COPY_PAGE:
		case IDM_EDIT_COPY_PANEL:
		case IDM_EDIT_WEB_FRAME:

			Src.EnableItem(!m_fScratch && !HasSelect());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:
			
			OnEditDelete(CString(IDS_DELETE));
			
			break;
		
		case IDM_EDIT_CUT:
			
			OnEditCut();
			
			break;
		
		case IDM_EDIT_COPY:
			
			OnEditCopy(FALSE);
			
			break;

		case IDM_EDIT_PASTE:
			
			OnEditPaste(CString(IDS_PASTE), pastePaste);
			
			break;
		
		case IDM_EDIT_PASTE_SPECIAL:
			
			OnEditPasteSpecial();
			
			break;
		
		case IDM_EDIT_SMART_DUP:
			
			OnEditDuplicate();
			
			break;
		
		case IDM_EDIT_SELECT_ALL:
			
			OnEditSelectAll();
			
			break;

		case IDM_EDIT_COPY_SIZE:

			OnEditCopySizeInit();

			break;

		case IDM_EDIT_COPY_ALL:
		case IDM_EDIT_COPY_FORMAT:
		case IDM_EDIT_COPY_FONT:
		case IDM_EDIT_COPY_FIGURE:
		case IDM_EDIT_COPY_ACTION:
		case IDM_EDIT_COPY_SELECT:

			OnEditCopyPropsInit(uID);

			break;

		case IDM_EDIT_SUMMARY:

			OnEditSummary();

			break;

		case IDM_EDIT_PROPERTIES:

			if( HasSelect() ) {
			
				OnEditItemProps();
				}
			else
				OnEditPageProps();
			
			break;

		case IDM_EDIT_POSITION:

			OnEditItemPos();
			
			break;

		case IDM_EDIT_FIND:

			OnEditFind();

			break;

		case IDM_EDIT_COPY_PAGE:
		case IDM_EDIT_COPY_PANEL:

			OnEditCopyPage(uID == IDM_EDIT_COPY_PANEL);

			break;

		case IDM_EDIT_WEB_FRAME:

			OnEditWebFrame();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::CanEditPaste(void)
{
	if( !IsListLocked() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			UINT uType;

			if( CanAcceptDataObject(pData, uType) ) {

				pData->Release();

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanEditPasteSpecial(void)
{
	if( !IsListLocked() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			FORMATETC Fmt = { m_cfSpec, NULL, 1, -1, TYMED_HGLOBAL };

			if( pData->QueryGetData(&Fmt) == S_OK ) {

				pData->Release();

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanEditDelete(void)
{
	if( HasSelect() ) {

		if( IsPosLocked() ) {

			return FALSE;
			}

		if( m_pWorkSet ) {

			if( m_SelList.GetCount() == m_pWorkList->GetItemCount() ) {

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanEditItemProps(void)
{
	if( HasSelect() ) {

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			return pPrim->HasProps();
			}

		return FALSE;
		}

	return !m_fScratch;
	}

BOOL CPageEditorWnd::CanEditItemPos(void)
{
	if( HasLoneSelect() ) {

		CPrim *pPrim = GetLoneSelect();

		if( pPrim->IsLine() ) {

			// TODO -- We ought to allow line editing 

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::OnEditDelete(CString Verb)
{
	if( HasSelect() ) {

		if( HasZoomPages() ) {

			if( HasLoneSelect() ) {

				OnOrganizeClearBinding();
				}
			else {
				CString Text = CString(IDS_BOUND_WIDGETS);

				Error(Text);

				return;
				}
			}

		IDataObject *pData = NULL;

		MakeDataObject(pData, FALSE, FALSE);

		CString Pos  = m_System.GetNavPos();

		CCmd *  pCmd = New CCmdDelete(Verb, Pos, pData);

		LocalExecCmd(pCmd);
		}
	}

void CPageEditorWnd::OnEditCut(void)
{
	if( OnEditCopy(TRUE) ) {

		OnEditDelete(CString(IDS_CUT));
		}
	}

BOOL CPageEditorWnd::OnEditCopy(BOOL fCut)
{
	if( HasSelect() ) {

		IDataObject *pData = NULL;

		if( !fCut ) {
			
			if( HasZoomPages() ) {

				CString Text = CString(IDS_CANNOT_COPY_BOUND);

				Error(Text);

				return FALSE;
				}
			}

		if( MakeDataObject(pData, TRUE, FALSE) ) {

			OleSetClipboard(pData);
			
			OleFlushClipboard();

			pData->Release();

			return TRUE;
			}
		}

	return FALSE;
	}

void CPageEditorWnd::OnEditPaste(CString Verb, UINT uMode)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		UINT uType;

		if( CanAcceptDataObject(pData, uType) ) {

			CCmd *pCmd = New CCmdPaste(Verb, pData, uMode);

			LocalExecCmd(pCmd);

			UpdateWindow();

			return;
			}

		pData->Release();
		}
	}

void CPageEditorWnd::OnEditPasteSpecial(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		FORMATETC Fmt = { m_cfSpec, NULL, 1, -1, TYMED_HGLOBAL };

		STGMEDIUM Med = { 0, NULL, NULL };

		if( pData->GetData(&Fmt, &Med) == S_OK ) {

			HGLOBAL hText = Med.hGlobal;

			CString Text  = PCTXT(GlobalLock(hText));

			GlobalUnlock(Med.hGlobal);

			ReleaseStgMedium(&Med);

			CStringArray Props;

			Text.Tokenize(Props, '\r');

			CPrimPasteSpecialDialog Dlg;

			if( Dlg.Execute(ThisObject) ) {

				UINT uID = Dlg.GetMode() + IDM_EDIT_COPY_FORMAT;

				OnEditCopyProps(NULL, Props, CString(IDS_CLIPBOARD), uID);
				}
			}

		pData->Release();
		}
	}

void CPageEditorWnd::OnEditDuplicate(void)
{
	if( OnEditCopy(FALSE) ) {

		OnEditPaste(CString(IDS_DUPLICATE), pasteDup);
		}
	}

void CPageEditorWnd::OnEditSelectAll(void)
{
	ClearSelect(FALSE);

	INDEX n = m_pWorkList->GetHead();

	while( !m_pWorkList->Failed(n) ) {

		AddSelect(n, TRUE);

		m_pWorkList->GetNext(n);
		}

	AddSelect(NULL, FALSE);
	}

void CPageEditorWnd::OnEditCopySizeInit(void)
{
	SetMode(modePickAny);

	m_uPick = IDM_EDIT_COPY_SIZE;
	}

void CPageEditorWnd::OnEditCopySizeDone(void)
{
	CString Save = m_System.GetNavPos();

	CString Verb = IDS_COPY_SIZE_TO;

	SaveMultiCmd(Save, Verb);

	CSize NewSize = m_pPick->GetRect().GetSize();

	UINT  uCount  = m_SelList.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		INDEX  Index = m_SelList[n];

		CPrim *pPrim = m_pWorkList->GetItem(Index);

		if( pPrim != m_pPick ) {

			CRect  OldRect  = pPrim->GetRect();

			CPoint OldTop   = OldRect.GetTopLeft();

			CPoint OldPos   = OldRect.GetCenter();
			
			CSize  OldSize  = OldRect.GetSize();

			if( NewSize != OldSize ) {

				CPoint NewPos  = OldPos - NewSize / 2;

				CRect  IntRect = CRect(OldTop, NewSize);

				CRect  NewRect = CRect(NewPos, NewSize);

				ClipMoveRect(NewRect);

				CString Item  = pPrim->GetFixedPath();

				pPrim->SetRect(OldRect, IntRect);

				pPrim->SetRect(IntRect, NewRect);

				if( !m_fScratch ) {

					CCmd *pCmd1 = New CCmdSize(Item, OldRect, IntRect);

					CCmd *pCmd2 = New CCmdSize(Item, IntRect, NewRect);

					LocalSaveCmd(pCmd1);

					LocalSaveCmd(pCmd2);
					}
				}
			}
		}

	SaveMultiCmd(Save, Verb);

	UpdateAll();

	m_pItem->SetDirty();
	}

void CPageEditorWnd::OnEditCopyPropsInit(UINT uID)
{
	SetMode(modePickAny);

	m_uPick = uID;
	}

void CPageEditorWnd::OnEditCopyPropsDone(void)
{
	CString What = L" ";

	CStringArray Props;

	CUIGetSet().GetProps(Props, m_pPick);

	CString Name = m_pPick->FindMetaList()->GetName();

	OnEditCopyProps(m_pPick, Props, Name, m_uPick);
	}

void CPageEditorWnd::OnEditCopyProps(CPrim *pPick, CStringArray &Props, CString Name, UINT uID)
{
	CString What = L" ";

	if( uID == IDM_EDIT_COPY_SELECT ) {

		CUIGetSetDialog Dlg(Name, Props);

		Dlg.SetLastBuffer(m_LastCopy);

		if( !Dlg.Execute(ThisObject) ) {

			return;
			}

		FlipTextAndData(&Props);
		}
	else {
		switch( uID ) {

			case IDM_EDIT_COPY_FORMAT:

				StripMatch( &Props,
					    L"TextItem\nText\n",
					    L"DataItem\nValue\n",
					    L"Action\n",
					    NULL
					    );
				
				What = CString(IDS_ALL_FORMATTING);

				break;
			
			case IDM_EDIT_COPY_FONT:

				StripOther( &Props,
					    L"TextItem\nFont",
					    L"TextItem\nAlignH",
					    L"TextItem\nAlignV",
					    L"TextItem\n/Margin/x1",
					    L"TextItem\n/Margin/y1",
					    L"TextItem\n/Margin/x2",
					    L"TextItem\n/Margin/y2",
					    L"TextItem\nLead",
					    L"DataItem\nFont",
					    L"DataItem\nAlignH",
					    L"DataItem\nAlignV",
					    L"DataItem\n/Margin/x1",
					    L"DataItem\n/Margin/y1",
					    L"DataItem\n/Margin/x2",
					    L"DataItem\n/Margin/y2",
					    NULL
					    );

				What = CString(IDS_TEXT_FORMAT);

				break;
			
			case IDM_EDIT_COPY_FIGURE:

				StripOther( &Props,
					    L"Fill\n",
					    L"Line\n",
					    L"Edge\n",
					    L"Face\n",
					    L"Trim\n",
					    L"Core\nColor1\n",
					    L"Core\nColor2\n",
					    L"Core\nHilite\n",
					    L"Core\nShadow\n",
					    L"Core\nBorder\n",
					    L"Core\nType\n",
					    NULL
					    );

				What = CString(IDS_FILLS_AND_EDGES);

				break;
			
			case IDM_EDIT_COPY_ACTION:

				StripOther( &Props,
					    L"Action\n",
					    NULL
					    );

				What = CString(IDS_ACTION_2);

				break;
			}

		FlipTextAndData(&Props);
		}

	CString Save = m_System.GetNavPos();

	CString Mode = pPick ? CString(IDS_COPY) : CString(IDS_PASTE);

	CString Verb = CPrintf(IDS_FMT, Mode, What);

	SaveMultiCmd(Save, Verb);

	BOOL fText   = HasProp(Props, L"TextItem\nText\n");

	BOOL fData   = HasProp(Props, L"DataItem\nValue\n");

	BOOL fAction = HasProp(Props, L"Action\nMode\n");

	UINT uCount  = m_SelList.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		INDEX  Index = m_SelList[n];

		CPrim *pPrim = m_pWorkList->GetItem(Index);

		if( pPrim != pPick ) {

			HGLOBAL hPrev = pPrim->TakeSnapshot();

			if( fText || fData || fAction ) {

				if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

					CPrimWithText *pHost = (CPrimWithText *) pPrim;

					if( fText || fData ) {

						if( !pHost->m_pTextItem && !pHost->m_pDataItem ) {

							if( fText ) {

								pHost->AddText();
								}
							else
								pHost->AddData();
							}
						}

					if( fAction ) {

						if( !pHost->m_pAction ) {

							pHost->AddAction();
							}
						}
					}
				}

			CUIGetSet().SetProps(pPrim, Props);

			pPrim->UpdateLayout();

			if( !m_fScratch ) {

				CCmdItem *pCmd = New CCmdItem(L"", pPrim, hPrev);

				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else
					LocalSaveCmd(pCmd);
				}
			}
		}

	SaveMultiCmd(Save, Verb);

	BuildHandles();

	UpdateImage();

	Invalidate(FALSE);

	m_pItem->SetDirty();
	}

BOOL CPageEditorWnd::OnEditSummary(void)
{
	return TRUE;
	}

BOOL CPageEditorWnd::OnEditItemProps(void)
{
	return OnEditItemProps(L"", NULL);
	}

BOOL CPageEditorWnd::OnEditItemProps(CString Tab)
{
	return OnEditItemProps(Tab, NULL);
	}

BOOL CPageEditorWnd::OnEditItemProps(CString Tab, HGLOBAL hItem)
{
	// REV3 -- The item dialog mechanism is very
	// expensive for groups as it saves all the child
	// primitives in case of a cancel. This is in why
	// we have to clear the hover here as otherwise
	// we end up with a dangling pointer on a cancel.

	m_fHover = FALSE;

	ClearHover();

	INDEX   Index = m_SelList[0];

	CPrim * pPrim = m_pWorkList->GetItem(Index);

	HGLOBAL hPrev = hItem ? hItem : pPrim->TakeSnapshot();
	
	COLOR   Back  = FindBackColor();

	CString Cat   = pPrim->HasImages() ? L"Symbols" : L"Tags";

	CPrimDialog Dlg(pPrim, Back, Tab);

	if( ArePropsLocked() ) {

		Dlg.ForceReadOnly();
		}

	if( m_System.ExecSemiModal(Dlg, Cat) ) {

		if( pPrim->IsWidget() ) {

			CPrimWidget *pWidget = (CPrimWidget *) pPrim;

			RecompilePrimList(pWidget->m_pList);
			}

		if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

			CPrimWithText *pHost = (CPrimWithText *) pPrim;

			if( pHost->m_pTextItem ) {

				if( pHost->m_pTextItem->IsEmpty() ) {

					pHost->RemText();
					}
				else
					pHost->m_pTextItem->TextDirty();
				}

			if( pHost->GetActionMode() == actOptional ) {

				if( pHost->IsActionNull() ) {

					pHost->RemAction();
					}

				if( pHost->m_pDataItem ) {

					if( pHost->m_pDataItem->m_Entry ) {

						pHost->RemAction();
						}
					}
				}
			}

		CString    Menu = CFormat(CString(IDS_EDIT_FMT), Dlg.GetCaption());

		CCmdItem * pCmd = New CCmdItem(Menu, pPrim, hPrev);

		if( pCmd->IsNull() ) {

			delete pCmd;
			}
		else {
			LocalSaveCmd(pCmd);

			pPrim->SetDirty();
			}

		pPrim->UpdateLayout();

		ClearHover();

		m_fHover = TRUE;

		UpdateAll();

		return TRUE;
		}

	GlobalFree(hPrev);

	ClearHover();

	m_fHover = TRUE;

	return FALSE;
	}

BOOL CPageEditorWnd::OnEditPageProps(void)
{
	HGLOBAL hPrev = m_pProps->TakeSnapshot();

	CItemDialog Dlg(m_pProps, CString(IDS_PAGE_PROPERTIES));

	if( m_System.ExecSemiModal(Dlg, L"") ) {

		CString    Menu = CString(IDS_EDIT_PAGE);

		CCmdItem * pCmd = New CCmdItem(Menu, m_pProps, hPrev);

		if( pCmd->IsNull() ) {

			delete pCmd;
			}
		else {
			LocalSaveCmd(pCmd);

			m_pProps->SetDirty();
			}

		UpdateImage();

		Invalidate(FALSE);

		return TRUE;
		}

	GlobalFree(hPrev);

	return FALSE;
	}

BOOL CPageEditorWnd::OnEditItemPos(void)
{
	INDEX   Index = m_SelList[0];

	CPrim * pPrim = m_pWorkList->GetItem(Index);

	HGLOBAL hPrev = pPrim->TakeSnapshot();

	BOOL    fRead = ArePropsLocked();

	CSize   Min   = pPrim->GetMinSize(m_pGDI);

	CRect   Work  = m_WorkRect;

	CPrimPositionDialog Dlg(pPrim, fRead, Min, Work);

	if( Dlg.Execute(ThisObject) ) {

		CString    Menu = CFormat(CString(IDS_EDIT_FMT), Dlg.GetCaption());

		CCmdItem * pCmd = New CCmdItem(Menu, pPrim, hPrev);

		if( pCmd->IsNull() ) {

			delete pCmd;
			}
		else {
			LocalSaveCmd(pCmd);

			pPrim->SetDirty();
			}

		UpdateAll();

		return TRUE;
		}

	GlobalFree(hPrev);

	ClearHover();

	return FALSE;
	}

BOOL CPageEditorWnd::OnEditFind(void)
{
	// REV3 -- Use a "Find" dialog to gather options for CString::Search().

	CStringDialog Dlg;

	Dlg.SetCaption(CString(IDS_FIND_TEXT_ON_PAGE));

	Dlg.SetGroup(CString(IDS_FIND));

	Dlg.SetData(m_LastFind);

	if( Dlg.Execute() ) {

		CString Find = Dlg.GetData();

		CStringArray List;

		CCodedItem *pCode = m_pSystem->m_pHeadCoded;

		while( pCode ) {

			CItem *pScan = pCode->GetParent();

			while( pScan ) {

				if( pScan == m_pList ) {

					CString Text = pCode->GetSource(TRUE);

					if( Text.Search(Find, searchContains) < NOTHING ) {

						CPrim * pPrim = (CPrim *) pCode->GetParent(AfxRuntimeClass(CPrim));

						CString Path  = pPrim->GetFixedPath();

						CString Name  = pPrim->GetHumanPath();

						List.Append(Name + L"\n" + Path);
						}

					break;
					}

				pScan = pScan->GetParent();
				}

			pCode = pCode->m_pNext;
			}

		if( List.IsEmpty() ) {

			CString Text = IDS_TEXT_NOT_FOUND;

			CWnd::GetActiveWindow().Error(Text);

			return FALSE;
			}

		m_System.SetFindList(CString(IDS_FIND_TEXT_ON_PAGE), List, TRUE);

		m_LastFind = Find;

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::OnEditCopyPage(BOOL fAll)
{
	if( !m_fScratch ) {

		CClientDC RC(ThisObject);

		CMemoryDC DC(RC);

		CSize  Disp = m_DispSize;

		CSize  Size = Disp;

		CPoint Pos  = fAll ? m_FrameRect.GetTopLeft() : CPoint(0, 0);

		if( fAll ) {

			Size.cx += m_FrameRect.left;

			Size.cx += m_FrameRect.right;
			
			Size.cy += m_FrameRect.top;
			
			Size.cy += m_FrameRect.bottom;
			}

		CBitmap Bitmap(RC, Size);

		DC.Select(Bitmap);

		DC.FillRect(Size, afxBrush(WHITE));

		if( fAll ) {

			m_pFramer->DrawFrame(DC, FALSE, FALSE);

			for( UINT n = 0; n < m_Keys.GetCount(); n++ ) {

				CKey const &Key = m_Keys[n];

				DrawKey(DC, Key.m_Rect, Key.m_uCode);
				}
			}

		DC.BitBlt(CRect(Pos, Disp), CDC::FromHandle(m_pWin->GetDC()), CPoint(0, 0), SRCCOPY);

		DC.Deselect();

		OpenClipboard(m_hWnd);

		EmptyClipboard();

		SetClipboardData(CF_BITMAP, Bitmap.GetHandle());

		CloseClipboard();
		}
	}

void CPageEditorWnd::OnEditWebFrame(void)
{
	#ifdef _DEBUG

	if( m_pFramer ) {

		CClientDC MainDC(ThisObject);

		CMemoryDC WorkDC(MainDC);

		CBitmap   Bitmap(MainDC, m_ViewExt);

		WorkDC.Select(Bitmap);

		WorkDC.FillRect(m_ViewExt, afxBrush(WHITE));

		WorkDC.SetMapMode(MM_ANISOTROPIC);

		WorkDC.SetWindowOrg  (0, 0);

		WorkDC.SetWindowExt  (m_TotalSize);
					
		WorkDC.SetViewportOrg(0, 0);

		WorkDC.SetViewportExt(m_ViewExt);

		DrawFrame(WorkDC);

		////////

		UINT             uSize = m_ViewExt.cx * m_ViewExt.cy * 4;

		BYTE             *pData = new BYTE [ uSize ];

		BITMAPINFO       *pInfo = new BITMAPINFO;

		BITMAPFILEHEADER *pHead = new BITMAPFILEHEADER;

		////////

		memset(pHead, 0, sizeof(BITMAPFILEHEADER));

		pHead->bfType    = 'MB';
		pHead->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO);
		pHead->bfSize    = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO) + uSize;

		////////

		memset(pInfo, 0, sizeof(BITMAPINFO));

		pInfo->bmiHeader.biSize		 = sizeof(BITMAPINFO);
		pInfo->bmiHeader.biWidth	 = m_ViewExt.cx;
		pInfo->bmiHeader.biHeight	 = m_ViewExt.cy;
		pInfo->bmiHeader.biPlanes	 = 1;
		pInfo->bmiHeader.biBitCount	 = 32;
		pInfo->bmiHeader.biCompression	 = BI_RGB;
		pInfo->bmiHeader.biSizeImage	 = 0;
		pInfo->bmiHeader.biXPelsPerMeter = 0;
		pInfo->bmiHeader.biYPelsPerMeter = 0;
		pInfo->bmiHeader.biClrUsed	 = 0;
		pInfo->bmiHeader.biClrImportant	 = 0;

		////////

		GetDIBits( WorkDC,
			   Bitmap,
			   0,
			   m_ViewExt.cy,
			   pData,
			   pInfo,
			   DIB_RGB_COLORS
			   );

		////////

		FILE *pBits = fopen("c:\\temp\\full-x1.bmp", "wb");

		if( pBits ) {

			fwrite(pHead, sizeof(*pHead), 1, pBits);
		
			fwrite(pInfo, sizeof(*pInfo), 1, pBits);

			fwrite(pData, uSize, 1, pBits);

			fclose(pBits);
			}

		////////

		FILE *pJson = fopen("c:\\temp\\full-x1.json", "wt");

		if( pJson ) {

			fprintf(pJson, "{\n");
			fprintf(pJson, "\t\"valid\": 1,\n");
			fprintf(pJson, "\t\"file\": \"/assets/png/full-x1.png\",\n");
			fprintf(pJson, "\t\"bits\": 16,\n");
			fprintf(pJson, "\t\"scale\": 1,\n");
			fprintf(pJson, "\t\"cxFrame\": %d,\n", m_ViewExt.cx);
			fprintf(pJson, "\t\"cyFrame\": %d,\n", m_ViewExt.cy);
			fprintf(pJson, "\t\"xpImage\": %d,\n", m_nScale * m_FrameRect.left);
			fprintf(pJson, "\t\"ypImage\": %d,\n", m_nScale * m_FrameRect.top);
			fprintf(pJson, "\t\"cxImage\": %d,\n", m_DispSize.cx);
			fprintf(pJson, "\t\"cyImage\": %d,\n", m_DispSize.cy);
			fprintf(pJson, "\t\"keys\": [\n");

			for( UINT n = 0; n < m_Keys.GetCount(); n++ ) {

				CKey const &Key = m_Keys[n];

				fprintf( pJson,
					 "\t\t{ \"x1\": %d, \"y1\": %d, \"x2\": %d, \"y2\": %d, \"vk\": %d }",
					 m_nScale * Key.m_Rect.left,
					 m_nScale * Key.m_Rect.top,
					 m_nScale * Key.m_Rect.right,
					 m_nScale * Key.m_Rect.bottom,
					 Key.m_uCode
					 );

				if( n < m_Keys.GetCount() - 1 ) {

					fprintf(pJson, ",\n");
					}
				else
					fprintf(pJson, "\n");
				}

			fprintf(pJson, "\t\t]\n");
			fprintf(pJson, "\t}\n");

			fclose(pJson);
			}

		////////

		delete[] pData;

		delete pHead;

		delete pInfo;

		WorkDC.Deselect();
		}

	#endif
	}

// Property Manipulation

BOOL CPageEditorWnd::HasProp(CStringArray &Props, PCTXT pProp)
{
	UINT uProp = wstrlen(pProp);

	for( UINT n = 0; n < Props.GetCount(); n++ ) {
	
		PCTXT s = Props.GetAt(n);

		if( *s ) {

			if( !wstrncmp(s, pProp, uProp) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CPageEditorWnd::StripMatch(CStringArray *pProps, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pProps);

	PCTXT *pList = (PCTXT *) alloca(sizeof(PCTXT) * 32);

	UINT  *pSize = (UINT  *) alloca(sizeof(UINT ) * 32);

	UINT   a;

	for( a = 0; a < 32; a++ ) {

		if( !(pList[a] = va_arg(pArgs, PCTXT)) ) {

			break;
			}

		pSize[a] = wstrlen(pList[a]);
		}

	for( UINT n = 0; n < pProps->GetCount(); n++ ) {
	
		PCTXT s = pProps->GetAt(n);

		if( *s ) {

			for( UINT i = 0; i < a; i++ ) {

				if( !wstrncmp(s, pList[i], pSize[i]) ) {

					pProps->Remove(n);

					n--;

					break;
					}
				}
			}
		}

	va_end(pArgs);
	}

void CPageEditorWnd::StripOther(CStringArray *pProps, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pProps);

	PCTXT *pList = (PCTXT *) alloca(sizeof(PCTXT) * 32);

	UINT  *pSize = (UINT  *) alloca(sizeof(UINT ) * 32);

	UINT   a;

	for( a = 0; a < 32; a++ ) {

		if( !(pList[a] = va_arg(pArgs, PCTXT)) ) {

			break;
			}

		pSize[a] = wstrlen(pList[a]);
		}

	for( UINT n = 0; n < pProps->GetCount(); n++ ) {
	
		PCTXT s = pProps->GetAt(n);

		if( *s ) {

			UINT i;

			for( i = 0; i < a; i++ ) {

				if( !wstrncmp(s, pList[i], pSize[i]) ) {

					break;
					}
				}

			if( i == a ) {

				pProps->Remove(n);

				n--;
				}
			}
		}

	va_end(pArgs);
	}

void CPageEditorWnd::FlipTextAndData(CStringArray *pProps)
{
	UINT  c  = pProps->GetCount();

	PCTXT p1 = L"TextItem\n";

	PCTXT p2 = L"DataItem\n";

	UINT  n1 = wstrlen(p1);

	UINT  n2 = wstrlen(p2);

	for( UINT n = 0; n < c; n++ ) {

		PCTXT s = pProps->GetAt(n);

		if( *s ) {

			if( !wstrncmp(s, p1, n1) ) {

				CString a = p2 + CString(s + n1);

				pProps->Append(a);
				}

			if( !wstrncmp(s, p2, n2) ) {

				CString a = p1 + CString(s + n2);

				pProps->Append(a);
				}
			}
		}
	}

// Default Colors

COLOR CPageEditorWnd::FindBackColor(void)
{
	if( m_pProps->m_pMaster ) {

		UINT   hItem = m_pProps->m_pMaster->GetObjectRef();

		CItem *pItem = m_pItem->GetDatabase()->GetHandleItem(hItem);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

				CDispPage *pPage = (CDispPage *) pItem;

				return pPage->m_pProps->m_pBack->GetColor();
				}
			}
		}

	return m_pProps->m_pBack->GetColor();
	}

COLOR CPageEditorWnd::FindTextColor(void)
{
	COLOR c = FindBackColor();

	BYTE  r = GetRED  (c);
	BYTE  g = GetGREEN(c);
	BYTE  b = GetBLUE (c);

	BYTE  i = BYTE((14*r + 45*g + 5*b) / 64);

	if( i >= 15 ) {

		return GetRGB(0,0,0);
		}

	return GetRGB(31,31,31);
	}

// Compilation

BOOL CPageEditorWnd::RecompileSelList(void)
{
	UINT  uCount = m_SelList.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		INDEX  Index = m_SelList[n];

		CPrim *pPrim = m_pWorkList->GetItem(Index);

		RecompilePrim(pPrim);
		}

	return TRUE;
	}

BOOL CPageEditorWnd::RecompilePrimList(CPrimList *pList)
{
	INDEX n = pList->GetHead();

	while( !pList->Failed(n) ) {

		CPrim *pPrim = pList->GetItem(n);

		RecompilePrim(pPrim);

		pList->GetNext(n);
		}

	return TRUE;
	}

BOOL CPageEditorWnd::RecompilePrim(CPrim *pPrim)
{
	RecompileFrom(pPrim);

	if( pPrim->IsSet() ) {

		if( pPrim->IsWidget() ) {

			CPrimWidget *pWidget = (CPrimWidget *) pPrim;

			pWidget->m_pData->Recompile();
			}

		CPrimSet *pSet = (CPrimSet *) pPrim;

		RecompilePrimList(pSet->m_pList);
		}

	return TRUE;
	}

BOOL CPageEditorWnd::RecompileFrom(CMetaItem *pItem)
{
	CMetaList *pList = pItem->FindMetaList();

	UINT      uCount = pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		UINT             uType  = pMeta->GetType();

		if( uType == metaObject || uType == metaVirtual ) {

			CItem *pChild = pMeta->GetObject(pItem);

			if( pChild ) {

				if( pChild->IsKindOf(AfxRuntimeClass(CCodedItem)) ) {

					CCodedItem *pCoded = (CCodedItem *) pChild;

					pCoded->Recompile(TRUE);

					continue;
					}

				if( pChild->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

					CMetaItem *pMeta = (CMetaItem *) pChild;

					RecompileFrom(pMeta);

					continue;
					}
				}
			}

		if( uType == metaCollect ) {

			CItemList *pList = (CItemList *) pMeta->GetObject(pItem);

			if( pList->IsKindOf(AfxRuntimeClass(CPrimList)) ) {

				CPrimList *pPrimList = (CPrimList *) pList;

				RecompilePrimList(pPrimList);
				}
			else {
				INDEX Index = pList->GetHead();

				while( !pList->Failed(Index) ) {

					CItem *pChild = pList->GetItem(Index);

					if( pChild->IsKindOf(AfxRuntimeClass(CMetaItem)) ) {

						CMetaItem *pMeta = (CMetaItem *) pChild;

						RecompileFrom(pMeta);
						}

					pList->GetNext(Index);
					}
				}
			}
		}

	return TRUE;
	}

// End of File
