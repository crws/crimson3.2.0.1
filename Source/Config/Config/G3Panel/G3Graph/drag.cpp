
#include "intern.hpp"

#include "gdiplus.hpp"

#include "../G3Prims/ScratchPadWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// IUnknown

HRESULT CPageEditorWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropSource ) {

			*ppObject = (IDropSource *) this;

			return S_OK;
			}

		if( iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CPageEditorWnd::AddRef(void)
{
	return 1;
	}

ULONG CPageEditorWnd::Release(void)
{
	return 1;
	}

// IDropSource

HRESULT CPageEditorWnd::QueryContinueDrag(BOOL fEscape, DWORD dwKeys)
{
	if( !fEscape ) {

		if( dwKeys & MK_RBUTTON ) {

			return DRAGDROP_S_CANCEL;
			}

		if( dwKeys & MK_LBUTTON ) {

			return S_OK;
			}

		return DRAGDROP_S_DROP;
		}

	return DRAGDROP_S_CANCEL;
	}
        
HRESULT CPageEditorWnd::GiveFeedback(DWORD dwEffect)
{
	if( dwEffect == DROPEFFECT_MOVE ) {

		SetCursor(m_MoveCursor);
		
		return S_OK;
		}

	if( dwEffect == DROPEFFECT_COPY ) {

		SetCursor(m_CopyCursor);

		return S_OK;
		}

	SetCursor(LoadCursor(NULL, IDC_NO));

	return S_OK;
	}

// IDropTarget

HRESULT CPageEditorWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	InstallHook();

	if( m_fScratch ) {

		GetParent().SetActiveWindow();
		}
	else {
		afxMainWnd->SetActiveWindow();

		m_pScratch->DragIntoMain();
		}

	if( m_uMode == modeZoom || m_uMode == modeGrab ) {

		SetMode(modeSelect);
		}

	if( m_uMode == modeSelect ) {

		if( !IsListLocked() ) {

			if( CanAcceptDataObject(pData, m_uDropType) ) {

				AcceptDataObject(pData, TRUE);

				m_TrackRect = m_AcceptRect;

				m_fDrop     = TRUE;

				SetDropMove(IsDropMove(dwKeys, pEffect));

				DropUpdate();

				Invalidate(FALSE);

				return S_OK;
				}
			}
		}

	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CPageEditorWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	if( m_fDrop ) {

		SetDropMove(IsDropMove(dwKeys, pEffect));

		DropUpdate();

		return S_OK;
		}

	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CPageEditorWnd::DragLeave(void)
{
	if( m_fDrop ) {

		FreeAcceptList();

		SetDropMove(FALSE);

		Invalidate(FALSE);

		m_fDrop = FALSE;

		RemoveHook();

		return S_OK;
		}

	m_DropHelp.DragLeave();

	RemoveHook();

	return S_OK;
	}

HRESULT CPageEditorWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	if( m_fDrop ) {

		if( IsDropMove(dwKeys, pEffect) ) {

			if( m_uDropType == dropSamePage ) {

				int dx = m_TrackRect.left - m_SelRect.left;

				int dy = m_TrackRect.top  - m_SelRect.top;

				NudgeSelection(dx, dy, FALSE);

				FreeAcceptList();

				*pEffect = DROPEFFECT_COPY;
				}
			else {
				SaveMultiCmd(m_DragNav, CString(IDS_MOVE));

				CopyAcceptList(FALSE);

				FreeAcceptList();

				SaveDropCmd(L"");
				}
			}
		else {
			CopyAcceptList(FALSE);

			FreeAcceptList();

			if( m_uDropType == dropCreate ) {

				// LATER -- Set colors on accept?

				SetSelectionTextColor();

				SaveDropCmd(CString(IDS_CREATE));

				if( CheckCreateText() ) {

					m_fDropMove = FALSE;
					
					m_fDrop     = FALSE;

					RemoveHook();

					return S_OK;
					}
				}
			else {
				if( m_uDropType == dropOther ) {

					SetSelectionTextColor();
					}

				SaveDropCmd(CString(IDS_COPY));
				}
			}

		m_fDropMove = FALSE;
		
		m_fDrop     = FALSE;

		UpdateImage();

		Invalidate(FALSE);

		SetFocus();

		RemoveHook();

		return S_OK;
		}

	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	RemoveHook();
	
	return S_OK;
	}

// Drag Support

void CPageEditorWnd::DragItem(void)
{
	IDataObject *pData = NULL;

	if( MakeDataObject(pData, TRUE, FALSE) ) {

		FindDragMetrics();

		MakeDragImage();

		CDragHelper *pHelp = New CDragHelper;

		pHelp->AddImage(pData, m_DragImage, m_DragSize, m_DragOffset);

		delete pHelp;

		m_DragNav      = m_System.GetNavPos();

		DWORD dwAllow  = DROPEFFECT_MOVE | DROPEFFECT_COPY;

		DWORD dwResult = DROPEFFECT_NONE;

		if( m_fScratch ) {

			m_pScratch->DragFromScratch(TRUE);

			DoDragDrop(pData, this, dwAllow, &dwResult);

			m_pScratch->DragFromScratch(FALSE);
			}
		else
			DoDragDrop(pData, this, dwAllow, &dwResult);

		if( dwResult == DROPEFFECT_NONE ) {

			m_System.Navigate(m_DragNav);
			}
		else {
			CString Drop = m_System.GetNavPos();

			if( dwResult == DROPEFFECT_MOVE ) {

				m_System.Navigate(m_DragNav);

				OnEditDelete(CString(IDS_DELETE));

				SaveMultiCmd(Drop, CString(IDS_MOVE));
				}

			m_System.Navigate(Drop);
			}

		m_DragNav.Empty();

		pData->Release();
		}
	}

void CPageEditorWnd::FindDragMetrics(void)
{
	m_DragPos    = GetCursorPos();

	m_DragOffset = m_Pos - m_SelRect.GetTopLeft();

	m_DragSize   = m_SelRect.GetSize();

	if( m_DragSize.cx > m_DragSize.cy ) {
		
		if( m_DragSize.cx > 128 ) {

			m_DragSize.cy *= 128;

			m_DragSize.cy /= m_DragSize.cx;

			m_DragSize.cx  = 128;
			}
		}
	else {
		if( m_DragSize.cy > 128 ) {

			m_DragSize.cx *= 128;

			m_DragSize.cx /= m_DragSize.cy;

			m_DragSize.cy  = 128;
			}
		}

	m_DragSize.cx   += 4;

	m_DragSize.cy   += 4;

	m_DragOffset.cx *= m_DragSize.cx;

	m_DragOffset.cy *= m_DragSize.cy;

	m_DragOffset.cx /= m_SelRect.cx();

	m_DragOffset.cy /= m_SelRect.cy();
	}

void CPageEditorWnd::MakeDragImage(void)
{
	SaveImage();

	m_pGDI->ClearScreen(0);

	UpdateImage(m_pWorkList, 2);

	GpBitmap *pImage = NULL;

	m_pWin->ReleaseBitmap();

	GdipCreateBitmapFromHBITMAP( m_pWin->GetBitmap(),
				     NULL,
				     &pImage
				     );

	m_pWin->RestoreBitmap();

	RestoreImage();
	
	////////

	CMemoryDC DC(m_pMap->GetHandle());

	m_DragImage.Create(*m_pMap, m_DragSize);

	DC.FillRect(CRect(m_DragSize), afxBrush(BLACK));

	DC.Select(m_DragImage);

	////////

	GpGraphics *pGraph = NULL;

	GdipCreateFromHDC(DC.GetHandle(), &pGraph);

	GdipDrawImageRectRectI( pGraph,
				pImage,
				2,
				2,
				m_DragSize.cx - 4,
				m_DragSize.cy - 4,
				m_SelRect.left,
				m_SelRect.top,
				m_SelRect.cx(),
				m_SelRect.cy(),
				UnitPixel,
				NULL,
				NULL,
				NULL
				);

	GdipDeleteGraphics(pGraph);

	GdipDisposeImage(pImage);

	////////

	DC.Deselect();
	}

// Drop Support

BOOL CPageEditorWnd::IsDropMove(DWORD dwKeys, DWORD *pEffect)
{
	if( m_fDrop ) {

		if( m_uDropType == dropOther ) {

			*pEffect = DROPEFFECT_COPY;

			return FALSE;
			}

		if( m_uDropType == dropDifferent ) {

			if( HasZoomPages(m_pAcceptList) ) {

				*pEffect = DROPEFFECT_NONE;

				return FALSE;
				}
			}

		if( m_uDropType == dropSameDatabase ) {

			if( HasZoomPages(m_pAcceptList) ) {

				*pEffect = DROPEFFECT_MOVE;

				return TRUE;
				}

			if( !(dwKeys & MK_SHIFT) ) {

				*pEffect = DROPEFFECT_COPY;

				return FALSE;
				}

			*pEffect = DROPEFFECT_MOVE;

			return TRUE;
			}

		if( m_uDropType == dropSamePage ) {

			if( HasZoomPages(m_pAcceptList) ) {

				*pEffect = DROPEFFECT_MOVE;

				return TRUE;
				}

			if( !(dwKeys & MK_CONTROL) ) {

				*pEffect = DROPEFFECT_MOVE;

				return TRUE;
				}

			*pEffect = DROPEFFECT_COPY;

			return FALSE;
			}

		*pEffect = DROPEFFECT_COPY;

		return FALSE;
		}

	*pEffect = DROPEFFECT_NONE;

	return FALSE;
	}

void CPageEditorWnd::SetDropMove(BOOL fMove)
{
	if( m_fDropMove != fMove ) {

		m_fDropMove = fMove;

		UpdateImage();
		
		Invalidate(FALSE);
		}
	}

// Command Helpers

void CPageEditorWnd::LocalExecCmd(CCmd *pCmd)
{
	if( !m_fScratch ) {

		m_System.ExecCmd(pCmd);
		}
	else {
		ExecCmd(pCmd);

		delete pCmd;
		}
	}

void CPageEditorWnd::LocalSaveCmd(CCmd *pCmd)
{
	if( !m_fScratch ) {

		m_System.SaveCmd(pCmd);
		}
	else {
		delete pCmd;
		}
	}

void CPageEditorWnd::SaveMultiCmd(CString Nav, CString Verb)
{
	if( !m_fScratch ) {

		CString Menu;

		if( Nav.Find('-') < NOTHING ) {

			Menu = CFormat(CString(IDS_FMT_PRIMITIVES), Verb);
			}
		else
			Menu = CFormat(CString(IDS_FMT_PRIMITIVE), Verb);

		CCmd *pCmd = New CCmdMulti(Nav, Menu, navSeq);

		LocalSaveCmd(pCmd);
		}
	}

BOOL CPageEditorWnd::SaveDropCmd(CString Verb)
{
	if( !m_fScratch ) {

		IDataObject *pData;

		if( MakeDataObject(pData, FALSE, FALSE) ) {

			CCmdBase *pCmd = New CCmdPaste(Verb, pData, pasteDrop);

			pCmd->m_Item = m_System.GetNavPos();

			pCmd->MakeMenu(FALSE);

			LocalSaveCmd(pCmd);

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

// Mouse Helpers
			
BOOL CPageEditorWnd::DragStart(void)
{
	BOOL fMove = FALSE;

	if( IsPosLocked() ) {

		return FALSE;
		}

	if( m_SelRect.PtInRect(m_Pos) ) {

		fMove = TRUE;
		}

	if( m_uHandle < NOTHING ) {

		CHand const &Hand = m_Handles[m_uHandle];

		if( Hand.m_Code == handMid ) {

			fMove = TRUE;
			}
		}

	if( fMove ) {

		if( !m_pHover || IsSelected(m_pHover) ) {

			m_uCapture = captNone;

			m_fDrag    = TRUE;

			Invalidate(FALSE);

			KillGhost();

			DragItem();

			UpdatePos();

			DefaultUpdate();

			m_fDrag = FALSE;

			Invalidate(FALSE);
			}

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::DropUpdate(void)
{
	CPoint Mouse = GetClientPos();

	ScrollIntoView(Mouse, TRUE);

	if( UpdatePos(Mouse) ) {

		// LATER -- Perform hover check, and if we're over a group
		// for a certain period of time, select that as the working
		// group. Likewise, if we're outside the working group for
		// a certain period of time, climb the working group tree.

		CPoint Pos = m_Pos - m_AcceptPos + m_AcceptRect.GetTopLeft();

		if( m_uDropType == dropCreate ) {

			SnapToGrid(Pos, 1);
			}
		else
			SnapToGrid(Pos, 2);

		CRect Rect = CRect(Pos, m_AcceptRect.GetSize());

		ClipMoveRect(Rect);

		if( m_TrackRect != Rect ) {

			MoveAcceptList(m_TrackRect, Rect);

			m_TrackRect = Rect;

			ShowTrackStatus();

			Invalidate(FALSE);
			}
		}
	}

void CPageEditorWnd::DropDraw(void)
{
	SetAdopter();

	m_Error.Empty();

	UpdateImage(m_pAcceptList, 0);

	ClearAdopter();
	}

void CPageEditorWnd::DropDraw(CDC &DC)
{
	ShowLineUp(DC, !m_fDropMove);

	CRect Rect = PPtoDP(m_TrackRect);

	BlendItemRect(DC, Rect, afxColor(RED));
	}

// End of File
