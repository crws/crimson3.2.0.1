
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3graph.hpp"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Other Libraries
//

#pragma	comment(lib, "msimg32.lib")

//////////////////////////////////////////////////////////////////////////
//
// Helper Functions
//

#define MulDivRound(a, b, c) (((a)*(b)+(c)/2)/(c))

//////////////////////////////////////////////////////////////////////////
//
// Virtual Key Code
//

#define	COPS_VK_SOFT1		0x80
#define	COPS_VK_SOFT2		0x81
#define	COPS_VK_SOFT3		0x82
#define	COPS_VK_SOFT4		0x83
#define	COPS_VK_SOFT5		0x84
#define	COPS_VK_SOFT6		0x85
#define	COPS_VK_SOFT7		0x86
#define	COPS_VK_SOFT8		0x87
#define	COPS_VK_SOFT9		0x88

#define	COPS_VK_MENU		0xA2

//////////////////////////////////////////////////////////////////////////
//
// Editor Mode
//

enum
{
	modeSelect,
	modeGrab,
	modeZoom,
	modePickAny,
	modePickLoc,
	modeText,
	};

//////////////////////////////////////////////////////////////////////////
//
// Paste Modes
//

enum
{
	pastePaste,
	pasteDup,
	pasteDrop,
	pasteLegacy
};

//////////////////////////////////////////////////////////////////////////
//
// Drop Types
//

enum
{
	dropSamePage,
	dropSameDatabase,
	dropDifferent,
	dropCreate,
	dropOther,
	dropText,
	};

//////////////////////////////////////////////////////////////////////////
//
// Capture Code
//

enum
{
	captNone,
	captPend,
	captSelect,
	captCreate,
	captSize,
	captGrab,
	};

//////////////////////////////////////////////////////////////////////////
//
// Handle Codes
//

enum
{
	handNone,
	handNW,
	handW,
	handSW,
	handN,
	handMid,
	handS,
	handNE,
	handE,
	handSE,
	handLine1,
	handLine2,
	handItem
	};

// End of File

#endif
