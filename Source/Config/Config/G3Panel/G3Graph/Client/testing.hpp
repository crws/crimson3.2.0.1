
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP

#define	INCLUDE_TESTING_HPP
	
//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUIManager;
class CCommsManager;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestFrameWnd;
class CTestItem;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestApp(void);

		// Destructor
		~CTestApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

class CTestFrameWnd : public CMainWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestFrameWnd(void);

		// Destructor
		~CTestFrameWnd(void);

	protected:
		// Data Members
		CSystemWnd * m_pSystem;
		CDatabase  * m_pDbase;
		CAccelerator m_Accel;

           	// Interface Control
		void OnUpdateInterface(void);
		void OnCreateStatusBar(void);
		void OnUpdateStatusBar(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnShowUI(BOOL fShow);
           
		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Item
//

class CTestItem : public CCommsSystem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestItem(void);

	protected:
		// Data Members
		CMetaItem * m_pUI;

		// System Data
		void AddData(void);

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
