
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	CButtonBitmap *pCats16 = New CButtonBitmap( CBitmap(L"ToolCats16"),
						    CSize(16, 16),
						    12
						    );

	CButtonBitmap *pCats24 = New CButtonBitmap( CBitmap(L"ToolCats24"),
						    CSize(24, 24),
						    12
						    );

	afxButton->AppendBitmap(0x2000, pCats16);
	
	afxButton->AppendBitmap(0x2100, pCats24);

	AfxTouch(AfxRuntimeClass(CPageEditorWnd));
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestFrameWnd;
	
	CRect Rect = CRect(230, 200, 1100, 900);

	if( GetSystemMetrics(SM_CXSCREEN) == 1024 ) {

		Rect = CRect(100, 50, 970, 750);
		}

	pWnd->Create( L"Crimson 3.0 Graphics Editor Prototype",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect,
		      AfxNull(CWnd),
		      CMenu(L"HeadMenu"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());
	
	afxThread->SetStatusText(L"Hello World");

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	if( Msg.message == WM_MOUSEWHEEL ) {

		CPoint Pos = CPoint(Msg.lParam);

		Msg.hwnd = WindowFromPoint(Pos);
		}

	return CDialogViewWnd::IsModelessMessage(Msg);
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:
			
			Src.EnableItem(TRUE);
			
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestFrameWnd, CMainWnd);

// Constructor

CTestFrameWnd::CTestFrameWnd(void)
{
	m_Accel.Create(L"TestMenu");

	ShowEdge(FALSE);

	m_pDbase = New CDatabase(L"Test");

	m_pDbase->Init();
	}

// Destructor

CTestFrameWnd::~CTestFrameWnd(void)
{
	m_pDbase->Kill();

	delete m_pDbase;
	}

// Interface Control

void CTestFrameWnd::OnUpdateInterface(void)
{
	CMainWnd::OnUpdateInterface();
	}

void CTestFrameWnd::OnCreateStatusBar(void)
{
	m_pSB->AddGadget(New CTextGadget(200, L"Recompile", textRed));
	}

void CTestFrameWnd::OnUpdateStatusBar(void)
{
	m_pSB->GetGadget(200)->AdjustFlag(MF_DISABLED, !m_pDbase->GetRecomp());
	}

// Message Map

AfxMessageMap(CTestFrameWnd, CMainWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_SHOWUI)

	AfxDispatchControlType(IDM_FILE, OnCommandControl)
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)

	AfxMessageEnd(CTestFrameWnd)
	};

// Accelerators

BOOL CTestFrameWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CTestFrameWnd::OnPostCreate(void)
{
	m_pView   = m_pDbase->CreateView(viewSystem);

	m_pSystem = (CSystemWnd *) m_pView;

	m_pView->Attach(m_pDbase);

	afxMainWnd->PostMessage(WM_UPDATEUI);

	CMainWnd::OnPostCreate();
	}

void CTestFrameWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		m_pView->SendMessage( m_MsgCtx.Msg.message,
				      m_MsgCtx.Msg.wParam,
				      m_MsgCtx.Msg.lParam
				      );
		}

	CMainWnd::OnInitPopup(Menu, nIndex, fSystem);
	}

void CTestFrameWnd::OnShowUI(BOOL fShow)
{
	}

// Command Handlers

BOOL CTestFrameWnd::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_OPEN ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestFrameWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Item
//

// Dynamic Class

AfxImplementDynamicClass(CTestItem, CCommsSystem);

// Constructor

CTestItem::CTestItem(void)
{
	m_pUI = AfxNewObject(CMetaItem, AfxNamedClass(L"CUIManager"));
	}

// System Data

void CTestItem::AddData(void)
{
	CCommsSystem::AddData();

	CDatabase *pDbase = GetDatabase();

	pDbase->AddCategory(L"Display Pages", L"UI", 0x21000002, 0x20000002);
	}

// Meta Data

void CTestItem::AddMetaData(void)
{
	CCommsSystem::AddMetaData();

	Meta_AddObject(UI);
	}

// End of File
