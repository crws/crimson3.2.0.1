
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/arc/factory"
//

// Constructor

CFileDataBaseArcFactory::CFileDataBaseArcFactory(void) : CFileData("/base0/arc/factory")
{	
	m_pData	= m_Data;

	m_uSize = elements(m_Data);
	}

// Attributes

UINT CFileDataBaseArcFactory::GetBaseSN(void)
{
	UINT uPtr = 0x0010;

	return GetLong(uPtr);
	}

UINT CFileDataBaseArcFactory::GetBaseType(void)
{
	UINT uPtr = 0x0014;

	return GetWord(uPtr);
	}

UINT CFileDataBaseArcFactory::GetBaseRevision(void)
{
	UINT uPtr = 0x0016;

	return GetWord(uPtr);
	}

UINT CFileDataBaseArcFactory::GetModuleId(void)
{
	UINT uPtr = 0x001E;

	return GetWord(uPtr);
	}

void CFileDataBaseArcFactory::GetMacId(PBYTE pData)
{
	UINT   uPtr = 0x0020;

	PCBYTE pMac = GetData(uPtr, 6);

	memcpy(pData, pMac, 6);
	}

// End of File
