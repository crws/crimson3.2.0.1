
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

class CEt3CommsDriverPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEt3CommsDriverPage(CEt3CommsDriver *pDriver);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEt3CommsDriver * m_pDriver;
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsDriverPage, CUIStdPage);

// Constructor

CEt3CommsDriverPage::CEt3CommsDriverPage(CEt3CommsDriver *pDriver)
{
	m_Class = AfxPointerClass(pDriver);

	m_pDriver = pDriver;
	}

// Operations

BOOL CEt3CommsDriverPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsDriver, CEt3UIItem);

// Constructor

CEt3CommsDriver::CEt3CommsDriver(void)
{
	m_Ident		= protDisabled;

	m_Flavor	= 0;

	m_PassEnable	= 0;

	m_PassTimeout	= 150;

	//m_Name  = CString(IDS_DRIVER);
	}

// Attributes

UINT CEt3CommsDriver::GetIdent(void)
{
	return m_Ident;
	}

BOOL CEt3CommsDriver::IsModbus(void)
{
	switch( m_Ident ) {

		case protModbusMaster:
		case protRtuSlave:
		case protAscSlave:
			break;
		
		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEt3CommsDriver::IsSixnet(void)
{
	switch( m_Ident ) {

		case protSixnetMaster:
		case protSixnetSlave:
			break;
		
		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEt3CommsDriver::IsMaster(void)
{
	switch( m_Ident ) {

		case protModbusMaster:
		case protSixnetMaster:
			break;
		
		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEt3CommsDriver::IsSlave(void)
{
	switch( m_Ident ) {

		case protSixnetSlave:
		case protRtuSlave:
		case protAscSlave:
			break;
		
		default:
			return FALSE;
		}

	return TRUE;
	}

CString CEt3CommsDriver::GetShortName(void)
{
	switch( m_Ident ) {

		case protModbusMaster:	return CString(IDS_MODBUS_MASTER);
		case protSixnetMaster:	return CString(IDS_UNIVERSAL_MASTER);
		case protSixnetSlave:	return CString(IDS_UNIVERSAL_SLAVE);
		case protRtuSlave:	return CString(IDS_MODBUS_RTU_SLAVE);
		case protAscSlave:	return CString(IDS_MODBUS_ASCII);
		}

	return CString();
	}

// UI Creation

BOOL CEt3CommsDriver::OnLoadPages(CUIPageList *pList)
{
	if( IsMaster() ) {

		pList->Append(New CUIStdPage(AfxThisClass()));
		}

	return FALSE;
	}

// UI Update

void CEt3CommsDriver::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		CheckFlavor(pHost);

		DoEnables(pHost);
		}

	if( Tag == "PassEnable" ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

void CEt3CommsDriver::PrepareData(void)
{
	UINT uPort = 1;

	CFileDataBaseCfgComms &File = m_pSystem->m_CfgComms;

	CFileDataBaseCfgComms::CSerial &Port = File.m_Serial[uPort];

	Port.SetProt(BuildProt());

	if( m_Ident != protDisabled ) {

		Port.SetPass(BuildPass());
		}
	}

// Meta Data Creation

void CEt3CommsDriver::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Ident);
	Meta_AddInteger(Flavor);
	Meta_AddInteger(PassEnable);
	Meta_AddInteger(PassTimeout);

	Meta_SetName((IDS_DRIVER));
	}

// Config File Help

UINT CEt3CommsDriver::BuildPass(void)
{
	UINT  uData = m_PassTimeout & 0x7FFF;

	SetBit(uData, m_PassEnable == 1, 15);

	return uData;
	}

UINT CEt3CommsDriver::BuildProt(void)
{
	static const UINT uProt[] = { 0, 1, 0, 1 };

	switch( m_Ident ) {

		case protModbusMaster:
		case protSixnetMaster:  return uProt[m_Flavor];
		case protSixnetSlave:	return 0x80;
		case protRtuSlave:	return 0x81;
		case protAscSlave:	return 0x82;
		}

	return 254;
	}

// Implementation

void CEt3CommsDriver::CheckFlavor(IUIHost *pHost)
{
	UINT uMask = IsModbus() ? 0x0C : 0x03;

	LimitEnum(pHost, L"Flavor", m_Flavor, uMask);
	}

void CEt3CommsDriver::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_PassEnable == 1;

	pHost->EnableUI("PassTimeout",	   fEnable);
	}

// End of File

