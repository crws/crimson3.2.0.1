
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/comms"
//

// Constructor

CFileDataBaseCfgComms::CFileDataBaseCfgComms(void) : CFileData("/base0/cfg/comms")
{	
	m_uSize = 1596;

	m_pData	= New BYTE [ m_uSize ];

	memset(m_pData, 0, m_uSize);

	m_Ethernet[0].SetBase(0x0100);
	m_Ethernet[0].SetFile(this);

	m_Ethernet[1].SetBase(0x0200);
	m_Ethernet[1].SetFile(this);
	
	m_Ethernet[2].SetBase(0x0300);
	m_Ethernet[2].SetFile(this);
	
	m_Ethernet[3].SetBase(0x0400);
	m_Ethernet[3].SetFile(this);
	
	m_Ethernet[4].SetBase(0x0500);
	m_Ethernet[4].SetFile(this);
	
	m_Serial[0]  .SetBase(0x0600);
	m_Serial[0]  .SetFile(this);
	
	m_Serial[1]  .SetBase(0x0610);
	m_Serial[1]  .SetFile(this);
	
	m_Serial[2]  .SetBase(0x0620);
	m_Serial[2]  .SetFile(this);
	
	m_Serial[3]  .SetBase(0x0630);
	m_Serial[3]  .SetFile(this);
	}

// Destructor

CFileDataBaseCfgComms::~CFileDataBaseCfgComms(void)
{
	if( m_pData ) {

		delete [] m_pData;

		m_pData = NULL;

		m_uSize = 0;
		}
	}

// Development

void CFileDataBaseCfgComms::Dump(void)
{
	AfxTrace(L"===============\n");

	AfxTrace(L"Properties for file %s\n", CString(GetFile()));

	AfxTrace(L"\tsixnet port number\t%d\n", GetSixnetPortNumber());
	AfxTrace(L"\tmodbus port number\t%d\n", GetModbusPortNumber());
	AfxTrace(L"\tconnection timeout\t%d\n", GetNetConnTimeout());
	AfxTrace(L"\tmode jumper\t%d\n",	GetNetModeJumper());
	AfxTrace(L"\tdisplay led\t%d\n",	GetDisplayLed());
	AfxTrace(L"\tQoS config\t%8.8X\n",	GetQosConfig());
	AfxTrace(L"\tring config\t%8.8X\n",	GetRingConfig());

	AfxTrace(L"web server\n");
	AfxTrace(L"\t-port\t%d\n",	 GetWebPort());
	AfxTrace(L"\t-options\t%2.2X\n", GetWebOptions());
	
	for( UINT n = 0; n < elements(m_Serial); n ++ ) {

		AfxTrace(L"serial %d\n", n);
		AfxTrace(L"\t-baud: %d\n", m_Serial[n].GetBaud());
		AfxTrace(L"\t-mode: %d\n", m_Serial[n].GetMode());
		}

	for( UINT m = 0; m < elements(m_Ethernet); m ++ ) {

		CEthernet &Net = m_Ethernet[m];

		AfxTrace(L"ethernet %d\n", m);
		AfxTrace(L"\tsettings\t%4.4X\n", Net.GetSettings());
		AfxTrace(L"\toptions\t%2.2X\n",  Net.GetOptions()[0]);
		AfxTrace(L"\t-addr:\t%s\n",      FormatAddr(Net.GetAddr()));
		AfxTrace(L"\t-mask:\t%s\n",      FormatAddr(Net.GetMask()));
		AfxTrace(L"\t-gate:\t%s\n",      FormatAddr(Net.GetGate()));
		
		AfxTrace(L"==\n");
		}

	AfxTrace(L"===============\n");
	}

// Attributes

UINT CFileDataBaseCfgComms::GetSixnetPortNumber(void)
{
	UINT uPtr = 0x10;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::GetModbusPortNumber(void)
{
	UINT uPtr = 0x12;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::GetNetConnTimeout(void)
{
	UINT uPtr = 0x0014;

	return GetLong(uPtr);
	}

UINT CFileDataBaseCfgComms::GetNetModeJumper(void)
{
	UINT uPtr = 0x0018;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::GetDisplayLed(void)
{
	UINT uPtr = 0x001A;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::GetQosConfig(void)
{
	UINT uPtr = 0x001C;

	return GetLong(uPtr);
	}

PCBYTE CFileDataBaseCfgComms::GetRingConfig(void)
{
	UINT uPtr = 0x0020;

	return GetData(uPtr, 16);
	}

UINT CFileDataBaseCfgComms::GetWebPort(void)
{
	UINT uPtr = 0x0030;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::GetWebOptions(void)
{
	UINT uPtr = 0x0032;

	return GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgComms::SetSixnetPortNumber(UINT uData)
{
	UINT uPtr = 0x0010;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::SetModbusPortNumber(UINT uData)
{
	UINT uPtr = 0x0012;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::SetNetConnTimeout(UINT uData)
{
	UINT uPtr = 0x0014;

	PutLong(uPtr, DWORD(uData));
	}

void CFileDataBaseCfgComms::SetNetModeJumper(UINT uData)
{
	UINT uPtr = 0x0018;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::SetDisplayLed(UINT uData)
{
	UINT uPtr = 0x001A;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::SetQosConfig(UINT uData)
{
	UINT uPtr = 0x001C;

	PutLong(uPtr, DWORD(uData));
	}

void CFileDataBaseCfgComms::SetRingConfig(PCBYTE pData)
{
	UINT uPtr = 0x0020;

	PutData(uPtr, pData, 16);
	}

void CFileDataBaseCfgComms::SetWebPort(UINT uData)
{
	UINT uPtr = 0x0030;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::SetWebOptions(UINT uData)
{
	UINT uPtr = 0x0032;

	PutByte(uPtr, BYTE(uData));
	}

// Ethernet Attributes

void CFileDataBaseCfgComms::CEthernet::SetFile(IFileData *pData)
{
	m_pData = pData;
	}

void CFileDataBaseCfgComms::CEthernet::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

UINT CFileDataBaseCfgComms::CEthernet::GetSettings(void)
{
	UINT uPtr = m_uBase + 0x0006;

	return m_pData->GetWord(uPtr);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetOptions(void)
{
	UINT uPtr = m_uBase + 0x0008;

	return m_pData->GetData(uPtr, 8);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetAddr(void)
{
	UINT uPtr = m_uBase + 0x0010;

	return m_pData->GetData(uPtr, 16);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetMask(void)
{
	UINT uPtr = m_uBase + 0x0020;

	return m_pData->GetData(uPtr, 16);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetGate(void)
{
	UINT uPtr = m_uBase + 0x0030;

	return m_pData->GetData(uPtr, 16);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetDNS1(void)
{
	UINT uPtr = m_uBase + 0x0040;

	return m_pData->GetData(uPtr, 16);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetDNS2(void)
{
	UINT uPtr = m_uBase + 0x0050;

	return m_pData->GetData(uPtr, 16);
	}

PCBYTE CFileDataBaseCfgComms::CEthernet::GetSecList(void)
{
	UINT uPtr = m_uBase + 0x0080;

	return m_pData->GetData(uPtr, 128);
	}

// Operations

void CFileDataBaseCfgComms::CEthernet::SetSettings(UINT uData)
{
	UINT uPtr = m_uBase + 0x0006;

	return m_pData->PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::CEthernet::SetOptions(PCBYTE pData)
{
	UINT uPtr = m_uBase + 0x0008;

	return m_pData->PutData(uPtr, pData, 8);
	}

void CFileDataBaseCfgComms::CEthernet::SetAddr(PCBYTE pAddr)
{
	UINT uPtr = m_uBase + 0x0010;

	return m_pData->PutData(uPtr, pAddr, 16);
	}

void CFileDataBaseCfgComms::CEthernet::SetMask(PCBYTE pAddr)
{
	UINT uPtr = m_uBase + 0x0020;

	return m_pData->PutData(uPtr, pAddr, 16);
	}

void CFileDataBaseCfgComms::CEthernet::SetGate(PCBYTE pAddr)
{
	UINT uPtr = m_uBase + 0x0030;

	return m_pData->PutData(uPtr, pAddr, 16);
	}

void CFileDataBaseCfgComms::CEthernet::SetDNS1(PCBYTE pAddr)
{
	UINT uPtr = m_uBase + 0x0040;

	return m_pData->PutData(uPtr, pAddr, 16);
	}

void CFileDataBaseCfgComms::CEthernet::SetDNS2(PCBYTE pAddr)
{
	UINT uPtr = m_uBase + 0x0050;

	return m_pData->PutData(uPtr, pAddr, 16);
	}

void CFileDataBaseCfgComms::CEthernet::SetSecList(PCBYTE pAddr)
{
	UINT uPtr = m_uBase + 0x0080;

	return m_pData->PutData(uPtr, pAddr, 128);
	}

// Serial

// Initialization

void CFileDataBaseCfgComms::CSerial::SetFile(IFileData *pData)
{
	m_pData = pData;
	}

void CFileDataBaseCfgComms::CSerial::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

// Attributes

UINT CFileDataBaseCfgComms::CSerial::GetBaud(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetMode(void)
{
	UINT uPtr = m_uBase + 0x0001;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetProtocol(void)
{
	UINT uPtr = m_uBase + 0x0002;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetHand(void)
{
	UINT uPtr = m_uBase + 0x0003;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetPassThru(void)
{
	UINT uPtr = m_uBase + 0x0004;

	return m_pData->GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetLead(void)
{
	UINT uPtr = m_uBase + 0x0006;

	return m_pData->GetWord(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetLag(void)
{
	UINT uPtr = m_uBase + 0x0008;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgComms::CSerial::GetGap(void)
{
	UINT uPtr = m_uBase + 0x000A;

	return m_pData->GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgComms::CSerial::SetBaud(UINT uData)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetMode(UINT uData)
{
	UINT uPtr = m_uBase + 0x0001;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetProt(UINT uData)
{
	UINT uPtr = m_uBase + 0x0002;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetHand(UINT uData)
{
	UINT uPtr = m_uBase + 0x0003;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetPass(UINT uData)
{
	UINT uPtr = m_uBase + 0x0004;

	m_pData->PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetLead(UINT uData)
{
	UINT uPtr = m_uBase + 0x0006;

	m_pData->PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetLag(UINT uData)
{
	UINT uPtr = m_uBase + 0x0008;

	m_pData->PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgComms::CSerial::SetGap(UINT uData)
{
	UINT uPtr = m_uBase + 0x000A;

	m_pData->PutWord(uPtr, WORD(uData));
	}

// Implementaiton

CString CFileDataBaseCfgComms::FormatAddr(PCBYTE pAddr)
{
	BYTE b1 = pAddr[0];
	BYTE b2 = pAddr[1];
	BYTE b3 = pAddr[2];
	BYTE b4 = pAddr[3];

	return CPrintf(L"%u.%u.%u.%u", b1, b2, b3, b4);
	}

// End of File

