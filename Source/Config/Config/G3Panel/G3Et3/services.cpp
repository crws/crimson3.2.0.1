  
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Service Item
//

// Dynamic Class

AfxImplementDynamicClass(CEt3Services, CMetaItem);

// Constructor

CEt3Services::CEt3Services(void)
{
	m_pList = New CItemIndexList;
	}

// Attributes

UINT CEt3Services::GetTreeImage(void) const
{
	return IDI_SERVICES;
	}

// Persistance

void CEt3Services::Init(void)
{
	CMetaItem::Init();

	AddServices();
	}

// Meta Data Creation

void CEt3Services::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(List);

	Meta_SetName((IDS_SERVICES));
	}

// Service Creation

void CEt3Services::AddServices(void)
{
	m_pList->AppendItem(New CHeartbeatItem);
	
	m_pList->AppendItem(New CWebServerItem);
	}

// End of File
