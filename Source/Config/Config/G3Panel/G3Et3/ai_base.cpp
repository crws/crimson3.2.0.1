
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogInputBaseItem, CIOItem);

// Constructor

CAnalogInputBaseItem::CAnalogInputBaseItem(void)
{
	m_uType	= 0;

	m_fTimebase = FALSE;
	}

// Overridables

UINT CAnalogInputBaseItem::GetTreeImage(void)
{
	return IDI_AI;
	}

// Persistence

void CAnalogInputBaseItem::Init(void)
{
	CIOItem::Init();

	m_fIs16Iso20m	 =  E3_MOD_16ISO20M == m_pSystem->GetModuleIdent();

	m_Integration	 = 0;

	m_TempUnits	 = 0;

	m_TempFormat	 = 0;
	}

void CAnalogInputBaseItem::PostLoad(void)
{
	CIOItem::PostLoad();

	m_fIs16Iso20m	 =  E3_MOD_16ISO20M == m_pSystem->GetModuleIdent();
	}

// Download Support

void CAnalogInputBaseItem::PrepareData(void)
{
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	if( m_pSystem->HasFlag(L"HasAITemperature") ) {

		UINT uFormat = m_TempFormat + m_TempUnits * 2;

		File.SetTemperatureReporting(uFormat);
		}

	if( m_pSystem->HasFlag(L"HasAIIntegration") ) {
		
		File.SetAnalogInputFiltering(m_Integration);
		}
	}

// Meta Data Creation

void CAnalogInputBaseItem::AddMetaData(void)
{
	CIOItem::AddMetaData();

	Meta_AddInteger(Integration);
	Meta_AddInteger(TempFormat);
	Meta_AddInteger(TempUnits);

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		UINT uOffset = (PBYTE(m_Range + n) - PBYTE(this));

		AddMeta( CPrintf("Range%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Feature1); n ++ ) {

		UINT uOffset = (PBYTE(m_Feature1 + n) - PBYTE(this));

		AddMeta( CPrintf("Feature1%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Feature2); n ++ ) {

		UINT uOffset = (PBYTE(m_Feature2 + n) - PBYTE(this));

		AddMeta( CPrintf("Feature2%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Timebase); n ++ ) {

		UINT uOffset = (PBYTE(m_Timebase + n) - PBYTE(this));

		AddMeta( CPrintf("Timebase%2.2d", 1 + n), metaInteger, uOffset );
		}
	}

// End of File
