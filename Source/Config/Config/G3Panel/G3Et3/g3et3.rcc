
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hxx"

#include "g3et3.hxx"
	
//////////////////////////////////////////////////////////////////////////
//								
// Version Data
//

#include "version.rcc"

//////////////////////////////////////////////////////////////////////////
//								
// Toolbar Icons
//

Et3Tool16 BITMAP "images\\et3tool16.bmp"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#include "g3et3.loc"

//////////////////////////////////////////////////////////////////////////
//								
// Other Resources
//

#include "comms.rcc"

#include "transfer.rcc"

#include "services.rcc"

#include "security.rcc"

#include "io.rcc"

//////////////////////////////////////////////////////////////////////////
//
// String Table
//

STRINGTABLE
BEGIN
	IDS_ADD_ADDITIONAL      "Add Additional Device"
	IDS_ADD_ANALOG          "Add Analog Transfer"
	IDS_ADD_DISCRETE        "Add Discrete Transfer"
	IDS_ADD_NEW_ANALOG      "Add a new Analog Transfer."
	IDS_ADD_NEW_DISCRETE    "Add a new Discrete Transfer."
	IDS_ANALOG              "Analogs" /* NOT USED */
	IDS_ANALOG_CHANNEL      "Analog Channel" /* NOT USED */
	IDS_ANALOG_INPUT        "Analog Input Channels"
	IDS_ANALOG_INPUTS       "Analog Inputs"
	IDS_ANALOG_OUTPUT       "Analogs Output Channels" /* NOT USED */
	IDS_ANALOG_OUTPUTS      "Analog Outputs"
	IDS_ANALOG_OUTPUT_2     "Analog Output Channels"
	IDS_ANALOG_TRANSFER     "Analog Transfer"
	IDS_BURNOUT_DETECTION   "Burnout Detection"
	IDS_CHANNELS            "Channels"
	IDS_CHANNEL_CAPTION     "I/O Channels"
	IDS_CHANNEL_MANAGER     "I/O Channels"
	IDS_COLD_JUNCTION       "Cold Junction Sensors"
	IDS_COMMS_CAPTION       "Communications"
	IDS_COMMS_MANAGER       "Comms Manager"
	IDS_COMMS_NAME          "<name>"
	IDS_COMMS_PORT          "Communications Port"
	IDS_COMMS_PORT_ID       "Port %u"
	IDS_CONFIGURATION       "The configuration setting of the discrete output channel."
	IDS_COUNTERS            "Counters"
	IDS_COUNTER_RESET       "Counter Reset"
	IDS_CREATE              "create"
	IDS_DELETE_1            "Delete" /* NOT USED */
	IDS_DELETE_2            "delete"
	IDS_DELETE_3            "Delete %1"
	IDS_DEV                 "DEV"
	IDS_DEVICES             "Devices"
	IDS_DEVICE_COMMANDS     "Device Commands"
	IDS_DISCRETE            "Discretes" /* NOT USED */
	IDS_DISCRETE_CHANNEL    "Discrete Channel" /* NOT USED */
	IDS_DISCRETE_INPUT      "Discrete Input Channels"
	IDS_DISCRETE_INPUTS     "Discrete Inputs"
	IDS_DISCRETE_OUTPUT     "Discrete Output Channels"
	IDS_DISCRETE_OUTPUTS    "Discrete Outputs"
	IDS_DISCRETE_TRANSFER   "Discrete Transfer"
	IDS_DOWNLOAD            "Download"
	IDS_DRIVER              "Driver"
	IDS_DRIVER_SELECTION    "Driver Selection"
	IDS_DRIVER_SETTINGS     "Driver Settings"
	IDS_ENABLE_COUNTER      "Enable the counter reset on the associated virtual counter channel."
	IDS_ETHERNET_1          "Ethernet 1"
	IDS_ETHERNET_2          "Ethernet 2"
	IDS_ETHERNET_3          "Ethernet Interface"
	IDS_FEATURE             "Feature 1"
	IDS_FEATURE_2           "Feature 2"
	IDS_FEATURE_3           "Feature"
	IDS_FORMAT              ""
	IDS_FORMAT_3            "Transfer%1!u!"
	IDS_FORMAT_4            "User%1!u!"
	IDS_FREE_INTERNAL       "Free Internal Registers"
	IDS_FREE_INTERNAL_2     "Free Internal registers"
	IDS_HEARTBEAT_BYPASS    "Heartbeat Bypass"
	IDS_INTEGRATION         "Integration"
	IDS_IO_STATUS_READ      "I/O Status (Read Only)"
	IDS_IO_TRANSFER         "I/O Transfer Status (Read Only)"
	IDS_LINK_STATUS_READ    "Link Status 1 (Read Only)"
	IDS_LINK_STATUS_READ_2  "Link Status 2 (Read Only)"
	IDS_MODBUS              "Modbus"
	IDS_MODBUS_ASCII        "Modbus ASCII Slave"
	IDS_MODBUS_MASTER       "Modbus Master"
	IDS_MODBUS_RTU_SLAVE    "Modbus RTU Slave"
	IDS_MODBUS_UDP          "Modbus UDP"
	IDS_MODE                "Mode"
	IDS_NAME_FMT_IS         "The name %1 is already in use."
	IDS_NETWORK             "Network"
	IDS_NETWORK_OVERLOAD    "Network Overload (Read Only)"
	IDS_NETWORK_PORT        "Network Port"
	IDS_NEW_ANALOG          "New Analog Transfer"
	IDS_NEW_DISCRETE        "New Discrete Transfer"
	IDS_NONE                "-1|None"
	IDS_OF_FMT              ", of %d"
	IDS_OPTIONS             "Options"
	IDS_PHYSICAL_IO         "Physical IO Channels"
	IDS_PORT_COMMANDS       "Port Commands"
	IDS_POWER_OK_READ       "Power 1 OK (Read Only)"
	IDS_POWER_OK_READ_2     "Power 2 OK (Read Only)"
	IDS_RANGE               "Range"
	IDS_RENAME_FMT          "Rename %1"
	IDS_REPORTING           "Reporting"
	IDS_RESERVED            "Reserved"
	IDS_RESET               "Reset"
	IDS_RESOLUTION          "Resolution"
	IDS_RING_COMPLETE       "Ring Complete (Read Only)"
	IDS_SCALE               "Scale"
	IDS_SCALE_BEYOND_PV     "Scale Beyond PV Points"
	IDS_SECURITY_MANAGER    "Security Manager"
	IDS_SELECT              "Select the communications driver for this port. After the driver has been selected, a device will be created. If the driver supports multi-drop operation, you will have the option of creating additional devices."
	IDS_SELECT_BIT_TO       "Select a bit to update when the transfer completes. Each active transfer must have a unique status bit."
	IDS_SELECT_COUNTER      "Select the counter type for this channel."
	IDS_SELECT_TIMEBASE     "Select the time-base for this channel."
	IDS_SELF_TEST_OK_READ   "Self Test OK (Read Only)"
	IDS_SERIAL_PORT         "Serial Port"
	IDS_SERVICES            "Services"
	IDS_SIXNET_UDR          "Sixnet UDR"
	IDS_STATUS_BIT          "Status Bit"
	IDS_SYSTEM              "System"
	IDS_TEMPERATURE         "Temperature"
	IDS_THERMOCOUPLE_TYPE   "Thermocouple Type"
	IDS_TIMEBASES           "Timebases"
	IDS_TIME_BASES          "Time bases"
	IDS_TPO_REGISTERS       "TPO Registers"
	IDS_TRANSFER            "I/O Transfer"
	IDS_TRANSFER_CAPTION    "I/O Transfers"
	IDS_TRANSFER_MANAGER    "I/O Transfers"
	IDS_TYPE                "Type"
	IDS_UNIVERSAL_MASTER    "Universal Master"
	IDS_UNIVERSAL_SLAVE     "Universal Slave"
	IDS_USER                "User" /* NOT USED */
	IDS_WATCHDOG            "Watchdog/Heartbeat"
	IDS_WEBSERVER           "Web Server"
END

//////////////////////////////////////////////////////////////////////////
//
// Prompt Table
//

STRINGTABLE
BEGIN
	IDM_COMMS_ADD_DEVICE		"Add a new device to this communications port."
	IDM_COMMS_DEL_DEVICE		"Delete the current device and any associated mapping blocks."
	IDM_TRANSFER_NEW_DISCRETE	"Create a new discrete transfer."
	IDM_TRANSFER_NEW_ANALOG		"Create a new analog transfer."
END

// End of File
