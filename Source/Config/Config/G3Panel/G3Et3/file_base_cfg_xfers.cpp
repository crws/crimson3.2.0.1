
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/xfers"
//

// Constructor

CFileDataBaseCfgXfers::CFileDataBaseCfgXfers(void) : CFileData("/base0/cfg/xfers")
{	
	m_uSize = 1312;

	m_pData	= New BYTE [ m_uSize ];	

	for( UINT n = 0; n < elements(m_Transfers); n ++ ) {

		CTransfer &Transfer = m_Transfers[n];

		Transfer.SetBase(0x0020 + n * 40);

		Transfer.SetFile(this);
		}
	}

// Destructor

CFileDataBaseCfgXfers::~CFileDataBaseCfgXfers(void)
{
	if( m_pData ) {

		delete [] m_pData;

		m_pData = NULL;

		m_uSize = 0;
		}
	}

// Development

CString CFileDataBaseCfgXfers::FormatIPAddr(PCBYTE pAddr)
{
	return CPrintf(L"%u.%u.%u.%u", 
			pAddr[0],
			pAddr[1],
			pAddr[2],
			pAddr[3]
			);
	}

void CFileDataBaseCfgXfers::Dump(void)
{
	AfxTrace(L"===============\n");

	AfxTrace(L"Properties for file %s\n", CString(GetFile()));

	AfxTrace(L" scan time\t\t\t%d\n",      GetScanTime());
	AfxTrace(L" serial timeout\t\t%d\n", GetSerialTimeout());
	AfxTrace(L" ethernet timeout\t%d\n", GetNetTimeout());

	for( UINT n = 0; n < elements(m_Transfers); n ++ ) {

		CTransfer x = m_Transfers[n];		

		AfxTrace(L"%d.\tblock type %2.2X\n", n, x.GetBlockType());
		AfxTrace(L"%d.\tstation\t\t%d\n",    n, x.GetStation());
		AfxTrace(L"%d.\tinterface\t%d\n",    n, x.GetInterface());
		AfxTrace(L"%d.\tip address\t%s\n",   n, FormatIPAddr(x.GetIPAddr()));
		AfxTrace(L"%d.\tdest port\t%d\n",    n, x.GetDestPort());
		AfxTrace(L"%d.\tstatus bit\t%d\n",   n, x.GetStatusBit());

		AfxTrace(L"%d.\tcount\t\t%d\n",      n, x.GetRegCount());
		AfxTrace(L"%d.\tsrce type\t%d\n",    n, x.GetSrceType());
		AfxTrace(L"%d.\tsrce addr\t%d\n",    n, x.GetSrceAddr());
		AfxTrace(L"%d.\tdest type\t%d\n",    n, x.GetDestType());
		AfxTrace(L"%d.\tdest addr\t%d\n",    n, x.GetDestAddr());
		}

	AfxTrace(L"===============\n");
	}

// Attributes

UINT CFileDataBaseCfgXfers::GetScanTime(void)
{
	UINT uPtr = 0x0010;

	return GetLong(uPtr);
	}

UINT CFileDataBaseCfgXfers::GetSerialTimeout(void)
{
	UINT uPtr = 0x0014;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgXfers::GetNetTimeout(void)
{
	UINT uPtr = 0x0016;

	return GetWord(uPtr);
	}

// Operations

void CFileDataBaseCfgXfers::ClearTransfers(void)
{
	UINT uPtr = 0x0020;

	memset(m_pData + uPtr, 0, 1280);
	}

void CFileDataBaseCfgXfers::PutScanTime(UINT uTime)
{
	UINT uPtr = 0x0010;

	PutLong(uPtr, uTime);
	}

void CFileDataBaseCfgXfers::PutSerialTimeout(UINT uTimeout)
{
	UINT uPtr = 0x0014;

	PutWord(uPtr, WORD(uTimeout));
	}

void CFileDataBaseCfgXfers::PutNetTimeout(UINT uTimeout)
{
	UINT uPtr = 0x0016;

	PutWord(uPtr, WORD(uTimeout));
	}

// Transfer

void CFileDataBaseCfgXfers::CTransfer::SetFile(IFileData *pData)
{
	m_pData = pData;
	}

void CFileDataBaseCfgXfers::CTransfer::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

// Attributes

UINT CFileDataBaseCfgXfers::CTransfer::GetBlockType(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetStation(void)
{
	UINT uPtr = m_uBase + 0x0001;

	return m_pData->GetWord(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetInterface(void)
{
	UINT uPtr = m_uBase + 0x0003;

	return m_pData->GetByte(uPtr);
	}

PCBYTE CFileDataBaseCfgXfers::CTransfer::GetIPAddr(void)
{
	UINT uPtr = m_uBase + 0x0004;

	return m_pData->GetData(uPtr, 16);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetDestPort(void)
{
	UINT uPtr = m_uBase + 0x0014;

	return m_pData->GetWord(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetStatusBit(void)
{
	UINT uPtr = m_uBase + 0x0016;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetRegCount(void)
{
	UINT uPtr = m_uBase + 0x001C;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetSrceType(void)
{
	UINT uPtr = m_uBase + 0x001D;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetSrceAddr(void)
{
	UINT uPtr = m_uBase + 0x001E;

	return m_pData->GetWord(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetDestType(void)
{
	UINT uPtr = m_uBase + 0x0020;

	return m_pData->GetByte(uPtr);
	}

UINT CFileDataBaseCfgXfers::CTransfer::GetDestAddr(void)
{
	UINT uPtr = m_uBase + 0x0021;

	return m_pData->GetWord(uPtr);
	}

// Operations

void CFileDataBaseCfgXfers::CTransfer::PutBlockType(UINT uType)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutByte(uPtr, BYTE(uType));
	}

void CFileDataBaseCfgXfers::CTransfer::PutStation(UINT uStation)
{
	UINT uPtr = m_uBase + 0x0001;

	m_pData->PutWord(uPtr, WORD(uStation));
	}

void CFileDataBaseCfgXfers::CTransfer::PutInterface(UINT uInterface)
{
	UINT uPtr = m_uBase + 0x0003;

	m_pData->PutByte(uPtr, BYTE(uInterface));
	}

void CFileDataBaseCfgXfers::CTransfer::PutIPAddr(PCBYTE pIPAddr)
{
	UINT uPtr = m_uBase + 0x0004;

	m_pData->PutData(uPtr, pIPAddr, 16);
	}

void CFileDataBaseCfgXfers::CTransfer::PutDestPort(UINT uPort)
{
	UINT uPtr = m_uBase + 0x0014;

	m_pData->PutWord(uPtr, WORD(uPort));
	}

void CFileDataBaseCfgXfers::CTransfer::PutStatusBit(UINT uStatus)
{
	UINT uPtr = m_uBase + 0x0016;

	m_pData->PutByte(uPtr, BYTE(uStatus));
	}

void CFileDataBaseCfgXfers::CTransfer::PutRegCount(UINT uCount)
{
	UINT uPtr = m_uBase + 0x001C;

	m_pData->PutByte(uPtr, BYTE(uCount));
	}

void CFileDataBaseCfgXfers::CTransfer::PutSrceType(UINT uType)
{
	UINT uPtr = m_uBase + 0x001D;

	m_pData->PutByte(uPtr, BYTE(uType));
	}

void CFileDataBaseCfgXfers::CTransfer::PutSrceAddr(UINT uAddr)
{
	UINT uPtr = m_uBase + 0x001E;
		
	m_pData->PutWord(uPtr, WORD(uAddr));
	}

void CFileDataBaseCfgXfers::CTransfer::PutDestType(UINT uType)
{
	UINT uPtr = m_uBase + 0x0020;

	m_pData->PutByte(uPtr, BYTE(uType));
	}

void CFileDataBaseCfgXfers::CTransfer::PutDestAddr(UINT uAddr)
{
	UINT uPtr = m_uBase + 0x0021;

	m_pData->PutWord(uPtr, WORD(uAddr));
	}

// End of File
