
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Heartbeat/Watchdog Page
//

class CHeartbeatPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CHeartbeatPage(CHeartbeatItem *pItem);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEt3CommsSystem     * m_pSystem;
		CHeartbeatItem      * m_pHeartbeat;

		// Implementation
		void LoadEnables(IUICreate *pView);
		void LoadIOActions(IUICreate *pView);
		void LoadHBActions(IUICreate *pView);
		void LoadBitMask(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Heartbeat/Watchdog Page
//

// Runtime Class

AfxImplementRuntimeClass(CHeartbeatPage, CUIStdPage);

// Constructor

CHeartbeatPage::CHeartbeatPage(CHeartbeatItem *pItem)
{
	m_Class      = AfxRuntimeClass(CHeartbeatItem);

	m_pHeartbeat = pItem;

	m_pSystem    = (CEt3CommsSystem *) pItem->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CHeartbeatPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartPage(1);

	LoadEnables(pView);

	LoadIOActions(pView);

	LoadHBActions(pView);

	pView->EndPage(TRUE);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

void CHeartbeatPage::LoadEnables(IUICreate *pView)
{
	pView->StartGroup(L"Watchdog Output Enables", 1);

	//LoadBitMask(pView);	

	pView->AddUI(m_pHeartbeat, L"root", L"EnablesPower1Fail");
	pView->AddUI(m_pHeartbeat, L"root", L"EnablesPower2Fail");
	//pView->AddUI(m_pHeartbeat, L"root", L"EnablesPoEFail");
	pView->AddUI(m_pHeartbeat, L"root", L"EnablesNetRingFail");
	pView->AddUI(m_pHeartbeat, L"root", L"EnablesCPUMonitor");
	pView->AddUI(m_pHeartbeat, L"root", L"EnablesIOPollTimeout");
	pView->AddUI(m_pHeartbeat, L"root", L"EnablesHeartTimeout");

	pView->EndGroup(FALSE);
	}

void CHeartbeatPage::LoadIOActions(IUICreate *pView)
{
	BOOL fHasPhysDO = m_pSystem->GetPhysDOs() > 0;

	BOOL fHasPhysAO = m_pSystem->GetPhysAOs() > 0;

	pView->StartGroup(L"I/O Poll Timeout Actions", 1);

	pView->AddUI(m_pHeartbeat, L"root", L"IOEnable");

	pView->AddUI(m_pHeartbeat, L"root", L"IOTimeout");

	if( fHasPhysDO || fHasPhysAO ) {

		pView->AddUI(m_pHeartbeat, L"root", L"IODropPhys");
		}

	if( fHasPhysDO ) {

		pView->AddUI(m_pHeartbeat, L"root", L"IODropFirst");
		}

	pView->AddUI(m_pHeartbeat, L"root", L"IODropVirt");

	pView->EndGroup(FALSE);
	}

void CHeartbeatPage::LoadHBActions(IUICreate *pView)
{
	BOOL fHasPhysDO = m_pSystem->GetPhysDOs() > 0;

	BOOL fHasPhysAO = m_pSystem->GetPhysAOs() > 0;

	pView->StartGroup(L"Heartbeat Timeout Actions", 1);

	pView->AddUI(m_pHeartbeat, L"root", L"HBEnable");

	pView->AddUI(m_pHeartbeat, L"root", L"HBType");

	pView->AddUI(m_pHeartbeat, L"root", L"HBAddr");

	pView->AddUI(m_pHeartbeat, L"root", L"HBDelay");

	pView->AddUI(m_pHeartbeat, L"root", L"HBTimeout");

	if( fHasPhysDO || fHasPhysAO ) {

		pView->AddUI(m_pHeartbeat, L"root", L"HBDropPhys");
		}

	if( fHasPhysDO ) {

		pView->AddUI(m_pHeartbeat, L"root", L"HBDropFirst");
		}

	pView->AddUI(m_pHeartbeat, L"root", L"HBDropVirt");

	//
	pView->AddUI(m_pHeartbeat, L"root", L"ResetEnable");

	pView->AddUI(m_pHeartbeat, L"root", L"ResetType");

	pView->AddUI(m_pHeartbeat, L"root", L"ResetAddr");

	pView->EndGroup(FALSE);
	}

void CHeartbeatPage::LoadBitMask(IUICreate *pView)
{
	CString Form;

	Form += L"1|20";	Form += L"Power 1 Failure";
	Form += L"|";		Form += L"Power 2 Failure";
	Form += L"|";		Form += L"PoE Failure";
	Form += L"|";		Form += L"Ethernet Ring Failure";
	Form += L"|";		Form += L"Watchdog CPU Monitor";
	Form += L"|";		Form += L"I/O Polling Timeout";
	Form += L"|";		Form += L"Heartbeat Timeout";

	if( TRUE ) {

		CUIData Data;

		Data.m_Tag	 = L"Enables";

		Data.m_Label	 = L""/*L"Watchdog Output Enables"*/;

		Data.m_ClassText = AfxNamedClass(L"CUITextBitMask");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIPick");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pHeartbeat, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Heartbeat/Watchdog Item
//

// Dynamic Class

AfxImplementDynamicClass(CHeartbeatItem, CEt3UIItem);

// Constructor

CHeartbeatItem::CHeartbeatItem(void)
{
	m_Enables	= 1 << 3;

	//
	m_EnablesPower1Fail    = 0;

	m_EnablesPower2Fail    = 0;

	m_EnablesPoEFail       = 0;

	m_EnablesNetRingFail   = 0;

	m_EnablesCPUMonitor    = 1;

	m_EnablesIOPollTimeout = 0;

	m_EnablesHeartTimeout  = 0;

	//
	m_IOEnable	= 0;

	m_IOTimeout	= 1;

	m_IODropPhys	= 0;

	m_IODropFirst	= 0;

	m_IODropVirt	= 0;

	//
	m_HBEnable	= 0;

	m_HBType	= 10;

	m_HBAddr	= 1;

	m_HBDelay	= 0;

	m_HBTimeout	= 1;

	m_HBDropPhys	= 0;

	m_HBDropVirt	= 0;

	m_HBDropFirst	= 0;

	m_fResetEnable  = 0;

	m_ResetType	= 10;

	m_ResetAddr     = 1;
	}

// Attributes

UINT CHeartbeatItem::GetTreeImage(void) const
{
	return IDI_WATCHDOG;
	}

// UI Creation

BOOL CHeartbeatItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CHeartbeatPage(this));
	
	return FALSE;
	}

// UI Update

void CHeartbeatItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag.EndsWith(L"Enable") ) {

		if( Tag.StartsWith(L"IO") ) {
			
			m_EnablesIOPollTimeout = 0;

			pHost->UpdateUI(this, L"EnablesIOPollTimeout");
			}

		if( Tag.StartsWith(L"HB") ) {
			
			m_EnablesHeartTimeout = 0;

			pHost->UpdateUI(this, L"EnablesHeartTimeout");
			}

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

void CHeartbeatItem::PrepareData(void)
{
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	File.SetWatchdogOutputEnables(BuildHBEnables());

	File.SetOutputIOTimeout      (m_IOEnable ? m_IOTimeout * 1000 : 0);

	File.SetIOTimeoutActions     (BuildIOActions());

	if( TRUE ) {

		UINT  uData = m_HBAddr - 1;

		SetBit(uData, m_HBType == 11, 15);

		File.SetHBDiscreteRegister(uData);
		}

	File.SetHBTimeout    (m_HBEnable ? m_HBTimeout : 0);

	File.SetHBBootTimeout(m_HBEnable ? m_HBDelay   : 0);

	if( TRUE ) {

		UINT  uData = m_ResetAddr - 1;

		SetBit(uData, m_ResetType == 11, 15);

		File.SetHBManualResetRegister(uData);		
		}

	if( TRUE ) {

		UINT uData = File.GetHBFeatureEnables();

		SetBit(uData, m_fResetEnable, 0);

		SetBit(uData, m_HBEnable,     1);

		File.SetHBFeatureEnables(uData);
		}
	}

// Meta Data Creation

void CHeartbeatItem::AddMetaData(void)
{
	CUIItem::AddMetaData();	

	Meta_AddInteger(Enables);
	Meta_AddInteger(EnablesPower1Fail);
	Meta_AddInteger(EnablesPower2Fail);
	Meta_AddInteger(EnablesNetRingFail);
	Meta_AddInteger(EnablesCPUMonitor);
	Meta_AddInteger(EnablesIOPollTimeout);
	Meta_AddInteger(EnablesHeartTimeout);
	Meta_AddInteger(IOEnable);
	Meta_AddInteger(IOTimeout);
	Meta_AddInteger(IODropPhys);
	Meta_AddInteger(IODropFirst);
	Meta_AddInteger(IODropVirt);
	Meta_AddInteger(HBEnable);
	Meta_AddInteger(HBType);
	Meta_AddInteger(HBAddr);
	Meta_AddInteger(HBDelay);
	Meta_AddInteger(HBTimeout);
	Meta_AddInteger(HBDropPhys);
	Meta_AddInteger(HBDropFirst);
	Meta_AddInteger(HBDropVirt);
	Meta_AddBoolean(ResetEnable);
	Meta_AddInteger(ResetType);
	Meta_AddInteger(ResetAddr);

	Meta_SetName((IDS_WATCHDOG));
	}

// Implementation

void CHeartbeatItem::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(L"EnablesIOPollTimeout", m_IOEnable > 0);
	pHost->EnableUI(L"EnablesHeartTimeout",  m_HBEnable > 0);

	pHost->EnableUI(L"IOTimeout",		 m_IOEnable > 0);
	pHost->EnableUI(L"IODropPhys",		 m_IOEnable > 0);
	pHost->EnableUI(L"IODropFirst",		 m_IOEnable > 0);
	pHost->EnableUI(L"IODropVirt",		 m_IOEnable > 0);

	pHost->EnableUI(L"HBType",		 m_HBEnable > 0);
	pHost->EnableUI(L"HBAddr",		 m_HBEnable > 0);
	pHost->EnableUI(L"HBDelay",		 m_HBEnable > 0);
	pHost->EnableUI(L"HBTimeout",		 m_HBEnable > 0);
	pHost->EnableUI(L"HBDropPhys",		 m_HBEnable > 0);
	pHost->EnableUI(L"HBDropFirst",		 m_HBEnable > 0);
	pHost->EnableUI(L"HBDropVirt",		 m_HBEnable > 0);
	
	pHost->EnableUI(L"ResetEnable",		 m_HBEnable > 0);
	pHost->EnableUI(L"ResetType",		 m_HBEnable > 0 && m_fResetEnable > 0);
	pHost->EnableUI(L"ResetAddr",		 m_HBEnable > 0 && m_fResetEnable > 0);
	}

WORD CHeartbeatItem::BuildHBEnables(void)
{
	WORD wData = 0;

	SetBit(wData, m_EnablesPower1Fail    > 0, 0);
	SetBit(wData, m_EnablesPower2Fail    > 0, 1);
	SetBit(wData, m_EnablesPoEFail       > 0, 2);
	SetBit(wData, m_EnablesNetRingFail   > 0, 3);
	SetBit(wData, m_EnablesCPUMonitor    > 0, 4);
	SetBit(wData, m_EnablesIOPollTimeout > 0, 5);
	SetBit(wData, m_EnablesHeartTimeout  > 0, 6);

	return wData;
	}

BYTE CHeartbeatItem::BuildIOActions(void)
{
	BYTE bData = 0;

	SetBit(bData, m_IOEnable > 0 && m_IODropPhys  > 0, 0);
	SetBit(bData, m_IOEnable > 0 && m_IODropFirst > 0, 1);
	SetBit(bData, m_IOEnable > 0 && m_IODropVirt  > 0, 2);

	SetBit(bData, m_HBEnable > 0 && m_HBDropPhys  > 0, 3);
	SetBit(bData, m_HBEnable > 0 && m_HBDropVirt  > 0, 4);
	SetBit(bData, m_HBEnable > 0 && m_HBDropFirst > 0, 5);

	return bData;
	}

// End of File
