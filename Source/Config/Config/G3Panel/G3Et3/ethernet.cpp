
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Default IPs
//

#define	IP1_STD	DWORD(MAKELONG(MAKEWORD( 21, 1), MAKEWORD(168, 192)))

#define	IP2_STD	DWORD(MAKELONG(MAKEWORD( 21, 0), MAKEWORD(  1,  10)))

#define	IP_MASK	DWORD(MAKELONG(MAKEWORD(0, 255), MAKEWORD(255, 255)))

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Page
//

class CEthernetPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEthernetPage(CEt3EthernetItem *pNet, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEt3EthernetItem * m_pNet;
		UINT               m_uPage;
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Page
//

// Runtime Class

AfxImplementRuntimeClass(CEthernetPage, CUIPage);

// Constructor

CEthernetPage::CEthernetPage(CEt3EthernetItem *pNet, UINT uPage)
{
	m_pNet  = pNet;

	switch( uPage ) {

		case 1:
			m_Title = CString(CString(IDS_OPTIONS));
			break;

		case 2:
			m_Title = CString(IDS_ETHERNET_1);
			break;
			
		case 3:
			m_Title = CString(IDS_ETHERNET_2);
			break;

		case 4:
			m_Title = CString(CString(IDS_DOWNLOAD));
			break;
		}

	m_uPage = uPage;
	}

// Operations

BOOL CEthernetPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CLASS    Class = AfxPointerClass(m_pNet);

	CUIPage *pPage = New CUIStdPage(m_Title, Class, m_uPage);

	pPage->LoadIntoView(pView, pItem);

	delete pPage;

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Item
//

// Dynamic Class

AfxImplementDynamicClass(CEt3EthernetItem, CEt3UIItem);

// Constructor

CEt3EthernetItem::CEt3EthernetItem(void)
{
	m_DualMode	    = 3;

	m_ConnTimeoutSelect = 0;

	m_ConnTimeout	    = 0;  

	m_QoSConfig	    = 0;

	m_DownMode	    = 1;

	m_DownIP	    = 0;

	m_DownPort	    = 1594;

	m_pFace0	    = New CEt3EthernetFace(0);

	m_pFace1            = New CEt3EthernetFace(1);

	m_pPorts            = New CEt3CommsPortList(2);
	}

// UI Creation

BOOL CEt3EthernetItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CEthernetPage(this, 2));

	pList->Append(New CEthernetPage(this, 3));

	pList->Append(New CEthernetPage(this, 4));

	pList->Append(New CEthernetPage(this, 1));

	return TRUE;
	}

// UI Update

void CEt3EthernetItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pHost);
		}
		
	if( Tag == "DownMode" ) {

		SaveDefaultIP();

		DoEnables(pHost);
		}

	if( Tag == "DownIP" || Tag == "DownPort" ) {

		SaveDefaultIP();
		}

	if( Tag == "ConnTimeoutSelect" ) {
		
		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CEt3EthernetItem::Init(void)
{
	m_DownIP = IP1_STD;

	CEt3UIItem::Init();

	SaveDefaultIP();
	}

void CEt3EthernetItem::PostLoad(void)
{
	CEt3UIItem::PostLoad();

	SaveDefaultIP();
	}

// Download Support

void CEt3EthernetItem::PrepareData(void)
{
	CEt3UIItem::PrepareData();

	CFileDataBaseCfgComms &File = m_pSystem->m_CfgComms;

	File.SetNetConnTimeout(BuildConnTimeout());

	File.SetQosConfig(BuildQoSConfig());

	File.SetNetModeJumper(m_DualMode);
	}

// Operations

void CEt3EthernetItem::MakePorts(void)
{
	m_pPorts->AppendItem(New CEt3CommsPortNetworkSixnet);

	m_pPorts->AppendItem(New CEt3CommsPortNetworkModbus);
	}

BOOL CEt3EthernetItem::SaveDefaultIP(void)
{
	DWORD IP = 0;

	switch( m_DownMode ) {

		case 0:
			IP = m_DownIP;
			break;

		case 1:
			IP = m_pFace0->GetAddress();
			break;

		case 2:
			IP = m_pFace1->GetAddress();
			break;
		}

	CDatabase *pDbase = GetDatabase();
	
	pDbase->SetIPAddress(TRUE, IP, m_DownPort);

	return TRUE;
	}

// Meta Data Creation

void CEt3EthernetItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(DualMode);
	Meta_AddInteger(ConnTimeoutSelect);
	Meta_AddInteger(ConnTimeout);
	Meta_AddInteger(QoSConfig);
	Meta_AddInteger(DownMode);
	Meta_AddInteger(DownIP);
	Meta_AddObject (Face0);
	Meta_AddObject (Face1);
	Meta_AddCollect(Ports);

	Meta_SetName((IDS_NETWORK));
	}

// Implementation

void CEt3EthernetItem::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("ConnTimeout", m_ConnTimeoutSelect == 4);

	pHost->EnableUI("DownIP",      !m_DownMode);
	}

// Config File Help

UINT CEt3EthernetItem::BuildConnTimeout(void)
{
	if( m_ConnTimeoutSelect == 4 ) {
		
		return m_ConnTimeout;
		}
	else {
		return m_ConnTimeoutSelect;
		}
	}

UINT CEt3EthernetItem::BuildQoSConfig(void)
{
	UINT uData = 0;

	SetBit(uData, m_QoSConfig == 1, 24);

	return uData;
	}

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Port Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CEt3EthernetFace, CEt3UIItem);

// Constructor

CEt3EthernetFace::CEt3EthernetFace(UINT uPort)
{
	m_uPort     = uPort;
	}

// UI Update

void CEt3EthernetFace::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		if( m_uPort == 0 ) {
			
			LimitEnum(pHost, L"PortMode", m_PortMode, 0x0E);
			}

		if( m_PortMode == 3 ) {

			FindAutoAddress();

			pHost->UpdateUI(L"Address");
			}

		DoEnables(pHost);
		}

	if( Tag == "PortMode" ) {

		if( m_PortMode == 3 ) {

			FindAutoAddress();

			pHost->UpdateUI(L"Address");
			}
		
		DoEnables(pHost);
		}

	if( Tag == "Address" || Tag == "PortMode" ) {

		CEt3EthernetItem *pItem = (CEt3EthernetItem *) GetParent();

		pItem->SaveDefaultIP();
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

DWORD CEt3EthernetFace::GetAddress(void) const
{
	return m_Address;
	}

// Persistence

void CEt3EthernetFace::Init(void)
{
	CEt3UIItem::Init();

	m_PortMode  = m_uPort == 0 ? 1 : 2;

	m_Address   = m_uPort == 0 ? IP1_STD : IP2_STD;

	m_NetMask   = DWORD(MAKELONG(MAKEWORD(0, 255), MAKEWORD(255, 255)));

	m_Gateway   = DWORD(MAKELONG(MAKEWORD(0,   0), MAKEWORD(  0,   0)));

	m_Secure    = 0;
	}

// Download Support

void CEt3EthernetFace::PrepareData(void)
{
	CFileDataBaseCfgComms            &File = m_pSystem->m_CfgComms;

	CFileDataBaseCfgComms::CEthernet &Face = File.m_Ethernet[m_uPort];

	Face.SetSettings(BuildSettings());
	Face.SetOptions (BuildOptions ());

	FindAutoAddress();

	Face.SetAddr(BuildIPAddr(m_Address));
	Face.SetMask(BuildIPAddr(m_NetMask));
	Face.SetGate(BuildIPAddr(m_Gateway));
	}

// Meta Data Creation

void CEt3EthernetFace::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(PortMode);
	Meta_AddInteger(Address);
	Meta_AddInteger(NetMask);
	Meta_AddInteger(Gateway);
	Meta_AddInteger(Secure);

	Meta_SetName((IDS_ETHERNET_3));
	}

// Implementation

void CEt3EthernetFace::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = (m_PortMode != 0);

	BOOL fManual = (m_PortMode == 1);

	BOOL fAuto   = (m_PortMode == 3);

	pHost->EnableUI("Address",    fEnable && fManual && !fAuto);
	
	pHost->EnableUI("NetMask",    fEnable && (fManual || fAuto));
	
	pHost->EnableUI("Gateway",    fEnable && (fManual || fAuto));
	}

void CEt3EthernetFace::FindAutoAddress(void)
{
	if( m_PortMode == 3 ) {

		UINT uStation = m_pSystem->m_pComms->m_Station;

		m_Address  = m_uPort == 0 ? IP1_STD : IP2_STD;

		m_Address &= IP_MASK;

		m_Address |= uStation & 255;
		}
	}

// Config File Help

UINT CEt3EthernetFace::BuildSettings(void)
{
	CFileDataBaseCfgComms            &File = m_pSystem->m_CfgComms;

	CFileDataBaseCfgComms::CEthernet &Face = File.m_Ethernet[m_uPort];

	if( m_uPort == 0 ) {

		UINT   uData = Face.GetSettings();

		return uData;
		}

	if( m_uPort == 1 ) {

		UINT   uData = Face.GetSettings();		

		SetBit(uData, m_PortMode > 0, 6);

		return uData;
		}

	AfxAssert(FALSE);

	return 0;
	}

PCBYTE CEt3EthernetFace::BuildOptions(void)
{
	CFileDataBaseCfgComms            &File = m_pSystem->m_CfgComms;

	CFileDataBaseCfgComms::CEthernet &Face = File.m_Ethernet[m_uPort];

	static	BYTE Data[8] = { 0 };

	memcpy(Data, Face.GetOptions(), 8);

	SetBit(Data[0], m_Secure   == 1, 0);

	SetBit(Data[0], m_PortMode == 2, 1);

	return Data;
	}

PCBYTE CEt3EthernetFace::BuildIPAddr(UINT uAddr)
{
	static	BYTE Data[16] = { 0 };

	if( (m_PortMode != 0) ) {

		Data[3] = LOBYTE(LOWORD(uAddr));
		Data[2] = HIBYTE(LOWORD(uAddr));
		Data[1] = LOBYTE(HIWORD(uAddr));
		Data[0] = HIBYTE(HIWORD(uAddr));
		}

	return Data;
	}

// End of File
