
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_COMTREE_HPP
	
#define	INCLUDE_COMTREE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Communications Tree View
//

class CEt3CommsTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CEt3CommsTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		CEt3CommsManager * m_pComms;
		CEt3CommsSystem  * m_pSystem;
		CNameMap	   m_MapFixed;
		UINT		   m_uLocked;
		BOOL		   m_fRefresh;

		// Editing Actions
		class CCmdCreate;
		class CCmdDelete;
		class CCmdRename;

		// Overridables
		void    OnAttach(void);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);

		// Notification Handlers
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);

		// Command Handlers
		BOOL OnCommsGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCommsControl(UINT uID, CCmdSource &Src);
		BOOL OnCommsCommand(UINT uID);
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnTransferGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnTransferControl(UINT uID, CCmdSource &Src);
		BOOL OnTransferCommand(UINT uID);
		BOOL CanTransferNew(void);
		BOOL CanTransferDelete(void);

		// Comms Menu
		BOOL CanCommsAddDevice(void);
		BOOL CanCommsDelDevice(void);
		void OnCommsAddDevice(void);
		void OnCommsDelDevice(void);

		// Item Menu : Create
		void OnCreateItem(CMetaItem *pItem);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);
		
		// Item Menu : Delete
		BOOL CanItemDelete(void);
		void OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);

		// Item Menu : Rename
		BOOL CanItemRename(void);
		BOOL OnItemRename(CString Name);
		void OnExecRename(CCmdRename *pCmd);
		void OnUndoRename(CCmdRename *pCmd);
		void RenameItem(CString Name, CString Prev);

		BOOL StepAway(void);

		// Tree Loading
		void      LoadTree(void);
		void      LoadRoot(void);
		HTREEITEM LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem);
		void      LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem);
		void      LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem);
		void      LoadList(HTREEITEM hRoot, CItemList *pList);
		HTREEITEM RefreshFrom(HTREEITEM hItem);

		// Item Mapping
		void AddToMap(CMetaItem *pItem, HTREEITEM hItem);
		void RemoveFromMap(CMetaItem *pItem);

		// Item Hooks
		void NewItemSelected(void);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		BOOL CheckItemName(CString const &Name);
		void KillItem(HTREEITEM hItem, BOOL fRemove);

		// Item Data
		CString GetItemList(CMetaItem *pItem);
		BOOL    HasVarImage(CMetaItem *pItem);

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Implementation
		void LockUpdate(BOOL fLock);
		void SkipUpdate(void);
		void ListUpdated(HTREEITEM hItem);
		BOOL Recompile(CLASS Class);
		BOOL Recompile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

class CEt3CommsTreeWnd::CCmdCreate : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdCreate(CItem *pRoot, CString List, CMetaItem *pItem);

		// Destructor
		~CCmdCreate(void);

		// Data Members
		CString m_Made;
		CString m_List;
		HGLOBAL m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

class CEt3CommsTreeWnd::CCmdDelete : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem);

		// Destructor
		~CCmdDelete(void);

		// Data Members
		CString m_Root;
		CString m_List;
		CString m_Next;
		HGLOBAL m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

class DLLAPI CEt3CommsTreeWnd::CCmdRename : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdRename(CMetaItem *pItem, CString Name);

		// Data Members
		CString m_Prev;
		CString m_Name;
	};

// End of File

#endif

