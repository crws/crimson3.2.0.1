
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SECURITY_HPP
	
#define	INCLUDE_SECURITY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEt3SecurityManager;
class CEt3UserList;
class CEt3User;

//////////////////////////////////////////////////////////////////////////
//
// Data Logger
//

class CEt3SecurityManager : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3SecurityManager(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT            m_Handle;
		UINT		m_Permissions;
		UINT		m_Download;
		CEt3UserList  * m_pUsers;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Config File Help
		UINT BuildWebOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

class CEt3UserList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3UserList(void);

		// Item Access
		CEt3User * GetItem(INDEX Index) const;
		CEt3User * GetItem(UINT uPos) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		void PrepareData(void);

		// Public Data
		UINT	m_uPtr;

	protected:
		// Data
		CEt3CommsSystem * m_pSystem;		
	};

//////////////////////////////////////////////////////////////////////////
//
// User
//

class CEt3User : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3User(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		
		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Data Members
		CString		m_Name;
		CString		m_Password;
		UINT		m_ViewIO;
		UINT		m_ModifyIO;
		UINT		m_ConfigTags;
		UINT		m_ConfigUsers;
		UINT		m_ConfigComms;
		UINT		m_Calibrate;
		UINT		m_LoadFW;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		// Config File Help
		UINT BuildPermissions(void);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// User
//

class CEt3UserNoLogin : public CEt3User
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3UserNoLogin(void);

		// Persistance
		void Init(void);

		// Download Support
		void PrepareData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEt3SecurityNavTreeWnd;

//////////////////////////////////////////////////////////////////////////
//
// Security Navigation Window
//

class CEt3SecurityNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CEt3SecurityNavTreeWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL CanItemCreate(void);
		void OnItemNew(void);

		// Notification Handlers
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void OnItemRenamed(CItem *pItem);

		// Item Locking
		BOOL IsItemLocked(HTREEITEM hItem);
	};
	
// End of File

#endif
