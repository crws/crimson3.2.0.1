
#include "intern.hpp"

#include "comtree.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications Tree View
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsTreeWnd, CStdTreeWnd);

// Constructors

CEt3CommsTreeWnd::CEt3CommsTreeWnd(void)
{
	m_dwStyle = m_dwStyle | TVS_HASBUTTONS;

	m_dwStyle = m_dwStyle | TVS_EDITLABELS | TVS_SHOWSELALWAYS;

	m_fMulti  = FALSE;

	m_uLocked = 0;

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"Et3Tree16"), afxColor(MAGENTA));
	
	m_Accel1.Create(L"NavTreeAccel");
	}

// IUnknown

HRESULT CEt3CommsTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
	}

ULONG CEt3CommsTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
	}

ULONG CEt3CommsTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
	}

// IUpdate

HRESULT CEt3CommsTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			RefreshFrom(hItem);
			}

		return S_OK;
		}

	if( uType == updateProps ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			if( HasVarImage(pMeta) ) {

				CTreeViewItem Node(hItem);

				LoadNodeItem(Node, pMeta);

				m_pTree->SetItem(Node);
				}
			}

		return S_OK;
		}

	if( uType == updateRename ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

				CEt3CommsPortSerial *pPort = (CEt3CommsPortSerial *) pItem;

				m_pTree->SetItemText(hItem, pPort->GetTreeLabel());
				}
			}

		return S_OK;
		}

	return S_OK;
	}

// Overridables

void CEt3CommsTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pComms  = (CEt3CommsManager *) m_pItem;

	m_pSystem = (CEt3CommsSystem  *) m_pItem->GetDatabase()->GetSystemItem();
	}

void CEt3CommsTreeWnd::OnExec(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		m_fMulti = !m_fMulti;

		LockUpdate(m_fMulti);
		}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		((CCmdItem *) pCmd)->Exec(m_pSelect);

		RefreshFrom(m_hSelect);

		ListUpdated(m_hSelect);

		//CheckItemCmd();
		}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnExecRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
		}

/*	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnExecPaste((CCmdPaste *) pCmd);
		}
*/	}

void CEt3CommsTreeWnd::OnUndo(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdMulti) ) {

		m_fMulti = !m_fMulti;

		LockUpdate(m_fMulti);
		}

	if( Class == AfxRuntimeClass(CCmdItem) ) {

		((CCmdItem *) pCmd)->Undo(m_pSelect);

		RefreshFrom(m_hSelect);

		ListUpdated(m_hSelect);

		//CheckItemCmd();
		}

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdRename) ) {

		OnUndoRename((CCmdRename *) pCmd);
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
		}
	}

// Message Map

AfxMessageMap(CEt3CommsTreeWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SHOWWINDOW)

	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit )
	AfxDispatchNotify(100, TVN_ENDLABELEDIT,   OnTreeEndEdit   )

	AfxDispatchGetInfoType(IDM_COMMS, OnCommsGetInfo)
	AfxDispatchControlType(IDM_COMMS, OnCommsControl)
	AfxDispatchCommandType(IDM_COMMS, OnCommsCommand)

	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)

	AfxDispatchGetInfoType(IDM_TRANSFER,  OnTransferGetInfo)
	AfxDispatchControlType(IDM_TRANSFER,  OnTransferControl)
	AfxDispatchCommandType(IDM_TRANSFER,  OnTransferCommand)

	AfxMessageEnd(CEt3CommsTreeWnd)
	};

// Message Handlers

void CEt3CommsTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
	}

void CEt3CommsTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"Et3CommsTreeTool"));
		}
	}

void CEt3CommsTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( fShow ) {

		m_System.SetNavCheckpoint();

		m_System.SetViewedItem(m_pSelect);
		}
	}

// Notification Handlers

BOOL CEt3CommsTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{
	return !CanItemRename();
	}

BOOL CEt3CommsTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
		}
	
	return FALSE;
	}

// Command Handlers

BOOL CEt3CommsTreeWnd::OnCommsGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_COMMS_ADD_DEVICE,  MAKELONG(0x0001, 0x4000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnCommsControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_COMMS_ADD_DEVICE:

			Src.EnableItem(CanCommsAddDevice());

			return TRUE;

		case IDM_COMMS_DEL_DEVICE:

			Src.EnableItem(CanCommsDelDevice());

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnCommsCommand(UINT uID)
{
	switch( uID ) {

		case IDM_COMMS_ADD_DEVICE:

			OnCommsAddDevice();

			return TRUE;

		case IDM_COMMS_DEL_DEVICE:

			OnCommsDelDevice();

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_ITEM_DELETE ) {

		Info.m_Image = MAKELONG(0x0008, 0x1000);
		}

	if( uID == IDM_ITEM_RENAME ) {

		Info.m_Image = MAKELONG(0x0021, 0x1000);
		}

	if( uID == IDM_ITEM_EXPAND ) {

		if( m_pTree->IsExpanded(m_hSelect) ) {

			Info.m_Image = MAKELONG(0x0020, 0x1000);
			}
		else
			Info.m_Image = MAKELONG(0x001F, 0x1000);
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	CString Label;

	switch( uID ) {

		case IDM_ITEM_DELETE:

			Src.EnableItem(CanItemDelete());

			return TRUE;

		case IDM_ITEM_RENAME:

			Src.EnableItem(CanItemRename());

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:

			OnItemDelete(IDS_DELETE_2, TRUE, TRUE);

			return TRUE;

		case IDM_ITEM_RENAME:

			m_pTree->EditLabel(m_hSelect);

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnTransferGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {
		
		case IDM_TRANSFER_NEW_DISCRETE:

			Info.m_Image   = 0x70000008;

			Info.m_ToolTip = CString(IDS_NEW_DISCRETE);

			Info.m_Prompt  = CString(IDS_ADD_NEW_DISCRETE);

			return TRUE;

		case IDM_TRANSFER_NEW_ANALOG:

			Info.m_Image   = 0x70000007;

			Info.m_ToolTip = CString(IDS_NEW_ANALOG);

			Info.m_Prompt  = CString(IDS_ADD_NEW_ANALOG);

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnTransferControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_TRANSFER_NEW_DISCRETE:
		case IDM_TRANSFER_NEW_ANALOG:

			Src.EnableItem(CanTransferNew());
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnTransferCommand(UINT uID)
{
	switch( uID ) {

		case IDM_TRANSFER_NEW_DISCRETE:

			OnCreateItem(New CTransferBlockDiscreteItem);

			return TRUE;

		case IDM_TRANSFER_NEW_ANALOG:

			OnCreateItem(New CTransferBlockAnalogItem);

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::CanTransferNew(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

			if( m_pSystem->GetTransfer() < 32 ) {

				return TRUE;
				}
			}
		}	

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::CanTransferDelete(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferBlockItem)) ) {
		
			return TRUE;
			}
		}

	return FALSE;
	}

// Comms Menu

BOOL CEt3CommsTreeWnd::CanCommsAddDevice(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::CanCommsDelDevice(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CEt3CommsTreeWnd::OnCommsAddDevice(void)
{
	OnCreateItem(New CEt3CommsDevice);
	}

void CEt3CommsTreeWnd::OnCommsDelDevice(void)
{
	OnItemDelete(IDS_DELETE_2, TRUE, TRUE);
	}

// Item Menu : Create

void CEt3CommsTreeWnd::OnCreateItem(CMetaItem *pItem)
{
	HTREEITEM        hRoot = m_hSelect;

	CMetaItem      * pRoot = m_pSelect;

	CString          List  = GetItemList(pRoot);

	CItemIndexList * pList = pRoot->FindIndexList(List);

	pList->AppendItem(pItem);

	m_System.SaveCmd(New CCmdCreate(pRoot, List, pItem));

	LockUpdate(TRUE);

	LoadItem(hRoot, NULL, pItem);

	ExpandItem(hRoot);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
	}

void CEt3CommsTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	pItem->SetParent(pList);

	pList->AppendItem(pItem);

	LockUpdate(TRUE);

	LoadItem(hRoot, NULL, pItem);

	ExpandItem(hRoot);

	LockUpdate(FALSE);

	ListUpdated(hRoot);
	}

void CEt3CommsTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Item];

	HTREEITEM        hItem = m_MapFixed[pCmd->m_Made];
	
	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	ListUpdated(hRoot);
	}

// Item Menu : Delete

BOOL CEt3CommsTreeWnd::CanItemDelete(void)
{
	if( CanCommsDelDevice() ) {
		
		return TRUE;
		}
	
	if( CanTransferDelete() ) {
		
		return TRUE;
		}
	
	return FALSE;
	}

void CEt3CommsTreeWnd::OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop)
{
	if( TRUE ) {

		CMetaItem      * pRoot = (CMetaItem *) m_pSelect->GetParent(2);

		CString          List  = GetItemList(pRoot);

		CItemIndexList * pList = pRoot->FindIndexList(List);

		INDEX            Index = pList->FindItemIndex(m_pSelect);

		CItem          * pNext = pList->GetNext(Index) ? pList->GetItem(Index) : NULL;

		CCmd           * pCmd  = New CCmdDelete(pRoot, List, pNext, m_pSelect);

		m_System.ExecCmd(pCmd);
		}
	}

void CEt3CommsTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];

	HTREEITEM        hItem = m_hSelect;
	
	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pItem = GetItemPtr(hItem);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CLASS            Class = AfxPointerClass(pItem);

	StepAway();

	pList->DeleteItem(pItem);

	KillTree(hItem, TRUE);

	ListUpdated(hRoot);

	Recompile(Class);
	}

void CEt3CommsTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	HTREEITEM        hRoot = m_MapFixed[pCmd->m_Root];
	
	HTREEITEM        hNext = m_MapFixed[pCmd->m_Next];

	CMetaItem      * pRoot = GetItemPtr(hRoot);

	CMetaItem      * pNext = GetItemOpt(hNext);

	CItemIndexList * pList = pRoot->FindIndexList(pCmd->m_List);

	CMetaItem      * pItem = (CMetaItem *) CItem::MakeFromSnapshot(pList, pCmd->m_hData);

	CLASS            Class = AfxPointerClass(pItem);

	pList->InsertItem(pItem, pNext);

	LockUpdate(TRUE);
	
	HTREEITEM hItem = LoadItem(hRoot, hNext, pItem);

	m_pTree->SelectItem(hItem);

	LockUpdate(FALSE);

	ListUpdated(hRoot);

	Recompile(Class);
	}

BOOL CEt3CommsTreeWnd::StepAway(void)
{
	HTREEITEM hItem;
	
	if( (hItem = m_pTree->GetNext(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	if( (hItem = m_pTree->GetPrevious(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	if( (hItem = m_pTree->GetParent(m_hSelect)) ) {

		m_pTree->SelectItem(hItem);

		return TRUE;
		}

	return FALSE;
	}

// Item Menu : Rename

BOOL CEt3CommsTreeWnd::CanItemRename(void)
{
	if( !IsReadOnly() ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

			return TRUE;
			}

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferBlockItem)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::OnItemRename(CString Name)
{
	CString Prev = m_pNamed->GetName();

	if( wstrcmp(Name, Prev) ) {

		if( !CheckItemName(Name) ) {

			CString Text = CFormat(CString(IDS_NAME_FMT_IS), Name);
		
			Error(Text);

			return FALSE;
			}

		if( m_pNamed->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
				}
			}

		CCmd *pCmd = New CCmdRename(m_pNamed, Name);

		m_System.ExecCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

void CEt3CommsTreeWnd::OnExecRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Name, pCmd->m_Prev);
	}

void CEt3CommsTreeWnd::OnUndoRename(CCmdRename *pCmd)
{
	RenameItem(pCmd->m_Prev, pCmd->m_Name);
	}

void CEt3CommsTreeWnd::RenameItem(CString Name, CString Prev)
{
	m_pNamed->SetName(Name);

	m_pNamed->SetDirty();

	m_pTree->SetItemText(m_hSelect, Name);

	if( m_pNamed->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		//Recompile();
		}

	m_System.ItemUpdated(m_pSelect, updateRename);
	}

// Tree Loading

void CEt3CommsTreeWnd::LoadTree(void)
{
	LoadRoot();

	LoadNodeKids(m_hRoot, m_pItem);

	SelectRoot();
	}

void CEt3CommsTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;
	
	LoadNodeItem(Node, m_pItem);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);

	AddToMap(m_pItem, m_hRoot);
	}

HTREEITEM CEt3CommsTreeWnd::LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem)
{
	CTreeViewItem Node;

	LoadNodeItem(Node, pItem);

	HTREEITEM hPrev = hNext ? m_pTree->GetPrevious(hNext) : TVI_LAST;

	HTREEITEM hUsed = hPrev ? hPrev : TVI_FIRST;

	HTREEITEM hItem = m_pTree->InsertItem(hRoot, hUsed, Node);

	AddToMap(pItem, hItem);

	LoadNodeKids(hItem, pItem);

	return hItem;
	}

void CEt3CommsTreeWnd::LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		Node.SetText(m_pItem->GetHumanName());

		Node.SetParam(LPARAM(m_pItem));

		Node.SetImages(IDI_COMMS);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {

		CEt3CommsPort *pPort = (CEt3CommsPort *) pItem;

		Node.SetText(pPort->GetTreeLabel());

		Node.SetParam(LPARAM(pPort));

		Node.SetImages(pPort->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		CEt3CommsDevice *pDevice = (CEt3CommsDevice *) pItem;

		Node.SetText(pDevice->m_Name);

		Node.SetParam(LPARAM(pDevice));

		Node.SetImages(pDevice->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CTransferBlockItem)) ) {

		CTransferBlockItem *pBlock= (CTransferBlockItem *) pItem;

		Node.SetText(pBlock->m_Name);

		Node.SetParam(LPARAM(pBlock));

		Node.SetImages(pBlock->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3EthernetItem)) ) {

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pItem));

		Node.SetImages(IDI_PORT_ETHERNET);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3Services)) ) {

		CEt3Services *pService = (CEt3Services *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pService));

		Node.SetImages(pService->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CWebServerItem)) ) {

		CWebServerItem *pService = (CWebServerItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pService));

		Node.SetImages(pService->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CWatchdogItem)) ) {

		CWatchdogItem *pService = (CWatchdogItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pService));

		Node.SetImages(pService->GetTreeImage());

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CHeartbeatItem)) ) {

		CHeartbeatItem *pService = (CHeartbeatItem *) pItem;

		Node.SetText(pItem->GetHumanName());

		Node.SetParam(LPARAM(pService));

		Node.SetImages(pService->GetTreeImage());

		return;
		}
	
	AfxAssert(FALSE);
	}

void CEt3CommsTreeWnd::LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem)
{
	if( pItem == m_pItem ) {

		CEt3CommsManager *pManager = (CEt3CommsManager *) pItem;

		LoadItem(hRoot, NULL, pManager->m_pEthernet);

		LoadList(hRoot, pManager->m_pPorts);

		LoadItem(hRoot, NULL, pManager->m_pServices);
		
		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {

		CEt3CommsPort *pPort = (CEt3CommsPort *) pItem;

		LoadList(hRoot, pPort->m_pDevices);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		CEt3CommsDevice *pDevice = (CEt3CommsDevice *) pItem;

		LoadList(hRoot, pDevice->m_pBlocks);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3EthernetItem)) ) {

		CEt3EthernetItem *pEth = (CEt3EthernetItem *) pItem;

		LoadList(hRoot, pEth->m_pPorts);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3Services)) ) {

		CEt3Services *pServices = (CEt3Services *) pItem;

		LoadList(hRoot, pServices->m_pList);

		m_pTree->Expand(hRoot, TVE_EXPAND);

		return;
		}
	}

void CEt3CommsTreeWnd::LoadList(HTREEITEM hRoot, CItemList *pList)
{
	for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

		CMetaItem *pItem = (CMetaItem *) pList->GetItem(n);

		if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {
			}

		LoadItem(hRoot, NULL, pItem);
		}
	}

HTREEITEM CEt3CommsTreeWnd::RefreshFrom(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		LockUpdate(TRUE);

		CString    State = SaveSelState();

		CMetaItem *pItem = GetItemPtr(hItem);

		HTREEITEM  hNext = m_pTree->GetNext(hItem);

		HTREEITEM  hRoot = m_pTree->GetParent(hItem);

		KillTree(hItem, TRUE);

		hItem = LoadItem(hRoot, hNext, pItem);

		ExpandItem(hRoot);

		ExpandItem(hItem);

		LoadSelState(State);

		SkipUpdate();

		return hItem;
		}

	m_fRefresh = TRUE;

	return hItem;
	}

// Item Mapping

void CEt3CommsTreeWnd::AddToMap(CMetaItem *pItem, HTREEITEM hItem)
{
	BOOL fTest = m_MapFixed.Insert(pItem->GetFixedPath(), hItem);

	AfxAssert(fTest);
	}

void CEt3CommsTreeWnd::RemoveFromMap(CMetaItem *pItem)
{
	m_MapFixed.Remove(pItem->GetFixedPath());
	}

// Item Hooks

void CEt3CommsTreeWnd::NewItemSelected(void)
{
	if( !m_uLocked ) {

		m_System.SetViewedItem(m_pSelect);
		}

	CString Text;

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		CEt3CommsDevice *pDevice = (CEt3CommsDevice *) m_pSelect;

		CEt3CommsPort     *pPort = pDevice->GetParentPort();

		Text = CPrintf(L"Port Number %d, Device Number %d", pPort->m_Number, pDevice->m_Number);
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {

		CEt3CommsPort *pPort = (CEt3CommsPort *) m_pSelect;

		Text = CPrintf(L"Port Number %d", pPort->m_Number);
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferBlockDiscreteItem)) ) {

		Text = CString(IDS_DISCRETE_TRANSFER);

		Text += CPrintf(CString(IDS_OF_FMT), m_pSystem->GetTransfer());
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferBlockAnalogItem)) ) {

		Text = CString(IDS_ANALOG_TRANSFER);

		Text += CPrintf(CString(IDS_OF_FMT), m_pSystem->GetTransfer());
		}

	afxThread->SetStatusText(Text);
	}

BOOL CEt3CommsTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"Et3CommsTreeCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

BOOL CEt3CommsTreeWnd::CheckItemName(CString const &Name)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		CEt3CommsPortList *pList;

		for( UINT s = 0; m_pComms->GetPortList(pList, s); s++  ) {

			INDEX Index = pList->GetHead();

			while( !pList->Failed(Index) ) {

				CEt3CommsPort *pPort = pList->GetItem(Index);

				CItem      *pItem = pPort->m_pDevices->GetItem(Name);

				if( pItem ) {

					if( pItem == m_pSelect ) {

						return TRUE;
						}

					return FALSE;
					}

				pList->GetNext(Index);
				}
			}

		return TRUE;
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferBlockItem)) ) {

		CTransferBlockItem *pBlock = (CTransferBlockItem *) m_pSelect;

		CItem      *pItem;

		if( (pItem = pBlock->GetParentList()->GetItem(Name)) ) {

			if( pItem == m_pSelect ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

void CEt3CommsTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	if( fRemove ) {

		if( hItem == m_hRoot ) {

			m_MapFixed.Empty();

			return;
			}

		// LATER -- This is very expensive, but we can't
		// get the item's fixed name as the item may well
		// have been deleted by this point. We could use
		// another index to speed this up if required...

		INDEX Index = m_MapFixed.FindData(hItem);

		if( !m_MapFixed.Failed(Index) ) {

			m_MapFixed.Remove(Index);

			return;
			}

		/*AfxAssert(FALSE);*/
		}
	}

// Item Data

CString CEt3CommsTreeWnd::GetItemList(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {

		return L"Devices";
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		return L"Blocks";
		}
	
	return L"";
	}

BOOL CEt3CommsTreeWnd::HasVarImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CWebServerItem)) ) {

		return TRUE;
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CTransferBlockItem)) ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CEt3CommsTreeWnd::LockUpdate(BOOL fLock)
{
	if( fLock ) {

		if( !m_uLocked++ ) {

			SetRedraw(FALSE);

			m_fRefresh = FALSE;
			}

		return;
		}

	if( !--m_uLocked ) {

		if( m_fRefresh ) {

			RefreshTree();
			}
		else
			SetRedraw(TRUE);

		m_System.SetViewedItem(m_pSelect);

		m_System.SetViewedItem(m_pSelect);

		m_System.ItemUpdated(m_pSelect, updateChildren);

		m_pTree->UpdateWindow();
		}
	}

void CEt3CommsTreeWnd::SkipUpdate(void)
{
	if( !--m_uLocked ) {

		SetRedraw(TRUE);

		m_pTree->UpdateWindow();
		}
	}

void CEt3CommsTreeWnd::ListUpdated(HTREEITEM hItem)
{
	if( !m_uLocked ) {

		// TODO -- Which should this be?

		CMetaItem *pItem = GetItemPtr(hItem);

		m_System.ItemUpdated(pItem, updateChildren);

		m_System.ItemUpdated(pItem, updateContents);
		}
	}

BOOL CEt3CommsTreeWnd::Recompile(CLASS Class)
{
/*	if( Class->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		return Recompile();
		}
*/
	return FALSE;
	}

// Selection Hooks

CString CEt3CommsTreeWnd::SaveSelState(void)
{
	// LATER -- Encode expand state of children?

	return m_SelPath;
	}

void CEt3CommsTreeWnd::LoadSelState(CString State)
{
	HTREEITEM hItem = m_MapFixed[State];

	if( hItem ) {

		m_pTree->SelectItem(hItem);

		return;
		}

	SelectRoot();
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsTreeWnd::CCmdCreate, CStdCmd);

// Constructor

CEt3CommsTreeWnd::CCmdCreate::CCmdCreate(CItem *pRoot, CString List, CMetaItem *pItem)
{
	m_Menu  = CFormat(CString(IDS_CREATE), pItem->GetName());

	m_Item  = pRoot->GetFixedPath();

	m_List  = List;

	m_Made  = pItem->GetFixedPath();

	m_hData = pItem->TakeSnapshot();
	}

// Destructor

CEt3CommsTreeWnd::CCmdCreate::~CCmdCreate(void)
{
	GlobalFree(m_hData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsTreeWnd::CCmdDelete, CStdCmd);

// Constructor

CEt3CommsTreeWnd::CCmdDelete::CCmdDelete(CItem *pRoot, CString List, CItem *pNext, CMetaItem *pItem)
{
	m_Menu  = CFormat(CString(IDS_DELETE_3), pItem->GetName());

	m_Item  = pItem->GetFixedPath();

	m_Root  = GetFixedPath(pRoot);

	m_List  = List;

	m_Next  = GetFixedPath(pNext);

	m_hData = pItem->TakeSnapshot();
	}

// Destructor

CEt3CommsTreeWnd::CCmdDelete::~CCmdDelete(void)
{
	GlobalFree(m_hData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsTreeWnd::CCmdRename, CStdCmd);

// Constructor

CEt3CommsTreeWnd::CCmdRename::CCmdRename(CMetaItem *pItem, CString Name)
{
	m_Menu = CFormat(CString(IDS_RENAME_FMT), pItem->GetName());

	m_Item = pItem->GetFixedPath();

	m_Prev = pItem->GetName();

	m_Name = Name;
	}

// End of File

