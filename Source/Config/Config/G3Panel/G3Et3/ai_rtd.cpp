
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

class CAnalogInputRTDPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAnalogInputRTDPage(CAnalogInputBaseItem *pInput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAnalogInputBaseItem  * m_pInput;
		CEt3CommsSystem       * m_pSystem;
		BOOL			m_fFeature1;

		// Implementation
		void LoadTemperature(IUICreate *pView);
		void LoadChannels(IUICreate *pView);
		void AddRangeUI   (IUICreate *pView, UINT uChan);
		void AddFeature1UI(IUICreate *pView, UINT uChan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

// Runtime Class

AfxImplementRuntimeClass(CAnalogInputRTDPage, CUIStdPage);

// Constructor

CAnalogInputRTDPage::CAnalogInputRTDPage(CAnalogInputBaseItem *pInput)
{
	m_Class   = AfxRuntimeClass(CAnalogInputBaseItem);	

	m_pInput  = pInput;

	m_fFeature1 = TRUE;

	m_pSystem = (CEt3CommsSystem *) pInput->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CAnalogInputRTDPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadTemperature(pView);

	LoadChannels(pView);

	return FALSE;
	}

// Implementation

void CAnalogInputRTDPage::LoadTemperature(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAITemperature") ) {

		pView->StartGroup(CString(IDS_TEMPERATURE), 1);

		pView->AddUI(m_pInput, L"root", L"TempUnits");

		pView->AddUI(m_pInput, L"root", L"TempFormat");
	
		pView->EndGroup(TRUE);
		}
	}

void CAnalogInputRTDPage::LoadChannels(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAIs()) ) {

		UINT uCols = 0;

		if( TRUE ) {
			
			uCols ++;
			}

		if( m_fFeature1 ) {
			
			uCols ++;
			}

		pView->StartTable(CString(IDS_CHANNELS), uCols);

		if( TRUE ) {
			
			pView->AddColHead(CString(IDS_RANGE));
			}

		if( m_fFeature1 ) {
			
			pView->AddColHead(CString(IDS_TYPE));
			}

		for( UINT n = 0; n < uCount; n ++ ) {

			pView->AddRowHead(CPrintf(L"AI%d", 1 + n));

			AddRangeUI   (pView, n);

			AddFeature1UI(pView, n);
			}

		pView->EndTable();
		}
	}

void CAnalogInputRTDPage::AddRangeUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "1|European Curve";
			Form += "|";	Form += "2|100 Ohm Platinum";
			Form += "|";	Form += "4|10 Ohm Copper";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Range%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Range", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputRTDPage::AddFeature1UI(IUICreate *pView, UINT uChan)
{
	if( m_fFeature1 ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "-1|Disabled";
			Form += "|";	Form += "0|alpha=0.00385";
			Form += "|";	Form += "1|alpha=0.00392";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Feature1%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature1", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogInputRTDItem, CAnalogInputBaseItem);

// Constructor

CAnalogInputRTDItem::CAnalogInputRTDItem(void)
{
	m_uType	= typeRTD;
	}

// UI Creation

BOOL CAnalogInputRTDItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CAnalogInputRTDPage(this));

	return FALSE;
	}

// UI Update

void CAnalogInputRTDItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		for( UINT n = 0; n < elements(m_Range); n ++ ) {

			LimitEnum(pHost, CPrintf(L"Range%2.2d", 1+n), m_Range[n], 0x02);
			}
		
		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Range") ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistence

void CAnalogInputRTDItem::Init(void)
{
	CAnalogInputBaseItem::Init();

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		m_Range[n] = 2;
		}

	for( UINT n = 0; n < elements(m_Feature1); n ++ ) {

		m_Feature1[n] = 0;
		}
	}

// Download Support

void CAnalogInputRTDItem::PrepareData(void)
{
	CAnalogInputBaseItem::PrepareData();

	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysAIs();

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CAnalogInput &AI = File.m_AIs[n];

		UINT uRange = 0;

		if( m_Feature1[n] != -1 ) {
			
			uRange |= (m_uType << 4);
		
			uRange |= m_Range[n] + m_Feature1[n];
			}

		AI.SetRange(uRange);
		}
	}

// Implementation

void CAnalogInputRTDItem::DoEnables(IUIHost *pHost)
{
	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		BOOL fEnable = m_Range[n] != -1;

		pHost->EnableUI(CPrintf(L"Feature1%2.2d", 1+n), fEnable);
		}
	}

// End of File
