
#include "intern.hpp"

#include "security.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Security Navigation Window
//
	
// Dynamic Class

AfxImplementDynamicClass(CEt3SecurityNavTreeWnd, CNavTreeWnd);
		
// Constructor

CEt3SecurityNavTreeWnd::CEt3SecurityNavTreeWnd(void) : CNavTreeWnd( L"Users",
						       NULL,
						       AfxRuntimeClass(CEt3User)
						       )
{
	}

// Message Map

AfxMessageMap(CEt3SecurityNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit)

	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)

	AfxMessageEnd(CEt3SecurityNavTreeWnd)
	};

// Message Handlers

void CEt3SecurityNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"SecurityNavTreeTool"));
		}
	}

// Notification Handlers

BOOL CEt3SecurityNavTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{	
	if( m_pSelect->IsKindOf((AfxRuntimeClass(CEt3UserNoLogin))) ) {
		
		return !CNavTreeWnd::OnTreeBeginEdit(uID, Info);
		}

	return CNavTreeWnd::OnTreeBeginEdit(uID, Info);
	}

// Command Handlers

BOOL CEt3SecurityNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Info.m_Image   = 0x40000007;

			Info.m_ToolTip = CString("New User");

			Info.m_Prompt  = CString("Add a new User.");

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3SecurityNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Src.EnableItem(CanItemCreate());

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3SecurityNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			OnItemNew();

			return TRUE;
		}

	return FALSE;
	}

BOOL CEt3SecurityNavTreeWnd::CanItemCreate(void)
{
	if( !IsReadOnly() ) {
		
		return m_pList->GetItemCount() < 8;
		}

	return FALSE;
	}

void CEt3SecurityNavTreeWnd::OnItemNew(void)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(CString(IDS_FORMAT_4), n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

// Tree Loading

void CEt3SecurityNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"Et3Tree16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CEt3SecurityNavTreeWnd::GetRootImage(void)
{
	return IDI_SECURITY;
	}

UINT CEt3SecurityNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return IDI_USER;
	}

BOOL CEt3SecurityNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"SecurityNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"SecurityNavTreeMissCtxMenu";

	return FALSE;
	}

void CEt3SecurityNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	CNavTreeWnd::OnItemRenamed(pItem);
	}

// Item Locking

BOOL CEt3SecurityNavTreeWnd::IsItemLocked(HTREEITEM hItem)
{
	if( GetItemPtr(hItem)->IsKindOf((AfxRuntimeClass(CEt3UserNoLogin))) ) {

		return TRUE;
		}

	return CNavTreeWnd::IsItemLocked(hItem);
	}

//////////////////////////////////////////////////////////////////////////
//
// Security Manager
//

// Dynamic Class

AfxImplementDynamicClass(CEt3SecurityManager, CEt3UIItem);

// Constructor

CEt3SecurityManager::CEt3SecurityManager(void)
{
	m_Handle      = HANDLE_NONE;

	m_Permissions = 1;

	m_Download    = 1;

	m_pUsers      = New CEt3UserList;
	}

// UI Creation

CViewWnd * CEt3SecurityManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CEt3SecurityNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return CUIItem::CreateView(uType);
	}

// Item Naming

CString CEt3SecurityManager::GetHumanName(void) const
{
	return CString(IDS_SECURITY_MANAGER);
	}

// Download Support

void CEt3SecurityManager::PrepareData(void)
{
	CEt3UIItem::PrepareData();

	CFileDataBaseCfgComms &File = m_pSystem->m_CfgComms;
	
	File.SetWebOptions(BuildWebOptions());
	}

// Meta Data Creation

void CEt3SecurityManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddCollect(Users);
	Meta_AddInteger(Permissions);

	Meta_SetName((IDS_SECURITY_MANAGER));
	}

// Config File Help

UINT CEt3SecurityManager::BuildWebOptions(void)
{
	CFileDataBaseCfgComms &File = m_pSystem->m_CfgComms;

	UINT uData = File.GetWebOptions();

	SetBit(uData, m_Permissions > 0, 2);
	SetBit(uData, m_Download    > 0, 1);	

	return uData;
	}

/////////////////////////////////////////////////////////////////////////
//
// Security List
//

// Dynamic Class

AfxImplementDynamicClass(CEt3UserList, CNamedList);

// Constructor

CEt3UserList::CEt3UserList(void)
{
	}

// Item Access

CEt3User * CEt3UserList::GetItem(INDEX Index) const
{
	return (CEt3User *) CNamedList::GetItem(Index);
	}

CEt3User * CEt3UserList::GetItem(UINT uPos) const
{
	return (CEt3User *) CNamedList::GetItem(uPos);
	}

// Persistance

void CEt3UserList::Init(void)
{
	CNamedList::Init();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));

	CMetaItem *pItem = New CEt3UserNoLogin;

	AppendItem(pItem);

	pItem->SetName(L"No Login Required");
	}

void CEt3UserList::PostLoad(void)
{
	CNamedList::PostLoad();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

// Download Support

void CEt3UserList::PrepareData(void)
{
	CFileDataBaseCfgUsers &File = m_pSystem->m_CfgUsers;

	File.ClearUsers();

	CNamedList::PrepareData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Security Item
//

// Dynamic Class

AfxImplementDynamicClass(CEt3User, CEt3UIItem);

// Constructor

CEt3User::CEt3User(void)
{
	}

// UI Update

void CEt3User::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Item Naming

CString CEt3User::GetHumanName(void) const
{
	return m_Name;
	}

// Persistance

void CEt3User::Init(void)
{
	CEt3UIItem::Init();

	m_Password	= L"password";

	m_ViewIO	= 0;

	m_ModifyIO	= 0;
	
	m_ConfigTags	= 0;

	m_ConfigUsers	= 0;

	m_ConfigComms	= 0;

	m_Calibrate	= 0;

	m_LoadFW	= 0;
	}

// Download Support

void CEt3User::PrepareData(void)
{
	CEt3UserList                *pList = (CEt3UserList *) GetParent();

	UINT                          uPos = pList->FindItemPos(this);

	CFileDataBaseCfgUsers        &File = m_pSystem->m_CfgUsers;

	CFileDataBaseCfgUsers::CUser &User = File.m_Users[uPos];

	AfxAssert(uPos > 0);

	User.SetUsername   (m_Name);

	User.SetPassword   (CCrypt(m_Name, m_Password).AsText());

	User.SetPermissions(BuildPermissions());

	User.SetOptions    (0x2B);
	}

// Meta Data Creation

void CEt3User::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddString (Password);
	Meta_AddInteger(ViewIO);
	Meta_AddInteger(ModifyIO);
	Meta_AddInteger(ConfigTags);
	Meta_AddInteger(ConfigUsers);
	Meta_AddInteger(ConfigComms);
	Meta_AddInteger(Calibrate);
	Meta_AddInteger(LoadFW);
	}

// Implementation

void CEt3User::DoEnables(IUIHost *pHost)
{
	}

// Config File Help

UINT CEt3User::BuildPermissions(void)
{
	UINT uData = 0;

	SetBit(uData, m_ViewIO      > 0, 0);
	SetBit(uData, m_ModifyIO    > 0, 1);
	SetBit(uData, m_ConfigTags  > 0, 2);
	SetBit(uData, m_ConfigUsers > 0, 3);
	SetBit(uData, m_ConfigComms > 0, 4);
	SetBit(uData, m_Calibrate   > 0, 5);
	SetBit(uData, m_LoadFW      > 0, 6);

	return uData;
	}

//////////////////////////////////////////////////////////////////////////
//
// Security Item
//

// Dynamic Class

AfxImplementDynamicClass(CEt3UserNoLogin, CEt3User);

// Constructor

CEt3UserNoLogin::CEt3UserNoLogin(void)
{
	}

// Persistance

void CEt3UserNoLogin::Init(void)
{
	CEt3UIItem::Init();

	m_ViewIO	= 1;

	m_ModifyIO	= 1;
	
	m_ConfigTags	= 1;

	m_ConfigUsers	= 1;

	m_ConfigComms	= 1;

	m_Calibrate	= 1;

	m_LoadFW	= 1;
	}

// Download Support

void CEt3UserNoLogin::PrepareData(void)
{
	CEt3UserList                *pList = (CEt3UserList *) GetParent();

	UINT                          uPos = pList->FindItemPos(this);

	CFileDataBaseCfgUsers        &File = m_pSystem->m_CfgUsers;

	CFileDataBaseCfgUsers::CUser &User = File.m_Users[uPos];

	AfxAssert(uPos == 0);

	User.SetUsername   (m_Name);

	User.SetPassword   (L"");

	User.SetPermissions(BuildPermissions());

	User.SetOptions    (0x2B);
	}

// End of File
