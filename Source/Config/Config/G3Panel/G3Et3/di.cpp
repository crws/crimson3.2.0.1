
#include "intern.hpp"

#include "io.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

class CDiscreteInputPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDiscreteInputPage(CDiscreteInputItem *pInput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CDiscreteInputItem * m_pInput;
		CEt3CommsSystem    * m_pSystem;

		// Implementation
		void LoadOptions(IUICreate *pView);
		void LoadChannel(IUICreate *pView);
		void AddFeatureUI(IUICreate *pView, UINT uChan);
		void AddTimebaseUI(IUICreate *pView, UINT uChan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

// Runtime Class

AfxImplementRuntimeClass(CDiscreteInputPage, CUIStdPage);

// Constructor

CDiscreteInputPage::CDiscreteInputPage(CDiscreteInputItem *pInput)
{
	m_Class   = AfxRuntimeClass(CDiscreteInputItem);	

	m_pInput  = pInput;

	m_pSystem = (CEt3CommsSystem *) pInput->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CDiscreteInputPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadOptions(pView);

	LoadChannel(pView);

	return TRUE;
	}

// Implementation

void CDiscreteInputPage::LoadOptions(IUICreate *pView)
{
	pView->StartGroup(L"Options", 1);

	if( m_pSystem->HasFlag(L"HasDISourceSink") ) {

		pView->AddUI(m_pInput, L"root", L"SourceSink");
		}

	if( m_pSystem->HasFlag(L"HasDIFiltering") ) {

		pView->AddUI(m_pInput, L"root", L"Filter");
		}

	if( m_pSystem->HasFlag(L"HasDICounters") ) {

		pView->AddUI(m_pInput, L"root", L"Counter");
		}

	pView->EndGroup(TRUE);
	}

void CDiscreteInputPage::LoadChannel(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetNumCounters()) ) {

		UINT uCols = 0;

		if( TRUE ) {
			
			uCols ++;
			}

		if( m_pInput->m_fTimebase ) {

			uCols ++;
			}

		pView->StartTable(CString(IDS_CHANNELS), uCols);

		if( TRUE ) {

			pView->AddColHead(CString(IDS_FEATURE_3));
			}

		if( m_pInput->m_fTimebase ) {

			pView->AddColHead(CString(IDS_TIME_BASES));
			}
		
		for( UINT n = 0; n < uCount; n ++ ) {

			pView->AddRowHead(CPrintf(L"DI%d", 1 + n));

			AddFeatureUI(pView, n);

			AddTimebaseUI(pView, n);
			}

		pView->EndTable();
		}
	}

void CDiscreteInputPage::AddFeatureUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		Form += L"";	Form += L"0|Up Counter";

		if( uChan < m_pSystem->GetNumHiSpeedCounters() ) {
				
			Form += L"|";	Form += L"1|High Speed Counter";
			}					

		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Channel%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Mode", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_SELECT_COUNTER);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CDiscreteInputPage::AddTimebaseUI(IUICreate *pView, UINT uChan)
{
	if( m_pInput->m_fTimebase ) {

		CString Form;

		if( TRUE ) {

			Form += L"";	Form += L"-1|Disabled";
			Form += L"|";	Form += L"1|Run time Seconds";
			Form += L"|";	Form += L"2|Run time Minutes";
			Form += L"|";	Form += L"100000|Rate 0.1 Seconds";
			Form += L"|";	Form += L"200000|Rate 0.2 Seconds";
			Form += L"|";	Form += L"500000|Rate 0.5 Seconds";
			Form += L"|";	Form += L"1000000|Rate 1 Second";
			Form += L"|";	Form += L"2000000|Rate 2 Second";
			Form += L"|";	Form += L"5000000|Rate 5 Second";
			Form += L"|";	Form += L"10000000|Rate 10 Second";
			Form += L"|";	Form += L"30000000|Rate 30 Second";
			Form += L"|";	Form += L"60000000|Rate 1 Minute";
			Form += L"|";	Form += L"3|ON Pulse Width";
			Form += L"|";	Form += L"4|OFF Pulse Width";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Timebase%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Time Base %d", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_SELECT_TIMEBASE);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Discrete Input
//

// Dynamic Class

AfxImplementDynamicClass(CDiscreteInputItem, CIOItem);

// Constructor

CDiscreteInputItem::CDiscreteInputItem(void)
{
	m_fTimebase = TRUE;
	}

// Overridables

UINT CDiscreteInputItem::GetTreeImage(void)
{
	return IDI_DI;
	}

// UI Creation

BOOL CDiscreteInputItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CDiscreteInputPage(this));

	return FALSE;
	}

// UI Update

void CDiscreteInputItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Counter" ) {

		if( m_Counter == 0 ) {

			for( UINT n = 0; n < elements(m_Timebase); n ++ ) {

				m_Timebase[n] = UINT(-1);

				pHost->UpdateUI(this, CPrintf("Timebase%2.2d", 1 + n));
				}

			for( UINT n = 0; n < elements(m_Channel); n ++ ) {

				m_Channel[n] = 0;

				pHost->UpdateUI(this, CPrintf("Channel%2.2d", 1 + n));
				}
			}

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CDiscreteInputItem::Init(void)
{
	CIOItem::Init();

	m_fIs16DIAC  	=  E3_MOD_16DIAC == m_pSystem->GetModuleIdent();

	if( m_fIs16DIAC ) {

		m_Filter	= 1;
		}
	else {
		m_Filter	= 0;
		}

	m_Counter	= 0;

	m_SourceSink	= 2;

	for( UINT n = 0; n < elements(m_Channel); n ++ ) {
		
		m_Channel[n] = 0;
		}

	for( UINT n = 0; n < elements(m_Timebase); n ++ ) {

		m_Timebase[n] = UINT(-1);
		}
	}

void CDiscreteInputItem::PostLoad(void)
{
	CIOItem::PostLoad();
	}

// Download Support

void CDiscreteInputItem::PrepareData(void)
{
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysDIs();	

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CDiscreteInput &DI = File.m_DIs[n];

		UINT m;

		if( n < (m = m_pSystem->GetNumCounters()) ) {

			UINT uData = 0;

			if( m_Counter ) {

				if( m_Filter ) {

					// slow

					if( m_Channel[n] ) {
					
						// hi speed

						uData = 0x04;
						}
					else {		
						// up counter

						uData = 0x05;
						}
					}
				else {
					// fast

					if( m_Channel[n] ) {
					
						// hi speed

						uData = 0x04;
						}
					else {		
						// up counter

						uData = 0x07;
						}
					}
				}
			else {
				if( m_Filter ) {

					// slow

					uData = 0x01;
					}
				else {
					// fast

					uData = 0x03;
					}				
				}

			SetBit(uData, m_Counter == 2, 3);

			if( TRUE ) {

				DWORD dwTime;

				if( (dwTime = m_Timebase[n]) != -1 ) {

					switch( dwTime ) {

						case 1:
						case 2:
							uData |= 0x50;
							break;

						case 3:
							uData |= 0x70;
							break;

						case 4:
							uData |= 0xF0;
							break;

						default:
							uData |= 0x20;
							break;
						}
					}
				}

			DI.SetConfig(uData);
			}
		else {
			}
		}

	if( TRUE ) {

		BYTE Data[8] = { 0 };

		for( UINT n = 0; n < elements(Data); n ++ ) {

			Data[n] = BYTE(m_SourceSink);
			}

		File.SetModuleSourceSinkJumper(Data);
		}

	if( m_fTimebase ) {

		UINT c = m_pSystem->GetPhysDIs();	

		for( UINT n = 0; n < c; n ++ ) {

			CFileDataBaseCfgSetup::CTimebase    &Base = File.m_Timebases[n];
			
			DWORD dwTime;

			if( (dwTime = m_Timebase[n]) != -1 ) {

				switch( dwTime ) {

					case 1:
						dwTime = 1000000;
						break;

					case 2:
						dwTime = 60000000;
						break;

					case 3:
						dwTime = 1000000;
						break;

					case 4:
						dwTime = 10000000;
						break;
					}
				
				Base.SetTime(dwTime);
				}
			else {
				Base.SetTime(0);
				}
			}
		}
	}

// Meta Data Creation

void CDiscreteInputItem::AddMetaData(void)
{
	CIOItem::AddMetaData();

	Meta_AddInteger(SourceSink);
	Meta_AddInteger(Filter);
	Meta_AddInteger(Counter);

	for( UINT n = 0; n < elements(m_Channel); n ++ ) {

		UINT uOffset = (PBYTE(m_Channel + n) - PBYTE(this));

		AddMeta( CPrintf("Channel%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Timebase); n ++ ) {

		UINT uOffset = (PBYTE(m_Timebase + n) - PBYTE(this));

		AddMeta( CPrintf("Timebase%2.2d", 1 + n), metaInteger, uOffset );
		}
	}

// Implementation

void CDiscreteInputItem::DoEnables(IUIHost *pHost)
{
	BOOL fCount32 = m_Counter == 2;

	BOOL fCount16 = m_Counter == 1;

	UINT   uCount = m_pSystem->GetNumCounters() / (fCount32 ? 2 : 1);

	for( UINT n = 0; n < elements(m_Channel); n ++ ) {

		BOOL  fChannel = fCount16 || fCount32;

		BOOL fTimebase = n < uCount;

		pHost->EnableUI(CPrintf("Channel%2.2d",  1 + n), fChannel);

		pHost->EnableUI(CPrintf("Timebase%2.2d", 1 + n), fChannel && fTimebase);
		}
	}

CAnalogInputBaseItem * CDiscreteInputItem::FindAI(void)
{
	return (CAnalogInputBaseItem *) m_pManager->m_pIOs->FindItem(AfxRuntimeClass(CAnalogInputBaseItem));
	}

// End of File
