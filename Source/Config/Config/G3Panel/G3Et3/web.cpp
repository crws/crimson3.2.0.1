
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// WebServer Item
//

// Dynamic Class

AfxImplementDynamicClass(CWebServerItem, CEt3UIItem);

// Constructor

CWebServerItem::CWebServerItem(void)
{
	m_Enable	= 1;

	m_Port		= 80;
	}

// UI Update

void CWebServerItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pHost);

		return;
		}

	if( Tag == L"Enable" ) {

		DoEnables(pHost);

		return;
		}
	}

// Attributes

UINT CWebServerItem::GetTreeImage(void) const
{
	return IDI_WEBSERVER;
	}

// Download Support

void CWebServerItem::PrepareData(void)
{
	CFileDataBaseCfgComms &File = m_pSystem->m_CfgComms;

	File.SetWebOptions(BuildWebOptions());

	File.SetWebPort   (m_Enable ? m_Port : 0);
	}

// Meta Data Creation

void CWebServerItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddInteger(Port);

	Meta_SetName((IDS_WEBSERVER));
	}

// Implementation

void CWebServerItem::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(L"Port", m_Enable > 0);
	}

// Config File Help

UINT CWebServerItem::BuildWebOptions(void)
{
	CFileDataBaseCfgComms &File = m_pSystem->m_CfgComms;

	UINT uData = File.GetWebOptions();
	
	SetBit(uData, m_Enable      > 0, 0);

	return uData;
	}

// End of File
