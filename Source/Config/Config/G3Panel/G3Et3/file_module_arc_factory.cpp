
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/module0/arc/factory"
//

// Constructor

CFileDataModuleArcFactory::CFileDataModuleArcFactory(void) : CFileData("/module0/arc/factory")
{	
	m_pData	= m_Data;

	m_uSize = elements(m_Data);
	}

// Attributes

UINT CFileDataModuleArcFactory::GetSerialNumber(void)
{
	UINT uPtr = 0x0010;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetType(void)
{
	UINT uPtr = 0x0010;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetRevision(void)
{
	UINT uPtr = 0x0016;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetSMIDNumber(void)
{
	UINT uPtr = 0x0018;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetFirmwareRevision(void)
{
	UINT uPtr = 0x001E;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetFirmwareLength(void)
{
	UINT uPtr = 0x0040;

	return GetLong(uPtr);
	}

UINT CFileDataModuleArcFactory::GetFirmwareChecksum(void)
{
	UINT uPtr = 0x0044;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetBootloaderVersion(void)
{
	UINT uPtr = 0x0046;

	return GetWord(uPtr);
	}

UINT CFileDataModuleArcFactory::GetProgrammableLogicVersion(void)
{
	UINT uPtr = 0x0048;

	return GetWord(uPtr);
	}

// End of File
