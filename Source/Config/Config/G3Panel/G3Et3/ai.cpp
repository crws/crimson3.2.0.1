
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

class CAnalogInputPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAnalogInputPage(CAnalogInputItem *pInput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAnalogInputItem    * m_pInput;
		CEt3CommsSystem     * m_pSystem;

		// Implementation
		void LoadIntegration(IUICreate *pView);
		void LoadTemperature(IUICreate *pView);
		void LoadChannels   (IUICreate *pView);
		void AddRangeUI(IUICreate *pView, UINT uChan);
		void AddScaleUI(IUICreate *pView, UINT uChan);
		void AddResolutionUI(IUICreate *pView, UINT uChan);
		void AddFeatureUI(IUICreate *pView, UINT uChan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

// Runtime Class

AfxImplementRuntimeClass(CAnalogInputPage, CUIStdPage);

// Constructor

CAnalogInputPage::CAnalogInputPage(CAnalogInputItem *pInput)
{
	m_Class   = AfxRuntimeClass(CAnalogInputItem);	

	m_pInput  = pInput;

	m_pSystem = (CEt3CommsSystem *) pInput->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CAnalogInputPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);
	
	LoadIntegration(pView);

	LoadTemperature(pView);

	LoadChannels(pView);

	return FALSE;
	}

// Implementation

void CAnalogInputPage::LoadIntegration(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAIIntegration") ) {

		CString Form;
		
		if( m_pSystem->HasFlag(L"HasTCType") ) {

			Form += L"";	Form += "128|200ms per channel";
			Form += L"|";	Form += "129|100ms per channel";
			Form += L"|";	Form += "130|50ms per channel";
			Form += L"|";	Form += "131|25ms per channel";
			Form += L"|";	Form += "132|12.5ms per channel";
			Form += L"|";	Form += "133|6.25ms per channel";
			Form += L"|";	Form += "134|3.12s per channel";
			Form += L"|";	Form += "135|1.5ms per channel";
			Form += L"|";	Form += "136|1ms per channel";
			Form += L"|";	Form += "137|0.5ms per channel";
			}
		else {
			Form += L"";	Form += L"0|Standard 50/60 Hz (21ms per channel)";
			Form += L"|";	Form += L"16|Best50 Hz (21ms per channel)";
			Form += L"|";	Form += L"32|Best 60 Hz (110ms per channel)";
			Form += L"|";	Form += L"80|Fast 50 Hz (200ms per channel)";
			Form += L"|";	Form += L"96|Fast 60 Hz (175ms per channel)";
			}

		pView->StartGroup(CString(IDS_OPTIONS), 1);

		CUIData Data;

		Data.m_Tag	 = "Integration";

		Data.m_Label 	 = CString(IDS_INTEGRATION);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);

		pView->EndGroup(TRUE);	
		}
	}

void CAnalogInputPage::LoadTemperature(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAITemperature") ) {

		pView->StartGroup(CString(IDS_TEMPERATURE), 1);

		pView->AddUI(m_pInput, L"root", L"TempUnits");

		if( TRUE ) {

			CString Form;

			Form += L"";	Form += L"One Degree";

			Form += L"|";	Form += L"Tenth Degree";
		
			//

			CUIData Data;

			Data.m_Tag	 = "TempFormat";

			Data.m_Label 	 = CString(IDS_REPORTING);

			Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

			Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

			Data.m_Format	 = Form;

			Data.m_Tip	 = CString(IDS_FORMAT);

			pView->AddUI(m_pInput, L"root", &Data);
			}	
	
		pView->EndGroup(TRUE);
		}
	}

void CAnalogInputPage::LoadChannels(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAIs()) ) {

		UINT uCols = 0;

		if( TRUE ) {
			
			uCols ++;
			}

		if( TRUE ) {
			
			uCols ++;
			}

		if( m_pSystem->HasFlag(L"HasAIResolution") ) {

			uCols ++;
			}
		
		pView->StartTable(CString(IDS_CHANNELS), uCols);

		if( TRUE ) {
			
			pView->AddColHead(CString(IDS_RANGE));
			}

		if( TRUE ) {
			
			pView->AddColHead(CString(IDS_SCALE));
			}

		if( m_pSystem->HasFlag(L"HasAIResolution") ) {

			pView->AddColHead(CString(IDS_RESOLUTION));
			}

		for( UINT n = 0; n < uCount; n ++ ) {

			pView->AddRowHead(CPrintf(L"AI%d", 1 + n));

			AddRangeUI(pView, n);

			AddScaleUI(pView, n);

			AddResolutionUI(pView, n);

			//AddFeatureUI(pView, n);
			}

		pView->EndTable();
		}
	}

void CAnalogInputPage::AddRangeUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( FALSE ) {

			// Voltage
			
			Form += "";	Form += "0|Disabled";
			Form += "|";	Form += "3|+-62.5 mV";
			Form += "|";	Form += "4|+-125 mV";
			Form += "|";	Form += "5|+-250 mV";
			Form += "|";	Form += "6|+-500 mV";
			Form += "|";	Form += "7|+-1.0 V";
			Form += "|";	Form += "8|+-2.0 V";
			Form += "|";	Form += "9|+-5.0 V";
			Form += "|";	Form += "10|+-10.0 V";
			}

		if( m_pSystem->HasFlag(L"HasTCType") ) {

			// Thermocouple
			
			Form += "";	Form += "0|Type J";
			Form += "|";	Form += "1|Type K";
			Form += "|";	Form += "2|Type E";
			Form += "|";	Form += "3|Type R";
			Form += "|";	Form += "4|Type T";
			Form += "|";	Form += "5|Type B";
			Form += "|";	Form += "6|Type C";
			Form += "|";	Form += "7|Type N";
			Form += "|";	Form += "8|Type S";
			}

		if( FALSE ) {

			// Current
			
			Form += "";	Form += "0|0-20mA";
			Form += "|";	Form += "4|4-20mA";
			}

		if( m_pSystem->HasFlag(L"HasRTDType") ) {

			// RTD

			Form += "";	Form += "2|European Curve";
			Form += "";	Form += "3|American Curve";
			Form += "";	Form += "4|10 ohm copper";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Range%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Range", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputPage::AddScaleUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( FALSE ) {

			Form += "";	Form += "Disabled";
			}

		if( m_pSystem->HasFlag(L"HasTCType") ) {

			// Thermocouple

			Form += "";	Form += "0|No Burnout";
			Form += "|";	Form += "1|Upscale";
			Form += "|";	Form += "2|Downscale";
			}

		if( FALSE ) {

			// Current
			
			Form += "";	Form += "0|Negative below 0mA";
			Form += "|";	Form += "0|Negative below 4mA";
			Form += "|";	Form += "0|Positive results only";
			}
		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Scale%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Scale", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputPage::AddResolutionUI(IUICreate *pView, UINT uChan)
{
	if( m_pSystem->HasFlag(L"HasAIResolution") ) {

		CString Form;

		if( m_pSystem->HasFlag(L"HasAIResolution") ) {
					
			Form = L"16-bit integrating|10-bit high speed";
			}
		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Resolution%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Resolution", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputPage::AddFeatureUI(IUICreate *pView, UINT uChan)
{
	if( m_pSystem->HasFlag(L"HasRTDType") ) {

		// RTD

		CString Form;

		Form += "";	Form += "2|alpha=0.00385";
		Form += "";	Form += "3|alpha=0.00392";
		Form += "";	Form += "4|Disabled";

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Resolution%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogInputItem, CIOItem);

// Constructor

CAnalogInputItem::CAnalogInputItem(void)
{
	m_Integration	 = 0;

	m_TempUnits	 = 0;

	m_TempFormat	 = 0;

	m_EnableCounters = 0;

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		m_Range[n] = 0;
		}

	for( UINT n = 0; n < elements(m_Scale); n ++ ) {

		m_Scale[n] = 0;
		}

	for( UINT n = 0; n < elements(m_Resolution); n ++ ) {

		m_Resolution[n] = 0;
		}
	}

// Overridables

UINT CAnalogInputItem::GetTreeImage(void)
{
	return IDI_AI;
	}

// UI Creation

BOOL CAnalogInputItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CAnalogInputPage(this));

	return FALSE;
	}

// UI Update

void CAnalogInputItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "TempFormat" ) {

		AfxTrace(L"temp reporting %d\n", m_TempFormat);
		}

	if( Tag == "TempUnits" ) {

		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistence

void CAnalogInputItem::Init(void)
{
	CIOItem::Init();

	if( m_pSystem->HasFlag(L"HasTCType") ) {

		m_Integration = 130;
		}
	else {
		m_Integration = 0;
		}
	}

// Download Support

void CAnalogInputItem::PrepareData(void)
{
	// TODO -- implement
	
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysAIs();

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CAnalogInput &AI = File.m_AIs[n];

		UINT uRange = AI.GetRange();

		AI.SetRange(uRange);
		}

	if( m_pSystem->HasFlag(L"HasAITemperature") ) {

		UINT uFormat = m_TempFormat + m_TempUnits * 2;

		File.SetTemperatureReporting(uFormat);
		}

	if( m_pSystem->HasFlag(L"HasAIIntegration") ) {
		
		File.SetAnalogInputFiltering(m_Integration);
		}
	}

// Meta Data Creation

void CAnalogInputItem::AddMetaData(void)
{
	CIOItem::AddMetaData();

	Meta_AddInteger(Integration);
	Meta_AddInteger(TempFormat);
	Meta_AddInteger(TempUnits);
	Meta_AddInteger(EnableCounters);

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		UINT uOffset = (PBYTE(m_Range + n) - PBYTE(this));

		AddMeta( CPrintf("Range%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Scale); n ++ ) {

		UINT uOffset = (PBYTE(m_Scale + n) - PBYTE(this));

		AddMeta( CPrintf("Scale%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Resolution); n ++ ) {

		UINT uOffset = (PBYTE(m_Resolution + n) - PBYTE(this));

		AddMeta( CPrintf("Resolution%2.2d", 1 + n), metaInteger, uOffset );
		}
	}

// Implementation

void CAnalogInputItem::DoEnables(IUIHost *pHost)
{
	}

// End of File
