
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ansi String Class
//

// Constructor

CAnsiString::CAnsiString(PCTXT pText)
{
	m_pText = WideToAnsi(pText);
	}

// Destructor

CAnsiString::~CAnsiString(void)
{
	delete m_pText;
	}

// Attributes

UINT CAnsiString::GetLength(void)
{
	return strlen(m_pText);
	}

// Operator

CAnsiString::operator LPCSTR (void) const
{
	return m_pText;
	}

//////////////////////////////////////////////////////////////////////////
//
// Wide to Ansi String Conversion
//

global LPSTR WideToAnsi(PCTXT pText)
{
	AfxValidateStringPtr(pText);

	UINT  uSize = wstrlen(pText);

	LPSTR pNew = New char [ uSize + 1 ];

	UINT n;

	for( n = 0; n < uSize; n ++ ) {

		pNew[n] = char(pText[n]);
		}

	pNew[n] = '\0';

	return pNew;
	}

global LPSTR WidetoAnsi(CString Text)
{
	return WideToAnsi(PCTXT(Text));
	}

// End of File
