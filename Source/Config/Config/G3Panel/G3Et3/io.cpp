
#include "intern.hpp"

#include "io.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Entry
//

class CUIEntry : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIEntry(PCTXT pEntry, PCTXT pLabel);

	protected:
		// Data Members
		CString	       m_Entry;
		CString	       m_Label;
		CLayItemText * m_pEntryLayout;
		CLayItemText * m_pLabelLayout;
		CStatic	     * m_pEntryCtrl;
		CStatic	     * m_pLabelCtrl;

		// Core Overridables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Entry
//

// Runtime Class

AfxImplementRuntimeClass(CUIEntry, CUIControl);

// Constructor

CUIEntry::CUIEntry(PCTXT pEntry, PCTXT pLabel)
{
	m_Entry	       = pEntry;

	m_Label	       = CPrintf(L": %s", pLabel);

	m_pEntryLayout = NULL;

	m_pLabelLayout = NULL;

	m_pEntryCtrl   = New CStatic;

	m_pLabelCtrl   = New CStatic;
	}

// Core Overridables

void CUIEntry::OnLayout(CLayFormation *pForm)
{
	UINT uCount = CString(L"XXX - XXX ").GetLength();

	if( m_Entry.GetLength() > uCount ) {
		
		m_pEntryLayout = New CLayItemText( 20, 1, 1 );
		}
	else {
		m_pEntryLayout = New CLayItemText( L"XXX-XXX ", 1, 1);
		}

	m_pLabelLayout = New CLayItemText( m_Label, 1, 1);

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pEntryLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pLabelLayout, horzLeft | vertCenter));
	
	pForm->AddItem(m_pMainLayout);
	}

void CUIEntry::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pEntryCtrl->Create( m_Entry,
			   SS_LEFT,
			   m_pEntryLayout->GetRect(),
			   Wnd,
			   NOTHING
			   );

	m_pLabelCtrl->Create( m_Label,
			   SS_LEFT,
			   m_pLabelLayout->GetRect(),
			   Wnd,
			   NOTHING
			   );

	m_pEntryCtrl->SetFont(afxFont(Dialog));

	m_pLabelCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pEntryCtrl);

	AddControl(m_pLabelCtrl);
	}

void CUIEntry::OnPosition(void)
{
	m_pEntryCtrl->MoveWindow(m_pEntryLayout->GetRect(), TRUE);

	m_pLabelCtrl->MoveWindow(m_pLabelLayout->GetRect(), TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Manager
//

class CIOManagerPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIOManagerPage(CEt3CommsSystem *pSystem);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CIOManager          * m_pChannel;
		CEt3CommsSystem     * m_pSystem;
		CDiscreteInputItem  * m_pDI;
		CDiscreteOutputItem * m_pDO;
		CAnalogInputItem    * m_pAI;
		CAnalogOutputItem   * m_pAO;

		// Implementation
		BOOL LoadPhysIOs(IUICreate *pView);
		BOOL LoadPhysDIs(IUICreate *pView);
		BOOL LoadPhysDOs(IUICreate *pView);
		BOOL LoadPhysAIs(IUICreate *pView);
		BOOL LoadPhysAOs(IUICreate *pView);
		BOOL LoadDIs(IUICreate *pView);
		BOOL LoadDOs(IUICreate *pView);
		BOOL LoadAIs(IUICreate *pView);
		BOOL LoadAOs(IUICreate *pView);

		void AddEntry(IUICreate *pView, UINT &uStart, UINT uCount, PCTXT pText);
		void AddEntry(IUICreate *pView, PCTXT pText);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Manager
//

// Runtime Class

AfxImplementRuntimeClass(CIOManagerPage, CUIStdPage);

// Constructor

CIOManagerPage::CIOManagerPage(CEt3CommsSystem *pSystem)
{
	m_pChannel = pSystem->m_pChannel;

	m_pSystem  = pSystem;

	m_pDI = (CDiscreteInputItem  *) m_pChannel->m_pIOs->FindItem(AfxNamedClass(L"CDiscreteInputItem"));

	m_pDO = (CDiscreteOutputItem *) m_pChannel->m_pIOs->FindItem(AfxNamedClass(L"CDiscreteOutputItem"));

	m_pAI = (CAnalogInputItem    *) m_pChannel->m_pIOs->FindItem(AfxNamedClass(L"CAnalogInputItem"));

	m_pAO = (CAnalogOutputItem   *) m_pChannel->m_pIOs->FindItem(AfxNamedClass(L"CAnalogOutputItem"));
	}

// Operations

BOOL CIOManagerPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadPhysIOs(pView);

	LoadDIs(pView);

	LoadDOs(pView);

	LoadAIs(pView);

	LoadAOs(pView);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

BOOL CIOManagerPage::LoadPhysIOs(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_PHYSICAL_IO), 1);

	LoadPhysDIs(pView);

	LoadPhysDOs(pView);

	LoadPhysAIs(pView);

	LoadPhysAOs(pView);

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL CIOManagerPage::LoadPhysDIs(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysDIs()) ) {
		
		AddEntry(pView, CPrintf(L"Discrete Input Channels : %d", uCount));
		}

	return TRUE;
	}

BOOL CIOManagerPage::LoadPhysDOs(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysDOs()) ) {
		
		AddEntry(pView, CPrintf(L"Discrete Output Channels : %d", uCount));
		}

	return TRUE;
	}

BOOL CIOManagerPage::LoadPhysAIs(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAIs()) ) {
		
		AddEntry(pView, CPrintf(L"Analog Input Channels : %d", uCount));
		}

	return TRUE;
	}

BOOL CIOManagerPage::LoadPhysAOs(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAOs()) ) {
		
		AddEntry(pView, CPrintf(L"Analog Output Channels : %d", uCount));
		}

	return TRUE;
	}

BOOL CIOManagerPage::LoadDIs(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_DISCRETE_INPUTS), 1);

	UINT uStart = 0;

	UINT uTotal = 512;

	UINT uCount;

	if( (uCount = m_pSystem->GetPhysDIs()) ) {

		AddEntry(pView, uStart, uCount, CString(IDS_DISCRETE_INPUTS));
		}

	if( uCount < 32 ) {

		AddEntry(pView, uStart, 32 - uCount,      CString(IDS_FREE_INTERNAL));
		}

	AddEntry(pView, uStart, 1,      CString(IDS_SELF_TEST_OK_READ));

	AddEntry(pView, uStart, 1,      CString(IDS_POWER_OK_READ));
		
	AddEntry(pView, uStart, 1,      CString(IDS_POWER_OK_READ_2));

	AddEntry(pView, uStart, 1,      CString(IDS_IO_STATUS_READ));

	AddEntry(pView, uStart, 1,      CString(IDS_LINK_STATUS_READ));
		
	AddEntry(pView, uStart, 1,      CString(IDS_LINK_STATUS_READ_2));

	AddEntry(pView, uStart, 1,      CString(IDS_RING_COMPLETE));

	AddEntry(pView, uStart, 1,      CString(IDS_RESERVED));

	AddEntry(pView, uStart, 1,      CString(IDS_HEARTBEAT_BYPASS));	//

	AddEntry(pView, uStart, 1,      CString(IDS_RESERVED));

	AddEntry(pView, uStart, 1,      CString(IDS_NETWORK_OVERLOAD));

	AddEntry(pView, uStart, 5,      CString(IDS_RESERVED));

	AddEntry(pView, uStart, 16,     CString(IDS_IO_TRANSFER));

	AddEntry(pView, uStart, uTotal - uStart, CString(IDS_FREE_INTERNAL_2));

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL CIOManagerPage::LoadDOs(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_DISCRETE_OUTPUTS), 1);

	UINT uStart = 0;

	UINT uTotal = 512;

	UINT uCount;

	if( (uCount = m_pSystem->GetPhysDOs()) ) {

		AddEntry(pView, uStart, uCount, CString(IDS_DISCRETE_OUTPUTS));
		}

	if( m_pSystem->HasFlag(L"HasDICounters") ) {

		UINT uNum = m_pSystem->GetNumCounters();

		AddEntry(pView, uStart, uNum, CString(IDS_COUNTER_RESET));
		}

	AddEntry(pView, uStart, uTotal - uStart, CString(IDS_FREE_INTERNAL_2));

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL CIOManagerPage::LoadAIs(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_ANALOG_INPUTS), 1);

	UINT uStart = 0;

	UINT uTotal = 256;

	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAIs()) ) {

		AddEntry(pView, uStart, uCount, CString(IDS_ANALOG_INPUTS));
		}

	if( m_pDI && m_pDI->m_fTimebase && (uCount = m_pSystem->GetNumCounters()) ) {

		AddEntry(pView, uStart, uCount, CString(IDS_COUNTERS));
		}

	if( m_pAI && m_pAI->IsKindOf(AfxRuntimeClass(CAnalogInputTCItem)) ) {

		AddEntry(pView, uStart, 4, CString(IDS_COLD_JUNCTION));
		}

	AddEntry(pView, uStart, uTotal - uStart, CString(IDS_FREE_INTERNAL_2));

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL CIOManagerPage::LoadAOs(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_ANALOG_OUTPUTS), 1);

	UINT uStart = 0;

	UINT uTotal = 256;

	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAOs()) ) {

		AddEntry(pView, uStart, uCount, CString(IDS_ANALOG_OUTPUTS));
		}

	if( m_pDO && m_pDO->m_fHasTPO && (uCount = m_pSystem->GetPhysDOs()) ) {

		AddEntry(pView, uStart, uCount, CString(IDS_TPO_REGISTERS));
		}

	AddEntry(pView, uStart, uTotal - uStart, CString(IDS_FREE_INTERNAL_2));

	pView->EndGroup(TRUE);

	return TRUE;
	}

void CIOManagerPage::AddEntry(IUICreate *pView, UINT &uStart, UINT uCount, PCTXT pText)
{
	if( uCount == 1 ) {

		CString Text = CPrintf(L"%d : %s", uStart + 1, pText);

		AddEntry(pView, Text);
		}
	else {
		CString Text = CPrintf(L"%d - %d : %s", uStart + 1, uStart + uCount, pText);

		AddEntry(pView, Text);
		}

	uStart += uCount;
	}

void CIOManagerPage::AddEntry(IUICreate *pView, PCTXT pText)
{
	CStringArray List;

	CString(pText).Tokenize(List, L":");

	pView->AddElement(New CUIEntry(List[0], List[1]));
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Manager
//

// Dynamic Class

AfxImplementDynamicClass(CIOManager, CEt3UIItem);

// Constructor

CIOManager::CIOManager(void)
{
	m_Handle = HANDLE_NONE;

	m_pIOs	 = New CIOList;
	}

// UI Creation

CViewWnd * CIOManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CIONavTreeWnd");
			
		return AfxNewObject(CViewWnd, Class);
		}						    

	return CUIItem::CreateView(uType);
	}

// UI Loading

BOOL CIOManager::OnLoadPages(CUIPageList *pList)
{			
	pList->Append(New CIOManagerPage(m_pSystem));

	return FALSE;
	}

// Item Naming

CString CIOManager::GetHumanName(void) const
{
	return CString(IDS_CHANNEL_CAPTION);
	}

// Persistance

void CIOManager::Init(void)
{
	CEt3UIItem::Init();

	AddIOs();
	}

void CIOManager::PostLoad(void)
{
	CEt3UIItem::PostLoad();

	AddIOs();
	}

// Meta Data Creation

void CIOManager::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddCollect(IOs);

	Meta_SetName((IDS_CHANNEL_MANAGER));
	}

// Impelmentation

void CIOManager::AddIOs(void)
{
	if( m_pSystem->GetPhysDIs() ) {	

		CString    Class = m_pSystem->GetDIClass();

		CMetaItem *pItem = AfxNewObject(CIOItem, AfxNamedClass(Class));
		
		if( m_pIOs->AppendItem(pItem) ) {

			pItem->SetName(CString(IDS_DISCRETE_INPUT));
			}
		}

	if( m_pSystem->GetPhysDOs() || m_pSystem->HasFlag(L"HasDICounters") ) {	

		CString    Class = m_pSystem->GetDOClass();

		CMetaItem *pItem = AfxNewObject(CIOItem, AfxNamedClass(Class));

		if( m_pIOs->AppendItem(pItem) ) {
			
			pItem->SetName(CString(IDS_DISCRETE_OUTPUT));
			}
		}

	if( m_pSystem->GetPhysAIs() ) {

		CString    Class = m_pSystem->GetAIClass();

		CMetaItem *pItem = AfxNewObject(CIOItem, AfxNamedClass(Class));

		if( m_pIOs->AppendItem(pItem) ) {

			pItem->SetName(CString(IDS_ANALOG_INPUT));
			}
		}

	if( m_pSystem->GetPhysAOs() ) {

		CString    Class = m_pSystem->GetAOClass();

		CMetaItem *pItem = AfxNewObject(CIOItem, AfxNamedClass(Class));

		if( m_pIOs->AppendItem(pItem) ) {

			pItem->SetName(CString(IDS_ANALOG_OUTPUT_2));
			}
		}
	}

/////////////////////////////////////////////////////////////////////////
//
// IO List
//

// Dynamic Class

AfxImplementDynamicClass(CIOList, CItemIndexList);

// Constructor

CIOList::CIOList(void)
{
	m_Class = NULL;
	}

// Item Search

CIOItem * CIOList::FindItem(CLASS Class)
{
	CIOItem *pItem = NULL;

	INDEX n = GetHead();

	while( !Failed(n) ) {		

		if( GetItem(n)->IsKindOf(Class) ) {

			pItem = GetItem(n);
			
			break;
			}
		
		GetNext(n);
		}

	return pItem;
	}

// Item Access

CIOItem * CIOList::GetItem(INDEX Index) const
{
	return (CIOItem *) CItemIndexList::GetItem(Index);
	}

CIOItem * CIOList::GetItem(UINT uPos) const
{
	return (CIOItem *) CItemIndexList::GetItem(uPos);
	}

// Operations

BOOL CIOList::AppendItem(CItem *pItem)
{
	CLASS Class = AfxNamedClass(pItem->GetClassName());

	if( FindItem(Class) ) {

		delete pItem;

		return FALSE;
		}

	return CItemIndexList::AppendItem(pItem);
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Channel Item
//

// Dynamic Class

AfxImplementDynamicClass(CIOItem, CEt3UIItem);

// Constructor

CIOItem::CIOItem(void)
{
	}

// Overridables

UINT CIOItem::GetTreeImage(void)
{
	return 1;
	}

// Persistance

void CIOItem::Init(void)
{
	CEt3UIItem::Init();

	FindManager();
	}

void CIOItem::PostLoad(void)
{
	CEt3UIItem::PostLoad();

	FindManager();
	}

// Meta Data Creation

void CIOItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Name);
	
	Meta_SetName(L"IO");
	}

// Implementation

void CIOItem::FindManager(void)
{
	m_pManager = m_pSystem->m_pChannel;
	}

// End of File
