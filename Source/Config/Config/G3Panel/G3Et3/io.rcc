
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// IO Tree Toolbar
//

IONavTreeTool MENU
BEGIN
	MENUITEM "1000000C",			IDM_ITEM_FIND
END

//////////////////////////////////////////////////////////////////////////
//
// Item Schemas
//

CIOManager_Schema RCDATA
BEGIN

	L"\0"
END

CIOItem_Schema RCDATA
BEGIN

	L"\0"
END

CDiscreteInputItem_Schema RCDATA
BEGIN
	L"Filter,Filtering Response,,Enum/DropDown,Fast (no filtering)|Slow (more filtering),"
	L"Select Fast Filter Response (no filtering) for instantaneous input updates.\n"
	L"Use this mode for:\n"
	L"- DC inputs with scans less then 1ms\n"
	L"- Counter applications with rates faster than 400 Hz\n"
	L"- When there is no contact bounce\n\n"
	L"Select Slow Filter Response to enable filtering of the input signals to eliminate "
	L"contact bounce from mechanical contacts, ignore power dropouts and ignore noise pulses.\n"
	L" Use this mode for:\n"
	L"- DC inputs with scans slower than 15ms\n"
	L"- Count inputs from mechanical contacts (20Hz)\n"
	L"\0"

	L"Counter,Counter Mode,,Enum/DropDown,Disabled|Use 16-bit Counter|Use 32-bit Counter,"
	L"Select whether counters should be enabled and the counter resolution to be used "
	L"when counting the number of discrete input rising edge transitions."
	L"\0"

	L"SourceSink,Source/Sink Mode,,Enum/DropDown,Source (DI-)|Sink (DI+)|Follow jumper,"
	L"Select the mode that matches the Sink/Source jumper setting on the base of the combination "
	L"I/O module onboard inputs. The number of inputs affected by this setting will vary by module type."
	L"\0"

	L"\0"
END

CDiscreteOutputItem_Schema RCDATA
BEGIN
	L"UseLast8,Use last 8 discrete channels as outputs,,OnOff/Check,,"
	L"\0"

	L"TPOEnable,Time Proportional Outputs,,Enum/DropDown,Disabled|Enabled,"
	L"\0"

	L"TPOCycle,Cycle Time,,Integer/EditBox,|0|ms|10|60000,"
	L"\0"

	L"TPOMinimum,Minimum OFF/ON,,Integer/EditBox,|0|ms|10|99999,"
	L"\0"

	L"\0"
END

CAnalogInputBaseItem_Schema RCDATA
BEGIN
	L"TempUnits,Units,,Enum/DropDown,Celsius|Fahrenheit,"
	L"\0"

	L"TempFormat,Resolution,,Enum/DropDown,One Degree|Tenth Degree,"
	L"\0"

	L"\0"
END

CAnalogInputItem_Schema RCDATA
BEGIN
	L"TempUnits,Units,,Enum/DropDown,Celsius|Fahrenheit,"	
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Page Schemas
//

CIOManager_Page RCDATA
BEGIN
	L"\0"
END

CIOItem_Page RCDATA
BEGIN
	L"\0"
END

CDiscreteInputItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"\0"
END

CDiscreteOutputItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"\0"
END

// End of File
