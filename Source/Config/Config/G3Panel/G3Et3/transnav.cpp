
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Transfer Navigation Window
//

class CTransferNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CTransferNavTreeWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Command Handlers
		BOOL OnTransferGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnTransferControl(UINT uID, CCmdSource &Src);
		BOOL OnTransferCommand(UINT uID);
		BOOL CanTransferNew(void);
		void OnTransferNew(CLASS Class);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		void NewItemSelected(void);
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void OnItemRenamed(CItem *pItem);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Transfer Navigation Window
//
	
// Dynamic Class

AfxImplementDynamicClass(CTransferNavTreeWnd, CNavTreeWnd);
		
// Constructor

CTransferNavTreeWnd::CTransferNavTreeWnd(void) : CNavTreeWnd( L"Transfers",
						 NULL,
						 AfxRuntimeClass(CTransferItem)
						 )
{
	}

// Message Map

AfxMessageMap(CTransferNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchGetInfoType(IDM_TRANSFER,  OnTransferGetInfo)
	AfxDispatchControlType(IDM_TRANSFER,  OnTransferControl)
	AfxDispatchCommandType(IDM_TRANSFER,  OnTransferCommand)

	AfxMessageEnd(CTransferNavTreeWnd)
	};

// Message Handlers

void CTransferNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"TransferNavTreeTool"));
		}
	}

// Command Handlers

BOOL CTransferNavTreeWnd::OnTransferGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {
		
		case IDM_TRANSFER_NEW_DISCRETE:

			Info.m_Image   = 0x70000008;

			Info.m_ToolTip = CString(IDS_NEW_DISCRETE);

			Info.m_Prompt  = CString(IDS_ADD_NEW_DISCRETE);

			return TRUE;

		case IDM_TRANSFER_NEW_ANALOG:

			Info.m_Image   = 0x70000007;

			Info.m_ToolTip = CString(IDS_NEW_ANALOG);

			Info.m_Prompt  = CString(IDS_ADD_NEW_ANALOG);

			return TRUE;
		}

	return FALSE;
	}

BOOL CTransferNavTreeWnd::OnTransferControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_TRANSFER_NEW_DISCRETE:
		case IDM_TRANSFER_NEW_ANALOG:

			Src.EnableItem(CanTransferNew());
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CTransferNavTreeWnd::OnTransferCommand(UINT uID)
{
	switch( uID ) {

		case IDM_TRANSFER_NEW_DISCRETE:

			OnTransferNew(AfxRuntimeClass(CTransferDiscreteItem));

			return TRUE;

		case IDM_TRANSFER_NEW_ANALOG:

			OnTransferNew(AfxRuntimeClass(CTransferAnalogItem));

			return TRUE;
		}

	return FALSE;
	}

BOOL CTransferNavTreeWnd::CanTransferNew(void)
{
	if( !m_fMulti && !IsItemLocked(m_hSelect) ) {
		
		return m_pList->GetItemCount() < 32;
		}

	return FALSE;
	}

void CTransferNavTreeWnd::OnTransferNew(CLASS Class)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(CString(IDS_FORMAT_3), n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, Class);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

// Tree Loading

void CTransferNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"Et3Tree16"), afxColor(MAGENTA));
	}

// Item Hooks

void CTransferNavTreeWnd::NewItemSelected(void)
{
	CNavTreeWnd::NewItemSelected();

	CString Text;

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferDiscreteItem)) ) {

		Text = CString(IDS_DISCRETE_TRANSFER);
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CTransferAnalogItem)) ) {

		Text = CString(IDS_ANALOG_TRANSFER);
		}

	afxThread->SetStatusText(Text);
	}

UINT CTransferNavTreeWnd::GetRootImage(void)
{
	return IDI_TRANSFER_GROUP;
	}

UINT CTransferNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return ((CTransferItem *) pItem)->GetTreeImage();
	}

BOOL CTransferNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"TransferNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"TransferNavTreeMissCtxMenu";

	return FALSE;
	}

void CTransferNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	CNavTreeWnd::OnItemRenamed(pItem);
	}

// End of File
