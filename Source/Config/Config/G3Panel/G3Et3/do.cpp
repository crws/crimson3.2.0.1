
#include "intern.hpp"

#include "io.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Discrete Output Page
//

class CDiscreteOutputPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDiscreteOutputPage(CDiscreteOutputItem *pOutput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CDiscreteOutputItem * m_pOutput;
		CEt3CommsSystem     * m_pSystem;

		// Implementation
		void LoadTimeProp(IUICreate *pView);
		void LoadUseLast8(IUICreate *pView);
		void LoadChannelMode(IUICreate *pView);
		void LoadCounterReset(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Discrete Output Page
//

// Runtime Class

AfxImplementRuntimeClass(CDiscreteOutputPage, CUIStdPage);

// Constructor

CDiscreteOutputPage::CDiscreteOutputPage(CDiscreteOutputItem *pOutput)
{
	m_Class   = AfxRuntimeClass(CDiscreteOutputItem);	

	m_pOutput  = pOutput;

	m_pSystem = (CEt3CommsSystem *) pOutput->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CDiscreteOutputPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadUseLast8(pView);

	LoadTimeProp(pView);

	LoadChannelMode(pView);

	LoadCounterReset(pView);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

void CDiscreteOutputPage::LoadTimeProp(IUICreate *pView)
{
	if( m_pOutput->m_fHasTPO ) {

		pView->StartGroup(L"Time Proportional Options", 1);

		pView->AddUI(m_pOutput, L"root", L"TPOEnable");

		pView->AddUI(m_pOutput, L"root", L"TPOCycle");

		pView->AddUI(m_pOutput, L"root", L"TPOMinimum");

		pView->EndGroup(TRUE);
		}
	}

void CDiscreteOutputPage::LoadUseLast8(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"UseLast8") ) {

		pView->StartGroup(L"Options", 1);

		pView->AddUI(m_pOutput, L"root", L"UseLast8");

		pView->EndGroup(TRUE);
		}
	}

void CDiscreteOutputPage::LoadChannelMode(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysDOs()) ) {

		pView->StartTable(CString(IDS_CHANNELS), 1);

		pView->AddColHead(CString(IDS_MODE));
	
		for( UINT n = 0; n < uCount; n ++ ) {

			CString Form;

			Form += L"";	Form += L"0|Disabled";
			Form += L"|";	Form += L"128|Enabled";

			if( n < m_pSystem->GetNumTPOs() ) {

				UINT uOutputs;
				
				if( (uOutputs = m_pSystem->GetPhysAOs()) ) {

					Form += "|";	Form += CPrintf(L"129|TPO controlled by AO%d", uOutputs + 1 + n);
					}
				else {
					Form += "|";	Form += L"129|TPO Enabled";
					}
				}

			pView->AddRowHead(CPrintf(L"DO%d", 1 + n));

			CUIData Data;

			Data.m_Tag	 = CPrintf(L"Mode%2.2d", 1 + n);

			Data.m_Label	 = CPrintf(L"Channel %d Mode", 1 + n);

			Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

			Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

			Data.m_Format	 = Form;

			Data.m_Tip	 = CString(IDS_CONFIGURATION);

			pView->AddUI(m_pOutput, L"root", &Data);
			}

		pView->EndTable();
		}
	}

void CDiscreteOutputPage::LoadCounterReset(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasDICounters") ) {

		UINT uCount = m_pSystem->GetPhysDOs();

		UINT   uNum = m_pSystem->GetNumCounters();

		pView->StartTable(CString(IDS_COUNTER_RESET), 1);

		pView->AddColHead(CString(IDS_RESET));

		for( UINT n = 0; n < uNum; n ++ ) {

			pView->AddRowHead(CPrintf(L"DO%d", 1 + n + uCount));

			CUIData Data;

			Data.m_Tag	 = CPrintf(L"Reset%2.2d", 1 + n);

			Data.m_Label	 = CPrintf(L"Counter %d Reset", 1 + n + uCount);

			Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

			Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

			Data.m_Format	 = L"0|Disabled|1|Enabled";

			Data.m_Tip	 = CString(IDS_ENABLE_COUNTER);

			pView->AddUI(m_pOutput, L"root", &Data);
			}

		pView->EndTable();
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Discrete Output
//

// Dynamic Class

AfxImplementDynamicClass(CDiscreteOutputItem, CIOItem);

// Constructor

CDiscreteOutputItem::CDiscreteOutputItem(void)
{
	m_fHasTPO	= FALSE;
	}

// Overridables

UINT CDiscreteOutputItem::GetTreeImage(void)
{
	return IDI_DO;
	}

// UI Creation

BOOL CDiscreteOutputItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CDiscreteOutputPage(this));

	return FALSE;
	}

// UI Update

void CDiscreteOutputItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		CheckUseLast8(pHost);

		CheckTPOEnable(pHost);

		DoEnables(pHost);
		}

	if( Tag == "UseLast8" ) {

		UINT c = m_pSystem->GetPhysDOs();

		for( UINT n = 0; n < c; n ++ ) {
				
			m_Mode[n] = UseLast8() ? 128 : 0;

			pHost->UpdateUI(pItem, CPrintf(L"Mode%2.2d", 1+n));
			}

		CheckUseLast8(pHost);

		CheckTPOEnable(pHost);

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Mode") ) {

		CheckUseLast8(pHost);
		}

	if( Tag == "TPOEnable" ) {

		CheckTPOEnable(pHost);

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CDiscreteOutputItem::Init(void)
{
	CIOItem::Init();

	m_UseLast8	= 0;

	m_TPOEnable	= 0;

	m_TPOCycle	= 5000;

	m_TPOMinimum	= 500;

	for( UINT n = 0; n < elements(m_Mode); n ++ ) {
		
		m_Mode[n] = 128;
		}

	for( UINT n = 0; n < elements(m_Reset); n ++ ) {
		
		m_Reset[n] = 0;
		}
	}

// Download Support

void CDiscreteOutputItem::PrepareData(void)
{
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysDOs();

	for( UINT n = 0; n < c; n ++ ) {

		CFileDataBaseCfgSetup::CDiscreteOutput &DO = File.m_DOs[n];

		DO.SetConfig(m_Mode[n]);
		}

	if( m_fHasTPO && m_TPOEnable ) {

		UINT   uPeriod     = 250;
		UINT   uInterval   = 0;
		UINT   uMinimum    = 0;
		double dBestError  = 1e30;

		double Cycle       = m_TPOCycle;
		double Minimum     = m_TPOMinimum;

		MakeMax(Cycle, 10);
		MakeMin(Cycle, 600000);

		MakeMax(Minimum, 10);
		MakeMin(Minimum, Cycle);

		for( UINT uPer = 250; uPer >= 1; --uPer ) {

			double dInterval = Cycle / uPer;

			MakeMax(dInterval, 10);

			UINT uInt        = UINT(dInterval + 0.9999);
				
			UINT uMin	 = UINT(Minimum / uInt + 0.9999);

			double dReqError = fabs(Cycle   - LONG(uInt) * uPer);

			double dMinError = fabs(Minimum - LONG(uMin)  * uInt);

			double dActError = dReqError / Cycle + dMinError / (Minimum * pow(Cycle / Minimum, 0.7));

			double dOffset   = (uPer < 125 && uInt > 10) ? 0.003 : 0;

			if( dActError + dOffset < (0.5 + 0.5 * (double)uPer / uPeriod) * dBestError ) {

				dBestError = dActError;
					
				uPeriod    = uPer;
					
				uMinimum   = uMin;
					
				uInterval  = uInt;
				}
			}

		File.SetTPOInterval(uInterval);

		File.SetTPOPeriod  (uPeriod);
	
		File.SetTPOMinimum (uMinimum);
		}

	if( m_pSystem->HasFlag(L"HasDICounters") ) {

		UINT uData = File.GetCounterResetEnables();

		UINT  uNum = m_pSystem->GetNumCounters();

		for( UINT n = 0; n < uNum; n ++ ) {

			SetBit(uData, m_Reset[n], n);
			}

		File.SetCounterResetEnables(uData);
		}
	}

// Meta Data Creation

void CDiscreteOutputItem::AddMetaData(void)
{
	CIOItem::AddMetaData();

	Meta_AddInteger(UseLast8);
	Meta_AddInteger(TPOEnable);
	Meta_AddInteger(TPOCycle);
	Meta_AddInteger(TPOMinimum);

	for( UINT n = 0; n < elements(m_Mode); n ++ ) {

		UINT uOffset = (PBYTE(m_Mode + n) - PBYTE(this));

		AddMeta( CPrintf("Mode%2.2d", 1 + n), metaInteger, uOffset );
		}

	for( UINT n = 0; n < elements(m_Reset); n ++ ) {

		UINT uOffset = (PBYTE(m_Reset + n) - PBYTE(this));

		AddMeta( CPrintf("Reset%2.2d", 1 + n), metaInteger, uOffset );
		}
	}

// Implementation

void CDiscreteOutputItem::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(L"TPOEnable",  UseLast8());

	pHost->EnableUI(L"TPOCycle",   UseLast8() && m_TPOEnable);
	
	pHost->EnableUI(L"TPOMinimum", UseLast8() && m_TPOEnable);
	}

void CDiscreteOutputItem::CheckUseLast8(IUIHost *pHost)
{
	UINT c = m_pSystem->GetPhysDOs();

	UINT n;

	for( n = 0; n < c; n ++ ) {
			
		if( m_Mode[n] ) {
				
			break;
			}
		}

	m_UseLast8 = n == c ? 0 : 1;
			
	pHost->UpdateUI(this, "UseLast8");
	}

void CDiscreteOutputItem::CheckTPOEnable(IUIHost *pHost)
{
	UINT uMask = 0x01;

	SetBit(uMask, UseLast8(), 1);

	if( m_fHasTPO ) {

		SetBit(uMask, UseLast8() && m_TPOEnable, 2);
		}
	else {
		SetBit(uMask, m_pSystem->GetNumTPOs() > 0, 2);		
		}

	UINT c = m_pSystem->GetPhysDOs();

	for( UINT n = 0; n < c; n ++ ) {

		LimitEnum(pHost, CPrintf(L"Mode%2.2d", 1+n), m_Mode[n], uMask);
		}
	}

BOOL CDiscreteOutputItem::UseLast8(void)
{
	if( m_pSystem->HasFlag(L"UseLast8") ) {

		return m_UseLast8;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Discrete Output - with TPO
//

// Dynamic Class

AfxImplementDynamicClass(CDiscreteOutputTPOItem, CDiscreteOutputItem);

// Constructor

CDiscreteOutputTPOItem::CDiscreteOutputTPOItem(void)
{
	m_fHasTPO = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Discrete Output - Relay
//

// Dynamic Class

AfxImplementDynamicClass(CDiscreteOutputRelayItem, CDiscreteOutputItem);

// Constructor

CDiscreteOutputRelayItem::CDiscreteOutputRelayItem(void)
{
	m_fHasTPO = FALSE;
	}

// UI Update

void CDiscreteOutputRelayItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT c = m_pSystem->GetPhysDOs();

		for( UINT n = 0; n < c; n ++ ) {

			UINT uMask = 0x02;

			LimitEnum(pHost, CPrintf(L"Mode%2.2d", 1+n), m_Mode[n], uMask); 
			
			pHost->UpdateUI(this, CPrintf(L"Mode%2.2d", 1+n));
			}		
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CDiscreteOutputRelayItem::Init(void)
{
	CIOItem::Init();

	UINT c = m_pSystem->GetPhysDOs();

	for( UINT n = 0; n < c; n ++ ) {

		m_Mode[n] = 128;
		}
	}

// End of File
