
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Transfer List
//

// Dynamic Class

AfxImplementDynamicClass(CTransferBlockList, CNamedList);

// Constructor

CTransferBlockList::CTransferBlockList(void)
{
	}

// Item Access

CTransferBlockItem * CTransferBlockList::GetItem(INDEX Index) const
{
	return (CTransferBlockItem *) CNamedList::GetItem(Index);
	}

CTransferBlockItem * CTransferBlockList::GetItem(UINT uPos) const
{
	return (CTransferBlockItem *) CNamedList::GetItem(uPos);
	}

CTransferBlockItem * CTransferBlockList::GetItem(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CTransferBlockItem *pBlock = GetItem(n);

		if( pBlock->m_Name == Name ) {

			return pBlock;
			}

		GetNext(n);
		}

	return NULL;
	}

// Persistence

void CTransferBlockList::Init(void)
{
	CNamedList::Init();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

void CTransferBlockList::PostLoad(void)
{
	CNamedList::PostLoad();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item Page
//

class CTransferBlockItemPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTransferBlockItemPage(CTransferBlockItem *pTransfer);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CTransferBlockItem * m_pTransfer;
		CEt3CommsSystem    * m_pSystem;

		// Implementation
		void LoadStatus(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Page
//

// Runtime Class

AfxImplementRuntimeClass(CTransferBlockItemPage, CUIStdPage);

// Constructor

CTransferBlockItemPage::CTransferBlockItemPage(CTransferBlockItem *pTransfer)
{
	m_Class     = AfxPointerClass(pTransfer);	

	m_pTransfer = pTransfer;

	m_pSystem   = (CEt3CommsSystem *) pTransfer->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CTransferBlockItemPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);	

	LoadStatus(pView);

	pView->NoRecycle();

	return FALSE;
	}

// Implementation

void CTransferBlockItemPage::LoadStatus(IUICreate *pView)
{
	CString Form = CString(IDS_NONE);

	for( UINT n = 0; n < 16; n ++ ) {

		Form += CPrintf(L"|%d|DI %d", 49 + n - 1, 49 + n);
		}

	CString Tip = CString(IDS_SELECT_BIT_TO);

	pView->StartGroup(CString(IDS_STATUS_BIT), 1);

	CUIData Data;

	Data.m_Tag	 = L"Status";

	Data.m_Label	 = CString(IDS_STATUS_BIT);

	Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

	Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Data.m_Format	 = Form;

	Data.m_Tip	 = Tip;

	pView->AddUI(m_pTransfer, L"root", &Data);

	pView->EndGroup(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item
//

// Dynamic Class

AfxImplementRuntimeClass(CTransferBlockItem, CEt3UIItem);

// Constructor

CTransferBlockItem::CTransferBlockItem(void)
{
	m_Direction	= 0;

	m_SrceType	= 0;

	m_SrceAddr	= 1;

	m_DestType	= 0;

	m_DestAddr	= 1;

	m_Count		= 8;

	m_Status	= NOTHING;
	}

// UI Creation

BOOL CTransferBlockItem::OnLoadPages(CUIPageList *pList)
{			 
	pList->Append(New CTransferBlockItemPage(this));

	return FALSE;
	}

// UI Update

void CTransferBlockItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Direction" ) {

		pHost->SendUpdate(updateProps);
		}

	if( Tag == "Status" ) {

		// TODO -- for unique status bits

		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Item Naming

CString CTransferBlockItem::GetHumanName(void) const
{
	return m_Name;
	}

// Persistence

void CTransferBlockItem::Init(void)
{
	CEt3UIItem::Init();

	CTransferBlockList *pList = GetParentList();

	for( UINT n = 1;; n++ ) {

		CPrintf Name(L"Transfer%u", n);

		if( pList->GetItem(Name) ) {

			continue;
			}

		m_Name = Name;

		break;
		}

	m_pSystem->AddTransfer();
	}

void CTransferBlockItem::PostLoad(void)
{
	CEt3UIItem::PostLoad();

	m_pSystem->AddTransfer();
	}

void CTransferBlockItem::Kill(void)
{
	CEt3UIItem::Kill();

	m_pSystem->DelTransfer();
	}

// Download Support

void CTransferBlockItem::PrepareData(void)
{
	UINT				    uIndex = m_pSystem->NextTransferIndex();

	CFileDataBaseCfgXfers                &File = m_pSystem->m_CfgXfers;

	CFileDataBaseCfgXfers::CTransfer &Transfer = File.m_Transfers[uIndex];

	// Copy Block Type
	Transfer.PutBlockType(BuildType());

	// Station Info Block
	Transfer.PutStation  (BuildStation());
	Transfer.PutInterface(BuildInterface());
	Transfer.PutIPAddr   (BuildIPAddr());
	Transfer.PutDestPort (BuildDestPort());
	Transfer.PutStatusBit(m_Status);

	// Copy Info Block
	Transfer.PutRegCount (m_Count);
	Transfer.PutSrceType (m_SrceType);
	Transfer.PutSrceAddr (m_SrceAddr - 1);
	Transfer.PutDestType (m_DestType);
	Transfer.PutDestAddr (m_DestAddr - 1);	
	}

// Meta Data Creation

void CTransferBlockItem::AddMetaData(void)
{
	CEt3UIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Direction);
	Meta_AddInteger(SrceType);
	Meta_AddInteger(SrceAddr);
	Meta_AddInteger(DestType);
	Meta_AddInteger(DestAddr);
	Meta_AddInteger(Count);
	Meta_AddInteger(Status);

	Meta_SetName((IDS_TRANSFER));
	}

// Implementation

void CTransferBlockItem::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = TRUE;

	pHost->EnableUI("Direction", fEnable);
	pHost->EnableUI("Count",     fEnable);
	pHost->EnableUI("SrceType",  fEnable);
	pHost->EnableUI("SrceAddr",  fEnable);
	pHost->EnableUI("DestType",  fEnable);
	pHost->EnableUI("DestAddr",  fEnable);
	pHost->EnableUI("Status",    fEnable);
	}

// Item Location

CTransferBlockList * CTransferBlockItem::GetParentList(void) const
{
	return (CTransferBlockList *) GetParent();
	}

CEt3CommsDevice * CTransferBlockItem::GetParentDevice(void) const
{
	return (CEt3CommsDevice *) GetParent(2);
	}

CEt3CommsPort * CTransferBlockItem::GetParentPort(void) const
{
	return (CEt3CommsPort *) GetParentDevice()->GetParentPort();
	}

// Config File Help

UINT CTransferBlockItem::BuildType(void)
{
	if( IsDeviceModbus() ) {
		
		return m_Direction == 0 ? 0x03 : 0x05;
		}

	if( IsDeviceSixnet() ) {
		
		return m_Direction == 0 ? 0x02 : 0x04;
		}

	return 0x00;
	}

UINT CTransferBlockItem::BuildStation(void)
{
	return GetParentDevice()->m_Station;
	}

UINT CTransferBlockItem::BuildInterface(void)
{
	CEt3CommsPort *pPort = GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {
		
		return 0x01;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {
		
		return 0x10;
		}

	return 0xFF;
	}

PCBYTE CTransferBlockItem::BuildIPAddr(void)
{
	CEt3CommsPort  * pPort   = GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {

		CEt3CommsDevice *pDevice = GetParentDevice();

		UINT uAddr = pDevice->m_IPAddr;

		static	BYTE Data[16] = { 0 };

		Data[3] = LOBYTE(LOWORD(uAddr));
		Data[2] = HIBYTE(LOWORD(uAddr));
		Data[1] = LOBYTE(HIWORD(uAddr));
		Data[0] = HIBYTE(HIWORD(uAddr));

		return Data;		
		}

	static	BYTE Null[16] = { 0 };

	return Null;
	}

UINT CTransferBlockItem::BuildDestPort(void)
{
	CEt3CommsPort *pPort = GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {
		
		return ((CEt3CommsPortNetwork *) pPort)->m_Port;
		}

	return 0;
	}

BOOL CTransferBlockItem::IsDeviceModbus(void)
{
	CEt3CommsPort *pPort = GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		CEt3CommsDriver *pDriver = ((CEt3CommsPortSerial *) pPort)->m_pDriver;

		return pDriver->IsModbus();
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkModbus)) ) {
			
		return TRUE;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkSixnet)) ) {
			
		return FALSE;
		}

	return FALSE;
	}

BOOL CTransferBlockItem::IsDeviceSixnet(void)
{
	CEt3CommsPort *pPort = GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		CEt3CommsDriver *pDriver = ((CEt3CommsPortSerial *) pPort)->m_pDriver;

		return pDriver->IsSixnet();
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkModbus)) ) {
			
		return FALSE;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkSixnet)) ) {
			
		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Discrete
//

// Dynamic Class

AfxImplementDynamicClass(CTransferBlockDiscreteItem, CTransferBlockItem);

// Constructor

CTransferBlockDiscreteItem::CTransferBlockDiscreteItem(void)
{
	m_SrceType = 10;

	m_SrceAddr = 1;

	m_DestType = 10;

	m_DestAddr = 1;
	}

// Attributes

UINT CTransferBlockDiscreteItem::GetTreeImage(void) const
{
	return m_Direction ? IDI_DISCRETE_READ : IDI_DISCRETE_WRITE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Analog
//

// Dynamic Class

AfxImplementDynamicClass(CTransferBlockAnalogItem, CTransferBlockItem);

// Constructor

CTransferBlockAnalogItem::CTransferBlockAnalogItem(void)
{
	m_SrceType = 0;

	m_SrceAddr = 1;

	m_DestType = 0;

	m_DestAddr = 1;
	}

// Attributes

UINT CTransferBlockAnalogItem::GetTreeImage(void) const
{
	return m_Direction ? IDI_ANALOG_READ : IDI_ANALOG_WRITE;
	}

// End of File

