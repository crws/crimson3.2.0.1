
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

class CAnalogInputIPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAnalogInputIPage(CAnalogInputBaseItem *pInput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAnalogInputBaseItem  * m_pInput;
		CEt3CommsSystem       * m_pSystem;
		BOOL			m_fFeature1;
		BOOL			m_fFeature2;

		// Implementation
		void LoadIntegration(IUICreate *pView);
		void LoadTemperature(IUICreate *pView);
		void LoadChannels   (IUICreate *pView);
		void AddRangeUI     (IUICreate *pView, UINT uChan);
		void AddFeature1UI  (IUICreate *pView, UINT uChan);
		void AddFeature2UI  (IUICreate *pView, UINT uChan);
		void LoadTimebases  (IUICreate *pView);
		void AddTimebaseUI  (IUICreate *pView, UINT uChan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

// Runtime Class

AfxImplementRuntimeClass(CAnalogInputIPage, CUIStdPage);

// Constructor

CAnalogInputIPage::CAnalogInputIPage(CAnalogInputBaseItem *pInput)
{
	m_Class   = AfxRuntimeClass(CAnalogInputBaseItem);

	m_pInput  = pInput;	

	m_pSystem = (CEt3CommsSystem *) pInput->GetDatabase()->GetSystemItem();

	UINT uIdent = m_pSystem->GetModuleIdent();

	if( uIdent == E3_MOD_MIX24880 || 
	    uIdent == E3_MOD_MIX24882 || 
	    uIdent == E3_MOD_MIX24884 ||
	    uIdent == E3_MOD_32AI20M  ||
	    uIdent == E3_MOD_16AI20M  ||
	    uIdent == E3_MOD_16AI8AO  ){

		m_fFeature1 = TRUE;

		m_fFeature2 = TRUE;
		}

	else if( uIdent == E3_MOD_16ISO20M ){

		m_fFeature1 = TRUE;

		m_fFeature2 = FALSE;
		}
	else {
		m_fFeature1 = FALSE;

		m_fFeature2 = TRUE;
		}
	}

// Operations

BOOL CAnalogInputIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadIntegration(pView);
	
	LoadTemperature(pView);

	LoadChannels (pView);

	LoadTimebases(pView);

	return FALSE;
	}

// Implementation

void CAnalogInputIPage::LoadIntegration(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAIIntegration") ) {

		CString Form;

		if( E3_MOD_16ISO20M == m_pSystem->GetModuleIdent() ) {

			Form += L"";	Form += "128|200ms per channel";
			Form += L"|";	Form += "129|100ms per channel";
			Form += L"|";	Form += "130|50ms per channel";
			Form += L"|";	Form += "131|25ms per channel";
			Form += L"|";	Form += "132|12.5ms per channel";
			Form += L"|";	Form += "133|6.25ms per channel";
			Form += L"|";	Form += "134|3.12s per channel";
			Form += L"|";	Form += "135|1.5ms per channel";
			Form += L"|";	Form += "136|1ms per channel";
			Form += L"|";	Form += "137|0.5ms per channel";
			}
		else {
			Form += L"";	Form += L"0|Best 50/60 Hz (131ms per channel)";
			Form += L"|";	Form += L"16|Standard 50 Hz (130ms per channel)";
			Form += L"|";	Form += L"32|Standard 60 Hz (110ms per channel)";
			Form += L"|";	Form += L"80|Fast 50 Hz (21ms per channel)";
			Form += L"|";	Form += L"96|Fast 60 Hz (21ms per channel)";
			}

		//
		pView->StartGroup(CString(IDS_OPTIONS), 1);

		CUIData Data;

		Data.m_Tag	 = "Integration";

		Data.m_Label 	 = CString(IDS_INTEGRATION);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);

		pView->EndGroup(TRUE);	
		}
	}

void CAnalogInputIPage::LoadTemperature(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAITemperature") ) {

		pView->StartGroup(CString(IDS_TEMPERATURE), 1);

		pView->AddUI(m_pInput, L"root", L"TempUnits");

		pView->AddUI(m_pInput, L"root", L"TempFormat");
	
		pView->EndGroup(TRUE);
		}
	}

void CAnalogInputIPage::LoadChannels(IUICreate *pView)
{
	UINT uPhys;

	if( (uPhys = m_pSystem->GetPhysAIs()) ) {

		UINT uCols = 0;

		if( TRUE ) {
			
			uCols ++;
			}

		if( m_fFeature1 ) {
			
			uCols ++;
			}

		if( m_fFeature2 ) {
			
			uCols ++;
			}

		pView->StartTable(CString(IDS_CHANNELS), uCols);

		if( TRUE ) {
			
			pView->AddColHead(CString(IDS_RANGE));
			}

		if( m_fFeature1 ) {

			CString Text = CString(IDS_FEATURE);

			Text = CString(IDS_SCALE_BEYOND_PV);
			
			pView->AddColHead(Text);
			}

		if( m_fFeature2 ) {

			CString Text = CString(IDS_FEATURE_2);

			Text = CString(IDS_RESOLUTION);
			
			pView->AddColHead(Text);
			}

		for( UINT n = 0; n < uPhys; n ++ ) {

			pView->AddRowHead(CPrintf(L"AI%d", 1 + n));

			AddRangeUI   (pView, n);

			AddFeature1UI(pView, n);

			AddFeature2UI(pView, n);
			}

		pView->EndTable();
		}
	}

void CAnalogInputIPage::AddRangeUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( TRUE ) {
			
			Form += "";	Form += "0|0-20mA";
			Form += "|";	Form += "1|4-20mA";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Range%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Range", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputIPage::AddFeature1UI(IUICreate *pView, UINT uChan)
{
	if( m_fFeature1 ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "-1|Disabled";
			Form += "|";	Form +=  "0|Negative below 4mA";
			Form += "|";	Form +=  "1|Positive Only";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Feature1%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature1", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputIPage::AddFeature2UI(IUICreate *pView, UINT uChan)
{
	if( m_fFeature2 ) {

		CString Form;

		if( TRUE ) {

			Form += L"";	Form += L"0|16-bit integrating";
			Form += L"|";	Form += L"1|10-bit high speed";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Feature2%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature2", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputIPage::LoadTimebases(IUICreate *pView)
{
	if( m_pInput->m_fTimebase ) {

		UINT uPhys;

		if( (uPhys = m_pSystem->GetPhysAIs()) ) {

			UINT uCount;

			if( (uCount = m_pSystem->GetNumCounters()) ) {

				UINT uCols = 0;

				if( TRUE ) {
			
					uCols ++;
					}

				pView->StartTable(CString(IDS_TIMEBASES), uCols);

				pView->AddColHead(CString(IDS_RANGE));
		
				for( UINT n = 0; n < uCount; n ++ ) {

					pView->AddRowHead(CPrintf(L"AI%d", uPhys + 1 + n));

					AddTimebaseUI(pView, n);
					}

				pView->EndTable();		
				}
			}
		}
	}

void CAnalogInputIPage::AddTimebaseUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( TRUE ) {

			Form += L"";	Form += L"-1|Disabled";
			Form += L"|";	Form += L"1|Run time Seconds";
			Form += L"|";	Form += L"2|Run time Minutes";
			Form += L"|";	Form += L"100000|Rate 0.1 Seconds";
			Form += L"|";	Form += L"200000|Rate 0.2 Seconds";
			Form += L"|";	Form += L"500000|Rate 0.5 Seconds";
			Form += L"|";	Form += L"1000000|Rate 1 Second";
			Form += L"|";	Form += L"2000000|Rate 2 Second";
			Form += L"|";	Form += L"5000000|Rate 5 Second";
			Form += L"|";	Form += L"10000000|Rate 10 Second";
			Form += L"|";	Form += L"30000000|Rate 30 Second";
			Form += L"|";	Form += L"60000000|Rate 1 Minute";
			Form += L"|";	Form += L"3|ON Pulse Width";
			Form += L"|";	Form += L"4|OFF Pulse Width";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Timebase%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Time Base %d", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogInputIItem, CAnalogInputBaseItem);

// Constructor

CAnalogInputIItem::CAnalogInputIItem(void)
{
	m_uType	= typeCurrent;

	m_fTimebase = FALSE;
	}

// UI Creation

BOOL CAnalogInputIItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CAnalogInputIPage(this));

	return FALSE;
	}

// UI Update

void CAnalogInputIItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {		

		for( UINT n = 0; n < elements(m_Range); n ++ ) {

			LimitEnum(pHost, CPrintf(L"Range%2.2d", 1+n), m_Range[n], 0x02);
			}

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Range") ) {

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Feature1") ) {

		UINT uLen  = CString(L"Feature1").GetLength();

		UINT uChan = watoi(Tag.Mid(uLen));

		if( m_Feature1[uChan-1] == -1 ) {

			m_Feature2[uChan-1] = 0;

			pHost->UpdateUI(CPrintf(L"Feature2%2.2d", uChan));
			}

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistence

void CAnalogInputIItem::Init(void)
{
	CAnalogInputBaseItem::Init();

	m_Integration = m_fIs16Iso20m ? 130 : 32;

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		m_Range[n] = 1;
		}

	for( UINT n = 0; n < elements(m_Feature1); n ++ ) {

		m_Feature1[n] = 1;
		}

	for( UINT n = 0; n < elements(m_Feature2); n ++ ) {

		m_Feature2[n] = 0;
		}

	for( UINT n = 0; n < elements(m_Timebase); n ++ ) {

		m_Timebase[n] = UINT(-1);
		}
	}

// Download Support

void CAnalogInputIItem::PrepareData(void)
{
	CAnalogInputBaseItem::PrepareData();

	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysAIs();

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CAnalogInput &AI = File.m_AIs[n];

		UINT uRange = 0;

		if( m_Feature1[n] != -1 ) {

			uRange |= m_uType << 4;

			SetBit(uRange, m_Feature1[n], 1);

			SetBit(uRange, m_Range[n],    2);			
			}

		SetBit(uRange, m_Feature2[n], 7);

		AI.SetRange(uRange);
		}

	if( m_fTimebase ) {

		UINT c = m_pSystem->GetNumCounters();

		for( UINT n = 0; n < c; n++ ) {

			CFileDataBaseCfgSetup::CTimebase &Base = File.m_Timebases[n];

			DWORD dwTime;

			if( (dwTime = m_Timebase[n]) != -1 ) {

				switch( dwTime ) {

					case 1:
						dwTime = 1000000;
						break;

					case 2:
						dwTime = 60000000;
						break;

					case 3:
						dwTime = 1000000;
						break;

					case 4:
						dwTime = 10000000;
						break;
					}
				
				Base.SetTime(dwTime);
				}
			else {
				Base.SetTime(0);
				}
			}
		}
	}

// Implementation

void CAnalogInputIItem::DoEnables(IUIHost *pHost)
{
	UINT uCount = 2;

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		BOOL    fRange = TRUE;
		
		BOOL fFeature1 = m_Feature1[n] != -1;

		BOOL fFeature2 = n < uCount;

		pHost->EnableUI(CPrintf(L"Range%2.2d",    1+n), fRange);

		pHost->EnableUI(CPrintf(L"Feature1%2.2d", 1+n), TRUE);

		pHost->EnableUI(CPrintf(L"Feature2%2.2d", 1+n), fFeature1 && fFeature2);
		}

	CDiscreteInputItem *pDI = FindDI();

	if( pDI && pDI->m_fTimebase ) {

		BOOL fCount32 = pDI->m_Counter == 2;

		for( UINT n = 0; n < elements(m_Timebase); n ++ ) {

			BOOL fEnable = fCount32 ? !(n&1) : TRUE;

			pHost->EnableUI(CPrintf(L"Timebase%2.2d", 1+n), fEnable);
			}
		}
	}

CDiscreteInputItem * CAnalogInputIItem::FindDI(void)
{
	return (CDiscreteInputItem *) m_pManager->m_pIOs->FindItem(AfxRuntimeClass(CDiscreteInputItem));
	}

// End of File
