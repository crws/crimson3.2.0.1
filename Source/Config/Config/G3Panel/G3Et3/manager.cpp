
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications Manager
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsManager, CEt3UIItem);

// Constructor

CEt3CommsManager::CEt3CommsManager(void)
{
	m_Handle      = HANDLE_NONE;

	m_Serial      = 0;

	m_Station     = 1;

	m_DisplayLed  = 0;

	m_MaxPort     = 0;

	m_MaxDevice   = 0;

	m_MaxTransfer = 0;

	m_pEthernet   = New CEt3EthernetItem;

	m_pPorts      = New CEt3CommsPortList;

	m_pServices   = New CEt3Services;
	}

// Item Lookup

CEt3CommsPort * CEt3CommsManager::FindPort(UINT uPort) const
{
	CEt3CommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CEt3CommsPort *pPort;
	
		if( pPort = pList->FindPort(uPort) ) {

			return pPort;
			}
		}

	return NULL;
	}

CEt3CommsDevice * CEt3CommsManager::FindDevice(UINT uDevice) const
{
	CEt3CommsPortList *pList;

	for( UINT s = 0; GetPortList(pList, s); s++ ) {

		CEt3CommsDevice *pDevice;
	
		if( pDevice = pList->FindDevice(uDevice) ) {

			return pDevice;
			}
		}

	return NULL;
	}

// Port List Access

BOOL CEt3CommsManager::GetPortList(CEt3CommsPortList * &pList, UINT uIndex) const
{
	switch( uIndex ) {

		case 0:
			pList = m_pPorts;

			return TRUE;

		case 1:
			pList = m_pEthernet->m_pPorts;

			return TRUE;
		}

	pList = NULL;

	return FALSE;
	}

// Operations

UINT CEt3CommsManager::AllocPortNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindPort(n) ) {

			MakeMax(m_MaxPort, n);

			return n;
			}
		}
	}

UINT CEt3CommsManager::AllocDeviceNumber(void)
{
	for( UINT n = 1;; n++ ) {

		if( !FindDevice(n) ) {

			MakeMax(m_MaxDevice, n);

			return n;
			}
		}
	}

// Property Save Filter

BOOL CEt3CommsManager::SaveProp(CString const &Tag) const
{
	return CMetaItem::SaveProp(Tag);
	}

// Persistence

void CEt3CommsManager::Init(void)
{
	CEt3UIItem::Init();
	
	m_Name		= "NewStation";

	m_ScanTime	= 1000;

	m_NetTimeout	= 1000;

	m_SerialTimeout	= 3000;
	}

// Download Support

void CEt3CommsManager::PrepareData(void)
{
	CEt3UIItem::PrepareData();

	if( TRUE ) {

		CFileDataBaseCfgSetup &Setup = m_pSystem->m_CfgSetup;

		Setup.SetStationName(m_Name);

		Setup.SetStationNumber(m_Station);
		}

	if( TRUE ) {

		CFileDataBaseCfgComms &Comms = m_pSystem->m_CfgComms;

		Comms.SetDisplayLed(BuildDisplayLed());
		}

	if( TRUE ) {

		CFileDataBaseCfgXfers &File = m_pSystem->m_CfgXfers;

		File.PutScanTime     (m_ScanTime);
	
		File.PutSerialTimeout(m_SerialTimeout);
	
		File.PutNetTimeout  (m_NetTimeout);
		}

	//CEt3UIItem::PrepareData();
	}

// UI Creation

CViewWnd * CEt3CommsManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CEt3CommsTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}						    

	if( uType == viewResource ) {

		CLASS Class = AfxNamedClass(L"CEt3DeviceResTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return CUIItem::CreateView(uType);
	}

// Item Naming

CString CEt3CommsManager::GetHumanName(void) const
{
	return CString(IDS_COMMS_CAPTION);
	}

// Meta Data Creation

void CEt3CommsManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddString(Name);
	Meta_AddInteger(Station);
	Meta_AddInteger(Serial);
	Meta_AddInteger(DisplayLed);
	Meta_AddInteger(ScanTime);
	Meta_AddInteger(NetTimeout);
	Meta_AddInteger(SerialTimeout);
	Meta_AddObject (Ethernet);
	Meta_AddCollect(Ports);
	Meta_AddCollect(Services);

	Meta_SetName((IDS_COMMS_MANAGER));
	}

// Implementation

// Config File Help

UINT CEt3CommsManager::BuildDisplayLed(void)
{
	if( m_DisplayLed == 0 ) {
		
		return NOTHING;
		}
	else {
		UINT uData = 0;

		SetBit(uData, m_DisplayLed == 1, 0);
		SetBit(uData, m_DisplayLed == 2, 1);
		SetBit(uData, m_DisplayLed == 1, 8);
		SetBit(uData, m_DisplayLed == 2, 9);

		return uData;
		}
	}

// End of File
