
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3ET3_HPP
	
#define	INCLUDE_G3ET3_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pccore.hpp>

#include <c3core.hpp>

#include <c3ui.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3et3.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Base IDs
//

#define E3_BASE_1   0x0080
#define E3_BASE_2A  0x0081
#define E3_BASE_2B  0x0082
#define E3_BASE_2C  0x0083
#define E3_BASE_2D  0x0084
#define E3_BASE_TC  0x0085
#define E3_BASE_20M 0x0086
#define E3_BASE_MX  0x0087

//////////////////////////////////////////////////////////////////////////
//
// Module IDs
//

#define E3_MOD_MIX24880		0x0010
#define E3_MOD_MIX24882		0x0011
#define E3_MOD_32DI24		0x0012
#define E3_MOD_16DI24		0x0013
#define E3_MOD_16DO24		0x0014
#define E3_MOD_16AI20M		0x0016
#define E3_MOD_16ISOTC		0x0017
#define E3_MOD_16ISO20M		0x0017
#define E3_MOD_8ISOTC		0x0018
#define E3_MOD_10RTD		0x0019
#define E3_MOD_16DIAC		0x001A
#define E3_MOD_32DO24		0x001B
#define E3_MOD_32AI20M		0x001C
#define E3_MOD_32AI10V		0x001D
#define E3_MOD_8AO20M		0x001E
#define E3_MOD_16AI8AO		0x001F
#define E3_MOD_16DORLY		0x0021
#define E3_MOD_MIX24884		0x0023

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3ET3

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3et3.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CEt3CommsSystem;
class CEt3CommsManager;
class CEt3CommsPort;
class CEt3CommsPortSerial;
class CEt3CommsPortNetwork;
class CEt3CommsPortList;
class CEt3EthernetItem;
class CEt3EthernetFace;
class CEt3CommsDeviceList;
class CEt3CommsDevice;
class CChannelManager;
class CEt3Services;
class CWebServerItem;
class CWatchdogItem;
class CEt3SecurityManager;
class CIOManager;
class CTransferManager;
class CTransferBlockItem;
class CTransferBlockList;

//////////////////////////////////////////////////////////////////////////
//
// File Data Interface
//

interface IFileData
{
	public:
		virtual void	Release(void)					= 0;
		virtual PCSTR	GetFile(void)					= 0;
		virtual BOOL	IsValid(void)					= 0;
		virtual void	SetValid(BOOL fValid)				= 0;

		virtual void	Create(UINT uSize)				= 0;

		virtual PBYTE	GetDataBuffer(void)				= 0;
		virtual UINT	GetDataSize(void)				= 0;

		virtual BYTE	GetByte(UINT &uPtr)				= 0;
		virtual WORD	GetWord(UINT &uPtr)				= 0;
		virtual DWORD	GetLong(UINT &uPtr)				= 0;
		virtual PCBYTE	GetData(UINT &uPtr, UINT uSize)			= 0;
		virtual CString	GetText(UINT &uPtr, UINT uSize)			= 0;
		
		virtual void	PutByte(UINT &uPtr, BYTE Data)			= 0;		
		virtual void	PutWord(UINT &uPtr, WORD Data)			= 0;
		virtual void	PutLong(UINT &uPtr, DWORD Data)			= 0;
		virtual void	PutData(UINT &uPtr, PCBYTE pData, UINT uSize)	= 0;
		virtual	void    PutText(UINT &uPtr, CString pData, UINT uSize)	= 0;

		virtual void	Dump(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

class DLLAPI CFileData : public IFileData
{
	public:
		// Constructor
		CFileData(PCSTR pFile);

		// IFileData
		void	Release();
		PCSTR	GetFile(void);
		BOOL	IsValid(void);
		void	SetValid(BOOL fValid);
		void	Create(UINT uSize);
		PBYTE	GetDataBuffer(void);
		UINT	GetDataSize(void);

		// Get Operations
		BYTE	GetByte(UINT &uPtr);
		WORD	GetWord(UINT &uPtr);
		DWORD	GetLong(UINT &uPtr);
		PCBYTE	GetData(UINT &uPtr, UINT uSize);
		CString	GetText(UINT &uPtr, UINT uSize);

		// Put Operations
		void	PutByte(UINT &uPtr, BYTE Data);
		void	PutWord(UINT &uPtr, WORD Data);
		void	PutLong(UINT &uPtr, DWORD Data);
		void	PutData(UINT &uPtr, PCBYTE pData, UINT uSize);
		void    PutText(UINT &uPtr, CString Data, UINT uSize);
		void	PutZero(UINT &uPtr, UINT uSize);

		// Development
		void	Dump(void);

	protected:
		// Data
		PCSTR	m_pFile;
		PBYTE	m_pData;
		UINT	m_uSize;
		BOOL	m_fValid;
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/users"
//

class DLLAPI CFileDataBaseCfgUsers : public CFileData
{
	public:
		// Constructor
		CFileDataBaseCfgUsers(void);

		// Development
		void	Dump(void);

		// Operations
		void ClearUsers(void);

		// User
		struct CUser {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);

			// Attributes
			CString GetUsername(void);
			CString GetPassword(void);
			UINT	GetPermissions(void);
			UINT    GetOptions(void);

			// Operations
			void SetUsername(CString Name);
			void SetPassword(CString Name);
			void SetPermissions(UINT uData);
			void SetOptions(UINT uData);

			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Public Data
		CUser	m_Users[8];

	protected:
		// Data
		BYTE	m_Data[80 * 8];

	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/xfers"
//


class DLLAPI CFileDataBaseCfgXfers : public CFileData
{
	public:
		// Constructor
		CFileDataBaseCfgXfers(void);

		// Destructor
		~CFileDataBaseCfgXfers(void);		

		// Operations
		void ClearTransfers(void);

		// Development
		void	Dump(void);

		// Attributes
		UINT GetScanTime(void);
		UINT GetSerialTimeout(void);
		UINT GetNetTimeout(void);

		// Operations
		void PutScanTime(UINT uTime);
		void PutSerialTimeout(UINT uTimeout);
		void PutNetTimeout(UINT uTimeout);

		// Transfer
		struct CTransfer {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);

			// Attributes
			UINT   GetBlockType(void);
			UINT   GetStation(void);
			UINT   GetInterface(void);
			PCBYTE GetIPAddr(void);
			UINT   GetDestPort(void);
			UINT   GetStatusBit(void);
			UINT   GetRegCount(void);
			UINT   GetSrceType(void);
			UINT   GetSrceAddr(void);
			UINT   GetDestType(void);
			UINT   GetDestAddr(void);

			// Operations
			void PutBlockType(UINT uType);
			void PutStation(UINT uStation);
			void PutInterface(UINT uInterface);
			void PutIPAddr(PCBYTE pIPAddr);
			void PutDestPort(UINT uPort);
			void PutStatusBit(UINT uStatus);
			void PutRegCount(UINT uData);
			void PutSrceType(UINT uData);
			void PutSrceAddr(UINT uData);
			void PutDestType(UINT uData);
			void PutDestAddr(UINT uData);

			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Public Data
		CTransfer	m_Transfers[32];

	protected:
		// Data		

		CString FormatIPAddr(PCBYTE pAddr);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/comms"
//


class DLLAPI CFileDataBaseCfgComms : public CFileData
{
	public:
		// Constructor
		CFileDataBaseCfgComms(void);

		// Destructor
		~CFileDataBaseCfgComms(void);

		// Development
		void	Dump(void);

		// Attributes
		UINT   GetSixnetPortNumber(void);
		UINT   GetModbusPortNumber(void);
		UINT   GetNetConnTimeout(void);
		UINT   GetNetModeJumper(void);
		UINT   GetDisplayLed(void);
		UINT   GetQosConfig(void);
		PCBYTE GetRingConfig(void);
		UINT   GetWebPort(void);
		UINT   GetWebOptions(void);

		// Operations
		void SetSixnetPortNumber(UINT uData);
		void SetModbusPortNumber(UINT uData);
		void SetNetConnTimeout(UINT uData);
		void SetNetModeJumper(UINT uData);		
		void SetDisplayLed(UINT uData);
		void SetQosConfig(UINT uData);
		void SetRingConfig(PCBYTE pData);
		void SetWebPort(UINT uData);
		void SetWebOptions(UINT uData);		

		// Ethernet
		struct CEthernet {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);

			// Attributes
			UINT   GetSettings(void);
			PCBYTE GetOptions(void);
			PCBYTE GetAddr(void);
			PCBYTE GetMask(void);
			PCBYTE GetGate(void);
			PCBYTE GetDNS1(void);
			PCBYTE GetDNS2(void);
			PCBYTE GetSecList(void);

			// Operations
			void SetSettings(UINT uData);
			void SetOptions(PCBYTE pData);
			void SetAddr(PCBYTE pAddr);
			void SetMask(PCBYTE pAddr);
			void SetGate(PCBYTE pAddr);
			void SetDNS1(PCBYTE pAddr);
			void SetDNS2(PCBYTE pAddr);
			void SetSecList(PCBYTE pAddr);

			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Serial
		struct CSerial {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);

			// Attributes
			UINT GetBaud(void);
			UINT GetMode(void);
			UINT GetProtocol(void);
			UINT GetHand(void);
			UINT GetPassThru(void);
			UINT GetLead(void);
			UINT GetLag(void);
			UINT GetGap(void);

			// Operations
			void SetBaud(UINT uData);
			void SetMode(UINT uData);
			void SetProt(UINT uData);
			void SetHand(UINT uData);
			void SetPass(UINT uData);
			void SetLead(UINT uData);
			void SetLag (UINT uData);
			void SetGap (UINT uData);

			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Public Data
		CEthernet	m_Ethernet[5];
		CSerial		m_Serial[4];

	protected:

		// Implementaiton
		CString FormatAddr(PCBYTE pAddr);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/module0/arc/factory"
//


class DLLAPI CFileDataModuleArcFactory : public CFileData
{
	public:
		// Constructor
		CFileDataModuleArcFactory(void);

		// Attributes
		UINT GetSerialNumber(void);
		UINT GetType(void);
		UINT GetRevision(void);
		UINT GetSMIDNumber(void);
		UINT GetFirmwareRevision(void);
		UINT GetFirmwareLength(void);
		UINT GetFirmwareChecksum(void);
		UINT GetBootloaderVersion(void);
		UINT GetProgrammableLogicVersion(void);

	protected:
		// Data
		BYTE	m_Data[66];
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/setup"
//

class DLLAPI CFileDataBaseCfgSetup : public CFileData
{
	public:
		// Constructor
		CFileDataBaseCfgSetup(void);

		// Development
		void	Dump(void);

		// Attributes
		UINT    GetStationNumber(void);
		UINT	GetWatchdogOutputEnables(void);
		UINT	GetOutputIOTimeout(void);
		UINT	GetIOTimeoutActions(void);
		UINT	GetModbusDiscreteIOForce(void);
		UINT	GetAnalogInputFiltering(void);
		UINT	GetTemperatureReporting(void);
		UINT	GetNetworkConfigJumper(void);
		UINT	GetBaseSourceSinkJumper(void);
		PCBYTE	GetTimerCounterTimebases(void);
		UINT	GetDiscreteInputFiltering(void);
		PCBYTE	GetModuleSourceSinkJumper(void);
		UINT	GetTPOInterval(void);
		UINT	GetTPOPeriod(void);
		UINT	GetTPOMinimum(void);
		UINT	GetHBDiscreteRegister(void);
		UINT	GetHBBootTimeout(void);
		UINT	GetHBTimeout(void);
		UINT	GetHBManualResetRegister(void);
		UINT	GetHBFeatureEnables(void);
		UINT	GetCounterResetEnables(void);
		PCBYTE  GetAnalogInputRange(void);
		PCBYTE  GetAnalogOutputRange(void);
		PCBYTE	GetDiscreteInputRange(void);
		PCBYTE	GetDiscreteOutputRange(void);		
		CString GetStationName(void);
		UINT	GetDICount(void);
		UINT	GetDOCount(void);
		UINT	GetAICount(void);
		UINT	GetAOCount(void);
		UINT    GetSerialNumber(void);

		// Operations
		void	SetStationNumber(UINT uData);
		void	SetWatchdogOutputEnables(UINT uData);
		void	SetOutputIOTimeout(UINT uData);
		void	SetIOTimeoutActions(UINT uData);
		void	SetModbusDiscreteIOForce(UINT uData);
		void	SetAnalogInputFiltering(UINT uData);
		void	SetTemperatureReporting(UINT uData);
		void	SetNetworkConfigJumper(UINT uData);
		void	SetBaseSourceSinkJumper(UINT uData);
		void	SetTimerCounterTimebases(PCBYTE pData);
		void	SetDiscreteInputFiltering(UINT uData);
		void	SetModuleSourceSinkJumper(PCBYTE pData);
		void	SetTPOInterval(UINT uData);
		void	SetTPOPeriod(UINT uData);
		void	SetTPOMinimum(UINT uData);
		void	SetHBDiscreteRegister(UINT uData);
		void	SetHBBootTimeout(UINT uData);
		void	SetHBTimeout(UINT uData);
		void	SetHBManualResetRegister(UINT uData);
		void	SetHBFeatureEnables(UINT uData);
		void	SetCounterResetEnables(UINT uData);
		void	SetStationName(CString Name);
		void    SetSerialNumber(UINT uData);

		// Timebasess
		struct CTimebase {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);
			
			// Attributes
			DWORD	GetTime(void);

			// Operations
			void	SetTime(UINT uData);

			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Analog Inputs
		struct CAnalogInput {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);
			
			// Attributes
			BYTE	GetRange(void);

			// Operations
			void	SetRange(UINT uData);

			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Analog Output
		struct CAnalogOutput {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);
			
			// Attributes
			BYTE	GetRange(void);

			// Operations
			void	SetRange(UINT uData);

			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Discrete Inputs
		struct CDiscreteInput {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);

			// Attributes
			UINT GetConfig(void);

			// Operations
			void SetConfig(UINT uData);
			
			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Discrete Outputs
		struct CDiscreteOutput {

			// Initialization
			void SetBase(UINT uBase);
			void SetFile(IFileData *pData);

			// Attributes
			UINT GetConfig(void);

			// Operations
			void SetConfig(UINT uData);
			
			// Data
			UINT        m_uBase;
			IFileData * m_pData;
			};

		// Public Data
		CAnalogInput	m_AIs[32];
		CAnalogOutput	m_AOs[32];
		CDiscreteInput	m_DIs[32];
		CDiscreteOutput	m_DOs[32];
		CTimebase	m_Timebases[32];

	protected:
		// Data
		BYTE	m_Data[464];
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/tags"
//

class DLLAPI CFileDataBaseCfgTags : public CFileData
{
	public:
		// Constructor
		CFileDataBaseCfgTags(void);

		// Development
		void	Dump(void);


	protected:
		// Data
		BYTE	m_Data[16];
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "base0/arc/factory"
//

class DLLAPI CFileDataBaseArcFactory : public CFileData
{
	public:
		// Constructor
		CFileDataBaseArcFactory(void);

		// Attributes
		UINT GetBaseSN(void);
		UINT GetBaseType(void);
		UINT GetBaseRevision(void);
		UINT GetModuleId(void);
		void GetMacId(PBYTE pData);

	protected:
		// Data
		BYTE	m_Data[38];
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/tmp/image"
//

class DLLAPI CFileDataBaseTmpImage : public CFileData
{
	public:
		// Constructor
		CFileDataBaseTmpImage(void);
		CFileDataBaseTmpImage(CInitData &Data);

		// Destructor
		~CFileDataBaseTmpImage(void);

		// Operations
		void Create(CInitData &Data);
		void Create(UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/temp/firmware"
//

class DLLAPI CFileDataBaseFirmware : public CFileData
{
	public:
		// Constructor
		CFileDataBaseFirmware(void);

		// Destructor
		~CFileDataBaseFirmware(void);

		// Operations
		void Create(UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item
//

class DLLAPI CEt3CommsSystem : public CSystemItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEt3CommsSystem(void);

		// Destructor
		~CEt3CommsSystem(void);

		// Attributes
		BOOL HasFlag(CString Name) const;
		UINT NextTransferIndex(void);
		UINT GetTransfer(void);

		// Operations
		void AddTransfer(void);
		void DelTransfer(void);

		// Overridables
		virtual UINT GetModuleIdent(void)		= 0;
		virtual UINT GetModuleBase(void)		= 0;
		virtual UINT GetPhysDIs(void)			= 0;
		virtual UINT GetPhysDOs(void)			= 0;
		virtual UINT GetPhysAIs(void)			= 0;
		virtual UINT GetPhysAOs(void)			= 0;
		virtual UINT GetNumCounters(void)		= 0;
		virtual UINT GetNumHiSpeedCounters(void)	= 0;
		virtual UINT GetNumTPOs(void)			= 0;

		virtual CString GetAIClass(void)		= 0;
		virtual CString GetAOClass(void)		= 0;
		virtual CString GetDIClass(void)		= 0;
		virtual CString GetDOClass(void)		= 0;

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Version Test
		BOOL NeedNewSoftware(void) const;

		// Conversion
		CString GetSpecies(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);
		void Save(CTreeFile &Tree);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT		      m_Format;
		UINT		      m_Major;
		UINT		      m_Level;
		UINT		      m_Revision;
		CEt3CommsManager    * m_pComms;
		CIOManager          * m_pChannel;
		CTransferManager    * m_pTransfer;
		CEt3SecurityManager * m_pSecurity;

		// Configuration Files
		CFileDataBaseCfgSetup	m_CfgSetup;
		CFileDataBaseCfgXfers	m_CfgXfers;
		CFileDataBaseCfgComms	m_CfgComms;
		CFileDataBaseCfgUsers	m_CfgUsers;
		CFileDataBaseCfgTags	m_CfgTags;

		CArray <IFileData *>	m_Files;

	protected:
		// Types
		typedef CTree  <CString>       CFlagMap;

		// Data
		CFlagMap	m_Flags;
		UINT		m_uIndex;
		UINT		m_uCount;
		DWORD		m_dwStatus;

		// System Data
		void AddNavCats(void);
		void AddResCats(void);

		// System Data
		void AddNavCatHead(void);
		void AddNavCatTail(void);
		void AddResCatHead(void);
		void AddResCatTail(void);

		// Port Creation
		virtual void MakePorts(void);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL CheckFormat(void);

		void BuildFileList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Et3 UI Item
//

class DLLAPI CEt3UIItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3UIItem(void);

		// Useful
		CString FormatIPAddr(PCBYTE pAddr);		
		BOOL    LimitEnum(IUIHost *pHost, CString Tag, UINT &uData, UINT uMask);

		// Persistence
		void Init(void);
		void PostLoad(void);

	protected:
		// Data
		CEt3CommsSystem * m_pSystem;
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Manager
//

class /*DLLAPI*/ CEt3CommsManager : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsManager(void);

		// Item Lookup
		CEt3CommsPort       * FindPort(UINT uPort) const;
		CEt3CommsDevice     * FindDevice(UINT uDevice) const;

		// Port List Access
		BOOL GetPortList(CEt3CommsPortList * &pList, UINT uIndex) const;

		// Operations
		UINT AllocPortNumber(void);
		UINT AllocDeviceNumber(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Item Naming
		CString GetHumanName(void) const;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT		       m_Handle;
		UINT		       m_Serial;
		UINT		       m_Station;
		UINT		       m_DisplayLed;
		UINT		       m_ScanTime;
		UINT		       m_NetTimeout;
		UINT		       m_SerialTimeout;
		CString		       m_Name;
		CEt3EthernetItem     * m_pEthernet;
		CEt3CommsPortList    * m_pPorts;
		CEt3Services         * m_pServices;

	protected:
		// Data Members
		UINT		  m_MaxPort;
		UINT		  m_MaxDevice;
		UINT		  m_MaxTransfer;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation

		// Config File Help
		UINT BuildDisplayLed(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Configuration
//

class /*DLLAPI*/ CEt3EthernetItem : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3EthernetItem(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);
		void PostLoad(void);

		// Download Support
		void PrepareData(void);

		// Item Properties		
		CEt3CommsPortList  * m_pPorts;

		// Operations
		void MakePorts(void);
		BOOL SaveDefaultIP(void);
		void SetSendPort(void);
		void UseSendPort(BOOL fSet);

		// Item Properties
		UINT		     m_DualMode;
		UINT		     m_Priority;
		UINT		     m_ConnTimeoutSelect;
		UINT		     m_ConnTimeout;
		UINT		     m_QoSConfig;
		UINT		     m_DownMode;
		UINT		     m_DownIP;
		UINT		     m_DownPort;
		CEt3EthernetFace   * m_pFace0;
		CEt3EthernetFace   * m_pFace1;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		// Config File Help
		UINT BuildQoSConfig(void);
		UINT BuildConnTimeout(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Port Configuration
//

class /*DLLAPI*/ CEt3EthernetFace : public CEt3UIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEt3EthernetFace(UINT uPort);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		DWORD GetAddress(void) const;

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_PortMode;
		UINT	m_Address;
		UINT	m_NetMask;
		UINT	m_Gateway;
		UINT	m_Secure;
		
	protected:
		// Data Members
		UINT m_uPort;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void FindAutoAddress(void);

		// Config File Help
		PCBYTE BuildOptions(void);
		UINT   BuildSettings(void);
		PCBYTE BuildIPAddr(UINT uAddr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Port List
//

class CEt3CommsPortList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsPortList(void);
		CEt3CommsPortList(UINT uLimit);

		// Item Access
		CEt3CommsPort * GetItem(INDEX Index) const;
		CEt3CommsPort * GetItem(UINT  uPort) const;

		// Device Access
		CEt3CommsPort      * FindPort(UINT uPort) const;
		CEt3CommsDevice    * FindDevice(UINT uDevice) const;

	protected:
		// Data Members
		UINT m_uLimit;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEt3CommsDriver;

//////////////////////////////////////////////////////////////////////////
//
// Driver Base Class
//

class CEt3CommsDriver : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsDriver(void);

		// Attributes
		UINT    GetIdent(void);
		BOOL	IsModbus(void);
		BOOL	IsSixnet(void);
		BOOL    IsMaster(void);
		BOOL    IsSlave(void);
		CString GetShortName(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		void PrepareData(void);

		// Drivers
		enum {
			protDisabled	 = 0,
			protModbusMaster,
			protSixnetMaster,
			protSixnetSlave,
			protRtuSlave,
			protAscSlave,
			};

		// Item Properties
		CString	m_Name;
		UINT	m_Ident;
		UINT	m_Flavor;
		UINT    m_PassEnable;
		UINT	m_PassTimeout;

	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Config File Help
		UINT BuildPass(void);
		UINT BuildProt(void);

		// Implementation
		void CheckFlavor(IUIHost *pHost);
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

class DLLAPI CEt3CommsPort : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsPort(void);

		// Attributes
		virtual UINT    GetTreeImage(void) const;
		virtual CString GetTreeLabel(void) const;

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Item Naming
		CString GetItemOrdinal(void) const;

		// Item Lookup
		CEt3CommsDevice    * FindDevice(UINT uDevice) const;

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);

		// Item Properties
		CString	              m_Name;
		UINT                  m_Number;
		CEt3CommsDeviceList * m_pDevices;

	protected:
		// Data Members
		UINT           m_uImage;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL AllocNumber(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Serial
//

class DLLAPI CEt3CommsPortSerial : public CEt3CommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsPortSerial(void);

		// Attributes
		CString GetTreeLabel(void) const;

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		void PrepareData(void);

		// Persistance
		void Init(void);

		// Item Properties
		UINT		  m_Protocol;
		UINT		  m_BaudRate;
		UINT		  m_DataBits;
		UINT		  m_Parity;
		UINT		  m_StopBits;
		UINT		  m_LeadTime;
		UINT		  m_LagTime;
		UINT		  m_GapTime;
		CEt3CommsDriver * m_pDriver;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		BOOL UpdateDriver(void);
		void CheckDevices(IUIHost *pHost);

		// Config File Help
		UINT BuildMode(void);
		UINT BuildHand(CFileDataBaseCfgComms::CSerial &Port);
		UINT BuildPass(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Network
//

class CEt3CommsPortNetwork : public CEt3CommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsPortNetwork(void);

		// Persistance
		void Init(void);

		// Item Properties
		UINT	m_Port;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Network Modbus
//

class CEt3CommsPortNetworkModbus : public CEt3CommsPortNetwork
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsPortNetworkModbus(void);

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Network SixNet
//

class CEt3CommsPortNetworkSixnet : public CEt3CommsPortNetwork
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsPortNetworkSixnet(void);

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Device List
//

class CEt3CommsDeviceList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsDeviceList(void);

		// Item Access
		CEt3CommsDevice * GetItem(INDEX   Index) const;
		CEt3CommsDevice * GetItem(UINT    uPos ) const;
		CEt3CommsDevice * GetItem(CString Name ) const;

		// Item Lookup
		CEt3CommsDevice    * FindDevice(UINT uDevice) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Device
//

class CEt3CommsDevice : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsDevice(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Location
		CEt3CommsDeviceList * GetParentList(void) const;
		CEt3CommsPort       * GetParentPort(void) const;

		// Attributes
		UINT GetTreeImage(void) const;

		// Persistance
		void Init(void);

		// Item Properties
		CString			  m_Name;
		UINT			  m_Number;
		UINT			  m_Station;
		UINT			  m_IPAddr;
		UINT			  m_IPPort;
		CTransferBlockList	* m_pBlocks;

	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL AllocNumber(void);
		void DoEnables(IUIHost *pHost);
	};


//////////////////////////////////////////////////////////////////////////
//
// Heartbeat/Watchdog Item
//

class CHeartbeatItem : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHeartbeatItem(void);

		// Attributes
		UINT    GetTreeImage(void) const;

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_Enables;
		UINT	m_EnablesPower1Fail;
		UINT	m_EnablesPower2Fail;
		UINT	m_EnablesPoEFail;
		UINT	m_EnablesNetRingFail;
		UINT	m_EnablesCPUMonitor;
		UINT	m_EnablesIOPollTimeout;
		UINT	m_EnablesHeartTimeout;
		UINT	m_IOEnable;
		UINT	m_IOTimeout;
		UINT	m_IODropPhys;
		UINT	m_IODropFirst;
		UINT	m_IODropVirt;
		UINT	m_HBEnable;
		UINT	m_HBType;
		UINT	m_HBAddr;
		UINT	m_HBTimeout;
		UINT	m_HBDelay;
		UINT	m_HBDropPhys;
		UINT	m_HBDropFirst;
		UINT	m_HBDropVirt;
		BOOL	m_fResetEnable;
		UINT	m_ResetAddr;
		UINT	m_ResetType;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		//
		WORD BuildHBEnables(void);
		BYTE BuildIOActions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Watchdog Item
//

class CWatchdogItem : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWatchdogItem(void);

		// Attributes
		UINT    GetTreeImage(void) const;

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_Power1Fail;
		UINT	m_Power2Fail;
		UINT	m_PoEFail;
		UINT	m_RingFail;
		UINT	m_CPUMonitor;
		UINT	m_IOPollEnable;
		UINT	m_HeartEnable;
		UINT	m_HeartType;
		UINT	m_HeartAddr;
		UINT	m_IOPollTimeout;
		UINT	m_IODropPhysOutput;
		UINT	m_IODropFirstDO;
		UINT	m_IODropVirtualOut;
		UINT	m_HBBootDelay;
		UINT	m_HBMaxTimeout;
		UINT	m_HBDropPhysOutput;
		UINT	m_HBDropFirstDO;
		UINT	m_HBDropVirtualOut;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		//
		WORD BuildWatchdogEnables(void);
		BYTE BuildIOTimeoutActionFlags(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Web Server Item
//

class CWebServerItem : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWebServerItem(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		UINT    GetTreeImage(void) const;

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_Enable;
		UINT	m_Port;
		UINT	m_Download;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		// Config File Help
		UINT BuildWebOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Services Configuration
//

class CEt3Services : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3Services(void);

		// Attributes
		UINT GetTreeImage(void) const;

		// Persistance
		void Init(void);

		// Item Properties
		CItemIndexList * m_pList;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Service Creation
		void AddServices(void);
	};

// End of File

#endif
