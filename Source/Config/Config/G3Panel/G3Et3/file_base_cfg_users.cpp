
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/cfg/users"
//

// Constructor

CFileDataBaseCfgUsers::CFileDataBaseCfgUsers(void) : CFileData("/base0/cfg/users")
{	
	m_pData = m_Data;

	m_uSize = sizeof(m_Data);

	for( UINT n = 0; n < elements(m_Users); n ++ ) {

		CUser &User = m_Users[n];

		User.SetBase(0x0000 + n * 80);

		User.SetFile(this);
		}
	}

// Development

void CFileDataBaseCfgUsers::Dump(void)
{
	AfxTrace(L"===============\n");

	AfxTrace(L"Properties for file %s\n", CString(GetFile()));

	for( UINT n = 0;  n < 8; n ++ ) {

		CUser &User = m_Users[n];

		AfxTrace(L" %d. namename\t[%s]\n",    n, User.GetUsername());
		AfxTrace(L" %d. password\t[%s]\n",    n, User.GetPassword());
		AfxTrace(L" %d. permission\t%4.4X\n", n, User.GetPermissions());
		AfxTrace(L" %d. options\t\t%2.2X\n",  n, User.GetOptions());
		}

	AfxTrace(L"===============\n");
	}

// Initialization

void CFileDataBaseCfgUsers::CUser::SetFile(IFileData *pData)
{
	m_pData = pData;
	}

void CFileDataBaseCfgUsers::CUser::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

// Attributes

CString CFileDataBaseCfgUsers::CUser::GetUsername(void)
{
	UINT uPtr = m_uBase + 0x0000;

	PCBYTE pData = m_pData->GetData(uPtr, 16);

	return CString(PCSTR(pData));
	}

CString CFileDataBaseCfgUsers::CUser::GetPassword(void)
{
	// TODO -- implement crypto

	UINT uPtr = m_uBase + 0x0020;

	PCBYTE pData = m_pData->GetData(uPtr, 16);

	return CString(PCSTR(pData));
	}

UINT CFileDataBaseCfgUsers::CUser::GetPermissions(void)
{
	UINT uPtr = m_uBase + 0x0040;

	return m_pData->GetWord(uPtr);
	}

UINT CFileDataBaseCfgUsers::CUser::GetOptions(void)
{
	UINT uPtr = m_uBase + 0x0042;

	return m_pData->GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgUsers::ClearUsers(void)
{
	memset(m_pData, 0, m_uSize);
	}

void CFileDataBaseCfgUsers::CUser::SetUsername(CString User)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutText( uPtr, User, 32);
	}

void CFileDataBaseCfgUsers::CUser::SetPassword(CString Pass)
{
	UINT uPtr = m_uBase + 0x0020;

	m_pData->PutText( uPtr, Pass, 32);
	}

void CFileDataBaseCfgUsers::CUser::SetPermissions(UINT uData)
{
	UINT uPtr = m_uBase + 0x0040;

	m_pData->PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgUsers::CUser::SetOptions(UINT uData)
{
	UINT uPtr = m_uBase + 0x0042;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

// End of File
