
#include "intern.hpp"

#include "io.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IO Navigation Window
//

class CIONavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CIONavTreeWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Notification Handlers
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		void NewItemSelected(void);
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);

		// Lock Support
		BOOL IsItemLocked(HTREEITEM hItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Navigation Window
//
	
// Dynamic Class

AfxImplementDynamicClass(CIONavTreeWnd, CNavTreeWnd);
		
// Constructor

CIONavTreeWnd::CIONavTreeWnd(void) : CNavTreeWnd( L"IOs",
						  NULL,
						  AfxRuntimeClass(CIOItem)
						  )
{
	}

// Message Map

AfxMessageMap(CIONavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit)

	AfxMessageEnd(CIONavTreeWnd)
	};

// Message Handlers

void CIONavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"IONavTreeTool"));
		}
	}

// Notification Handlers

BOOL CIONavTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{	
	return TRUE;
	}

// Tree Loading

void CIONavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"Et3Tree16"), afxColor(MAGENTA));
	}

// Item Hooks

void CIONavTreeWnd::NewItemSelected(void)
{
	CNavTreeWnd::NewItemSelected();
	}

UINT CIONavTreeWnd::GetRootImage(void)
{
	// TODO -- images

	return IDI_IO;
	}

UINT CIONavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	// TODO -- images

	if( pItem->IsKindOf(m_Class) ) {
		
		return ((CIOItem *) pItem)->GetTreeImage();
		}

	return 70;
	}

// Item Locking

BOOL CIONavTreeWnd::IsItemLocked(HTREEITEM hItem)
{
	return TRUE;
	}

// End of File

