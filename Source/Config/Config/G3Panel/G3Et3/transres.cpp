
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Transfer Resource Window
//

class CTransferResTreeWnd : public CResTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CTransferResTreeWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Transfer Resource Window
//
	
// Dynamic Class

AfxImplementDynamicClass(CTransferResTreeWnd, CResTreeWnd);
		
// Constructor

CTransferResTreeWnd::CTransferResTreeWnd(void) : CResTreeWnd( L"Transfers",
						 NULL,
						 AfxRuntimeClass(CTransferItem)
						 )
{
	}

// Message Map

AfxMessageMap(CTransferResTreeWnd, CResTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxMessageEnd(CTransferResTreeWnd)
	};

// Message Handlers

void CTransferResTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"TransferResTreeTool"));
		}
	}

// Tree Loading

void CTransferResTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"Et3Tree16"), afxColor(MAGENTA));
	}

UINT CTransferResTreeWnd::GetRootImage(void)
{
	return IDI_TRANSFER_GROUP;
	}

UINT CTransferResTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return ((CTransferItem *) pItem)->GetTreeImage();
	}

// End of File

