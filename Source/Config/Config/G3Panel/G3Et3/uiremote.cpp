
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Communications Device
//

class CUITextRemoteDevice : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextRemoteDevice(void);

	protected:
		// Data
		UINT m_Type;

		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(void);
		void AddData(UINT Data, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Communications Device
//

// Dynamic Class

AfxImplementDynamicClass(CUITextRemoteDevice, CUITextEnum);

// Constructor

CUITextRemoteDevice::CUITextRemoteDevice(void)
{
	}

// Overridables

void CUITextRemoteDevice::OnBind(void)
{
	CUITextEnum::OnBind();

	AddData(NOTHING, L"None");
	
	AddData();
	}

// Implementation

void CUITextRemoteDevice::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	m_Enum.Append(Enum);
	}

void CUITextRemoteDevice::AddData(void)
{
	CEt3CommsSystem *pSystem = (CEt3CommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CEt3CommsManager *pComms = pSystem->m_pComms;

	CEt3CommsPortList *pPorts;

	for( UINT s = 0; pComms->GetPortList(pPorts, s); s ++ ) {

		for( INDEX i = pPorts->GetHead(); !pPorts->Failed(i); pPorts->GetNext(i) ) {

			CEt3CommsPort          *pPort = pPorts->GetItem(i);

			CEt3CommsDeviceList *pDevices = pPort->m_pDevices;

			for( INDEX d = pDevices->GetHead(); !pDevices->Failed(d); pDevices->GetNext(d) ) {

				CEt3CommsDevice  *pDevice = pDevices->GetItem(d);

				UINT  uIndex = pDevice->m_Number;

				CString Name = pDevice->m_Name;

				AddData(uIndex, Name);
				}
			}
		}
	}

// End of File
