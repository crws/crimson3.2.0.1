
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/module0/tmp/firmware"
//

// Constructor

CFileDataBaseFirmware::CFileDataBaseFirmware(void) : CFileData("/module0/tmp/firmware.bin")
{	
	m_pData	= NULL;

	m_uSize = 0;
	}

// Destructor

CFileDataBaseFirmware::~CFileDataBaseFirmware(void)
{	
	if( m_pData ) {

		delete [] m_pData;

		m_pData = NULL;

		m_uSize = 0;
		}
	}

// Operations

void CFileDataBaseFirmware::Create(UINT uSize)
{
	AfxAssert(m_pData == NULL);

	m_uSize = uSize;

	m_pData = New BYTE [ m_uSize ];
	}

// End of File

