
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3et3.hpp"

#include "transfer.hpp"

#include "security.hpp"

#include "io.hpp"

#include "block.hpp"

#include <wincrypt.h>

// Bit Setting Macros

#define SetBit(d, v, n)	((v) ? (d) |= (1 << n) : (d) &= ~(1 << n))

//////////////////////////////////////////////////////////////////////////
//
// Wide to Ansi String Conversion
//

extern LPSTR WideToAnsi(PCTXT pText);
extern LPSTR WidetoAnsi(CString Text);

//////////////////////////////////////////////////////////////////////////
//
// Ansi String Class
//

class CAnsiString
{
	public:
		// Constructor
		CAnsiString(PCTXT pText);

		// Destructor
		~CAnsiString(void);

		// Attributes
		UINT GetLength(void);

		// Conversion
		operator LPCSTR (void) const;

	protected:
		// Data
		LPSTR	m_pText;
	};

//////////////////////////////////////////////////////////////////////////
//
// RFC 2617 compliant encryption
//

class CCrypt
{
	public:
		// Constructor
		CCrypt(CString One, CString Two);

		// Destructor
		~CCrypt(void);

		// Attribute
		PCBYTE GetHash(void);

		// Operation
		CString AsText(void);

	protected:
		// Data
		HCRYPTPROV	m_Prov;
		HCRYPTHASH	m_Hash;
		CString		m_Realm;
		CStringArray	m_Salt;
		CString		m_Work;
		BYTE		m_bHash[16];

		// Implementation
		void Build(void);

		// Wrapper
		BOOL AcquireContext(void);
		BOOL CreateHash(void);
		BOOL HashData(void);
		BOOL GetHashParam(void);
	};

// End of File

#endif
