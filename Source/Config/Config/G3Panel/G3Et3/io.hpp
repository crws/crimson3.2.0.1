
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IO_HPP
	
#define	INCLUDE_IO_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CEt3CommsItem;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CIOManager;
class CIOList;
class CIOItem;

//////////////////////////////////////////////////////////////////////////
//
// IO Manager
//

class CIOManager : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIOManager(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Loading
		BOOL OnLoadPages(CUIPageList *pList);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Item Properties
		UINT            m_Handle;
		CIOList       * m_pIOs;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Impelmentation
		void AddIOs(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO List
//

class CIOList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIOList(void);

		// Item Search
		CIOItem * FindItem(CLASS Class);

		// Item Access
		CIOItem * GetItem(INDEX Index) const;
		CIOItem * GetItem(UINT uPos) const;

		// Operations
		BOOL AppendItem(CItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item
//

class CIOItem : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIOItem(void);

		// Overridables
		virtual UINT GetTreeImage(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Item Properties
		CString		m_Name;

	protected:
		// Data Members
		CIOManager * m_pManager;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindManager(void);
		};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CAnalogInputBaseItem;

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Discrete Input
//

class CDiscreteInputItem : public CIOItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDiscreteInputItem(void);

		// Overridables
		UINT GetTreeImage(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_SourceSink;
		UINT	m_Filter;
		UINT	m_Counter;
		UINT	m_Channel[32];
		UINT	m_Timebase[32];

	public:
		// Public Data
		BOOL	m_fTimebase;

	protected:
		// Data
		BOOL	m_fIs16DIAC;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		CAnalogInputBaseItem * FindAI(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Discrete Output
//

class CDiscreteOutputItem : public CIOItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDiscreteOutputItem(void);

		// Overridables
		UINT GetTreeImage(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_UseLast8;
		UINT	m_TPOEnable;
		UINT	m_TPOCycle;
		UINT	m_TPOMinimum;
		UINT	m_Mode[32];
		UINT	m_Reset[32];

	public:
		// Public Data
		BOOL	m_fHasTPO;

	protected:
		// Data Members

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void CheckUseLast8(IUIHost *pHost);
		void CheckTPOEnable(IUIHost *pHost);
		BOOL UseLast8(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Discrete Output
//

class CDiscreteOutputTPOItem : public CDiscreteOutputItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDiscreteOutputTPOItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Discrete Output
//

class CDiscreteOutputRelayItem : public CDiscreteOutputItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDiscreteOutputRelayItem(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

class CAnalogInputBaseItem : public CIOItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogInputBaseItem(void);

		// Overridables
		UINT GetTreeImage(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_Integration;
		UINT	m_TempFormat;
		UINT	m_TempUnits;
		UINT	m_Range   [32];
		UINT	m_Feature1[32];
		UINT	m_Feature2[32];
		UINT	m_Timebase[32];

	public:
		// Public Data
		BOOL		     m_fTimebase;

	protected:
		// Data
		BOOL	m_fIs16Iso20m;

		// Type
		enum {
			typeVoltage	 = 0,
			typeThermocouple = 1,
			typeCurrent	 = 5,
			typeRTD		 = 6,
			};

		// Data
		UINT	m_uType;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

class CAnalogInputTCItem : public CAnalogInputBaseItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogInputTCItem(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

class CAnalogInputVItem : public CAnalogInputBaseItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogInputVItem(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

class CAnalogInputIItem : public CAnalogInputBaseItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogInputIItem(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties

	protected:

		// Implementation
		void DoEnables(IUIHost *pHost);
		CDiscreteInputItem * FindDI(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

class CAnalogInputRTDItem : public CAnalogInputBaseItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogInputRTDItem(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties

	protected:

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

class CAnalogInputItem : public CIOItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogInputItem(void);

		// Overridables
		UINT GetTreeImage(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_Integration;
		UINT	m_TempFormat;
		UINT	m_TempUnits;
		UINT	m_EnableCounters;
		UINT	m_Range[32];
		UINT	m_Scale[32];
		UINT	m_Resolution[32];

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Output
//

class CAnalogOutputItem : public CIOItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogOutputItem(void);

		// Overridables
		UINT GetTreeImage(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistence
		void Init(void);

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT	m_Range[32];

	protected:
		// Data
		DWORD	m_dwMask;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Output
//

class CAnalogOutputMix20882Item : public CAnalogOutputItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogOutputMix20882Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Output
//

class CAnalogOutputMix20884Item : public CAnalogOutputItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogOutputMix20884Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Output
//

class CAnalogOutputAO20MItem : public CAnalogOutputItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnalogOutputAO20MItem(void);
	};

// End of File

#endif
