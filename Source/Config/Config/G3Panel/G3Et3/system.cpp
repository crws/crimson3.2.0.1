
#include "intern.hpp"

#include "..\..\build.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item -- Core
//

// Database Version

#include "../../../../version/dbver.hpp"

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsSystem, CSystemItem);

// Constructor

CEt3CommsSystem::CEt3CommsSystem(void)
{
	m_Format       = 0;

	m_Major        = 0;

	m_Level	       = 1;

	m_Revision     = 0x0204;

	m_pComms       = New CEt3CommsManager;

	m_pChannel     = New CIOManager;

	m_pTransfer    = New CTransferManager;

	m_pSecurity    = New CEt3SecurityManager;

	BuildFileList();

	m_uCount      = 0;
	}

// Destructor

CEt3CommsSystem::~CEt3CommsSystem(void)
{
	}

// Attributes

BOOL CEt3CommsSystem::HasFlag(CString Name) const
{
	return !m_Flags.Failed(m_Flags.Find(Name));
	}

UINT CEt3CommsSystem::NextTransferIndex(void)
{
	return m_uIndex++;
	}

UINT CEt3CommsSystem::GetTransfer(void)
{
	return m_uCount;
	}

// Version Test

BOOL CEt3CommsSystem::NeedNewSoftware(void) const
{
	return DBASE_MAJOR < m_Major;
	}

// Conversion

CString CEt3CommsSystem::GetSpecies(void) const
{
	return L"e3";
	}

// Operations

void CEt3CommsSystem::AddTransfer(void)
{
	m_uCount ++;
	}

void CEt3CommsSystem::DelTransfer(void)
{
	if( m_uCount ) m_uCount --;
	}

// UI Creation

CViewWnd * CEt3CommsSystem::CreateView(UINT uType)
{
	if( uType == viewSystem ) {

		return New CSystemWnd;
		}

	return NULL;
	}

// Persistance

void CEt3CommsSystem::Init(void)
{		
	CSystemItem::Init();

	MakePorts();

	m_pComms->m_pEthernet->MakePorts();	

	AddNavCats();

	AddResCats();

	m_Format = DBASE_FORMAT;

	m_Major  = DBASE_MAJOR;	
	}

void CEt3CommsSystem::PostLoad(void)
{	
	CSystemItem::PostLoad();

	AddNavCats();

	AddResCats();

	CheckFormat();
	}

void CEt3CommsSystem::Save(CTreeFile &Tree)
{
	MakeMax(m_Major, DBASE_MAJOR);

	CSystemItem::Save(Tree);
	}

// Download Support

void CEt3CommsSystem::PrepareData(void)
{
	CFileDataBaseCfgXfers &File = m_CfgXfers;

	File.ClearTransfers();

	m_uIndex = 0;
	}

// System Data

void CEt3CommsSystem::AddNavCats(void)
{
	AddNavCatHead();

	AddNavCatTail();
	}

void CEt3CommsSystem::AddResCats(void)
{
	AddResCatHead();

	AddResCatTail();
	}

void CEt3CommsSystem::AddNavCatHead(void)
{
	CDatabase *pDbase = GetDatabase();

	if( TRUE ) {

		pDbase->AddNavCategory( IDS_COMMS_CAPTION,
					L"Comms",
					0x21000011,
					0x20000011,
					L"Transfer|Comms"
					);
		}
	}

void CEt3CommsSystem::AddNavCatTail(void)
{
	CDatabase *pDbase = GetDatabase();

	if( TRUE ) {

		pDbase->AddNavCategory( IDS_CHANNEL_CAPTION,
					L"Channel",
					0x21000012,
					0x20000012,
					L"Transfer|Comms"
					);
		}

	if( FALSE ) {

		pDbase->AddNavCategory( IDS_TRANSFER_CAPTION,
					L"Transfer",
					0x2100000F,
					0x2000000F,
					L"Transfer"
					);
		}

	if( TRUE ) {

		pDbase->AddNavCategory( IDS_SECURITY_MANAGER,
					L"Security",
					0x21000006,
					0x20000006,
					L"Transfer|Comms"
					);
		}
	}

void CEt3CommsSystem::AddResCatHead(void)
{
	}

void CEt3CommsSystem::AddResCatTail(void)
{
	CDatabase *pDbase = GetDatabase();

	if( TRUE ) {

		pDbase->AddResCategory( IDS_DEVICES,
					L"Comms",
					0x21000000,
					0x20000000
					);
		}

	if( FALSE ) {

		pDbase->AddResCategory( IDS_TRANSFER_CAPTION,
					L"Transfer",
					0x2100000F,
					0x2000000F
					);
		}
	}

// Port Creation

void CEt3CommsSystem::MakePorts(void)
{
	}

// Property Save Filter

BOOL CEt3CommsSystem::SaveProp(CString const &Tag) const
{
	if( Tag == L"Transfer" ) {

		return FALSE;
		}

	return CSystemItem::SaveProp(Tag);
	}

// Meta Data

void CEt3CommsSystem::AddMetaData(void)
{
	CSystemItem::AddMetaData();

	Meta_AddInteger(Format);
	Meta_AddInteger(Major);
	Meta_AddInteger(Revision);
	Meta_AddObject (Comms);
	Meta_AddObject (Channel);
	Meta_AddObject (Transfer);
	Meta_AddObject (Security);

	Meta_SetName((IDS_SYSTEM));
	}

// Implementation

BOOL CEt3CommsSystem::CheckFormat(void)
{
	if( m_Format != DBASE_FORMAT ) {

		m_Format = DBASE_FORMAT;

		return TRUE;
		}

	return FALSE;
	}

void CEt3CommsSystem::BuildFileList(void)
{
	m_Files.Append(&m_CfgSetup);
	m_Files.Append(&m_CfgXfers);
	m_Files.Append(&m_CfgComms);
	m_Files.Append(&m_CfgUsers);
	m_Files.Append(&m_CfgTags );
	}

// End of File
