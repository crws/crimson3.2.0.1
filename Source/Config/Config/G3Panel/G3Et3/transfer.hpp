
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TRANSFER_HPP
	
#define	INCLUDE_TRANSFER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTransferManager;
class CTransferList;
class CTransferItem;

//////////////////////////////////////////////////////////////////////////
//
// Data Logger
//

class CTransferManager : public CEt3UIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferManager(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		void Validate(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		void PrepareData(void);

		// Item Properties
		UINT            m_Handle;
		CTransferList * m_pTransfers;
		UINT            m_ScanTime;
		UINT            m_NetTimeout;
		UINT            m_SerialTimeout;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

class CTransferList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferList(void);

		// Item Access
		CTransferItem * GetItem(INDEX Index) const;
		CTransferItem * GetItem(UINT uPos) const;

		// Persistence
		void Init(void);
		void PostLoad(void);

		// Download Support
		void PrepareData(void);

	protected:
		// Data
		CEt3CommsSystem * m_pSystem;
	};

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item
//

class CTransferItem : public CEt3UIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTransferItem(void);

		// Attributes
		virtual UINT GetTreeImage(void) const	= 0;

		// Operations
		void CheckDevice(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		
		// Item Naming
		CString GetHumanName(void) const;

		// Persistence
		void PostLoad(void);

		// Download Support
		void PrepareData(void);

		// Data Members
		CString		m_Name;
		UINT		m_Direction;
		UINT		m_Remote;
		UINT		m_SrceType;
		UINT		m_SrceAddr;
		UINT		m_DestType;
		UINT		m_DestAddr;
		UINT		m_Count;
		UINT		m_Status;

	protected:
		// Data
		CEt3CommsDevice * m_pDevice;
		UINT		  m_uImage;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void   DoEnables(IUIHost *pHost);

		// Config File Help
		UINT   BuildType(void);
		UINT   BuildStation(void);
		UINT   BuildInterface(void);
		PCBYTE BuildIPAddr(void);
		UINT   BuildDestPort(void);

		CEt3CommsDevice * FindDevice(void);
		BOOL IsDeviceSixnet(void);
		BOOL IsDeviceModbus(void);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Discrete
//

class CTransferDiscreteItem : public CTransferItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferDiscreteItem(void);

		// Attributes
		UINT GetTreeImage(void) const;
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Analog
//

class CTransferAnalogItem : public CTransferItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferAnalogItem(void);

		// Attributes
		UINT GetTreeImage(void) const;
	};

// End of File

#endif
