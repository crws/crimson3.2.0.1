
#include "Intern.hpp"

#include "LinuxDaxConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAx Configuration Applicator
//

// Instantiator

DLLAPI IConfigApplicator * Create_LinuxDaxConfigApplicator(CString Model, IPxeModel *pModel)
{
	return new CLinuxDaxConfigApplicator(Model, pModel);
}

// Constructor

CLinuxDaxConfigApplicator::CLinuxDaxConfigApplicator(CString Model, IPxeModel *pModel)
{
	StdSetRef();

	m_Model  = Model;

	m_pModel = pModel;

	m_pModel->AddRef();
}

// Destructor

CLinuxDaxConfigApplicator::~CLinuxDaxConfigApplicator(void)
{
	AfxRelease(m_pModel);
}

// IUnknown

HRESULT CLinuxDaxConfigApplicator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IConfigApplicator);

	StdQueryInterface(IConfigApplicator);

	return E_NOINTERFACE;
}

ULONG CLinuxDaxConfigApplicator::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxDaxConfigApplicator::Release(void)
{
	StdRelease();
}

// IConfigApplicator

CString CLinuxDaxConfigApplicator::GetDisplayName(void)
{
	return m_Model.Left(4).ToUpper();
}

CString CLinuxDaxConfigApplicator::GetProcessor(void)
{
	return L"LinuxA8";
}

CString CLinuxDaxConfigApplicator::GetModelList(void)
{
	return m_Model.Left(4).ToLower();
}

CString CLinuxDaxConfigApplicator::GetModelInfo(CString Model)
{
	return L"LinuxA8,DistroA8,none";
}

CString CLinuxDaxConfigApplicator::GetEmulatorModel(CSize DispSize)
{
	CString Base = m_Model.Left(4);

	Base += CPrintf("-%4.4u-%3.3u", DispSize.cx, DispSize.cy);

	return Base;
}

BOOL CLinuxDaxConfigApplicator::GetPorts(CStringArray &List, CJsonData *pConfig)
{
	UINT ns = m_pModel->GetObjCount('s');

	if( ns >= 1 ) {

		UINT port1 = tatoi(pConfig->GetValue("base.port1", "0"));

		List.Append(CPrintf(IDS("Serial Port 1,0,0,S0,S%c,Base Unit"), port1 ? '4' : 'P'));
	}

	if( ns >= 2 ) {

		UINT port2 = tatoi(pConfig->GetValue("base.port2", "0"));

		List.Append(CPrintf(IDS("Serial Port 2,1,0,S1,S%c,Base Unit"), port2 ? '4' : '2'));
	}

	if( ns >= 3 ) {

		UINT port3 = tatoi(pConfig->GetValue("base.port3", "1"));

		List.Append(CPrintf(IDS("Serial Port 3,2,0,S2,S%c,Base Unit"), port3 ? '4' : '2'));
	}

	UINT uFrom = List.GetCount();

	ScanForSerial(List, pConfig, uFrom);

	return TRUE;
}

BOOL CLinuxDaxConfigApplicator::GetModules(CStringArray &List, CJsonData *pHard)
{
	UINT nb = m_pModel->GetObjCount('m');

	UINT mn = 0;

	for( UINT n = 0; n < nb; n++ ) {

		CJsonData *pSlot = pHard->GetChild(CPrintf("modules.mlist.%4.4X", n));

		if( pSlot ) {

			UINT uType = UINT(tatoi(pSlot->GetValue("type", "0")));

			if( uType >= 200 ) {

				CString Info;

				Info += CPrintf("%u,", mn++);

				Info += CPrintf("%s%s-%u,", "B", pSlot->GetValue("order", "0"), uType);

				Info += CPrintf("%u,", uType);

				Info += CPrintf("Expansion Module %u", 1 + n);

				List.Append(Info);
			}
		}
	}

	return TRUE;
}

BOOL CLinuxDaxConfigApplicator::HasModules(CJsonData *pHard)
{
	UINT nb = m_pModel->GetObjCount('m');

	for( UINT n = 0; n < nb; n++ ) {

		CJsonData *pSlot = pHard->GetChild(CPrintf("modules.mlist.%4.4X", n));

		if( pSlot ) {

			UINT uType = UINT(tatoi(pSlot->GetValue("type", "0")));

			if( uType >= 200 ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Implementation

void CLinuxDaxConfigApplicator::ScanForSerial(CStringArray &List, CJsonData *pHard, UINT &uFrom)
{
	UINT ns = m_pModel->GetObjCount('x');

	for( UINT s = 0; s < ns; s++ ) {

		CJsonData *pSlot = pHard->GetChild(CPrintf("sleds.slist.%4.4X", s));

		if( pSlot ) {

			UINT uType = tatoi(pSlot->GetValue("type", "0"));

			if( uType == 103 || uType == 104 || uType == 105 ) {

				if( TRUE ) {

					char cType = (uType == 103) ? '2' : '4';
					
					CString Info;

					Info += CPrintf("Serial Port %u,", 1+uFrom);

					Info += CPrintf("%u,", uFrom);

					Info += CPrintf("%u,", 0);

					Info += CPrintf("S%u,", uFrom);

					Info += CPrintf("S%c,", cType);

					Info += CPrintf("Sled %u Port %u", 1+s, 1);

					List.Append(Info);

					uFrom++;
				}

				if( TRUE ) {

					char cType = (uType == 104) ? '4' : '2';

					CString Info;

					Info += CPrintf("Serial Port %u,", 1+uFrom);

					Info += CPrintf("%u,", uFrom);

					Info += CPrintf("%u,", 0);

					Info += CPrintf("S%u,", uFrom);

					Info += CPrintf("S%c,", cType);

					Info += CPrintf("Sled %u Port %u", 1+s, 2);

					List.Append(Info);

					uFrom++;
				}
			}
		}
	}
}

// End of File
