
#include "Intern.hpp"

#include "RlosCrxConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RLOS CR Series Configuration Applicator
//

// Instantiator

DLLAPI IConfigApplicator * Create_RlosCrxConfigApplicator(CString Model, IPxeModel *pModel)
{
	return new CRlosCrxConfigApplicator(Model, pModel);
}

// Constructor

CRlosCrxConfigApplicator::CRlosCrxConfigApplicator(CString Model, IPxeModel *pModel) : CRlosBaseConfigApplicator(Model, pModel)
{
}

// IConfigApplicator

CString CRlosCrxConfigApplicator::GetDisplayName(void)
{
	CString Base = m_Model.Left(4);

	return MakeFull(L"CR", Base);
}

CString CRlosCrxConfigApplicator::GetProcessor(void)
{
	return "AM437";
}

CString CRlosCrxConfigApplicator::GetModelList(void)
{
	CString Base = m_Model.Left(4);

	return MakeFull(L"cr", Base);
}

CString CRlosCrxConfigApplicator::GetModelInfo(CString Model)
{
	CString Base = m_Model.Left(4);

	CString Full = MakeFull(L"cr", Base);

	if( Model == Full ) {

		CString List;

		List += Base + L',';

		List += L"prom" + Base + L',';

		List += L"load" + Base + L',';

		List += L"primcxx";

		return List;
	}

	return L"";
}

CString CRlosCrxConfigApplicator::GetEmulatorModel(CSize DispSize)
{
	CString Base = m_Model.Left(4);

	if( Base == L"ca07" && DispSize.cx == 320 ) {

		Base += L"eq";
	}

	if( Base == L"ca10" && DispSize.cx == 640 ) {

		Base += L"ev";
	}

	if( Base == L"co07" && DispSize.cx == 320 ) {

		Base += L"eq";
	}

	return Base;
}

BOOL CRlosCrxConfigApplicator::GetPorts(CStringArray &List, CJsonData *pHard)
{
	if( m_Model.StartsWith(L"co") ) {

		switch( m_pModel->GetObjCount('p') ) {

			case 3:
			{
				List.Append(IDS("Programming Port,0,0,S0,SP,Base Unit"));

				List.Append(IDS("RS-232 Comms Port,1,0,S1,S2,Base Unit"));

				List.Append(IDS("RS-485 Comms Port,1,1,S2,S4,Base Unit"));
			}
			break;
		}
	}

	if( m_Model.StartsWith(L"ca") ) {

		switch( m_pModel->GetObjCount('p') ) {

			case 3:
			{
				List.Append(IDS("Programming Port,0,0,S0,SP,Base Unit"));

				List.Append(IDS("RS-232 Comms Port,1,0,S1,S2,Base Unit"));

				List.Append(IDS("RS-485 Comms Port,2,0,S2,S4,Base Unit"));
			}
			break;

			case 4:
			{
				List.Append(IDS("Programming Port,0,S0,SP,Base Unit"));

				List.Append(IDS("RS-232 Comms Port,1,0,S1,S2,Base Unit"));

				List.Append(IDS("RS-485 Comms Port A,2,0,S2,S4,Base Unit"));

				List.Append(IDS("RS-485 Comms Port B,3,0,S3,S4,Base Unit"));
			}
			break;
		}
	}

	return CRlosBaseConfigApplicator::GetPorts(List, pHard);
}

// Implementation

CString CRlosCrxConfigApplicator::MakeFull(CString const &Prefix, CString const &Base)
{
	if( Base.Left(2) == L"co" ) {

		return Prefix + L"1000-" + Base.Mid(2) + "000";
	}

	if( Base.Left(2) == L"ca" ) {

		return Prefix + L"3000-" + Base.Mid(2) + "000";
	}

	AfxAssert(FALSE);

	return L"";
}

// End of File
