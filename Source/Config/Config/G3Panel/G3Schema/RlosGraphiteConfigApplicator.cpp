
#include "Intern.hpp"

#include "RlosGraphiteConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RLOS Graphite Configuration Applicator
//

// Instantiator

DLLAPI IConfigApplicator * Create_RlosGraphiteConfigApplicator(CString Model, IPxeModel *pModel)
{
	return new CRlosGraphiteConfigApplicator(Model, pModel);
}

// Constructor

CRlosGraphiteConfigApplicator::CRlosGraphiteConfigApplicator(CString Model, IPxeModel *pModel) : CRlosBaseConfigApplicator(Model, pModel)
{
}

// IConfigApplicator

CString CRlosGraphiteConfigApplicator::GetDisplayName(void)
{
	CString Base = GetBase(m_Model);

	if( Base == L"gc" ) {

		return IDS("Edge Controller");
	}

	if( Base == L"gsr" ) {

		return IDS("Core Controller");
	}

	return IDS("Graphite ") + Base.ToUpper();
}

CString CRlosGraphiteConfigApplicator::GetProcessor(void)
{
	return "MX51";
}

CString CRlosGraphiteConfigApplicator::GetModelList(void)
{
	CString Base = GetBase(m_Model);

	return Base;
}

CString CRlosGraphiteConfigApplicator::GetModelInfo(CString Model)
{
	CString Base = GetBase(m_Model);

	if( Model == Base ) {

		CString List;

		List += Base + L',';

		List += L"prom" + Base.Mid(1) + L',';

		List += L"load" + Base.Mid(1);

		return List;
	}

	return L"";
}

CString CRlosGraphiteConfigApplicator::GetEmulatorModel(CSize DispSize)
{
	CString Base = GetBase(m_Model);

	if( Base == L"gc" || Base == L"gsr" ) {

		return CPrintf("gc-%4.4u-%3.3u", DispSize.cx, DispSize.cy);
	}

	return Base;
}

BOOL CRlosGraphiteConfigApplicator::GetPorts(CStringArray &List, CJsonData *pHard)
{
	switch( m_pModel->GetObjCount('p') ) {

		case 3:
		{
			List.Append(IDS("Programming Port,0,0,S0,SP,Base Unit"));

			List.Append(IDS("RS-485 Comms Port,1,0,S1,S4,Base Unit"));

			List.Append(IDS("RS-232 Comms Port,2,0,S2,S2,Base Unit"));
		}
		break;

		case 4:
		{
			List.Append(IDS("Programming Port,0,0,S0,SP,Base Unit"));

			List.Append(IDS("RS-485 Comms Port A,1,0,S1,S4,Base Unit"));

			List.Append(IDS("RS-232 Comms Port,2,0,S2,S2,Base Unit"));

			List.Append(IDS("RS-485 Comms Port B,3,0,S3,S4,Base Unit"));
		}
		break;
	}

	return CRlosBaseConfigApplicator::GetPorts(List, pHard);
}

// Implementation

CString CRlosGraphiteConfigApplicator::GetBase(CString const &Model)
{
	CString Base = Model.Left(3);

	if( Base == L"gce" ) {

		return L"gc";
	}

	if( Base == L"gsr" ) {

		return L"gsr";
	}

	return Model.ToLower();
}

// End of File
