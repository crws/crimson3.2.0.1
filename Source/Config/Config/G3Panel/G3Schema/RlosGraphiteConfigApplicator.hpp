
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosGraphiteConfigApplicator_HPP

#define INCLUDE_RlosGraphiteConfigApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RlosBaseConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS Graphite Configuration Applicator
//

class CRlosGraphiteConfigApplicator : public CRlosBaseConfigApplicator
{
public:
	// Constructors
	CRlosGraphiteConfigApplicator(CString Model, IPxeModel *pModel);

	// IConfigApplicator
	CString METHOD GetDisplayName(void);
	CString METHOD GetProcessor(void);
	CString METHOD GetModelList(void);
	CString METHOD GetModelInfo(CString Model);
	CString METHOD GetEmulatorModel(CSize DispSize);
	BOOL    METHOD GetPorts(CStringArray &List, CJsonData *pHard);

protected:
	// Implementation
	CString GetBase(CString const &Model);
};

// End of File

#endif
