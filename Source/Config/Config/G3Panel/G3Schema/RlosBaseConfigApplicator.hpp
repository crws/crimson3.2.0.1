
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosBaseConfigApplicator_HPP

#define INCLUDE_RlosBaseConfigApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// RLOS Configuration Applicator
//

class CRlosBaseConfigApplicator : public IConfigApplicator
{
public:
	// Constructors
	CRlosBaseConfigApplicator(CString Model, IPxeModel *pModel);

	// Destructor
	virtual ~CRlosBaseConfigApplicator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IConfigApplicator
	BOOL METHOD GetPorts(CStringArray &List, CJsonData *pHard);
	BOOL METHOD GetModules(CStringArray &List, CJsonData *pHard);
	BOOL METHOD HasModules(CJsonData *pHard);

protected:
	// Data Members
	ULONG	    m_uRefs;
	CString	    m_Model;
	IPxeModel * m_pModel;

	// Implementation
	void ScanForModules(CStringArray &List, CJsonData *pHard, UINT uType, UINT &uFrom, PCTXT pType, PCTXT pName);
};

// End of File

#endif
