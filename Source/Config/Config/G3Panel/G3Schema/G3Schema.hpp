
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3Schema_HPP

#define	INCLUDE_G3Schema_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3comms.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "G3Schema.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3SCHEMA

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "G3Schema.lib")

#endif

#define DLLNOT 

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define STRONG_INLINE inline

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPortList;

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "AutoPointer.hpp"

#include "JsonData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon COM Definitions
//

#define	ULM		ULONG METHOD

typedef LONG volatile * PVLONG;

//////////////////////////////////////////////////////////////////////////
//
// COM Interface Support
//

#define AfxDeclareIID(t, n)	static WORD const m_iit = t; \
				static WORD const m_iin = n  \

#define	AfxAeonIID(i)		CGuid(i::m_iit, i::m_iin)

#define	AfxIsInterface(r, i)	((CGuid const &) r).IsAeon(i::m_iit, i::m_iin)

#define	AfxIsUnknown(r)		((CGuid const &) r).IsUnknown()

//////////////////////////////////////////////////////////////////////////
//
// COM Helper Macros
//

#define AtomicIncrement(pv)	(InterlockedIncrement(PVLONG(pv)))

#define AtomicDecrement(pv)	(InterlockedDecrement(PVLONG(pv)))

#define StdQueryUnknown(i)	*ppObject = NULL;			\
				if( AfxIsUnknown(riid) ) {		\
				AtomicIncrement(&m_uRefs);		\
				*ppObject = (i *) this;			\
				return S_OK;				\
				}					\

#define StdQueryInterface(i)	if( AfxIsInterface(riid, i) ) {		\
				AtomicIncrement(&m_uRefs);		\
				*ppObject = (i *) this;			\
				return S_OK;				\
				}					\

#define	StdSetRef()		m_uRefs = 1;				\

#define	StdAddRef()		return AtomicIncrement(&m_uRefs);	\

#define	StdRelease()		if( !AtomicDecrement(&m_uRefs) ) {	\
				delete this;				\
				return 0;				\
				}					\
				return m_uRefs;				\

//////////////////////////////////////////////////////////////////////////
//
// Config Update Interface
//

interface IConfigUpdate : public IUnknown
{
	AfxDeclareIID(200, 11);

	virtual void METHOD OnConfigUpdate(char cTag) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Config Storage Interface
//

interface IConfigStorage : public IUnknown
{
	AfxDeclareIID(200, 12);

	virtual bool METHOD AddUpdateSink(IConfigUpdate *pUpdate)                 = 0;
	virtual bool METHOD SetConfig(char cTag, CString const &Text, bool fEdit) = 0;
	virtual bool METHOD GetConfig(char cTag, CString &Text)                   = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Schema Generator Interface
//

interface ISchemaGenerator : public IUnknown
{
	AfxDeclareIID(200, 13);

	virtual bool METHOD Open(void)                           = 0;
	virtual bool METHOD GetDefault(char cTag, CString &Text) = 0;
	virtual bool METHOD GetSchema(char cTag, CString &Text)  = 0;
	virtual bool METHOD GetNaming(char cTag, CString &Text)  = 0;
};

//////////////////////////////////////////////////////////////////////////
//								
// Model Information Interface
//

interface IPxeModel : public IUnknown
{
	AfxDeclareIID(200, 18);

	virtual void    METHOD MakeAppObjects(void)                              = 0;
	virtual void    METHOD MakePxeObjects(void)                              = 0;
	virtual BOOL    METHOD AdjustHardware(CJsonData *pData)                  = 0;
	virtual BOOL    METHOD ApplyModelSpec(CString Model)                     = 0;
	virtual BOOL	METHOD AppendModelSpec(CString &Model, CJsonData *pData) = 0;
	virtual BOOL    METHOD GetDispList(CArray<DWORD> &List)                  = 0;
	virtual UINT    METHOD GetObjCount(char cTag)                            = 0;
	virtual PCDWORD METHOD GetUsbPaths(char cTag)                            = 0;
	virtual UINT    METHOD GetPortType(UINT uPort)                           = 0;
};

//////////////////////////////////////////////////////////////////////////
//								
// Configuration Applicator Interface
//

interface IConfigApplicator : public IUnknown
{
	AfxDeclareIID(200, 19);

	virtual CString METHOD GetDisplayName(void)				= 0;
	virtual CString METHOD GetProcessor(void)                               = 0;
	virtual CString METHOD GetModelList(void)                               = 0;
	virtual CString METHOD GetModelInfo(CString Model)                      = 0;
	virtual CString METHOD GetEmulatorModel(CSize DispSize)			= 0;
	virtual BOOL    METHOD GetPorts(CStringArray &List, CJsonData *pHard)   = 0;
	virtual BOOL    METHOD GetModules(CStringArray &List, CJsonData *pHard) = 0;
	virtual BOOL    METHOD HasModules(CJsonData *pHard)                     = 0;
};

// End of File

#endif
