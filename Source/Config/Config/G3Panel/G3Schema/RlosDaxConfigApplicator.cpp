
#include "Intern.hpp"

#include "RlosDaxConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RLOS DAx Configuration Applicator
//

// Instantiator

DLLAPI IConfigApplicator * Create_RlosDaxConfigApplicator(CString Model, IPxeModel *pModel)
{
	return new CRlosDaxConfigApplicator(Model, pModel);
}

// Constructor

CRlosDaxConfigApplicator::CRlosDaxConfigApplicator(CString Model, IPxeModel *pModel) : CRlosBaseConfigApplicator(Model, pModel)
{
}

// IConfigApplicator

CString CRlosDaxConfigApplicator::GetDisplayName(void)
{
	CString Base = m_Model.Left(4);

	return Base.ToUpper();
}

CString CRlosDaxConfigApplicator::GetProcessor(void)
{
	return "AM437";
}

CString CRlosDaxConfigApplicator::GetModelList(void)
{
	CString Base = m_Model.Left(4);

	return Base;
}

CString CRlosDaxConfigApplicator::GetModelInfo(CString Model)
{
	CString Base = m_Model.Left(4);

	if( Model == Base ) {

		CString List;

		List += Base + L',';

		List += L"prom" + Base + L',';

		List += L"load" + Base + L',';

		List += L"primcxx";

		return List;
	}

	return L"";
}

CString CRlosDaxConfigApplicator::GetEmulatorModel(CSize DispSize)
{
	CString Base = m_Model.Left(4);

	if( Base == L"da10" ) {

		return L"";
	}

	if( Base == L"da30" ) {

		return Base + CPrintf("-%4.4u-%3.3u", DispSize.cx, DispSize.cy);
	}

	AfxAssert(FALSE);

	return L"";
}

BOOL CRlosDaxConfigApplicator::GetPorts(CStringArray &List, CJsonData *pHard)
{
	switch( m_pModel->GetObjCount('p') ) {

		case 3:
		{
			List.Append(IDS("Programming Port,0,0,S0,SP,Base Unit"));

			List.Append(IDS("RS-232 Comms Port,1,0,S1,S2,Base Unit"));

			List.Append(IDS("RS-485 Comms Port,2,0,S2,S4,Base Unit"));
		}
		break;
	}

	return CRlosBaseConfigApplicator::GetPorts(List, pHard);
}

// End of File
