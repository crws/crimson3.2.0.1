
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosDaxConfigApplicator_HPP

#define INCLUDE_RlosDaxConfigApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RlosBaseConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS DAx Configuration Applicator
//

class CRlosDaxConfigApplicator : public CRlosBaseConfigApplicator
{
public:
	// Constructors
	CRlosDaxConfigApplicator(CString Model, IPxeModel *pModel);

	// IConfigApplicator
	CString METHOD GetDisplayName(void);
	CString METHOD GetProcessor(void);
	CString METHOD GetModelList(void);
	CString METHOD GetModelInfo(CString Model);
	CString METHOD GetEmulatorModel(CSize DispSize);
	BOOL    METHOD GetPorts(CStringArray &List, CJsonData *pHard);
};

// End of File

#endif
