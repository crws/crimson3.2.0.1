
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_JsonData_HPP

#define	INCLUDE_JsonData_HPP

//////////////////////////////////////////////////////////////////////////
//
// JSON Types
//

enum JsonType
{
	jsonNull,
	jsonAuto,
	jsonString,
	jsonNumber,
	jsonBool,
	jsonObject
};

//////////////////////////////////////////////////////////////////////////
//
// JSON Data
//

class DLLAPI CJsonData
{
public:
	// Constructor
	CJsonData(void);
	CJsonData(BOOL fList);

	// Destructor
	~CJsonData(void);

	// Operations
	void Empty(void);
	BOOL Parse(PCTXT pText);
	void AddObject(CString Name);
	void AddChild(CString Name, BOOL fList, CJsonData * &pSub);
	void AddObject(CString Name, CJsonData * &pSub);
	void AddList(CString Name, CJsonData * &pSub);
	void AddValue(CString Name, CString Value, UINT Type);
	void AddValue(CString Name, CString Value);
	void AddNull(CString Name);
	void AddChild(BOOL fList, CJsonData * &pSub);
	void AddObject(CJsonData * &pSub);
	void AddList(CJsonData * &pSub);
	void AddMember(CString Value);
	BOOL Delete(CString Name);
	BOOL Delete(UINT uIndex);
	void SetValue(INDEX Index, CString Value);

	// Attributes
	inline BOOL IsEmpty(void) const;
	inline BOOL IsList(void) const;
	inline UINT GetCount(void) const;

	// Attributes
	CString     GetAsText(BOOL fPretty) const;
	BOOL	    GetNames(CStringArray &Names) const;
	BOOL        HasName(CString Name) const;
	BOOL        HasName(UINT uIndex) const;
	UINT        GetType(CString Name) const;
	UINT        GetType(UINT uIndex) const;
	CString     GetValue(CString Name) const;
	CString     GetValue(UINT uIndex) const;
	CString     GetValue(CString Name, CString Default) const;
	CString     GetValue(UINT uIndex, CString Default) const;
	CJsonData * GetChild(CString Name) const;
	CJsonData * GetChild(UINT uIndex) const;
	CString     GetPathValue(PCTXT pPath) const;
	CJsonData * GetPathChild(PCTXT pPath) const;

	// Enumeration
	inline INDEX	   GetHead(void) const;
	inline INDEX	   GetTail(void) const;
	inline BOOL	   GetNext(INDEX &Index) const;
	inline BOOL	   GetPrev(INDEX &Index) const;
	inline UINT        GetType(INDEX Index) const;
	inline CString     GetName(INDEX Index) const;
	inline CString     GetValue(INDEX Index) const;
	inline CJsonData * GetChild(INDEX Index) const;
	inline BOOL	   Failed(INDEX Index) const;

protected:
	// Static Data
	static WCHAR const m_cEsc[];
	static WCHAR const m_cRep[];

	// Pair Data
	struct CJsonPair
	{
		// Data Members
		CString     m_Name;
		CString     m_Value;
		UINT	    m_Type;
		CJsonData * m_pSub;

		// Constructors
		inline CJsonPair(void);
		inline CJsonPair(CJsonPair const &That);
		inline CJsonPair(CString const &Name);
		inline CJsonPair(CString const &Name, CString const &Value, UINT Type);
		inline CJsonPair(CString const &Name, CString const &Value);
		inline CJsonPair(CString const &Name, CJsonData *pSub);

		// Comparison Function
		friend inline int AfxCompare(CJsonPair const &a, CJsonPair const &b)
		{
			return wcsicmp(a.m_Name, b.m_Name);
		}

	// Comparison Operators
		inline int operator == (CJsonPair const &That) const;
		inline int operator >  (CJsonPair const &That) const;
	};

// Data Members
	CTree <CJsonPair> m_Tree;
	BOOL		  m_fList;
	UINT		  m_uIndex;

	// Implementation
	BOOL    ParseBlock(PCTXT &pText);
	CString GetAsText(BOOL fPretty, UINT uLevel) const;
	CString UniEncode(CString Text) const;
	CString CreateName(UINT uIndex) const;
	UINT    FindAutoType(CString const &Data) const;
	BOOL    IsNumber(PCTXT pText) const;
};

//////////////////////////////////////////////////////////////////////////
//
// JSON Data
//

// Attributes

STRONG_INLINE BOOL CJsonData::IsEmpty(void) const
{
	return m_Tree.IsEmpty();
}

STRONG_INLINE BOOL CJsonData::IsList(void) const
{
	return m_fList;
}

STRONG_INLINE UINT CJsonData::GetCount(void) const
{
	return m_Tree.GetCount();
}

// Enumeration

STRONG_INLINE INDEX CJsonData::GetHead(void) const
{
	return m_Tree.GetHead();
}

STRONG_INLINE INDEX CJsonData::GetTail(void) const
{
	return m_Tree.GetTail();
}

STRONG_INLINE BOOL CJsonData::GetNext(INDEX &Index) const
{
	return m_Tree.GetNext(Index);
}

STRONG_INLINE BOOL CJsonData::GetPrev(INDEX &Index) const
{
	return m_Tree.GetPrev(Index);
}

STRONG_INLINE CString CJsonData::GetName(INDEX Index) const
{
	return m_Tree[Index].m_Name;
}

STRONG_INLINE UINT CJsonData::GetType(INDEX Index) const
{
	return m_Tree[Index].m_Type;
}

STRONG_INLINE CString CJsonData::GetValue(INDEX Index) const
{
	return m_Tree[Index].m_Value;
}

STRONG_INLINE CJsonData * CJsonData::GetChild(INDEX Index) const
{
	return m_Tree[Index].m_pSub;
}

STRONG_INLINE BOOL CJsonData::Failed(INDEX Index) const
{
	return m_Tree.Failed(Index);
}

// CJSonPair Constructors

STRONG_INLINE CJsonData::CJsonPair::CJsonPair(void)
{
	m_Type = jsonNull;

	m_pSub = NULL;
}

STRONG_INLINE CJsonData::CJsonPair::CJsonPair(CJsonPair const &That)
{
	m_Name  = That.m_Name;

	m_Value = That.m_Value;

	m_Type  = That.m_Type;

	m_pSub  = That.m_pSub;
}

STRONG_INLINE CJsonData::CJsonPair::CJsonPair(CString const &Name)
{
	m_Name  = Name;

	m_Type  = jsonNull;

	m_pSub  = NULL;
}

STRONG_INLINE CJsonData::CJsonPair::CJsonPair(CString const &Name, CString const &Value, UINT Type)
{
	m_Name  = Name;

	m_Value = Value;

	m_Type  = Type;

	m_pSub  = NULL;
}

STRONG_INLINE CJsonData::CJsonPair::CJsonPair(CString const &Name, CString const &Value)
{
	m_Name  = Name;

	m_Value = Value;

	m_Type  = jsonAuto;

	m_pSub  = NULL;
}

STRONG_INLINE CJsonData::CJsonPair::CJsonPair(CString const &Name, CJsonData *pSub)
{
	m_Name = Name;

	m_Type = jsonObject;

	m_pSub = pSub;
}

// CJSonPair Comparison Operators

STRONG_INLINE int CJsonData::CJsonPair::operator == (CJsonPair const &That) const
{
	return m_Name == That.m_Name;
}

STRONG_INLINE int CJsonData::CJsonPair::operator >  (CJsonPair const &That) const
{
	return m_Name > That.m_Name;
}

// End of File

#endif
