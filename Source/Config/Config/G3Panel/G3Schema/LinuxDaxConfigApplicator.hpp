
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxDaxConfigApplicator_HPP

#define INCLUDE_LinuxDaxConfigApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// DAx Configuration Applicator
//

class CLinuxDaxConfigApplicator : public IConfigApplicator
{
public:
	// Constructors
	CLinuxDaxConfigApplicator(CString Model, IPxeModel *pModel);

	// Destructor
	~CLinuxDaxConfigApplicator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IConfigApplicator
	CString METHOD GetDisplayName(void);
	CString METHOD GetProcessor(void);
	CString METHOD GetModelList(void);
	CString METHOD GetModelInfo(CString Model);
	CString METHOD GetEmulatorModel(CSize DispSize);
	BOOL    METHOD GetPorts(CStringArray &List, CJsonData *pConfig);
	BOOL    METHOD GetModules(CStringArray &List, CJsonData *pConfig);
	BOOL    METHOD HasModules(CJsonData *pConfig);

protected:
	// Data Members
	ULONG	    m_uRefs;
	CString	    m_Model;
	IPxeModel * m_pModel;

	// Implementation
	void ScanForSerial(CStringArray &List, CJsonData *pHard, UINT &uFrom);
};

// End of File

#endif
