
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Database Object
//

// Static Data

CStratonDatabase * CStratonDatabase::m_pThis	= NULL;

DWORD		   CStratonDatabase::m_dwClient	= 0;

// Object Location

CStratonDatabase * CStratonDatabase::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CStratonDatabase::CStratonDatabase(void)
{
	AfxAssert(m_pThis == NULL);

	m_pThis		= this;

	m_dwClient	= 0;
	
	Connect();
	}

// Destructor

CStratonDatabase::~CStratonDatabase(void)
{
	Disconnect();
	}

// Connection

BOOL CStratonDatabase::Connect(void)
{
	m_dwClient = K5DB_Connect( NULL, 
				   WM_DATABASE, 
				   K5DBCNX_SELFNOTIF, 
				   0, 
				   "Client"
				   );

	return m_dwClient != 0;
	}

BOOL CStratonDatabase::SetCallback(HWND hWnd)
{
	return SetError(K5DB_SetCallback( m_dwClient, 
					  hWnd, 
					  WM_DATABASE
					  ));
	}

void CStratonDatabase::Disconnect(void)
{
	K5DB_Disconnect(m_dwClient);	
	}

// Methods

DWORD CStratonDatabase::GetKindOfObject(DWORD dwHandle)
{
	return K5DB_GetKindOfObject(dwHandle);
	}

CString CStratonDatabase::GetGhostName(DWORD dwHandle)
{
	LPCSTR pName = K5DB_GetGhostName(dwHandle);

	return String(pName);
	}

// Project

DWORD CStratonDatabase::CreateProject(PCTXT pPath)
{
	CError Error(FALSE);

	return SetError(Error, K5DB_CreateProject( m_dwClient, 
						   LPCSTR(CAnsiString(pPath))
						   ));
	}

BOOL CStratonDatabase::DeleteProject(DWORD dwHandle)
{
	return SetError(K5DB_DeleteProject( m_dwClient, 
					    dwHandle
					    ));
	}

DWORD CStratonDatabase::OpenProject(PCTXT pPath)
{
	return K5DB_OpenProject( m_dwClient, 
				 LPCSTR(CAnsiString(pPath))
				 );
	}

void CStratonDatabase::CloseProject(DWORD dwHandle)
{
	K5DB_CloseProject( m_dwClient, 
			   dwHandle
			   );
	}

BOOL CStratonDatabase::SaveProject(DWORD dwHandle)
{
	return SetError(K5DB_SaveProject( m_dwClient, 
					  dwHandle
					  ));
	}

CString CStratonDatabase::GetProjectDateStamp(DWORD dwHandle)
{
	DWORD dwTime = K5DB_GetProjectDateStamp(m_dwClient, dwHandle);

	time_t dt    = dwTime;

	LPSTR pTime  = ctime(&dt);

	CString Time = String(pTime);

	Time.Remove(L'\r');

	Time.Remove(L'\n');

	return Time;
	}

// Group

UINT CStratonDatabase::GetGroups(DWORD dwProject, CLongArray &List)
{
	List.Empty();

	UINT uTypes = GetNbGroup(dwProject);

	if( uTypes ) {

		PDWORD pTypes = New DWORD [ uTypes ];	
		
		GetGroups(dwProject, pTypes);

		List.Append(pTypes, uTypes);

		delete[] pTypes;
		}

	return uTypes;
	}

DWORD CStratonDatabase::FindGroup(DWORD dwProject, PCTXT pName)
{
	return K5DB_FindGroup( m_dwClient,
			       dwProject,
			       LPCSTR(CAnsiString(pName))
			       );
	}

CString CStratonDatabase::GetGroupDesc(DWORD dwProject, DWORD dwGroup, DWORD &dwStyle)
{
	DWORD dwIOType;
	DWORD dwSlot;
	DWORD dwSubSlot;
	DWORD dwVarCount;

	LPCSTR pName =  K5DB_GetGroupDesc( m_dwClient, 
					   dwProject,
					   dwGroup,
					   &dwStyle,
					   &dwIOType,
					   &dwSlot,
					   &dwSubSlot,
					   &dwVarCount
					   );

	return String(pName);
	}

// Program

UINT CStratonDatabase::GetPrograms(DWORD dwProject, DWORD dwSection, CLongArray &List)
{
	List.Empty();

	UINT uTypes = GetNbProgram(dwProject, dwSection);

	if( uTypes ) {

		PDWORD pTypes = New DWORD [ uTypes ];	
		
		GetPrograms(dwProject, dwSection, pTypes);

		List.Append(pTypes, uTypes);

		delete[] pTypes;
		}

	return uTypes;
	}

DWORD CStratonDatabase::FindProgram(DWORD dwProject, PCTXT pName)
{
	return K5DB_FindProgram(m_dwClient, dwProject, CAnsiString(pName));
	}

BOOL CStratonDatabase::CanCreateProgram(DWORD dwProject, DWORD dwLanguage, DWORD dwSection, DWORD dwParent, PCTXT pName)
{
	return SetError(CError(FALSE), K5DB_CanCreateProgram( m_dwClient, 
							      dwProject, 
							      dwLanguage, 
							      dwSection, 
							      dwParent, 
							      CAnsiString(pName))
							      );
	}

DWORD CStratonDatabase::CreateProgram(DWORD dwProject, DWORD dwLanguage, DWORD dwSection, DWORD dwParent, PCTXT pName)
{
	return K5DB_CreateProgram( m_dwClient, 
				   dwProject, 
				   dwLanguage, 
				   dwSection, 
				   dwParent, 
				   CAnsiString(pName));
	}

BOOL CStratonDatabase::DeleteProgram(DWORD dwProject, DWORD dwProgram)
{
	return SetError(K5DB_DeleteProgram( m_dwClient, 
					    dwProject, 
					    dwProgram
					    ));
	}

BOOL CStratonDatabase::RenameProgram(CError &Error, DWORD dwProject, DWORD dwProgram, PCTXT pName)
{
	DWORD dwCode = K5DB_RenameProgram( m_dwClient, dwProject, dwProgram, CAnsiString(pName));

	if( dwCode != K5DB_OK ) {

		if( dwCode == K5DBERR_DUPPROGNAME ) {

			Error.SetError(CString(IDS_SPECIFIED_PROGRAM));
			}
		else {
			CString Text = CString(K5DB_GetMessage(dwCode));

			Error.SetError(CPrintf(L"Error %d : %s", dwCode, Text));
			}

		return FALSE;
		}

	return TRUE;
	}

CString CStratonDatabase::GetProgramDesc(DWORD dwProject, DWORD dwProgram, DWORD &dwLanguage, DWORD &dwSection, DWORD &dwParent)
{
	LPCSTR pName =  K5DB_GetProgramDesc( m_dwClient, 
					     dwProject,
					     dwProgram,
					    &dwLanguage, 
					    &dwSection, 
					    &dwParent 
					    );

	return String(pName);
	}

BOOL CStratonDatabase::SaveProgramChanges(DWORD dwProject, DWORD dwProgram)
{
	return SetError(K5DB_SaveProgramChanges( m_dwClient, 
						 dwProject, 
						 dwProgram
						 ));
	}

BOOL CStratonDatabase::GetProgramSchedule(DWORD dwProject, DWORD dwProgram, DWORD &dwPeriod, DWORD &dwOffset)
{
	return SetError(K5DB_GetProgramSchedule( m_dwClient, 
						 dwProject, 
						 dwProgram, 
						&dwPeriod, 
						&dwOffset));
	}

BOOL CStratonDatabase::SetProgramSchedule(DWORD dwProject, DWORD dwProgram, DWORD dwPeriod, DWORD dwOffset)
{
	return SetError(K5DB_SetProgramSchedule( m_dwClient, 
						 dwProject, 
						 dwProgram, 
						 dwPeriod, 
						 dwOffset));
	}

BOOL CStratonDatabase::GetProgramOnCallFlag(DWORD dwProject, DWORD dwProgram)
{
	return K5DB_GetProgramOnCallFlag( m_dwClient, 
					  dwProject, 
					  dwProgram
					  );
	}

BOOL CStratonDatabase::SetProgramOnCallFlag(DWORD dwProject, DWORD dwProgram, DWORD dwOnCall)
{
	return SetError(K5DB_SetProgramOnCallFlag( m_dwClient, 
						   dwProject, 
						   dwProgram, 
						   dwOnCall
						   ));
	}

BOOL CStratonDatabase::MoveProgram(DWORD dwProject, DWORD dwProgram, DWORD dwMove, DWORD dwParent)
{
	CError Error(FALSE);

	return SetError(Error, K5DB_MoveProgram( m_dwClient, 
						 dwProject, 
						 dwProgram, 
						 dwMove, 
						 dwParent
						 ));
	}

// Variable

CString CStratonDatabase::GetVariableDesc(DWORD dwProject, DWORD dwVariable, DWORD &dwType, DWORD &dwDim, DWORD &dwLen, DWORD &dwFlags)
{
	DWORD dwGroup = GetVarGroup( dwProject, dwVariable);

	LPCSTR pName =  K5DB_GetVarDesc( m_dwClient, 
					 dwProject,
					 dwGroup,
					 dwVariable,
					 &dwType, 
					 &dwDim, 
					 &dwLen, 
					 &dwFlags
					 );

	return String(pName);
	}

BOOL CStratonDatabase::SetVariableDesc(DWORD dwProject, DWORD dwVariable, DWORD dwType, DWORD dwDim, DWORD dwLen, DWORD dwFlags)
{
	DWORD dwGroup = GetVarGroup( dwProject, dwVariable);

	return SetError(K5DB_SetVarDesc( m_dwClient, 
					 dwProject, 
					 dwGroup, 
					 dwVariable, 
					 dwType, 
					 dwDim, 
					 dwLen, 
					 dwFlags));
	}

BOOL CStratonDatabase::CanSetVariableDesc(DWORD dwProject, DWORD dwVariable, DWORD dwType, DWORD dwDim, DWORD dwLen, DWORD dwFlags)
{
	DWORD dwGroup = GetVarGroup( dwProject, dwVariable);

	return SetError(CError(FALSE), K5DB_CanSetVarDesc( m_dwClient, 
							   dwProject, 
							   dwGroup, 
							   dwVariable, 
							   dwType, 
							   dwDim, 
							   dwLen, 
							   dwFlags)
							    );
	}

DWORD CStratonDatabase::GetVarGroup(DWORD dwProject, DWORD dwVariable)
{
	return K5DB_GetVarGroup( m_dwClient, dwProject, dwVariable);
	}

DWORD CStratonDatabase::FindVarInGroup(DWORD dwProject, DWORD dwGroup, PCTXT pSymbol)
{
	return K5DB_FindVarInGroup( m_dwClient, 
				    dwProject, 
				    dwGroup,
				    LPCSTR(CAnsiString(pSymbol)) 
				    );
	}

DWORD CStratonDatabase::FindVar(DWORD dwProject, PCTXT pSymbol, DWORD dwFrom, DWORD dwGroup, DWORD dwFlags)
{
	return K5DB_FindVar( m_dwClient, 
			     dwProject, 
			     LPCSTR(CAnsiString(pSymbol)), 
			     dwFrom, 
			     dwGroup,
			     dwFlags
			     );
	}

DWORD CStratonDatabase::FindVarExact(DWORD dwProject, PCTXT pSymbol, DWORD dwFrom, DWORD dwGroup)
{
	return FindVar( dwProject, 
			pSymbol, 
			dwFrom, 
			dwGroup,
			K5DBVAR_FINDEXACT
			);
	}

DWORD CStratonDatabase::CreateVar(DWORD dwProject, DWORD dwGroup, DWORD dwType, UINT uDim, UINT uLen, DWORD dwFlags, PCTXT pName) 
{
	DWORD dwPosID = DWORD(-1);

	return K5DB_CreateVar( m_dwClient,
			       dwProject, 
			       dwGroup,
			       dwPosID,
			       dwType, 
			       uDim, 
			       uLen,
			       dwFlags,
			       LPCSTR(CAnsiString(pName))
			       );
	}

BOOL CStratonDatabase::DeleteVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar) 
{
	return SetError(K5DB_DeleteVar(m_dwClient, dwProject, dwGroup, dwVar));
	}

BOOL CStratonDatabase::CanRenameVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pName)
{
	return SetError(CError(FALSE), K5DB_CanRenameVar( m_dwClient, 
						         dwProject, 
							 dwGroup, 
							 dwVar, 
							 LPCSTR(CAnsiString(pName))
							));
	}

BOOL CStratonDatabase::RenameVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pName) 
{
	return SetError(K5DB_RenameVar( m_dwClient, 
					dwProject, 
					dwGroup, 
					dwVar, 
					LPCSTR(CAnsiString(pName))
					));
	}

CString CStratonDatabase::GetVarPrevName(DWORD dwProject, DWORD dwVar)
{
	LPCSTR pName =  K5DB_GetVarPrevName( m_dwClient, 
					     dwProject, 
					     dwVar
					     );

	return String(pName);
	}

BOOL CStratonDatabase::GetVarInitValue(DWORD dwProject, DWORD dwGroup, DWORD dwVar, CString &Init)
{
	DWORD dwValid;

	LPCSTR pValue = K5DB_GetVarInitValue( m_dwClient, 
					      dwProject, 
					      dwGroup, 
					      dwVar, 
					     &dwValid
					     );

	Init = String(pValue);

	return dwValid == TRUE;
	}

BOOL CStratonDatabase::SetVarInitValue(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pInit)
{
	return SetError(K5DB_SetVarInitValue( m_dwClient, 
						dwProject, 
						dwGroup, 
						dwVar, 
						LPCSTR(CAnsiString(pInit))
						));
	}

BOOL CStratonDatabase::CheckVarInitValue(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pInit)
{
	CError Error(FALSE); 

	return SetError(Error, K5DB_CheckVarInitValue( m_dwClient, 
						       dwProject, 
						       dwGroup, 
						       dwVar, 
						       LPCSTR(CAnsiString(pInit))
						       ));
	}

CString	CStratonDatabase::GetSerBuffer(void)
{
	LPCSTR pStr = K5DB_GetSerBuffer(m_dwClient);

	return String(pStr);
	}

void CStratonDatabase::SetSerBuffer(PCTXT pBuffer)
{
	K5DB_SetSerBuffer(m_dwClient, LPCSTR(CAnsiString(pBuffer)));
	}

void CStratonDatabase::ReleaseSerBuffer(void)
{
	K5DB_RealeaseSerBuffer(m_dwClient);
	}

BOOL CStratonDatabase::SerializeVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar)
{
	return SetError(K5DB_SerializeVar( m_dwClient, 
					   dwProject, 
					   dwGroup, 
					   dwVar
					   )); 
	}

DWORD CStratonDatabase::PasteSerializedVar(DWORD dwProject, DWORD dwGroup)
{
	return K5DB_PasteSerializedVar( m_dwClient, 
					dwProject, 
					dwGroup,
					DWORD(-1)
					);
	}

// Data Types

UINT CStratonDatabase::GetTypes(DWORD hProj, CLongArray &List)
{
	List.Empty();

	UINT uTypes = GetNbType(hProj);

	if( uTypes ) {

		PDWORD pTypes = New DWORD [ uTypes ];	
		
		GetTypes(hProj, pTypes);

		List.Append(pTypes, uTypes);

		delete[] pTypes;
		}

	return uTypes;
	}

DWORD CStratonDatabase::FindType(DWORD dwProject, PCTXT pTypeName)
{
	return K5DB_FindType( m_dwClient,
		              dwProject,
			      LPCSTR(CAnsiString(pTypeName))
			      );
	}

CString CStratonDatabase::GetTypeDesc(DWORD dwProject, DWORD dwType, DWORD &dwFlags)
{
	LPCSTR pStr = K5DB_GetTypeDesc( m_dwClient,
		                        dwProject,
					dwType,
					&dwFlags
					);

	return String(pStr);
	}

DWORD CStratonDatabase::GetTypeUDFB(DWORD dwProject, DWORD dwType)
{
	return K5DB_GetTypeUDFB( m_dwClient,
		                 dwProject,
				 dwType
				 );
	}

UINT CStratonDatabase::GetTypeParams(DWORD dwProject, DWORD dwType, CLongArray &List)
{
	List.Empty();

	UINT uTypes = GetNbTypeParam(dwProject, dwType);

	if( uTypes ) {

		PDWORD pTypes = New DWORD [ uTypes ];	
		
		GetTypeParams(dwProject, dwType, pTypes);

		List.Append(pTypes, uTypes);

		delete[] pTypes;
		}

	return uTypes;
	}

UINT CStratonDatabase::GetUDFBNbIO(DWORD dwProject, DWORD dwType, CLongArray &Inputs, CLongArray &Outputs)
{
	Inputs.Empty();

	Outputs.Empty();

	UINT uCount = IntGetUDFBNbIO( dwProject, dwType, NULL, NULL);

	if( uCount ) {
		
		PDWORD pInputs  = New DWORD [ uCount ];

		PDWORD pOutputs = New DWORD [ uCount ];

		IntGetUDFBNbIO( dwProject, dwType, pInputs, pOutputs);

		Inputs.Append(pInputs, 0);

		Outputs.Append(pOutputs, 0);
		
		delete [] pInputs;

		delete [] pOutputs;
		}

	return uCount;
	}

// Properties

BOOL CStratonDatabase::SetProperty(DWORD dwProject, DWORD dwHandle, DWORD dwProp, PCTXT pValue)
{
	return SetError(K5DB_SetProperty( m_dwClient, 
					  dwProject, 
					  dwHandle, 
					  dwProp, 
					  LPCSTR(CAnsiString(pValue))
					  ));
	}

CString	CStratonDatabase::GetProperty(DWORD dwProject, DWORD dwHandle, DWORD dwProp)
{
	LPCSTR pValue = K5DB_GetProperty( m_dwClient, dwProject, dwHandle, dwProp);

	return String(pValue);
	}

// Services

CFilename CStratonDatabase::GetProjectPath(DWORD dwHandle)
{
	LPSTR pPath = New char [ _MAX_PATH ];

	memset(pPath, 0, _MAX_PATH);

	DWORD dwCode = K5DB_GetProjectPath( m_dwClient,
		                            dwHandle,
					    pPath
					    );

	CString Path = CString(pPath);

	delete[] pPath;

	if( SetError(dwCode) ) {
		
		return Path;
		}

	return CString();
	}

CFilename CStratonDatabase::GetGlobalFilePath(DWORD dwProject, PCTXT pSuffix)
{
	LPSTR pPath = New char [ _MAX_PATH ];

	memset(pPath, 0, _MAX_PATH);

	DWORD dwCode = K5DB_GetGlobalFilePath( m_dwClient,
		                               dwProject,
					       LPCSTR(CAnsiString(pSuffix)),
					       pPath
					       );

	CString Path = CString(pPath);

	delete[] pPath;

	if( SetError(dwCode) ) {
		
		return Path;
		}

	return CString();
	}

CFilename CStratonDatabase::GetEqvPath(DWORD dwProject, BOOL fCommon, DWORD dwProgram)
{
	LPSTR pPath = New char [ _MAX_PATH ];

	memset(pPath, 0, _MAX_PATH);

	DWORD dwCode = K5DB_GetEqvPath( m_dwClient,
		                        dwProject,
					fCommon,
					dwProgram,
					pPath
					);

	CString Path = CString(pPath);

	delete[] pPath;

	if( SetError(dwCode) ) {
		
		return Path;
		}

	return CString();
	}

CFilename CStratonDatabase::GetProgramPath(DWORD dwProject, DWORD dwProgram)
{
	LPSTR pPath = New char [ _MAX_PATH ];

	DWORD dwCode = K5DB_GetProgramPath( m_dwClient,
		                            dwProject,
				            dwProgram,
					    pPath
				            );

	CString Path = CString(pPath);

	delete[] pPath;

	SetError(dwCode);

	return Path;
	}

// Events

CString CStratonDatabase::GetEventDesc(DWORD dwEvent)
{
	DWORD dwFlags;

	CString Text;

	//Text += GetClientName();

	if( !Text.IsEmpty() ) {

		Text += " - ";
		}
	
	Text +=  GetEventDesc(dwEvent, dwFlags);

	return Text;
	}

CString CStratonDatabase::GetEventDesc(DWORD dwEvent, DWORD &dwFlags)
{
	LPCSTR pStr = K5DB_GetEventDesc(dwEvent, &dwFlags);

	return String(pStr);
	}

// Comments

CString CStratonDatabase::GetComment(DWORD dwProject, DWORD dwHandle, DWORD dwType)
{
	UINT  uLen = K5DB_GetCommentLength( m_dwClient, 
					    dwProject, 
					    dwHandle, 
					    dwType
					    );

	LPSTR pStr = New char [ ++uLen ];

	DWORD dwLang = K5DB_GetComment( m_dwClient, 
					dwProject, 
					dwHandle, 
					dwType, 
					pStr, 
					uLen
					);

	AfxTouch(dwLang);

	CString Text(pStr);

	delete [] pStr;

	return Text;
	}

// Visual dialog for variable selection

BOOL CStratonDatabase::SelectVar(DWORD dwProject, PCTXT pText, CVarSelCtx &Ctx, DWORD &dwIdent)
{
	_s_K5DBvsel ctx;

	ctx.hParentGroup = Ctx.m_dwParent;
	ctx.hPrefType	 = Ctx.m_dwType;
	ctx.hwndParent   = Ctx.m_hWnd;
	ctx.ptPos.x	 = Ctx.m_Pos.x;
	ctx.ptPos.y	 = Ctx.m_Pos.y;
	ctx.dwOptions    = Ctx.m_dwOptions;

	BOOL fResult = K5DB_SelectVar( m_dwClient, 
				       dwProject, 
				       LPCSTR(CAnsiString(pText)),
				       &ctx, 
				       &dwIdent
				       );

	return fResult;
	}

// Internal

UINT CStratonDatabase::GetNbType(DWORD dwProject)
{
	return K5DB_GetNbType(m_dwClient, dwProject);
	}

UINT CStratonDatabase::GetTypes(DWORD dwProject, DWORD *pTypes)
{
	return K5DB_GetTypes( m_dwClient,
		              dwProject,
			      pTypes
			      );
	}

UINT CStratonDatabase::GetNbGroup(DWORD dwProject)
{
	return K5DB_GetNbGroup(m_dwClient, dwProject);
	}

UINT CStratonDatabase::GetGroups(DWORD dwProject, DWORD *pGroups)
{
	return K5DB_GetGroups( m_dwClient,
		              dwProject,
			      pGroups
			      );
	}

UINT CStratonDatabase::GetNbProgram(DWORD dwProject, DWORD dwSection)
{
	return K5DB_GetNbProgram(m_dwClient, dwProject, dwSection);
	}

UINT CStratonDatabase::GetPrograms(DWORD dwProject, DWORD dwSection, DWORD *pGroups)
{
	return K5DB_GetPrograms( m_dwClient,
		                 dwProject,
			         dwSection,
			         pGroups
			         );
	}

UINT CStratonDatabase::GetNbTypeParam(DWORD dwProject, DWORD dwType)
{
	return K5DB_GetNbTypeParam( m_dwClient, 
				    dwProject, 
				    dwType
				    );
	}

UINT CStratonDatabase::GetTypeParams(DWORD dwProject, DWORD dwType, DWORD *pParams)
{
	return K5DB_GetTypeParams( m_dwClient, 
				   dwProject, 
				   dwType,
				   pParams
				   );
	}

UINT CStratonDatabase::IntGetUDFBNbIO(DWORD dwProject, DWORD dwType, DWORD *pdwNbIn, DWORD *pdwNbOut)
{
	return K5DB_GetUDFBNbIO( m_dwClient, 
				 dwProject, 
				 dwType,
				 pdwNbIn,
				 pdwNbOut
				 );
	}

// Error Reporting

BOOL CStratonDatabase::SetError(CError &Error, DWORD dwCode)
{
	if( dwCode != K5DB_OK ) {

		CString Text = CString(K5DB_GetMessage(dwCode));

		if( Text.Right(1) != L"." ) {

			Text += ".";
			}

		Error.SetError(CPrintf(L"Error %d : %s", dwCode, Text));

		if( Error.AllowUI() ) {
		
			Error.Show(*afxMainWnd);
			}
		}

	return dwCode == K5DB_OK;
	}

BOOL CStratonDatabase::SetError(DWORD dwCode)
{
	return SetError(CError(TRUE), dwCode);
	}

// String Support

CString CStratonDatabase::String(LPCSTR pStr)
{
	return pStr ? CString(pStr) : CString();
	}

// End of File
