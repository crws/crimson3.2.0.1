
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Proerties
//

CStratonProperties::CStratonProperties(DWORD dwProject, DWORD dwHandle)
{
	m_dwProject = dwProject;

	m_dwHandle  = dwHandle;
	}

CStratonProperties::CStratonProperties(DWORD dwProject)
{
	m_dwProject = dwProject;

	m_dwHandle  = dwProject;
	}

// Operations

BOOL CStratonProperties::Set(DWORD dwProp, CString Value)
{
	return afxDatabase->SetProperty( m_dwProject, 
					 m_dwHandle, 
					 dwProp, 
					 Value
					 );

	}

CString CStratonProperties::Get(DWORD dwProp)
{
	return afxDatabase->GetProperty( m_dwProject, 
					 m_dwHandle, 
					 dwProp
					 );
	}

// End of File
