
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program
//

CStratonProgram::CStratonProgram(void)
{
	}

// Attributes

CFilename CStratonProgram::GetProgramName(UINT uType) const
{
	return GetProgramPath(uType).GetName();
	}

CFilename CStratonProgram::GetProgramPath(UINT uType) const
{
	switch( uType ) {

		case 0:
			return afxDatabase->GetProgramPath(m_dwProject, m_dwHandle);

		case 1:
			return afxDatabase->GetEqvPath(m_dwProject, FALSE, m_dwHandle);

		case 2:
			return afxDatabase->GetProgramPath(m_dwProject, m_dwHandle).WithType(L"dcl");
		}

	return CString();
	}

// Operations

BOOL CStratonProgram::Create(CString Name, DWORD dwLanguage, DWORD dwSection, DWORD dwParent)
{
	if( afxDatabase->CanCreateProgram( m_dwProject, 
					   dwLanguage, 
					   dwSection, 
					   dwParent, 
					   Name
					   )) {		

		m_dwHandle = afxDatabase->CreateProgram( m_dwProject, 
							 dwLanguage, 
							 dwSection, 
							 dwParent, 
							 Name
							 );

		//AfxAssert(m_dwHandle);

		return m_dwHandle != 0;
		}

	return FALSE;
	}

BOOL CStratonProgram::Delete(void)
{
	return afxDatabase->DeleteProgram( m_dwProject, 
				           m_dwHandle
				           );
	}

BOOL CStratonProgram::Rename(CError &Error, CString Name)
{
	CString Prev = CStratonProgramDescriptor(m_dwProject, m_dwHandle).m_Name;

	if( afxDatabase->RenameProgram(Error, m_dwProject, m_dwHandle, Name) ) {

		CStratonProperties Props(m_dwProject, m_dwHandle);

		Props.Set(K5DBPROP_USER + 1, Prev);

		return TRUE;
		}

	return FALSE;
	}

BOOL CStratonProgram::CanRename(CString Name)
{
	return !afxDatabase->FindProgram( m_dwProject, Name);
	}

void CStratonProgram::Save(void)
{
	afxDatabase->SaveProgramChanges(m_dwProject, m_dwHandle);
	}

BOOL CStratonProgram::Connect(CString Name)
{
	m_dwHandle = afxDatabase->FindProgram(m_dwProject, Name);

	return m_dwHandle ? TRUE : FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Program Descriptor
//

CStratonProgramDescriptor::CStratonProgramDescriptor(DWORD dwProject, DWORD dwHandle)
{
	m_dwProject = dwProject;

	m_dwHandle  = dwHandle;

	Get();
	}

// Operations

void CStratonProgramDescriptor::Set(void)
{
	}

void CStratonProgramDescriptor::Get(void)
{
	CStratonProperties Props(m_dwProject, m_dwHandle);

	m_Prev = Props.Get(K5DBPROP_USER + 1);

	m_Name = afxDatabase->GetProgramDesc( m_dwProject, 
					      m_dwHandle, 
					      m_dwLanguage, 
					      m_dwSection, 
					      m_dwParent
					      );
	}

// Attributes

BOOL CStratonProgramDescriptor::IsException(void)
{
	CString Path = afxDatabase->GetEqvPath( m_dwProject, FALSE, m_dwHandle);

	return FALSE;
	}

BOOL CStratonProgramDescriptor::IsEnabled(void)
{
	CString Prop = afxDatabase->GetProperty( m_dwProject, m_dwHandle, K5DBPROP_PRGCHECK);

	return Prop != "#FALSE#";
	}

BOOL CStratonProgramDescriptor::IsCalled(void)
{
	return !!afxDatabase->GetProgramOnCallFlag( m_dwProject, m_dwHandle);
	}

BOOL CStratonProgramDescriptor::IsUdFb(void)
{
	return !!(m_dwSection & K5DBSECTION_UDFB);
	}

WCHAR CStratonProgramDescriptor::GetSectName(void)
{
	UINT n = 0;

	while( !(m_dwSection & (1 << n)) ) {
		
		n ++;
		}

	return L"BSECU"[n];
	}

//////////////////////////////////////////////////////////////////////////
//
// Program Schedule
//

CStratonProgramSchedule::CStratonProgramSchedule(DWORD dwProject, DWORD dwHandle)
{
	m_dwProject = dwProject;

	m_dwHandle  = dwHandle;

	Get();
	}

// Operations

BOOL CStratonProgramSchedule::Set(void)
{
	return afxDatabase->SetProgramSchedule( m_dwProject, 
					     m_dwHandle, 
					     m_dwPeriod, 
					     m_dwOffset 
					     );
	}

void CStratonProgramSchedule::Get(void)
{
	if( afxDatabase->GetProgramSchedule( m_dwProject, 
					     m_dwHandle, 
					     m_dwPeriod, 
					     m_dwOffset) 
					     ){
		
		return;
		}

	AfxAssert(FALSE);
	}

// End of File
