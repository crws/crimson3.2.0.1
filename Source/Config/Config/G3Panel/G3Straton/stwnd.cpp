
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Structured Text Window
//

// Dynamic Class

AfxImplementDynamicClass(CStratonSTWnd, CStratonWnd);

// Constructor

CStratonSTWnd::CStratonSTWnd(void)
{
	m_pLib = CSTLibrary::FindInstance();
	}

// 4.17 - ST Control

void CStratonSTWnd::SetTab(int nTab)
{
	SendCommand(L"settab%d", nTab);
	}

void CStratonSTWnd::SetSyntaxColoring(CString Syntax)
{
	SendCommand(L"setsyntaxcoloring%s", Syntax);
	}

void CStratonSTWnd::EnableSyntax(BOOL fEnable)
{
	SendCommand(L"enablesyntax%b", fEnable);
	}

void CStratonSTWnd::SetValueInText(BOOL fEnable)
{
	SendCommand(L"setvalueintext%b", fEnable);
	}

BOOL CStratonSTWnd::GetValueInText(void)
{
	return BOOL(SendCommand(L"getvalueintext"));
	}

BOOL CStratonSTWnd::CanIndent(void)
{
	return BOOL(SendCommand(L"CanIndent"));
	}

BOOL CStratonSTWnd::Indent(void)
{
	return BOOL(SendCommand(L"Indent"));
	}

BOOL CStratonSTWnd::CanInsertComment(void)
{
	return BOOL(SendCommand(L"CanInsertComment"));
	}

BOOL CStratonSTWnd::InsertComment(void)
{
	return BOOL(SendCommand(L"InsertComment"));
	}

BOOL CStratonSTWnd::CanRemoveComment(void)
{
	return BOOL(SendCommand(L"CanRemoveComment"));
	}

BOOL CStratonSTWnd::RemoveComment(void)
{
	return BOOL(SendCommand(L"RemoveComment"));
	}

// End of File
