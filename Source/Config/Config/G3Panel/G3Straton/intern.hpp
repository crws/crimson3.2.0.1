
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Support
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3straton.hpp"

#include "intern.hxx"

#include "..\build.hxx"

#include "straton\K5DBApi.h"

#include "straton\K5DBRegApi.h"

#include "straton\W5EditApi.h"

#include "straton\W5EditFBDApi.h"

#include "straton\W5EditResApi.h"

#include "straton\K5CmpApi.h"

#include "straton\K5XRefApiLite.h"

#include "straton\K5MWApi.h"

//////////////////////////////////////////////////////////////////////////
//								
// Other Libraries
//

#pragma	comment(lib, "k5dbsrv.lib")

#pragma	comment(lib, "k5dbreg.lib")

#pragma	comment(lib, "k5mw.lib")

//////////////////////////////////////////////////////////////////////////
//
// Wide to Ansi String Conversion
//

extern LPSTR WideToAnsi(PCTXT pText);
extern LPSTR WidetoAnsi(CString Text);

/////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef const char *	(*PK5GETCLASSNAME)	(void);

typedef LRESULT		(*PK5EXECUTECOMMAND)	(HWND, int, LPSTR*);

typedef void		(*PK5SETSTRING)		(DWORD, LPCSTR);

//////////////////////////////////////////////////////////////////////////
//
// Command Builder
//

// cppcheck-suppress noCopyConstructor

class CCommandBuilder
{
	public:
		// Constructor
		CCommandBuilder(PCTXT pFormat, va_list pArgs);

		// Destructor
		~CCommandBuilder(void);

		// Attributes
		int     GetCount(void);
		LPSTR * GetArgs(void);

	protected:
		// Data
		CStringArray	m_List;
		LPSTR         * m_pArg;

		// Implementation
		BOOL AddName(UINT uIndex);
		BOOL AddBool(UINT uIndex, va_list & pArgs);
		BOOL AddLong(UINT uIndex, va_list & pArgs);
		BOOL AddInt (UINT uIndex, va_list & pArgs);
		BOOL AddText(UINT uIndex, va_list & pArgs);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStratonLibrary;
class CSFCLibrary;
class CSTLibrary;
class CFBDLibrary;
class CLDLibrary;

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

class CStratonLibrary
{
	public:
		// Constructor
		CStratonLibrary(PCTXT pName);

		// Command Execution
		LRESULT ExecuteCommand(HWND hWnd, CCommandBuilder &Cmd);

		// Class Definition
		PCTXT GetClassName(void) const;
		void  SetString(DWORD dwID, PCTXT pText);

	protected:
		// Data
		PCTXT			m_pName;
		HMODULE			m_hLib;
		PK5GETCLASSNAME		m_pfnGetClassName;
		PK5EXECUTECOMMAND	m_pfnExecuteCommand;
		PK5SETSTRING		m_pfnSetString;
		CString			m_Class;

		// Library Management
		void LoadLib(PCTXT pName);
		void FreeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

class CSFCLibrary : public CStratonLibrary
{
	public:
		// Object Location
		static CStratonLibrary * FindInstance(void);

		// Constructor
		CSFCLibrary(void);

		// Destructors
		~CSFCLibrary(void);

	protected:
		// Static Data
		static CStratonLibrary * m_pThis;
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

class CSTLibrary : public CStratonLibrary
{
	public:
		// Object Location
		static CStratonLibrary * FindInstance(void);

		// Constructor
		CSTLibrary(void);

		// Destructors
		~CSTLibrary(void);

	protected:
		// Static Data
		static CStratonLibrary * m_pThis;
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

class CFBDLibrary : public CStratonLibrary
{
	public:
		// Object Location
		static CStratonLibrary * FindInstance(void);

		// Constructor
		CFBDLibrary(void);

		// Destructors
		~CFBDLibrary(void);

	protected:
		// Static Data
		static CStratonLibrary * m_pThis;
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

class CLDLibrary : public CStratonLibrary
{
	public:
		// Object Location
		static CStratonLibrary * FindInstance(void);

		// Constructor
		CLDLibrary(void);

		// Destructors
		~CLDLibrary(void);

	protected:
		// Static Data
		static CStratonLibrary * m_pThis;
	};


//////////////////////////////////////////////////////////////////////////
//
// Straton Compiler
//

// Type Definitions

typedef BOOL	(*PFK5CMP_NeedBuild)(LPCSTR,int);
typedef void	(*PFK5CMP_CleanProject)(LPCSTR);
typedef BOOL	(*PFK5CMP_BuildDefault)(LPCSTR,HWND);
typedef BOOL	(*PFK5CMP_BuildDefaultFileReport)(LPCSTR,LPCSTR);
typedef BOOL	(*PFK5CMP_CheckProgram)(LPCSTR,LPCSTR,int,HWND,FILE*);
typedef BOOL	(*PFK5CMP_BuildOneUDFB)(LPCSTR,LPCSTR,int,HWND,FILE*);
typedef BOOL	(*PFK5CMP_CheckDefFile)(LPCSTR);
typedef LPCSTR	(*PFK5CMP_ExportVariables)(LPCSTR, LPCSTR, DWORD, DWORD);
typedef BOOL	(*PFK5CMP_ImportVariables)(LPCSTR, LPCSTR, LPCSTR, LPCSTR, DWORD, DWORD);
typedef BOOL	(*PFK5CMP_SLCheck)(DWORD *,LPSTR);
typedef BOOL	(*PFK5CMP_BuildProject)(LPCSTR,int,LPCSTR,LPCSTR,BOOL,HWND,FILE*,BOOL);

// Class Definition

class CStratonCompiler
{
	public:
		// Object Location
		static CStratonCompiler * FindInstance(void);

		// Constructor
		CStratonCompiler(void);

		// Operations
		BOOL	NeedBuild(PCTXT pPath);
		void	CleanProject(PCTXT pPath);
		BOOL	BuildDefault(PCTXT pPath, HWND hWnd);
		BOOL	BuildDefaultFileReport(PCTXT pPath, PCTXT pFile);
		BOOL	CheckProgram(PCTXT pPath, PCTXT pName, int iCodeType, HWND hWnd);
		BOOL	CheckProgram(PCTXT pPath, PCTXT pName, int iCodeType, HWND hWnd, FILE *pFile);
		BOOL	BuildOneUDFB(PCTXT pPath, PCTXT pName, int iCodeType, HWND hWnd, FILE *pFile);
		BOOL	CheckDefFile(PCTXT pPath);
		BOOL	SLCheck(UINT &uNbIO, CString &Oem);
		BOOL	BuildProject(PCTXT pPath, PCTXT pTarg, PCTXT pCode, BOOL fIntelEndian, HWND hWnd, BOOL fTraceFile);
		CString ExportVariables(PCTXT pPath, PCTXT pGroup, DWORD dwSyntax, DWORD dwFlags);
		BOOL	ImportVariables(PCTXT pPath, PCTXT pGroup, PCTXT pInput, PCTXT pError, DWORD dwSyntax, DWORD dwFlags);
		BOOL	ConversionNeedBuild(PCTXT pPath, PCTXT pName, DWORD dwLanguage);
		BOOL	CanConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage);
		BOOL	ConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage);

	protected:
		// Static Data
		static CStratonCompiler * m_pThis;

		// Data
		PCTXT				m_pName;
		HMODULE				m_hLib;
		PFK5CMP_NeedBuild		m_pfnNeedBuild;
		PFK5CMP_CleanProject		m_pfnCleanProject;
		PFK5CMP_BuildDefault		m_pfnBuildDefault;
		PFK5CMP_BuildDefaultFileReport	m_pfnBuildDefaultFileReport;
		PFK5CMP_CheckProgram		m_pfnCheckProgram;
		PFK5CMP_BuildOneUDFB		m_pfnBuildOneUDFB;
		PFK5CMP_CheckDefFile		m_pfnCheckDefFile;
		PFK5CMP_ConversionNeedBuild	m_pfnConversionNeedBuild;
		PFK5CMP_CanConvertProgram	m_pfnCanConvertProgram;
		PFK5CMP_ConvertProgram		m_pfnConvertProgram;
		PFK5CMP_SLCheck			m_pfnSLCheck;
		PFK5CMP_BuildProject		m_pfnBuildProject;
		PFK5CMP_ExportVariables		m_pExportVariables;
		PFK5CMP_ImportVariables		m_pImportVariables;

		// Library Management
		void LoadLib(PCTXT pName);
		void FreeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// XRef Object
//

// Type Definitions 

typedef int	(*PPK5XREF_GetVersion)(void);
typedef int	(*PPK5XREF_FindInFiles)(LPCSTR, LPCSTR, HWND, FILE *, DWORD, DWORD);
typedef int	(*PPK5XREF_FindUnusedVars)(LPCSTR, HWND, FILE *, DWORD, DWORD);
typedef int	(*PPK5XREF_FindMultAssign)(LPCSTR, HWND, FILE *, DWORD, DWORD);
typedef int	(*PPK5XREF_FindInProg)(LPCSTR, LPCSTR, LPCSTR, DWORD, HWND, FILE *, DWORD, DWORD);
typedef int	(*PPK5XREF_FindUsedLibs)(LPCSTR, HWND, FILE *, DWORD, DWORD);
typedef int	(*PPK5XREF_ReplaceInFiles)(HWND, LPCSTR, LPCSTR, HWND, FILE *, DWORD, DWORD);
typedef int	(*PPK5XREF_AutoReplaceInFiles)(HWND, LPCSTR, LPCSTR, LPCSTR, HWND, FILE *, DWORD, DWORD);

// Class

class CCrossReference
{
	public:
		// Object Location
		static CCrossReference * FindInstance(void);

		// Constructor
		CCrossReference(void);

		// Version
		INT	GetVersion(void);

		// Operations
		UINT FindInFiles(PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile);
		UINT FindUnusedVars(PCTXT pPath, HWND hWnd, FILE *pFile);
		UINT FindMultAssign(PCTXT pPath, HWND hWnd, FILE *pFile);
		UINT FindInProg(PCTXT pPath, PCTXT pName, PCTXT pFind, DWORD dwLanguage, HWND hWnd, FILE *pFile);
		UINT FindUsedLibs(PCTXT pPath, HWND hWnd, FILE *pFile);
		UINT ReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile);
		UINT AutoReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, PCTXT pReplace, HWND hWnd, FILE *pFile);

	protected:
		// Static Data
		static CCrossReference * m_pThis;
		static DWORD	     m_dwClient;

		// Data
		PCTXT				m_pName;
		HMODULE				m_hLib;
		PPK5XREF_GetVersion		m_pfnGetVersion;
		PPK5XREF_FindInFiles		m_pfnFindInFiles;
		PPK5XREF_FindUnusedVars		m_pfnFindUnusedVars;
		PPK5XREF_FindMultAssign		m_pfnFindMultAssign;
		PPK5XREF_FindInProg		m_pfnFindInProg;
		PPK5XREF_FindUsedLibs		m_pfnFindUsedLibs;
		PPK5XREF_ReplaceInFiles		m_pfnReplaceInFiles;
		PPK5XREF_AutoReplaceInFiles	m_pfnAutoReplaceInFiles;

		// Library Management
		void LoadLib(PCTXT pName);
		void FreeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// W5EditRes Object
//

// Type Definitions 

typedef LPCSTR	(*PPW5RES_SelBlockEx)(HWND, LPCSTR, LPCSTR, int *, int *, DWORD *, BOOL *, BOOL *);

// Class

class CEditResource
{
	public:
		// Object Location
		static CEditResource * FindInstance(void);

		// Constructor
		CEditResource(void);

		// Destructor
		~CEditResource(void);

		// Operations
		CString SelBlockEx(HWND hWnd, PCTXT pPath, PCTXT pName, int &nIn, int &nOut, DWORD &dwIdent, BOOL &fInst, BOOL &dDB);

	protected:
		// Static Data
		static CEditResource * m_pThis;

		// Data
		PCTXT				m_pName;
		HMODULE				m_hLib;
		PPW5RES_SelBlockEx		m_pSelBlockEx;

		// Library Management
		void LoadLib(PCTXT pName);
		void FreeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Block Properties Dialog
//

class CBlockPropertiesDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CBlockPropertiesDialog(CFunctionBlock &FB);

	protected:
		// Data
		CFunctionBlock	*m_pFB;
				 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCommandYes(UINT uID);
		BOOL OnCommandNo(UINT uID);

		// Implementation
		void LoadCaption(void);
		void LoadInputs(void);
		BOOL ReadInputs(void);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Unsupported Function Blocks File Builder
//

class CUnsupportedBlocks
{
	public:
		// Constructor
		CUnsupportedBlocks(void);

		// Destructor
		~CUnsupportedBlocks(void);

	protected:
		// Data
		FILE	*m_pFile;

		// Implementation
		void Build(void);
	};

// End of File
