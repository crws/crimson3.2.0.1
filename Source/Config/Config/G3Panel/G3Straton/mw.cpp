
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Middleware Object
//

// Static Data

CMiddleware * CMiddleware::m_pThis	= NULL;

DWORD	   CMiddleware::m_dwClient	= 0;

// Object Location

CMiddleware * CMiddleware::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CMiddleware::CMiddleware(void)
{
	AfxAssert(m_pThis == NULL);

	LoadLib(L"K5MW");

	m_pThis		= this;

	m_dwClient	= 0;
	}

// Destructor

CMiddleware::~CMiddleware(void)
{
	}

// Compatibility

INT CMiddleware::GetVersion(void)
{
	return K5MW_GetVersion();
	}

// Data Server Connection

BOOL CMiddleware::Connect(HWND hCallback, DWORD dwFlags)
{
	m_dwClient = K5MW_Connect( hCallback, 
				   WM_MWEVENT, 
				   dwFlags, 
				   0, 
				   "Client"
				   );

	return m_dwClient != 0;
	}

BOOL CMiddleware::SetCallback(HWND hWnd)
{
	return SetError(K5MW_SetCallback( m_dwClient, 
					  hWnd, 
					  WM_MWEVENT
					  ));
	}

void CMiddleware::Disconnect(void)
{
	K5MW_Disconnect(m_dwClient);	
	}

// Project Connection

DWORD CMiddleware::FindProject(PCTXT pPath)
{
	return K5MW_FindProject(LPCSTR(CAnsiString(pPath)));
	}

DWORD CMiddleware::OpenProject(PCTXT pPath, PCTXT pDriver, PCTXT pSetup, DWORD dwTargetID, DWORD dwProtocol)
{
	return K5MW_OpenProject( m_dwClient, 
				 LPCSTR(CAnsiString(pPath)),
				 LPCSTR(CAnsiString(pDriver)),
				 LPCSTR(CAnsiString(pSetup)),
				 dwTargetID,
				 dwProtocol
				 );
	}

void CMiddleware::CloseProject(DWORD dwProject)
{
	K5MW_CloseProject( m_dwClient, dwProject);
	}

BOOL CMiddleware::IsCommOk(DWORD dwProject)
{
	return K5MW_IsCommOK( m_dwClient, dwProject);
	}

BOOL CMiddleware::IsSimulMode(DWORD dwProject)
{
	return K5MW_IsSimulMode( m_dwClient, dwProject);
	}

CString CMiddleware::GetStatus(DWORD dwProject, DWORD &dwCon, DWORD &dwApp)
{
	return String(K5MW_GetStatus( m_dwClient, 
				      dwProject, 
				      &dwCon, 
				      &dwApp
				      ));
	}

void CMiddleware::SetHexMode(DWORD dwProject, BOOL fEnable)
{
	K5MW_SetHexMode(m_dwClient, dwProject, fEnable);
	}

BOOL CMiddleware::GetHexMode(DWORD dwProject)
{
	return BOOL(K5MW_GetHexMode(m_dwClient, dwProject) > 0);
	}

// Spying Variables

BOOL CMiddleware::Subscribe(DWORD dwProject, DWORD dwLoopID, PCTXT pSymbol, PCTXT pParent)
{
	return SetError(K5MW_Subscribe( m_dwClient, 
					dwProject, 
					dwLoopID, 
					LPCSTR(CAnsiString(pSymbol)), 
					LPCSTR(CAnsiString(pParent))
					));
	}

BOOL CMiddleware::Unsubscribe(DWORD dwProject, DWORD dwLoopID)
{
	return SetError(K5MW_Unsubscribe(m_dwClient, dwProject, dwLoopID));
	}

// Read Variables

CString CMiddleware::GetStringValue(DWORD dwProject, PCTXT pSymbol, PCTXT pParent)
{
	LPCSTR pValue = K5MW_GetStringValue( m_dwClient, 
					     dwProject, 
					     LPCSTR(CAnsiString(pSymbol)), 
					     LPCSTR(CAnsiString(pParent))
					     );

	return String(pValue);
	}

CString CMiddleware::GetIDStringValue(DWORD dwProject, DWORD dwLoopID)
{
	LPCSTR pValue = K5MW_GetIDStringValue( m_dwClient, 
					       dwProject,
					       dwLoopID
					       );

	return String(pValue);
	}

BOOL CMiddleware::GetBinValue(DWORD dwProject, PCTXT pSymbol, PCTXT pParent, BYTE &bValue, DWORD &dwValid, DWORD &dwLocked)
{
	// TODO -- Implement

	return FALSE;
	}

BOOL CMiddleware::GetIDBinValue(DWORD dwProject, DWORD dwLoopID, BYTE &bValue, DWORD &dwValid, DWORD &dwLocked)
{
	// TODO -- Implement

	return FALSE;
	}

// Commands

BOOL CMiddleware::Control(DWORD dwProject, HWND hWnd, CPoint Pos, PCTXT pSymbol, PCTXT pParent)
{
	return SetError(K5MW_Control( m_dwClient, 
				      dwProject, 
				      hWnd, 
				      Pos.x, 
				      Pos.y, 
				      LPCSTR(CAnsiString(pSymbol)), 
				      LPCSTR(CAnsiString(pParent)),
				      0
				      ));
	}

BOOL CMiddleware::Execute(DWORD dwProject, PCTXT pCommand)
{
	return SetError(K5MW_Execute( m_dwClient, 
				      dwProject, 
				      LPCSTR(CAnsiString(pCommand))
				      ));
	}

// Miscellaneous

CString CMiddleware::GetMessage(void)
{
	return CString();
	}

BOOL CMiddleware::SubscribeToLog(void)
{
	return FALSE;
	}

// Error Reporting

BOOL CMiddleware::SetError(CError &Error, DWORD dwCode)
{
	#ifdef MW_LINKED

	if( dwCode != K5DB_OK ) {

		CString Text = CString(K5MW_GetMessage(dwCode));

		Error.SetError(CPrintf(L"%d\t - %s", dwCode, Text));

		if( Error.AllowUI() ) {
		
			Error.Show(*afxMainWnd);
			}
		}

	#endif

	return dwCode == K5DB_OK;
	}

BOOL CMiddleware::SetError(DWORD dwCode)
{
	return SetError(CError(TRUE), dwCode);
	}

// String Support

CString CMiddleware::String(LPCSTR pStr)
{
	return pStr ? CString(pStr) : CString();
	}

// Library Management

void CMiddleware::LoadLib(PCTXT pName)
{
	CFilename Path = afxModule->GetFilename().GetDirectory();

	CFilename Name = Path + L"Straton\\" + pName;

	if( (m_hLib = LoadLibrary(Name)) ) {
		
		return;
		}

	DWORD dwError = GetLastError();

	AfxTrace(L"ERROR: Failed to load %s\t%8.8X\n", pName, dwError);

	AfxAssert(m_hLib);
	}

void CMiddleware::FreeLib(void)
{
	AfxVerify(FreeLibrary(m_hLib));
	}

// End of File
