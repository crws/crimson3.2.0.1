
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Variable Create Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CVariableSelectDialog, CStdDialog);

// Constructor

CVariableSelectDialog::CVariableSelectDialog(DWORD dwProject)
{
	m_dwProject = dwProject;

	SetName(L"VariableSelectDialog");
	}

// Message Map

AfxMessageMap(CVariableSelectDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PREDESTROY)

	AfxDispatchNotify(202, CBN_SELCHANGE, OnModeChange)

	AfxDispatchCommand(IDOK,  OnCommandYes)
	AfxDispatchCommand(IDNO,  OnCommandNo)

	AfxMessageEnd(CVariableSelectDialog)
	};

// Message Handlers

BOOL CVariableSelectDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
//	LoadConfig();

//	LoadCaption();

	LoadMode();

	return TRUE;
	}

void CVariableSelectDialog::OnPreDestroy(void)
{
//	SaveConfig();
	}

// Notification Handlers

void CVariableSelectDialog::OnModeChange(UINT uID, CWnd &Wnd)
{
	CComboBox &List = (CComboBox &) GetDlgItem(202);

	if( List.GetCurSelData() == modeVariable ) {
		
		Information(CString(IDS_PICK_VARIABLEN));
		}

	if( List.GetCurSelData() == modeCreate ) {
		
		CVariableCreateDialog Dlg(m_dwProject);

		CSystemWnd  &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

		if( System.ExecSemiModal(Dlg, L"Variables") ) {

			}
		}
	}

// Command Handlers

BOOL CVariableSelectDialog::OnCommandYes(UINT uID)
{
	if( TRUE ) {

		EndDialog(1);
		}

	return TRUE;
	}

BOOL CVariableSelectDialog::OnCommandNo(UINT uID)
{
	EndDialog(0);

	return TRUE;
	}

// Implementation

void CVariableSelectDialog::LoadMode(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(202);
	
	List.AddString(CString(IDS_GENERAL),	    modeGeneral);
	List.AddString(CString(IDS_VARIABLE),	    modeVariable);
	List.AddString(CString(IDS_NEW_VARIABLE), modeCreate);

	List.SelectData(modeGeneral);
	}

// End of File

