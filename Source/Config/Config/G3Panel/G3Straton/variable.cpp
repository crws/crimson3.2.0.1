
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Variable
//

CStratonVariable::CStratonVariable(void)
{
	}

// Attributes

DWORD CStratonVariable::GetGroup(void) const
{
	return afxDatabase->GetVarGroup(m_dwProject, m_dwHandle);
	}

// Operations

BOOL CStratonVariable::Create(CString Name, DWORD dwGroup, DWORD dwType, DWORD dwExtent, DWORD dwFlags)
{
	CStratonDataTypeDescriptor Type(m_dwProject, dwType);

	CStratonGroupDescriptor  Group(m_dwProject, dwGroup);

	DWORD dwLen = Type.IsString() ? 255 : 0;

	m_dwHandle = afxDatabase->CreateVar( m_dwProject, 
					     dwGroup, 
					     dwType, 
					     dwExtent, 
					     dwLen, 
					     dwFlags, 
					     Name
					     );

	//AfxAssert(m_dwHandle);

	return m_dwHandle != 0;
	}

BOOL CStratonVariable::Delete(void)
{
	DWORD dwGroup = afxDatabase->GetVarGroup(m_dwProject, m_dwHandle);

	return afxDatabase->DeleteVar( m_dwProject, 
				       dwGroup, 
				       m_dwHandle
				       );
	}

BOOL CStratonVariable::Rename(CString Name)
{
	DWORD dwGroup = afxDatabase->GetVarGroup(m_dwProject, m_dwHandle);
	
	return afxDatabase->RenameVar( m_dwProject, 
				       dwGroup, 
				       m_dwHandle, 
				       Name
				       );
	}

BOOL CStratonVariable::CanRename(CString Name)
{
	DWORD dwGroup = afxDatabase->GetVarGroup( m_dwProject, m_dwHandle);

	return afxDatabase->CanRenameVar( m_dwProject, dwGroup, m_dwHandle, Name);
	}

void CStratonVariable::SetHandle(DWORD dwHandle)
{
	m_dwHandle = dwHandle;
	}

BOOL CStratonVariable::Connect(DWORD dwGroup, CString Name)
{
	m_dwHandle = afxDatabase->FindVarInGroup( m_dwProject, 
						  dwGroup,
						  Name
						  );

	return m_dwHandle ? TRUE : FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Descriptor
//

// Constructor

CStratonVariableDescriptor::CStratonVariableDescriptor(DWORD dwProject, DWORD dwHandle)
{
	m_dwProject = dwProject;

	m_dwHandle = dwHandle;

	m_dwType   = 0;

	m_dwGroup  = 0;

	Get();
	}

// Attributes

BOOL CStratonVariableDescriptor::CanSet(CError &Error)
{
	BOOL fString = CStratonDataTypeDescriptor(m_dwProject, m_dwType).IsString();

	return afxDatabase->CanSetVariableDesc( m_dwProject, 
					        m_dwHandle, 
					        m_dwType, 
					        m_dwDim, 
					        fString ? (m_dwLen ? m_dwLen : K5DBMAX_STRING) : 0, 
					        m_dwFlags 
					        );
	}

// Operations

BOOL CStratonVariableDescriptor::Set(void)
{
	BOOL fString = CStratonDataTypeDescriptor(m_dwProject, m_dwType).m_dwFlags & K5DBTYPE_STRING;

	if( afxDatabase->SetVariableDesc( m_dwProject, 
					  m_dwHandle, 
					  m_dwType, 
					  m_dwDim, 
					  fString ? (m_dwLen ? m_dwLen : K5DBMAX_STRING) : 0, 
					  m_dwFlags 
					  ) ) {

		Get();
		
		return TRUE;
		}

	return FALSE;
	}

void CStratonVariableDescriptor::Get(void)
{
	m_dwGroup = afxDatabase->GetVarGroup( m_dwProject,
					      m_dwHandle
					      );

	m_Name    = afxDatabase->GetVariableDesc( m_dwProject, 
					          m_dwHandle, 
					          m_dwType, 
					          m_dwDim, 
					          m_dwLen, 
					          m_dwFlags 
					          );

	m_Prev    = afxDatabase->GetVarPrevName( m_dwProject, 
					         m_dwHandle
					         );

	afxDatabase->GetVarInitValue( m_dwProject, 
				      m_dwGroup, 
				      m_dwHandle, 
				      m_Init
				      );
	}

// Initial Data

BOOL CStratonVariableDescriptor::CheckInitValue(CError &Error, CString Text)
{
	return afxDatabase->CheckVarInitValue( m_dwProject, 
					       m_dwGroup, 
					       m_dwHandle, 
					       Text
					       );
	}

BOOL CStratonVariableDescriptor::SetInitValue(CError &Error, CString Text)
{
	if( afxDatabase->SetVarInitValue( m_dwProject, 
				          m_dwGroup, 
				          m_dwHandle, 
				          Text
				          ) ) {

		afxDatabase->GetVarInitValue( m_dwProject, 
					      m_dwGroup, 
					      m_dwHandle, 
					      m_Init
					      );

		return TRUE;
		}

	return FALSE;
	}

// Attributes

CString CStratonVariableDescriptor::GetGroupName(void)
{
	return CStratonGroupDescriptor(m_dwProject, m_dwGroup).m_Name;
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Serializer
//

// Constructor

CStratonVariableSerializeBuffer::CStratonVariableSerializeBuffer(DWORD dwProject, DWORD dwGroup, DWORD dwIdent)
{
	m_dwProject = dwProject; 

	m_dwGroup   = dwGroup;

	m_dwIdent   = dwIdent;
	}

// Destructor

CStratonVariableSerializeBuffer::~CStratonVariableSerializeBuffer(void)
{
	afxDatabase->ReleaseSerBuffer();
	}

// Operations

BOOL CStratonVariableSerializeBuffer::Serialize(void)
{
	return afxDatabase->SerializeVar(m_dwProject, m_dwGroup, m_dwIdent);
	}

DWORD CStratonVariableSerializeBuffer::Paste(DWORD dwGroup)
{
	return afxDatabase->PasteSerializedVar(m_dwProject, dwGroup);
	}

BOOL CStratonVariableSerializeBuffer::DeleteVar(void)
{
	return afxDatabase->DeleteVar(m_dwProject, m_dwGroup, m_dwIdent);
	}

// End of File
