
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Unsupported Function Blocks File Builder
//

// Constructor

CUnsupportedBlocks::CUnsupportedBlocks(void)
{
	CString Path;

	Path += afxRegistry->GetLibFolderPath();

	Path += L"\\";

	Path += L"UnsupBlocks.def";

	if( (m_pFile = _wfopen(Path, L"w")) ) {

		LPCSTR pLine = "; Unsupported Blocks";

		fputs(pLine, m_pFile);

		fputs("\n\n", m_pFile);
		
		Build();
		}
	}

// Destructor

CUnsupportedBlocks::~CUnsupportedBlocks(void)
{
	if( m_pFile ) {
		
		fclose(m_pFile);

		m_pFile = NULL;
		}
	}

// Implementation

void CUnsupportedBlocks::Build(void)
{
	CLongArray List;

	UINT c;
	
	if( (c = afxRegistry->GetBlocks(K5DBREG_ALL, List)) ) {
		
		for( UINT n = 0; n < c; n ++ ) {

			DWORD dwIdent = List[n];

			CFunctionDefinition Defn(dwIdent);

			if( Defn.CanLoad() ) {

				continue;
				}

			CAnsiString Name(Defn.GetName());

			fputs(Name, m_pFile);

			fputs("\n", m_pFile);
			}
		}
	}

// End of File
