
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Instruction List Editor Window
//

class CInstructionListEditorWnd : public CEditorWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CInstructionListEditorWnd(void);

	protected:
		// Data

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);

		// Implementation
		void ShowStatus(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// Instruction List Editor Window
//

// Dynamic Class

AfxImplementDynamicClass(CInstructionListEditorWnd, CEditorWnd);

// Constructor

CInstructionListEditorWnd::CInstructionListEditorWnd(void)
{
	m_pEdit = New CStratonILWnd;
	}

// Message Map

AfxMessageMap(CInstructionListEditorWnd, CEditorWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxMessageEnd(CInstructionListEditorWnd)
	};

// Message Handlers

void CInstructionListEditorWnd::OnPostCreate(void)
{
	CEditorWnd::OnPostCreate();
	}

// Implementation

void CInstructionListEditorWnd::ShowStatus(void)
{
	CString Text;

	afxThread->SetStatusText(Text);
	}

// End of File
