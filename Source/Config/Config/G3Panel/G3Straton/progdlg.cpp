
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Create Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CProgramCreateDialog, CStdDialog);

// Static Data

CString	CProgramCreateDialog::m_Name	= CString();

DWORD	CProgramCreateDialog::m_Lang	= K5DBLANGUAGE_FBD;

DWORD	CProgramCreateDialog::m_Sect	= K5DBSECTION_END;

DWORD	CProgramCreateDialog::m_Call	= callMainCycle;

// Constructor

CProgramCreateDialog::CProgramCreateDialog(DWORD dwProject)
{
	m_dwProject = dwProject;

	m_dwParent  = 0;

	SetName(L"ProgramCreateDialog");
	}

CProgramCreateDialog::CProgramCreateDialog(DWORD dwProject, DWORD dwParent)
{
	m_dwProject = dwProject;

	m_dwParent  = dwParent;

	SetName(L"ProgramCreateDialog");
	}

CProgramCreateDialog::CProgramCreateDialog(DWORD dwProject, DWORD dwParent, DWORD dwCall)
{
	m_dwProject = dwProject;

	m_dwParent  = dwParent;

	m_Call      = dwCall;

	SetName(L"ProgramCreateDialog1");
	}

// Attributes

CString CProgramCreateDialog::GetName(void)
{
	return m_Name;
	}

DWORD CProgramCreateDialog::GetLang(void)
{
	return m_Lang;
	}

DWORD CProgramCreateDialog::GetSect(void)
{
	return m_Sect;
	}

DWORD CProgramCreateDialog::GetCall(void)
{
	return m_Call;
	}

// Message Map

AfxMessageMap(CProgramCreateDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PREDESTROY)

	AfxDispatchNotify(200, EN_CHANGE,     OnEditChange)
	AfxDispatchNotify(201, CBN_SELCHANGE, OnLangChange)
	AfxDispatchNotify(202, CBN_SELCHANGE, OnCallChange)

	AfxDispatchCommand(IDOK,  OnCommandYes)
	AfxDispatchCommand(IDNO,  OnCommandNo)

	AfxMessageEnd(CProgramCreateDialog)
	};

// Message Handlers

BOOL CProgramCreateDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadConfig();

	LoadCaption();

	LoadSect();

	LoadLang();

	LoadName();

	return TRUE;
	}

void CProgramCreateDialog::OnPreDestroy(void)
{
	SaveConfig();
	}

// Notification Handlers

void CProgramCreateDialog::OnEditChange(UINT uID, CWnd &Ctrl)
{
	CEditCtrl &Edit = (CEditCtrl &) Ctrl;

	GetDlgItem(IDOK).EnableWindow(Edit.GetWindowTextLength() > 0);
	}

void CProgramCreateDialog::OnLangChange(UINT uID, CWnd &Ctrl)
{
	CComboBox &Call = (CComboBox &) GetDlgItem(202);

	Call.ResetContent();
	}

void CProgramCreateDialog::OnCallChange(UINT uID, CWnd &Ctrl)
{
	CComboBox &Call = (CComboBox &) GetDlgItem(202);

	Call.GetCurSelData();
	}

// Command Handlers

BOOL CProgramCreateDialog::OnCommandYes(UINT uID)
{
	ReadLang();

	ReadSect();

	if( ReadName() ) {
		
		EndDialog(1);
		}

	return TRUE;
	}

BOOL CProgramCreateDialog::OnCommandNo(UINT uID)
{
	EndDialog(0);

	return TRUE;
	}

// Implementation

void CProgramCreateDialog::LoadCaption(void)
{
	CString Text;

	Text += CString(IDS_CREATE_NEW);

	if( m_dwParent ) {

		CString Name = CStratonProgramDescriptor(m_dwProject, m_dwParent).m_Name;

		Text += CPrintf(CString(IDS_CHILD_OF_FMT), Name);
		}

	SetWindowText(Text);
	}

void CProgramCreateDialog::LoadName(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	for( UINT n = 1;; n++ ) {

		CFormat Name("Program%1!u!", n);

		DWORD dwLanguage = m_Lang;
		DWORD dwSection  = m_Sect;
		DWORD dwParent   = m_dwParent;

		if( afxDatabase->CanCreateProgram( m_dwProject, 
						   dwLanguage, 
						   dwSection, 
						   dwParent, 
						   Name) ) {
				
			Edit.SetWindowText(Name);
				
			break;
			}
		}

	OnEditChange(200, Edit);

	SetDlgFocus (200);
	}

void CProgramCreateDialog::LoadLang(void)
{
	CComboBox &Lang = (CComboBox &) GetDlgItem(201);

	if( m_Sect & K5DBSECTION_CHILD ) {

		Lang.AddString(CString(IDS_SEQUENTIAL_FLOW),	K5DBLANGUAGE_SFC);
		
		Lang.SelectData(m_Lang);

		return;
		}

	if( m_Sect & K5DBSECTION_UDFB ) {

		Lang.AddString(CString(IDS_STRUCTURED_TEXT),		K5DBLANGUAGE_ST);

		Lang.AddString(CString(IDS_FUNCTION_BLOCK),	K5DBLANGUAGE_FBD);

		Lang.AddString(CString(IDS_LADDER_DIAGRAM),		K5DBLANGUAGE_LD);

		Lang.AddString(CString(IDS_INSTRUCTION_LIST),		K5DBLANGUAGE_IL);

		Lang.SelectData(m_Lang);		
		}
	else {
		if( m_Sect & K5DBSECTION_END ) {

			// LATER - support SFC

			/*Lang.AddString(CString(IDS_SEQUENTIAL_FLOW),	K5DBLANGUAGE_SFC);*/
			}

		Lang.AddString(CString(IDS_STRUCTURED_TEXT),		K5DBLANGUAGE_ST);

		Lang.AddString(CString(IDS_FUNCTION_BLOCK),	K5DBLANGUAGE_FBD);

		Lang.AddString(CString(IDS_LADDER_DIAGRAM),		K5DBLANGUAGE_LD);

		Lang.AddString(CString(IDS_INSTRUCTION_LIST),		K5DBLANGUAGE_IL);
		}

	for( UINT n = 0; n < Lang.GetCount(); n ++ ) {

		if( m_Lang == Lang.GetItemData(n) ) {

			Lang.SelectData(m_Lang);
			
			return;
			}
		}	

	Lang.SetCurSel(0);
	}

void CProgramCreateDialog::LoadSect(void)
{
	if( m_dwParent ) {

		m_Sect = K5DBSECTION_CHILD;

		m_Lang = K5DBLANGUAGE_SFC;
		
		return;
		}
	
	if( m_Call == callUdfb ) {
			
		m_Sect = K5DBSECTION_UDFB;

		return;
		}

	if( m_Call == callSubProgram ) {
			
		m_Sect = K5DBSECTION_BEGIN;

		return;
		}

	if( m_Lang == K5DBLANGUAGE_SFC ) {

		m_Sect = K5DBSECTION_SFC;
		
		return;
		}

	m_Sect = K5DBSECTION_END;
	}

BOOL CProgramCreateDialog::ReadName(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	CString Name = Edit.GetWindowText();

	CError Error(TRUE);

	CProgramNamer Prog(m_dwProject, m_Lang, m_Sect);

	if( Prog.Validate(Error, Name) ) {

		if( !Prog.CanFindName(Error, Name) ) {

			m_Name = Name;
		
			return TRUE;
			}
		}

	Error.Show(*afxMainWnd);

	return FALSE;
	}

BOOL CProgramCreateDialog::ReadLang(void)
{
	CComboBox &Lang = (CComboBox &) GetDlgItem(201);

	m_Lang = Lang.GetCurSelData();

	return TRUE;
	}

BOOL CProgramCreateDialog::ReadSect(void)
{
	if( FALSE ) {

		CComboBox &Lang = (CComboBox &) GetDlgItem(201);

		CComboBox &Call = (CComboBox &) GetDlgItem(202);

		if( Call.GetCurSelData() == callUdfb ) {
		
			m_Sect = K5DBSECTION_UDFB;

			return TRUE;
			}

		switch( Lang.GetCurSelData() ) {
		
			case K5DBLANGUAGE_SFC:

				if( m_dwParent ) {
				
					m_Sect = K5DBSECTION_CHILD;
					}
				else {		
					m_Sect = K5DBSECTION_SFC;
					}

				break;

			default:

				if( m_Call == callSubProgram ) {
				
					m_Sect = K5DBSECTION_BEGIN;
					}
				else {
					m_Sect = K5DBSECTION_END;
					}

				break;
			}
		}

	return TRUE;
	}

void CProgramCreateDialog::LoadConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(L"NewProgram");

	m_Lang = Reg.GetValue(L"Lang", K5DBLANGUAGE_FBD);
	}

void CProgramCreateDialog::SaveConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(L"NewProgram");

	Reg.SetValue(L"Lang",  UINT(GetLang()));
	}

// End of File
