
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Support
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3STRATON_HPP
	
#define	INCLUDE_G3STRATON_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3straton.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3STRATON

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3straton.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Init and Term Hooks
//

extern void DLLAPI StratonInit(void);

extern void DLLAPI StratonTerm(void);

//////////////////////////////////////////////////////////////////////////
//								
// Framework Messages
//

#define	WM_DATABASE	(WM_APP + 0x500)
#define	WM_DBBUILD	(WM_APP + 0x501)
#define	WM_MWEVENT	(WM_APP + 0x502)

//////////////////////////////////////////////////////////////////////////
//
// Notification Dispatch
//

#include "g3dbmsg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ansi String Class
//

class DLLAPI CAnsiString
{
	public:
		// Constructor
		CAnsiString(PCTXT pText);

		// Destructor
		~CAnsiString(void);

		// Attributes
		UINT GetLength(void);

		// Conversion
		operator LPCSTR (void) const;

	protected:
		// Data
		LPSTR m_pText;
	};

//////////////////////////////////////////////////////////////////////////
//
// Database - Variable Selection Dialog
//

struct DLLAPI CVarSelCtx
{
	DWORD	m_dwParent;
	DWORD	m_dwType;
	HWND	m_hWnd;
	CPoint  m_Pos;
	DWORD	m_dwOptions;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStratonDatabase;
class CStratonRegistry;
class CMiddleware;

//////////////////////////////////////////////////////////////////////////
//
// Object Location
//

#define afxDatabase	CStratonDatabase::FindInstance()
#define afxRegistry	CStratonRegistry::FindInstance()
#define afxMiddleware	CMiddleware::FindInstance()

//////////////////////////////////////////////////////////////////////////
//
// Database
//

class DLLAPI CStratonDatabase
{
	public:
		// Object Location
		static CStratonDatabase * FindInstance(void);

		// Constructor
		CStratonDatabase(void);

		// Destructor
		~CStratonDatabase(void);

		// Connection
		BOOL Connect(void);
		BOOL SetCallback(HWND hWnd);
		void Disconnect(void);

		// Methods
		DWORD   GetKindOfObject(DWORD dwHandle);
		CString GetGhostName(DWORD dwHandle);

		// Project
		DWORD	CreateProject(PCTXT pPath);
		BOOL	DeleteProject(DWORD dwHandle);
		DWORD	OpenProject (PCTXT pPath);
		void	CloseProject(DWORD dwHandle);
		BOOL    SaveProject (DWORD dwHandle);
		CString GetProjectDateStamp(DWORD hProject);

		// Program
		UINT	GetPrograms(DWORD dwProject, DWORD dwSection, CLongArray &List);
		DWORD	FindProgram(DWORD dwProject, PCTXT pName);
		BOOL	CanCreateProgram(DWORD dwProject, DWORD dwLanguage, DWORD dwSection, DWORD dwParent, PCTXT pName);
		DWORD	CreateProgram(DWORD dwProject, DWORD dwLanguage, DWORD dwSection, DWORD dwParent, PCTXT pName);
		BOOL    DeleteProgram(DWORD dwProject, DWORD dwProgram);
		BOOL	RenameProgram(CError &Error, DWORD dwProject, DWORD dwProgram, PCTXT pName);
		CString GetProgramDesc(DWORD dwProject, DWORD dwProgram, DWORD &dwLanguage, DWORD &dwSection, DWORD &dwParent);
		BOOL	SaveProgramChanges(DWORD dwProject, DWORD dwProgram);
		BOOL	GetProgramSchedule(DWORD dwProject, DWORD dwProgram, DWORD &dwPeriod, DWORD &dwOffset);
		BOOL	SetProgramSchedule(DWORD dwProject, DWORD dwProgram, DWORD dwPeriod, DWORD dwOffset);
		BOOL	GetProgramOnCallFlag(DWORD dwProject, DWORD dwProgram);
		BOOL	SetProgramOnCallFlag(DWORD dwProject, DWORD dwProgram, DWORD dwOnCall);
		BOOL	MoveProgram(DWORD dwProject, DWORD dwProgram, DWORD dwMove, DWORD dwParent);

		// Group
		UINT	GetGroups(DWORD dwProject, CLongArray &List);
		DWORD   FindGroup(DWORD dwProject, PCTXT pName);
		CString GetGroupDesc(DWORD dwProject, DWORD dwGroup, DWORD &dwStyle);

		// Variable
		CString GetVariableDesc(DWORD dwProject, DWORD dwVariable, DWORD &dwType, DWORD &dwDim, DWORD &dwLen, DWORD &dwFlags);
		BOOL    SetVariableDesc(DWORD dwProject, DWORD dwVariable, DWORD dwType, DWORD dwDim, DWORD dwLen, DWORD dwFlags);
		BOOL    CanSetVariableDesc(DWORD dwProject, DWORD dwVariable, DWORD dwType, DWORD dwDim, DWORD dwLen, DWORD dwFlags);
		DWORD   GetVarGroup(DWORD dwProject, DWORD dwVariable);
		DWORD   FindVarInGroup(DWORD dwProject, DWORD dwGroup, PCTXT pSymbol);
		DWORD	FindVar(DWORD dwProject, PCTXT pSymbol, DWORD dwFrom, DWORD dwGroup, DWORD dwFlags);
		DWORD	FindVarExact(DWORD hProject, PCTXT pSymbol, DWORD dwFrom, DWORD dwGroup);
		DWORD   CreateVar(DWORD dwProject, DWORD dwGroup, DWORD dwType, UINT uDim, UINT uLen, DWORD dwFlags, PCTXT pName);
		BOOL    DeleteVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar) ;
		BOOL    CanRenameVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pName);
		BOOL    RenameVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pName);
		CString GetVarPrevName(DWORD dwProject, DWORD dwVar);
		BOOL	GetVarInitValue(DWORD dwProject, DWORD dwGroup, DWORD dwVar, CString &Init);
		BOOL	SetVarInitValue(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pInit);
		BOOL	CheckVarInitValue(DWORD dwProject, DWORD dwGroup, DWORD dwVar, PCTXT pInit);
		CString	GetSerBuffer(void);
		void	SetSerBuffer(PCTXT pBuffer);
		void	ReleaseSerBuffer(void);
		BOOL	SerializeVar(DWORD dwProject, DWORD dwGroup, DWORD dwVar);
		DWORD	PasteSerializedVar(DWORD dwProject, DWORD dwGroup);

		// Data Types
		UINT	GetTypes(DWORD dwProject, CLongArray &List);
		DWORD	FindType(DWORD dwProject, PCTXT pTypeName);
		CString GetTypeDesc(DWORD hProject, DWORD dwType, DWORD &dwFlags);
		DWORD	GetTypeUDFB(DWORD hProject, DWORD dwType);
		UINT	GetTypeParams(DWORD dwProject, DWORD dwType, CLongArray &List);
		UINT    GetUDFBNbIO(DWORD dwProject, DWORD dwType, CLongArray &Inputs, CLongArray &Outputs);

		// Properties
		BOOL	SetProperty(DWORD dwProject, DWORD dwHandle, DWORD dwProp, PCTXT pValue);
		CString GetProperty(DWORD dwProject, DWORD dwHandle, DWORD dwProp);

		// Services
		CFilename GetProjectPath(DWORD dwProject);
		CFilename GetGlobalFilePath(DWORD dwProject, PCTXT pSuffix);
		CFilename GetEqvPath(DWORD dwProject, BOOL fCommon, DWORD dwProgram);
		CFilename GetProgramPath(DWORD dwProject, DWORD dwProgram);

		// Events
		CString GetEventDesc(DWORD dwEvent);
		CString GetEventDesc(DWORD dwEvent, DWORD &dwFlags);

		// Comments
		CString GetComment(DWORD dwProject, DWORD dwHandle, DWORD dwType);

		// Visual dialog for variable selection
		BOOL SelectVar(DWORD dwProject, PCTXT Text, CVarSelCtx &Ctx, DWORD &dwIdent);

	protected:
		// Static Data
		static CStratonDatabase * m_pThis;
		static DWORD		  m_dwClient;

		// Internal
		UINT GetNbType(DWORD dwProject);
		UINT GetTypes(DWORD dwProject, DWORD *pTypes);
		UINT GetNbGroup(DWORD dwProject);
		UINT GetGroups(DWORD dwProject, DWORD *pGroups);
		UINT GetNbProgram(DWORD dwProject, DWORD dwSection);
		UINT GetPrograms(DWORD dwProject, DWORD dwSection, DWORD *pGroups);
		UINT GetNbTypeParam(DWORD dwProject, DWORD dwType);
		UINT GetTypeParams(DWORD dwProject, DWORD dwType, DWORD *pParams);
		UINT IntGetUDFBNbIO(DWORD dwProject, DWORD dwType, DWORD *pdwNbIn, DWORD *pdwNbOut);

		// Error Reporting
		BOOL SetError(CError &Error, DWORD dwCode);
		BOOL SetError(DWORD dwCode);

		// String Support
		CString String(LPCSTR pStr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Registry
//

class DLLAPI CStratonRegistry
{
	public:
		// Object Location
		static CStratonRegistry * FindInstance(void);

		// Constructor
		CStratonRegistry(void);

		// Attributes
		UINT	GetVersion(void);
		CString	GetLibFolderPath(void);
		CString	GetLibDefineFilePath(void);

		// Blocks
		UINT	GetBlocks(DWORD dwMask, CLongArray &List);
		CString GetBlockLibName(DWORD dwIndex);
		CString	GetBlockName(DWORD dwBlock);
		CString	GetBlockDesc(DWORD dwBlock, DWORD &dwKind, DWORD &dwLibNo, DWORD &dwNbInput, DWORD &dwNbOutput);
		CString GetBlockInput(DWORD dwBlock, DWORD dwPin, DWORD &dwType);
		CString GetBlockOutput(DWORD dwBlock, DWORD dwPin, DWORD &dwType);
		DWORD	FindBlock(CString Name);
		DWORD	GetPinNames(DWORD dwBlock, DWORD dwStyle, CString &In, CString &Out);
		DWORD	BlockNeedEN(DWORD dwBlock);
		DWORD	BlockNeedENO(DWORD dwBlock);
		CString GetBlockPinTypeName(DWORD dwType);
		CString GetBlockInputPinTypeName(DWORD dwBlock, DWORD dwPin);
		BOOL	BlockCanBeExtended(CString Name);
		CString GetBlockProperties(DWORD dwBlock);
		CString GetBlockPropSyntax(DWORD dwBlock);
		BOOL	IsBlockInputRef(DWORD dwBlock, DWORD dwPin);
		BOOL	IsBlockInputArray(DWORD dwBlock, DWORD dwPin);
		HICON	GetFBIcon(DWORD dwBlock);
		DWORD	GetFBConstraint(DWORD dwBlock, DWORD dwGroup);

		// IOs
		UINT	GetIOLibNames(CStringArray &List);
		DWORD	GetNbIOLib(void);
		CString	GetIOLibName(DWORD dwIndex);
		UINT	GetIOs(PCTXT pLib, CLongArray &List);
		DWORD	FindIO(PCTXT pName);
		CString GetIODesc(DWORD hIO, DWORD &dwNbGroup);
		CString GetIOGroupDesc(DWORD hIO, DWORD dwGroup, CString &Name, CString &Prefix);
		DWORD	GetIOGroupNbChannel(DWORD hIO, DWORD hGroup, DWORD &dwMin, DWORD &dwMax);
		CString	GetIOChannelName(DWORD hIO, DWORD hGroup, DWORD hChannel);
		CString	GetIOProperty(DWORD hIO, DWORD hGroup, DWORD hProp);

		// Profiles
		UINT	GetProfiles(PCTXT pLib, CLongArray &List);
		DWORD	FindProfile(PCTXT pName);
		CString GetProfileName(DWORD hProfile);
		CString GetProfileText(DWORD hProfile);
		CString GetProfileLibName(DWORD hProfile);

		// Types
		UINT	GetTypes(PCTXT pLib, CLongArray &List);
		DWORD	FindType(PCTXT pName);
		CString GetTypeName(DWORD hType);
		CString GetTypeText(DWORD hType);
		CString GetTypeLibName(DWORD hType);

	protected:
		// Static Data
		static CStratonRegistry * m_pThis;

		// Data Members
		PCTXT	m_pName;
		HMODULE	m_hLib;

		// Internal
		UINT	GetNbBlock(DWORD dwMask);
		UINT	GetBlocks(DWORD dwMask, PDWORD pList);
		UINT	GetNbIO(PCTXT pLib);
		UINT	GetIOs (PCTXT pLib, PDWORD pList);
		UINT	GetNbProfile(PCTXT pLib);
		UINT	GetProfiles (PCTXT pLib, PDWORD pList);
		UINT	GetNbType(PCTXT pLib);
		UINT	GetTypes (PCTXT pLib, PDWORD pList);

		// String Support
		CString String(LPCSTR pStr);

		// Library Management
		void LoadLib(PCTXT pName);
		void FreeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Middleware
//

class DLLAPI CMiddleware
{
	public:
		// Object Location
		static CMiddleware * FindInstance(void);

		// Constructor
		CMiddleware(void);

		// Destructor
		~CMiddleware(void);

		// Compatibility
		INT GetVersion(void);

		// Data Server Connection
		BOOL Connect(HWND hCallback, DWORD dwFlags);
		void Disconnect(void);
		BOOL SetCallback(HWND hWnd);

		// Project Connection
		DWORD	FindProject (PCTXT pPath);
		DWORD	OpenProject (PCTXT pPath, PCTXT pDriver, PCTXT pSetup, DWORD dwTargetID, DWORD dwProtocol);
		void	CloseProject(DWORD dwProject);
		BOOL	IsCommOk(DWORD dwProject);
		BOOL	IsSimulMode(DWORD dwProject);
		CString GetStatus(DWORD dwProject, DWORD &dwCon, DWORD &dwApp);

		void	SetHexMode(DWORD dwProject, BOOL fEnable);
		BOOL	GetHexMode(DWORD dwProject);

		// Spying Variables
		BOOL Subscribe(DWORD dwProject, DWORD dwLoopID, PCTXT pSymbol, PCTXT pParent);
		BOOL Unsubscribe(DWORD dwProject, DWORD dwLoopID);

		// Read Variables
		CString GetStringValue(DWORD dwProject, PCTXT pSymbol, PCTXT pParent);
		CString GetIDStringValue(DWORD dwProject, DWORD dwLoopID);
		BOOL    GetBinValue(DWORD dwProject, PCTXT pSymbol, PCTXT pParent, BYTE &bValue, DWORD &dwValid, DWORD &dwLocked);
		BOOL    GetIDBinValue(DWORD dwProject, DWORD dwLoopID, BYTE &bValue, DWORD &dwValid, DWORD &dwLocked);

		// Commands
		BOOL Control(DWORD dwProject, HWND hWnd, CPoint Pos, PCTXT pSymbol, PCTXT pParent);
		BOOL Execute(DWORD dwProject, PCTXT pCommand);

		// Miscellaneous
		CString GetMessage(void);
		BOOL	SubscribeToLog(void);

	protected:
		// Static Data
		static CMiddleware * m_pThis;
		static DWORD	     m_dwClient;

		// Data
		PCTXT				m_pName;
		HMODULE				m_hLib;

		// Error Reporting
		BOOL SetError(CError &Error, DWORD dwCode);
		BOOL SetError(DWORD dwCode);

		// String Support
		CString String(LPCSTR pStr);

		// Library Management
		void LoadLib(PCTXT pName);
		void FreeLib(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Compiler APIs
//

BOOL	DLLAPI	Straton_NeedBuild(PCTXT pPath);
void	DLLAPI	Straton_CleanProject(PCTXT pPath);
BOOL	DLLAPI	Straton_BuildDefault(PCTXT pPath, HWND hWnd);
BOOL	DLLAPI	Straton_BuildDefaultFileReport(PCTXT pPath, PCTXT pFile);
BOOL	DLLAPI	Straton_CheckProgram(PCTXT pPath, PCTXT pName, HWND hWnd);
BOOL	DLLAPI	Straton_CheckProgram(PCTXT pPath, PCTXT pName, HWND hWnd, FILE *pFile);
BOOL	DLLAPI	Straton_BuildOneUDFB(PCTXT pPath, PCTXT pName, HWND hWnd, FILE *pFile);
BOOL	DLLAPI	Straton_CheckDefFile(PCTXT pPath);
BOOL	DLLAPI	Straton_ConversionNeedBuild(PCTXT pPath, PCTXT pName, DWORD dwLanguage);
BOOL	DLLAPI	Straton_CanConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage);
BOOL	DLLAPI	Straton_ConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage);
BOOL	DLLAPI	Straton_SLCheck(UINT &uNbIO, CString &Oem);
BOOL	DLLAPI	Straton_BuildProject(PCTXT pPath, PCTXT pTarg, PCTXT pCode, BOOL fIntelEndian, HWND hWnd, BOOL fTraceFile);
CString DLLAPI	Straton_ExportVariables(PCTXT pPath, PCTXT pGroup, DWORD dwSyntax, DWORD dwFlags);
CString DLLAPI	Straton_ImportVariables(PCTXT pPath, PCTXT pGroup, PCTXT pInput, PCTXT pError, DWORD dwSyntax, DWORD dwFlags);

//////////////////////////////////////////////////////////////////////////
//
// Straton XRef APIs
//

INT	DLLAPI	Straton_XRefGetVersion(void);
UINT	DLLAPI	Straton_XRefFindInFiles(PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile);
UINT	DLLAPI	Straton_XRefFindUnusedVars(PCTXT pPath, HWND hWnd, FILE *pFile);
UINT	DLLAPI	Straton_XRefFindMultAssign(PCTXT pPath, HWND hWnd, FILE *pFile);
UINT	DLLAPI	Straton_XRefFindInProg(PCTXT pPath, PCTXT pName, PCTXT pFind, DWORD dwLanguage, HWND hWnd, FILE *pFile);
UINT	DLLAPI	Straton_XRefReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile);
UINT	DLLAPI	Straton_XRefAutoReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, PCTXT pReplace, HWND hWnd, FILE *pFile);

//////////////////////////////////////////////////////////////////////////
//
// Straton W5EditRes APIs
//

CString	DLLAPI	Straton_SelBlockEx(HWND hWnd, PCTXT pPath, PCTXT pName, int &nIn, int &nOut, DWORD &dwIdent, BOOL &fInst, BOOL &fDB);

//////////////////////////////////////////////////////////////////////////
//
// Straton Object
//

class DLLAPI CStratonObject : public CItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStratonObject(void);

		// Management
		void SetProject(DWORD dwProject);

		// Attribute
		DWORD GetHandle(void);
		DWORD GetProject(void);

	protected:
		// Data
		DWORD m_dwProject;
		DWORD m_dwHandle;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Project
//

class DLLAPI CStratonProject : public CStratonObject
{
	public:
		// Constructor
		CStratonProject(void);

		// Destructor
		~CStratonProject(void);

		// Attributes
		CString   GetDate(void);
		CFilename GetProjectPath(void);
		CFilename GetGlobalFilePath(PCTXT pSuffix);
		CFilename GetNextPath(BOOL fCreate);

		// Operations
		BOOL Make(void);
		BOOL Open(CString Path);
		void Save(void);

	protected:
		// Static Data
		static UINT m_uProj;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Type Descriptor
//

class DLLAPI CStratonProjectDescriptor
{
	public:
		// Constructor
		CStratonProjectDescriptor(DWORD dwProject);

		// Operations
		void Save(void);

	protected:
		// Data
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable
//

class DLLAPI CStratonProgram : public CStratonObject
{
	public:
		// Constructor
		CStratonProgram(void);

		// Attributes
		CFilename GetProgramName(UINT uType) const;
		CFilename GetProgramPath(UINT uType) const;

		// Operations
		BOOL Create(CString Name, DWORD dwLanguage, DWORD dwSection, DWORD dwParent);
		BOOL Delete(void);
		BOOL Rename(CError &Error, CString Name);
		BOOL CanRename(CString Name);
		void Save(void);		
		BOOL Connect(CString Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable
//

class DLLAPI CStratonVariable : public CStratonObject
{
	public:
		// Constructor
		CStratonVariable(void);

		// Attributes
		DWORD GetGroup(void) const;

		// Operations
		BOOL Create(CString Name, DWORD dwGroup, DWORD dwType, DWORD dwExtent, DWORD dwFlags);
		BOOL Delete(void);
		BOOL Rename(CString Name);
		BOOL CanRename(CString Name);
		void SetHandle(DWORD dwHandle);
		BOOL Connect(DWORD dwGroup, CString Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Type
//

class DLLAPI CStratonDataType : public CStratonObject
{
	public:
		// Constructor
		CStratonDataType(void);

		// Flags
		enum {
			typeInvalid	= 0x1000,
			typeBasic	= 0x0001,
			typeString	= 0x0002,
			typeUdfb	= 0x0040,
			};
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Group
//

class DLLAPI CStratonGroup : public CStratonObject
{
	public:
		// Constructor
		CStratonGroup(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Group Helper
//

class DLLAPI CStratonGroupHelper
{
	public:
		// Constructor
		CStratonGroupHelper(DWORD dwProject);

		// Operations
		UINT GetHandles(CLongArray &List);

	protected:
		// Data Members
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Type Helper
//

class DLLAPI CStratonDataTypeHelper
{
	public:
		// Constructor
		CStratonDataTypeHelper(DWORD dwProject);

		// Configuration
		void SkipUdFb(void);
		void SkipStdFb(void);

		// Operations
		UINT GetHandles(CLongArray &List);

	protected:
		// Data Members
		DWORD	m_dwProject;
		BOOL	m_SkipUdFb;
		BOOL	m_SkipStdFb;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Type Descriptor
//

class DLLAPI CStratonDataTypeDescriptor
{
	public:
		// Constructor
		CStratonDataTypeDescriptor(DWORD dwProject, DWORD dwHandle);

		// Attributes
		BOOL IsBasic(void) const;
		BOOL IsUdFb(void) const;
		BOOL IsStdFb(void) const;
		BOOL IsFb(void) const;
		BOOL IsString(void) const;
		BOOL IsNumeric(void) const;
		BOOL IsReal(void) const;
		BOOL IsBool(void) const;
		BOOL IsTime(void) const;
		BOOL Is64Bit(void) const;
		BOOL IsSigned(void) const;
		BOOL GetRange(INT64 &nMin, INT64 &nMax);

		// Public Data
		CString m_Name;
		DWORD	m_dwHandle;
		DWORD	m_dwFlags;

	protected:
		// Data
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Descriptor
//

class DLLAPI CStratonVariableDescriptor
{
	public:
		// Constructor
		CStratonVariableDescriptor(DWORD dwProject, DWORD dwHandle);

		// Attributes
		BOOL CanSet(CError &Error);

		// Operations
		BOOL Set(void);
		void Get(void);
		
		// Attributes
		CString GetGroupName(void);

		// Initial Data
		BOOL CheckInitValue(CError &Error, CString Text);
		BOOL SetInitValue(CError &Error, CString Text);

		// Public Data
		CString m_Name;
		DWORD	m_dwGroup;
		DWORD	m_dwHandle;
		DWORD	m_dwType;
		DWORD   m_dwDim;
		DWORD   m_dwLen;
		DWORD	m_dwFlags;
		CString m_Prev;
		CString m_Init;

	protected:
		// Data
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Group Descriptor
//

class DLLAPI CStratonGroupDescriptor
{
	public:
		// Constructor
		CStratonGroupDescriptor(DWORD dwProject, DWORD dwHandle);

		// Attributes
		CString GetStyleName(void);
		BOOL	IsRetentive(void) const;
		BOOL	IsGlobal(void) const;

		// Attributes
		CString m_Name;
		DWORD	m_dwHandle;
		DWORD	m_dwStyle;

	protected:
		// Data
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Program Descriptor
//

class DLLAPI CStratonProgramDescriptor
{
	public:
		// Constructor
		CStratonProgramDescriptor(DWORD dwProject, DWORD dwHandle);

		// Operations
		void Set(void);
		void Get(void);

		// Attributes
		BOOL  IsException(void);
		BOOL  IsEnabled(void);
		BOOL  IsCalled(void);
		BOOL  IsUdFb(void);
		WCHAR GetSectName(void);

		// Public Data
		DWORD	m_dwProject;
		DWORD	m_dwHandle;
		CString	m_Name;
		CString	m_Prev;
		DWORD	m_dwLanguage;
		DWORD	m_dwSection;
		DWORD	m_dwParent;
	};

//////////////////////////////////////////////////////////////////////////
//
// Program Schedule
//

class DLLAPI CStratonProgramSchedule
{
	public:
		// Constructor
		CStratonProgramSchedule(DWORD dwProject, DWORD dwHandle);

		// Operations
		BOOL Set(void);
		void Get(void);

		// Public Data
		DWORD	m_dwHandle;
		DWORD	m_dwPeriod;
		DWORD	m_dwOffset;		

	protected:
		// Data
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Serializer
//

class DLLAPI CStratonVariableSerializeBuffer
{
	public:
		// Constructor
		CStratonVariableSerializeBuffer(DWORD dwProject, DWORD dwGroup, DWORD dwIdent);

		// Destructor
		~CStratonVariableSerializeBuffer(void);

		// Operations
		BOOL  Serialize(void);
		DWORD Paste(DWORD dwGroup);
		BOOL  DeleteVar(void);

	protected:
		// Data
		DWORD	m_dwProject;
		DWORD	m_dwGroup;
		DWORD	m_dwIdent;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Descriptor
//

class DLLAPI CStratonProperties
{
	public:
		// Constructor
		CStratonProperties(DWORD dwProject, DWORD dwHandle);
		CStratonProperties(DWORD dwProject);

		// Operations
		BOOL	Set(DWORD dwProp, CString Value);
		CString Get(DWORD dwProp);

	protected:
		// Data
		DWORD	m_dwProject;
		DWORD	m_dwHandle;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CObjectNamer;
class CProgramNamer;
class CVariableNamer;

//////////////////////////////////////////////////////////////////////////
//
// Object Naming Helper
//

class DLLAPI CObjectNamer
{
	public:
		// Contructor
		CObjectNamer(DWORD dwProject);

		// Operation
		void MakeUnique(CString &Name);

		// Overidable
		virtual BOOL CanFindName(CError &Error, CString Name);
		virtual BOOL Validate(CError &Error, CString Name);

	protected:
		// Data
		CString	m_Disambig;
		DWORD	m_dwProject;
	};

//////////////////////////////////////////////////////////////////////////
//
// Program Naming Helper
//

class DLLAPI CProgramNamer : public CObjectNamer
{
	public:
		// Contructor
		CProgramNamer(DWORD dwProject, DWORD dwLanguage, DWORD dwSection);

		// Overidable
		BOOL CanFindName(CError &Error, CString Name);
		BOOL Validate(CError &Error, CString Name);

	protected:
		// Data
		DWORD m_dwLanguage;
		DWORD m_dwSection;
		DWORD m_dwParent;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Naming Helper
//

class DLLAPI CVariableNamer : public CObjectNamer
{
	public:
		// Contructor
		CVariableNamer(DWORD dwProject);
		CVariableNamer(DWORD dwProject, DWORD dwGroup);

		// Overidable
		BOOL CanFindName(CError &Error, CString Name);
		BOOL Validate(CError &Error, CString Name);

	protected:
		// Data
		DWORD	m_dwGroup;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStratonWnd;
class CStratonLDWnd;
class CStratonSTWnd;
class CCommandBuilder;
class CStratonLibrary;

//////////////////////////////////////////////////////////////////////////
//
// Find Replace
//

struct CFindReplace
{	
	CString	m_Search;
	CString	m_Replace;
	DWORD	m_dwCommand;
	DWORD	m_dwFlags;
	HWND	m_hListBox;
	FILE  * m_pFile;
	DWORD	m_dwData;

	};

//////////////////////////////////////////////////////////////////////////
//
// Function Block
//

struct CFunctionBlock
{
	CString	m_Name;
	DWORD	m_dwID;
	int	m_nInputs;
	int	m_nOutputs;
	BOOL	m_fInstantiable;
	BOOL	m_fDBObject;
	DWORD	m_dwData;

	};

//////////////////////////////////////////////////////////////////////////
//
// Variable
//

struct CVariable
{
	CString	m_Name;
	DWORD	m_dwID;
	BOOL	m_fLocal;
	CString	m_Short;
	CString	m_Long;
	CString	m_IOName;
	DWORD	m_dwData;

	};

//////////////////////////////////////////////////////////////////////////
//
// Tool Tip Info
//

struct CToolTipInfo
{
	BOOL m_fName;
	BOOL m_fType;
	BOOL m_fDim;
	BOOL m_fAttrib;
	BOOL m_fProps;
	BOOL m_fSyb;
	BOOL m_fInitValue;
	BOOL m_fTag;
	BOOL m_fDesc;

	};

//////////////////////////////////////////////////////////////////////////
//
// SFC Settings
//

struct CSfcSettings
{
	CString	m_Data;

	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

class DLLAPI CStratonWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Operations
		BOOL Create(CWnd &Parent);

		// 4.1 - Common
		void	SetEnable(BOOL fEnable);
		BOOL	GetEnable(void);
		void	SetReadOnly(BOOL fReadOnly);
		BOOL	GetReadOnly(void);
		void	SetModified(BOOL fModify);
		BOOL	IsModified(void);
		void	SetDebug(BOOL fDebug);
		BOOL	IsDebug(void);//
		BOOL	IsEmpty(void);

		// 4.2 - Database Connection 
		BOOL	SetProjectPath(PCTXT pPath);
		CString GetProjectPath(void);
		DWORD	GetProjectId(void);//
		BOOL	RemoveProject(DWORD dwProj);//
		CString GetProgramName(void);
		void	SetID(DWORD dwID);

		// 4.3 - Editing
		BOOL	CanCut(void);
		void	Cut(void);
		BOOL	CanClear(void);
		void	Clear(void);
		BOOL	CanCopy(void);
		void	Copy(void);
		BOOL	CanPaste(void);
		void	Paste(void);
		BOOL	CanUndo(void);
		void	Undo(void);
		BOOL	CanRedo(void);
		void	Redo(void);
		void	EmptyUndoStack(void);
		BOOL	CanSwapItemStyle(void);
		void	SwapItemStyle(void);

		// 4.4 - Display
		BOOL	CanSetZoom(void);
		void	SetZoom(int nRatio);
		void	SetZoom(int nDir, int nRatio);
		int	GetZoom(void);
		BOOL	CanSetGrid(void);
		void	SetGrid(BOOL fGrid);
		BOOL	IsGridVisible(void);
		BOOL	IsBookmark(void);//
		void	SetBookmark(BOOL fSet);//
		void	GoNextBookmark(void);//
		void	GoPrevBookmark(void);//
		void	DeleteAllBookmark(void);//

		// 4.5 - Selection
		BOOL	HasSelection(void);
		BOOL	CanSelectAll(void);
		void	SelectAll(void);
		void	DeselectAll(void);
		void	EnsureSelVisible(void);
		void	SelectItem(DWORD dwData, BOOL fDeselectAll);
		void	SelectItems(CLongArray Data, BOOL fDeselectAll);

		// 4.6 - File Exchange
		BOOL	CanSave(void);
		void	Save(PCTXT pPath);
		void	Load(PCTXT pPath);
		BOOL	CanInsertFile(void);
		void	InsertFile(PCTXT pPath);

		// 4.7 - Text Exchange
		void	SetText(PCTXT pText);
		CString GetText(void);
		UINT	GetSelTextLength(void);
		CString GetSelText(void);
		BOOL	CanInsertText(void);
		void	InsertText(CPoint Pos, PCTXT pText);

		// 4.8 - Location Find/Replace
		void	LocateError(PCTXT pError);
		void    Goto(PCTXT pGoto);
		DWORD	FindReplace(CFindReplace &Find);
		DWORD	SearchIn(CFindReplace &Find);//
		CPoint	GetCurPos(void);
		CSize	GetSelSize(void);
		CPoint	GetSelPos(void);
		CPoint	GetSymbolPos(void);
		CPoint	GetCaret(void);
		void	GotoXy(CPoint Pos);
		void	SaveSel(void);
		void	RestoreSel(void);
		CString FindVarInput(PCTXT pInput);//

		// 4.9 - Document Properties
		BOOL	CanFormatProgram(void);
		void	FormatProgram(void);
		BOOL	CanViewInfo(void);
		void	ViewInfo(void);
		CSize	GetCellSize(void);//
		void	SetCellSize(CSize);//
		CSize	GetSize(void);//

		// 4.10 - Paint
		int	Paint(HDC hDC, CRect Rect);
		void	ReloadBitmap(void);

		// 4.11 - Function Blocks/Symbols
		void	SetCurrentFB(CFunctionBlock &FB);
		BOOL	CanInsertFB(void);
		void	InsertFB(CFunctionBlock &FB);
		void	SetFB(CFunctionBlock &FB);
		void	GetFB(CFunctionBlock &FB);
		void	GetVar(CVariable &Var);
		BOOL	CanInsertSymbol(void);
		void	InsertSymbol(PCTXT pSymbol);
		CString GetSymbolName(void);
		CString GetTypeName(void);
		DWORD	GetItemType(void);
		CString GetItemTypeSel(void);
		WCHAR	GetFirstChar(void);
		CString	GetUsedBlock(void);
		UINT	GetDbId(CLongArray &List);//
		CString GetInputBlockVar(int nPin);//
		BOOL	CanEditPins(void);//
		void	EditPins(void);//

		// 4.12 - Settings
		BOOL EnableDragDrop(BOOL fEnable);
		void UndoRedoSize(int nSize);
		void KeepFbdSelect(BOOL fEnable);
		void CopyBitmap(BOOL fEnable);
		void PromptVarName(BOOL fEnable);
		void PromptInstance(BOOL fEnable);
		BOOL EnablePulse(BOOL fEnable);
		void SetVertVarSize(int nSize);
		void SetHorzVarSize(int nSize);
		void SetPageWidth(int nSize);
		void SetContents(int nType, PCTXT pContents);
		BOOL AutoDeclareInst(BOOL fEnable);
		BOOL AutoDeclareSymbol(BOOL fEnable);
		void HideOptionDlgVar(BOOL fHide);
		void EnableCopy(BOOL fEnable);
		void HideScroll(BOOL fEnable);
		void SetToolTipInfos(BOOL fEdit, CToolTipInfo const &Info);
		void EditPropAfterInsertVar(BOOL fEnable);
		void SetChildOffset(int nOffset);
		void SetAutoEdit(UINT uMask, BOOL fSet);
		void SetAutoEdit(DWORD dwMask);
		BOOL HideIOName(BOOL fHide);//
		BOOL UnderlineGlobal(BOOL fEnable);//
		BOOL SetInfos(DWORD dwInfo, PVOID pInfo);//
		void GetInfos(DWORD dwInfo, PVOID pInfo);//
		BOOL SetBW(BOOL fEnable);//
		void InsertVarWhenInsertFB(BOOL fEnable);
		BOOL DisplayIO(BOOL fEnable);
		void Activate(BOOL fEnable);
		void SetAutoConnectVariable(BOOL fEnable);
		void ShowStLine(BOOL fShow);

		// 4.26 - Tree Display
		BOOL CanHexDisplay(void);
		BOOL IsHexDisplay(void);
		void SwapHexDisplay(void);

		// 4.28 - Help
		CString Help(PCTXT pFile);
		CString HelpOn(PCTXT pFile);
		CString GetInterfaces(WORD wStyle);

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Command Execution
		LRESULT SendCommand(PCTXT pFormat,...);
		LRESULT SendCommand(CCommandBuilder &Cmd);

		// Command Help
		CString String(LPCSTR pStr);

	protected:
		// Data
		CStratonLibrary * m_pLib;
		CString		  m_Name;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);

	protected:
		// Constructor
		CStratonWnd(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

class DLLAPI CStratonFBDWnd : public CStratonWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStratonFBDWnd(void);

		// 4.18 - FBD Control
		BOOL	IsCheck(UINT uID);
		BOOL	CanCheck(UINT uID);
		void	Check(UINT uID);
		BOOL	CanDisplayFbdOrder(void);
		DWORD	DisplayFbdOrder(void);
		BOOL	CanCreatUdFb(void);
		BOOL	CreatUdFb(void);
		BOOL	CanConnect(void);
		BOOL	Connect(void);
		BOOL	CanRotateCorners(void);
		BOOL	RotateCorners(int nSym);
		BOOL    SnapFbdGrid(BOOL fEnable);
		void	DrawFbdBridge(BOOL fEnable);
		BOOL	CanShowSpy(void);//
		BOOL	IsSpyVisible(void);//
		BOOL	ShowSpy(void);//
		void	SetRules(PCTXT pRules);
		CString	GetRules(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

class DLLAPI CStratonLDWnd : public CStratonWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStratonLDWnd(void);

		// 4.20 - LD
		BOOL CanAlignCoils(void);
		void AlignCoils(void);
		BOOL CanInsertContactBefore(void);
		void InsertContactBefore(void);
		BOOL CanInsertContactAfter(void);
		void InsertContactAfter(void);
		BOOL CanInsertContactParallel(void);
		void InsertContactParallel(void);
		BOOL CanInsertCoil(void);
		void InsertCoil(void);
		BOOL CanInsertFBBefore(void);
		void InsertFBBefore(void);
		BOOL CanInsertFBAfter(void);
		void InsertFBAfter(void);
		BOOL CanInsertFBParallel(void);
		void InsertFBParallel(void);
		BOOL CanInsertJump(void);
		void InsertJump(void);
		BOOL CanInsertRung(void);
		void InsertRung(void);
		BOOL CanInsertComment(void);
		void InsertComment(void);
		BOOL CanInsertHorz(void);
		void InsertHorz(void);
		void WrapRungs(BOOL fWrap);

	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

class DLLAPI CStratonSFCWnd : public CStratonWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// 4.21
		BOOL	CanInsert(void);
		void	InsertStep(void);
		void	InsertTrans(void);
		void	InsertJump(void);
		void	InsertMainDiv(void);
		void	InsertDiv(void);
		void	InsertCnv(void);
		void	InsertMacro(void);
		void	InsertMacroBody(void);
		void	InsertInitStep(void);
		BOOL	CanSwapStyle(void);
		void	SwapStyle(void);
		CString	GetTransCode(int nRef);
		CString	GetTransNote(int nRef);
		void	SetTransNote(int nRef, CString Code);
		void	SetTransCode(int nRef, CString Code);
		CString	GetStepCodeDef(int nRef);
		void	SetStepCodeDef(int nRef, CString Code);
		CString	GetStepCodeP1(int nRef);
		void	SetStepCodeP1(int nRef, CString Code);
		CString	GetStepCodeN (int nRef);
		void	SetStepCodeN (int nRef, CString Code);
		CString	GetStepCodeP0(int nRef);
		void	SetStepCodeP0(int nRef, CString Code);
		CString	GetStepNote (int nRef);
		void	SetStepNote (int nRef, CString Code);
		CString	GetMacroNote(int nRef);
		void	SetMacroNote(int nRef, CString Code);
		BOOL	IsSelStep(void);
		BOOL	IsSelTrans(void);
		DWORD	IsSelMacro(void);
		DWORD	GetSelRefNum(void);
		void	LockStep(BOOL fLock, int nRef);
		void	LockTrans(BOOL fLock, int nRef);
		void	LockMacro(BOOL fLock, int nRef);
		BOOL	IsStepLock(int nRef);
		BOOL	IsTransLock(int nRef);
		BOOL	CanEnterSelRef(void);
		void	EnterSelRef(void);
		BOOL	CanEditSelCode(void);
		UINT	GetStepLanguageP1(int nRef);
		UINT	GetStepLanguageP0(int nRef);
		UINT	GetStepLanguageN(int nRef);
		UINT	GetTransLanguage(int nRef);
		void	SetNoteDisplay(BOOL fEnable);
		BOOL	GetNoteDisplay(void);
		void	SetDisplay(UINT uMode);
		UINT	GetDisplay(void);
		BOOL	CanRenumber(void);
		void	Renumber(void);
		DWORD	NextPosItem(BOOL fLoop, int nRef);
		BOOL	IsStep(CPoint Pos);
		BOOL	IsTrans(CPoint Pos);
		BOOL	IsComment(CPoint Pos);
		BOOL	IsSelComment(void);
		void	LockComment(BOOL fLock, int nRef);
		CString GetCommentNote(int nRef);
		void	SetCommentNote(int nRef, CString Code);
		DWORD	GetItemGuid(CPoint Pos);
		void	SetTimer(void);
		BOOL	SetSFCSettings(DWORD dwFlags);
		BOOL	SetSFCSettings(CSfcSettings const &Settings);
		void	InsertComment(void);

		// Constructor
		CStratonSFCWnd(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

class DLLAPI CStratonSTWnd : public CStratonWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStratonSTWnd(void);

		// 4.17 - ST Control
		BOOL CanSetTab(void);
		void SetTab(int nTab);
		void SetSyntaxColoring(CString Syntax);
		void EnableSyntax(BOOL fEnable);
		void SetValueInText(BOOL fEnable);
		BOOL GetValueInText(void);
		void AutoComplete(void);
		BOOL CanIndent(void);
		BOOL Indent(void);
		BOOL CanInsertComment(void);
		BOOL InsertComment(void);
		BOOL CanRemoveComment(void);
		BOOL RemoveComment(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

class DLLAPI CStratonILWnd : public CStratonWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStratonILWnd(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStratonWnd;

//////////////////////////////////////////////////////////////////////////
//
// Variable Select Dialog
//

class DLLAPI CVariableSelectDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CVariableSelectDialog(DWORD dwProject);

		// Modes
		enum {
			modeGeneral,
			modeVariable,
			modeCreate
			};

	protected:

		// Data Members
		DWORD	m_dwProject;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPreDestroy(void);

		// Notification Handlers
		void OnModeChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnCommandYes(UINT uID);
		BOOL OnCommandNo(UINT uID);

		// Implementation
		void LoadMode(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Create Dialog
//

class DLLAPI CVariableCreateDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CVariableCreateDialog(DWORD dwProject);
		CVariableCreateDialog(DWORD dwProject, DWORD dwParent);
		CVariableCreateDialog(DWORD dwProject, DWORD dwParent, CString Name);
		CVariableCreateDialog(DWORD dwProject, DWORD dwParent, DWORD dwAttr);

		// Attributes
		static CString	GetName (void);
		static DWORD	GetType (void);
		static DWORD	GetAttr (void);
		static DWORD	GetGroup(void);
		
	protected:
		// Static Data
		static CString	m_Name;
		static DWORD	m_Type;
		static DWORD	m_Attr;
		static DWORD	m_Group;

		// Data Members
		DWORD	m_dwProject;
		PCTXT	m_pFormat;
				 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPreDestroy(void);

		// Command Handlers
		BOOL OnCommandYes(UINT uID);
		BOOL OnCommandNo(UINT uID);

		// Implementation
		void LoadCaption(void);
		void LoadName(void);
		void LoadType(void);
		void LoadAttr(void);
		void LoadGroup(void);
		BOOL ReadName(void);
		BOOL ReadType(void);
		BOOL ReadAttr(void);
		BOOL ReadGroup(void);
		void FindFormat(void);
		BOOL IsParam(void);
		BOOL IsGlobal(void);

		void LoadConfig(void);
		void SaveConfig(void);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Program Create Dialog
//

class DLLAPI CProgramCreateDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CProgramCreateDialog(DWORD dwProject);
		CProgramCreateDialog(DWORD dwProject, DWORD dwParent);
		CProgramCreateDialog(DWORD dwProject, DWORD dwParent, DWORD dwCall);

		// Attributes
		static CString GetName(void);
		static DWORD   GetLang(void);
		static DWORD   GetSect(void);
		static DWORD   GetCall(void);
		
		// Section
		enum {
			sectBegin    =   1,
			sectSfcMain  =   2,
			sectEnd      =   4,
			sectSfcChild =   8,
			sectUdfb     =  16,
			};
		
		// Language
		enum {
			langSFC =  1,
			langST	=  2,
			langFBD	=  4,
			langLD	=  8,
			langIL	= 16,
			};
		
		// Call
		enum {
			callNever,
			callMainCycle,
			callSubProgram,
			callUdfb,
			callSfcChild,
			};

	protected:
		// Static Data
		static CString	m_Name;
		static DWORD	m_Lang;
		static DWORD	m_Sect;
		static DWORD	m_Call;

		// Data Members
		DWORD	m_dwProject;
		DWORD	m_dwParent;
				 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPreDestroy(void);

		// Notification Handlers
		void OnEditChange(UINT uID, CWnd &Ctrl);
		void OnLangChange(UINT uID, CWnd &Ctrl);
		void OnCallChange(UINT uID, CWnd &Ctrl);

		// Command Handlers
		BOOL OnCommandYes(UINT uID);
		BOOL OnCommandNo(UINT uID);

		// Implementation
		void LoadCaption(void);
		void LoadName(void);
		void LoadLang(void);
		void LoadSect(void);
		BOOL ReadName(void);
		BOOL ReadLang(void);
		BOOL ReadSect(void);

		void LoadConfig(void);
		void SaveConfig(void);
	};	

///////////////////////////////////////////////////////////////////////////
//
// Editor Window
//

class DLLAPI CEditorWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CEditorWnd(void);

		// Editor Access
		CStratonWnd & GetEditor(void);

		// Attributes
		BOOL IsDirty(void) const;

		// Operations
		void Attach(CItem *pItem);
		void Attach(DWORD dwProject, DWORD dwProgram);
		void SetDirty(void);
		void Commit(void);
		void Reload(void);

		// Edit Commands
		class CCmdEdit;

	protected:
		// Data
		CStratonWnd	* m_pEdit;
		BOOL		  m_fDirty;
		UINT		  m_cfVariable;
		DWORD		  m_dwProject;
		DWORD		  m_dwProgram;
		CString		  m_Prev;
		CString		  m_MenuCntx;
		CString		  m_MenuMain;
		CString		  m_ToolMain;

		// Configuration
		BOOL	m_fInsertVarWhenInsertFB;
		BOOL	m_fUnderlineGlobal;
		BOOL	m_fKeepFbdSelect;
		BOOL	m_fCreateGlobal;
		BOOL	m_fApplyOptions;
		DWORD	m_dwTypeHint;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Prev);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnPrint(CDC &DC, UINT uFlags);
		
		// Edit Command Handlers
		BOOL OnEditGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL CanEditProperties(void);
		BOOL OnEditProperties(void);

		// View Command Handlers
		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Tool Command Handlers
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);

		// Notification Handlers
		void OnModified(UINT uID, UINT uData);
		void OnRClick(UINT uID, UINT uCode);
		void OnSetVar(UINT uID, UINT uCode);
		void OnSetFb(UINT uID, UINT uCode);
		void OnGetFocus(UINT uID, CWnd &Wnd);
		void OnEditProps(UINT uID, UINT uCode);

		// Implementation
		void ShowDefaultStatus(void);
		void LoadConfig(void);
		void SaveConfig(void);
		void LoadSettings(void);
		void SelectVariable(CString Name);
		void SelectVariable(void);
		
		BOOL IsDown(UINT uCode);
		
		BOOL IsComment(DWORD dwType);
		BOOL IsFunction(DWORD dwType);
		BOOL IsVariable(DWORD dwType);
		BOOL IsCoil(DWORD dwType);
		BOOL IsContact(DWORD dwType);
		BOOL IsInOut(DWORD dwType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Project View -- Move Command
//

class DLLAPI CEditorWnd::CCmdEdit : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdEdit(CString Item, CString Prev, CString Text);

		// Data Members
		CStringDelta m_Delta;
	};

//////////////////////////////////////////////////////////////////////////
//
// Off-line Simulator
//

class DLLAPI COfflineSimulator : public CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		COfflineSimulator(void);

		// Attributes
		BOOL IsRunning(void);
		BOOL CanPauseResume(void);
		BOOL CanSingleStep(void);
		BOOL FindProject(PCTXT pPath);
		void SetHexMode(BOOL fEnable);
		BOOL GetHexMode(void);

		// Operations
		BOOL Open(CWnd &Wnd, CDatabase *pDbase, PCTXT pPath, BOOL fOnline);
		void Close(void);

		// Commands
		void SetCycleTime(UINT uScan);
		void PauseResume(void);
		void SingleStep(void);

		// Event
		void OnEvent(UINT uCode, DWORD dwIdent);

	protected:
		// Data
		DWORD m_dwProject;
		BOOL  m_fRunning;
		BOOL  m_fOnline;

		// Event Handlers
		void OnChange(DWORD dwIdent);
		void OnStatus(DWORD dwIdent);
		void OnTiming(DWORD dwIdent);
		void OnProgress(DWORD dwIdent);
		void OnPoll(DWORD dwIdent);
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Blocks Definition
//

class DLLAPI CFunctionDefinition
{
	public:
		// Constructor
		CFunctionDefinition(DWORD dwIdent);

		// Filter
		BOOL CanLoad(void);

		// Properties
		CString GetName(void);

		// Attributes
		BOOL IsStandard(void);
		BOOL IsCustom(void);
		BOOL IsOperator(void);
		BOOL IsFunction(void);
		BOOL IsFuncBlock(void);
		BOOL IsIec(void);
		BOOL IsMath(void);
		BOOL Is64Bit(void);
		BOOL IsMulti(void);
		BOOL IsArray(void);
		BOOL IsException(void);
		BOOL IsCategory(PCTXT pName);

	protected:
		// Type
		enum {
			funcSO	= 1,
			funcSF,
			funcSB,
			funcCF,
			funcCB,
			funcNK,
			};

		// Data
		DWORD		m_dwIdent;
		DWORD		m_Type;
		CString		m_Name;
		CStringArray	m_Class;
		CStringArray	m_Input;
		CStringArray	m_Output;

		// Implementation
		BOOL Is64Bit(CStringArray const &List);
		BOOL Is64Bit(CString const &Type);
		BOOL IsArray(CStringArray const &List);
		BOOL IsArray(CString const &Type);		
		void Build(void);
		void Clean(void);
	};

// End of File

#endif
