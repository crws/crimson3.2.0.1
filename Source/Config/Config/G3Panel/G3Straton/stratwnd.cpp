
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Window
//

// Dynamic Class

AfxImplementDynamicClass(CStratonWnd, CWnd);

// Constructor

CStratonWnd::CStratonWnd(void)
{
	}

// Operations

BOOL CStratonWnd::Create(CWnd &Parent)
{
	CRect Rect = Parent.GetClientRect();

	return CWnd::Create( m_Name,
			     WS_CHILD | WS_VISIBLE | WS_TABSTOP,
			     Rect,
		  	     Parent,
			     HMENU(IDVIEW),
			     NULL
			     );
	}

// 4.1 - Common

BOOL CStratonWnd::GetEnable(void)
{
	return BOOL(SendMessage(WM_CTRL_GETENABLE));	
	}

void CStratonWnd::SetEnable(BOOL fEnable)
{
	SendMessage(WM_CTRL_SETENABLE, WPARAM(fEnable), 0);
	}

void CStratonWnd::SetReadOnly(BOOL fReadOnly)
{
	SendCommand(L"SetReadOnly%b", fReadOnly);
	}

BOOL CStratonWnd::GetReadOnly(void)
{
	return BOOL(SendCommand(L"GetReadOnly"));
	}

void CStratonWnd::SetModified(BOOL fModify)
{
	SendMessage(WM_CTRL_SETMODIFIED, WPARAM(fModify), 0);	
	}

BOOL CStratonWnd::IsModified(void)
{
	return BOOL(SendMessage(WM_CTRL_ISMODIFIED));	
	}

void CStratonWnd::SetDebug(BOOL fDebug)
{
	SendMessage(WM_CTRL_SETDEBUGSTATE, WPARAM(fDebug), 0);
	}

BOOL CStratonWnd::IsDebug(void)
{
	return BOOL(SendCommand(L"Isdebug"));
	}	

BOOL CStratonWnd::IsEmpty(void)
{
	return BOOL(SendMessage(WM_CTRL_ISEMPTY, 0, 0));
	}

// 4.2 - Database Connection

BOOL CStratonWnd::SetProjectPath(PCTXT pPath)
{
	return BOOL(SendMessage(WM_CTRL_SETPROJECTPATH, 0, LPARAM(LPCSTR(CAnsiString(pPath)))));
	}

CString CStratonWnd::GetProjectPath(void)
{
	return String(LPCSTR(SendCommand(L"getprojectpath")));
	}

DWORD CStratonWnd::GetProjectId(void)
{
	return DWORD(SendCommand(L"getprojectid"));
	}

BOOL CStratonWnd::RemoveProject(DWORD dwProj)
{
	return DWORD(SendCommand(L"removeproject%d", dwProj));
	}

CString CStratonWnd::GetProgramName(void)
{
	return String(LPCSTR(SendCommand(L"getprogramname")));
	}

void CStratonWnd::SetID(DWORD dwID)
{
	SendMessage(WM_CTRL_SETID, 0, LPARAM(dwID));
	}

// 4.3 Editing

BOOL CStratonWnd::CanCut(void)
{
	return BOOL(SendMessage(WM_CTRL_CANCUT));	
	}

void CStratonWnd::Cut(void)
{
	SendMessage(WM_CTRL_CUT);	
	}

BOOL CStratonWnd::CanClear(void)
{
	return BOOL(SendMessage(WM_CTRL_CANCLEAR));	
	}

void CStratonWnd::Clear(void)
{
	SendMessage(WM_CTRL_CLEAR);	
	}

BOOL CStratonWnd::CanCopy(void)
{
	return BOOL(SendMessage(WM_CTRL_CANCOPY));	
	}

void CStratonWnd::Copy(void)
{
	SendMessage(WM_CTRL_COPY);	
	}

BOOL CStratonWnd::CanPaste(void)
{
	return BOOL(SendMessage(WM_CTRL_CANPASTE));	
	}

void CStratonWnd::Paste(void)
{
	SendMessage(WM_CTRL_PASTE);	
	}

BOOL CStratonWnd::CanUndo(void)
{
	return BOOL(SendMessage(WM_CTRL_CANUNDO));
	}

void CStratonWnd::Undo(void)
{
	SendMessage(WM_CTRL_UNDO);	
	}

BOOL CStratonWnd::CanRedo(void)
{
	return BOOL(SendMessage(WM_CTRL_CANREDO));	
	}

void CStratonWnd::Redo(void)
{
	SendMessage(WM_CTRL_REDO);	
	}

void CStratonWnd::EmptyUndoStack(void)
{
	SendCommand(L"emptyundostack");
	}

BOOL CStratonWnd::CanSwapItemStyle(void)
{
	return BOOL(SendCommand(L"canswapitemstyle"));
	}

void CStratonWnd::SwapItemStyle(void)
{
	SendCommand(L"swapitemstyle");
	}

// 4.4 - Display

BOOL CStratonWnd::CanSetZoom(void)
{
	return BOOL(SendMessage(WM_CTRL_CANSETZOOM));	
	}

void CStratonWnd::SetZoom(int nRatio)
{
	SetZoom(W5ZOOM_AT, nRatio);
	}

void CStratonWnd::SetZoom(int nDir, int nRatio)
{	
	SendMessage(WM_CTRL_SETZOOM, WPARAM(nDir), LPARAM(nRatio));
	}

int CStratonWnd::GetZoom(void)
{
	return int(SendCommand(L"GetZoom"));
	}

BOOL CStratonWnd::CanSetGrid(void)
{
	return BOOL(SendMessage(WM_CTRL_CANSETGRID));	
	}

void CStratonWnd::SetGrid(BOOL fGrid)
{
	SendMessage(WM_CTRL_SETGRID, WPARAM(fGrid), 0);
	}

BOOL CStratonWnd::IsGridVisible(void)
{
	return BOOL(SendMessage(WM_CTRL_ISGRIDVISIBLE));	
	}

BOOL CStratonWnd::IsBookmark(void)
{
	return BOOL(SendCommand(L"isbookmark"));
	}

void CStratonWnd::SetBookmark(BOOL fSet)
{
	SendCommand(L"setbookmark%b", fSet);
	}

void CStratonWnd::GoNextBookmark(void)
{
	SendCommand(L"gonextbookmark");
	}

void CStratonWnd::GoPrevBookmark(void)
{
	SendCommand(L"goprevbookmark");
	}

void CStratonWnd::DeleteAllBookmark(void)
{
	SendCommand(L"deletallbookmark");
	}

// 4.5 Selection

BOOL CStratonWnd::HasSelection(void)
{
	return BOOL(SendMessage(WM_CTRL_HASSELECTION));	
	}

BOOL CStratonWnd::CanSelectAll(void)
{
	return BOOL(SendMessage(WM_CTRL_CANSELECTALL, 0, 0));	
	}

void CStratonWnd::SelectAll(void)
{
	SendMessage(WM_CTRL_SELECTALL, 0, 0);	
	}

void CStratonWnd::DeselectAll(void)
{
	SendCommand(L"deselectall");
	}

void CStratonWnd::EnsureSelVisible(void)
{
	SendCommand(L"ensureselvisible");
	}

void CStratonWnd::SelectItem(DWORD dwData, BOOL fDeselectAll)
{
	}

void CStratonWnd::SelectItems(CLongArray Data, BOOL fDeselectAll)
{
	}

// 4.6 - File Exchange

BOOL CStratonWnd::CanSave(void)
{
	return BOOL(SendMessage(WM_CTRL_CANSAVE));
	}

void CStratonWnd::Save(PCTXT pPath)
{
	SendMessage(WM_CTRL_SAVE, 0, LPARAM(LPCSTR(CAnsiString(pPath))));
	}

void CStratonWnd::Load(PCTXT pPath)
{
	SendMessage(WM_CTRL_LOAD, 0, LPARAM(LPCSTR(CAnsiString(pPath))));
	}

BOOL CStratonWnd::CanInsertFile(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTFILE));
	}

void CStratonWnd::InsertFile(PCTXT pPath)
{
	SendMessage(WM_CTRL_INSERTFILE, 0, LPARAM(LPCSTR(CAnsiString(pPath))));
	}

// 4.7 - Text Exchange

void CStratonWnd::SetText(PCTXT pText)
{
	SendCommand(L"settext%s", pText);

	return;

	LRESULT r = SendMessage(WM_CTRL_SETTEXT, 0, LPARAM((LPCSTR(CAnsiString(pText)))));

	AfxTouch(r);
	}

CString CStratonWnd::GetText(void)
{
	return String(LPCSTR(SendMessage(WM_CTRL_GETTEXT)));
	}

UINT CStratonWnd::GetSelTextLength(void)
{
	return UINT(SendMessage(WM_CTRL_GETSELTEXTLENGTH, 0, 0));
	}

CString CStratonWnd::GetSelText(void)
{
	UINT  uLen    = GetSelTextLength() + 1;

	LPSTR pBuffer = New char [ uLen ];

	memset(pBuffer, 0, uLen);

	SendMessage(WM_CTRL_GETSELTEXT, 0, LPARAM(pBuffer));

	CString Text =  CString(pBuffer);

	delete[] pBuffer;

	return Text;
	}

BOOL CStratonWnd::CanInsertText(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTTEXT));
	}

void CStratonWnd::InsertText(CPoint Pos, PCTXT pText)
{
	SendMessage(WM_CTRL_INSERTTEXT, WPARAM(Pos), LPARAM(LPCSTR(CAnsiString(pText))));
	}

// 4.8 - Location Find/Replace

void CStratonWnd::LocateError(PCTXT pError)
{
	SendCommand(L"LocateError%s", pError);
	}

void CStratonWnd::Goto(PCTXT pGoto)
{
	// NOTE --- obsolete

	SendMessage(WM_CTRL_GOTO, 0, LPARAM(LPCSTR(CAnsiString(pGoto))));
	}

DWORD CStratonWnd::FindReplace(CFindReplace &Find)
{
	str_W5FindReplace Work;

	Work.sStringToFind	= CAnsiString(Find.m_Search);
	Work.sStringToReplace	= CAnsiString(Find.m_Replace);
	Work.dwCommand		= Find.m_dwCommand;
	Work.dwFlags		= Find.m_dwFlags;
	Work.hwndListBox	= Find.m_hListBox;
	Work.pFile		= Find.m_pFile;
	Work.dwData		= Find.m_dwData;

	DWORD r = SendMessage(WM_CTRL_FINDREPLACE, 0, LPARAM((str_W5FindReplace *) &Work));

	return r;
	}

DWORD CStratonWnd::SearchIn(CFindReplace &Find)
{
	// NOTE -- duplicate of FindReplace

	return 0;
	}

CPoint CStratonWnd::GetCurPos(void)
{
	return CPoint(SendMessage(WM_CTRL_GETCURPOS, 0, 0));
	}

CSize CStratonWnd::GetSelSize(void)
{
	DWORD dwSize = DWORD(SendCommand(L"GetSelSize"));

	return CSize(dwSize);
	}

CPoint CStratonWnd::GetSelPos(void)
{
	DWORD dwPos = DWORD(SendCommand(L"GetSelPos"));

	return CPoint(dwPos);
	}

CPoint CStratonWnd::GetSymbolPos(void)
{
	DWORD dwPos = DWORD(SendCommand(L"GetSymbolPos"));

	return CPoint(dwPos);
	}

CPoint CStratonWnd::GetCaret(void)
{
	DWORD dwPos = DWORD(SendCommand(L"GetCaret"));

	return CPoint(dwPos);
	}

void CStratonWnd::GotoXy(CPoint Pos)
{
	SendCommand(L"GotoXy%ld%ld", Pos.x, Pos.y);
	}

void CStratonWnd::SaveSel(void)
{
	SendCommand(L"savesel\n");
	}

void CStratonWnd::RestoreSel(void)
{
	SendCommand(L"restoresel\n");
	}

CString CStratonWnd::FindVarInput(PCTXT pInput)
{
	// TODO -- how does this work?

	return String(LPCSTR(SendCommand(L"findvarinput%s", pInput)));
	}

// 4.10 - Paint

int CStratonWnd::Paint(HDC hDC, CRect Rect)
{
	return int(SendMessage(WM_CTRL_PAINT, WPARAM(hDC), LPARAM((CRect const *) &Rect)));
	}

void CStratonWnd::ReloadBitmap(void)
{
	SendCommand(L"reloadbitmap");
	}

// 4.11 - Function Blocks/Symbols

void CStratonWnd::SetCurrentFB(CFunctionBlock &FB)
{
	str_W5FB Work;

	strcpy(Work.sName, CAnsiString(FB.m_Name));
	
	Work.dwID	   = FB.m_dwID;
	
	Work.iNbIn	   = FB.m_nInputs;

	Work.iNbOut	   = FB.m_nOutputs;

	Work.bInstanciable = FB.m_fInstantiable;

	Work.bDBObject	   = FB.m_fDBObject;

	LRESULT r = SendMessage(WM_CTRL_SETCURRENTFB, 0, LPARAM((str_W5FB const *) &Work));

	AfxTouch(r);
	}

BOOL CStratonWnd::CanInsertFB(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTFB));
	}

void CStratonWnd::InsertFB(CFunctionBlock &FB)
{
	str_W5FB Work;

	strcpy(Work.sName, CAnsiString(FB.m_Name));
	
	Work.dwID	   = FB.m_dwID;
	
	Work.iNbIn	   = FB.m_nInputs;

	Work.iNbOut	   = FB.m_nOutputs;

	Work.bInstanciable = FB.m_fInstantiable;

	Work.bDBObject	   = FB.m_fDBObject;

	LRESULT r = SendMessage(WM_CTRL_INSERTFB, 0, LPARAM((str_W5FB const *) &Work));

	AfxTouch(r);
	}

void CStratonWnd::SetFB(CFunctionBlock &FB)
{
	str_W5FB Work;

	strcpy(Work.sName, CAnsiString(FB.m_Name));
	
	Work.dwID	   = FB.m_dwID;
	
	Work.iNbIn	   = FB.m_nInputs;

	Work.iNbOut	   = FB.m_nOutputs;

	Work.bInstanciable = FB.m_fInstantiable;

	Work.bDBObject	   = FB.m_fDBObject;

	LRESULT r = SendMessage(WM_CTRL_SETFB, 0, LPARAM((str_W5FB const *) &Work));

	AfxTouch(r);
	}

void CStratonWnd::GetFB(CFunctionBlock &FB)
{
	str_W5FB Work;

	LRESULT r = SendMessage(WM_CTRL_GETFB, 0, LPARAM((str_W5FB *) &Work));

	AfxTouch(r);

	FB.m_Name	   = CString(Work.sName);

	FB.m_dwID	   = Work.dwID;

	FB.m_nInputs	   = Work.iNbIn;

	FB.m_nOutputs	   = Work.iNbOut;

	FB.m_fInstantiable = BOOL(Work.bInstanciable);

	FB.m_fDBObject	   = BOOL(Work.bDBObject);

	FB.m_dwData	   = Work.dwData;
	}

void CStratonWnd::GetVar(CVariable &Var)
{
	//AfxAssert(FALSE);

	_s_W5Variable Work;

	LRESULT r = SendMessage(WM_CTRL_GETVAR, 0, LPARAM((_s_W5Variable *) &Work));

	AfxTouch(r);

	Var.m_Name	= CString(Work.sName);

	Var.m_dwID	= Work.dwID;

	Var.m_fLocal	= BOOL(Work.bLocal);

	Var.m_Short	= CString(Work.sShortComment);

	Var.m_Long	= CString(Work.sLongComment);

	Var.m_IOName	= CString(Work.sIOName);

	Var.m_dwData	= Work.dwData;
	}

BOOL CStratonWnd::CanInsertSymbol(void)
{
	return BOOL(SendCommand(L"CanInsertSymbol"));
	}

void CStratonWnd::InsertSymbol(PCTXT pSymbol)
{
	SendCommand(L"InsertSymbol%s", pSymbol);
	}

CString CStratonWnd::GetSymbolName(void)
{
	return String(LPCSTR(SendMessage(WM_CTRL_GETSYMBOLNAME)));
	}

CString CStratonWnd::GetTypeName(void)
{
	return String(LPCSTR(SendMessage(WM_CTRL_GETTYPENAME)));
	}

DWORD CStratonWnd::GetItemType(void)
{
	return DWORD(SendCommand(L"GetItemType"));
	}

CString CStratonWnd::GetItemTypeSel(void)
{
	return String(LPCSTR(SendCommand(L"GetItemTypeSel")));
	}

WCHAR CStratonWnd::GetFirstChar(void)
{
	// NOTE -- how does this work?

	char cChar = char(SendMessage(WM_CTRL_GETFIRSTCHAR));

	AfxAssert(FALSE);

	return WCHAR(cChar);
	}

CString	CStratonWnd::GetUsedBlock(void)
{
	// NOTE -- how does this work

	return String(LPCSTR(SendCommand(L"GetUsedBlock")));
	}

UINT CStratonWnd::GetDbId(CLongArray &List)
{
	UINT uCount;

	if( (uCount = UINT(SendCommand(L"getdbid%ld", 0))) ) {

		PDWORD pList = New DWORD [ uCount ];

		SendCommand(L"getdbid%ld", pList);

		List.Append(pList, uCount);

		delete[] pList;

		return List.GetCount();
		}

	return 0;
	}

CString	CStratonWnd::GetInputBlockVar(int nPin)
{
	return CString();
	}

BOOL CStratonWnd::CanEditPins(void)
{
	return FALSE;
	}

void CStratonWnd::EditPins(void)
{
	}

// 4.12 - Settings

BOOL CStratonWnd::EnableDragDrop(BOOL fEnable)
{
	return BOOL(SendMessage(WM_CTRL_ENABLEDRAGNDROP, WPARAM(fEnable), 0));
	}

void CStratonWnd::UndoRedoSize(int nSize)
{
	SendMessage(WM_CTRL_UNDOREDOSIZE, WPARAM(nSize), 0);
	}

void CStratonWnd::KeepFbdSelect(BOOL fEnable)
{
	SendCommand(L"KeepFbdSelect%b", fEnable);
	}

void CStratonWnd::CopyBitmap(BOOL fEnable)
{
	SendMessage(WM_CTRL_COPYBITMAP, WPARAM(fEnable), 0);
	}

void CStratonWnd::PromptVarName(BOOL fEnable)
{
	SendMessage(WM_CTRL_PROMPTVARNAME, WPARAM(fEnable), 0);
	}

void CStratonWnd::PromptInstance(BOOL fEnable)
{
	SendMessage(WM_CTRL_PROMPTINSTANCE, WPARAM(fEnable), 0);
	}

BOOL CStratonWnd::EnablePulse(BOOL fEnable)
{
	return BOOL(SendMessage(WM_CTRL_ENABLEPULSE, WPARAM(fEnable), 0));
	}

void CStratonWnd::SetVertVarSize(int nSize)
{
	SendMessage(WM_CTRL_SETVERTVARSIZE, WPARAM(nSize), 0);
	}

void CStratonWnd::SetHorzVarSize(int nSize)
{
	SendMessage(WM_CTRL_SETHORZVARSIZE, WPARAM(nSize), 0);
	}

void CStratonWnd::SetPageWidth(int nSize)
{
	SendMessage(WM_CTRL_SETPAGEWIDTH, WPARAM(nSize), 0);
	}

void CStratonWnd::SetContents(int nType, PCTXT pContents)
{
	CAnsiString Str(pContents);

	SendMessage(WM_CTRL_SETCONTENTS, WPARAM(nType), LPARAM(LPCSTR(Str)));
	}

BOOL CStratonWnd::AutoDeclareInst(BOOL fEnable)
{
	return BOOL(SendMessage(WM_CTRL_AUTODECLAREINST, WPARAM(fEnable), LPARAM(0L)));
	}

BOOL CStratonWnd::AutoDeclareSymbol(BOOL fEnable)
{
	return BOOL(SendMessage(WM_CTRL_AUTODECLARESYMBOL, WPARAM(fEnable), LPARAM(0L)));
	}

void CStratonWnd::HideOptionDlgVar(BOOL fHide)
{
	SendCommand(L"HideOptionDlgVar%b", fHide);
	}

void CStratonWnd::EnableCopy(BOOL fEnable)
{
	SendCommand(L"EnableCopy%b", fEnable);
	}

void CStratonWnd::HideScroll(BOOL fEnable)
{
	SendMessage(WM_CTRL_HIDESCROLL, WPARAM(fEnable), LPARAM(0L));
	}

void CStratonWnd::SetToolTipInfos(BOOL fEdit, CToolTipInfo const &Info)
{
	str_W5TooltipInfos Work;

	Work.bName	= Info.m_fName;
	Work.bType	= Info.m_fType;
	Work.bDim	= Info.m_fDim;
	Work.bAttrib	= Info.m_fAttrib;
	Work.bProps	= Info.m_fProps;
	Work.bSyb	= Info.m_fSyb;
	Work.bInitValue	= Info.m_fInitValue;
	Work.bTag	= Info.m_fTag;
	Work.bDesc	= Info.m_fDesc;

	SendMessage(WM_CTRL_SETTOOLTIPINFOS, WPARAM(fEdit), LPARAM( (str_W5TooltipInfos *) &Work));
	}

void CStratonWnd::EditPropAfterInsertVar(BOOL fEnable)
{
	SendCommand(L"EditPropAfterInsertVar%b", fEnable);
	}

void CStratonWnd::SetChildOffset(int nOffset)
{
	SendCommand(L"SetChildOffset%d", nOffset);
	}

void CStratonWnd::InsertVarWhenInsertFB(BOOL fEnable)
{
	SendMessage(WM_CTRL_INSERTVARWHENINSERTFB, WPARAM(fEnable), LPARAM(0L));	
	}

BOOL CStratonWnd::DisplayIO(BOOL fEnable)
{
	return BOOL(SendCommand(L"DisplayIO%b", fEnable));
	}

void CStratonWnd::SetAutoEdit(UINT uMask, BOOL fSet)
{
/*	if( fSet ) {

		m_uAutoEdit |= uMask;
		}
	else {
		m_uAutoEdit &= ~uMask;
		}

	SendCommand(L"SetAutoEdit%d", m_uAutoEdit);
*/	}

void CStratonWnd::SetAutoEdit(DWORD dwMask)
{
	SendCommand(L"SetAutoEdit%d", dwMask);

/*	/*m_uAutoEdit = uMask;

	SendCommand(L"SetAutoEdit%d", m_uAutoEdit);
*/	}

BOOL CStratonWnd::HideIOName(BOOL fHide)
{
	return BOOL(SendCommand(L"HideIOName%b", fHide));
	}

BOOL CStratonWnd::UnderlineGlobal(BOOL fEnable)
{
	return BOOL(SendMessage(WM_CTRL_UNDERLINEGLOBAL, WPARAM(fEnable), 0));
	}

BOOL CStratonWnd::SetInfos(DWORD dwInfo, PVOID pInfo)
{
	return FALSE;
	}

void CStratonWnd::GetInfos(DWORD dwInfo, PVOID pInfo)
{
	}

BOOL CStratonWnd::SetBW(BOOL fEnable)
{
	return BOOL(SendCommand(L"SetBW%b", fEnable));
	}

void CStratonWnd::Activate(BOOL fEnable)
{
	SendMessage(WM_CTRL_ACTIVATE, WPARAM(fEnable), 0);
	}

void CStratonWnd::SetAutoConnectVariable(BOOL fEnable)
{
	SendMessage(WM_CTRL_SETAUTOCONNECTVARIABLE, WPARAM(fEnable), 0);
	}

void CStratonWnd::ShowStLine(BOOL fShow)
{
	SendMessage(WM_CTRL_SHOWSTLINE, WPARAM(fShow));
	}

// 4.26 - Tree Display

BOOL CStratonWnd::CanHexDisplay(void)
{
	return BOOL(SendCommand(L"CanHexDisplay"));
	}

BOOL CStratonWnd::IsHexDisplay(void)
{
	return BOOL(SendCommand(L"IsHexDisplay"));
	}

void CStratonWnd::SwapHexDisplay(void)
{
	SendCommand(L"SwapHexDisplay");
	}

// Message Map

AfxMessageMap(CStratonWnd, CWnd)
{
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_MOUSEMOVE)

	AfxMessageEnd(CStratonWnd)
	};

// Default Class Name

PCTXT CStratonWnd::GetDefaultClassName(void) const
{
	return m_pLib->GetClassName();
	}

// Message Handlers

void CStratonWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	AfxCallDefProc();

	GetParent().SendMessageConst(WM_MOUSEMOVE, uFlags, Pos);
	}

void CStratonWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	AfxCallDefProc();

	GetParent().SendMessageConst(WM_MOUSEWHEEL, WPARAM(nDelta), LPARAM(Pos));
	}

// Command Execution

LRESULT CStratonWnd::SendCommand(PCTXT pFormat,...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	CCommandBuilder Cmd(pFormat, pArgs);

	va_end(pArgs);

	LRESULT r = m_pLib->ExecuteCommand(GetHandle(), Cmd);

	return r;
	}

LRESULT CStratonWnd::SendCommand(CCommandBuilder &Cmd)
{
	return m_pLib->ExecuteCommand(GetHandle(), Cmd);
	}

// Command Help

CString CStratonWnd::String(LPCSTR pStr)
{
	if( pStr ) {
		
		return CString(pStr);
		}

	return CString();
	}

// End of File
