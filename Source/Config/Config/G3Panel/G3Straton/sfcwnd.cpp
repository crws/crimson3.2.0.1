
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Sequential Flow Chart Window
//

// Dynamic Class

AfxImplementDynamicClass(CStratonSFCWnd, CStratonWnd);

// Constructor

CStratonSFCWnd::CStratonSFCWnd(void)
{
	m_pLib = CSFCLibrary::FindInstance();
	}

// 4.21 - SFC Control

BOOL CStratonSFCWnd::CanInsert(void)
{
	return BOOL(SendMessage(W5EDITSFCM_CANINSERT));
	}

void CStratonSFCWnd::InsertStep(void)
{
	SendMessage(W5EDITSFCM_INSERTSTEP);
	}

void CStratonSFCWnd::InsertTrans(void)
{
	SendMessage(W5EDITSFCM_INSERTTRANS);
	}

void CStratonSFCWnd::InsertJump(void)
{
	SendMessage(W5EDITSFCM_INSERTJUMP);
	}

void CStratonSFCWnd::InsertMainDiv(void)
{
	SendMessage(W5EDITSFCM_INSERTMAINDIV);
	}

void CStratonSFCWnd::InsertDiv(void)
{
	SendMessage(W5EDITSFCM_INSERTDIV);
	}

void CStratonSFCWnd::InsertCnv(void)
{
	SendMessage(W5EDITSFCM_INSERTCNV);
	}

void CStratonSFCWnd::InsertMacro(void)
{
	SendMessage(W5EDITSFCM_INSERTMACRO);
	}

void CStratonSFCWnd::InsertMacroBody(void)
{
	SendMessage(W5EDITSFCM_INSERTMACROBODY);
	}

void CStratonSFCWnd::InsertInitStep(void)
{
	SendMessage(W5EDITSFCM_INSERTINITSTEP);
	}

BOOL CStratonSFCWnd::CanSwapStyle(void)
{
	return BOOL(SendCommand(L"CanSwapStyle"));
	}

void CStratonSFCWnd::SwapStyle(void)
{
	SendCommand(L"SwapStyle");
	}

CString CStratonSFCWnd::GetTransCode(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETTRANSCODE, WPARAM(nRef), LPARAM(0L))));
	}

CString CStratonSFCWnd::GetTransNote(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETTRANSNOTE, WPARAM(nRef), LPARAM(0L))));
	}

void CStratonSFCWnd::SetTransNote(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETTRANSNOTE, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

void CStratonSFCWnd::SetTransCode(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETTRANSCODE, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

CString CStratonSFCWnd::GetStepCodeDef(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETSTEPCODE_DEF, WPARAM(nRef), LPARAM(0L))));
	}

void CStratonSFCWnd::SetStepCodeDef(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETSTEPCODE_DEF, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

CString CStratonSFCWnd::GetStepCodeP1(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETSTEPCODE_P1, WPARAM(nRef), LPARAM(0L))));
	}

void CStratonSFCWnd::SetStepCodeP1(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETSTEPCODE_P1, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

CString CStratonSFCWnd::GetStepCodeN(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETSTEPCODE_N, WPARAM(nRef), LPARAM(0L))));
	}

void CStratonSFCWnd::SetStepCodeN(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETSTEPCODE_N, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

CString CStratonSFCWnd::GetStepCodeP0(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETSTEPCODE_P0, WPARAM(nRef), LPARAM(0L))));
	}

void CStratonSFCWnd::SetStepCodeP0(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETSTEPCODE_P0, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

CString CStratonSFCWnd::GetStepNote(int nRef)
{
	return String(LPCSTR(SendMessage(W5EDITSFCM_GETSTEPNOTE, WPARAM(nRef), LPARAM(0L))));
	}

void CStratonSFCWnd::SetStepNote(int nRef, CString Code)
{
	SendMessage(W5EDITSFCM_SETSTEPNOTE, WPARAM(nRef), LPARAM(LPCSTR(CAnsiString(Code))));
	}

CString CStratonSFCWnd::GetMacroNote(int nRef)
{
	return String(LPCSTR(SendCommand(L"GetMacroNote%d", nRef)));
	}

void CStratonSFCWnd::SetMacroNote(int nRef, CString Code)
{
	SendCommand(L"GetMacroNote%d%s", nRef, WideToAnsi(Code));
	}

BOOL CStratonSFCWnd::IsSelStep(void)
{
	return BOOL(SendMessage(W5EDITSFCM_ISSELSTEP));
	}

BOOL CStratonSFCWnd::IsSelTrans(void)
{
	return BOOL(SendMessage(W5EDITSFCM_ISSELTRANS));
	}

DWORD CStratonSFCWnd::IsSelMacro(void)
{
	return DWORD(SendCommand(L"IsSelMacro"));
	}

DWORD CStratonSFCWnd::GetSelRefNum(void)
{
	return DWORD(SendMessage(W5EDITSFCM_GETSELREFNUM));
	}

void CStratonSFCWnd::LockStep(BOOL fLock, int nRef)
{
	SendMessage(W5EDITSFCM_LOCKSTEP, WPARAM(fLock), LPARAM(nRef));
	}

void CStratonSFCWnd::LockTrans(BOOL fLock, int nRef)
{
	SendMessage(W5EDITSFCM_LOCKTRANS, WPARAM(fLock), LPARAM(nRef));
	}

void CStratonSFCWnd::LockMacro(BOOL fLock, int nRef)
{
	SendCommand(L"LockMacro%b%d", fLock, nRef);

	/*SendMessage(W5EDITSFCM_LOCKMACRO, WPARAM(fLock), LPARAM(nRef));*/
	}

BOOL CStratonSFCWnd::IsStepLock(int nRef)
{
	return BOOL(/*SendMessage(W5EDITSFCM_ISSTEPLOCK, WPARAM(0), LPARAM(nRef))*/FALSE);
	}

BOOL CStratonSFCWnd::IsTransLock(int nRef)
{
	return BOOL(/*SendMessage(W5EDITSFCM_ISTRANSLOCK, WPARAM(0), LPARAM(nRef))*/FALSE);
	}

BOOL CStratonSFCWnd::CanEnterSelRef(void)
{
	return BOOL(SendMessage(W5EDITSFCM_CANENTERSELREF));
	}

void CStratonSFCWnd::EnterSelRef(void)
{
	SendMessage(W5EDITSFCM_ENTERSELREF);
	}

BOOL CStratonSFCWnd::CanEditSelCode(void)
{
	return BOOL(SendMessage(W5EDITSFCM_CANEDITSELCODE));
	}

UINT CStratonSFCWnd::GetStepLanguageP1(int nRef)
{
	return UINT(SendMessage(W5EDITSFCM_GETSTEPLANGUAGEP1, WPARAM(nRef), LPARAM(0)));
	}

UINT CStratonSFCWnd::GetStepLanguageP0(int nRef)
{
	return UINT(SendMessage(W5EDITSFCM_GETSTEPLANGUAGEP0, WPARAM(nRef), LPARAM(0)));
	}

UINT CStratonSFCWnd::GetStepLanguageN(int nRef)
{
	return UINT(SendMessage(W5EDITSFCM_GETSTEPLANGUAGEN, WPARAM(nRef), LPARAM(0)));
	}

UINT CStratonSFCWnd::GetTransLanguage(int nRef)
{
	return UINT(SendMessage(W5EDITSFCM_GETTRANSLANGUAGE, WPARAM(nRef), LPARAM(0)));
	}

void CStratonSFCWnd::SetNoteDisplay(BOOL fEnable)
{
	SendMessage(W5EDITSFCM_SETNOTEDISPLAY, WPARAM(fEnable), LPARAM(0));
	}

BOOL CStratonSFCWnd::GetNoteDisplay(void)
{
	return BOOL(SendMessage(W5EDITSFCM_GETNOTEDISPLAY));
	}

void CStratonSFCWnd::SetDisplay(UINT uMode)
{
	SendCommand(L"SetDisplay%d", uMode);	
	}

UINT CStratonSFCWnd::GetDisplay(void)
{
	return UINT(SendCommand(L"GetDisplay"));
	}

BOOL CStratonSFCWnd::CanRenumber(void)
{
	return BOOL(SendMessage(W5EDITSFCM_CANRENUMBER));
	}

void CStratonSFCWnd::Renumber(void)
{
	SendMessage(W5EDITSFCM_RENUMBER);
	}

DWORD CStratonSFCWnd::NextPosItem(BOOL fLoop, int nRef)
{
	return DWORD(SendMessage(W5EDITSFCM_NEXTPOSITEM, WPARAM(fLoop), LPARAM(nRef)));
	}

BOOL CStratonSFCWnd::IsStep(CPoint Pos)
{
	int nRef;

	DWORD Point = DWORD(Pos);

	return BOOL(SendMessage( W5EDITSFCM_ISSTEP, 
				 WPARAM((DWORD *) &Point), 
				 LPARAM((int *) &nRef)));
	}

BOOL CStratonSFCWnd::IsTrans(CPoint Pos)
{
	int nRef;

	DWORD Point = DWORD(Pos);

	return BOOL(SendMessage( W5EDITSFCM_ISTRANS, 
				 WPARAM((DWORD *) &Point), 
				 LPARAM((PINT) &nRef))
				 );

	}

BOOL CStratonSFCWnd::IsComment(CPoint Pos)
{
	return BOOL(SendCommand(L"IsComment"));
	}

BOOL CStratonSFCWnd::IsSelComment(void)
{
	return BOOL(SendCommand(L"IsSelComment"));
	}

void CStratonSFCWnd::LockComment(BOOL fLock, int nRef)
{
	SendCommand(L"LockComment%b%d", fLock, nRef);
	}

CString CStratonSFCWnd::GetCommentNote(int nRef)
{
	return String(LPCSTR(SendCommand(L"GetCommentNote%d", nRef)));
	}

void CStratonSFCWnd::SetCommentNote(int nRef, CString Code)
{
	SendCommand(L"SetCommentNote%d%s", nRef, Code);
	}

DWORD CStratonSFCWnd::GetItemGuid(CPoint Pos)
{
	return 0;
	}

void CStratonSFCWnd::SetTimer(void)
{
	SendCommand(L"SetTimer");
	}

BOOL CStratonSFCWnd::SetSFCSettings(DWORD dwFlags)
{
	return SendCommand(L"SetSFCSettings2%ld", dwFlags);
	}

BOOL CStratonSFCWnd::SetSFCSettings(CSfcSettings const &Settings)
{
	return FALSE;
	}

void CStratonSFCWnd::InsertComment(void)
{
	SendCommand(L"InsertComment");
	}

// End of File
