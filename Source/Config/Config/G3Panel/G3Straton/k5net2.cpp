
#include "intern.hpp"

#include "..\..\shell\shlink\shlink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// K5Net API Hooks
//

// Macros

#define	clink	extern "C"

#define	EXPORT	__declspec(dllexport)

// Return Codes

#define K5NET_OK		0
#define K5NETERR_INTERNAL	1
#define K5NETERR_BUSY		2
#define K5NETERR_NOTSUPPORTED	3
#define K5NETERR_NODATA		4
#define K5NETERR_BADPORT	5
#define K5NETERR_CANTCOMM	6
#define K5NETERR_BADTARGETID	7
#define K5NETERR_OPENLOCALFILE	8
#define K5NETERR_DOWNLOAD	9

// Static Data

global	CDatabase *	g_pDbase;

static	BOOL volatile	m_fOkay       = FALSE;

static	HWND volatile	m_hParent     = NULL;

static	HWND volatile	m_hMainWnd    = NULL;

static	UINT		m_msgCallback = 0;

static	UINT		m_uSendTime   = 0;

static	CEvent		m_Event(FALSE);

static	BYTE		m_bFind[8] = { 0x07, '*', '*', '*', '*', '*', '*', '*' };

static	BYTE		m_bName[8] = { 0x07, 'x', 'x', 'x', 'x', 'x', 'x', 'x' };

// Debug

#if 0

static void Debug(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTraceArgs(pText, pArgs);

	va_end(pArgs);
	}

static void Dump(PCBYTE p, UINT c)
{
	AfxDump(p, c);
	}

#endif

// Event Callback

static BOOL OnEvent(PVOID pParam, UINT uCode)
{
	switch( uCode ) {

		case IDINIT:
		case IDDONE:
				
			m_fOkay = TRUE;

			break;

		case IDFAIL:
				
			m_fOkay = FALSE;

			break;

		case IDCRED:

			PostMessage(m_hMainWnd, WM_AFX_COMMAND, 0xD046, 0);

			return TRUE;

		default:

			return TRUE;
		}

	if( m_hParent ) {

		UINT uTime = GetTickCount() - m_uSendTime;

		UINT uTest = m_fOkay ? 0 : 250;

		if( uTime < uTest ) {

			Sleep(uTest - uTime);
			}

		PostMessage(m_hParent, m_msgCallback, 0, 0);

		return TRUE;
		}

	m_Event.Set();

	return TRUE;
	}

// Name Fettling

clink BOOL K5NETDRV_SetName(PCTXT pName)
{
	AfxAssert(wstrlen(pName) == 7);

	for( UINT n = 0; n < 7; n++ ) {

		m_bName[n+1] = LOBYTE(pName[n]);
		}

	return TRUE;
	}

// K5Net APIs

clink int EXPORT K5NETDRV_GetVersion(void)
{
	AfxAssert(FALSE);

	return 1;
	}

clink LPCSTR EXPORT K5NETDRV_GetName(void)
{
	AfxAssert(FALSE);

	return "T5 Runtime";
	}

clink void EXPORT K5NETDRV_ToggleTrace(void)
{
	AfxAssert(FALSE);
	}

clink DWORD EXPORT K5NETDRV_GetProtocol(void)
{
	AfxAssert(FALSE);

	return 2;
	}

clink DWORD EXPORT K5NETDRV_AcceptAppCtl(void)
{
	AfxAssert(FALSE);

	return 0;
	}

clink HWND EXPORT K5NETDRV_GetTaskMan(void)
{
	AfxAssert(FALSE);

	return NULL;
	}

clink DWORD EXPORT K5NETDRV_Setup(HWND hParent, LPSTR pConfig, DWORD dwBufSize)
{
	AfxAssert(FALSE);

	return 0;
	}

clink DWORD EXPORT K5NETDRV_Connect( LPCSTR pConfig,
				     DWORD  dwTargetID,
				     HWND   hParent,
				     HWND   hTrace,
				     DWORD  msgCallback,
				     PDWORD pReport
				     )
{
	m_Event.Reset();

	if( !m_hParent ) {

		if( Link_StratonStart(OnEvent, NULL) ) {

			m_Event.WaitForObject();

			if( m_fOkay ) {

				m_hParent     = hParent;

				m_hMainWnd    = afxMainWnd->GetHandle();

				m_msgCallback = msgCallback;

				if( pReport ) {
		
					*pReport = K5NET_OK;
					}

				return 1;
				}

			Link_StratonClose();
			}
		}

	if( pReport ) {
		
		*pReport = K5NETERR_CANTCOMM;
		}

	return 0;
	}

clink void EXPORT K5NETDRV_Disconnect(DWORD hContext)
{
	if( m_hParent ) {

		Link_StratonClose();

		m_hParent = NULL;
		}
	}

clink DWORD EXPORT K5NETDRV_Send(DWORD hContext, DWORD dwCode, DWORD dwSize, PBYTE pBuffer)
{
	if( m_hParent ) {

		m_uSendTime = GetTickCount();

		Link_StratonSend(pBuffer, dwSize);

		return K5NET_OK;
		}

	return K5NETERR_CANTCOMM;
	}

clink DWORD EXPORT K5NETDRV_Receive(DWORD hContext, PDWORD pCode, PDWORD pSize, PDWORD pPercent, PBYTE pBuffer)
{
	if( m_hParent ) {

		if( m_fOkay ) {

			*pSize = Link_StratonRecv(pBuffer, 4096);

			if( *pBuffer == 0x81 ) {

				// This is the reply frame at the start of the online debugging
				// process and it contains the project name. But we fettle the
				// downloaded code to always have a project name of 7 stars so
				// as to avoid changes when the same code is loaded into a new
				// Straton instance. This means we have to replace the stars
				// with the real project name or we won't be able to go online!

				PBYTE pName = pBuffer + 0x14;

				if( !memcmp(pName, m_bFind, sizeof(m_bFind)) ) {

					memcpy(pName, m_bName, sizeof(m_bName));
					}
				}

			*pCode    = 0;

			*pPercent = 0;

			return K5NET_OK;
			}
		}

	*pSize    = 0;

	*pCode    = 0;

	*pPercent = 0;

	return K5NETERR_CANTCOMM;
	}

clink void EXPORT K5NETDRV_Abort(DWORD hContext)
{
	}

// End of File
