
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Ladder Diagram Editor Window
//

class CLadderDiagramEditorWnd : public CEditorWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CLadderDiagramEditorWnd(void);

	protected:
		// Data


		// Accelerator
		CAccelerator m_Accel;

		// Message Map
		AfxDeclareMessageMap();
	
		// Accelerators
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnMouseMove(UINT uFlags, CPoint Pos);

		// Command Handlers
		BOOL OnCtrlGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCtrlControl(UINT uID, CCmdSource &Src);
		BOOL OnCtrlCommand(UINT uID);

		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);

		// Notification Handlers

		// Access
		CStratonLDWnd &GetEditor(void);

		// Information
		void LoadSettings(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// Ladder Editor Window
//

// Dynamic Class

AfxImplementDynamicClass(CLadderDiagramEditorWnd, CEditorWnd);

// Constructor

CLadderDiagramEditorWnd::CLadderDiagramEditorWnd(void)
{
	m_pEdit = New CStratonLDWnd;

	m_Accel.Create(L"LadderDiagramEditorMenu");
	}

// Message Map

AfxMessageMap(CLadderDiagramEditorWnd, CEditorWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_MOUSEMOVE)

	AfxDispatchGetInfoType(IDM_CTRL,  OnCtrlGetInfo )
	AfxDispatchControlType(IDM_CTRL,  OnCtrlControl )
	AfxDispatchCommandType(IDM_CTRL,  OnCtrlCommand )

	AfxDispatchGetInfoType(IDM_VIEW,  OnViewGetInfo )
	AfxDispatchControlType(IDM_VIEW,  OnViewControl )
	AfxDispatchCommandType(IDM_VIEW,  OnViewCommand )

	AfxDispatchGetInfoType(IDM_TOOL, OnToolGetInfo )
	AfxDispatchControlType(IDM_TOOL, OnToolControl )
	AfxDispatchCommandType(IDM_TOOL, OnToolCommand )

	AfxMessageEnd(CLadderDiagramEditorWnd)
	};

// Accelerators

BOOL CLadderDiagramEditorWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CLadderDiagramEditorWnd::OnPostCreate(void)
{
	CEditorWnd::OnPostCreate();

	LoadSettings();
	}

void CLadderDiagramEditorWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	ShowDefaultStatus();
	}

// Command Handlers

BOOL CLadderDiagramEditorWnd::OnCtrlGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CLadderDiagramEditorWnd::OnCtrlControl(UINT uID, CCmdSource &Src)
{
	CStratonLDWnd &Wnd = (CStratonLDWnd &) *m_pEdit;

	switch( uID ) {


		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CLadderDiagramEditorWnd::OnCtrlCommand(UINT uID)
{
	CStratonLDWnd &Wnd = (CStratonLDWnd &) *m_pEdit;

	switch( uID ) {


		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return FALSE;
	}

BOOL CLadderDiagramEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CLadderDiagramEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	CStratonLDWnd &Wnd = (CStratonLDWnd &) *m_pEdit;

	switch( uID ) {


		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CLadderDiagramEditorWnd::OnViewCommand(UINT uID)
{
	CStratonLDWnd &Wnd = (CStratonLDWnd &) *m_pEdit;

	switch( uID ) {


		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CLadderDiagramEditorWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	return FALSE;
	}

BOOL CLadderDiagramEditorWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	CStratonLDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_CONTACT_BEFORE:
			Src.EnableItem(Wnd.CanInsertContactBefore());
			break;

		case IDM_TOOL_CONTACT_AFTER:
			Src.EnableItem(Wnd.CanInsertContactAfter());
			break;

		case IDM_TOOL_CONTACT_PARALLEL:
			Src.EnableItem(Wnd.CanInsertContactParallel());
			break;

		case IDM_TOOL_FB_BEFORE:
			Src.EnableItem(Wnd.CanInsertFBBefore());
			break;

		case IDM_TOOL_HORZ:
			Src.EnableItem(Wnd.CanInsertHorz());
			break;

		case IDM_TOOL_SWAP_STYLE:
			Src.EnableItem(Wnd.CanSwapItemStyle());
			break;

		case IDM_TOOL_FB_AFTER:
			Src.EnableItem(Wnd.CanInsertFBAfter());
			break;

		case IDM_TOOL_FB_PARALLEL:
			Src.EnableItem(Wnd.CanInsertFBParallel());
			break;

		case IDM_TOOL_JUMP:
			Src.EnableItem(Wnd.CanInsertJump());
			break;

		case IDM_TOOL_COIL:
			Src.EnableItem(Wnd.CanInsertCoil());
			break;

		case IDM_TOOL_RUNG:
			Src.EnableItem(Wnd.CanInsertRung());
			break;

		case IDM_TOOL_COMMENT:
			Src.EnableItem(Wnd.CanInsertComment());
			break;

		case IDM_TOOL_ALIGN_COILS:
			Src.EnableItem(Wnd.CanAlignCoils());
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CLadderDiagramEditorWnd::OnToolCommand(UINT uID)
{
	CStratonLDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_CONTACT_BEFORE:
			Wnd.InsertContactBefore();
			break;

		case IDM_TOOL_CONTACT_AFTER:
			Wnd.InsertContactAfter();
			break;

		case IDM_TOOL_CONTACT_PARALLEL:
			Wnd.InsertContactParallel();
			break;

		case IDM_TOOL_HORZ:
			Wnd.InsertHorz();
			break;

		case IDM_TOOL_SWAP_STYLE:
			Wnd.SwapItemStyle();
			break;

		case IDM_TOOL_FB_BEFORE:
			Wnd.InsertFBBefore();
			break;

		case IDM_TOOL_FB_AFTER:
			Wnd.InsertFBAfter();
			break;

		case IDM_TOOL_FB_PARALLEL:
			Wnd.InsertFBParallel();
			break;

		case IDM_TOOL_JUMP:
			Wnd.InsertJump();
			break;

		case IDM_TOOL_COIL:
			Wnd.InsertCoil();
			break;

		case IDM_TOOL_RUNG:
			Wnd.InsertRung();
			break;

		case IDM_TOOL_COMMENT:
			Wnd.InsertComment();
			break;

		case IDM_TOOL_ALIGN_COILS:
			Wnd.AlignCoils();
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Notification Handlers

// Implementation

CStratonLDWnd & CLadderDiagramEditorWnd::GetEditor(void)
{	
	return (CStratonLDWnd &) *m_pEdit;
	}

// Implementation

void CLadderDiagramEditorWnd::LoadSettings(void)
{
	CStratonWnd &Wnd = GetEditor();

	//
	DWORD dwMask = AUTOEDIT_ALL;

	dwMask &= ~AUTOEDIT_SETVAR;

	//dwMask &= ~AUTOEDIT_SETFB;

	Wnd.SetAutoEdit(dwMask);

	Wnd.PromptVarName(TRUE);

	Wnd.AutoDeclareSymbol(TRUE);
	}

// End of File
