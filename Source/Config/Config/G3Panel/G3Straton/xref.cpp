
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// XRef Object
//

// Static Data

CCrossReference * CCrossReference::m_pThis	= NULL;

DWORD	          CCrossReference::m_dwClient	= 0;

// Object Location

CCrossReference * CCrossReference::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CCrossReference::CCrossReference(void)
{
	AfxAssert(m_pThis == NULL);

	LoadLib(L"K5XRef");

	m_pThis		= this;

	m_dwClient	= 0;
	}

// Version

INT CCrossReference::GetVersion(void)
{
	return m_pfnGetVersion();
	}

// Operations

UINT CCrossReference::FindInFiles(PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile)
{
	return m_pfnFindInFiles( LPCSTR(CAnsiString(pPath)), 
				 LPCSTR(CAnsiString(pFind)), 
				 hWnd, 
				 pFile, 
				 0, 0
				 );
	}

UINT CCrossReference::FindUnusedVars(PCTXT pPath, HWND hWnd, FILE *pFile)
{
	return m_pfnFindUnusedVars( LPCSTR(CAnsiString(pPath)), 
				    hWnd, 
				    pFile, 
				    0, 0
				    );
	}

UINT CCrossReference::FindMultAssign(PCTXT pPath, HWND hWnd, FILE *pFile)
{
	return m_pfnFindMultAssign( LPCSTR(CAnsiString(pPath)), 
				    hWnd, 
				    pFile, 
				    0, 0
				    );
	}

UINT CCrossReference::FindInProg(PCTXT pPath, PCTXT pName, PCTXT pFind, DWORD dwLanguage, HWND hWnd, FILE *pFile)
{
	return m_pfnFindInProg( LPCSTR(CAnsiString(pPath)), 
				LPCSTR(CAnsiString(pName)), 
				LPCSTR(CAnsiString(pFind)), 
				dwLanguage,
				hWnd, 
				pFile, 
				0, 0
				);
	}

UINT CCrossReference::FindUsedLibs(PCTXT pPath, HWND hWnd, FILE *pFile)
{
	return m_pfnFindUsedLibs( LPCSTR(CAnsiString(pPath)), 
				  hWnd, 
				  pFile, 
				  0, 0
				  );
	}

UINT CCrossReference::ReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile)
{
	return m_pfnReplaceInFiles( hParent, 
				    LPCSTR(CAnsiString(pPath)),
				    LPCSTR(CAnsiString(pFind)),
				    hWnd,
				    pFile,
				    0, 0
				    );
	}

UINT CCrossReference::AutoReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, PCTXT pReplace, HWND hWnd, FILE *pFile)
{
	return m_pfnAutoReplaceInFiles( hParent, 
					LPCSTR(CAnsiString(pPath)),
					LPCSTR(CAnsiString(pFind)),
					LPCSTR(CAnsiString(pReplace)),
					hWnd,
					pFile,
					0, 0
					);
	}

// Library Management

void CCrossReference::LoadLib(PCTXT pName)
{
	CFilename Path = afxModule->GetFilename().GetDirectory();

	CFilename Name = Path + L"Straton\\" + pName;

	if( (m_hLib = LoadLibrary(Name)) ) {

		m_pfnGetVersion		= (PPK5XREF_GetVersion)		GetProcAddress(m_hLib,	"K5XRef_GetVersion");

		m_pfnFindInFiles	= (PPK5XREF_FindInFiles)	GetProcAddress(m_hLib,	"K5XRef_FindInFiles");

		m_pfnFindUnusedVars	= (PPK5XREF_FindUnusedVars)	GetProcAddress(m_hLib,	"K5XRef_FindUnusedVars");

		m_pfnFindMultAssign	= (PPK5XREF_FindMultAssign)	GetProcAddress(m_hLib,	"K5XRef_FindMultAssign");

		m_pfnFindInProg		= (PPK5XREF_FindInProg)		GetProcAddress(m_hLib,	"K5XRef_FindInProg");

		m_pfnFindUsedLibs	= (PPK5XREF_FindUsedLibs)	GetProcAddress(m_hLib,	"K5XRef_FindUsedLibs");

		m_pfnReplaceInFiles	= (PPK5XREF_ReplaceInFiles)	GetProcAddress(m_hLib,	"K5XRef_ReplaceInFiles");

		m_pfnAutoReplaceInFiles	= (PPK5XREF_AutoReplaceInFiles)	GetProcAddress(m_hLib,	"K5XRef_AutoReplaceInFiles");
		
		AfxAssert( m_pfnGetVersion         && 
			   m_pfnFindInFiles        &&
			   m_pfnFindUnusedVars     &&
			   m_pfnFindMultAssign     &&
			   m_pfnFindInProg         &&
			   m_pfnFindUsedLibs       &&
			   m_pfnReplaceInFiles	   && 
			   m_pfnAutoReplaceInFiles );
		
		return;
		}

	DWORD dwError = GetLastError();

	AfxTrace(L"ERROR: Failed to load %s\t%8.8X\n", pName, dwError);

	AfxAssert(m_hLib);
	}

void CCrossReference::FreeLib(void)
{
	AfxVerify(FreeLibrary(m_hLib));
	}

//////////////////////////////////////////////////////////////////////////
//
// XRef APIs
//

global	INT	Straton_XRefGetVersion(void)
{
	return CCrossReference::FindInstance()->GetVersion();
	}

global	UINT	Straton_XRefFindInFiles(PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile)
{
	return CCrossReference::FindInstance()->FindInFiles(pPath, pFind, hWnd, pFile);
	}

global	UINT	Straton_XRefFindUnusedVars(PCTXT pPath, HWND hWnd, FILE *pFile)
{
	return CCrossReference::FindInstance()->FindUnusedVars(pPath, hWnd, pFile);
	}

global	UINT	Straton_XRefFindMultAssign(PCTXT pPath, HWND hWnd, FILE *pFile)
{
	return CCrossReference::FindInstance()->FindMultAssign(pPath, hWnd, pFile);
	}

global	UINT	Straton_XRefFindInProg(PCTXT pPath, PCTXT pName, PCTXT pFind, DWORD dwLanguage, HWND hWnd, FILE *pFile)
{
	return CCrossReference::FindInstance()->FindInProg(pPath, pName, pFind, dwLanguage, hWnd, pFile);
	}

global	UINT	Straton_XRefReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, HWND hWnd, FILE *pFile)
{
	return CCrossReference::FindInstance()->ReplaceInFiles(hParent, pPath, pFind, hWnd, pFile);
	}

global	UINT	Straton_XRefAutoReplaceInFiles(HWND hParent, PCTXT pPath, PCTXT pFind, PCTXT pReplace, HWND hWnd, FILE *pFile)
{
	return CCrossReference::FindInstance()->AutoReplaceInFiles(hParent, pPath, pFind, pReplace, hWnd, pFile);
	}

// End of File
