
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Registry
//

// Static Data

CStratonRegistry * CStratonRegistry::m_pThis = NULL;

// Object Location

CStratonRegistry * CStratonRegistry::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CStratonRegistry::CStratonRegistry(void)
{
	AfxAssert(m_pThis == NULL);

	LoadLib(L"K5DBReg");

	m_pThis = this;
	}

// Attributes

UINT CStratonRegistry::GetVersion(void)
{
	return K5DBReg_GetVersion();
	}

CString CStratonRegistry::GetLibFolderPath(void)
{
	LPSTR pStr = New char [ _MAX_PATH ];

	K5DBReg_GetLibFolderPath(pStr);

	CString Text = afxModule->GetFilename().GetDirectory() + String(pStr);

	delete[] pStr;
	
	return Text;
	}

CString CStratonRegistry::GetLibDefineFilePath(void)
{
	LPSTR pStr = New char [ _MAX_PATH ];

	K5DBReg_GetLibDefineFilePath(pStr);

	CString Text = afxModule->GetFilename().GetDirectory() + String(pStr);

	delete[] pStr;
	
	return Text;
	}

// Blocks

UINT CStratonRegistry::GetBlocks(DWORD dwMask, CLongArray &List)
{
	UINT uCount = GetNbBlock(dwMask);

	if( uCount ) {

		PDWORD pList = New DWORD [ uCount ];

		GetBlocks(dwMask, pList);
		
		List.Empty();

		List.Append(pList, uCount);

		delete[] pList;
		}

	return uCount;
	}

CString CStratonRegistry::GetBlockLibName(DWORD dwIndex)
{
	return String(K5DBReg_GetBlockLibName(dwIndex));
	}

CString CStratonRegistry::GetBlockDesc(DWORD dwBlock, DWORD &dwKind, DWORD &dwLibNo, DWORD &dwNbInput, DWORD &dwNbOutput)
{
	return String(K5DBReg_GetBlockDesc( dwBlock, 
					    &dwKind, 
					    &dwLibNo, 
					    &dwNbInput, 
					    &dwNbOutput
					    ));
	}

CString CStratonRegistry::GetBlockName(DWORD dwBlock)
{
	DWORD dwKind, dwLibNo, dwNbInput, dwNbOutput;

	return GetBlockDesc( dwBlock,
			     dwKind, 
			     dwLibNo, 
			     dwNbInput, 
			     dwNbOutput
			     );
	}

CString CStratonRegistry::GetBlockInput(DWORD dwBlock, DWORD dwPin, DWORD &dwType)
{
	return String(K5DBReg_GetBlockInput( dwBlock, 
					     dwPin, 
					     &dwType
					     ));
	}

CString CStratonRegistry::GetBlockOutput(DWORD dwBlock, DWORD dwPin, DWORD &dwType)
{
	return String(K5DBReg_GetBlockOutput( dwBlock, 
					      dwPin, 
					      &dwType
					      ));
	}

DWORD CStratonRegistry::FindBlock(CString Name)
{
	return K5DBReg_FindBlock(CAnsiString(Name));
	}

DWORD CStratonRegistry::GetPinNames(DWORD dwBlock, DWORD dwStyle, CString &In, CString &Out)
{
	// NOTE -- not much success with this fucntion - Stirng assigment failure?

	LPCSTR  pIn;
	
	LPCSTR pOut;

	DWORD dwResult = K5DBReg_GetPinNames( dwBlock, 
					      dwStyle, 
					      &pIn, 
					      &pOut
					      );

	In  = pIn;

	Out = pOut;

	return dwResult;
	}

DWORD CStratonRegistry::BlockNeedEN(DWORD dwBlock)
{
	return K5DBReg_BlockNeedEN(dwBlock);
	}

DWORD CStratonRegistry::BlockNeedENO(DWORD dwBlock)
{
	return K5DBReg_BlockNeedENO(dwBlock);
	}

CString CStratonRegistry::GetBlockPinTypeName(DWORD dwType)
{
	return String(K5DBReg_GetBlockPinTypeName(dwType));
	}

CString CStratonRegistry::GetBlockInputPinTypeName(DWORD dwBlock, DWORD dwPin)
{
	return String(K5DBReg_GetBlockInputPinTypeName(dwBlock, dwPin));
	}

BOOL CStratonRegistry::BlockCanBeExtended(CString Name)
{
	return K5DBREG_BlockCanBeExtended(LPCSTR(CAnsiString(Name)));
	}

CString CStratonRegistry::GetBlockProperties(DWORD dwBlock)
{
	return String(K5DBReg_GetBlockProperties(dwBlock));
	}

CString CStratonRegistry::GetBlockPropSyntax(DWORD dwBlock)
{
	return String(K5DBReg_GetBlockPropSyntax(dwBlock));
	}

BOOL CStratonRegistry::IsBlockInputRef(DWORD dwBlock, DWORD dwPin)
{
	return K5DBReg_IsBlockInputRef(dwBlock, dwPin);
	}

BOOL CStratonRegistry::IsBlockInputArray(DWORD dwBlock, DWORD dwPin)
{
	return K5DBReg_IsBlockInputArray(dwBlock, dwPin);
	}

HICON CStratonRegistry::GetFBIcon(DWORD dwBlock)
{
	return K5DBReg_GetFBIcon(dwBlock);
	}

DWORD CStratonRegistry::GetFBConstraint(DWORD dwBlock, DWORD dwGroup)
{
	return K5DBReg_GetFBConstraint(dwBlock, dwGroup);
	}

// IOs

UINT CStratonRegistry::GetIOLibNames(CStringArray &List)
{
	UINT uCount = GetNbIOLib();

	for( UINT n = 0; n < uCount; n ++ ) {		

		CString Name = GetIOLibName(n);
		
		List.Empty();

		List.Append(Name);		
		}

	return uCount;
	}

DWORD CStratonRegistry::GetNbIOLib(void)
{
	return K5DBReg_GetNbIOLib();
	}

CString	CStratonRegistry::GetIOLibName(DWORD dwIndex)
{
	return String(K5DBReg_GetIOLibName(dwIndex));
	}

UINT CStratonRegistry::GetIOs(PCTXT pLib, CLongArray &List)
{
	UINT uCount = GetNbIO(pLib);

	if( uCount ) {

		PDWORD pList = New DWORD [ uCount ];

		GetIOs(pLib, pList);
		
		List.Empty();

		List.Append(pList, uCount);

		delete[] pList;
		}

	return uCount;
	}

DWORD CStratonRegistry::FindIO(PCTXT pName)
{
	return K5DBReg_FindIO(LPCSTR(CAnsiString(pName)));
	}

CString CStratonRegistry::GetIODesc(DWORD hIO, DWORD &dwNbGroup)
{
	return String(K5DBReg_GetIODesc(hIO, &dwNbGroup));
	}

CString CStratonRegistry::GetIOGroupDesc(DWORD hIO, DWORD dwGroup, CString &Name, CString &Prefix)
{
	LPCSTR pName;

	LPCSTR pPrefix;

	CString Desc = String(K5DBReg_GetIOGroupDesc( hIO, 
						      dwGroup, 
						      &pName, 
					              &pPrefix
					              ));
	Name   = CString(pName);

	Prefix = CString(pPrefix);

	return Desc;
	}

DWORD CStratonRegistry::GetIOGroupNbChannel(DWORD hIO, DWORD hGroup, DWORD &dwMin, DWORD &dwMax)
{
	return K5DBReg_GetIOGroupNbChannel(hIO, hGroup, &dwMin, &dwMax);
	}

CString CStratonRegistry::GetIOChannelName(DWORD hIO, DWORD hGroup, DWORD hChannel)
{
	return String(K5DBReg_GetIOChannelName(hIO, hGroup, hChannel));
	}

CString CStratonRegistry::GetIOProperty(DWORD hIO, DWORD hGroup, DWORD hProp)
{
	return CString(K5DBReg_GetIOProperty(hIO, hGroup, hProp));
	}

// Profiles

UINT CStratonRegistry::GetProfiles(PCTXT pLib, CLongArray &List)
{
	UINT uCount = GetNbProfile(pLib);

	if( uCount ) {

		PDWORD pList = New DWORD [ uCount ];

		GetProfiles(pLib, pList);
		
		List.Empty();

		List.Append(pList, uCount);

		delete[] pList;
		}

	return uCount;
	}

DWORD CStratonRegistry::FindProfile(PCTXT pName)
{
	return K5DBReg_FindProfile(LPCSTR(CAnsiString(pName)));
	}

CString CStratonRegistry::GetProfileName(DWORD hProfile)
{
	return String(K5DBReg_GetProfileName(hProfile));
	}

CString CStratonRegistry::GetProfileText(DWORD hProfile)
{
	return String(K5DBReg_GetProfileText(hProfile));
	}

CString CStratonRegistry::GetProfileLibName(DWORD hProfile)
{
	return String(K5DBReg_GetProfileLibName(hProfile));
	}

// Types

UINT CStratonRegistry::GetTypes(PCTXT pLib, CLongArray &List)
{
	UINT uCount = GetNbType(pLib);

	if( uCount ) {

		PDWORD pList = New DWORD [ uCount ];

		GetTypes(pLib, pList);
		
		List.Empty();

		List.Append(pList, uCount);

		delete[] pList;
		}

	return uCount;
	}

DWORD CStratonRegistry::FindType(PCTXT pName)
{
	return K5DBReg_FindType(LPCSTR(CAnsiString(pName)));
	}

CString CStratonRegistry::GetTypeName(DWORD dwType)
{
	return String(K5DBReg_GetTypeName(dwType));
	}

CString CStratonRegistry::GetTypeText(DWORD hType)
{
	return String(K5DBReg_GetTypeText(hType));
	}

CString CStratonRegistry::GetTypeLibName(DWORD dwType)
{
	return String(K5DBReg_GetTypeLibName(dwType));
	}

// Internal

UINT CStratonRegistry::GetNbBlock(DWORD dwMask)
{
	return K5DBReg_GetNbBlock(dwMask);
	}

UINT CStratonRegistry::GetBlocks(DWORD dwMask, PDWORD pList)
{
	return K5DBReg_GetBlocks(dwMask, pList);
	}

UINT CStratonRegistry::GetNbIO(PCTXT pLib)
{
	return K5DBReg_GetNbIO(LPCSTR(CAnsiString(pLib)));
	}

UINT CStratonRegistry::GetIOs(PCTXT pLib, PDWORD pList)
{
	return K5DBReg_GetIOs(LPCSTR(CAnsiString(pLib)), pList);
	}

UINT CStratonRegistry::GetNbProfile(PCTXT pLib)
{
	return K5DBReg_GetNbProfile(LPCSTR(CAnsiString(pLib)));
	}

UINT CStratonRegistry::GetProfiles(PCTXT pLib, PDWORD pList)
{
	return K5DBReg_GetProfiles(LPCSTR(CAnsiString(pLib)), pList);
	}

UINT CStratonRegistry::GetNbType(PCTXT pLib)
{
	return K5DBReg_GetNbType(LPCSTR(CAnsiString(pLib)));
	}

UINT CStratonRegistry::GetTypes(PCTXT pLib, PDWORD pList)
{
	return K5DBReg_GetTypes(LPCSTR(CAnsiString(pLib)), pList);
	}

// String Support

CString CStratonRegistry::String(LPCSTR pStr)
{
	return pStr ? CString(pStr) : CString();
	}

// Library Management

void CStratonRegistry::LoadLib(PCTXT pName)
{
	CFilename Path = afxModule->GetFilename().GetDirectory();

	CFilename Name = Path + L"Straton\\" + pName;

	if( (m_hLib = LoadLibrary(Name)) ) {
		
		return;
		}

	DWORD dwError = GetLastError();

	AfxTrace(L"ERROR: Failed to load %s\t%8.8X\n", pName, dwError);

	AfxAssert(m_hLib);
	}

void CStratonRegistry::FreeLib(void)
{
	AfxVerify(FreeLibrary(m_hLib));
	}

// End of File
