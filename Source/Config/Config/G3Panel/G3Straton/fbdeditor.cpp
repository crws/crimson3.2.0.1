
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Function Block Diagram Editor Window
//

class CFunctionBlockDiagramEditorWnd : public CEditorWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CFunctionBlockDiagramEditorWnd(void);

	protected:
		// Data
		BOOL	m_fGridSnap;

		// Accelerator
		CAccelerator m_Accel;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);
	
		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnMouseMove(UINT uFlags, CPoint Pos);

		// Command Handlers
		BOOL OnCtrlGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnCtrlControl(UINT uID, CCmdSource &Src);
		BOOL OnCtrlCommand(UINT uID);

		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);

		// Notification Handlers
		void OnModified(UINT uID, UINT uData);

		// Access
		CStratonFBDWnd &GetEditor(void);

		// Implementation
		void LoadConfig(void);
		void SaveConfig(void);
		void LoadSettings(void);
		void CheckTool(UINT uCmd, CCmdSource &Src);
	};

/////////////////////////////////////////////////////////////////////////
//
// Function Block Diagram Editor Window
//

// Dynamic Class

AfxImplementDynamicClass(CFunctionBlockDiagramEditorWnd, CEditorWnd);

// Constructor

CFunctionBlockDiagramEditorWnd::CFunctionBlockDiagramEditorWnd(void)
{
	m_pEdit = New CStratonFBDWnd;

	m_Accel.Create(L"FunctionBlockDiagramEditorMenu");
	}

// Message Map

AfxMessageMap(CFunctionBlockDiagramEditorWnd, CEditorWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_MOUSEMOVE)

	AfxDispatchGetInfoType(IDM_CTRL,  OnCtrlGetInfo )
	AfxDispatchControlType(IDM_CTRL,  OnCtrlControl )
	AfxDispatchCommandType(IDM_CTRL,  OnCtrlCommand )

	AfxDispatchGetInfoType(IDM_VIEW,  OnViewGetInfo )
	AfxDispatchControlType(IDM_VIEW,  OnViewControl )
	AfxDispatchCommandType(IDM_VIEW,  OnViewCommand )

	AfxDispatchGetInfoType(IDM_TOOL,  OnToolGetInfo )
	AfxDispatchControlType(IDM_TOOL,  OnToolControl )
	AfxDispatchCommandType(IDM_TOOL,  OnToolCommand )

	AfxDispatchNotify(100, W5EDITN_MODIFIED,	OnModified  )

	AfxMessageEnd(CFunctionBlockDiagramEditorWnd)
	};

// Accelerators

BOOL CFunctionBlockDiagramEditorWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CFunctionBlockDiagramEditorWnd::OnPostCreate(void)
{
	CEditorWnd::OnPostCreate();

	LoadConfig();

	LoadSettings();
	}

void CFunctionBlockDiagramEditorWnd::OnPreDestroy(void)
{
	CEditorWnd::OnPreDestroy();

	SaveConfig();
	}

void CFunctionBlockDiagramEditorWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	ShowDefaultStatus();
	}

// Command Handlers

BOOL CFunctionBlockDiagramEditorWnd::OnCtrlGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnCtrlControl(UINT uID, CCmdSource &Src)
{
	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnCtrlCommand(UINT uID)
{
	switch( uID ) {

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_VIEW_EXECUTION_ORDER:
			
			Src.EnableItem(Wnd.CanDisplayFbdOrder());

			break;

		case IDM_VIEW_GRID_SNAP:

			Src.EnableItem(TRUE);

			Src.CheckItem(m_fGridSnap);
			
			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnViewCommand(UINT uID)
{
	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_VIEW_EXECUTION_ORDER:
			
			Wnd.DisplayFbdOrder();

			break;

		case IDM_VIEW_GRID_SNAP:

			m_fGridSnap = !m_fGridSnap;

			Wnd.SnapFbdGrid(m_fGridSnap);			

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_UNDERLINE_GLOBAL:
			//Info.m_Image = 0x6000000A;
			break;

		case IDM_TOOL_INSERT_VAR_WITH_FB:
			//Info.m_Image = 0x60000000;
			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return FALSE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	static UINT const List[] = 

	{	IDM_TOOL_SELECT,	FBD_SELECT,
		IDM_TOOL_FUNCTION,	FBD_ADD_FB,
		IDM_TOOL_VARIABLE,	FBD_ADD_VAR,
		IDM_TOOL_COMMENT,	FBD_ADD_COMMENT,
		IDM_TOOL_ARC,		FBD_ADD_ARC,
		IDM_TOOL_CORNER,	FBD_ADD_CORNER,
		IDM_TOOL_BREAK,		FBD_ADD_BREAK,
		IDM_TOOL_LABEL,		FBD_ADD_LABEL,
		IDM_TOOL_JUMP,		FBD_ADD_JUMP,
		IDM_TOOL_LEFT_RAIL,	FBD_ADD_LEFTRAIL,
		IDM_TOOL_CONTACT,	FBD_ADD_CONTACT,
		IDM_TOOL_OR_RAIL,	FBD_ADD_OR,
		IDM_TOOL_COIL,		FBD_ADD_COIL,
		IDM_TOOL_RIGHT_RAIL,	FBD_ADD_RIGHTRAIL,

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			CheckTool(List[n+1], Src);

			return TRUE;
			}
		}

	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_KEEP_FBD_SELECT:

			Src.EnableItem(TRUE);
			
			Src.CheckItem(!m_fKeepFbdSelect);
			
			break;

		case IDM_TOOL_UNDERLINE_GLOBAL:

			Src.EnableItem(TRUE);
			
			Src.CheckItem(m_fUnderlineGlobal);

			break;

		case IDM_TOOL_INSERT_VAR_WITH_FB:

			Src.EnableItem(TRUE);

			Src.CheckItem(m_fInsertVarWhenInsertFB);

			break;

		case IDM_TOOL_SWAP_STYLE:

			Src.EnableItem(Wnd.CanSwapItemStyle());
			
			break;

		default:		
			return FALSE;
		}
	
	return TRUE;
	}

BOOL CFunctionBlockDiagramEditorWnd::OnToolCommand(UINT uID)
{
	static UINT const List[] = 

	{	IDM_TOOL_SELECT,	FBD_SELECT,
		IDM_TOOL_FUNCTION,	FBD_ADD_FB,
		IDM_TOOL_VARIABLE,	FBD_ADD_VAR,
		IDM_TOOL_COMMENT,	FBD_ADD_COMMENT,
		IDM_TOOL_ARC,		FBD_ADD_ARC,
		IDM_TOOL_CORNER,	FBD_ADD_CORNER,
		IDM_TOOL_BREAK,		FBD_ADD_BREAK,
		IDM_TOOL_LABEL,		FBD_ADD_LABEL,
		IDM_TOOL_JUMP,		FBD_ADD_JUMP,
		IDM_TOOL_LEFT_RAIL,	FBD_ADD_LEFTRAIL,
		IDM_TOOL_CONTACT,	FBD_ADD_CONTACT,
		IDM_TOOL_OR_RAIL,	FBD_ADD_OR,
		IDM_TOOL_COIL,		FBD_ADD_COIL,
		IDM_TOOL_RIGHT_RAIL,	FBD_ADD_RIGHTRAIL,

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			CStratonFBDWnd &Wnd = GetEditor();

			Wnd.Check(List[n+1]);

			return TRUE;
			}
		}

	CStratonFBDWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_KEEP_FBD_SELECT:

			m_fKeepFbdSelect = !m_fKeepFbdSelect;

			Wnd.KeepFbdSelect(m_fKeepFbdSelect);
			
			break;

		case IDM_TOOL_UNDERLINE_GLOBAL:

			m_fUnderlineGlobal = !m_fUnderlineGlobal;

			Wnd.UnderlineGlobal(m_fUnderlineGlobal);

			Wnd.Invalidate(FALSE);

			break;

		case IDM_TOOL_INSERT_VAR_WITH_FB:

			m_fInsertVarWhenInsertFB = !m_fInsertVarWhenInsertFB;

			Wnd.InsertVarWhenInsertFB(m_fInsertVarWhenInsertFB);

			break;

		case IDM_TOOL_SWAP_STYLE:

			Wnd.SwapItemStyle();

			break;

		default:		
			return FALSE;
		}

	return TRUE;
	}

// Notification Handlers

void CFunctionBlockDiagramEditorWnd::OnModified(UINT uID, UINT uData)
{
	CEditorWnd::OnModified(uID, uData);
	}

// Access

CStratonFBDWnd & CFunctionBlockDiagramEditorWnd::GetEditor(void)
{	
	return (CStratonFBDWnd &) *m_pEdit;
	}

// Implementation

void CFunctionBlockDiagramEditorWnd::LoadConfig(void)
{
	CEditorWnd::LoadConfig();

	CStratonFBDWnd &Wnd = GetEditor();

	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(GetClassName());

	m_fGridSnap         = (Reg.GetValue(L"GridSnap",	   UINT(TRUE)));

	Wnd.SnapFbdGrid(m_fGridSnap);
	}

void CFunctionBlockDiagramEditorWnd::SaveConfig(void)
{
	CEditorWnd::SaveConfig();

	CStratonFBDWnd &Wnd = GetEditor();

	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(GetClassName());

	Reg.SetValue(L"GridSnap",		m_fGridSnap);
	
	AfxTouch(Wnd);
	}

void CFunctionBlockDiagramEditorWnd::LoadSettings(void)
{
	CStratonFBDWnd &Wnd = GetEditor();

	//
	DWORD dwMask = AUTOEDIT_ALL;

	dwMask &= ~AUTOEDIT_SETVAR;

	//dwMask &= ~AUTOEDIT_SETFB;

	//dwMask &= ~AUTOEDIT_EDITPROPS;

	Wnd.SetAutoEdit(dwMask);

	Wnd.PromptVarName(TRUE);

	Wnd.PromptInstance(TRUE);

	Wnd.AutoDeclareSymbol(TRUE);

	Wnd.DrawFbdBridge(TRUE);
	}

void CFunctionBlockDiagramEditorWnd::CheckTool(UINT uCmd, CCmdSource &Src)
{
	CStratonFBDWnd &Wnd = GetEditor();

	Src.EnableItem(Wnd.CanCheck(uCmd));

	Src.CheckItem(Wnd.IsCheck(uCmd));
	}

// End of File
