
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ladder Diagram Window
//

// Dynamic Class

AfxImplementDynamicClass(CStratonLDWnd, CStratonWnd);

// Constructor

CStratonLDWnd::CStratonLDWnd(void)
{
	m_pLib = CLDLibrary::FindInstance();
	}

// 4.20 - LD Control

BOOL CStratonLDWnd::CanAlignCoils(void)
{
	return BOOL(SendMessage(WM_CTRL_CANALIGNCOILS));
	}

void CStratonLDWnd::AlignCoils(void)
{
	SendMessage(WM_CTRL_ALIGNCOILS);
	}

BOOL CStratonLDWnd::CanInsertContactBefore(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTCONTACTBEFORE));
	}

void CStratonLDWnd::InsertContactBefore(void)
{
	SendMessage(WM_CTRL_INSERTCONTACTBEFORE);
	}

BOOL CStratonLDWnd::CanInsertContactAfter(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTCONTACTAFTER));
	}

void CStratonLDWnd::InsertContactAfter(void)
{
	SendMessage(WM_CTRL_INSERTCONTACTAFTER);
	}

BOOL CStratonLDWnd::CanInsertContactParallel(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTCONTACTPARALLEL));
	}

void CStratonLDWnd::InsertContactParallel(void)
{
	SendMessage(WM_CTRL_INSERTCONTACTPARALLEL);
	}

BOOL CStratonLDWnd::CanInsertCoil(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTCOIL));
	}

void CStratonLDWnd::InsertCoil(void)
{
	SendMessage(WM_CTRL_INSERTCOIL);
	}

BOOL CStratonLDWnd::CanInsertFBBefore(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTFBBEFORE));
	}

void CStratonLDWnd::InsertFBBefore(void)
{
	SendMessage(WM_CTRL_INSERTFBBEFORE);
	}

BOOL CStratonLDWnd::CanInsertFBAfter(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTFBAFTER));
	}

void CStratonLDWnd::InsertFBAfter(void)
{
	SendMessage(WM_CTRL_INSERTFBAFTER);
	}

BOOL CStratonLDWnd::CanInsertFBParallel(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTFBPARALLEL));
	}

void CStratonLDWnd::InsertFBParallel(void)
{
	SendMessage(WM_CTRL_INSERTFBPARALLEL);
	}

BOOL CStratonLDWnd::CanInsertJump(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTJUMP));
	}

void CStratonLDWnd::InsertJump(void)
{
	SendMessage(WM_CTRL_INSERTJUMP);
	}

BOOL CStratonLDWnd::CanInsertRung(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTRUNG));
	}

void CStratonLDWnd::InsertRung(void)
{
	SendMessage(WM_CTRL_INSERTRUNG);
	}

BOOL CStratonLDWnd::CanInsertComment(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTCOMMENT));
	}

void CStratonLDWnd::InsertComment(void)
{
	SendMessage(WM_CTRL_INSERTCOMMENT);
	}

BOOL CStratonLDWnd::CanInsertHorz(void)
{
	return BOOL(SendMessage(WM_CTRL_CANINSERTHORZ));
	}

void CStratonLDWnd::InsertHorz(void)
{
	SendMessage(WM_CTRL_INSERTHORZ);
	}

void CStratonLDWnd::WrapRungs(BOOL fWrap)
{
	SendMessage(WM_CTRL_WRAPRUNGS, WPARAM(fWrap), LPARAM(0L));
	}

// End of File
