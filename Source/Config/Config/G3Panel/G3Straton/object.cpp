
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object
//

// Dynamic Class

AfxImplementDynamicClass(CStratonObject, CItem);

// Contructor

CStratonObject::CStratonObject(void)
{
	m_dwProject = 0;

	m_dwHandle  = 0;
	}

// Managemnet

void CStratonObject::SetProject(DWORD dwProject)
{
	m_dwProject = dwProject;
	}

// Attribute

DWORD CStratonObject::GetHandle(void)
{
	return m_dwHandle;
	}

DWORD CStratonObject::GetProject(void)
{
	return m_dwProject;
	}

// Meta Data Creation

void CStratonObject::AddMetaData(void)
{
	}

// End of File
