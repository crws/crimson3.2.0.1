
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Type
//

CStratonDataType::CStratonDataType(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Data Type Helper
//

CStratonDataTypeHelper::CStratonDataTypeHelper(DWORD dwProject)
{
	m_dwProject = dwProject;

	m_SkipUdFb  = FALSE;

	m_SkipStdFb = FALSE;
	}

// Configuration

void CStratonDataTypeHelper::SkipUdFb(void)
{
	m_SkipUdFb = TRUE;
	}

void CStratonDataTypeHelper::SkipStdFb(void)
{
	m_SkipStdFb = TRUE;
	}

// Operations

UINT CStratonDataTypeHelper::GetHandles(CLongArray &Handles)
{
	CLongArray List;

	UINT c;

	if( (c = afxDatabase->GetTypes(m_dwProject, List)) ) {

		for( UINT n = 0; n < c; n ++ ) {

			DWORD dwType = List[n];

			CStratonDataTypeDescriptor Desc(m_dwProject, dwType);

			if( Desc.IsBasic() ) {
				
				Handles.Append(dwType);

				continue;
				}

			if( !m_SkipUdFb && Desc.IsUdFb() ) {
				
				Handles.Append(dwType);
				
				continue;
				}

			if( !m_SkipStdFb && Desc.IsStdFb() ) {

				DWORD dwRegType = afxRegistry->FindBlock(Desc.m_Name);

				CFunctionDefinition Defn(dwRegType);

				if( Defn.CanLoad() ) {

					if( Handles.Find(dwType) < NOTHING ) {

						}

					Handles.Append(dwType);
					}

				continue;
				}
			}
		}

	return c;
	}

//////////////////////////////////////////////////////////////////////////
//
// Data Type Descriptor
//

CStratonDataTypeDescriptor::CStratonDataTypeDescriptor(DWORD dwProject, DWORD dwHandle)
{
	m_dwProject = dwProject;

	m_dwHandle = dwHandle;

	m_Name = afxDatabase->GetTypeDesc( dwProject, dwHandle, m_dwFlags);
	}

// Attributes

BOOL CStratonDataTypeDescriptor::IsBasic(void) const
{
	return m_dwFlags & K5DBTYPE_BASIC;
	}

BOOL CStratonDataTypeDescriptor::IsUdFb(void) const
{
	return m_dwFlags & K5DBTYPE_UDFB;
	}

BOOL CStratonDataTypeDescriptor::IsStdFb(void) const
{
	return m_dwFlags & K5DBTYPE_STDFB;
	}

BOOL CStratonDataTypeDescriptor::IsFb(void) const
{
	return m_dwFlags & K5DBTYPE_FB;
	}

BOOL CStratonDataTypeDescriptor::IsString(void) const
{
	return m_dwFlags & K5DBTYPE_STRING;
	}

BOOL CStratonDataTypeDescriptor::IsNumeric(void) const
{
	if( m_dwFlags & K5DBTYPE_BASIC ) {

		static const PCTXT List[] = {
			
			L"SINT",
			L"USINT",
			L"BYTE",
			L"INT",
			L"UINT",
			L"WORD",
			L"LWORD",
			L"DINT",
			L"UDINT",
			L"DWORD",
			L"LINT",
			L"ULINT",

			};

		CStringArray Types;

		for( UINT n = 0; n < elements(List); n ++ ) {
			
			Types.Append(List[n]);
			}		
		
		return Types.Find(m_Name) < NOTHING;
		}

	return FALSE;
	}

BOOL CStratonDataTypeDescriptor::IsReal(void) const
{
	if( m_dwFlags & K5DBTYPE_BASIC ) {
		
		return m_Name.EndsWith(L"REAL");
		}

	return FALSE;
	}

BOOL CStratonDataTypeDescriptor::IsBool(void) const
{
	if( m_dwFlags & K5DBTYPE_BASIC ) {
		
		return m_Name.EndsWith(L"BOOL");
		}

	return FALSE;
	}

BOOL CStratonDataTypeDescriptor::IsTime(void) const
{
	if( m_dwFlags & K5DBTYPE_BASIC ) {
		
		return m_Name.EndsWith(L"TIME");
		}

	return FALSE;
	}

BOOL CStratonDataTypeDescriptor::Is64Bit(void) const
{
	if( m_dwFlags & K5DBTYPE_BASIC ) {

		CStringArray Types(L"LREAL", L"LWORD", L"LINT", L"ULINT");
		
		return Types.Find(m_Name) < NOTHING;
		}

	return FALSE;
	}

BOOL CStratonDataTypeDescriptor::IsSigned(void) const
{
	if( m_dwFlags & K5DBTYPE_BASIC ) {

		static const PCTXT List[] = {
			
			L"SINT",
			L"INT",
			L"DINT",
			L"LINT",
			L"REAL",
			L"LREAL",

			};

		CStringArray Types;

		for( UINT n = 0; n < elements(List); n ++ ) {
			
			Types.Append(List[n]);
			}		
		
		return Types.Find(m_Name) < NOTHING;
		}

	return FALSE;
	}

BOOL CStratonDataTypeDescriptor::GetRange(INT64 &nMin, INT64 &nMax)
{
	if( IsNumeric() ) {

		struct CInfo {
		
			CString m_Name;

			INT64 m_nMin;

			INT64 m_nMax;
			};

		static const CInfo Info[] = {
		
			{ L"SINT",	       -128,	    127	},
			{ L"USINT",	          0,	    255	},
			{ L"BYTE",	          0,	    255	},
			{ L"INT",	     -32768,	  32767	},
			{ L"UINT",	          0,	  65535	},
			{ L"WORD",	          0,	  65535	},
			{ L"DINT",	-2147483647, 2147483647	},
			{ L"UDINT",	          0, 4294967295L },
			{ L"DWORD",	          0, 4294967295L },		

			};

		for( UINT n = 0; n < elements(Info); n ++ ) {

			CInfo const &Entry = Info[n];
		
			if( Entry.m_Name == m_Name ) {
			
				nMin = Entry.m_nMin;

				nMax = Entry.m_nMax;

				return TRUE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Public Data

// End of File
