
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function Block Diagram Window
//

// Dynamic Class

AfxImplementDynamicClass(CStratonFBDWnd, CStratonWnd);

// Constructor

CStratonFBDWnd::CStratonFBDWnd(void)
{
	m_pLib = CFBDLibrary::FindInstance();
	}

// FBD Control

BOOL CStratonFBDWnd::IsCheck(UINT uID)
{
	return BOOL(SendMessage(WM_CTRL_ISCHECK, WPARAM(uID), 0));
	}

BOOL CStratonFBDWnd::CanCheck(UINT uID)
{
	return BOOL(SendMessage(WM_CTRL_CANCHECK, WPARAM(uID), 0));	
	}

void CStratonFBDWnd::Check(UINT uID)
{
	SendMessage(WM_CTRL_CHECK, WPARAM(uID), 0);
	}

BOOL CStratonFBDWnd::CanDisplayFbdOrder(void)
{
	return BOOL(SendMessage(WM_CTRL_CANDISPLAYFBDORDER, 0, 0));
	}

DWORD CStratonFBDWnd::DisplayFbdOrder(void)
{
	return DWORD(SendMessage(WM_CTRL_DISPLAYFBDORDER, 0, 0));
	}

BOOL CStratonFBDWnd::CanCreatUdFb(void)
{
	return BOOL(SendCommand(L"cancreateudfb"));
	}

BOOL CStratonFBDWnd::CreatUdFb(void)
{
	return BOOL(SendCommand(L"createudfb"));
	}

BOOL CStratonFBDWnd::CanConnect(void)
{
	return BOOL(SendCommand(L"canconnect"));
	}

BOOL CStratonFBDWnd::Connect(void)
{
	return BOOL(SendCommand(L"connect"));
	}

BOOL CStratonFBDWnd::CanRotateCorners(void)
{
	return BOOL(SendCommand(L"canrotatecorners"));
	}

BOOL CStratonFBDWnd::RotateCorners(int nSym)
{
	return BOOL(SendCommand(L"rotatecorners%d", nSym));
	}

BOOL CStratonFBDWnd::SnapFbdGrid(BOOL fEnable)
{
	BOOL fSupported = BOOL(SendCommand(L"snapfbdgrid%b", fEnable));

	return fSupported;
	}

void CStratonFBDWnd::DrawFbdBridge(BOOL fEnable)
{
	SendMessage(WM_CTRL_DRAWFBDBRIDGE, WPARAM(fEnable), 0);
	}

// End of File
