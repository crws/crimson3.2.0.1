
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Support
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Directory Clean-Up
//

static void DeleteDirectory(CString Path)
{
	TCHAR path[MAX_PATH];

	memset (path, 0, sizeof(path));

	wstrcpy(path, Path);

	SHFILEOPSTRUCT shfo = {

		NULL,
		FO_DELETE,
		path,
		NULL,
		FOF_SILENT | FOF_NOERRORUI | FOF_NOCONFIRMATION,
		FALSE,
		NULL,
		NULL
		};

	SHFileOperation(&shfo);
	}

static void CleanProjectDirectory(void)
{
	CModule * pApp = afxModule->GetApp();

	CPrintf   Path = CPrintf( "%s%8.8X",
				  pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Control"),
				  GetCurrentProcessId()
				  );

	DeleteDirectory(Path);
	}

static void CleanOrphanProjects(void)
{
	CModule * pApp = afxModule->GetApp();

	CPrintf   Base = CPrintf("%s", pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Control"));

	CString   Spec = Base + L"*.*";

	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(Spec, &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			if( Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) {

				if( Data.cFileName[0] != '.' ) {

					PTXT  end = NULL;

					DWORD pid = wcstoul(Data.cFileName, &end, 16);

					if( !*end ) {

						HANDLE hFind = OpenProcess(GENERIC_READ, FALSE, pid);

						if( !hFind ) {

							DeleteDirectory(Base + Data.cFileName);
							}
						else
							CloseHandle(hFind);
						}
					}
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Init and Term Hooks
//

void DLLAPI StratonInit(void)
{
	CleanOrphanProjects();

	CleanProjectDirectory();

	New CStratonDatabase;

	New CStratonRegistry;

	New CStratonCompiler;

	New CCrossReference;

	New CEditResource;

	New CMiddleware;

	New CSFCLibrary;

	New CSTLibrary;

	New CFBDLibrary;

	New CLDLibrary;

//	Commented out to prevent UnSupBlocks.def from being built

//	CUnsupportedBlocks File;
	}

void DLLAPI StratonTerm(void)
{
	delete CStratonDatabase::FindInstance();

	delete CStratonRegistry::FindInstance();

	delete CStratonCompiler::FindInstance();

	delete CCrossReference::FindInstance();

	delete CEditResource::FindInstance();

	delete CMiddleware::FindInstance();

	delete CSFCLibrary::FindInstance();

	delete CSTLibrary::FindInstance();

	delete CFBDLibrary::FindInstance();

	delete CLDLibrary::FindInstance();

	CleanProjectDirectory();
	}

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Straton\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			CButtonBitmap *pTool16 = New CButtonBitmap( CBitmap(L"Tool16"),
								    CSize(16, 16),
								    55
								    );

			CButtonBitmap *pEdit16 = New CButtonBitmap( CBitmap(L"Edit16"),
								    CSize(16, 16),
								    24
								    );
			
			afxButton->AppendBitmap(0x6000, pTool16);

			afxButton->AppendBitmap(0x7000, pEdit16);

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Straton\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

void DLLAPI Fake(void)
{
	}

// End of File
