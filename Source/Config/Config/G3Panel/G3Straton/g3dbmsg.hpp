
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3DBMSG_HPP
	
#define	INCLUDE_G3DBMSG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Message Dispatch Macros
//

// Database Notifications

#define On_WM_DATABASE(n)			DPM(n,(void(CTP)(UINT,DWORD))&OnDatabase,"V-1L2L")
#define On_WM_DBBUILD(n)			DPM(n,(void(CTP)(UINT,DWORD))&OnDbBuild,"V-1L2L")
#define On_WM_MWEVENT(n)			DPM(n,(void(CTP)(UINT,DWORD))&OnMiddleware,"V-1L2L")

// From xls Notifications Page

#define	No_W5EDITN_DBLCLK(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_RCLICK(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SELCHANGE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5W2w")
#define	No_W5EDITN_EDITPROPS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SETVARFIRSTCHAR(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SETVAR(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SETFB(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_GETFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd&))&f,"T-5W2w")
#define	No_W5EDITN_COMPLETION(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_ADDITEM(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_REMOVEITEM(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_MODIFYITEM(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_LINKTO(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_BEGINDRAG(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_INITARRAYVAR(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_HEADERSIZECHANGED(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_HEADERSELCHANGED(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_HEADERDBLCLK(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_LISBLOCKCHANGED(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_MODIFIED(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_VSCROLL(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,int,CWnd &))&f,"T-5L6W2w")
#define	No_W5EDITN_HSCROLL(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,int,CWnd &))&f,"T-5L6W2w")
#define	No_W5EDITN_CHAR(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_KEYDOWN(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_RCLICKGRID(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_RCLICKLIST(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_UDFBTOUPDATE(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")

#define	No_W5EDITN_LISTKEYWORD(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_OPENUDFB(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SFCOPENDEFAULT(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SFCOPENNOTES(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SFCOPENP1(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SFCOPENN(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_SFCOPENP0(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_PROPCHANGED(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_RCLICKSYMBOL(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_RCLICKFB(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_COLCHANGE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_DROP(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")
#define	No_W5EDITN_ARRANGECOL(i1,i2,n,f)	DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,UINT))&f,"T-1W2L")

// End of File

#endif
