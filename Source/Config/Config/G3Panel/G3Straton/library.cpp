
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

// Constructors

CStratonLibrary::CStratonLibrary(PCTXT pName)
{
	m_pName = pName;
	}
		
// Command Execution

LRESULT CStratonLibrary::ExecuteCommand(HWND hWnd, CCommandBuilder &Cmd)
{
	return m_pfnExecuteCommand( hWnd, 
				    Cmd.GetCount(), 
				    Cmd.GetArgs()
				    );	
	}

// Class Definition

PCTXT CStratonLibrary::GetClassName(void) const
{
	return m_Class;
	}

void CStratonLibrary::SetString(DWORD dwID, PCTXT pText)
{
	if( m_pfnSetString ) {

		m_pfnSetString(dwID, LPCSTR(CAnsiString(pText)));
		}
	}

// Library Management

void CStratonLibrary::LoadLib(PCTXT pName)
{
	CFilename Path = afxModule->GetFilename().GetDirectory();

	CFilename Name = Path + L"Straton\\" + pName;

	if( (m_hLib = LoadLibrary(Name)) ) {

		m_pfnGetClassName	= (PK5GETCLASSNAME)   GetProcAddress(m_hLib,	"K5GetClassName");

		m_pfnExecuteCommand	= (PK5EXECUTECOMMAND) GetProcAddress(m_hLib,	"K5ExecuteCommand");

		m_pfnSetString		= (PK5SETSTRING)      GetProcAddress(m_hLib,	"K5SetString");

		AfxAssert(m_pfnGetClassName && m_pfnExecuteCommand /*&& m_pfnSetString*/);

		m_Class = CString(m_pfnGetClassName());

		return;
		}

	DWORD dwError = GetLastError();

	AfxTrace(L"ERROR: Failed to load %s\t%8.8X\n", pName, dwError);

	AfxAssert(m_hLib);
	}

void CStratonLibrary::FreeLib(void)
{
	AfxVerify(FreeLibrary(m_hLib));
	}

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

// Static Data

CStratonLibrary * CSFCLibrary::m_pThis = NULL;

// Object Location

CStratonLibrary * CSFCLibrary::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructors

CSFCLibrary::CSFCLibrary(void) : CStratonLibrary(L"W5EditSFC")
{
	AfxAssert(m_pThis == NULL);

	LoadLib(m_pName);

	m_pThis = this;
	}

// Destructors

CSFCLibrary::~CSFCLibrary(void)
{
	AfxAssert(m_pThis == this);

	m_pThis = NULL;

	FreeLib();
	}

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

// Static Data

CStratonLibrary * CSTLibrary::m_pThis = NULL;

// Object Location

CStratonLibrary * CSTLibrary::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructors

CSTLibrary::CSTLibrary(void) : CStratonLibrary(L"W5EditST")
{
	AfxAssert(m_pThis == NULL);

	LoadLib(m_pName);

	m_pThis = this;
	}

// Destructors

CSTLibrary::~CSTLibrary(void)
{
	AfxAssert(m_pThis == this);

	m_pThis = NULL;

	FreeLib();
	}

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

// Static Data

CStratonLibrary * CFBDLibrary::m_pThis = NULL;

// Object Location

CStratonLibrary * CFBDLibrary::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructors

CFBDLibrary::CFBDLibrary(void) : CStratonLibrary(L"W5EditFBD")
{
	AfxAssert(m_pThis == NULL);

	LoadLib(m_pName);

	m_pThis = this;
	}

// Destructors

CFBDLibrary::~CFBDLibrary(void)
{
	AfxAssert(m_pThis == this);

	m_pThis = NULL;

	FreeLib();
	}

//////////////////////////////////////////////////////////////////////////
//
// Straton Library
//

// Static Data

CStratonLibrary * CLDLibrary::m_pThis = NULL;

// Object Location

CStratonLibrary * CLDLibrary::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructors

CLDLibrary::CLDLibrary(void) : CStratonLibrary(L"W5EditLD")
{
	AfxAssert(m_pThis == NULL);

	LoadLib(m_pName);

	m_pThis = this;
	}

// Destructors

CLDLibrary::~CLDLibrary(void)
{
	AfxAssert(m_pThis == this);

	m_pThis = NULL;

	FreeLib();
	}

// End of File
