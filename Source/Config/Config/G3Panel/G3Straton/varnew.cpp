
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Variable Create Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CVariableCreateDialog, CStdDialog);

// Static Data

CString	CVariableCreateDialog::m_Name	= CString();

DWORD	CVariableCreateDialog::m_Type	= 0;

DWORD	CVariableCreateDialog::m_Attr	= 0;

DWORD	CVariableCreateDialog::m_Group	= 0;

// Constructor

CVariableCreateDialog::CVariableCreateDialog(DWORD dwProject)
{
	m_dwProject = dwProject;

	m_Group     = 0;

	m_Attr      = 0;

	FindFormat();

	SetName(L"VariableCreateDialog");
	}

CVariableCreateDialog::CVariableCreateDialog(DWORD dwProject, DWORD dwParent)
{
	m_dwProject = dwProject;

	m_Group     = dwParent;

	m_Attr      = 0;

	FindFormat();

	SetName(L"VariableCreateDialog4");
	}

CVariableCreateDialog::CVariableCreateDialog(DWORD dwProject, DWORD dwParent, DWORD dwAttr)
{
	m_dwProject = dwProject;

	m_Group     = dwParent;

	m_Attr      = dwAttr;

	FindFormat();

	SetName(L"VariableCreateDialog");
	}

CVariableCreateDialog::CVariableCreateDialog(DWORD dwProject, DWORD dwParent, CString Name)
{
	m_Name	    = Name;

	m_dwProject = dwProject;

	m_Group     = dwParent;

	m_Attr      = 0;

	SetName(L"VariableCreateDialog4");
	}

// Attributes

CString CVariableCreateDialog::GetName(void)
{
	return m_Name;
	}

DWORD CVariableCreateDialog::GetType(void)
{
	return m_Type;
	}

DWORD CVariableCreateDialog::GetAttr(void)
{
	return m_Attr;
	}

DWORD CVariableCreateDialog::GetGroup(void)
{
	return m_Group;
	}

// Message Map

AfxMessageMap(CVariableCreateDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PREDESTROY)

	AfxDispatchCommand(IDOK, OnCommandYes)
	AfxDispatchCommand(IDNO, OnCommandNo)

	AfxMessageEnd(CVariableCreateDialog)
	};

// Message Handlers

BOOL CVariableCreateDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadConfig();

	LoadCaption();

	LoadName();

	LoadType();

	LoadAttr();

	LoadGroup();

	return TRUE;
	}

void CVariableCreateDialog::OnPreDestroy(void)
{
	SaveConfig();
	}

// Command Handlers

BOOL CVariableCreateDialog::OnCommandYes(UINT uID)
{
	if( ReadName() ) {
		
		ReadType();

		ReadAttr();

		ReadGroup();

		EndDialog(1);
		}

	return TRUE;
	}

BOOL CVariableCreateDialog::OnCommandNo(UINT uID)
{
	EndDialog(0);

	return TRUE;
	}

// Implementation

void CVariableCreateDialog::LoadCaption(void)
{
	CStratonGroupDescriptor Desc(m_dwProject, m_Group);

	CString Text;

	Text += CString(IDS_CREATE_NEW_2);

	if( m_Attr ) {
		
		Text += CString(IDS_PARAMETER);
		}
	else {
		Text += Desc.GetStyleName();

		Text += L" ";
	
		Text += CString(IDS_VARIABLE);
		}

	#if defined(_DEBUG)

	Text += CString(IDS_IN);

	Text += Desc.m_Name;

	#endif

	SetWindowText(Text);
	}

void CVariableCreateDialog::LoadName(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	CLongArray Global;

	CStratonGroupHelper(m_dwProject).GetHandles(Global);

	BOOL fGlobal = Global.Find(m_Group) < NOTHING;

	for( UINT n = 1;; n++ ) {

		CFormat Name(m_pFormat, n);

		if( fGlobal ) {

			if( !afxDatabase->FindVarExact(m_dwProject, Name, 0, 0) ){

				Edit.SetWindowText(Name);

				break;
				}
			}
		else {
			if( !afxDatabase->FindVarInGroup(m_dwProject, m_Group, Name) ){

				Edit.SetWindowText(Name);

				break;
				}
			}
		}
	}

void CVariableCreateDialog::LoadType(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(201);

	CLongArray Types;

	CStratonDataTypeHelper Help(m_dwProject);

	#pragma warning(suppress: 6286)

	if( TRUE || IsParam() ) {

		Help.SkipUdFb();

		Help.SkipStdFb();
		}

	if( Help.GetHandles(Types) ) {
	
		UINT c;
	
		if( (c = Types.GetCount()) ) {

			for( UINT n = 0; n < c; n ++ ) {
		
				CStratonDataTypeDescriptor Desc(m_dwProject, Types[n]);

/*				if( IsParam() ) {
					
					if( !Desc.IsBasic() ) {
						
						continue;
						}				
					}
*/
				List.AddString(Desc.m_Name,	Desc.m_dwHandle);
				}		
			}
		}
	else {
		List.AddString(CString(IDS_UNKNOWN),	0);
		}

	CStratonDataTypeDescriptor Type(m_dwProject, m_Type);

	List.SelectString(Type.m_Name);
	}

void CVariableCreateDialog::LoadAttr(void)
{
	CStratonGroupDescriptor Desc(m_dwProject, m_Group);

	if( IsParam() ) {

		CComboBox &List = (CComboBox &) GetDlgItem(202);

		List.AddString(CString(IDS_INPUT), K5DBVAR_FBINPUT);
	
		List.AddString(CString(IDS_INPUTOUTPUT_2), K5DBVAR_FBINPUT | K5DBVAR_INOUT);

		List.AddString(CString(IDS_OUTPUT), K5DBVAR_FBOUTPUT);

		List.SetCurSel(0);
		}
	else {
		CComboBox &List = (CComboBox &) GetDlgItem(202);

		List.AddString(CString(IDS_READ_AND_WRITE), 0);

		List.AddString(CString(IDS_READ_ONLY), K5DBVAR_READONLY);
		
		List.SetCurSel(0);
		}
	}

void CVariableCreateDialog::LoadGroup(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(203);

	DWORD   dwGlobal = afxDatabase->FindGroup(m_dwProject, CString(K5DBGROUPNAME_GLOBAL));
	
	CStratonGroupDescriptor Global(m_dwProject, dwGlobal);
	
	CStratonGroupDescriptor Local(m_dwProject, m_Group);

	List.AddString(Global.GetStyleName(),	Global.m_dwHandle);
	
	List.AddString(Local.m_Name,		Local.m_dwHandle);

	List.SelectData(m_Group ? m_Group : dwGlobal);
	}

BOOL CVariableCreateDialog::ReadName(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	CString Name = Edit.GetWindowText();

	CVariableNamer Var(m_dwProject, m_Group);

	CError Error(TRUE);

	if( Var.Validate(Error, Name) ) {
		
		if( !Var.CanFindName(Error, Name) ) {

			m_Name = Name;

			return TRUE;
			}
		}

	Error.Show(*afxMainWnd);

	return FALSE;
	}

BOOL CVariableCreateDialog::ReadType(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(201);

	m_Type = List.GetCurSelData();

	return TRUE;
	}

BOOL CVariableCreateDialog::ReadAttr(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(202);

	m_Attr = List.GetCurSelData();

	return TRUE;
	}

BOOL CVariableCreateDialog::ReadGroup(void)
{
	CComboBox &List = (CComboBox &) GetDlgItem(203);

	m_Group = List.GetCurSelData();

	return TRUE;
	}

void CVariableCreateDialog::FindFormat(void)
{
	CStratonGroupDescriptor Group(m_dwProject, m_Group);

	switch( Group.m_dwStyle ) {
		
		case K5DBGROUP_GLOBAL:
		case K5DBGROUP_RETAIN:

			m_pFormat = L"Variable%1!u!";

			break;

		case K5DBGROUP_LOCAL:
		case K5DBGROUP_UDFB:

			if( m_Attr == K5DBVAR_FBINPUT ) {
					
				m_pFormat = L"Input%1!u!";
					
				break;
				}

			if( m_Attr == K5DBVAR_FBOUTPUT ) {
					
				m_pFormat = L"Output%1!u!";
					
				break;
				}

			if( m_Attr == (K5DBVAR_FBOUTPUT | K5DBVAR_INOUT) ) {
					
				m_pFormat = L"InOut%1!u!";
					
				break;
				}

			m_pFormat = L"Local%1!u!";

			break;

		default:

			m_pFormat = L"Other%1!u!";

			break;
		}
	}

BOOL CVariableCreateDialog::IsParam(void)
{
	return m_Attr > 0;
	}

void CVariableCreateDialog::LoadConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(L"NewVariable");

	m_Type = Reg.GetValue(L"Type", afxDatabase->FindType(m_dwProject, L"BOOL"));

	//
	CStratonDataTypeHelper Help(m_dwProject);
	
	if( IsParam() ) {

		Help.SkipUdFb();

		Help.SkipStdFb();
		}

	CLongArray List;

	if( Help.GetHandles(List) ) {		
		
		if( List.Find(m_Type) == NOTHING ) {

			m_Type = afxDatabase->FindType(m_dwProject, L"BOOL");
			}
		}
	}

void CVariableCreateDialog::SaveConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(L"NewVariable");

	Reg.SetValue(L"Type",  UINT(GetType()));
	}

// End of File
