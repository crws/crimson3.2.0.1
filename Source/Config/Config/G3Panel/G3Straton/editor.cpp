
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Editor Window
//

// Dynamic Class

AfxImplementDynamicClass(CEditorWnd, CViewWnd);

// Constructor

CEditorWnd::CEditorWnd(void)
{
	m_pEdit      = NULL;

	m_dwProject  = 0;

	m_cfVariable = RegisterClipboardFormat(L"CF_K5DBEDIT");

	m_fDirty     = FALSE;
	}

// Editor Access

CStratonWnd & CEditorWnd::GetEditor(void)
{
	return *m_pEdit;
	}

// Attributes

BOOL CEditorWnd::IsDirty(void) const
{
	return m_fDirty;
	}

// Operations

void CEditorWnd::Attach(CItem *pItem)
{
	m_pItem = pItem;
	}

void CEditorWnd::Attach(DWORD dwProject, DWORD dwProgram)
{
	Commit();

	m_dwProject = dwProject;
	
	m_dwProgram = dwProgram;

	CString ProjectPath = afxDatabase->GetProjectPath(dwProject);

	CString ProgramPath = afxDatabase->GetProgramPath(dwProject, dwProgram);

	CStratonWnd &Wnd = GetEditor();

	Wnd.SetProjectPath(ProjectPath);

	Wnd.SetID(dwProgram);

	Wnd.Load(ProgramPath);

	m_Prev = Wnd.GetText();
	}

void CEditorWnd::SetDirty(void)
{
	m_fDirty = TRUE;
	}

void CEditorWnd::Commit(void)
{
	if( m_fDirty ) {

		if( m_dwProject ) {

			CStratonWnd &Wnd = GetEditor();

			CString     Path = afxDatabase->GetProgramPath(m_dwProject, m_dwProgram);

			Wnd.Save(Path);

			afxDatabase->SaveProgramChanges(m_dwProject, m_dwProgram);
			}

		m_fDirty = FALSE;
		}
	}

void CEditorWnd::Reload(void)
{
	AfxAssert(!m_fDirty);

	CString ProgramPath = afxDatabase->GetProgramPath(m_dwProject, m_dwProgram);

	CStratonWnd &Wnd = GetEditor();

	Wnd.Load(ProgramPath);

	m_Prev = Wnd.GetText();
	}

// Message Map

AfxMessageMap(CEditorWnd, CViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_PRINT)

	AfxDispatchGetInfoType(IDM_EDIT,  OnEditGetInfo )
	AfxDispatchControlType(IDM_EDIT,  OnEditControl )
	AfxDispatchCommandType(IDM_EDIT,  OnEditCommand )

	AfxDispatchGetInfoType(IDM_VIEW,  OnViewGetInfo )
	AfxDispatchControlType(IDM_VIEW,  OnViewControl )
	AfxDispatchCommandType(IDM_VIEW,  OnViewCommand )

	AfxDispatchGetInfoType(IDM_TOOL,  OnToolGetInfo )
	AfxDispatchControlType(IDM_TOOL,  OnToolControl )
	AfxDispatchCommandType(IDM_TOOL,  OnToolCommand )

	AfxDispatchNotify(100, W5EDITN_MODIFIED,	OnModified )
	AfxDispatchNotify(100, W5EDITN_RCLICK,		OnRClick   )
	AfxDispatchNotify(100, W5EDITN_SETVAR,		OnSetVar   )
	AfxDispatchNotify(100, W5EDITN_SETFB,		OnSetFb    )
	AfxDispatchNotify(100, W5EDITN_GETFOCUS,	OnGetFocus )
	AfxDispatchNotify(100, W5EDITN_EDITPROPS,	OnEditProps)

	AfxMessageEnd(CEditorWnd)
	};

// Message Handlers

void CEditorWnd::OnPostCreate(void)
{
	if( m_pEdit ) {

		m_pEdit->Create(ThisObject);

		CString Name = CString(GetClassName()).Mid(1);

		m_MenuCntx = Name;
		
		m_MenuMain = Name;
		
		m_ToolMain = Name;

		m_MenuCntx.Replace(L"Wnd", L"Cntx");

		m_MenuMain.Replace(L"Wnd", L"Menu");

		m_ToolMain.Replace(L"Wnd", L"Tool");

		LoadConfig();

		LoadSettings();
		}
	}

void CEditorWnd::OnPreDestroy(void)
{
	SaveConfig();
	}

void CEditorWnd::OnSize(UINT uCode, CSize Size)
{
	if( m_pEdit ) {

		m_pEdit->MoveWindow(GetClientRect(), TRUE);
		}
	}

void CEditorWnd::OnSetFocus(CWnd &Prev)
{
	m_pEdit->SetFocus();
	}

void CEditorWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {

		Menu.MergeMenu(CMenu(PCTXT(m_MenuMain)));
		}
	}

void CEditorWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {

		Menu.AppendMenu(CMenu(PCTXT(m_ToolMain)));
		}
	}

void CEditorWnd::OnPrint(CDC &DC, UINT uFlags)
{
	CStratonWnd &Wnd = GetEditor();

	if( !(uFlags & PRF_CHECKVISIBLE) || IsWindowVisible() ) {

		CRect Rect = Wnd.GetWindowRect();

		ScreenToClient(Rect);

		Wnd.Paint(DC.GetHandle(), Rect);
		}
	}

// Edit Command Handlers

BOOL CEditorWnd::OnEditGetInfo(UINT uID, CCmdInfo &Info)
{
	return FALSE;
	}

BOOL CEditorWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	CStratonWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_EDIT_DELETE:

			Src.EnableItem(!Wnd.IsDebug() && Wnd.HasSelection());

			break;

		case IDM_EDIT_CUT:

			Src.EnableItem(!Wnd.IsDebug() && Wnd.CanCut());			

			break;

		case IDM_EDIT_COPY:			

			Src.EnableItem(!Wnd.IsDebug() && Wnd.CanCopy());			

			break;

		case IDM_EDIT_PASTE:			

			Src.EnableItem(!Wnd.IsDebug() && Wnd.CanPaste());

			break;

		case IDM_EDIT_SELECT_ALL:

			Src.EnableItem(!Wnd.IsEmpty() && Wnd.CanSelectAll());

			break;

		case IDM_EDIT_PROPERTIES:

			Src.EnableItem(CanEditProperties());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::OnEditCommand(UINT uID)
{
	CStratonWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_EDIT_DELETE:

			Wnd.Cut();

			break;

		case IDM_EDIT_CUT:

			Wnd.Cut();

			break;

		case IDM_EDIT_COPY:

			Wnd.Copy();

			break;

		case IDM_EDIT_PASTE:
			
			Wnd.Paste();

			break;
			
		case IDM_EDIT_SELECT_ALL:

			Wnd.SelectAll();

			break;

		case IDM_EDIT_PROPERTIES:
			
			OnEditProperties();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::CanEditProperties(void)
{
	CStratonWnd &Wnd = GetEditor();

	DWORD dwType = Wnd.GetItemType();

	if( IsFunction(dwType) ) {

		CFunctionBlock FB;
		
		Wnd.GetFB(FB);	

		return afxRegistry->BlockCanBeExtended(FB.m_Name);
		}

	return FALSE;
	}

BOOL CEditorWnd::OnEditProperties(void)
{
	CStratonWnd &Wnd = GetEditor();

	CFunctionBlock FB;

	Wnd.GetFB(FB);

	CBlockPropertiesDialog Dlg(FB);

	if( Dlg.Execute(*afxMainWnd) ) {

		Wnd.SetFB(FB);

		afxDatabase->SaveProgramChanges(m_dwProject, m_dwProgram);
		
		return TRUE;
		}

	return FALSE;
	}

// View Command Handlers

BOOL CEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_VIEW_ZOOM_MORE,	MAKELONG(0x0004, 0x7000),
		IDM_VIEW_ZOOM_LESS,	MAKELONG(0x0005, 0x7000),
		IDM_VIEW_GRID_SHOW,	MAKELONG(0x0003, 0x7000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	CStratonWnd &Wnd = GetEditor();

	if( uID >= IDM_VIEW_ZOOM_16_1 && uID <= IDM_VIEW_ZOOM_1_16 ) {

		INT const nZoom[] = {
			
			16000,
			 8000,
			 4000,
			 2000,
			 1000,
			  500,
			  250,
			  125,
			   76

			};

		Src.EnableItem(Wnd.CanSetZoom());

		Src.CheckItem(Wnd.GetZoom() == nZoom[uID - IDM_VIEW_ZOOM_16_1] / 10);

		return TRUE;
		}

	switch( uID ) {

		case IDM_VIEW_GRID_SHOW:

			Src.EnableItem(Wnd.CanSetGrid());

			Src.CheckItem( Wnd.IsGridVisible());
			
			break;

		case IDM_VIEW_ZOOM_MORE:
		case IDM_VIEW_ZOOM_LESS:
		case IDM_VIEW_ZOOM_FIT:

			Src.EnableItem(Wnd.CanSetZoom());

			break;

		case IDM_VIEW_ZOOM_FITSEL:

			Src.EnableItem(Wnd.CanSetZoom() && Wnd.HasSelection());

			break;

		case IDM_VIEW_SWAP_HEX:

			if( Wnd.IsDebug() ) {

				CString Path = Wnd.GetProjectPath();

				COfflineSimulator Simulate;

				if( Simulate.FindProject(Path) ) {

					Src.EnableItem(Wnd.IsDebug());

					Src.CheckItem(Simulate.GetHexMode());
					}
				}

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CEditorWnd::OnViewCommand(UINT uID)
{
	CStratonWnd &Wnd = GetEditor();

	if( uID >= IDM_VIEW_ZOOM_16_1 && uID <= IDM_VIEW_ZOOM_1_16 ) {

		INT const nZoom[] = {
			
			16000,
			 8000,
			 4000,
			 2000,
			 1000,
			  500,
			  250,
			  125,
			   76

			};

		Wnd.SetZoom(W5ZOOM_AT, nZoom[uID - IDM_VIEW_ZOOM_16_1] / 10);

		ShowDefaultStatus();

		return TRUE;
		}

	switch( uID ) {

		case IDM_VIEW_GRID_SHOW:

			Wnd.SetGrid(!Wnd.IsGridVisible());
			
			break;

		case IDM_VIEW_ZOOM_MORE:
			
			Wnd.SetZoom(W5ZOOM_IN, 0);
			
			ShowDefaultStatus();
			
			break;

		case IDM_VIEW_ZOOM_LESS:
			
			Wnd.SetZoom(W5ZOOM_OUT, 0);
			
			ShowDefaultStatus();
			
			break;

		case IDM_VIEW_ZOOM_FIT:
			
			Wnd.SetZoom(W5ZOOM_FIT, 0);
			
			ShowDefaultStatus();

			break;

		case IDM_VIEW_ZOOM_FITSEL:
			
			Wnd.SetZoom(W5ZOOM_FITSEL, 0);
			
			ShowDefaultStatus();

			break;

		case IDM_VIEW_SWAP_HEX:

			if( Wnd.IsDebug() ) {

				CString Path = Wnd.GetProjectPath();

				COfflineSimulator Simulate;

				if( Simulate.FindProject(Path) ) {					

					Simulate.SetHexMode(!Simulate.GetHexMode());
					}
				}

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

// Tool Command Handlers

BOOL CEditorWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return FALSE;
	}

BOOL CEditorWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	CStratonWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_CREATE_GLOBAL:

			Src.EnableItem(TRUE);

			Src.CheckItem(m_fCreateGlobal);

			break;

		default:		
			return FALSE;
		}

	AfxTouch(Wnd);
	
	return TRUE;
	}

BOOL CEditorWnd::OnToolCommand(UINT uID)
{
	CStratonWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_CREATE_GLOBAL:

			m_fCreateGlobal = !m_fCreateGlobal;

			break;

		default:		
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

// Notification Handlers

void CEditorWnd::OnModified(UINT uID, UINT uData)
{
	CStratonWnd &Wnd = GetEditor();

	if( Wnd.IsModified() ) {

		CString Text = Wnd.GetText();

		if( Text.CompareC(m_Prev) ) {
			
			CSysProxy Proxy;

			Proxy.Bind(this);

			CString Item = Proxy.GetNavPos();

			// We should really find a way to store the selection
			// before and after the edit in here or we will not have
			// a perfect undo behavior...

			CCmd   *pCmd = New CCmdEdit(Item, m_Prev, Text);

			Proxy.SaveCmd(pCmd);

			m_pItem->SetDirty();

			m_Prev   = Text;

			m_fDirty = TRUE;
			}
		}	
	}

void CEditorWnd::OnRClick(UINT uID, UINT uCode)
{
	SetFocus();

	CMenu Load = CMenu(PCTXT(m_MenuCntx));

	if( Load.IsValid() ) {

		CMenu &Menu = Load.GetSubMenu(0);

		Menu.SendInitMessage();

		Menu.DeleteDisabled();

		Menu.MakeOwnerDraw(FALSE);
		
		Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
				     GetCursorPos(),
				     afxMainWnd->GetHandle()
				     );

		Menu.FreeOwnerDraw();
		}
	}

void CEditorWnd::OnSetVar(UINT uID, UINT uCode)
{
	SelectVariable();
	}

void CEditorWnd::OnEditProps(UINT uID, UINT uCode)
{
	}

void CEditorWnd::OnSetFb(UINT uID, UINT uCode)
{
	CanEditProperties() && OnEditProperties();
	}

void CEditorWnd::OnGetFocus(UINT uID, CWnd &Wnd)
{
//	ShowDefaultStatus();
	}

// Implementation

void CEditorWnd::ShowDefaultStatus(void)
{
	CStratonWnd &Wnd = GetEditor();

	if( !Wnd.IsDebug() ) {

		CString Text;

		if( TRUE ) {
		
			Text += CPrintf(CString(IDS_ZOOM_FMT), Wnd.GetZoom());

			Text += L"  ";
			}

		if( Wnd.HasSelection() ) {

			CPoint Pos = Wnd.GetSelPos();

			CSize Size = Wnd.GetSelSize();

			CRect Rect(Pos, Size);

			Text += CPrintf(CString(IDS_POSITION_FMT), Rect.left, Rect.top, Rect.right, Rect.bottom);

			Text += L"  ";

			Text += CPrintf(CString(IDS_SIZE_FMT), Size.cx, Size.cy);

			Text += L"  ";

			DWORD dwType = Wnd.GetItemType();

			if( IsFunction(dwType) ) {

				CString   Type = Wnd.GetTypeName();

				Text += CPrintf(CString(IDS_FUNCTION_SELECTED), Type);

				Text += L"  ";
				}

			if( IsVariable(dwType) || IsCoil(dwType) || IsContact(dwType) || IsInOut(dwType) ) {

				CString Symbol = Wnd.GetSymbolName();

				if( IsVariable(dwType) ) {

					Text += CString(IDS_VARIABLE_SELECTED);
					}

				if( IsCoil(dwType) ) {

					Text += CString(IDS_COIL_SELECTED);
					}
			
				if( IsContact(dwType) ) {

					Text += CString(IDS_CONTACT_SELECTED);
					}
			
				if( IsInOut(dwType) ) {

					Text += CString(IDS_INPUTOUTPUT);
					}

				if( !Symbol.IsEmpty() ) {

					Text += L"  (";

					Text += Symbol;

					Text += L")";
					}

				Text += L"  ";
				}

			if( IsComment(dwType) ) {

				}
			}
		else {
			CPoint Pos = Wnd.GetCurPos();

			Text += CPrintf(CString(IDS_POSITION_FMT_2), Pos.x, Pos.y);		

			Text += L"  ";
			}

		afxThread->SetStatusText(Text);
		}	
	}

void CEditorWnd::LoadConfig(void)
{
	CStratonWnd &Wnd = GetEditor();

	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(GetClassName());

	m_fInsertVarWhenInsertFB = (Reg.GetValue(L"InsertVarWhenInsertFB", UINT(FALSE)));

	m_fUnderlineGlobal	 = (Reg.GetValue(L"UnderlineGlobal",	   UINT(FALSE)));

	m_fKeepFbdSelect         = (Reg.GetValue(L"KeepFbdSelect",	   UINT(FALSE)));

	Wnd.SetGrid		   (Reg.GetValue(L"GridShow",	           UINT(TRUE)));

	Wnd.SetZoom                (Reg.GetValue(L"Zoom", UINT(100)));

	//
	Wnd.InsertVarWhenInsertFB(m_fInsertVarWhenInsertFB);

	Wnd.UnderlineGlobal(m_fUnderlineGlobal);

	m_fCreateGlobal		= (Reg.GetValue(L"CreateGlobal",           UINT(TRUE)));

	m_fApplyOptions		= (Reg.GetValue(L"ApplyOptions",           UINT(FALSE)));

	m_dwTypeHint		= Reg.GetValue(L"TypeHint", afxDatabase->FindType(m_dwProject, L"BOOL"));

	CLongArray List;

	if( afxDatabase->GetTypes(m_dwProject, List) ) {		
		
		if( List.Find(m_dwTypeHint) == NOTHING ) {

			m_dwTypeHint = afxDatabase->FindType(m_dwProject, L"BOOL");
			}
		}
	}

void CEditorWnd::SaveConfig(void)
{
	CStratonWnd &Wnd = GetEditor();

	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(GetClassName());

	Reg.SetValue(L"InsertVarWhenInsertFB",  m_fInsertVarWhenInsertFB);

	Reg.SetValue(L"UnderlineGlobal",	m_fUnderlineGlobal);

	Reg.SetValue(L"KeepFbdSelect",		m_fKeepFbdSelect);

	Reg.SetValue(L"GridShow",		Wnd.IsGridVisible());	

	Reg.SetValue(L"Zoom",			Wnd.GetZoom());	
	
	Reg.SetValue(L"CreateGlobal",		m_fCreateGlobal);

	Reg.SetValue(L"ApplyOptions",		m_fApplyOptions);
	}

void CEditorWnd::LoadSettings(void)
{
	CStratonWnd &Wnd = GetEditor();

	Wnd.AutoDeclareInst(TRUE);
	}

void CEditorWnd::SelectVariable(CString Name)
{
	CStratonWnd &Wnd = GetEditor();

	DWORD   dwLocal = afxDatabase->FindGroup(m_dwProject, Wnd.GetProgramName());

	DWORD dwOptions = K5DBVSEL_CREATEVAR    |
			  K5DBVSEL_FOCUSEDIT    |
			  K5DBVSEL_DEFAULTPOS   |
			  K5DBVSEL_SHOWALWAYS   ;

	if( m_fCreateGlobal ) {
		
		dwOptions |= K5DBVSEL_CREATEGLOBAL;
		}

	if( m_fApplyOptions ) {

		dwOptions |= K5DBVSEL_APPLYOPTIONS;
		}

	CVarSelCtx Ctx;

	Ctx.m_dwParent  = dwLocal;
	Ctx.m_dwType    = m_dwTypeHint;
	Ctx.m_hWnd      = GetHandle();
	Ctx.m_Pos       = CPoint();
	Ctx.m_dwOptions = dwOptions;

	DWORD   dwIdent = 0;

	if( afxDatabase->SelectVar(m_dwProject, Name, Ctx, dwIdent) ) {

		CString Buff = afxDatabase->GetSerBuffer();

		Wnd.InsertSymbol(Buff);

		afxDatabase->ReleaseSerBuffer();

		CStratonVariableDescriptor Desc(m_dwProject, dwIdent);
		
		m_dwTypeHint = Desc.m_dwType;

		Wnd.Invalidate(FALSE);
		}
	}

void CEditorWnd::SelectVariable(void)
{
	CStratonWnd &Wnd = GetEditor();

	CString Name = Wnd.GetSymbolName();

	if( Name.IsEmpty() ) {

		DWORD      dwLocal = afxDatabase->FindGroup(m_dwProject, Wnd.GetProgramName());

		for( UINT n = 1;; n ++ ) {

			if( m_fCreateGlobal ) {

				CFormat Hint(L"Variable%1!u!", n);
			
				if( !afxDatabase->FindVarExact(m_dwProject, Hint, 0, 0) ) {

					SelectVariable(Hint);
				
					return;
					}
				}
			else {
				CFormat Hint(L"Local%1!u!", n);
			
				if( !afxDatabase->FindVarInGroup(m_dwProject, dwLocal, Hint) ) {

					SelectVariable(Hint);
				
					return;
					}
				}
			}
		}
	
	SelectVariable(Name);
	}

BOOL CEditorWnd::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

BOOL CEditorWnd::IsComment(DWORD dwType)
{
	switch( dwType ) {

		case W5TYPE_COMMENT:

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::IsCoil(DWORD dwType)
{
	switch( dwType ) {

		case W5TYPE_COIL:
		case W5TYPE_COIL_I:
		case W5TYPE_COIL_R:
		case W5TYPE_COIL_S:
		case W5TYPE_COIL_P:
		case W5TYPE_COIL_N:

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::IsContact(DWORD dwType)
{
	switch( dwType ) {

		case W5TYPE_CONTACT:
		case W5TYPE_CONTACT_I:
		case W5TYPE_CONTACT_P:
		case W5TYPE_CONTACT_N:
		case W5TYPE_CONTACT_IP:
		case W5TYPE_CONTACT_IN:

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::IsVariable(DWORD dwType)
{
	switch( dwType ) {

		case W5TYPE_VARIABLE:
		case W5TYPE_BLOCK_INI:
		case W5TYPE_BLOCK_IN:
		case W5TYPE_BLOCK_OUTI:
		case W5TYPE_BLOCK_OUT:

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::IsInOut(DWORD dwType)
{
	switch( dwType ) {

		case W5TYPE_BLOCK_INI:
		case W5TYPE_BLOCK_IN:
		case W5TYPE_BLOCK_OUTI:
		case W5TYPE_BLOCK_OUT:

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CEditorWnd::IsFunction(DWORD dwType)
{
	switch( dwType ) {

		case W5TYPE_BLOCK:

			break;
		
		default:
			return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Project Item View -- Move Command
//

// Runtime Class

AfxImplementRuntimeClass(CEditorWnd::CCmdEdit, CCmd);

// Constructor

CEditorWnd::CCmdEdit::CCmdEdit(CString Item, CString Prev, CString Text)
{
	m_Menu = CString(IDS_CHANGE_SOURCE);

	m_Item = Item;

	Prev.GetDelta(m_Delta, Text);
	}

// End of File
