
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Block Properties Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBlockPropertiesDialog, CStdDialog);

// Constructor

CBlockPropertiesDialog::CBlockPropertiesDialog(CFunctionBlock &FB)
{
	m_pFB = &FB;

	SetName(L"BlockPropertiesDialog");
	}

// Message Map

AfxMessageMap(CBlockPropertiesDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,  OnCommandYes)
	AfxDispatchCommand(IDNO,  OnCommandNo)

	AfxMessageEnd(CBlockPropertiesDialog)
	};

// Message Handlers

BOOL CBlockPropertiesDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadCaption();

	LoadInputs();

	return TRUE;
	}

// Command Handlers

BOOL CBlockPropertiesDialog::OnCommandYes(UINT uID)
{
	if( ReadInputs() ) {

		EndDialog(1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBlockPropertiesDialog::OnCommandNo(UINT uID)
{
	EndDialog(0);

	return TRUE;
	}

// Implementation

void CBlockPropertiesDialog::LoadCaption(void)
{
	CString Text;

	Text += L"Edit Function Block Properties";

	SetWindowText(Text);
	}

void CBlockPropertiesDialog::LoadInputs(void)
{
	CEditCtrl    &Edit = (CEditCtrl &) GetDlgItem(200);

	CFunctionBlock &FB = (CFunctionBlock &) *m_pFB;

	Edit.SetWindowText(CPrintf(L"%d", FB.m_nInputs));
	}

BOOL CBlockPropertiesDialog::ReadInputs(void)
{
	CEditCtrl &Edit = (CEditCtrl &) GetDlgItem(200);

	UINT uInputs = watoi(Edit.GetWindowText());

	if( uInputs > 1  ) {
	
		CFunctionBlock &FB = (CFunctionBlock &) *m_pFB;

		FB.m_nInputs = uInputs;
		
		return TRUE;
		}

	CError Error;

	Error.Set(L"Out of range");

	Error.Show(ThisObject);	

	SetDlgFocus(200);

	return FALSE;
	}

// End of File
