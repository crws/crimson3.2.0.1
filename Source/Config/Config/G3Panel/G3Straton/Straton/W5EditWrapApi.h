/******************************************************************************
* This wrapper was automatically created
* It is used to wrap the control interfaces of COPA-DATA France dlls.
* Creation Date: Nov 04, 2015 (09:22:34)
* This source code has been automatically created, do not change it.
*
*
******************************************************************************/
#pragma once

#include "W5EditApi.h"
#include "W5EditTLApi.h"
#include "W5EditFFApi.h"
#include "W5EditFFCtrlApi.h"

//the extern function in dll
typedef LPCSTR  (*K5GETCLASSNAME)   ();
typedef void    (*K5SETSTRING)      (DWORD, LPCSTR);
typedef LRESULT (*K5EXECUTECOMMAND) ( HWND hWnd, int argc, LPSTR* argv);


class CW5EditWrapApi : public CWnd
{
public:
    //construction
	CW5EditWrapApi()
    {
        _m_hDll = NULL;

        _m_pfExecuteCommand = NULL;
        _m_pfGetClassName   = NULL;
        _m_pfSetString      = NULL;
    }
    //destruction
	virtual ~CW5EditWrapApi()
    {
        DestroyWindow();

        if ( _m_hDll )
            FreeLibrary(_m_hDll);

        _m_pfExecuteCommand = NULL;
        _m_pfGetClassName   = NULL;
        _m_pfSetString      = NULL;
    }

public:
    //init dll path
    BOOL Init(LPCSTR szDll, LPCSTR szTLGUID = NULL)
    {
        if ( _m_hDll )
        {
            FreeLibrary(_m_hDll);
            _m_hDll = NULL;
            _m_pfExecuteCommand = NULL;
            _m_pfGetClassName   = NULL;
            _m_pfSetString      = NULL;
        }

        _m_hDll = LoadLibrary(szDll);

        _m_sTLType = szTLGUID;

        if ( _m_hDll )
        {
            _m_pfExecuteCommand = (K5EXECUTECOMMAND)GetProcAddress(_m_hDll, "K5ExecuteCommand");
            _m_pfGetClassName   = (K5GETCLASSNAME)  GetProcAddress(_m_hDll, "K5GetClassName");
            _m_pfSetString      = (K5SETSTRING)     GetProcAddress(_m_hDll, "K5SetString");
        }

        return _m_hDll!=NULL;
    }

    //window creation
    virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
    {
        BOOL bCreate = FALSE;

        CString sClass = GetClassName();
        if ( !sClass.IsEmpty() )
            bCreate = CWnd::CreateEx(0, sClass, _m_sTLType, dwStyle, rect, pParentWnd, nID);

        return bCreate;
    }

    //for control translations
    void SetString(DWORD dwID, LPCSTR szString)
    {
        if ( _m_pfSetString )
            _m_pfSetString(dwID,szString);
    }

    CString GetClassName()
    {
        if ( _m_pfGetClassName )
            return _m_pfGetClassName();

        return "";
    }

private:
    CString     _m_sTLType;

    HINSTANCE           _m_hDll;
    K5EXECUTECOMMAND    _m_pfExecuteCommand;
    K5GETCLASSNAME      _m_pfGetClassName;
    K5SETSTRING         _m_pfSetString;

public: //the generic execution functions
    LRESULT Execute(int argc, LPSTR* argv)
    {
        if ( _m_pfExecuteCommand && GetSafeHwnd() && ::IsWindow(GetSafeHwnd()) )
            return _m_pfExecuteCommand(GetSafeHwnd(), argc, argv);
        else
            return 0L;
    }
    LRESULT Execute(LPSTR szCommand)
    {
        LPSTR argv[1];
        argv[0] = szCommand;

        return Execute(1, argv);
    }
    LRESULT Execute(LPSTR szCommand, HTREEITEM hItem)
    {
        LPSTR argv[2];
        argv[0] = szCommand;

        CString sItem;
#ifdef _WIN64
        sItem.Format("%I64u", (unsigned __int64)hItem);
#else
        sItem.Format("%ld", (long)hItem);
#endif
        argv[1] = sItem.GetBuffer(255);

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, bool bParam)
    {
        LPSTR argv[2];
        argv[0] = szCommand;
        argv[1] = bParam?"1":"0";

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, WORD wParam)
    {
        LPSTR argv[2];
        argv[0] = szCommand;
        CString sParam;
        sParam.Format("%d", wParam);
        argv[1] = sParam.GetBuffer(255);

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, LPCSTR szParam)
    {
        LPSTR argv[2];
        argv[0] = szCommand;
        argv[1] = (LPSTR)szParam;

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, int iParam)
    {
        LPSTR argv[2];

        argv[0] = szCommand;

        CString sParam;
        sParam.Format("%d", iParam);
        argv[1] = sParam.GetBuffer(255);

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, DWORD dwParam)
    {
        LPSTR argv[2];

        argv[0] = szCommand;

        CString sParam;
        sParam.Format("%ld", dwParam);
        argv[1] = sParam.GetBuffer(255);

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, long lParam)
    {
        LPSTR argv[2];

        argv[0] = szCommand;

        CString sParam;
        sParam.Format("%ld", lParam);
        argv[1] = sParam.GetBuffer(255);

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, void* pParam)
    {
        LPSTR argv[2];

        argv[0] = szCommand;

        CString sParam;
#ifdef _WIN64
        sParam.Format("%I64u", (unsigned __int64)pParam);
#else
        sParam.Format("%ld", (long)pParam);
#endif

        argv[1] = sParam.GetBuffer(255);

        return Execute(2, argv);
    }

    LRESULT Execute(LPSTR szCommand, int iParam, LPCSTR szParam)
    {
        LPSTR argv[3];

        argv[0] = szCommand;

        CString sParam;
        sParam.Format("%ld", iParam);
        argv[1] = sParam.GetBuffer(255);

        argv[2] = (LPSTR)szParam;

        return Execute(3, argv);
    }

#ifdef _WIN64
    LRESULT Execute(LPSTR szCommand, WPARAM wParam, LPARAM lParam)
    {
        LPSTR argv[3];

        argv[0] = szCommand;

        CString sParam1;
        sParam1.Format("%I64u", (unsigned __int64)wParam);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%I64u", (unsigned __int64)lParam);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv);
    }
#else
    LRESULT Execute(LPSTR szCommand, int iParam1, int iParam2)
    {
        LPSTR argv[3];

        argv[0] = szCommand;

        CString sParam1;
        sParam1.Format("%ld", iParam1);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", iParam2);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv);
    }
#endif

public: //list of wrapped commands
//BEGIN_WRAP_C++
    LPCSTR Help(LPCSTR szCsvFile = NULL)                 { return (LPCSTR)Execute("Help", szCsvFile); }
    void SetEnable(BOOL bEnable = TRUE)                  { Execute("SetEnable", bEnable); }
    BOOL IsEnable(void)                                  { return Execute("IsEnable") != 0; }
    void SetReadOnly(BOOL bReadOnly = TRUE)              { Execute("SetReadOnly", bReadOnly); }
    BOOL IsReadOnly(void)                                { return Execute("IsReadOnly") != 0; }
    void SetModified(BOOL bModified = TRUE)              { Execute("SetModified", bModified); }
    BOOL IsModified(void)                                { return Execute("IsModified") != 0; }
    BOOL IsEmpty(void)                                   { return Execute("IsEmpty") != 0; }
    void SetDebug(BOOL bDebug = TRUE)                    { Execute("SetDebug", bDebug); }
    BOOL SetProjectPath(LPCSTR szProjectPath)            { return Execute("SetProjectPath", szProjectPath) != 0; }
    BOOL CanCut(void)                                    { return Execute("CanCut") != 0; }
    void Cut(void)                                       { Execute("Cut"); }
    BOOL CanClear(void)                                  { return Execute("CanClear") != 0; }
    void Clear(void)                                     { Execute("Clear"); }
    BOOL CanCopy(void)                                   { return Execute("CanCopy") != 0; }
    void Copy(void)                                      { Execute("Copy"); }
    BOOL CanPaste(void)                                  { return Execute("CanPaste") != 0; }
    void Paste(void)                                     { Execute("Paste"); }
    BOOL CanUndo(void)                                   { return Execute("CanUndo") != 0; }
    void Undo(void)                                      { Execute("Undo"); }
    BOOL CanRedo(void)                                   { return Execute("CanRedo") != 0; }
    void Redo(void)                                      { Execute("Redo"); }
    BOOL CanSelectAll(void)                              { return Execute("CanSelectAll") != 0; }
    void SelectAll(void)                                 { Execute("SelectAll"); }
    BOOL CanSave(void)                                   { return Execute("CanSave") != 0; }
    void Save(LPCSTR szPath)                             { Execute("Save", szPath); }
    void Load(LPCSTR szPath)                             { Execute("Load", szPath); }
    void InsertFile(LPCSTR szPath)                       { Execute("InsertFile", szPath); }
    void SetText(LPCSTR szText)                          { Execute("SetText", szText); }
    LPCSTR GetText(void)                                 { return (LPCSTR)Execute("GetText"); }
    int GetTextLenght(void)                              { return (int)Execute("GetTextLenght"); }
    void FocusGroup(LPCSTR szGroup)                      { Execute("FocusGroup", szGroup); }
    void FilterGroup(LPCSTR szGroupList)                 { Execute("FilterGroup", szGroupList); }
    LPCSTR GetSelGroupName(void)                         { return (LPCSTR)Execute("GetSelGroupName"); }
    DWORD GetGroupID(void)                               { return (DWORD)Execute("GetGroupID"); }
    BOOL SetSyntaxColoring(LPCSTR szSyntax)              { return Execute("SetSyntaxColoring", szSyntax) != 0; }
    void LocateError(LPCSTR szError)                     { Execute("LocateError", szError); }
    void EmptyUndoStack(void)                            { Execute("EmptyUndoStack"); }
    void SetID(DWORD dwID)                               { Execute("SetID", dwID); }
    BOOL CanInsertSymbol(void)                           { return Execute("CanInsertSymbol") != 0; }
    void InsertSymbol(LPCSTR szSymbol)                   { Execute("InsertSymbol", szSymbol); }
    int SetTab(int iTab)                                 { return (int)Execute("SetTab", iTab); }
    BOOL CanSetZoom(void)                                { return Execute("CanSetZoom") != 0; }
    void SetZoom(int iDir, int iRatio = 0)               { Execute("SetZoom", iDir, iRatio); }
    BOOL CanSetGrid(void)                                { return Execute("CanSetGrid") != 0; }
    void SetGrid(BOOL bGrid = TRUE)                      { Execute("SetGrid", bGrid); }
    long FindReplace(LPCSTR szFind, LPCSTR szReplace, DWORD dwCommand, DWORD dwFlags, HWND hListBox = NULL)
    {
        LPSTR argv[6];

        argv[0] = "FindReplace";

        argv[1] = (LPSTR)szFind;

        argv[2] = (LPSTR)szReplace;

        CString sParam3;
        sParam3.Format("%ld", dwCommand);
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%ld", dwFlags);
        argv[4] = sParam4.GetBuffer(255);

        CString sParam5;
#ifdef _WIN64
        sParam5.Format("%I64u", (unsigned __int64)hListBox);
#else
        sParam5.Format("%ld", (long)hListBox);
#endif
        argv[5] = sParam5.GetBuffer(255);

        return (long)Execute(6, argv);
    }
    void Goto(LPCSTR szGoto)                             { Execute("Goto", szGoto); }
    long GetCurPos(void)                                 { return (long)Execute("GetCurPos"); }
    BOOL IsCheck(int iMode)                              { return Execute("IsCheck", iMode) != 0; }
    BOOL CanCheck(int iMode)                             { return Execute("CanCheck", iMode) != 0; }
    void Check(int iMode)                                { Execute("Check", iMode); }
    BOOL CanFormatProgram(void)                          { return Execute("CanFormatProgram") != 0; }
    void FormatProgram(void)                             { Execute("FormatProgram"); }
    BOOL CanViewInfo(void)                               { return Execute("CanViewInfo") != 0; }
    void ViewInfo(void)                                  { Execute("ViewInfo"); }
    BOOL CanInsertFile(void)                             { return Execute("CanInsertFile") != 0; }
    int GetSelTextLength(void)                           { return (int)Execute("GetSelTextLength"); }
    LPCSTR GetSelText(void)                              { return (LPCSTR)Execute("GetSelText"); }
    int PaintToDC(HDC hDC, int iLeft, int iTop, int iRight, int iBottom)
    {
        LPSTR argv[6];

        argv[0] = "PaintToDC";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hDC);
#else
        sParam1.Format("%ld", (long)hDC);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iLeft);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
        sParam3.Format("%d", iTop);
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%d", iRight);
        argv[4] = sParam4.GetBuffer(255);

        CString sParam5;
        sParam5.Format("%d", iBottom);
        argv[5] = sParam5.GetBuffer(255);

        return (int)Execute(6, argv);
    }
    BOOL SetBGColor(COLORREF rgbMain, COLORREF rgbUsed)
    {
        LPSTR argv[3];

        argv[0] = "SetBGColor";

        CString sParam1;
        sParam1.Format("%ld", rgbMain);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", rgbUsed);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL SetCurrentFB(LPCSTR szName, DWORD dwID, int nbIn, int nbOut, BOOL bInstanciable, BOOL bDBObject)
    {
        LPSTR argv[7];

        argv[0] = "SetCurrentFB";

        argv[1] = (LPSTR)szName;

        CString sParam2;
        sParam2.Format("%ld", dwID);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
        sParam3.Format("%d", nbIn);
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%d", nbOut);
        argv[4] = sParam4.GetBuffer(255);

        argv[5] = bInstanciable?"1":"0";

        argv[6] = bDBObject?"1":"0";

        return Execute(7, argv) != 0;
    }
    BOOL CanInsertFB(void)                               { return Execute("CanInsertFB") != 0; }
    void InsertFB(LPCSTR szName)                         { Execute("InsertFB", szName); }
    void SetFB(LPCSTR szName, DWORD dwID, int nbIn, int nbOut, BOOL bInstanciable, BOOL bDBObject)
    {
        LPSTR argv[7];

        argv[0] = "SetFB";

        argv[1] = (LPSTR)szName;

        CString sParam2;
        sParam2.Format("%ld", dwID);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
        sParam3.Format("%d", nbIn);
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%d", nbOut);
        argv[4] = sParam4.GetBuffer(255);

        argv[5] = bInstanciable?"1":"0";

        argv[6] = bDBObject?"1":"0";

        Execute(7, argv);
    }
    LPCSTR GetFB(void)                                   { return (LPCSTR)Execute("GetFB"); }
    LPCSTR GetSymbolName(void)                           { return (LPCSTR)Execute("GetSymbolName"); }
    LPCSTR GetTypeName(void)                             { return (LPCSTR)Execute("GetTypeName"); }
    TCHAR GetFirstChar(void)                             { return (TCHAR)Execute("GetFirstChar"); }
    BOOL IsGridVisible(void)                             { return Execute("IsGridVisible") != 0; }
    BOOL EnableDragnDrop(BOOL bEnable = TRUE)            { return Execute("EnableDragnDrop", bEnable) != 0; }
    int UndoRedoSize(int iSize)                          { return (int)Execute("UndoRedoSize", iSize); }
    BOOL KeepFBDSelect(BOOL bSet = TRUE)                 { return Execute("KeepFBDSelect", bSet) != 0; }
    BOOL CopyBitmap(BOOL bSet = TRUE)                    { return Execute("CopyBitmap", bSet) != 0; }
    BOOL PromptVarname(BOOL bSet = TRUE)                 { return Execute("PromptVarname", bSet) != 0; }
    BOOL PromptInstance(BOOL bSet = TRUE)                { return Execute("PromptInstance", bSet) != 0; }
    BOOL EnablePulse(BOOL bSet = TRUE)                   { return Execute("EnablePulse", bSet) != 0; }
    int SetVertVarSize(int iHeight)                      { return (int)Execute("SetVertVarSize", iHeight); }
    int SetHorzVarSize(int iWidth)                       { return (int)Execute("SetHorzVarSize", iWidth); }
    int SetPageWidth(int iWidth)                         { return (int)Execute("SetPageWidth", iWidth); }
    int SetContents(int iContentType, LPCSTR szQualifPrefix) { return (int)Execute("SetContents", iContentType, szQualifPrefix); }
    BOOL PrintSetProperty(int iProp, void* pValue)
    {
        LPSTR argv[3];

        argv[0] = "PrintSetProperty";

        CString sParam1;
        sParam1.Format("%d", iProp);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)pValue);
#else
        sParam2.Format("%ld", (long)pValue);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    int PrintGetNbVertFolio(void)                        { return (int)Execute("PrintGetNbVertFolio"); }
    int PrintGetNbHorzFolio(void)                        { return (int)Execute("PrintGetNbHorzFolio"); }
    BOOL PrintGetFolio(int iX, int iY)                   { return Execute("PrintGetFolio", iX, iY) != 0; }
    void PrintFolio(int iX, int iY, LPCSTR szFile)
    {
        LPSTR argv[4];

        argv[0] = "PrintFolio";

        CString sParam1;
        sParam1.Format("%d", iX);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iY);
        argv[2] = sParam2.GetBuffer(255);

        argv[3] = (LPSTR)szFile;

        Execute(4, argv);
    }
    int GetCellHeight(void)                              { return (int)Execute("GetCellHeight"); }
    int GetCellWidth(void)                               { return (int)Execute("GetCellWidth"); }
    void SetCellHeight(int iHeight)                      { Execute("SetCellHeight", iHeight); }
    void SetCellWidth(int iWidth)                        { Execute("SetCellWidth", iWidth); }
    DWORD DisplayFBDOrder(void)                          { return (DWORD)Execute("DisplayFBDOrder"); }
    BOOL CanChangeProperties(void)                       { return Execute("CanChangeProperties") != 0; }
    void SetProperties(LPCSTR szProp)                    { Execute("SetProperties", szProp); }
    LPCSTR GetProperties(void)                           { return (LPCSTR)Execute("GetProperties"); }
    void EnsureSelVisible(void)                          { Execute("EnsureSelVisible"); }
    void AlignCoils(void)                                { Execute("AlignCoils"); }
    BOOL CanAlignCoils(void)                             { return Execute("CanAlignCoils") != 0; }
    BOOL CanDisplayFBDOrder(void)                        { return Execute("CanDisplayFBDOrder") != 0; }
    BOOL CanInsertText(void)                             { return Execute("CanInsertText") != 0; }
    void InsertText(int iX, int iY, LPCSTR szText)
    {
        LPSTR argv[4];

        argv[0] = "InsertText";

        CString sParam1;
        sParam1.Format("%d", iX);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iY);
        argv[2] = sParam2.GetBuffer(255);

        argv[3] = (LPSTR)szText;

        Execute(4, argv);
    }
    int PrintGetNbSymbols(int iX, int iY)                { return (int)Execute("PrintGetNbSymbols", iX, iY); }
    LPCSTR PrintGetSymbols(int iX, int iY)               { return (LPCSTR)Execute("PrintGetSymbols", iX, iY); }
    void WrapRungs(BOOL bWrap = TRUE)                    { Execute("WrapRungs", bWrap); }
    BOOL SummaryPrint(BOOL bAll, HDC hDC)
    {
        LPSTR argv[3];

        argv[0] = "SummaryPrint";

        argv[1] = bAll?"1":"0";

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)hDC);
#else
        sParam2.Format("%ld", (long)hDC);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL AutoDeclareInst(BOOL bSet = TRUE)               { return Execute("AutoDeclareInst", bSet) != 0; }
    int SetWidthSummaryPrint(int iWidth)                 { return (int)Execute("SetWidthSummaryPrint", iWidth); }
    int SetHeightSummaryPrint(int iHeight)               { return (int)Execute("SetHeightSummaryPrint", iHeight); }
    BOOL HasSelection(void)                              { return Execute("HasSelection") != 0; }
    BOOL CanInsertContactBefore(void)                    { return Execute("CanInsertContactBefore") != 0; }
    void InsertContactBefore(void)                       { Execute("InsertContactBefore"); }
    BOOL CanInsertContactAfter(void)                     { return Execute("CanInsertContactAfter") != 0; }
    void InsertContactAfter(void)                        { Execute("InsertContactAfter"); }
    BOOL CanInsertContactParallel(void)                  { return Execute("CanInsertContactParallel") != 0; }
    void InsertContactParallel(void)                     { Execute("InsertContactParallel"); }
    BOOL CanInsertCoil(void)                             { return Execute("CanInsertCoil") != 0; }
    void InsertCoil(void)                                { Execute("InsertCoil"); }
    BOOL CanInsertFBBefore(void)                         { return Execute("CanInsertFBBefore") != 0; }
    void InsertFBBefore(void)                            { Execute("InsertFBBefore"); }
    BOOL CanInsertFBAfter(void)                          { return Execute("CanInsertFBAfter") != 0; }
    void InsertFBAfter(void)                             { Execute("InsertFBAfter"); }
    BOOL CanInsertFBParallel(void)                       { return Execute("CanInsertFBParallel") != 0; }
    void InsertFBParallel(void)                          { Execute("InsertFBParallel"); }
    BOOL CanInsertJump(void)                             { return Execute("CanInsertJump") != 0; }
    void InsertJump(void)                                { Execute("InsertJump"); }
    BOOL CanInsertRung(void)                             { return Execute("CanInsertRung") != 0; }
    void InsertRung(void)                                { Execute("InsertRung"); }
    BOOL CanInsertComment(void)                          { return Execute("CanInsertComment") != 0; }
    void InsertComment(void)                             { Execute("InsertComment"); }
    BOOL CanInsertHorz(void)                             { return Execute("CanInsertHorz") != 0; }
    void InsertHorz(void)                                { Execute("InsertHorz"); }
    BOOL EnableSyntax(BOOL bEnable = TRUE)               { return Execute("EnableSyntax", bEnable) != 0; }
    void Lock(BOOL bLock = TRUE)                         { Execute("Lock", bLock); }
    BOOL CanInsert(void)                                 { return Execute("CanInsert") != 0; }
    void InsertStep(void)                                { Execute("InsertStep"); }
    void InsertTrans(void)                               { Execute("InsertTrans"); }
    void InsertMainDiv(void)                             { Execute("InsertMainDiv"); }
    void InsertDiv(void)                                 { Execute("InsertDiv"); }
    void InsertCnv(void)                                 { Execute("InsertCnv"); }
    void InsertMacro(void)                               { Execute("InsertMacro"); }
    void InsertMacroBody(void)                           { Execute("InsertMacroBody"); }
    BOOL CanSwapStyle(void)                              { return Execute("CanSwapStyle") != 0; }
    void SwapStyle(void)                                 { Execute("SwapStyle"); }
    void InsertInitStep(void)                            { Execute("InsertInitStep"); }
    LPCSTR GetTransCode(int iRef)                        { return (LPCSTR)Execute("GetTransCode", iRef); }
    LPCSTR GetTransNote(int iRef)                        { return (LPCSTR)Execute("GetTransNote", iRef); }
    void SetTransNote(int iRef, LPCSTR szCode)           { Execute("SetTransNote", iRef, szCode); }
    LPCSTR GetStepCode_Def(int iRef)                     { return (LPCSTR)Execute("GetStepCode_Def", iRef); }
    void SetStepCode_Def(int iRef, LPCSTR szCode)        { Execute("SetStepCode_Def", iRef, szCode); }
    LPCSTR GetStepCode_P1(int iRef)                      { return (LPCSTR)Execute("GetStepCode_P1", iRef); }
    void SetStepCode_P1(int iRef, LPCSTR szCode)         { Execute("SetStepCode_P1", iRef, szCode); }
    LPCSTR GetStepCode_N(int iRef)                       { return (LPCSTR)Execute("GetStepCode_N", iRef); }
    void SetStepCode_N(int iRef, LPCSTR szCode)          { Execute("SetStepCode_N", iRef, szCode); }
    LPCSTR GetStepCode_P0(int iRef)                      { return (LPCSTR)Execute("GetStepCode_P0", iRef); }
    void SetStepCode_P0(int iRef, LPCSTR szCode)         { Execute("SetStepCode_P0", iRef, szCode); }
    LPCSTR GetStepNote(int iRef)                         { return (LPCSTR)Execute("GetStepNote", iRef); }
    void SetStepNote(int iRef, LPCSTR szCode)            { Execute("SetStepNote", iRef, szCode); }
    long IsSelStep(void)                                 { return (long)Execute("IsSelStep"); }
    long IsSelTrans(void)                                { return (long)Execute("IsSelTrans"); }
    long GetSelRefNum(void)                              { return (long)Execute("GetSelRefNum"); }
    void LockStep(BOOL bLock, int iRef)
    {
        LPSTR argv[3];

        argv[0] = "LockStep";

        argv[1] = bLock?"1":"0";

        CString sParam2;
        sParam2.Format("%d", iRef);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    void LockTrans(BOOL bLock, int iRef)
    {
        LPSTR argv[3];

        argv[0] = "LockTrans";

        argv[1] = bLock?"1":"0";

        CString sParam2;
        sParam2.Format("%d", iRef);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    void EnterSelRef(void)                               { Execute("EnterSelRef"); }
    BOOL CanEnterSelRef(void)                            { return Execute("CanEnterSelRef") != 0; }
    BOOL CanEditSelCode(void)                            { return Execute("CanEditSelCode") != 0; }
    int GetStepLanguageP1(int iRef)                      { return (int)Execute("GetStepLanguageP1", iRef); }
    int GetStepLanguageN(int iRef)                       { return (int)Execute("GetStepLanguageN", iRef); }
    int GetStepLanguageP0(int iRef)                      { return (int)Execute("GetStepLanguageP0", iRef); }
    int GetTransLanguage(int iRef)                       { return (int)Execute("GetTransLanguage", iRef); }
    void SetNoteDisplay(BOOL bSet = TRUE)                { Execute("SetNoteDisplay", bSet); }
    BOOL GetNoteDisplay(void)                            { return Execute("GetNoteDisplay") != 0; }
    BOOL CanRenumber(void)                               { return Execute("CanRenumber") != 0; }
    void Renumber(void)                                  { Execute("Renumber"); }
    long NextPosItem(BOOL bLoop = TRUE)                  { return (long)Execute("NextPosItem", bLoop); }
    void ActiveStep(BOOL bEnsure = TRUE)                 { Execute("ActiveStep", bEnsure); }
    long IsStep(int iX, int iY)                          { return (long)Execute("IsStep", iX, iY); }
    long IsTrans(int iX, int iY)                         { return (long)Execute("IsTrans", iX, iY); }
    BOOL CanSetTab(void)                                 { return Execute("CanSetTab") != 0; }
    BOOL EnableCopy(BOOL bEnable = TRUE)                 { return Execute("EnableCopy", bEnable) != 0; }
    void SetTransCode(int iRef, LPCSTR szCode)           { Execute("SetTransCode", iRef, szCode); }
    int GetZoom(void)                                    { return (int)Execute("GetZoom"); }
    BOOL FocusVar(LPCSTR szGroup, LPCSTR szVar)
    {
        LPSTR argv[3];

        argv[0] = "FocusVar";

        argv[1] = (LPSTR)szGroup;

        argv[2] = (LPSTR)szVar;

        return Execute(3, argv) != 0;
    }
    BOOL IsStepLock(int iRef)                            { return Execute("IsStepLock", iRef) != 0; }
    BOOL IsTransLock(int iRef)                           { return Execute("IsTransLock", iRef) != 0; }
    long GetSymbolPos(void)                              { return (long)Execute("GetSymbolPos"); }
    BOOL IsDebug(void)                                   { return Execute("IsDebug") != 0; }
    BOOL ResetStepPos(void)                              { return Execute("ResetStepPos") != 0; }
    BOOL SetStepPos(LPCSTR szLocation)                   { return Execute("SetStepPos", szLocation) != 0; }
    void RemoveAllBkp(void)                              { Execute("RemoveAllBkp"); }
    void SetBkp(LPCSTR szLocation)                       { Execute("SetBkp", szLocation); }
    long GetCaret(void)                                  { return (long)Execute("GetCaret"); }
    BOOL HasBreakpoint(void)                             { return Execute("HasBreakpoint") != 0; }
    void SetInstance(LPCSTR szInstance)                  { Execute("SetInstance", szInstance); }
    void SetParent(LPCSTR szParent)                      { Execute("SetParent", szParent); }
    void LoadExpand(LPCSTR szPath)                       { Execute("LoadExpand", szPath); }
    void SaveExpand(LPCSTR szPath)                       { Execute("SaveExpand", szPath); }
    BOOL ReloadBitmap(void)                              { return Execute("ReloadBitmap") != 0; }
    BOOL CanAlign(void)                                  { return Execute("CanAlign") != 0; }
    void Align(int iHorz, int iVert)                     { Execute("Align", iHorz, iVert); }
    int GetNbItem(void)                                  { return (int)Execute("GetNbItem"); }
    BOOL GetZOrderStruct(int iItem, void* pStrZOrder)
    {
        LPSTR argv[3];

        argv[0] = "GetZOrderStruct";

        CString sParam1;
        sParam1.Format("%d", iItem);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)pStrZOrder);
#else
        sParam2.Format("%ld", (long)pStrZOrder);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL CanMoveTop(void)                                { return Execute("CanMoveTop") != 0; }
    void MoveTop(void)                                   { Execute("MoveTop"); }
    BOOL CanMoveBottom(void)                             { return Execute("CanMoveBottom") != 0; }
    void MoveBottom(void)                                { Execute("MoveBottom"); }
    BOOL CanResize(void)                                 { return Execute("CanResize") != 0; }
    void Resize(BOOL bX, BOOL bY)
    {
        LPSTR argv[3];

        argv[0] = "Resize";

        argv[1] = bX?"1":"0";

        argv[2] = bY?"1":"0";

        Execute(3, argv);
    }
    long GetSelSize(void)                                { return (long)Execute("GetSelSize"); }
    long GetSelPos(void)                                 { return (long)Execute("GetSelPos"); }
    void NewFile(void)                                   { Execute("NewFile"); }
    BOOL CanSwapItemStyle(void)                          { return Execute("CanSwapItemStyle") != 0; }
    void SwapItemStyle(void)                             { Execute("SwapItemStyle"); }
    BOOL CanChangeBkColor(void)                          { return Execute("CanChangeBkColor") != 0; }
    void ChangeBkColor(void)                             { Execute("ChangeBkColor"); }
    LPCSTR GetParent(void)                               { return (LPCSTR)Execute("GetParent"); }
    LPCSTR GetInstance(void)                             { return (LPCSTR)Execute("GetInstance"); }
    BOOL HideScroll(BOOL bHide = TRUE)                   { return Execute("HideScroll", bHide) != 0; }
    void SetTimer(void)                                  { Execute("SetTimer"); }
    void SelectItem(DWORD_PTR dwData, BOOL bDeselectAll = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "SelectItem";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)dwData);
#else
        sParam1.Format("%ld", (long)dwData);
#endif
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bDeselectAll?"1":"0";

        Execute(3, argv);
    }
    DWORD GetItemID(void)                                { return (DWORD)Execute("GetItemID"); }
    void MoveBefore(DWORD dwMove, DWORD dwBefore)
    {
        LPSTR argv[3];

        argv[0] = "MoveBefore";

        CString sParam1;
        sParam1.Format("%ld", dwMove);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", dwBefore);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    void MoveAfter(DWORD dwMove, DWORD dwAfter)
    {
        LPSTR argv[3];

        argv[0] = "MoveAfter";

        CString sParam1;
        sParam1.Format("%ld", dwMove);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", dwAfter);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    BOOL CanActivate(void)                               { return Execute("CanActivate") != 0; }
    BOOL GetActivation(void)                             { return Execute("GetActivation") != 0; }
    void SetActivation(BOOL bActive = TRUE)              { Execute("SetActivation", bActive); }
    void SetUserID(LPCSTR szUserID)                      { Execute("SetUserID", szUserID); }
    LPCSTR GetLink(void)                                 { return (LPCSTR)Execute("GetLink"); }
    long SetPrompt(BOOL bAppend, LPCSTR szPrompt)
    {
        LPSTR argv[3];

        argv[0] = "SetPrompt";

        argv[1] = bAppend?"1":"0";

        argv[2] = (LPSTR)szPrompt;

        return (long)Execute(3, argv);
    }
    LPCSTR GetCommandLine(void)                          { return (LPCSTR)Execute("GetCommandLine"); }
    BOOL CanCreateUDFB(void)                             { return Execute("CanCreateUDFB") != 0; }
    BOOL CreateUDFB(void)                                { return Execute("CreateUDFB") != 0; }
    void SetValueInText(BOOL bSet = TRUE)                { Execute("SetValueInText", bSet); }
    void GotoXY(DWORD dwX, DWORD dwY)
    {
        LPSTR argv[3];

        argv[0] = "GotoXY";

        CString sParam1;
        sParam1.Format("%ld", dwX);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", dwY);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    int GetHeight(void)                                  { return (int)Execute("GetHeight"); }
    int GetWidth(void)                                   { return (int)Execute("GetWidth"); }
    void SaveSel(void)                                   { Execute("SaveSel"); }
    void RestoreSel(void)                                { Execute("RestoreSel"); }
    BOOL SetTooltipInfos(BOOL bEdit, str_W5TooltipInfos* pStrTI)
    {
        LPSTR argv[3];

        argv[0] = "SetTooltipInfos";

        argv[1] = bEdit?"1":"0";

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)pStrTI);
#else
        sParam2.Format("%ld", (long)pStrTI);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    LPCSTR GetTreeParent(void)                           { return (LPCSTR)Execute("GetTreeParent"); }
    BOOL FocusTree(void)                                 { return Execute("FocusTree") != 0; }
    void CollapseAll(void)                               { Execute("CollapseAll"); }
    void ExpandAll(void)                                 { Execute("ExpandAll"); }
    LPCSTR GetProgramName(void)                          { return (LPCSTR)Execute("GetProgramName"); }
    void SetProgramName(LPCSTR szProgram)                { Execute("SetProgramName", szProgram); }
    BOOL CanMoveDown(void)                               { return Execute("CanMoveDown") != 0; }
    HTREEITEM MoveDown(void)                             { return (HTREEITEM)Execute("MoveDown"); }
    BOOL CanMoveUp(void)                                 { return Execute("CanMoveUp") != 0; }
    HTREEITEM MoveUp(void)                               { return (HTREEITEM)Execute("MoveUp"); }
    BOOL CanHexDisplay(void)                             { return Execute("CanHexDisplay") != 0; }
    BOOL IsHexDisplay(void)                              { return Execute("IsHexDisplay") != 0; }
    void SwapHexDisplay(void)                            { Execute("SwapHexDisplay"); }
    BOOL CanSort(int iCol)                               { return Execute("CanSort", iCol) != 0; }
    void Sort(int iCol)                                  { Execute("Sort", iCol); }
    HTREEITEM InsertItem(BOOL bLast, LPCSTR szItem)
    {
        LPSTR argv[3];

        argv[0] = "InsertItem";

        argv[1] = bLast?"1":"0";

        argv[2] = (LPSTR)szItem;

        return (HTREEITEM)Execute(3, argv);
    }
    void Empty(void)                                     { Execute("Empty"); }
    BOOL CanSaveValues(void)                             { return Execute("CanSaveValues") != 0; }
    BOOL SaveValues(int iCol = W5F_CURRENTCOL, LPCSTR szCol = NULL) { return Execute("SaveValues", iCol, szCol) != 0; }
    BOOL CanSendReceipe(int iCol = W5F_CURRENTCOL)       { return Execute("CanSendReceipe", iCol) != 0; }
    BOOL SendReceipe(int iCol = W5F_CURRENTCOL)          { return Execute("SendReceipe", iCol) != 0; }
    BOOL CanRemoveCol(int iCol = W5F_CURRENTCOL)         { return Execute("CanRemoveCol", iCol) != 0; }
    BOOL CanInsertCol(int iCol = W5F_CURRENTCOL)         { return Execute("CanInsertCol", iCol) != 0; }
    int InsertCol(int iCol = W5F_CURRENTCOL, LPCSTR szCol = NULL, WORD wColType = NO_TYPE, WORD wColTypeEx = 0)
    {
        LPSTR argv[5];

        argv[0] = "InsertCol";

        CString sParam1;
        sParam1.Format("%d", iCol);
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = (LPSTR)szCol;

        CString sParam3;
        sParam3.Format("%d", wColType);
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%d", wColTypeEx);
        argv[4] = sParam4.GetBuffer(255);

        return (int)Execute(5, argv);
    }
    BOOL CanRenameCol(int iCol = W5F_CURRENTCOL)         { return Execute("CanRenameCol", iCol) != 0; }
    BOOL RenameCol(int iCol = W5F_CURRENTCOL)            { return Execute("RenameCol", iCol) != 0; }
    BOOL CanCopyCol(int iCol = W5F_CURRENTCOL)           { return Execute("CanCopyCol", iCol) != 0; }
    BOOL CopyCol(int iCol = W5F_CURRENTCOL)              { return Execute("CopyCol", iCol) != 0; }
    BOOL CanDecrease(void)                               { return Execute("CanDecrease") != 0; }
    void Decrease(void)                                  { Execute("Decrease"); }
    BOOL CanIncrease(void)                               { return Execute("CanIncrease") != 0; }
    void Increase(void)                                  { Execute("Increase"); }
    BOOL CanUncheck(void)                                { return Execute("CanUncheck") != 0; }
    void Uncheck(void)                                   { Execute("Uncheck"); }
    BOOL CanCheckTree(void)                              { return Execute("CanCheckTree") != 0; }
    LPCSTR CheckTree(void)                               { return (LPCSTR)Execute("CheckTree"); }
    void ResetValues(void)                               { Execute("ResetValues"); }
    void DeselectAll(void)                               { Execute("DeselectAll"); }
    BOOL CanPrint(void)                                  { return Execute("CanPrint") != 0; }
    BOOL IsPrintText(void)                               { return Execute("IsPrintText") != 0; }
    BOOL IsPrintGraphic(void)                            { return Execute("IsPrintGraphic") != 0; }
    LPCSTR GetSerialCol(void)                            { return (LPCSTR)Execute("GetSerialCol"); }
    LPCSTR GetSerialExpand(void)                         { return (LPCSTR)Execute("GetSerialExpand"); }
    void SetSerialCol(LPCSTR szSerial)                   { Execute("SetSerialCol", szSerial); }
    void SetSerialExpand(LPCSTR szSerial)                { Execute("SetSerialExpand", szSerial); }
    void SetLocalSel(DWORD dwGroup)                      { Execute("SetLocalSel", dwGroup); }
    void Expand(void)                                    { Execute("Expand"); }
    void SwapCollapse(void)                              { Execute("SwapCollapse"); }
    BOOL GetEditMode(void)                               { return Execute("GetEditMode") != 0; }
    void SetEditMode(BOOL bEdit = TRUE)                  { Execute("SetEditMode", bEdit); }
    DWORD GetSelVarID(void)                              { return (DWORD)Execute("GetSelVarID"); }
    BOOL IsDicoEnable(BOOL bWithSelVar, BOOL bWithSelGrp, BOOL bWithReadOnly)
    {
        LPSTR argv[4];

        argv[0] = "IsDicoEnable";

        argv[1] = bWithSelVar?"1":"0";

        argv[2] = bWithSelGrp?"1":"0";

        argv[3] = bWithReadOnly?"1":"0";

        return Execute(4, argv) != 0;
    }
    void ArrangeColumns(void)                            { Execute("ArrangeColumns"); }
    BOOL CanInsertStructure(void)                        { return Execute("CanInsertStructure") != 0; }
    void InsertStructure(void)                           { Execute("InsertStructure"); }
    BOOL CanMoveStructure(WORD wMove)                    { return Execute("CanMoveStructure", wMove) != 0; }
    void MoveStructure(WORD wMove)                       { Execute("MoveStructure", wMove); }
    BOOL CanEdit(void)                                   { return Execute("CanEdit") != 0; }
    BOOL CanExpand(void)                                 { return Execute("CanExpand") != 0; }
    void Collapse(void)                                  { Execute("Collapse"); }
    BOOL CanExpandAll(void)                              { return Execute("CanExpandAll") != 0; }
    BOOL CanSwapGlobalRet(void)                          { return Execute("CanSwapGlobalRet") != 0; }
    void SwapGlobalRet(void)                             { Execute("SwapGlobalRet"); }
    LPCSTR GetSelVarName(void)                           { return (LPCSTR)Execute("GetSelVarName"); }
    BOOL CanEditProperties(void)                         { return Execute("CanEditProperties") != 0; }
    int GetSortedCol(void)                               { return (int)Execute("GetSortedCol"); }
    BOOL IsSortAscending(void)                           { return Execute("IsSortAscending") != 0; }
    BOOL SetItemText(HTREEITEM hItem, int iCol, LPCSTR szText)
    {
        LPSTR argv[4];

        argv[0] = "SetItemText";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iCol);
        argv[2] = sParam2.GetBuffer(255);

        argv[3] = (LPSTR)szText;

        return Execute(4, argv) != 0;
    }
    LPCSTR GetItemText(HTREEITEM hItem, int iCol)
    {
        LPSTR argv[3];

        argv[0] = "GetItemText";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iCol);
        argv[2] = sParam2.GetBuffer(255);

        return (LPCSTR)Execute(3, argv);
    }
    void SetColWidth(int iCol, int iWidth)               { Execute("SetColWidth", iCol, iWidth); }
    HTREEITEM GetFirstItem(void)                         { return (HTREEITEM)Execute("GetFirstItem"); }
    HTREEITEM GetNextLineItem(HTREEITEM hItem)           { return (HTREEITEM)Execute("GetNextLineItem", hItem); }
    void EVTChanged(void)                                { Execute("EVTChanged"); }
    BOOL IsLoaded(void)                                  { return Execute("IsLoaded") != 0; }
    BOOL IsOutOfDate(void)                               { return Execute("IsOutOfDate") != 0; }
    LPCSTR GetProjectPath(void)                          { return (LPCSTR)Execute("GetProjectPath"); }
    void LockBinding(BOOL bLock = TRUE)                  { Execute("LockBinding", bLock); }
    BOOL RemoveCol(int iCol = W5F_CURRENTCOL)            { return Execute("RemoveCol", iCol) != 0; }
    void Refresh(void)                                   { Execute("Refresh"); }
    DWORD GetPrgID(void)                                 { return (DWORD)Execute("GetPrgID"); }
    LPCSTR GetPrgName(void)                              { return (LPCSTR)Execute("GetPrgName"); }
    BOOL CanMovePrg(WORD wMove)                          { return Execute("CanMovePrg", wMove) != 0; }
    BOOL MovePrg(WORD wMove)                             { return Execute("MovePrg", wMove) != 0; }
    BOOL CanAddPrg(WORD wPrg)                            { return Execute("CanAddPrg", wPrg) != 0; }
    BOOL AddPrg(WORD wPrg)                               { return Execute("AddPrg", wPrg) != 0; }
    BOOL CanRenamePrg(void)                              { return Execute("CanRenamePrg") != 0; }
    BOOL CanCmdPrg(WORD wCommand)                        { return Execute("CanCmdPrg", wCommand) != 0; }
    BOOL CmdPrg(WORD wCommand)                           { return Execute("CmdPrg", wCommand) != 0; }
    BOOL CanOpenPrg(void)                                { return Execute("CanOpenPrg") != 0; }
    long GetNbPrg(void)                                  { return (long)Execute("GetNbPrg"); }
    BOOL CanCopyPrg(void)                                { return Execute("CanCopyPrg") != 0; }
    int GetColWidth(int iCol)                            { return (int)Execute("GetColWidth", iCol); }
    WORD GetColType(int iCol = W5F_CURRENTCOL)           { return (WORD)Execute("GetColType", iCol); }
    WORD GetExColType(int iCol = W5F_CURRENTCOL)         { return (WORD)Execute("GetExColType", iCol); }
    HTREEITEM GetSelectedItem(void)                      { return (HTREEITEM)Execute("GetSelectedItem"); }
    int GetCurrentCol(void)                              { return (int)Execute("GetCurrentCol"); }
    DWORD_PTR GetItemData(HTREEITEM hItem)               { return (DWORD_PTR)Execute("GetItemData", hItem); }
    void SetItemData(HTREEITEM hItem, DWORD_PTR dwData)
    {
        LPSTR argv[3];

        argv[0] = "SetItemData";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)dwData);
#else
        sParam2.Format("%ld", (long)dwData);
#endif
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    int GetNbCol(void)                                   { return (int)Execute("GetNbCol"); }
    BOOL SetItemColor(HTREEITEM hItem, COLORREF rgb)
    {
        LPSTR argv[3];

        argv[0] = "SetItemColor";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", rgb);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    HTREEITEM GetPrevLineItem(HTREEITEM hItem)           { return (HTREEITEM)Execute("GetPrevLineItem", hItem); }
    void SetCurrentCol(int iCol)                         { Execute("SetCurrentCol", iCol); }
    void SetSortCol(int iCol, BOOL bAscending = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "SetSortCol";

        CString sParam1;
        sParam1.Format("%d", iCol);
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bAscending?"1":"0";

        Execute(3, argv);
    }
    HTREEITEM GetFirstSel(void)                          { return (HTREEITEM)Execute("GetFirstSel"); }
    HTREEITEM GetNextSel(HTREEITEM hItem)                { return (HTREEITEM)Execute("GetNextSel", hItem); }
    HTREEITEM InsertItemEx(LPCSTR szItem, HTREEITEM hParent = TVI_ROOT, HTREEITEM hInsertAfter = TVI_LAST)
    {
        LPSTR argv[4];

        argv[0] = "InsertItemEx";

        argv[1] = (LPSTR)szItem;

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)hParent);
#else
        sParam2.Format("%ld", (long)hParent);
#endif
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
#ifdef _WIN64
        sParam3.Format("%I64u", (unsigned __int64)hInsertAfter);
#else
        sParam3.Format("%ld", (long)hInsertAfter);
#endif
        argv[3] = sParam3.GetBuffer(255);

        return (HTREEITEM)Execute(4, argv);
    }
    BOOL SetItemBold(HTREEITEM hItem, BOOL bBold = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "SetItemBold";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bBold?"1":"0";

        return Execute(3, argv) != 0;
    }
    void SetColItemText(int iCol, LPCSTR szCol)          { Execute("SetColItemText", iCol, szCol); }
    void SetColItemParam(int iCol, DWORD_PTR dwData)
    {
        LPSTR argv[3];

        argv[0] = "SetColItemParam";

        CString sParam1;
        sParam1.Format("%d", iCol);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)dwData);
#else
        sParam2.Format("%ld", (long)dwData);
#endif
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    DWORD_PTR GetColItemParam(int iCol)                  { return (DWORD_PTR)Execute("GetColItemParam", iCol); }
    LPCSTR GetColItemText(int iCol)                      { return (LPCSTR)Execute("GetColItemText", iCol); }
    BOOL EnsureVisible(HTREEITEM hItem)                  { return Execute("EnsureVisible", hItem) != 0; }
    HTREEITEM GetParentItem(HTREEITEM hItem)             { return (HTREEITEM)Execute("GetParentItem", hItem); }
    HTREEITEM GetPrevSiblingItem(HTREEITEM hItem)        { return (HTREEITEM)Execute("GetPrevSiblingItem", hItem); }
    HTREEITEM GetNextSiblingItem(HTREEITEM hItem)        { return (HTREEITEM)Execute("GetNextSiblingItem", hItem); }
    HTREEITEM GetChildItem(HTREEITEM hItem)              { return (HTREEITEM)Execute("GetChildItem", hItem); }
    LPCSTR GetCSVFormat(void)                            { return (LPCSTR)Execute("GetCSVFormat"); }
    BOOL CanStartSampling(void)                          { return Execute("CanStartSampling") != 0; }
    void ChangeSetup(void)                               { Execute("ChangeSetup"); }
    BOOL NeedSetup(void)                                 { return Execute("NeedSetup") != 0; }
    BOOL StartSampling(void)                             { return Execute("StartSampling") != 0; }
    BOOL CanStopSampling(void)                           { return Execute("CanStopSampling") != 0; }
    BOOL StopSampling(void)                              { return Execute("StopSampling") != 0; }
    BOOL CanSetupSampling(void)                          { return Execute("CanSetupSampling") != 0; }
    BOOL IsAutoScroll(void)                              { return Execute("IsAutoScroll") != 0; }
    void EnableAutoScroll(BOOL bEnable = TRUE)           { Execute("EnableAutoScroll", bEnable); }
    LPCSTR GetUsedBlock(void)                            { return (LPCSTR)Execute("GetUsedBlock"); }
    BOOL SetPropValue(int iProp, LPCSTR szValue)         { return Execute("SetPropValue", iProp, szValue) != 0; }
    BOOL SetPropName(int iProp, LPCSTR szName)           { return Execute("SetPropName", iProp, szName) != 0; }
    BOOL SetPropReadOnly(int iProp, BOOL bReadOnly = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "SetPropReadOnly";

        CString sParam1;
        sParam1.Format("%d", iProp);
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bReadOnly?"1":"0";

        return Execute(3, argv) != 0;
    }
    BOOL SetPropHeader(int iProp, LPCSTR szHeader)       { return Execute("SetPropHeader", iProp, szHeader) != 0; }
    LPCSTR GetPropHeader(int iProp)                      { return (LPCSTR)Execute("GetPropHeader", iProp); }
    BOOL SetPropType(int iProp, int iType)               { return Execute("SetPropType", iProp, iType) != 0; }
    int GetPropType(int iProp)                           { return (int)Execute("GetPropType", iProp); }
    BOOL SetPropDesc(int iProp, LPCSTR szDesc)           { return Execute("SetPropDesc", iProp, szDesc) != 0; }
    BOOL SetPropMaxLen(int iProp, int iMaxLen)           { return Execute("SetPropMaxLen", iProp, iMaxLen) != 0; }
    BOOL SetPropEnum(int iProp, LPCSTR szEnum)           { return Execute("SetPropEnum", iProp, szEnum) != 0; }
    LPCSTR GetPropValue(int iProp)                       { return (LPCSTR)Execute("GetPropValue", iProp); }
    LPCSTR GetPropName(int iProp)                        { return (LPCSTR)Execute("GetPropName", iProp); }
    void SetPropSyntax(LPCSTR szSyntax)                  { Execute("SetPropSyntax", szSyntax); }
    BOOL CheckAllProp(void)                              { return Execute("CheckAllProp") != 0; }
    LPCSTR GetAllProps(LPCSTR szSeparator = NULL)        { return (LPCSTR)Execute("GetAllProps", szSeparator); }
    long IsSelMacro(void)                                { return (long)Execute("IsSelMacro"); }
    void LockMacro(BOOL bLock, int iRef)
    {
        LPSTR argv[3];

        argv[0] = "LockMacro";

        argv[1] = bLock?"1":"0";

        CString sParam2;
        sParam2.Format("%d", iRef);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    LPCSTR GetMacroNote(int iRef)                        { return (LPCSTR)Execute("GetMacroNote", iRef); }
    void SetMacroNote(int iRef, LPCSTR szCode)           { Execute("SetMacroNote", iRef, szCode); }
    void SetUsed(LPCSTR szUsed)                          { Execute("SetUsed", szUsed); }
    void SortTree(WORD wSortMethod, BOOL bInit = FALSE)
    {
        LPSTR argv[3];

        argv[0] = "SortTree";

        CString sParam1;
        sParam1.Format("%d", wSortMethod);
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bInit?"1":"0";

        Execute(3, argv);
    }
    WORD GetSortMethod(void)                             { return (WORD)Execute("GetSortMethod"); }
    DWORD GetProgramID(void)                             { return (DWORD)Execute("GetProgramID"); }
    DWORD GetFolderID(void)                              { return (DWORD)Execute("GetFolderID"); }
    BOOL EditProperties(void)                            { return Execute("EditProperties") != 0; }
    BOOL CanEditParameters(void)                         { return Execute("CanEditParameters") != 0; }
    void EditParameters(void)                            { Execute("EditParameters"); }
    BOOL CanCreateFile(DWORD dwFileType = 0)             { return Execute("CanCreateFile", dwFileType) != 0; }
    BOOL CreateFile(DWORD dwFileType = 0)                { return Execute("CreateFile", dwFileType) != 0; }
    void AddFileTypes(DWORD dwFileTypes)                 { Execute("AddFileTypes", dwFileTypes); }
    DWORD GetFileID(void)                                { return (DWORD)Execute("GetFileID"); }
    DWORD GetFileSection(void)                           { return (DWORD)Execute("GetFileSection"); }
    BOOL SetRedraw(BOOL bRedraw = TRUE)                  { return Execute("SetRedraw", bRedraw) != 0; }
    void SetErrorMode(BOOL bErrorMode = TRUE)            { Execute("SetErrorMode", bErrorMode); }
    LPCSTR GetPrintableText(void)                        { return (LPCSTR)Execute("GetPrintableText"); }
    BOOL NotifChanges(BOOL bNotif = TRUE)                { return Execute("NotifChanges", bNotif) != 0; }
    BOOL CanInsertNetwork(void)                          { return Execute("CanInsertNetwork") != 0; }
    void InsertNetwork(void)                             { Execute("InsertNetwork"); }
    BOOL CanInsertMasterPort(void)                       { return Execute("CanInsertMasterPort") != 0; }
    void InsertMasterPort(void)                          { Execute("InsertMasterPort"); }
    BOOL CanInsertSlaveRequest(void)                     { return Execute("CanInsertSlaveRequest") != 0; }
    void InsertSlaveRequest(void)                        { Execute("InsertSlaveRequest"); }
    void LockConfig(BOOL bLock = TRUE)                   { Execute("LockConfig", bLock); }
    LPCSTR GetKey(void)                                  { return (LPCSTR)Execute("GetKey"); }
    BOOL CanExecCommand(int iCommand)                    { return Execute("CanExecCommand", iCommand) != 0; }
    BOOL ExecCommand(int iCommand)                       { return Execute("ExecCommand", iCommand) != 0; }
    int GetNbCommand(void)                               { return (int)Execute("GetNbCommand"); }
    LPCSTR GetCommand(int iCommand)                      { return (LPCSTR)Execute("GetCommand", iCommand); }
    BOOL CanExport(void)                                 { return Execute("CanExport") != 0; }
    void Export(void)                                    { Execute("Export"); }
    BOOL CanImport(void)                                 { return Execute("CanImport") != 0; }
    void Import(void)                                    { Execute("Import"); }
    BOOL CanExportTree(void)                             { return Execute("CanExportTree") != 0; }
    void ExportTree(void)                                { Execute("ExportTree"); }
    BOOL CanImportTree(void)                             { return Execute("CanImportTree") != 0; }
    void ImportTree(void)                                { Execute("ImportTree"); }
    LPCSTR GetError(void)                                { return (LPCSTR)Execute("GetError"); }
    int GetLastChar(void)                                { return (int)Execute("GetLastChar"); }
    int GetLastKeyDown(void)                             { return (int)Execute("GetLastKeyDown"); }
    BOOL AutoDeclareSymbol(BOOL bSet = TRUE)             { return Execute("AutoDeclareSymbol", bSet) != 0; }
    int GetValueInText(void)                             { return (int)Execute("GetValueInText"); }
    void HideArrangeCol(void)                            { Execute("HideArrangeCol"); }
    void SetStyle(DWORD dwStyle)                         { Execute("SetStyle", dwStyle); }
    void SelectItems(int nbItem, void* arrItem, BOOL bDeselectAll = TRUE)
    {
        LPSTR argv[4];

        argv[0] = "SelectItems";

        CString sParam1;
        sParam1.Format("%d", nbItem);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)arrItem);
#else
        sParam2.Format("%ld", (long)arrItem);
#endif
        argv[2] = sParam2.GetBuffer(255);

        argv[3] = bDeselectAll?"1":"0";

        Execute(4, argv);
    }
    int GetContentWidth(void)                            { return (int)Execute("GetContentWidth"); }
    int GetContentHeight(void)                           { return (int)Execute("GetContentHeight"); }
    BOOL SetPropEnumEdit(int iProp, LPCSTR szEnum)       { return Execute("SetPropEnumEdit", iProp, szEnum) != 0; }
    void LockComment(BOOL bLock, int iRef)
    {
        LPSTR argv[3];

        argv[0] = "LockComment";

        argv[1] = bLock?"1":"0";

        CString sParam2;
        sParam2.Format("%d", iRef);
        argv[2] = sParam2.GetBuffer(255);

        Execute(3, argv);
    }
    DWORD IsSelComment(void)                             { return (DWORD)Execute("IsSelComment"); }
    void SetCommentNote(int iRef, LPCSTR szCode)         { Execute("SetCommentNote", iRef, szCode); }
    LPCSTR GetCommentNote(int iRef)                      { return (LPCSTR)Execute("GetCommentNote", iRef); }
    BOOL GetTargetMark(void* pTargetMark)
    {
        LPSTR argv[2];

        argv[0] = "GetTargetMark";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)pTargetMark);
#else
        sParam1.Format("%ld", (long)pTargetMark);
#endif
        argv[1] = sParam1.GetBuffer(255);

        return Execute(2, argv) != 0;
    }
    BOOL CanRecordSampling(void)                         { return Execute("CanRecordSampling") != 0; }
    BOOL IsRecording(void)                               { return Execute("IsRecording") != 0; }
    void StartRecordSampling(LPCSTR szPath)              { Execute("StartRecordSampling", szPath); }
    void StopRecordSampling(void)                        { Execute("StopRecordSampling"); }
    BOOL SetPropMinInt(int iProp, int iMinInt)           { return Execute("SetPropMinInt", iProp, iMinInt) != 0; }
    BOOL SetPropMaxInt(int iProp, int iMaxInt)           { return Execute("SetPropMaxInt", iProp, iMaxInt) != 0; }
    BOOL CanInsertFFLDItem(int iType)                    { return Execute("CanInsertFFLDItem", iType) != 0; }
    BOOL InsertFFLDItem(int iType)                       { return Execute("InsertFFLDItem", iType) != 0; }
    int GetNetSel(void)                                  { return (int)Execute("GetNetSel"); }
    long GetNetItemSel(void)                             { return (long)Execute("GetNetItemSel"); }
    void UpdateUDFB(void)                                { Execute("UpdateUDFB"); }
    BOOL NeedUpdateUDFB(void)                            { return Execute("NeedUpdateUDFB") != 0; }
    LPCSTR ListUpdateUDFB(void)                          { return (LPCSTR)Execute("ListUpdateUDFB"); }
    void SetLinePerComment(int nbLine)                   { Execute("SetLinePerComment", nbLine); }
    void ImportComment(void)                             { Execute("ImportComment"); }
    void ExportComment(void)                             { Execute("ExportComment"); }
    void DisplayConfirmation(BOOL bDisplay = TRUE)       { Execute("DisplayConfirmation", bDisplay); }
    BOOL IsUndef(void)                                   { return Execute("IsUndef") != 0; }
    void SetUndef(BOOL bIgnore)                          { Execute("SetUndef", bIgnore); }
    LPCSTR HelpOn(LPCSTR szCommand)                      { return (LPCSTR)Execute("HelpOn", szCommand); }
    DWORD GetItemType(void)                              { return (DWORD)Execute("GetItemType"); }
    BOOL CanClearRow(void)                               { return Execute("CanClearRow") != 0; }
    BOOL ClearRow(void)                                  { return Execute("ClearRow") != 0; }
    BOOL CanClearNetwork(void)                           { return Execute("CanClearNetwork") != 0; }
    BOOL ClearNetwork(void)                              { return Execute("ClearNetwork") != 0; }
    BOOL CanCollapseAll(void)                            { return Execute("CanCollapseAll") != 0; }
    BOOL CanCollapse(void)                               { return Execute("CanCollapse") != 0; }
    DWORD GetProjectID(void)                             { return (DWORD)Execute("GetProjectID"); }
    BOOL RemoveProject(DWORD dwPrj)                      { return Execute("RemoveProject", dwPrj) != 0; }
    void DebugProject(DWORD dwPrj, BOOL bDebug = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "DebugProject";

        CString sParam1;
        sParam1.Format("%ld", dwPrj);
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bDebug?"1":"0";

        Execute(3, argv);
    }
    BOOL DisplayIO(BOOL bDisplay = TRUE)                 { return Execute("DisplayIO", bDisplay) != 0; }
    LPCSTR GetItemTypeSel(void)                          { return (LPCSTR)Execute("GetItemTypeSel"); }
    BOOL CanCreateItem(int iType, int iSubType = 0)      { return Execute("CanCreateItem", iType, iSubType) != 0; }
    BOOL CreateItem(int iType, int iSubType = 0, LPCSTR szDll = NULL)
    {
        LPSTR argv[4];

        argv[0] = "CreateItem";

        CString sParam1;
        sParam1.Format("%d", iType);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iSubType);
        argv[2] = sParam2.GetBuffer(255);

        argv[3] = (LPSTR)szDll;

        return Execute(4, argv) != 0;
    }
    void EditInPlace(BOOL bEdit = TRUE)                  { Execute("EditInPlace", bEdit); }
    LPCRECT PrintFolioEx(int iFolioX, int iFolioY, HDC hDC, int iLeftPos, int iTopPos)
    {
        LPSTR argv[6];

        argv[0] = "PrintFolioEx";

        CString sParam1;
        sParam1.Format("%d", iFolioX);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iFolioY);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
#ifdef _WIN64
        sParam3.Format("%I64u", (unsigned __int64)hDC);
#else
        sParam3.Format("%ld", (long)hDC);
#endif
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%d", iLeftPos);
        argv[4] = sParam4.GetBuffer(255);

        CString sParam5;
        sParam5.Format("%d", iTopPos);
        argv[5] = sParam5.GetBuffer(255);

        return (LPCRECT)Execute(6, argv);
    }
    int GetNbVertFolioEx(void)                           { return (int)Execute("GetNbVertFolioEx"); }
    int GetNbHorzFolioEx(void)                           { return (int)Execute("GetNbHorzFolioEx"); }
    BOOL SetPrinter(int iDPIX, int iDPIY, int iFolioWidth, int iFolioHeight)
    {
        LPSTR argv[5];

        argv[0] = "SetPrinter";

        CString sParam1;
        sParam1.Format("%d", iDPIX);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iDPIY);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
        sParam3.Format("%d", iFolioWidth);
        argv[3] = sParam3.GetBuffer(255);

        CString sParam4;
        sParam4.Format("%d", iFolioHeight);
        argv[4] = sParam4.GetBuffer(255);

        return Execute(5, argv) != 0;
    }
    void DisplayFolio(BOOL bDisplay = TRUE)              { Execute("DisplayFolio", bDisplay); }
    BOOL IsDisplayFolio(void)                            { return Execute("IsDisplayFolio") != 0; }
    BOOL SetCallback(int iCallBack, void* pfCB, void* pData)
    {
        LPSTR argv[4];

        argv[0] = "SetCallback";

        CString sParam1;
        sParam1.Format("%d", iCallBack);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)pfCB);
#else
        sParam2.Format("%ld", (long)pfCB);
#endif
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
#ifdef _WIN64
        sParam3.Format("%I64u", (unsigned __int64)pData);
#else
        sParam3.Format("%ld", (long)pData);
#endif
        argv[3] = sParam3.GetBuffer(255);

        return Execute(4, argv) != 0;
    }
    BOOL AdjustFolio(int iMaxFolioX, int iMaxFolioY)     { return Execute("AdjustFolio", iMaxFolioX, iMaxFolioY) != 0; }
    HTREEITEM MoveItemAfter(HTREEITEM hTarget, HTREEITEM hItem)
    {
        LPSTR argv[3];

        argv[0] = "MoveItemAfter";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hTarget);
#else
        sParam1.Format("%ld", (long)hTarget);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam2.Format("%ld", (long)hItem);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return (HTREEITEM)Execute(3, argv);
    }
    BOOL IsItemExpanded(HTREEITEM hItem1)                { return Execute("IsItemExpanded", hItem1) != 0; }
    void ExpandItem(HTREEITEM hItem, BOOL bExpand = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "ExpandItem";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bExpand?"1":"0";

        Execute(3, argv);
    }
    BOOL SetTreeIcon(HBITMAP hBmp, int cx)
    {
        LPSTR argv[3];

        argv[0] = "SetTreeIcon";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hBmp);
#else
        sParam1.Format("%ld", (long)hBmp);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", cx);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    void RemoveBkp(LPCSTR szLocation)                    { Execute("RemoveBkp", szLocation); }
    BOOL EditPropAfterInsertVar(BOOL bInsert = TRUE)     { return Execute("EditPropAfterInsertVar", bInsert) != 0; }
    int SetChildOffset(int iOffset)                      { return (int)Execute("SetChildOffset", iOffset); }
    DWORD GetScreenID(void)                              { return (DWORD)Execute("GetScreenID"); }
    void SetFirstNetworkTag(int iFirstTag)               { Execute("SetFirstNetworkTag", iFirstTag); }
    DWORD GetItemGUID(int iX, int iY)                    { return (DWORD)Execute("GetItemGUID", iX, iY); }
    BOOL LockItemType(BOOL bLock, DWORD dwPrj, DWORD dwItemType, LPCSTR szPath = NULL)
    {
        LPSTR argv[5];

        argv[0] = "LockItemType";

        argv[1] = bLock?"1":"0";

        CString sParam2;
        sParam2.Format("%ld", dwPrj);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
        sParam3.Format("%ld", dwItemType);
        argv[3] = sParam3.GetBuffer(255);

        argv[4] = (LPSTR)szPath;

        return Execute(5, argv) != 0;
    }
    void SetScreenID(DWORD dwID)                         { Execute("SetScreenID", dwID); }
    void SetRules(LPCSTR szRules)                        { Execute("SetRules", szRules); }
    LPCSTR GetRules(void)                                { return (LPCSTR)Execute("GetRules"); }
    HTREEITEM GetNextVisibleItem(HTREEITEM hItem)        { return (HTREEITEM)Execute("GetNextVisibleItem", hItem); }
    LPCSTR GetInterfaces(WORD wStyle)                    { return (LPCSTR)Execute("GetInterfaces", wStyle); }
    void SetFilter(DWORD dwFilter)                       { Execute("SetFilter", dwFilter); }
    BOOL CanConnect(void)                                { return Execute("CanConnect") != 0; }
    BOOL Connect(void)                                   { return Execute("Connect") != 0; }
    BOOL SetAutoConnectVariable(BOOL bAuto = TRUE)       { return Execute("SetAutoConnectVariable", bAuto) != 0; }
    BOOL InsertVarAfterInsertFB(BOOL bInsert = TRUE)     { return Execute("InsertVarAfterInsertFB", bInsert) != 0; }
    DWORD GetItemSubType(void)                           { return (DWORD)Execute("GetItemSubType"); }
    void SetPrinterSetup(str_W5PrintFolioSetup* pStrSetup)
    {
        LPSTR argv[2];

        argv[0] = "SetPrinterSetup";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)pStrSetup);
#else
        sParam1.Format("%ld", (long)pStrSetup);
#endif
        argv[1] = sParam1.GetBuffer(255);

        Execute(2, argv);
    }
    BOOL SetSFCSettings(str_W5SFCSettings* pStrSettings)
    {
        LPSTR argv[2];

        argv[0] = "SetSFCSettings";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)pStrSettings);
#else
        sParam1.Format("%ld", (long)pStrSettings);
#endif
        argv[1] = sParam1.GetBuffer(255);

        return Execute(2, argv) != 0;
    }
    BOOL CanRotateCorners(void)                          { return Execute("CanRotateCorners") != 0; }
    void RotateCorners(int iSym)                         { Execute("RotateCorners", iSym); }
    long SearchIn(str_W5FindReplace* pStrFR)
    {
        LPSTR argv[2];

        argv[0] = "SearchIn";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)pStrFR);
#else
        sParam1.Format("%ld", (long)pStrFR);
#endif
        argv[1] = sParam1.GetBuffer(255);

        return (long)Execute(2, argv);
    }
    void SetDisplay(WORD wDisplay)                       { Execute("SetDisplay", wDisplay); }
    WORD GetDisplay(void)                                { return (WORD)Execute("GetDisplay"); }
    BOOL CanRemoveComment(void)                          { return Execute("CanRemoveComment") != 0; }
    void RemoveComment(void)                             { Execute("RemoveComment"); }
    void SetGraphicProperties(LPCSTR szGraObject, LPCSTR szProperties)
    {
        LPSTR argv[3];

        argv[0] = "SetGraphicProperties";

        argv[1] = (LPSTR)szGraObject;

        argv[2] = (LPSTR)szProperties;

        Execute(3, argv);
    }
    void SetTracePoint(LPCSTR szLocation)                { Execute("SetTracePoint", szLocation); }
    BOOL HasTracepoint(void)                             { return Execute("HasTracepoint") != 0; }
    BOOL SetBkpEx(LPCSTR szLocation, DWORD dwType)
    {
        LPSTR argv[3];

        argv[0] = "SetBkpEx";

        argv[1] = (LPSTR)szLocation;

        CString sParam2;
        sParam2.Format("%ld", dwType);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    LPCSTR GetBkpList(void)                              { return (LPCSTR)Execute("GetBkpList"); }
    BOOL SetSFCSettings2(DWORD dwFlags)                  { return Execute("SetSFCSettings2", dwFlags) != 0; }
    DWORD SetAutoEdit(DWORD dwAutoEdit)                  { return (DWORD)Execute("SetAutoEdit", dwAutoEdit); }
    BOOL SetItemImage(HTREEITEM hItem, int iImage)
    {
        LPSTR argv[3];

        argv[0] = "SetItemImage";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iImage);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL SetItemTooltip(HTREEITEM hItem, LPCSTR szTooltip)
    {
        LPSTR argv[3];

        argv[0] = "SetItemTooltip";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = (LPSTR)szTooltip;

        return Execute(3, argv) != 0;
    }
    int GetDBID(DWORD* pArrID)
    {
        LPSTR argv[2];

        argv[0] = "GetDBID";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)pArrID);
#else
        sParam1.Format("%ld", (long)pArrID);
#endif
        argv[1] = sParam1.GetBuffer(255);

        return (int)Execute(2, argv);
    }
    LPCSTR GetInputBlockVar(int iPin)                    { return (LPCSTR)Execute("GetInputBlockVar", iPin); }
    BOOL SetInfos(DWORD dwInfo, void* pInfos)
    {
        LPSTR argv[3];

        argv[0] = "SetInfos";

        CString sParam1;
        sParam1.Format("%ld", dwInfo);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)pInfos);
#else
        sParam2.Format("%ld", (long)pInfos);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL GetInfos(DWORD dwInfo, void* pInfos)
    {
        LPSTR argv[3];

        argv[0] = "GetInfos";

        CString sParam1;
        sParam1.Format("%ld", dwInfo);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)pInfos);
#else
        sParam2.Format("%ld", (long)pInfos);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL HideIOName(BOOL bHide = TRUE)                   { return Execute("HideIOName", bHide) != 0; }
    LPCRECT GetFolioRect(int iFolioX, int iFolioY)       { return (LPCRECT)Execute("GetFolioRect", iFolioX, iFolioY); }
    LPCSTR GetHeaderXY(int iX, int iY, LPCRECT rctLog)
    {
        LPSTR argv[4];

        argv[0] = "GetHeaderXY";

        CString sParam1;
        sParam1.Format("%d", iX);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%d", iY);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
#ifdef _WIN64
        sParam3.Format("%I64u", (unsigned __int64)rctLog);
#else
        sParam3.Format("%ld", (long)rctLog);
#endif
        argv[3] = sParam3.GetBuffer(255);

        return (LPCSTR)Execute(4, argv);
    }
    LPCSTR GetSymbolInFolio(int iX, int iY)              { return (LPCSTR)Execute("GetSymbolInFolio", iX, iY); }
    BOOL SetItemBitmap(HTREEITEM hItem, HBITMAP hBmp)
    {
        LPSTR argv[3];

        argv[0] = "SetItemBitmap";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
#ifdef _WIN64
        sParam2.Format("%I64u", (unsigned __int64)hBmp);
#else
        sParam2.Format("%ld", (long)hBmp);
#endif
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL CanShowSpy(void)                                { return Execute("CanShowSpy") != 0; }
    BOOL IsSpyVisible(void)                              { return Execute("IsSpyVisible") != 0; }
    void ShowSpy(BOOL bShow = TRUE)                      { Execute("ShowSpy", bShow); }
    BOOL CanEditPins(void)                               { return Execute("CanEditPins") != 0; }
    BOOL EditPins(void)                                  { return Execute("EditPins") != 0; }
    BOOL UnderlineGlobal(BOOL bUnderline = TRUE)         { return Execute("UnderlineGlobal", bUnderline) != 0; }
    BOOL IsBookmark(void)                                { return Execute("IsBookmark") != 0; }
    void SetBookmark(BOOL bSet = TRUE)                   { Execute("SetBookmark", bSet); }
    void GoNextBookmark(void)                            { Execute("GoNextBookmark"); }
    void GoPrevBookmark(void)                            { Execute("GoPrevBookmark"); }
    void DeleteAllBookmark(void)                         { Execute("DeleteAllBookmark"); }
    BOOL CanPreview(void)                                { return Execute("CanPreview") != 0; }
    BOOL IsPreview(void)                                 { return Execute("IsPreview") != 0; }
    void TogglePreview(void)                             { Execute("TogglePreview"); }
    BOOL DeleteItem(HTREEITEM hItem)                     { return Execute("DeleteItem", hItem) != 0; }
    BOOL SetPropGroup(int iProp, LPCSTR szGroup)         { return Execute("SetPropGroup", iProp, szGroup) != 0; }
    void SetMaxWidth(int iMaxWidth)                      { Execute("SetMaxWidth", iMaxWidth); }
    void SetMaxHeight(int iMaxHeight)                    { Execute("SetMaxHeight", iMaxHeight); }
    BOOL EditStructure(void)                             { return Execute("EditStructure") != 0; }
    BOOL SetBW(BOOL bBackWhite = TRUE)                   { return Execute("SetBW", bBackWhite) != 0; }
    BOOL SnapFBDGrid(BOOL bSnap = TRUE)                  { return Execute("SnapFBDGrid", bSnap) != 0; }
    BOOL DrawFBDBridge(BOOL bDraw = TRUE)                { return Execute("DrawFBDBridge", bDraw) != 0; }
    BOOL DlgCreateVar(LPCSTR szVar = NULL, DWORD dwFlags = 0)
    {
        LPSTR argv[3];

        argv[0] = "DlgCreateVar";

        argv[1] = (LPSTR)szVar;

        CString sParam2;
        sParam2.Format("%ld", dwFlags);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL SetPropFilter(int iProp, LPCSTR szFilter)       { return Execute("SetPropFilter", iProp, szFilter) != 0; }
    void AutoComplete(void)                              { Execute("AutoComplete"); }
    BOOL IsPropReadOnly(int iProp)                       { return Execute("IsPropReadOnly", iProp) != 0; }
    BOOL CanFindVariables(void)                          { return Execute("CanFindVariables") != 0; }
    int FindVariables(void)                              { return (int)Execute("FindVariables"); }
    BOOL CanRenameVariables(void)                        { return Execute("CanRenameVariables") != 0; }
    int RenameVariables(void)                            { return (int)Execute("RenameVariables"); }
    BOOL CanForceInitValue(void)                         { return Execute("CanForceInitValue") != 0; }
    void ForceInitValue(void)                            { Execute("ForceInitValue"); }
    LPCSTR FindVarInput(LPCSTR szInput)                  { return (LPCSTR)Execute("FindVarInput", szInput); }
    BOOL CanGenerateShared(void)                         { return Execute("CanGenerateShared") != 0; }
    BOOL GenerateShared(void)                            { return Execute("GenerateShared") != 0; }
    long IsComment(int iX, int iY)                       { return (long)Execute("IsComment", iX, iY); }
    long IsMacro(int iX, int iY)                         { return (long)Execute("IsMacro", iX, iY); }
    BOOL ForcePrintZoom(BOOL bForce, int iZoom = 100)
    {
        LPSTR argv[3];

        argv[0] = "ForcePrintZoom";

        argv[1] = bForce?"1":"0";

        CString sParam2;
        sParam2.Format("%d", iZoom);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL HideOptionDlgVar(BOOL bHide = TRUE)             { return Execute("HideOptionDlgVar", bHide) != 0; }
    HTREEITEM GetFirstVisibleItem(void)                  { return (HTREEITEM)Execute("GetFirstVisibleItem"); }
    BOOL CanIndent(void)                                 { return Execute("CanIndent") != 0; }
    BOOL Indent(void)                                    { return Execute("Indent") != 0; }
    void RemoveStyle(DWORD dwStyle)                      { Execute("RemoveStyle", dwStyle); }
    BOOL CanFilter(DWORD dwFilter)                       { return Execute("CanFilter", dwFilter) != 0; }
    BOOL SetItemBreakpoint(HTREEITEM hItem = NULL, DWORD dwBkpType = BKP_NONE)
    {
        LPSTR argv[3];

        argv[0] = "SetItemBreakpoint";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", dwBkpType);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
    BOOL SetItemCurPos(HTREEITEM hItem = NULL)           { return Execute("SetItemCurPos", hItem) != 0; }
    BOOL SetCsvSeparatorComa(BOOL bSetComa = TRUE)       { return Execute("SetCsvSeparatorComa", bSetComa) != 0; }
    BOOL CanGroupVar(void)                               { return Execute("CanGroupVar") != 0; }
    BOOL GroupVar(BOOL bGroup = TRUE)                    { return Execute("GroupVar", bGroup) != 0; }
    BOOL SelectTreeItem(HTREEITEM hItem, BOOL bDeselectAll = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "SelectTreeItem";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bDeselectAll?"1":"0";

        return Execute(3, argv) != 0;
    }
    BOOL EnableNotif(WORD wNotif, BOOL bEnable = TRUE)
    {
        LPSTR argv[3];

        argv[0] = "EnableNotif";

        CString sParam1;
        sParam1.Format("%d", wNotif);
        argv[1] = sParam1.GetBuffer(255);

        argv[2] = bEnable?"1":"0";

        return Execute(3, argv) != 0;
    }
    BOOL SetTooltipInfosEx(BOOL bEdit, BOOL bSet, int iTooltipInfo)
    {
        LPSTR argv[4];

        argv[0] = "SetTooltipInfosEx";

        argv[1] = bEdit?"1":"0";

        argv[2] = bSet?"1":"0";

        CString sParam3;
        sParam3.Format("%d", iTooltipInfo);
        argv[3] = sParam3.GetBuffer(255);

        return Execute(4, argv) != 0;
    }
    HTREEITEM SelectItemType(DWORD dwPrj, DWORD dwItemType, DWORD dwObject = 0, LPCSTR szPath = NULL)
    {
        LPSTR argv[5];

        argv[0] = "SelectItemType";

        CString sParam1;
        sParam1.Format("%ld", dwPrj);
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", dwItemType);
        argv[2] = sParam2.GetBuffer(255);

        CString sParam3;
        sParam3.Format("%ld", dwObject);
        argv[3] = sParam3.GetBuffer(255);

        argv[4] = (LPSTR)szPath;

        return (HTREEITEM)Execute(5, argv);
    }
    BOOL SetItemImageStatus(HTREEITEM hItem, DWORD dwStatus = 0)
    {
        LPSTR argv[3];

        argv[0] = "SetItemImageStatus";

        CString sParam1;
#ifdef _WIN64
        sParam1.Format("%I64u", (unsigned __int64)hItem);
#else
        sParam1.Format("%ld", (long)hItem);
#endif
        argv[1] = sParam1.GetBuffer(255);

        CString sParam2;
        sParam2.Format("%ld", dwStatus);
        argv[2] = sParam2.GetBuffer(255);

        return Execute(3, argv) != 0;
    }
//END_WRAP_C++
};

class CW5Ctrl : public CW5EditWrapApi
{
public:
    //construction / destruction
	CW5Ctrl()            {}
    virtual ~CW5Ctrl()   {}

    //creation
    virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
    {
        if ( Init(m_sDll, m_sTLType) )
            return CW5EditWrapApi::Create(dwStyle, rect, pParentWnd, nID);
        return FALSE;
    }

protected:
    CString m_sDll;
    CString m_sTLType;
};

class CW5FBDCtrl : public CW5Ctrl
{
public:
	CW5FBDCtrl()            { m_sDll = "W5EditFBD.dll"; }
    virtual ~CW5FBDCtrl()   {}
};
class CW5LDCtrl : public CW5Ctrl
{
public:
	CW5LDCtrl()            { m_sDll = "W5EditLD.dll"; }
    virtual ~CW5LDCtrl()   {}
};
class CW5STCtrl : public CW5Ctrl
{
public:
	CW5STCtrl()            { m_sDll = "W5EditST.dll"; }
    virtual ~CW5STCtrl()   {}
};
class CW5SFCCtrl : public CW5Ctrl
{
public:
	CW5SFCCtrl()            { m_sDll = "W5EditSFC.dll"; }
    virtual ~CW5SFCCtrl()   {}
};
class CW5FFSFCCtrl : public CW5Ctrl
{
public:
	CW5FFSFCCtrl()          
    { 
        m_sDll = "W5EditFF.dll";
        m_sTLType = GUID_SFC;
    }
    virtual ~CW5FFSFCCtrl() {}
};
class CW5SAMACtrl : public CW5Ctrl
{
public:
	CW5SAMACtrl()          
    { 
        m_sDll = "W5EditFF.dll";
        m_sTLType = GUID_SAMA;
    }
    virtual ~CW5SAMACtrl() {}
};
class CW5ACTCtrl : public CW5Ctrl
{
public:
	CW5ACTCtrl()            { m_sDll = "W5EditACT.dll"; }
    virtual ~CW5ACTCtrl()   {}
};
class CW5FFLDCtrl : public CW5Ctrl
{
public:
	CW5FFLDCtrl()            { m_sDll = "W5EditFFLD.dll"; }
    virtual ~CW5FFLDCtrl()   {}
};
class CW5AnalyserCtrl : public CW5Ctrl
{
public:
	CW5AnalyserCtrl()            { m_sDll = "W5EditAnalyser.dll"; }
    virtual ~CW5AnalyserCtrl()   {}
};
class CW5HMICtrl : public CW5Ctrl
{
public:
	CW5HMICtrl()            { m_sDll = "W5EditHMI.dll"; }
    virtual ~CW5HMICtrl()   {}
};




