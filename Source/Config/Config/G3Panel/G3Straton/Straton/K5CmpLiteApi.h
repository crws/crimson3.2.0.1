// K5 Compiler - exports

// K5CMP Server API

#if !defined(_K5CMPLITEAPI_H__INCLUDED_)
#define _K5CMPLITEAPI_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

#define K5CMP_APICALL __declspec(dllimport)

#define IGNORE_WRAPPER	0

/////////////////////////////////////////////////////////////////////////////
// compiler - build

BOOL K5CMP_APICALL K5CmpNeedBuild		// test if a project needs to be compiled
	(
	LPCSTR szProjectPath,				// pathname of the project
	int    iCodeType					// reserved - must be 0
	);									// TRUE if project code is obsolete

void K5CMP_APICALL K5CmpCleanProject	// Clean the project
	(
	LPCSTR szProjectPath				// pathname of the project
	);

BOOL K5CMP_APICALL K5CmpBuildDefault	// build the application - default setting
	(
	LPCSTR szProjectPath,				// pathname of the project
	HWND   hwndOutputListbox			// handle of a LISTBOX output
	);									// returns TRUE if no error

BOOL K5CMP_APICALL K5CmpBuildDefaultFileReport // build the application
	(
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szReportFile                 // pathname of the report file
	);

BOOL K5CMP_APICALL K5CmpBuildProject
	(
	LPCSTR szProjectPath,
	int    iCodeType,
	LPCSTR szTargetName,
	LPCSTR szCodeSuffix,
	BOOL   bIntelEndian,
	HWND   hwndOutputListbox,
	FILE * fOutputFile,
	BOOL   bTraceFile
	);

BOOL K5CMP_APICALL K5CmpCheckProgram	// compile one program (check)
	(
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szProgramName,				// name of the program
	int    iCodeType,					// reserved - must be 0
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * f				            // reserved - mst be NULL
	);									// returns TRUE if no error

BOOL K5CMP_APICALL K5CmpConversionNeedBuild // test if conversion is possible
	(
	LPCSTR szProjectPath,    // project pathname
	LPCSTR szProgram,        // program name
	DWORD  dwLanguage        // new language (K5DBLANG...)
	); // return TRUE if conversion is possible

BOOL K5CMP_APICALL K5CmpCanConvertProgram // test if conversion is possible
	(
	LPCSTR szProjectPath,    // project pathname
	LPCSTR szProgram,        // program name
	DWORD  dwLanguage        // new language (K5DBLANG...)
	); // return TRUE if conversion is possible

BOOL K5CMP_APICALL K5CMP_SLCheck        // check license
    (
    DWORD *pdwNbIO,                     // OUT: mab nb IOs
    LPSTR pszOEM                        // OUT: OEM code - must be char[16]
    ); // TRUE if licensed

/////////////////////////////////////////////////////////////////////////////
// variable import/export
 
#define K5CMP_VTEXT_IEC     0
#define K5CMP_VTEXT_XML     1
#define K5CMP_VTEXT_CSV     2
 
#define K5CMP_VTEXT_SORT    0x0001  // export: sort by name
#define K5CMP_VTEXT_NOLOCK  0x0002  // export: omit locked variables
 
LPCSTR K5CMP_APICALL K5CmpExportVariables
    (
       LPCSTR szProjectPath,   // project path name
       LPCSTR szGroupName,     // group name
    DWORD  dwSyntax,        // syntax selection
    DWORD  dwFlags          // options
    );
 
BOOL K5CMP_APICALL K5CmpImportVariables
    (
       LPCSTR szProjectPath,   // project path name
       LPCSTR szGroupName,     // group name
    LPCSTR szText,          // input text
    LPCSTR szErrFile,       // error report file name (or NULL)
    DWORD  dwSyntax,        // syntax selection
    DWORD  dwFlags          // reserved for extensions
    );
 
/////////////////////////////////////////////////////////////////////////////
// helpers

typedef BOOL   (*PFK5CMP_NeedBuild)(LPCSTR,int);
typedef void   (*PFK5CMP_CleanProject)(LPCSTR);
typedef BOOL   (*PFK5CMP_BuildDefault)(LPCSTR,HWND);
typedef BOOL   (*PFK5CMP_BuildDefaultFileReport)(LPCSTR,LPCSTR);
typedef BOOL   (*PFK5CMP_CheckProgram)(LPCSTR,LPCSTR,int,HWND,FILE*);
typedef BOOL   (*PFK5CMP_SLCheck)(DWORD *,LPSTR);
typedef BOOL   (*PFK5CMP_BuildProject)(LPCSTR,int,LPCSTR,LPCSTR,BOOL,HWND,FILE*,BOOL);

typedef BOOL   (*PFK5CMP_ConversionNeedBuild)(LPCSTR,LPCSTR,DWORD);
typedef BOOL   (*PFK5CMP_CanConvertProgram)(LPCSTR,LPCSTR,DWORD);
typedef BOOL   (*PFK5CMP_ConvertProgram)(LPCSTR,LPCSTR,DWORD,DWORD);

#define GETK5CMPPROC_NeedBuild(h) \
    ((PFK5CMP_NeedBuild)GetProcAddress((h), "K5CmpNeedBuild"))
#define GETK5CMPPROC_CleanProject(h) \
    ((PFK5CMP_CleanProject)GetProcAddress((h), "K5CmpCleanProject"))
#define GETK5CMPPROC_BuildDefault(h) \
    ((PFK5CMP_BuildDefault)GetProcAddress((h), "K5CmpBuildDefault"))
#define GETK5CMPPROC_BuildDefaultFileReport(h) \
    ((PFK5CMP_BuildDefaultFileReport)GetProcAddress((h), "K5CmpBuildDefaultFileReport"))
#define GETK5CMPPROC_CheckProgram(h) \
    ((PFK5CMP_CheckProgram)GetProcAddress((h), "K5CmpCheckProgram"))
#define GETK5CMPPROC_ConversionNeedBuild(h) \
    ((PFK5CMP_ConversionNeedBuild)GetProcAddress((h), "K5CmpConversionNeedBuild"))
#define GETK5CMPPROC_CanConvertProgram(h) \
    ((PFK5CMP_CanConvertProgram)GetProcAddress((h), "K5CmpCanConvertProgram"))
#define GETK5CMPPROC_ConvertProgram(h) \
    ((PFK5CMP_ConvertProgram)GetProcAddress((h), "K5CmpConvertProgram"))
#define GETK5CMPPROC_SLCheck(h) \
    ((PFK5CMP_SLCheck)GetProcAddress((h), "K5CMP_SLCheck"))
#define GETK5CMPPROC_BuildProject(h) \
    ((PFK5CMP_BuildProject)GetProcAddress((h), "K5CmpBuildProject"))

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#ifndef IGNORE_WRAPPER

/////////////////////////////////////////////////////////////////////////////
// wrapper class

#ifdef __cplusplus

class CK5CmpLiteApi
{
public:
    CK5CmpLiteApi (void)
    {
        _m_hLib = LoadLibrary ("Straton\\K5CMP.DLL");
        if (_m_hLib)
        {
            _m_pfNeedBuild = GETK5CMPPROC_NeedBuild (_m_hLib);
            _m_pfCleanProject = GETK5CMPPROC_CleanProject (_m_hLib);
            _m_pfBuildDefault = GETK5CMPPROC_BuildDefault (_m_hLib);
            _m_pfCheckProgram = GETK5CMPPROC_CheckProgram (_m_hLib);
            _m_pfBuildProject = GETK5CMPPROC_BuildProject (_m_hLib);
            _m_pfBuildDefaultFileReport = GETK5CMPPROC_BuildDefaultFileReport (_m_hLib);
        }
        else
        {
            _m_pfNeedBuild = NULL;
            _m_pfCleanProject = NULL;
            _m_pfBuildDefault = NULL;
            _m_pfCheckProgram = NULL;
            _m_pfBuildProject = NULL;
            _m_pfBuildDefaultFileReport = NULL;
        }
    }
    virtual ~CK5CmpLiteApi (void)
    {
        if (_m_hLib)
        {
            FreeLibrary (_m_hLib);
            _m_pfNeedBuild = NULL;
            _m_pfCleanProject = NULL;
            _m_pfBuildDefault = NULL;
            _m_pfCheckProgram = NULL;
            _m_pfBuildProject = NULL;
        }
    }

public:
    BOOL ProjectNeedBuild (LPCSTR szProjectPath)
    {
        if (_m_pfNeedBuild == NULL)
            return FALSE;
        return _m_pfNeedBuild (szProjectPath, 0);
    }
    void CleanProject (LPCSTR szProjectPath)
    {
        if (_m_pfCleanProject != NULL)
            _m_pfCleanProject (szProjectPath);
    }
    BOOL BuildProject (LPCSTR szProjectPath, HWND hwndOutputListbox)
    {
        if (_m_pfBuildDefault == NULL)
            return FALSE;
        return _m_pfBuildDefault (szProjectPath, hwndOutputListbox);
    }
    BOOL BuildProject (LPCSTR szProjectPath, FILE *fOut)
    {
        if (_m_pfBuildProject == NULL)
            return FALSE;
        return _m_pfBuildProject (szProjectPath, 0, NULL, NULL, FALSE, NULL, fOut, FALSE);
    }
    BOOL BuildProject (LPCSTR szProjectPath, LPCSTR szReportFile)
    {
        if (_m_pfBuildDefaultFileReport != NULL)
            return _m_pfBuildDefaultFileReport (szProjectPath, szReportFile);
        if (_m_pfBuildProject == NULL)
            return FALSE;
        FILE *f = fopen (szReportFile, "wb");
        if (f == NULL)
            return FALSE;
        BOOL rc = _m_pfBuildProject (szProjectPath, 0, NULL, NULL, FALSE, NULL, f, FALSE);
        fclose (f);
        return rc;
    }
    BOOL CheckProgram (LPCSTR szProjectPath, LPCSTR szProgramName, HWND hwndOutputListbox)
    {
        if (_m_pfCheckProgram == NULL)
            return FALSE;
        return _m_pfCheckProgram (szProjectPath, szProgramName, 0, hwndOutputListbox, NULL);
    }

private:
    HINSTANCE _m_hLib;
    PFK5CMP_NeedBuild _m_pfNeedBuild;
    PFK5CMP_CleanProject _m_pfCleanProject;
    PFK5CMP_BuildDefault _m_pfBuildDefault;
    PFK5CMP_BuildDefaultFileReport _m_pfBuildDefaultFileReport;
    PFK5CMP_CheckProgram _m_pfCheckProgram;
    PFK5CMP_BuildProject _m_pfBuildProject;
};

#endif

#endif // __cplusplus

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_K5CMPLITEAPI_H__INCLUDED_)
