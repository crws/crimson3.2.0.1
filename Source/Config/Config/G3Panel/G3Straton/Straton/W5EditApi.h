#if !defined(_W5EDITAPI_H_)
#define _W5EDITAPI_H_

/* exchange structures */
typedef struct _s_W5FB {
	char	sName[255];
	DWORD	dwID;               //no longer used, can be null
    int     iNbIn;
    int     iNbOut;
    BOOL    bInstanciable;
    BOOL    bDBObject;
	DWORD	dwData;				// reserved for extension		
} str_W5FB;

typedef struct _s_W5Variable {
	LPCSTR	sName;
    BOOL    bLocal;
	DWORD	dwID;
	LPCSTR	sShortComment;
	LPCSTR	sLongComment;
	LPCSTR	sIOName;
	DWORD	dwData;				// reserved for extension		
} str_W5Variable;

#define CLIENTMSG_COMMAND   0
#define CLIENTMSG_NOTIF     1
typedef struct _s_W5ClientMsg { //used to send message to Control Debug Client 
	HWND	hWndFrom;
	char	szKingOf[32];
	int argc;
	char argv[7][257];
    LRESULT lRet;
    WORD wType;
	DWORD	dwData;				// reserved for extension		
} str_W5ClientMsg;

//structure used by interface "SetTooltipInfos"
typedef struct _s_W5TooltipInfos {
    BOOL bName;
    BOOL bType;
    BOOL bDim;
    BOOL bAttrib;
    BOOL bProps;
    BOOL bSyb;
    BOOL bInitValue;
    BOOL bTag;
    BOOL bDesc;
} str_W5TooltipInfos;

//structure used by interface "SetPrinterSetup"
typedef struct _s_W5PrintFolioSetup {
    BOOL bBW; //indicates that print is in black and white
} str_W5PrintFolioSetup;

//structure used by interface "SetSFCSettings"
typedef struct _s_W5SFCSettings {
    BOOL bNotifLvl2; //indicates that user can open a qualifier when double clicks
} str_W5SFCSettings;
#define SFC_NOFIFLVL2       0x00000001

//structure used by interfaces "SetInfos" and "GetInfos"
#define W5EDITINFOS_NONE        0
#define W5EDITINFOS_GRID        1
typedef struct _s_W5INFOGRID {
	BOOL bShow;
	BOOL bSnapToGrid;
    int  iStepGrid;
	DWORD	dwData;				// reserved for extension		
} str_W5InfosGrid;
#define W5EDITINFOS_STLINENB    2
#define W5EDITINFOS_SOLUTION    3
#define W5EDITINFOS_STATUS      4
typedef struct _s_W5INFOSTATUS {
	BOOL bEditing;
	DWORD	dwData;				// reserved for extension		
} str_W5InfosStatus;


#define INFO_NAME           0   //index of tooltip infos
#define INFO_TYPE           1
#define INFO_DIM            2
#define INFO_ATTRIB         3
#define INFO_PROPS          4
#define INFO_SYB            5
#define INFO_INITVALUE      6
#define INFO_TAG            7
#define INFO_DESC           8
#define MAX_TOOLTIPINFOS    9

//wrap defines (used only by GetInterfaces command)
#define WRAPTYPE_CSV    0
#define WRAPTYPE_C      1
#define WRAPTYPE_CS     2
#define WRAPTYPE_XML    3

//flags used by interface "SetAutoEdit"
#define AUTOEDIT_DEFAULT        0x00000000 //control will send all notifications and do not manage autoedit
#define AUTOEDIT_ALL            0xFFFFFFFF //control will send autoedit and won't send edit notifications
#define AUTOEDIT_SETVAR         0x00000001 //for W5EDITN_SETVAR and W5EDITN_SETVARFIRSTCHAR and W5EDITN_COMPLETION
#define AUTOEDIT_SETFB          0x00000002 //for W5EDITN_SETFB      (use the "W5EditRes.dll")
#define AUTOEDIT_EDITPROPS      0x00000004 //for W5EDITN_EDITPROPS  (use the "W5EditRes.dll")
#define AUTOEDIT_INITARRAYVAR   0x00000008 //for W5EDITN_INITARRAYVAR
#define AUTOEDIT_KEYWORD        0x00000010 //for W5EDITN_LISTKEYWORD
#define AUTOEDIT_GRAPROPS       0x00000020 //when double click on graphic item in W5EditACT.dll

//Zoom defines
#define W5ZOOM_IN         1
#define W5ZOOM_OUT        -1
#define W5ZOOM_AT         0
#define W5ZOOM_FIT        2
#define W5ZOOM_FITSEL     3

// flags: default: case insensitive - go down - loop at the end
#define W5FIND_WHOLEWORD  0x0001
#define W5REPLACE_ALL     0x0002  //when all occurences of substring has to be replaced
#define W5ASSIGNATION     0x0004  //search for assigned symbols(coil, := in ST...)
#define W5DONTLOOP        0x0008  //do not loop at the end of search
#define W5NEXTITEM        0x0010  //select next item
#define W5PREVITEM        0x0020  //select previous item
#define W5FOCUS           0x0040  //focus control when find string
#define W5ANYWHERE        0x0080  //search anywhere in line, not only at beginning
#define W5FIND_MATCHCASE  0x0100  //match case
#define W5FIND_VARPREFIX  0x0200  //when searching in dictionary, search only variable by prefix
#define W5FIND_SELECTION  0x0400  //when seraching/replacing in a control, do it only in current selection

//search flags for FreeForm Ladder (FFLD)
#define W5FINDFFLD_WHOLEWORD    0x0001  //search the whole word(s) only
#define W5FINDFFLD_MATCHCASE    0x0002  //search for the case sensitive string
#define W5FINDFFLD_ASSIGNATION  0x0004  //search for assigned symbols(coil, := in ST...)
#define W5FINDFFLD_DONTLOOP     0x0008  //do not loop at the end of document
#define W5FINDFFLD_BACKWARD     0x0010  //find and replace go to previous found item
#define W5FINDFFLD_JUMP         0x0020  //search in jump labels
#define W5FINDFFLD_FOCUS        0x0040  //focus control when find string
#define W5FINDFFLD_NETLABEL     0x0080  //search in network labels
#define W5FINDFFLD_DATAIN       0x0100  //search in data in
#define W5FINDFFLD_DATAOUT      0x0200  //search in data out
#define W5FINDFFLD_COIL         0x0400  //search in coils
#define W5FINDFFLD_CONTACT      0x0800  //search in contacts
#define W5FINDFFLD_FBTYPE       0x1000  //search in function block types (names)
#define W5FINDFFLD_FBINSTANCE   0x2000  //search in function block instances
#define W5FINDFFLD_NETTAG       0x4000  //search in network tags
#define W5FINDFFLD_COMMENT      0x8000  //search in comments
//NB: user can replace only variable/instance names, comments and jump labels

// commands
#define W5FIND_NEXT                0x0001   // find next
#define W5FIND_REPLACEANDNEXT      0x0002   // replace and find next
#define W5FIND_REPLACEALL          0x0004   // replace all found strings
#define W5FIND_ACTIVESTEP          0x0008   // find the active step in SFC
#define W5FIND_FIRST               0x0011   // find string including current selection

//results of search
#define W5RESULT_FIND       0x0001  //find the occurence
#define W5RESULT_REPLACED   0x0002  //replace the occurence

//others define
#define W5CONTENT_PROGRAM       1
#define W5CONTENT_LOCALDEFINE   2
#define W5CONTENT_COMMONDEFINE  3
#define W5CONTENT_GLOBALDEFINE  4
#define W5CONTENT_QUALIFIER     5
#define W5CONTENT_LIBDEFINE     6

//defines for "SetItemImageStatus" command
#define W5ITEMMASKBMP_NONE      0x00000000
#define W5ITEMMASKBMP_LOCK      0x00000001
#define W5ITEMMASKBMP_SC        0x00000002

//type item defines (used with command "GetItemType")
                                    //general description   FBD         FFLD        LD              SFC         ST
#define W5TYPE_NONE             0
#define W5TYPE_NETWORK          1   //                      break       network     not used        not used    not used
#define W5TYPE_COIL             2   // -( )-                                                        not used    not used
#define W5TYPE_COIL_I           3   // -(\)-                                                        not used    not used
#define W5TYPE_COIL_R           4   // -(R)-                                                        not used    not used
#define W5TYPE_COIL_S           5   // -(S)-                                                        not used    not used
#define W5TYPE_COIL_P           6   // -(P)-                                                        not used    not used
#define W5TYPE_COIL_N           7   // -(N)-                                                        not used    not used
#define W5TYPE_COMMENT          8   //                      comment     not used    comment line    note        not used
#define W5TYPE_CONTACT          9   // -] [-                                                        not used    not used
#define W5TYPE_CONTACT_I        10  // -]\[-                                                        not used    not used
#define W5TYPE_CONTACT_P        11  // -]P[-                                                        not used    not used
#define W5TYPE_CONTACT_N        12  // -]N[-                                                        not used    not used
#define W5TYPE_CONTACT_IP       13  // -]/P[-                                                       not used    not used
#define W5TYPE_CONTACT_IN       14  // -]/N[-                                                       not used    not used
#define W5TYPE_CORNER           15  //                      corner      not used    not used        not used    not used
#define W5TYPE_JUMP             16  //jump                                                                      not used
#define W5TYPE_RETURN           17  //RETURN                                                        not used    not used
#define W5TYPE_LABEL            18  //                      label       not used    rung header     not used    not used
#define W5TYPE_OR               19  //                      vert line   vert line   not used        not used    not used
#define W5TYPE_POWERL           20  //left power rail                                               not used    not used
#define W5TYPE_POWERR           21  //right power rail                  not used    not used        not used    not used
#define W5TYPE_VARIABLE         22  //                      variable    not used    not used        not used    not used
#define W5TYPE_BLOCK            23  //Function block                                                not used    used
#define W5TYPE_BLOCK_IN         24  //box input             not used                                not used    not used
#define W5TYPE_BLOCK_INI        25  //negated box input     not used                                not used    not used
#define W5TYPE_BLOCK_OUT        26  //box output            not used                                not used    not used
#define W5TYPE_BLOCK_OUTI       27  //negated box output    not used                                not used    not used
#define W5TYPE_LINE             28  //                      arc         horz line   horz line       not used    not used     
#define W5TYPE_STEP             29  // sfc step             not used    not used    not used                    not used
#define W5TYPE_MACRO            30  // sfc macro            not used    not used    not used                    not used
#define W5TYPE_TRANS            31  //sfc transition        not used    not used    not used                    not used
#define W5TYPE_PROJECT          32  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONPRG       33  //                      not used    not used    not used        not used    not used 
#define W5TYPE_PRG              34  //                      not used    not used    not used        not used    not used 
#define W5TYPE_FOLDER           35  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONFIELDBUS  36  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONPROFILES  37  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONIOS       38  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONGLOBALDEFINE 39 //                    not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONVARS      40  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONGRA       41  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONRCP       42  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONSPY       43  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONCURVE     44  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONSTRINGTABLE 45 //                     not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONSIGNAL    46  //                      not used    not used    not used        not used    not used 
#define W5TYPE_FILE             47  //                      not used    not used    not used        not used    not used 
#define W5TYPE_CONSTANT         48  //not used              not used    not used    not used        not used    used
#define W5TYPE_SUBPRG           49  //not used              not used    not used    not used        not used    used
#define W5TYPE_KEYWORD          50  //not used              not used    not used    not used        not used    used
#define W5TYPE_UDFB             51  //not used              not used    not used    not used        not used    used
#define W5TYPE_EXTERN           52  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONBINDING   53  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONHMI       54  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONHMISTRING 55  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONHMIBMP    56  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONHMIFONT   57  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONHMISCREEN 58  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SCREEN           59  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SECTIONTYPES     60  //                      not used    not used    not used        not used    not used 
#define W5TYPE_SCREENMASK       61  //                      not used    not used    not used        not used    not used  
#define W5TYPE_SECTIONIEC850    62  //                      not used    not used    not used        not used    not used 
#define W5TYPE_TOOLSMENU        63  //                      not used    not used    not used        not used    not used 
#define W5TYPE_ISTEP            64  // sfc initial step     not used    not used    not used                    not used
#define W5TYPE_GHOSTPROJECT     65  // used by multitreeprj for ghost projects
#define W5TYPE_MULTISPYLIST     66  // used by multitreeprj not used    not used    not used        not used    not used
#define W5TYPE_GLOBAL           67  // used by multitreeprj not used    not used    not used        not used    not used
#define W5TYPE_SECTIONMULTIBINDING 68//used by multitreeprj not used    not used    not used        not used    not used
#define W5TYPE_MULTISOFTSCOPE   69  // used by multitreeprj not used    not used    not used        not used    not used

//sub types for extern items
#define W5SUBTYPE_NONE          0
#define W5SUBTYPE_WEB           1
#define W5SUBTYPE_OEM           2
#define W5SUBTYPE_OTHER         3

typedef struct _s_W5FindReplace {
    LPCSTR  sStringToFind;
    LPCSTR  sStringToReplace;
    DWORD   dwCommand;
    DWORD   dwFlags;
    HWND    hwndListBox;
    FILE    *pFile;
    DWORD   dwData; // reserved for future
} str_W5FindReplace;

typedef struct _s_W5FindReplaceResult {
    int     iNetwork;
    POINT   ptLocation;
    BOOL    bReplaced;
    BOOL    bFound;
    char    sFound[255];

} str_W5FR_Result;

//call backs
typedef bool    (*K5CANCLEARVAR)                (HWND hWnd, unsigned long dwVar, void* pData); //used to test if var can be cleared
typedef bool    (*K5CLEARVARAFTEREDITCANCEL)    (HWND hWnd, unsigned long dwVar, void* pData); //used after user hit cancel in variable edit box
typedef bool    (*K5CANMOVEVAR)                 (HWND hWnd, unsigned long dwVar, DWORD dwGroupFrom, DWORD dwGroupTo, void* pData); //test if user can drag n drop var from group to another
typedef bool    (*K5DROPFB)                     (HWND hWnd, LPCSTR szFB, void* pData); //is called when user drop a function block fro library
typedef int     (*K5COMPARETREEITEM)            (DWORD_PTR dwData1, DWORD_PTR dwData2); //is called to compare two items during SortChildrenCB
typedef BOOL    (*K5CHECKCELLVALUE)             (HWND hWnd, DWORD_PTR hItem, int iCol, LPCSTR szValue, void* pData, BOOL *pbQuiet); //used to test if value is allowed in treelist item
typedef BOOL    (*K5CANCHANGEVAR)               (HWND hWnd, unsigned long dwVar, unsigned long dwInfoType, void* pData); //used to test if var can be modified by user

#define W5CB_NONE                       0
#define W5CB_CANCLEARVAR                1
#define W5CB_CLEARVARAFTERCANCELEDIT    2
#define W5CB_CANMOVEVAR                 3
#define W5CB_DROPFB                     4
#define W5CB_COMPARETREEITEM            5
#define W5CB_CHECKCELLVALUE             6
#define W5CB_CANCHANGEVAR               7

#define CANMODIFYVAR_NAME       0x0001 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_TYPE       0x0002 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_DIM        0x0004 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_ATTRIB     0x0008 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_PROPS      0x0010 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_SYB        0x0020 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_INITVALUE  0x0040 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_TAG        0x0080 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_DESC       0x0100 //define used by W5CB_CANCHANGEVAR callback
#define CANMODIFYVAR_USERGROUP  0x0200 //define used by W5CB_CANCHANGEVAR callback


//breakpoints defines (used by SetBkpEx command)
#define BKP_NONE            0
#define BKP_BREAKACTIVE     1
#define BKP_BREAKINACTIVE   2
#define BKP_TRACEACTIVE     3
#define BKP_TRACEINACTIVE   4

//command messages
#define WM_CTRL_BASEEVENT           (WM_USER)       /* DB event, dwID */
#define WM_CTRL_MWEVENT             (WM_USER+1)     /* MW event, dwID */
#define WM_CTRL_SETID               (WM_USER+2)     /* 0, dwID */
#define WM_CTRL_SETPROJECTPATH      (WM_USER+3)     /* 0, LPCSTR szProjectPath */ 
#define WM_CTRL_GETREADONLYSTATUS   (WM_USER+4)     /* 0, 0L */
#define WM_CTRL_SETREADONLYSTATUS   (WM_USER+5)     /* bReadOnly, 0L */
#define WM_CTRL_GETENABLE           (WM_USER+6)     /* 0, 0L */
#define WM_CTRL_SETENABLE           (WM_USER+7)     /* bEnable, 0L */
#define WM_CTRL_SETDEBUGSTATE       (WM_USER+8)     /* bDebug, 0L */
#define WM_CTRL_CANSAVE             (WM_USER+9)     /* 0, 0L */
#define WM_CTRL_SAVE                (WM_USER+10)    /* 0, LPCSTR szFilePath */
#define WM_CTRL_LOAD                (WM_USER+11)    /* 0, LPCSTR szFilePath */
#define WM_CTRL_CANUNDO             (WM_USER+12)    /* 0, 0L */
#define WM_CTRL_UNDO                (WM_USER+13)    /* 0, 0L */
#define WM_CTRL_CANREDO             (WM_USER+14)    /* 0, 0L */
#define WM_CTRL_REDO                (WM_USER+15)    /* 0, 0L */
#define WM_CTRL_CANCUT              (WM_USER+16)    /* 0, 0L */
#define WM_CTRL_CUT                 (WM_USER+17)    /* 0, 0L */
#define WM_CTRL_CANCOPY             (WM_USER+18)    /* 0, 0L */
#define WM_CTRL_COPY                (WM_USER+19)    /* 0, 0L */
#define WM_CTRL_CANPASTE            (WM_USER+20)    /* 0, 0L */
#define WM_CTRL_PASTE               (WM_USER+21)    /* 0, 0L */
#define WM_CTRL_CANCLEAR            (WM_USER+22)    /* 0, 0L */
#define WM_CTRL_CLEAR               (WM_USER+23)    /* 0, 0L */
#define WM_CTRL_CANSELECTALL        (WM_USER+24)    /* 0, 0L */
#define WM_CTRL_SELECTALL           (WM_USER+25)    /* 0, 0L */
#define WM_CTRL_CANINSERTSYMBOL     (WM_USER+26)    /* 0, 0L */
#define WM_CTRL_INSERTSYMBOL        (WM_USER+27)    /* 0, szSymbol */
#define WM_CTRL_SETFONT             (WM_USER+30)    /* HFONT , 0L */
#define WM_CTRL_CANSETTAB           (WM_USER+31)    /* 0, 0L */
#define WM_CTRL_SETTAB              (WM_USER+32)    /* iTabLen, 0L */
#define WM_CTRL_CANSETZOOM          (WM_USER+33)    /* 0, 0L */
#define WM_CTRL_SETZOOM             (WM_USER+34)    /* 0, 0L */
#define WM_CTRL_CANSETGRID          (WM_USER+35)    /* 0, 0L */
#define WM_CTRL_SETGRID             (WM_USER+36)    /* BOOL bShowGrid, 0L */
#define WM_CTRL_FINDREPLACE         (WM_USER+37)    /* 0, &str_W5FindReplace - rc: SearchResult */
#define WM_CTRL_ISMODIFIED          (WM_USER+38)    /* 0, 0L */
#define WM_CTRL_GOTO                (WM_USER+39)    /* 0 , LPCSTR szGoto */
#define WM_CTRL_LOCATEERROR         (WM_USER+40)    /* 0 , LPCSTR szError */
#define WM_CTRL_GETCURPOS           (WM_USER+41)    /* 0, 0L; rc = MAKELONG(PosX,PosY)*/
#define WM_CTRL_ISCHECK             (WM_USER+42)    /* idcheck, 0L */
#define WM_CTRL_CANCHECK            (WM_USER+43)    /* idcheck, 0L */
#define WM_CTRL_CHECK               (WM_USER+44)    /* idCheck, 0L */
#define WM_CTRL_CANFORMATPROGRAM    (WM_USER+45)    /* 0, 0L */
#define WM_CTRL_FORMATPROGRAM       (WM_USER+46)    /* 0, 0L */
#define WM_CTRL_CANVIEWINFO         (WM_USER+47)    /* 0, 0L */
#define WM_CTRL_VIEWINFO            (WM_USER+48)    /* 0, 0L */
#define WM_CTRL_CANINSERTFILE       (WM_USER+49)    /* 0 ,0L */
#define WM_CTRL_INSERTFILE          (WM_USER+50)    /* 0, szPathname */
#define WM_CTRL_GETSELTEXTLENGTH    (WM_USER+51)    /* 0, 0L */
#define WM_CTRL_GETSELTEXT          (WM_USER+52)    /* 0, szAppBuffer */
#define WM_CTRL_SETTEXT             (WM_USER+53)    /* 0, szText */
#define WM_CTRL_GETTEXT             (WM_USER+54)    /* 0, 0L; return LPCSTR*/
#define WM_CTRL_PAINT               (WM_USER+55)    /* hDC, pRect; return height*/
#define WM_CTRL_SETBGCOLOR          (WM_USER+56)    /* rgbMain, rgbUsed */
#define WM_CTRL_SETCURRENTFB        (WM_USER+57)    /* 0, &str_FB */
#define WM_CTRL_CANINSERTFB         (WM_USER+58)    /* 0, 0L */
#define WM_CTRL_INSERTFB            (WM_USER+59)    /* 0, &str_W5FB  */
#define WM_CTRL_SETFB               (WM_USER+60)    /* 0, &str_W5FB */
#define WM_CTRL_GETVAR              (WM_USER+61)    /* 0, &str_W5Variable */              
#define WM_CTRL_GETFB               (WM_USER+62)    /* 0, &str_W5FB */     
#define WM_CTRL_GETSYMBOLNAME       (WM_USER+63)    /* 0, 0L ; return LPCSTR */
#define WM_CTRL_GETTYPENAME         (WM_USER+64)    /* 0, 0L ; return LPCSTR */
#define WM_CTRL_GETFIRSTCHAR        (WM_USER+65)    /* 0, 0L ; return TCHAR */        
#define WM_CTRL_ISGRIDVISIBLE       (WM_USER+66)    /* 0, 0L ; return true if grid visible */ 
#define WM_CTRL_ISEMPTY             (WM_USER+67)    /* 0, 0L ; return true if empty */            
#define WM_CTRL_SETMODIFIED         (WM_USER+68)    /* BOOL, 0L ; */   

//settings
#define WM_CTRL_ENABLEDRAGNDROP     (WM_USER+69)    /* bDragnDrop, 0L */
#define WM_CTRL_UNDOREDOSIZE        (WM_USER+70)    /* iSize, 0L */
#define WM_CTRL_KEEPFBDSELECT       (WM_USER+71)    /* BOOL bKeepSelect, 0L */  
#define WM_CTRL_COPYBITMAP          (WM_USER+72)    /* BOOL bCopyBitmap, 0L */  
#define WM_CTRL_PROMPTVARNAME       (WM_USER+73)    /* BOOL bPromptVarName, 0L */  
#define WM_CTRL_PROMPTINSTANCE      (WM_USER+74)    /* BOOL bPromptInstance, 0L */
#define WM_CTRL_ENABLEPULSE         (WM_USER+75)    /* BOOL bEnablePulse, 0L */  
#define WM_CTRL_SETVERTVARSIZE      (WM_USER+76)    /* int iVertVarSize, 0L */  
#define WM_CTRL_SETHORZVARSIZE      (WM_USER+77)    /* int iHorzVarSize, 0L */  
#define WM_CTRL_SETPAGEWIDTH        (WM_USER+78)    /* int iPageWidth, 0L rem: iPageWidth is in millimeters */
#define WM_CTRL_SETCONTENTS         (WM_USER+79)    /* int iTypeContents, LPCSTR szQualifier; see define contents */

//print
#define WM_CTRL_PRINTSETPROPERTY    (WM_USER+80)    /* int iProp, dwValue */
#define WM_CTRL_PRINTGETNBVERTFOLIO (WM_USER+81)    /* 0, 0L; return nb vert folios */
#define WM_CTRL_PRINTGETNBHORZFOLIO (WM_USER+82)    /* 0, 0L; return nb horz folios  */
#define WM_CTRL_PRINTGETFOLIO       (WM_USER+83)    /* POINT folio, 0L; return BOOL, true if folio is full */
#define WM_CTRL_PRINTFOLIO          (WM_USER+84)    /* POINT folio, LPCSTR szImageFile */
#define WM_CTRL_GETCELLHEIGHT       (WM_USER+85)    /* 0, 0L; return iCellY */
#define WM_CTRL_GETCELLWIDTH        (WM_USER+86)    /* 0, 0L; return iCellX */
#define WM_CTRL_SETCELLHEIGHT       (WM_USER+87)    /* int iCellY, 0L */
#define WM_CTRL_SETCELLWIDTH        (WM_USER+88)    /* int iCellX, 0L */

//edition
#define WM_CTRL_HASSELECTION                (WM_USER+89)    /* 0, 0L; return TRUE if has selection */
#define WM_CTRL_CANINSERTCONTACTBEFORE      (WM_USER+90)    /* 0, 0L */
#define WM_CTRL_INSERTCONTACTBEFORE         (WM_USER+91)    /* 0, 0L */
#define WM_CTRL_CANINSERTCONTACTAFTER       (WM_USER+92)    /* 0, 0L */
#define WM_CTRL_INSERTCONTACTAFTER          (WM_USER+93)    /* 0, 0L */
#define WM_CTRL_CANINSERTCONTACTPARALLEL    (WM_USER+94)    /* 0, 0L */
#define WM_CTRL_INSERTCONTACTPARALLEL       (WM_USER+95)    /* 0, 0L */
#define WM_CTRL_CANINSERTCOIL               (WM_USER+96)    /* 0, 0L */
#define WM_CTRL_INSERTCOIL                  (WM_USER+97)    /* 0, 0L */
#define WM_CTRL_CANINSERTFBBEFORE           (WM_USER+98)    /* 0, 0L */
#define WM_CTRL_INSERTFBBEFORE              (WM_USER+99)    /* 0, 0L */
#define WM_CTRL_CANINSERTFBAFTER            (WM_USER+100)   /* 0, 0L */
#define WM_CTRL_INSERTFBAFTER               (WM_USER+101)   /* 0, 0L */
#define WM_CTRL_CANINSERTFBPARALLEL         (WM_USER+102)   /* 0, 0L */
#define WM_CTRL_INSERTFBPARALLEL            (WM_USER+103)   /* 0, 0L */
#define WM_CTRL_CANINSERTJUMP               (WM_USER+104)   /* 0, 0L */
#define WM_CTRL_INSERTJUMP                  (WM_USER+105)   /* 0, 0L */
#define WM_CTRL_CANINSERTRUNG               (WM_USER+106)   /* 0, 0L */
#define WM_CTRL_INSERTRUNG                  (WM_USER+107)   /* 0, 0L */
#define WM_CTRL_CANINSERTCOMMENT            (WM_USER+108)   /* 0, 0L */
#define WM_CTRL_INSERTCOMMENT               (WM_USER+109)   /* 0, 0L */
#define WM_CTRL_CANINSERTHORZ               (WM_USER+110)   /* 0, 0L */
#define WM_CTRL_INSERTHORZ                  (WM_USER+111)   /* 0, 0L */

#define WM_CTRL_DISPLAYFBDORDER             (WM_USER+112)   /* 0, 0L, return DWORD */
#define WM_CTRL_CANCHANGEPROPERTIES         (WM_USER+113)   /* 0, 0L, return BOOL */
#define WM_CTRL_SETPROPERTIES               (WM_USER+114)   /* 0, LPCSTR szProperties */
#define WM_CTRL_GETPROPERTIES               (WM_USER+115)   /* 0, 0L, return LPCSTR */
#define WM_CTRL_ENSURESELVISIBLE            (WM_USER+116)   /* 0, 0L  */
#define WM_CTRL_ALIGNCOILS                  (WM_USER+117)   /* 0, 0L */
#define WM_CTRL_CANALIGNCOILS               (WM_USER+118)   /* 0, 0L */
#define WM_CTRL_CANDISPLAYFBDORDER          (WM_USER+119)   /* 0, 0L, return BOOL */
#define WM_CTRL_CANINSERTTEXT               (WM_USER+120)   /* 0 ,0L; return BOOL */
#define WM_CTRL_INSERTTEXT                  (WM_USER+121)   /* POINT*, szListItem */
#define WM_CTRL_PRINTGETNBSYMBOLS           (WM_USER+122)   /* POINT*, 0L; return nb symbols */
#define WM_CTRL_PRINTGETSYMBOLS             (WM_USER+123)   /* POINT*, DWORD*; return nb symbols */
#define WM_CTRL_WRAPRUNGS                   (WM_USER+124)   /* BOOL, 0L */
#define WM_CTRL_SUMMARYPRINT                (WM_USER+125)   /* BOOL bAll, HDC hDC */
#define WM_CTRL_AUTODECLAREINST             (WM_USER+126)   /* BOOL bAutoDeclare, 0L */
#define WM_CTRL_SETWIDTHSUMMARYPRINT        (WM_USER+128)   /* iWidth, 0L */
#define WM_CTRL_SETHEIGHTSUMMARYPRINT       (WM_USER+129)   /* iHeight, 0L */
#define WM_CTRL_GETTEXTLENGHT               (WM_USER+130)   /* 0, 0L; return int */  
#define WM_CTRL_FITZOOM                     (WM_USER+131)   /* 0,  pRect*/
#define WM_CTRL_HIDESCROLL                  (WM_USER+132)   /* bHide, 0 */
#define WM_CTRL_VALUEINTEXT					(WM_USER+133)	/* bSet, 0*/
#define WM_CTRL_SETTOOLTIPINFOS				(WM_USER+134)	/* bEdit, &str_W5TooltipInfos */
#define WM_CTRL_CBM                         (WM_USER+135)
#define WM_CTRL_HISTORY                     (WM_USER+136)
#define WM_CTRL_AUTODECLARESYMBOL           (WM_USER+137)   /* BOOL bAutoDeclare, 0L */
#define WM_CTRL_INSERTVARWHENINSERTFB       (WM_USER+138)   /* BOOL bInsert, 0L */
#define WM_CTRL_ACTIVATE                    (WM_USER+139)   /* bActivate, 0L */
#define WM_CTRL_ERCEVENT                    (WM_USER+140)   /* */
#define WM_CTRL_SETAUTOCONNECTVARIABLE      (WM_USER+141)   /* BOOL bAuto, 0L */
#define WM_CTRL_UNDERLINEGLOBAL             (WM_USER+142)   /* BOOL bUnderline, 0L */
#define WM_CTRL_SNAPFBDGRID                 (WM_USER+143)   /* BOOL bSnapFBDToGrid, 0L */
#define WM_CTRL_DRAWFBDBRIDGE               (WM_USER+144)   /* BOOL bDrawFBDBridge, 0L */
#define WM_CTRL_CSVSEPARATORCOMA            (WM_USER+145)   /* BOOL bCsvSeparatorComa, 0L */
#define WM_CTRL_SETHORZFBSIZE               (WM_USER+146)   /* int iHorzFBSize, 0L */
#define WM_CTRL_SHOWSTLINE                  (WM_USER+147)   /* BOOL bShowSTLine, 0L */

//ST range messages 
#define W5EDITSTM_ENABLESYNTAX  (WM_USER+200)   /* bFlag, 0L */


//SFC range messages
#define W5EDITSFCM_LOCK             (WM_USER+200)   /* bLock, 0L - at caret position */

#define W5EDITSFCM_CANINSERT        (WM_USER+201)   /* 0, 0L */
#define W5EDITSFCM_INSERTSTEP       (WM_USER+202)   /* 0, 0L */
#define W5EDITSFCM_INSERTTRANS      (WM_USER+203)   /* 0, 0L */
#define W5EDITSFCM_INSERTJUMP       (WM_USER+204)   /* 0, 0L */
#define W5EDITSFCM_INSERTMAINDIV    (WM_USER+205)   /* 0, 0L */
#define W5EDITSFCM_INSERTDIV        (WM_USER+206)   /* 0, 0L */
#define W5EDITSFCM_INSERTCNV        (WM_USER+207)   /* 0, 0L */
#define W5EDITSFCM_INSERTMACRO      (WM_USER+208)   /* 0, 0L */
#define W5EDITSFCM_INSERTMACROBODY  (WM_USER+209)   /* 0, 0L */

#define W5EDITSFCM_CANSWAPSTYLE     (WM_USER+210)   /* 0, 0L */
#define W5EDITSFCM_SWAPSTYLE        (WM_USER+211)   /* 0, 0L */

#define W5EDITSFCM_INSERTINITSTEP   (WM_USER+212)   /* 0, 0L */


#define W5EDITSFCM_GETTRANSCODE     (WM_USER+213)   /* iTransRef, 0L - rc = language */
#define W5EDITSFCM_SETTRANSCODE     (WM_USER+214)   /* iTransRef, szText */
#define W5EDITSFCM_GETTRANSNOTE     (WM_USER+215)   /* iTransRef, 0L */
#define W5EDITSFCM_SETTRANSNOTE     (WM_USER+216)   /* iTransRef, szText */

#define W5EDITSFCM_GETSTEPCODE_DEF  (WM_USER+217)   /* iStepRef, 0L - rc = szText */
#define W5EDITSFCM_SETSTEPCODE_DEF  (WM_USER+218)   /* iStepRef, szText */
#define W5EDITSFCM_GETSTEPCODE_P1   (WM_USER+219)   /* iStepRef, 0L - rc = szText */
#define W5EDITSFCM_SETSTEPCODE_P1   (WM_USER+220)   /* iStepRef, szText */
#define W5EDITSFCM_GETSTEPCODE_N    (WM_USER+221)   /* iStepRef, 0L - rc = szText */
#define W5EDITSFCM_SETSTEPCODE_N    (WM_USER+222)   /* iStepRef, szText */
#define W5EDITSFCM_GETSTEPCODE_P0   (WM_USER+223)   /* iStepRef, 0L - rc = szText */
#define W5EDITSFCM_SETSTEPCODE_P0   (WM_USER+224)   /* iStepRef, szText */
#define W5EDITSFCM_GETSTEPNOTE      (WM_USER+225)   /* iStepRef, 0L */
#define W5EDITSFCM_SETSTEPNOTE      (WM_USER+226)   /* iStepRef, szText */

#define W5EDITSFCM_ISSELSTEP        (WM_USER+227)   /* 0, &iRef */
#define W5EDITSFCM_ISSELTRANS       (WM_USER+228)   /* 0, &iRef */
#define W5EDITSFCM_GETSELREFNUM     (WM_USER+229)   /* 0, 0L */

#define W5EDITSFCM_LOCKSTEP         (WM_USER+230)   /* bLock, iStepRef */
#define W5EDITSFCM_LOCKTRANS        (WM_USER+231)   /* bLock, iTransRef */

#define W5EDITSFCM_ENTERSELREF      (WM_USER+232)   /* 0, 0L */
#define W5EDITSFCM_CANENTERSELREF   (WM_USER+233)   /* 0, 0L */
#define W5EDITSFCM_CANEDITSELCODE   (WM_USER+236)   /* 0, 0L */

#define W5EDITSFCM_GETSTEPLANGUAGEP1 (WM_USER+237)   /* iStepRef, 0L */
#define W5EDITSFCM_GETSTEPLANGUAGEN  (WM_USER+238)   /* iStepRef, 0L */
#define W5EDITSFCM_GETSTEPLANGUAGEP0 (WM_USER+239)   /* iStepRef, 0L */
#define W5EDITSFCM_GETTRANSLANGUAGE (WM_USER+240)   /* iTransRef, 0L */

#define W5EDITSFCM_SETNOTEDISPLAY   (WM_USER+241)   /* bMode, 0L */
#define W5EDITSFCM_GETNOTEDISPLAY   (WM_USER+242)   /* 0, 0L */

#define W5EDITSFCM_CANRENUMBER      (WM_USER+243)   /* 0, 0L */
#define W5EDITSFCM_RENUMBER         (WM_USER+244)   /* 0, 0L */

#define W5EDITSFCM_NEXTPOSITEM      (WM_USER+245)   /* bLoop, &iRef */

#define W5EDITSFCM_ACTIVESTEP       (WM_USER+246)   /* 0, 0L */

#define W5EDITSFCM_ISSTEP           (WM_USER+247)   /* POINT* ptPos, &iRef */
#define W5EDITSFCM_ISTRANS          (WM_USER+248)   /* POINT* ptPos, &iRef */

//dictionary
#define W5EDITTL_POSTIMPORT         (WM_USER+300)   /*when import a program from another project */
#define W5EDITTL_POSTSELECT         (WM_USER+301)   /*when rename many variables, defer selection */

//FFLD range
#define WM_POSTDROP                 (WM_USER+400)   /*when user drop items, use it to post after drag n drop operation */
 
//FBD range
#define W5EDITFBD_X5MESSAGEMIN    (WM_USER+800)     //define for activeX message
#define W5EDITFBD_X5MESSAGEMAX    (WM_USER+900)     //do not declare other message between 

//multi base event
#define WM_CTRL_MBASEEVENT          (WM_USER+1000)

//used to send message to Control Debug Client
#define WM_MSG_CLIENTLIST           (WM_USER+1001)

//Notifications sent to frame
#define WM_NOTIFY_SETVAR            (WM_USER+1)     /* 0, 0 */
#define WM_NOTIFY_SETFB             (WM_USER+2)     /* 0, 0 */
#define WM_NOTIFY_SETVARFIRSTCHAR   (WM_USER+3)     /* 0, 0 */
#define WM_NOTIFY_RCLICK            (WM_USER+4)     /* 0, 0 */
#define WM_NOTIFY_SETPROPERTIES     (WM_USER+5)     /* 0, 0 */

#define W5EDITN_DBLCLK              1 /* Sent by control when user double click on control and double click is not traited by control */
#define W5EDITN_RCLICK              2 /* Sent by control when user right click on control */
#define W5EDITN_SELCHANGE           3 /* Sent by control when selection has changed */
#define W5EDITN_EDITPROPS           4 /* Sent by control to edit properties of the current selected item */
#define W5EDITN_SETVARFIRSTCHAR     5 /* Sent by control when a character that should be first of identifier is hit */
#define W5EDITN_SETVAR              6 /* Sent by control when double click on symbol (var, io function block, instance name) */
#define W5EDITN_SETFB               7 /* Sent by control when double click on function block body. */
#define W5EDITN_GETFOCUS            8 /* Sent by control when gain focus. */
#define W5EDITN_COMPLETION          9 /* Sent by control when autocompletion is called */
#define W5EDITN_ADDITEM             10 /* Sent by control after new item has been inserted */
#define W5EDITN_REMOVEITEM          11 /* Sent by control after item has been deleted */
#define W5EDITN_MODIFYITEM          12 /* Sent by control after item has been modified */
#define W5EDITN_LINKTO              13 /* Sent by control after hit on a link object */
#define W5EDITN_BEGINDRAG           14 /* Sent by control when user begin to drag the selection */
#define W5EDITN_INITARRAYVAR        15 /* Sent by control when user has double click on a initial value that is an array */
#define W5EDITN_HEADERSIZECHANGED   16 /* Sent by control when user has resized a column */
#define W5EDITN_HEADERSELCHANGED    17 /* Sent by control when current column has changed */
#define W5EDITN_HEADERDBLCLK        18 /* Sent by control when user has double click on the header */
#define W5EDITN_LISBLOCKCHANGED     19 /* Sent by control when list of used blocks has changed */
#define W5EDITN_MODIFIED            20 /* Sent by user to indicate that modified flag has changed */
#define W5EDITN_VSCROLL             21 /* Sent by control when user has scroll vertically */
#define W5EDITN_HSCROLL             22 /* Sent by control when user has scroll horizontally */
#define W5EDITN_CHAR                23 /* Sent by control when user has hit a char and control has not trapped it */
#define W5EDITN_KEYDOWN             24 /* Sent by control when user has hit a key and control has not trapped it */
#define W5EDITN_RCLICKGRID          25 /* Only for fieldbus controls */
#define W5EDITN_RCLICKLIST          26 /* Only for fieldbus controls */
#define W5EDITN_UDFBTOUPDATE        27 /* used to indicates to parent that there are many udfb to update*/
                                       /* because of the definition and file that does not correspond */
#define W5EDITN_LISTKEYWORD         28 /* Sent by control where user want to see keyword list */
#define W5EDITN_OPENUDFB            29 /* Sent by control where user want to open selected UDFB */
#define W5EDITN_SFCOPENDEFAULT      30 /* Sent by control when user double click on action block */
#define W5EDITN_SFCOPENNOTES        31 /* Sent by control when user double click on notes */
#define W5EDITN_SFCOPENP1           32 /* Sent by control when user double click on qualifier P1 */
#define W5EDITN_SFCOPENN            33 /* Sent by control when user double click on qualifier N */
#define W5EDITN_SFCOPENP0           34 /* Sent by control when user double click on qualifier P0 */
#define W5EDITN_PROPCHANGED         35 /* Sent by control when item property has changed in control */
#define W5EDITN_RCLICKSYMBOL        36 /* Sent by control when right click on instance */
#define W5EDITN_RCLICKFB            37 /* Sent by control when right click on FB body */
#define W5EDITN_COLCHANGE           38 /* Sent by control when selected column has changed */
#define W5EDITN_DROP                39 /* Sent by control when drag n drop operation has been dropped in control */
#define W5EDITN_ARRANGECOL          40 /* Sent by dictionary when user has rearranged the columns order/filter */
#define W5EDITN_EXPANDCOLLAPSE      41 /* Sent by control when a tree item has been expanded or collapsed */
#define W5EDITN_CLICKONICON         42 /* Sent by control when a user click on image item in a treelist */

#endif // !defined(_W5EDITAPI_H_)

