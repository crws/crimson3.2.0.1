#if !defined(_W5EditSFCAPI_H_)
#define _W5EditSFCAPI_H_

#define W5EDITSFC_CLASSNAME	    	_T("W5EditSFC")

#define W5EDITSFCLV2_NONE           0
#define W5EDITSFCLV2_STIL           1
#define W5EDITSFCLV2_LD             2
#define W5EDITSFCLV2_FBD            3
#define W5EDITSFCLV2_FFLD           4

#define LV2_DEFAULT         0
#define LV2_NOTE            1
#define LV2_P1              2
#define LV2_N               3
#define LV2_P0              4

//defines used for display (See SetDisplay command)
#define DISPLAY_ALL     0
#define DISPLAY_NOTE    1
#define DISPLAY_ACTION  2


/*
#define W5EDITSFCN_DBLCLK        1
#define W5EDITSFCN_RCLICK        2
*/
#ifdef __cplusplus
extern "C" {
#endif

#ifdef W5EDITSFCDLL
	/* Build the DLL */
	#define W5EDITSFC_APICALL __declspec(dllexport)
#else
	#define W5EDITSFC_APICALL __declspec(dllimport)
#endif

#ifdef __cplusplus
}
#endif

#endif // !defined(_W5EditSFCAPI_H_)

