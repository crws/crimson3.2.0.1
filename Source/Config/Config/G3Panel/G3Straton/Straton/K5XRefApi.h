// K5 Cross-References - exports

// K5XREF Server API

#include "K5XRefApiLite.h"

#if !defined(_K5XREFAPI_H__INCLUDED_)
#define _K5XREFAPI_H__INCLUDED_

/////////////////////////////////////////////////////////////////////////////
// XRV Browser Wrapper

#ifdef __cplusplus

class CK5XRVApi
{
public:
    CK5XRVApi (LPCSTR szXmlFile)
    {
        _m_hXRV = NULL;
        _m_hLib = LoadLibrary ("Straton\\K5XREF.DLL");
        if (_m_hLib != NULL)
        {
            _m_pfLoad = GETK5XRV_Load (_m_hLib);
            _m_pfFree = GETK5XRV_Free (_m_hLib);
            _m_pfGetRef = GETK5XRV_GetRef (_m_hLib);
            _m_pfBuild = GETK5XRV_Build (_m_hLib);
            _m_pfNeedBuild = GETK5XRV_NeedBuild (_m_hLib);
            _m_pfBuildAndLoad = GETK5XRV_BuildAndLoad (_m_hLib);
        }
        else
        {
            _m_pfLoad = NULL;
            _m_pfFree = NULL;
            _m_pfGetRef = NULL;
            _m_pfBuild = NULL;
            _m_pfNeedBuild = NULL;
            _m_pfBuildAndLoad = NULL;
        }
    }
    virtual ~CK5XRVApi ()
    {
        if (_m_hXRV && _m_pfFree)
            _m_pfFree (_m_hXRV);
        if (_m_hLib)
            FreeLibrary (_m_hLib);
    }

public:
    BOOL LoadXmlFile (LPCSTR szXmlFile)
    {
        if (_m_hXRV && _m_pfFree)
            _m_pfFree (_m_hXRV);
        if (!_m_pfLoad)
            return FALSE;
        _m_hXRV = _m_pfLoad (szXmlFile);
        return (_m_hXRV != NULL);
    }
    BOOL LoadProjectFile (LPCSTR szProjectPath)
    {
        CString str = szProjectPath;
        str += "\\__XRV.XML";
        return LoadXmlFile (str);
    }
    BOOL IsBrowingInfoAvailable (void)
    {
        return (_m_hXRV != NULL);
    }
    BOOL NeedBuildProject (LPCSTR szProjectPath)
    {
        if ( _m_hXRV == NULL )
            return TRUE;

        if (_m_pfNeedBuild == NULL)
            return FALSE;
        CString str = szProjectPath;
        str += "\\__XRV.XML";
        return _m_pfNeedBuild (szProjectPath, str);
    }
    BOOL BuildAndReloadProject (LPCSTR szProjectPath)
    {
        if (_m_pfBuildAndLoad == NULL)
            return FALSE;
        if (_m_hXRV && _m_pfFree)
            _m_pfFree (_m_hXRV);

        CString str = szProjectPath;
        str += "\\__XRV.XML";
        _m_hXRV = _m_pfBuildAndLoad (szProjectPath, str);
        return (_m_hXRV != NULL);
    }
    void EnsureBrowsingInfoIsUpdated (LPCSTR szProjectPath)
    {
        if (NeedBuildProject (szProjectPath))
            BuildAndReloadProject (szProjectPath);
        else if (!IsBrowingInfoAvailable ())
            LoadProjectFile (szProjectPath);
    }
    int GetRefList (CStringList *psl, LPCSTR szVarName, LPCSTR szParent="\0")
    {
        psl->RemoveAll ();
        if (_m_pfGetRef == NULL || _m_hXRV == NULL)
            return 0;

        CString str = _m_pfGetRef (_m_hXRV, szVarName, szParent);
        int index;
        while ((index = str.Find ("\r\n")) >= 0)
        {
            psl->AddTail (str.Left (index));
            str.Delete (0, index + 2);
        }
        if (!str.IsEmpty ())
            psl->AddTail (str);
        return (int)psl->GetCount ();
    }

private:
    HINSTANCE _m_hLib;
    DWORD_PTR _m_hXRV;
    PFK5XRV_Load _m_pfLoad;
    PFK5XRV_Free _m_pfFree;
    PFK5XRV_GetRef _m_pfGetRef;
    PFK5XRV_Build _m_pfBuild;
    PFK5XRV_NeedBuild _m_pfNeedBuild;
    PFK5XRV_BuildAndLoad _m_pfBuildAndLoad;
};

#endif

/////////////////////////////////////////////////////////////////////////////
// XRV BuilderWrapper

#ifdef __cplusplus

class CK5XRVBuildApi
{
public:
    CK5XRVBuildApi ()
    {
        _m_hLib = LoadLibrary ("Straton\\K5XREF.DLL");
        if (_m_hLib != NULL)
            _m_pfBuild = GETK5XRV_Build (_m_hLib);
        else
            _m_pfBuild = NULL;
    }
    virtual ~CK5XRVBuildApi ()
    {
        if (_m_hLib)
            FreeLibrary (_m_hLib);
    }

public:
    void Build (LPCSTR szProjectPath, LPCSTR szXmlFile)
    {
        if (_m_pfBuild)
            _m_pfBuild (szProjectPath, szXmlFile);
    }
    void BuildProject (LPCSTR szProjectPath)
    {
        CString str = szProjectPath;
        str += "\\__XRV.XML";
        Build (szProjectPath, str);
    }

private:
    HINSTANCE _m_hLib;
    PFK5XRV_Build _m_pfBuild;
};

#endif

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_K5XREFAPI_H__INCLUDED_)
