// K5 Compiler - exports

// K5CMP Server API

#if !defined(_K5CMPAPI_H__INCLUDED_)
#define _K5CMPAPI_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef K5CMPDLL
	/* Build the DLL */
	#define K5CMP_APICALL __declspec(dllexport)
#else
	#define K5CMP_APICALL __declspec(dllimport)
#endif

#define IGNORE_WRAPPER	0

/////////////////////////////////////////////////////////////////////////////
// compiler - build

int K5CMP_APICALL K5CMP_GetVersion		// get API version number
	(
	void
	);									// returns the API version number

BOOL K5CMP_APICALL K5CmpNeedBuild		// test if a project needs to be compiled
	(
	LPCSTR szProjectPath,				// pathname of the project
	int    iCodeType					// reserved - must be 0
	);									// TRUE if project code is obsolete

void K5CMP_APICALL K5CmpCleanProject	// Clean the project
	(
	LPCSTR szProjectPath				// project pathname
	);

BOOL K5CMP_APICALL K5CmpBuildDefault	// build the application - default setting
	(
	LPCSTR szProjectPath,				// pathname of the project
	HWND   hwndOutputListbox			// handle of a LISTBOX output
	);									// returns TRUE if no error

BOOL K5CMP_APICALL K5CmpBuildSolution   // build a complete solution
	(
	LPCSTR szProjectList,               // list of project pathnames (sep = \n)
	HWND   hwndOutputListbox            // handle of a LISTBOX output
	);                                  // returns TRUE if no error

BOOL K5CMP_APICALL K5CmpBuildDefaultFileReport // build the application
	(
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szReportFile                 // pathname of the report file
	);

/////////////////////////////////////////////////////////////////////////////
// compiler - check (one by one)

BOOL K5CMP_APICALL K5CmpCheckProgram	// compile one program (check)
	(
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szProgramName,				// name of the program
	int    iCodeType,					// reserved - must be 0
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile					// handle of an output file
	);									// returns TRUE if no error

BOOL K5CMP_APICALL K5CmpBuildOneUDFB	// rebuild only one UDFB
	(
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szUDFBName,					// name of the UDFB
	int    iCodeType,					// reserved - must be 0
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile					// handle of an output file
	);									// returns TRUE if no error

BOOL K5CMP_APICALL K5CmpCheckDefFile    // check definition file
    (
    LPCSTR szPath
    );

/////////////////////////////////////////////////////////////////////////////
// extensions

void K5CMP_APICALL K5CMP_SetDemoMode
	(
	BOOL bDemo
	);

void K5CMP_APICALL K5CmpEditOptions
	(
	LPCSTR szProjectPath,
	HWND hwndParent
	);

BOOL K5CMP_APICALL K5CmpExternBuild
	(
	LPCSTR szProjectPath,
	HWND   hwndOutputListbox
	);

BOOL K5CMP_APICALL K5CmpBuildProject
	(
	LPCSTR szProjectPath,
	int    iCodeType,
	LPCSTR szTargetName,
	LPCSTR szCodeSuffix,
	BOOL   bIntelEndian,
	HWND   hwndOutputListbox,
	FILE * fOutputFile,
	BOOL   bTraceFile
	);

BOOL K5CMP_APICALL K5CmpBuildAllUDFBs
	(
	LPCSTR szProjectPath,
	int    iCodeType,
	HWND   hwndOutputListbox,
	FILE * fOutputFile
	);

DWORD K5CMP_APICALL K5CmpListUnusedVars
	(
	LPCSTR szProjectPath,
	HWND   hwndOutputListbox,
	FILE * fOutputFile
	);

DWORD K5CMP_APICALL K5CmpListUsedLibs
	(
	LPCSTR szProjectPath,
	HWND   hwndOutputListbox,
	FILE * fOutputFile
	);

DWORD K5CMP_APICALL K5CmpGetFbdOrder
	(
	LPCSTR szProjectPath,
    LPCSTR szFile,
	DWORD *pdwID,
	DWORD dwMax
	);

DWORD K5CMP_APICALL K5CmpGetFbdOrderEx
	(
	LPCSTR szProjectPath,
    LPCSTR szProgram,
    LPCSTR szFile,
	DWORD *pdwID,
	DWORD dwMax
	);

DWORD K5CMP_APICALL K5CmpGetSamaOrder
	(
	LPCSTR szDiagram,
	DWORD *pdwID,
	DWORD dwMax
	);

void K5CMP_APICALL K5CmpAppIoImport
	(
	LPCSTR szProjectPath,
    LPCSTR szXmlPath
	);

/////////////////////////////////////////////////////////////////////////////
// convert program to another language

#define K5TR_KEEPSTASCOMMENT    0x0001  // keep ST text as comment

BOOL K5CMP_APICALL K5CmpConvertProgram
	(
	LPCSTR szProjectPath,
    LPCSTR szProgram,
    DWORD  dwLanguage,
    DWORD  dwOptions
    );

BOOL K5CMP_APICALL K5CmpCanConvertProgram
	(
	LPCSTR szProjectPath,
    LPCSTR szProgram,
    DWORD  dwLanguage
    );

BOOL K5CMP_APICALL K5CmpConversionNeedBuild
	(
	LPCSTR szProjectPath,
    LPCSTR szProgram,
    DWORD  dwLanguage
    );

LPCSTR K5CMP_APICALL K5CmpConvertStExpToFbd // DON'T USE THAT!
	(
	LPCSTR szExp,
    DWORD dwOptions
    );

/////////////////////////////////////////////////////////////////////////////
// variable import/export

#define K5CMP_VTEXT_IEC         0
#define K5CMP_VTEXT_XML         1
#define K5CMP_VTEXT_CSV         2
#define K5CMP_VTEXT_CSVCOMA     3

#define K5CMP_VTEXT_SORT    0x0001  // export: sort by name
#define K5CMP_VTEXT_NOLOCK  0x0002  // export: omit locked variables

LPCSTR K5CMP_APICALL K5CmpExportVariables
    (
	LPCSTR szProjectPath,   // project path name
	LPCSTR szGroupName,     // group name
    DWORD  dwSyntax,        // syntax selection
    DWORD  dwFlags          // options
    );

BOOL K5CMP_APICALL K5CmpImportVariables
    (
	LPCSTR szProjectPath,   // project path name
	LPCSTR szGroupName,     // group name
    LPCSTR szText,          // input text
    LPCSTR szErrFile,       // error report file name (or NULL)
    DWORD  dwSyntax,        // syntax selection
    DWORD  dwFlags          // reserved for extensions
    );

/////////////////////////////////////////////////////////////////////////////
// helpers

typedef BOOL   (*PFK5CMP_ConvertProgram)(LPCSTR,LPCSTR,DWORD,DWORD);
typedef BOOL   (*PFK5CMP_CanConvertProgram)(LPCSTR,LPCSTR,DWORD);
typedef BOOL   (*PFK5CMP_ConversionNeedBuild)(LPCSTR,LPCSTR,DWORD);
typedef LPCSTR (*PFK5CMP_ConvertStExpToFbd)(LPCSTR,DWORD);

#define GETK5CMPPROC_ConvertProgram(h) \
    ((PFK5CMP_ConvertProgram)GetProcAddress((h), "K5CmpConvertProgram"))
#define GETK5CMPPROC_CanConvertProgram(h) \
    ((PFK5CMP_CanConvertProgram)GetProcAddress((h), "K5CmpCanConvertProgram"))
#define GETK5CMPPROC_ConversionNeedBuild(h) \
    ((PFK5CMP_ConversionNeedBuild)GetProcAddress((h), "K5CmpConversionNeedBuild"))
#define GETK5CMPPROC_ConvertStExpToFbd(h) \
    ((PFK5CMP_ConvertStExpToFbd)GetProcAddress((h), "K5CmpConvertStExpToFbd"))

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#ifndef IGNORE_WRAPPER

/////////////////////////////////////////////////////////////////////////////
// conversion wrapper

#ifdef __cplusplus

class CK5CmpConvApi
{
public:
    CK5CmpConvApi (void)
    {
        _m_hLib = LoadLibrary (_T("Straton\\K5CMP.DLL"));
        if (_m_hLib != NULL)
        {
            _m_pfConvertProgram = GETK5CMPPROC_ConvertProgram (_m_hLib);
            _m_pfCanConvertProgram = GETK5CMPPROC_CanConvertProgram (_m_hLib);
            _m_pfConversionNeedBuild = GETK5CMPPROC_ConversionNeedBuild (_m_hLib);
            _m_pfConvertStExpToFbd = GETK5CMPPROC_ConvertStExpToFbd (_m_hLib);
        }
        else
        {
            _m_pfConvertProgram = NULL;
            _m_pfCanConvertProgram = NULL;
            _m_pfConversionNeedBuild = NULL;
            _m_pfConvertStExpToFbd = NULL;
        }
    }
    virtual ~CK5CmpConvApi (void)
    {
        if (_m_hLib)
            FreeLibrary (_m_hLib);
        _m_pfConvertProgram = NULL;
        _m_pfCanConvertProgram = NULL;
        _m_pfConversionNeedBuild = NULL;
        _m_pfConvertStExpToFbd = NULL;
    }

public:
    BOOL ConvertProgram (LPCSTR szProjectPath, LPCSTR szProgram, DWORD dwLanguage, DWORD dwOptions=0)
    {
        if (_m_pfConvertProgram == NULL)
            return FALSE;
        return _m_pfConvertProgram (szProjectPath, szProgram, dwLanguage, dwOptions);
    }
    BOOL CanConvertProgram (LPCSTR szProjectPath, LPCSTR szProgram, DWORD dwLanguage)
    {
        if (_m_pfCanConvertProgram == NULL)
            return FALSE;
        return _m_pfCanConvertProgram (szProjectPath, szProgram, dwLanguage);
    }
    BOOL ConversionNeedBuild (LPCSTR szProjectPath, LPCSTR szProgram, DWORD dwLanguage)
    {
        if (_m_pfConversionNeedBuild ==NULL)
            return FALSE;
        return _m_pfConversionNeedBuild (szProjectPath, szProgram, dwLanguage);
    }
    BOOL ConvertStExpToFbd (LPCSTR szExp, CString *pstrFbd, DWORD dwOptions=0)
    {
        pstrFbd->Empty ();
        if (_m_pfConvertStExpToFbd == NULL)
            return FALSE;
        LPCSTR sz = _m_pfConvertStExpToFbd (szExp, dwOptions);
        if (sz == NULL)
            return FALSE;
        *pstrFbd = sz;
        return TRUE;
    }


private:
    HINSTANCE _m_hLib;
    PFK5CMP_ConvertProgram _m_pfConvertProgram;
    PFK5CMP_CanConvertProgram _m_pfCanConvertProgram;
    PFK5CMP_ConversionNeedBuild _m_pfConversionNeedBuild;
    PFK5CMP_ConvertStExpToFbd _m_pfConvertStExpToFbd;
};

#endif

#endif

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_K5CMPAPI_H__INCLUDED_)
