// K5DBReg Registry API

#if !defined(_K5DBREGAPI_H__INCLUDED_)
#define _K5DBREGAPI_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef K5DBREGDLL
	/* Build the DLL */
	#define K5DBREG_APICALL __declspec(dllexport)
#else
	#define K5DBREG_APICALL __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// compatibility

int K5DBREG_APICALL K5DBReg_GetVersion	// get API version number
	(
	void
	);									// returns the API version number

/////////////////////////////////////////////////////////////////////////////
// pathnames

void K5DBREG_APICALL K5DBReg_GetLibFolderPath
	(
	LPSTR szLibPath
	);

void K5DBREG_APICALL K5DBReg_GetLibDefineFilePath
	(
	LPSTR szLibPath
	);

/////////////////////////////////////////////////////////////////////////////
// general

#define K5DBREGOBJ_BLOCK		1
#define K5DBREGOBJ_IO			2
#define K5DBREGOBJ_PROF         3
#define K5DBREGOBJ_TYPE         4

/////////////////////////////////////////////////////////////////////////////
// blocks

#define K5DBREG_STDOP		0x00000001
#define K5DBREG_STDFUNC		0x00000002
#define K5DBREG_STDFB		0x00000004
#define K5DBREG_CFUNC		0x00000020
#define K5DBREG_CFB			0x00000040
#define K5DBREG_UDFB		0x00000400

#define K5DBREG_ALL			0x00000fff
#define K5DBREG_FUNC		0x00000022
#define K5DBREG_FB			0x00000444

#define K5DBREG_BOOL		1
#define K5DBREG_DINT		2
#define K5DBREG_REAL		3
#define K5DBREG_LREAL   	4
#define K5DBREG_BYTE		5
#define K5DBREG_WORD		6
#define K5DBREG_LINT		7
#define K5DBREG_TIME		8
#define K5DBREG_STRING		9
#define K5DBREG_ANY			10
#define K5DBREG_USINT		11
#define K5DBREG_UINT		12
#define K5DBREG_UDINT		13
#define K5DBREG_ULINT		14
#define K5DBREG_COMPLEX     15

#define K5DBREG_WANTENENO   0x0001      // for GetPinNames() dwStyle only

DWORD K5DBREG_APICALL K5DBReg_GetNbBlock (
	DWORD dwMask
	);

DWORD K5DBREG_APICALL K5DBReg_GetBlocks (
	DWORD dwMask,
	DWORD *phBlocks
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockLibName (
	DWORD dwIndex
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockDesc (
	DWORD dwBlock,
	DWORD *pdwKind,
	DWORD *pdwLibNo,
	DWORD *pdwNbInput,
	DWORD *pdwNbOutput
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockInput (
	DWORD dwBlock,
	DWORD dwPin,
	DWORD *pdwType
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockOutput (
	DWORD dwBlock,
	DWORD dwPin,
	DWORD *pdwType
	);

DWORD K5DBREG_APICALL K5DBReg_FindBlock (
    LPCSTR szBlockName
    ); // RETURN: id of the block or NULL if not found

DWORD K5DBREG_APICALL K5DBReg_GetPinNames (
    DWORD dwBlock,
    DWORD dwStyle,
    LPCSTR *pszIn,
    LPCSTR *pszOut
    ); // reserved

DWORD K5DBREG_APICALL K5DBReg_BlockNeedEN (
	DWORD dwBlock
	);

DWORD K5DBREG_APICALL K5DBReg_BlockNeedENO (
	DWORD dwBlock
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockPinTypeName (
    DWORD dwType
    );

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockInputPinTypeName (
    DWORD dwBlock,
    DWORD dwPin
    );

BOOL K5DBREG_APICALL K5DBREG_BlockCanBeExtended (
    LPCSTR szBlockName
    );

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockProperties (
	DWORD dwBlock
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetBlockPropSyntax (
	DWORD dwBlock
	);

BOOL K5DBREG_APICALL K5DBReg_IsBlockInputRef (
	DWORD dwBlock,
	DWORD dwPin
	);

BOOL K5DBREG_APICALL K5DBReg_IsBlockInputArray (
	DWORD dwBlock,
	DWORD dwPin
	);

HICON K5DBREG_APICALL K5DBReg_GetFBIcon (
	DWORD dwBlock
	);

DWORD K5DBREG_APICALL K5DBReg_GetFBConstraint (
	DWORD dwBlock,
    DWORD dwGroup // 0 is version mark / -1 is != 0 if IEC compatible
	);

/////////////////////////////////////////////////////////////////////////////
// IOs

DWORD K5DBREG_APICALL K5DBReg_GetNbIOLib (
	void
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetIOLibName (
	DWORD dwIndex
	);

DWORD K5DBREG_APICALL K5DBReg_GetNbIO (
	LPCSTR szLib
	);

DWORD K5DBREG_APICALL K5DBReg_GetIOs (
	LPCSTR szLib,
	DWORD *phIOs
	);

DWORD K5DBREG_APICALL K5DBReg_FindIO (
	LPCSTR szName
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetIODesc (
	DWORD hIO,
	DWORD *pdwNbGroup
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetIOGroupDesc (
	DWORD hIO,
	DWORD hGroup,
	LPCSTR *pszTypeName,
	LPCSTR *pszPrefix
	);

DWORD K5DBREG_APICALL K5DBReg_GetIOGroupNbChannel (
	DWORD hIO,
	DWORD hGroup,
	DWORD *pdwMin,
	DWORD *pdwMax
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetIOChannelName (
	DWORD hIO,
	DWORD hGroup,
	DWORD hChannel
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetIOProperty (
	DWORD hIO,
	DWORD hGroup,
	DWORD hProp
	);

/////////////////////////////////////////////////////////////////////////////
// Profiles

DWORD K5DBREG_APICALL K5DBReg_GetNbProfile (
	LPCSTR szLib
	);

DWORD K5DBREG_APICALL K5DBReg_GetProfiles (
	LPCSTR szLib,
	DWORD *phProfs
	);

DWORD K5DBREG_APICALL K5DBReg_FindProfile (
	LPCSTR szName
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetProfileName (
	DWORD hProf
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetProfileText (
	DWORD hProf
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetProfileLibName (
	DWORD hProf
	);

/////////////////////////////////////////////////////////////////////////////
// Types

DWORD K5DBREG_APICALL K5DBReg_GetNbType (
	LPCSTR szLib
	);

DWORD K5DBREG_APICALL K5DBReg_GetTypes (
	LPCSTR szLib,
	DWORD *phTypes
	);

DWORD K5DBREG_APICALL K5DBReg_FindType (
	LPCSTR szName
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetTypeName (
	DWORD hType
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetTypeText (
	DWORD hType
	);

LPCSTR K5DBREG_APICALL K5DBReg_GetTypeLibName (
	DWORD hType
	);

/////////////////////////////////////////////////////////////////////////////
// comments

#define K5DBREGCOMM_SINGLELINE	1
#define K5DBREGCOMM_MULTILINE	2

void K5DBREG_APICALL K5DBReg_SetCommentLanguage (
	LPCSTR szLanguage
	);

DWORD K5DBREG_APICALL K5DBReg_GetCommentLength (
	DWORD dwCommType,
	DWORD dwObjectType,
	LPCSTR szObjectName
	);

DWORD K5DBREG_APICALL K5DBReg_GetComment (
	DWORD dwCommType,
	DWORD dwObjectType,
	LPCSTR szObjectName,
	LPSTR szBuffer,
	DWORD dwBufLen
	);

void K5DBREG_APICALL K5DBReg_GetUserBlockDocumentation (
    LPSTR szPathHtmlEntry
    );

/////////////////////////////////////////////////////////////////////////////
// editing (for lib manager only)

// DONT USE THAT IF YOU ARE NOT ME!

void K5DBREG_APICALL K5XReg_ReloadAll (void);
void K5DBREG_APICALL K5XReg_Save (LPCSTR szLib);
BOOL K5DBREG_APICALL K5XReg_IsModified (LPCSTR szLib);
void K5DBREG_APICALL K5XReg_Del (LPCSTR szLib, DWORD dwType, DWORD dwID);
BOOL K5DBREG_APICALL K5XReg_Ren (LPCSTR szLib, DWORD dwType, DWORD dwID, LPCSTR szName);
BOOL K5DBREG_APICALL K5XReg_New (LPCSTR szLib, DWORD dwType, DWORD dwKind, LPCSTR szName);
BOOL K5DBREG_APICALL K5XReg_SetComment (LPCSTR szLib, DWORD dwCommType, DWORD dwObjectType,
                                        LPCSTR szObjectName, LPCSTR szText);
BOOL K5DBREG_APICALL K5XReg_SetBlock (LPCSTR szLib, DWORD dwID, DWORD dwNbi, DWORD dwNbo,
                                      DWORD *pdwInType, DWORD *pdwOutType,
                                      LPCSTR *pszIn, LPCSTR *pszOut);
BOOL K5DBREG_APICALL K5XReg_SetBlockInPinComplexType (LPCSTR szLib, DWORD dwID,
                                                      DWORD dwPin, LPCSTR szType);
BOOL K5DBREG_APICALL K5XReg_SetBlockPropSyntax (LPCSTR szLib, DWORD dwBlock,
                                                LPCSTR szProperties);
void K5DBREG_APICALL K5XReg_AddIOGroup (LPCSTR szLib, DWORD dwID,
                                        LPCSTR szName, LPCSTR szPrefix,
                                        LPCSTR szChannels, DWORD dwReset);
void K5DBREG_APICALL K5XReg_AddIOProp (LPCSTR szLib, DWORD dwID,
                                       DWORD dwGroup, DWORD dwProp,
                                       LPCSTR szText);
BOOL K5DBREG_APICALL K5XReg_SetProfile (LPCSTR szLib, DWORD dwID, LPCSTR szProf);
BOOL K5DBREG_APICALL K5XReg_SetType (LPCSTR szLib, DWORD dwID, LPCSTR szType);

/////////////////////////////////////////////////////////////////////////////
// AS-i Profiles management

#define K5DBREGASI_IO(p)                ((p) & 0x000f)
#define K5DBREGASI_ID(p)                (((p)>>4) & 0x000f)
#define K5DBREGASI_ID1(p)               (((p)>>8) & 0x000f)
#define K5DBREGASI_ID2(p)               (((p)>>12) & 0x000f)
#define K5DBREGASI_PRF(io,id,id1,id2)   (((io) & 0x000f) \
                                        |(((id)<<4) & 0x00f0) \
                                        |(((id1)<<8) & 0x0f00) \
                                        |(((id2)<<12) & 0xf000))

DWORD K5DBREG_APICALL K5DBRegASi_GetNbProfile ( // get nb profiles
	void
	); // RETURN: number of registered profiles

DWORD K5DBREG_APICALL K5DBRegASi_GetProfiles (  // enumerate
	DWORD *phProfiles                           // OUT: profile IDs
	); // RETURN: number of registered profiles

LPCSTR K5DBREG_APICALL K5DBRegASi_GetDesc (     // profile description
    DWORD hProfile,                             // profile ID
    DWORD *pdwProfile                           // profile number
    ); // RETURN: profile name

BOOL K5DBREG_APICALL K5DBRegASi_SetDesc (       // profile description
    DWORD hProfile,                             // profile ID
    DWORD dwProfile,                            // profile number
    LPCSTR szName                               // new name
    ); // RETURN: true if ok

DWORD K5DBREG_APICALL K5DBRegASi_Create (       // new profile
    DWORD dwProfile,                            // profile number
    LPCSTR szName                               // profile name
    ); // RETURN: profile ID or 0 if error

BOOL K5DBREG_APICALL K5DBRegASi_Delete (        // delete profile
    DWORD hProfile                              // profile ID
    ); // RETURN: true if ok

BOOL K5DBREG_APICALL K5DBRegASi_ProfileToStr (  // profile to string
    DWORD dwProfile,                            // profile number
    LPSTR szProfile                             // "io.id.id1.id2" (7 char)
    ); // RETURN: true if ok

BOOL K5DBREG_APICALL K5DBRegASi_StrToProfile (  // string to profile
    LPCSTR szProfile,                           // "io.id.id1.id2"
    DWORD *pdwProfile                           // profile number
    ); // RETURN: true if ok

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // !defined(_K5DBREGAPI_H__INCLUDED_)
