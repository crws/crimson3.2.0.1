#if !defined(_W5EDITSTAPI_H_)
#define _W5EDITSTAPI_H_

#define W5EDITST_CLASSNAME		_T("W5EditST")
/*
#define W5EDITSTN_DBLCLK        1
#define W5EDITSTN_RCLICK        2
*/
#ifdef __cplusplus
extern "C" {
#endif

#ifdef W5EDITSTDLL
	/* Build the DLL */
	#define W5EDITST_APICALL __declspec(dllexport)
#else
	#define W5EDITST_APICALL __declspec(dllimport)
#endif
/////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif

#endif // !defined(_W5EDITSTAPI_H_)

