#if !defined(_W5EditLDAPI_H_)
#define _W5EditLDAPI_H_

#define W5EDITLD_CLASSNAME		_T("W5EDITLD")

/*
#define W5EDITLDN_DBLCLK        1
#define W5EDITLDN_RCLICK        2
*/
#ifdef __cplusplus
extern "C" {
#endif

#ifdef W5EDITLDDLL
	/* Build the DLL */
	#define W5EDITLD_APICALL __declspec(dllexport)
#else
	#define W5EDITLD_APICALL __declspec(dllimport)
#endif

#ifdef __cplusplus
}
#endif

#endif // !defined(_W5EditLDAPI_H_)

