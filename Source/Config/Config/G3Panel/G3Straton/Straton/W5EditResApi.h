#if !defined(_W5EDIRESAPI_H_)
#define _W5EDIRESAPI_H_

#include "W5EditApi.h"

//languages
#define LANGUAGE_DEF    _T("DEF")
#define LANGUAGE_USA    _T("USA")
#define LANGUAGE_FRA    _T("FRA")
#define LANGUAGE_GER    _T("GER")
#define LANGUAGE_ITA    _T("ITA")
#define LANGUAGE_SPA    _T("SPA")
#define LANGUAGE_KOR    _T("KOR")
#define LANGUAGE_RUS    _T("RUS")
#define LANGUAGE_POR    _T("POR")
#define LANGUAGE_CHT    _T("CHT")
#define LANGUAGE_CHS    _T("CHS")

///////////////// key words for translation ///////////////////////////////////
//String                                ID      Default value

//for TL dll, range 0 - 999
#define IDSTL_OK                        0       //"OK"
#define IDSTL_CANCEL                    1       //"Cancel"
#define IDSTL_HELP                      2       //"Help"
#define IDSTL_DLGCOPYCOL_COPYTO         3       //"Copy to:"
#define IDSTL_DEFINEO_TITLE             4       //"Library Defines (OEM)"
#define IDSTL_DEFINEC_TITLE             5       //"Common defines"
#define IDSTL_DEFINEG_TITLE             6       //"Global defines"
#define IDSTL_DEFINEL_TITLE             7       //"Local Defines"
#define IDSTL_TUTORIAL_DEFINE_TITLE     8       //"Define"
#define IDSTL_DLGDIC_COMMENT            9       //"Description"
#define IDSTL_DLGDIC_NAME               10      //"Name"
#define IDSTL_DLGDIC_VALUE              11      //"Value"
#define IDSTL_RECIPE                    12      //"NewRecipe"
#define IDSTL_TRIG_OUTRANGE             13      //"Value invalid or out of range"
#define IDSTL_SYMBOLUNKNOWN             14      //"Symbol unknown!"
#define IDSTL_CANNOTSENDRECIPE          15      //"Too many variables.\nRecipe can not be sent to the target."
#define IDSTL_CHECK                     16      //"Enabled"
#define IDSTL_PERIODPRG                 17      //"Period"
#define IDSTL_PHASE                     18      //"Phase"
#define IDSTL_DLGDIC_PROPS              19      //"Properties"
#define IDSTL_DLGDIC_TYPE               20      //"Type"
#define IDSTL_DLGDIC_DIM                21      //"Dim."
#define IDSTL_DLGDIC_ATTR               22      //"Attrib."
#define IDSTL_DLGDIC_SYB                23      //"Syb."
#define IDSTL_DLGDIC_INIT               24      //"Init value"
#define IDSTL_DLGDIC_ALIAS              25      //"Tag"
#define IDSTL_DLGDIC_GLOBAL             26      //"Global variables"
#define IDSTL_DLGDIC_RETAIN             27      //"RETAIN variables"
#define IDSTL_DLGDIC_IX                 28      //"Boolean Inputs"
#define IDSTL_DLGDIC_QX                 29      //"Boolean Outputs"
#define IDSTL_DLGDIC_ID                 30      //"Analog Inputs"
#define IDSTL_DLGDIC_QD                 31      //"Analog Inputs"
#define IDSTL_DLGDIC_IS                 32      //"String Inputs"
#define IDSTL_DLGDIC_QS                 33      //"String Outputs"
#define IDSTL_DLGDIC_IW                 34      //"Word Inputs"
#define IDSTL_DLGDIC_QW                 35      //"Word Outputs"
#define IDSTL_DLGDIC_INPUT              36      //"Input"
#define IDSTL_DLGDIC_OUTPUT             37      //"Output"
#define IDSTL_DLGDIC_IN                 38      //"IN"
#define IDSTL_DLGDIC_OUT                39      //"OUT"
#define IDSTL_DLGDIC_READONLY           40      //"Read Only"
#define IDSTL_DLGDIC_ADDED              41      //"Added"
#define IDSTL_DLGDIC_DELETED            42      //"Deleted"
#define IDSTL_DLGDIC_INPARAM            43      //"Input Parameter"
#define IDSTL_DLGDIC_OUTPARAM           44      //"Output Parameter"
#define IDSTL_DLGDIC_PRIVATE            45      //"Private Variable"
#define IDSTL_DLGDIC_EXTERN             46      //"External"
#define IDSTL_CONFIRMDELETESTRUCTURE    47      //"Do you want to delete this structure?"
#define IDSTL_GRAPHICLIST               48      //"Graphic"
#define IDSTL_SPYLIST                   49      //"Spy"
#define IDSTL_RECIPELIST                50      //"Recipe"
#define IDSTL_PROMPT                    51      //"Command Line"
#define IDSTL_PROGRAMLOCKED             52      //"Program %s is in use.\nPlease close it before modify."
#define IDSTL_CONFIRMDELETECHILDREN     53      //"Do you want to delete this program and all its children?"
#define IDSTL_CONFIRMDELETEPROGRAM      54      //"Do you want to delete this program?"
#define IDSTL_PROGNAMELONG              55      //"Program name is too long!"
#define IDSTL_UNTITLEDSFC               56      //"UntitledSFC"
#define IDSTL_UNTITLEDST                57      //"UntitledST"
#define IDSTL_UNTITLEDFBD               58      //"UntitledFBD"
#define IDSTL_UNTITLEDLD                59      //"UntitledLD"
#define IDSTL_UNTITLEDIL                60      //"UntitledIL"
#define IDSTL_TASK                      61      //"Task"
#define IDSTL_TIME                      62      //"Time"
#define IDSTL_ID                        63      //"ID"
#define IDSTL_STRING                    64      //"String"
#define IDSTL_BADSYNTAXPROGRAM          65      //"Bad syntax!"
#define IDSTL_SIGNAL                    66      //"Signal"
#define IDSTL_DLGCOLUMN                 67      //"Arrange columns"
#define IDSTL_AVAILABLE                 68      //"Available:"
#define IDSTL_VISIBLE                   69      //"Visible:"
#define IDSTL_DEFAULT                   70      //"Default"
#define IDSTL_MOVEUP                    71      //"Move Up"
#define IDSTL_MOVEDOWN                  72      //"Move Down"
#define IDSTL_CATEGORYPROJECT           73      //"(Project)"
#define IDSTL_ALL                       74      //"(All)"
#define IDSTL_RECENT                    75      //"(Recent)"
#define IDSTL_BLOCKSUSED                76      //"(Used)"
#define IDSTL_NODEBUGINFO               77      //"No debug infos available"
#define IDSTL_INSTANCE                  78      //"Instance"
#define IDSTL_EVENT                     79      //"Event description"
#define IDSTL_CONFIRMDELETEFOLDER       80      //"Do you want to delete this folder?"
#define IDSTL_STRINGTABLES              81      //"String Tables"
#define IDSTL_SIGNALS                   82      //"Signals"
#define IDSTL_CONFIRMDEL                83      //"Do you want to delete this file '%s'?"
#define IDSTL_NEWSPY                    84      //"NewSpy"
#define IDSTL_NEWGRAPHICS               85      //"NewGraphics"
#define IDSTL_STRINGTABLE               86      //"StringTable"
#define IDSTL_OBJECT                    87      //"Object"
#define IDSTL_PRODUCTNAME               88      //"STRATON"
#define IDSTL_FORMATEXPORTXMLCSV        89      //"Export files (*.CSV)|*.CSV||"
#define IDSTL_FORMATIMPORTXMLCSV        90      //"Import files (*.CSV)|*.CSV||"
#define IDSTL_PROPERTYMISSED            91      //"At least one property has not been checked.\nMaybe the imported file is not compatible with the current grid selection.\nDo you wish to import file anyway?"
#define IDSTL_CONFIGEXIST               92      //"Configuration already exist.\nOverwrite it?"
#define IDSTL_FORMATIMPORTFIELDBUS      93      //"Import Fieldbus Files (*.FB5)|*.FB5||"
#define IDSTL_IMPORTERROR               94      //"The import file is not compatible with the selected fieldbus configuration."
#define IDSTL_FORMATEXPORTFIELDBUS      95      //"Export Fieldbus Files (*.FB5)|*.FB5||"
#define IDSTL_CONFIRMREMOVECONFIG       96      //"Do you want to remove this configuration?"
#define IDSTL_CREATIONNVAR_FAILED       97      //"Creation failed"
#define IDSTL_CONFIGLIST                98      //"Add Configuration"
#define IDSTL_GROUPLIST                 99      //"Choose a configuration"
#define IDSTL_SYMBOL                    100     //"Symbol"
#define IDSTL_BIND                      101     //"Binding"
#define IDSTL_ADDRESS                   102     //"Address"
#define IDSTL_PORT                      103     //"Port"
#define IDSTL_NETWORK                   104     //"Network"
#define IDSTL_CS                        105     //"Connection status"
#define IDSTL_VS                        106     //"Variable status"
#define IDSTL_VT                        107     //"Variable time stamp"
#define IDSTL_VD                        108     //"Variable date stamp"
#define IDSTL_IDENTIFIER                109     //"Identifier"
#define IDSTL_CHECKDATA                 110     //"Exchange data"
#define IDSTL_CHECKSTATUS               111     //"Variable error status"
#define IDSTL_CHECKDATE                 112     //"Variable date stamp"
#define IDSTL_CHECKTIME                 113     //"Variable time stamp"
#define IDSTL_CHECKCONNECTION           114     //"Connection error status"
#define IDSTL_DESTINATION               115     //"Destination"
#define IDSTL_HYSTERESISP               116     //"Positive hysteresis"
#define IDSTL_HYSTERESISN               117     //"Negative hysteresis"
#define IDSTL_ERR_VARSYB                118     //"You must specify the symbol of a declared variable"
#define IDSTL_SYMBOLALREADYUSED         119     //"Symbol is already used!"
#define IDSTL_IDUSED                    120     //"The ID symbol is already used."
#define IDSTL_BOOLEANCANTBEUSED         121     //"Can not use hysteresis on boolean variables!"
#define IDSTL_GROUP                     122     //"Group"
#define IDSTL_EXTERNVARIABLE            123     //"Extern Variable"
#define IDSTL_VARIABLE                  124     //"Variable"
#define IDSTL_PUBLICVARIABLE            125     //"Public Variable"
#define IDSTL_HYSTERESIS                126     //"Hysteresis"
#define IDSTL_POSITIVE                  127     //"Positive:"
#define IDSTL_NEGATIVE                  128     //"Negative:"
#define IDSTL_SYMMETRIC                 129     //"Symmetric"
#define IDSTL_VARALREADYPROFILED        130     //"Can not change var profile.\nVariable has already a profile."
#define IDSTL_DLGDIC_VARUSERGROUP       131     //"User Group"
#define IDSTL_COLUMN                    132     //"Columns..."
#define IDSTL_VISIBLE2                  133     //"Visible"
#define IDSTL_WIDTH                     134     //"Width"
#define IDSTL_FILTER                    135     //"Filter"
#define IDSTL_CANNOTACTIVATEHISTORY     136     //"Can not activate history in this project\nPerhaps project is open twice"
#define IDSTL_RESETFILTER               137     //"Reset Filter"
#define IDSTL_DIAGRAM                   138     //"# Diagram"
#define IDSTL_COLOR                     139     //"Color"
#define IDSTL_CURVE                     140     //"Soft Scope"
#define IDSTL_ENDIAN                    141     //"Runtime conventions"
#define IDSTL_PASSWORD                  142     //"Password"
#define IDSTL_ACCESS                    143     //"Access Rights"
#define IDSTL_PROTECT                   144     //"Protection"
#define IDSTL_MAXLEN                    145     //"Maximum length"
#define IDSTL_DEFVALUE                  146     //"Default value"
#define IDSTL_EDITMODE                  147     //"Editing mode"
#define IDSTL_MIN                       148     //"Minimum value"
#define IDSTL_MAX                       149     //"Maximum value"
#define IDSTL_BIGENDIAN                 150     //"Big Endian"
#define IDSTL_LITTLEENDIAN              151     //"Little Endian"
#define IDSTL_READWRITE                 152     //"Read/Write"
#define IDSTL_YES                       153     //"Yes"
#define IDSTL_NO                        154     //"No"
#define IDSTL_INHERIT                   155     //"Inherit"
#define IDSTL_EDITBOOL                  156     //"Checkbox"
#define IDSTL_EDITEDIT                  157     //"Edit box"
#define IDSTL_EDITLIST                  158     //"List box"
#define IDSTL_EDITCOMBO                 169     //"Combo box"
#define IDSTL_USER                      170     //"User"
#define IDSTL_ADD                       171     //"Add"
#define IDSTL_DEL                       172     //"Delete"
#define IDSTL_ZOOM                      173     //"Zoom"
#define IDSTL_AUTOSCROLL                174     //"Auto scroll"
#define IDSTL_REFRESHRATE               175     //"Refresh rate:"
#define IDSTL_REFRESH                   176     //"Refresh"
#define IDSTL_TIMERANGES                177     //"Time ranges"
#define IDSTL_TIMERANGE                 178     //"Time Range:"
#define IDSTL_FROM                      179     //"From:"
#define IDSTL_TO                        180     //"To:"
#define IDSTL_PROGRAMS                  181     //"Programs"
#define IDSTL_SECTIONFIELDBUS           182     //"Fieldbus Configurations"
#define IDSTL_SECTIONPROFILE            183     //"Profiles"
#define IDSTL_SECTIONIO                 184     //"I/Os"
#define IDSTL_NEWFOLDER                 185     //"New Folder"
#define IDSTL_FILELOCKED                186     //"File '%s' is in use.\nPlease close it before modify."
#define IDSTL_DELETEFOLDERANDCHILDREN   187     //"Do you want to delete this folder and all its children?"
#define IDSTL_SECTIONBINDING            188     //"Binding Configuration"
#define IDSTL_DLGDIC_GAIN               189     //"Gain"
#define IDSTL_DLGDIC_OFFSET             190     //"Offset"
#define IDSTL_RESULT                    191     //"Results"
#define IDSTL_BY                        192     //"By:"
#define IDSTL_HMI                       193     //"Embedded HMI"
#define IDSTL_HMISTRING                 194     //"Strings"
#define IDSTL_HMIBMP                    195     //"Bitmaps"
#define IDSTL_HMIFONT                   196     //"Fonts"
#define IDSTL_HMISCREEN                 197     //"Screens"
#define IDSTL_SELECTDEVICE              198     //"You have to choose a device to continue."
#define IDSTL_PATH                      199     //"Path"
#define IDSTL_PALETTE                   200     //"Palette"
#define IDSTL_HEIGHT                    201     //"Height"
#define IDSTL_DEVFONT                   202     //"Device System Font"
#define IDSTL_ALIGNCENTER               203     //"Center"
#define IDSTL_ALIGNRIGHT                204     //"Right"
#define IDSTL_ALIGNLEFT                 205     //"Left"
#define IDSTL_DEVICE                    206     //"Device"
#define IDSTL_SCREEN                    207     //"Screen"
#define IDSTL_BKBITMAP                  208     //"Background Bitmap"
#define IDSTL_SIZE                      209     //"Screen Size"
#define IDSTL_ALIGNTOP                  210     //"Top"
#define IDSTL_ALIGNBOTTOM               211     //"Bottom"
#define IDSTL_DIRECTION                 212     //"Direction"
#define IDSTL_ALIGNMENT                 213     //"Alignment"
#define IDSTL_BKCOLOR                   214     //"Background Color"
#define IDSTL_NONE                      215     //"(None)"
#define IDSTL_CONFIRMDELETEHMI          216     //"All HMI resources will be deleted.\nDo you want to continue?"
#define IDSTL_DIRTOP                    217     //"To Top"
#define IDSTL_DIRLEFT                   218     //"To Left"
#define IDSTL_DIRBOTTOM                 219     //"To Bottom"
#define IDSTL_DIRRIGHT                  220     //"To Right"
#define IDSTL_MINWIDTH                  221     //"Minimum width: %d"
#define IDSTL_STYLEHISTO                222     //"Histogram"
#define IDSTL_STYLEPOINT                223     //"Points"
#define IDSTL_DRAWSTYLE                 224     //"Drawing style"
#define IDSTL_CANNOTMODIFYDURINGDEBUG   225     //"Can not modify (move, copy or clear) item during debug"
#define IDSTL_ENUMTYPE                  226     //"EnumType%d"
#define IDSTL_BITFIELDTYPE              227     //"BitField%d"
#define IDSTL_TYPES                     228     //Types
#define IDSTL_DLGDIC_INOUT              229     //"INOUT"
#define IDSTL_DIRRIGHTTOP               230     //"To Top-Right"
#define IDSTL_DIRLEFTTOP                231     //"To Top-Left"
#define IDSTL_DIRLEFTBOTTOM             232     //"To Bottom-Left"
#define IDSTL_DIRRIGHTBOTTOM            233     //"To Bottom-Right"
#define IDSTL_MASK                      234     //"Mask"
#define IDSTL_WEBITEM                   235     //"WEB Item"
#define IDSTL_WEBCOMMENT                236     //"For WEB pages"
#define IDSTL_OTHERITEM                 237     //"Others"
#define IDSTL_OTHERCOMMENT              238     //"For all other extern files (text or binary)"
#define IDSTL_CREATEEXTERNITEM          239     //"Create Extern Item"
#define IDSTL_CHOOSEURL                 240     //"Item name / path / URL"
#define IDSTL_ITEMTYPE                  241     //"Item type:"
#define IDSTL_ITEM                      242     //"Item"
#define IDSTL_QUALITY                   243     //"Quality"
#define IDSTL_FORMAT                    244     //"Format"
#define IDSTL_SCAN                      245     //"Scan"
#define IDSTL_IDALREADYEXIST            246     //"ID already exists!"        
#define IDSTL_PALETTENOTFOUND           247     //"Palette not found!"
#define IDSTL_DEVICEFONTNOTFOUND        248     //"Device System Font not found!"
#define IDSTL_DEVICESDIFFERS            249     //"Can not copy screen.\nDevices are different."  
#define IDSTL_GENERICTEXT_FORMAT        250     //"File (*.%s)|*.%s| All Files (*.*) |*.*||"
#define IDSTL_IEC850                    251     //"ied"
#define IDSTL_FORMATIMPORTIEC850        252     //"File ICD(*.icd)|*.icd|File SCL(*.scl)|*.scl|File XML(*.xml)|*.xml|All Files (*.*)|*.*||"
#define IDSTL_PROJECTLOCKED             253     //"Can not perform this operation.\nThe project is in read only"
#define IDSTL_NAMEEXIST                 254     //"This name is already used.\nPlease choose another name.
#define IDSTL_CHOOSEWEBPAGE             255     //"Choose the WEB page or IP Address"
#define IDSTL_FINDWEBPAGE               256     //"Search for a WEB Page"
#define IDSTL_CREATENEWSCREEN           257     //"Create Screen"
#define IDSTL_VARIABLES                 258     //"Variables"
#define IDSTL_ONLYDATE                  260     //"Month Day, Year"
#define IDSTL_ONLYTIME                  261     //"Hour:Minute:Seconde"
#define IDSTL_BOTH                      262     //"Month Day, Year (Hour:Minute:Seconde)"
#define IDSTL_YMD                       263     //"Year.Month.Day"
#define IDSTL_COMMENT                   264     //"Comment"
#define IDSTL_NAMEISEMPTY               265     //"Name is empty."
#define IDSTL_EDITSTRUCTURE             266     //"EditStructure"
#define IDSTL_OVERWRITEPROPS            267     //"Are you sure to write all properties of the selected variables?"
#define IDSTL_CREATEVARIABLE            268     //"Create Variable"
#define IDSTL_EDITVARIABLE              269     //"Edit Variable"
#define IDSTL_SYSTEM                    270     //"System"
#define IDSTL_IDALREADYEXISTREPLACE     271     //"ID already exists.\nDo you want to replace it?"
#define IDSTL_YESALL                    272     //"Yes All"
#define IDSTL_NOALL                     273     //"No All"
#define IDSTL_SELECTVARS                274     //"Select Variables"
#define IDSTL_REPLACE                   275     //"Replace"
#define IDSTL_INCOLUMNS                 276     //"In Columns"
#define IDSTL_WHAT                      277     //"What:"
#define IDSTL_MATCHCASE                 278     //"Match Case"
#define IDSTL_PREFIX                    279     //"Prefix"
#define IDSTL_SUFFIX                    280     //"Suffix"
#define IDSTL_EVERYWHERE                281     //"Everywhere"
#define IDSTL_WHOLEWORD                 282     //"Whole Word"
#define IDSTL_SEARCHIN                  283     //"Search:"
#define IDSTL_REPLACEIN                 284     //"Replace:"
#define IDSTL_RENAMEVARS                285     //"Rename Variables"
#define IDSTL_IN                        286     //"Where:"
#define IDSTL_UNDO                      287     //"Undo"
#define IDSTL_CUT                       288     //"Cut"
#define IDSTL_COPY                      289     //"Copy"
#define IDSTL_PASTE                     290     //"Paste"
#define IDSTL_DELETE                    291     //"Clear"
#define IDSTL_SELECTALL                 292     //"Select All"
#define IDSTL_FILE                      293     //"File :"
#define IDSTL_VERSION                   294     //"Version :"
#define IDSTL_DRIVERINFOS               295     //"Driver Informations"
#define IDSTL_SELECT                    296     //"Select"
#define IDSTL_CANNOTADDMORECHILDREN     297     //"Can not add more than %d children."
#define IDSTL_BADFILENAME               298     //"A file name can not use these characters: \\ / : * ? \" < > |"
#define IDSTL_MAINTASK                  299     //"Main task"
#define IDSTL_DLGDIC_PUBLIC             300     //"Public"
#define IDSTL_CLEARREPORT               301     //"Clear report"
#define IDSTL_DLGDIC_OWNER              302     //"Owner"
#define IDSTL_CLEARFRAME                303     //"Delete frames"
#define IDSTL_ITERATE                   304     //"Iterate"
#define IDSTL_DECLAREINDB               305     //"Declare variables in database"
#define IDSTL_SETOFFSET                 306     //"Set offsets"
#define IDSTL_SETVARIABLES              307     //"Set Variables"
#define IDSTL_REMOVEOLDVAR              308     //"Remove old variables"
#define IDSTL_TESTSEQ                   309     //"NewTestSeq"
#define IDSTL_INPUTS                    310     //"Inputs"
#define IDSTL_ALLPROJECTS               311     //"(All Projects)"
#define IDSTL_NEWMULTISPYLIST           312     //"NewMultiSpy"
#define IDSTL_SECTIONMULTIBINDING       313     //"Global Binding Editor"
#define IDSTL_NEW                       314     //"New"
#define IDSTL_NEWMULTISOFTSCOPE         315     //"NewMultiSoftscope"
#define IDSTL_READMAX                   316     //"Can not read more than %d items."
#define IDSTL_LOCKBYSOURCECONTROL       317     //"Can not perform this operation.\nThe object is locked by source control."
#define IDSTL_HMILOCKBYSOURCECONTROL    318     //"Can not perform this operation.\nHMI is locked by source control."
#define IDSTL_WORKSPACELOCKBYSOURCECONTROL 319  //"Can not perform this operation.\nWorkspace is locked by source control."
#define IDSTL_PRJLOCKBYSOURCECONTROL    320     //"Can not perform this operation.\nProject is locked by source control."
#define IDSTL_CONFIGOCKBYSOURCECONTROL  321     //"Can not perform this operation.\nConfiguration is locked by Source Control."
#define IDSTL_GLOBALLOCKBYSOURCECONTROL 322     //"Can not declare variables.\nThe global group is locked by source control."
#define IDSTL_BINDINGLOCKBYSOURCECONTROL 323    //"Can not perform this operation.\nThe binding configuration is locked by source control."
#define IDSTL_ENTERVALUE                324     //"Enter Value"
#define IDSTL_HEXADECIMAL               325     //"Hexadecimal:"
#define IDSTL_DECIMAL                   326     //"Decimal:"
#define IDSTL_BINARY                    327     //"Binary:"
#define IDSTL_ABORT                     328     //"Abort"
#define IDSTL_RETRY                     329     //"Retry"
#define IDSTL_IGNORE                    330     //"Ignore"
#define IDSTL_CONTINUE                  331     //"Continue"
#define IDSTL_REMOVEOLDITEMS            332     //"Node already contains children.\nDo you want to erase them before import new ones?"
#define IDSTL_APPLY                     333     //"Apply"
#define IDSTL_WIZBACK                   334     //"< Previous"
#define IDSTL_WIZNEXT                   335     //"> Next"
#define IDSTL_WIZFINISH                 336     //"Finish"
#define IDSTL_WIZHTML5                  337     //"Generate HTML5 WEB page"
#define IDSTL_VARSHOULDBEEMBEDDED       338     //"Symbol should be embedded"
#define IDSTL_MTSHAREDSOURCECONTROL     339     //"Can not perform this operation.\nThe 'PUBLIC' property is locked by source control."
#define IDSTL_SORTBY                    340     //"Sort by:"
#define IDSTL_SORTVARIABLES             341     //"Sort Variables"
#define IDSTL_ASCENDING                 342     //"Ascending"
#define IDSTL_DEFAULT2                  343     //"(Default)"
#define IDSTL_SUBPROGRAMS               344     //"Sub-Programs"

//for ACT dll, range 1000 - 1999
#define IDSACT_GRID                 1000    //"Grid Settings"
#define IDSACT_SHOWGRID2            1001    //"Show Grid"
#define IDSACT_SNAPTOGRID           1002    //"Snap to Grid"
#define IDSACT_XMAX                 1003    //"X max"
#define IDSACT_YMAX                 1004    //"Y max"
#define IDSACT_ITEMPROPERTIES       1005    //"Graphic Item Properties"

//for HMI dll, range 2000 - 2999
#define IDSHMI_SCREENPROPERTIES     2000    //"Screen Properties"
#define IDSHMI_OBJECTPROPERTIES     2001    //"Object Properties"
#define IDSHMI_OBJECTOUTOFDEVICE    2002    //"%s (x=%d, y=%d, sx=%d, sy=%d): size is out of device area (x=%d, y=%d, sx=%d, sy=%d)"
#define IDSHMI_GRIDSTEP             2003    //"Grid Step"

//for PRINT dll, range 3000 - 3999
#define IDSPRINT_NOTROOM            3000    //"Not enough room to insert item."
#define IDSPRINT_POSITION           3001    //"Position"
#define IDSPRINT_SIZE               3002    //"Size"
#define IDSPRINT_BORDERSIZE         3003    //"Border Size"
#define IDSPRINT_BORDERCOLOR        3004    //"Border Color"
#define IDSPRINT_NBHEADERS          3005    //"Nb Headers"
#define IDSPRINT_FONT               3006    //"Font"
#define IDSPRINT_FONTCOLOR          3007    //"Font Color"
#define IDSPRINT_BACKCOLOR          3008    //"Back Color"
#define IDSPRINT_TEXT               3009    //"Text"
#define IDSPRINT_BITMAPPATH         3010    //"Bitmap Path"
#define IDSPRINT_AREA               3011    //"Printing control area"
#define IDSPRINT_DOCNAME            3012    //" Document name"
#define IDSPRINT_PAGENB             3013    //" Page Number"
#define IDSPRINT_BUILD              3014    //" Build Version"
#define IDSPRINT_MENUADDTEXT        3015    //"Add Text"
#define IDSPRINT_MENUADDDOCNAME     3016    //"Add Document name"
#define IDSPRINT_MENUADDPAGENB      3017    //"Add Page Number"
#define IDSPRINT_MENUADDBUILD       3018    //"Add Build Version"
#define IDSPRINT_MENUADDDATE        3019    //"Add Date"
#define IDSPRINT_MENUADDBITMAP      3020    //"Add Bitmap"
#define IDSPRINT_ENTERYOURTEXT      3021    //"Enter your text"
#define IDSPRINT_SELECTBITMAP       3022    //"Select Bitmap..."
#define IDSPRINT_PROJECTNAME        3023    //"Project Name"
#define IDSPRINT_DOCDESCRIPTION     3024    //"Document Description"
#define IDSPRINT_PROJECTDESCRIPTION 3025    //"Project Description"
#define IDSPRINT_XTHICKNESS         3026    //"X Thickness"
#define IDSPRINT_YTHICKNESS         3027    //"Y Thickness"
#define IDSPRINT_SHOWFOLIO          3028    //"Show Folio"

//for IED dll, range    4000 - 4999
#define IDSIED_SUBNETWORK           4000    //"SubNetwork"
#define IDSIED_TEXT                 4001    //"Text"
#define IDSIED_COMMUNICATION        4002    //"Communication"
#define IDSIED_SELECTFCDA           4003    //"Select FCDAs"
#define IDSIED_SELECTGOOSETEMPLATE  4004    //"Select Goose Template"
#define IDSIED_TEMPLATE             4005    //"Template:"
#define IDSIED_LASTUSED             4006    //"(Last Used)"
#define IDSIED_GOOSEBLOCKDEFAULT    4007    //"GOOSE Control Block default"
#define IDSIED_SELECTIEDTOIMPORT    4008    //"Select IED to Import"
#define IDSIED_SELECTLNODE          4009    //"Select Logical Node"
#define IDSIED_BASECLASS            4010    //"Base Class:"
#define IDSIED_TYPENAME             4011    //"Type Name:"
#define IDSIED_NOCLASSSELECTED      4012    //"No class selected!"
#define IDSIED_NOTYPESELECTED       4013    //"No type selected!"
#define IDSIED_SELECTRCBTEMPLATE    4014    //"Select Report Control Block Template"
#define IDSIED_BRCB                 4015    //"BRCB_Default"
#define IDSIED_URCB                 4016    //"URCB_Default"
#define IDSIED_SERVICES             4017    //"Services"
#define IDSIED_ADDNEWTYPE           4018    //"Add New Type"
#define IDSIED_DETAILS              4019    //"Details"
#define IDSIED_EXPANDALL            4020    //"Expand All"
#define IDSIED_COLLAPSEALL          4021    //"Collapse All"
#define IDSIED_EXPANDALLCHECKED     4022    //"Show Checked"
#define IDSIED_TYPEALREADYEXIST     4023    //"This type already exists!"
#define IDSIED_ATLEASTONECHECKED    4024    //"At least one of them should be checked:"
#define IDSIED_MANDATORY            4025    //"This is mandatory"
#define IDSIED_VIEWTYPE             4026    //"View Type"
#define IDSIED_CONFIGWILLDELETED    4027    //"The current configuration will be deleted.\nDo you want to continue?"
#define IDSIED_IMPORTSCL            4028    //"Import SCL File"
#define IDSIED_IMPORTSCLFORMAT      4029    //"ICD files (*.icd)|*.icd|CID files (*.cid)|*.cid|SCL files (*.scl)|*.scl|XML files (*.xml)|*.xml|SCD files (*.scd)|*.scd|All files (*.*) |*.*||"
#define IDSIED_INSTANCECANNOTBEEMPTY 4030   //"This instance name can not be empty."
#define IDSIED_INSTANCEALREADYUSED  4031    //"This instance name is already used by another logical device.\nPlease use another instance name."
#define IDSIED_DATASETCANOTBEEMPTY  4032    //"This data set name can not be empty."
#define IDSIED_DATASETALREADYUSED   4033    //"This data set name is already used by another data set.\nPlease use another name."
#define IDSIED_LDEVICEATLEASTLLN0   4034    //"LDevice '%s' must have one LLN0 node"
#define IDSIED_LDEVICEATLEASTLPHD   4035    //"LDevice '%s' must have one LPHD node"
#define IDSIED_DATASETDOESNOTEXIST1 4036    //"Report Control Block '%s' : Data Set '%s' does not exist in Logical Device '%s'"
#define IDSIED_DATASETDOESNOTEXIST2 4037    //"Goose Control Block '%s' : Data Set '%s' does not exist in Logical Device '%s'"
#define IDSIED_DATASETNOTUSED       4038    //"Warning: Data Set '%s' not used"
#define IDSIED_CHECKOK              4039    //"Check succeeded"
#define IDSIED_CHECKKO              4040    //"Check failed"
#define IDSIED_NERRORS              4041    //" - %d errors"
#define IDSIED_1ERROR               4042    //" - 1 error"
#define IDSIED_NWARNINNGS           4043    //" - %d warnings"
#define IDSIED_1WARNING             4044    //" - 1 warning"
#define IDSIED_ERRORINDATATYPESXML  4045    //"Can not add the types below. There are error(s) in 'IEC61850 DataTypes.xml' file"
#define IDSIED_FILEERROS            4046    //"File has errors.\nSee output."
#define IDSIED_IMPORTXMLICD         4047    //"XML files (*.xml)|*.xml| ICD files (*icd)|*.icd||"
#define IDSIED_TYPEISUSED           4048    //"Type is used.\nYou can not delete a used type"
#define IDSIED_NOTHINGTODELETE      4049    //"Nothing to delete."
#define IDSIED_CLEARUNUSED          4050    //"Clear Unused Types"
#define IDSIED_TYPESREMOVED         4051    //"Types removed:"
#define IDSIED_SEEOUTPUT            4052    //"See Output for results"
#define IDSIED_NOTHINGDONE          4053    //"Nothing done.\nAll types are used."
#define IDSIED_CANNOTCREATEVARS     4054    //"Can not create variables.\nProject is in debug."
#define IDSIED_CREATEVARS           4055    //"Create Variables"
#define IDSIED_CONFIRMCREATEFORDEVICE 4056  //"Create variables for all logical nodes in logical device?"
#define IDSIED_CONFIRMCREATEFORIED  4057    //"Create variables for all logical nodes in tree?"
#define IDSIED_VARSKIPPED           4058    //"Variable creation '%s' (%s) skipped"
#define IDSIED_VARNOTCREATED        4059    //"Can not create var '%s' (type '%s' unknown)."
#define IDSIED_VARCREATION1         4060    //"Variable creation '%s' (%s --> %s) "
#define IDSIED_VARCREATION2         4061    //"Variable creation '%s' (%s --> %s[%d]) "
#define IDSIED_FAILED               4062    //"Failed!"
#define IDSIED_NODATASETDECLARED    4063    //"No Data Set declared"
#define IDSIED_USEDINRCB            4064    //"     Used in Report Control Block '%s'"
#define IDSIED_USEDINGOOSE          4065    //"     Used in Goose Control Block '%s'"
#define IDSIED_DSNOTUSED            4066    //"Data Set '%s' not used"
#define IDSIED_DSUSEDN              4067    //"Data Set '%s' (used %d times):"
#define IDSIED_DATASETDOESNOTEXIST  4068    //"Report Control Block '%s' : Data Set '%s' does not exist in Logical Device '%s'"
#define IDSIED_SPECIFYSUBNETWORK    4069    //"You must specify a SubNetwork name"
#define IDSIED_DONAMEDOESNOTEXIST   4070    //"The DO Name '%s' used in DataSet '%s' does not exist in Logical Node '%s'"
#define IDSIED_CREATEDATYPE         4071    //"Create New DA Type"
#define IDSIED_CANNOTDELDA          4072    //"This DAType is used in %s '%s'.\nIt can not be deleted."
#define IDSIED_CANNOTDELENUM        4073    //"This ENUM Type is used in %s '%s'.\nIt can not be deleted."
#define IDSIED_CANNOTFINDLN         4074    //"Can not find logical node '%s'"
#define IDSIED_DAIDOESNOTEXIST      4075    //"The DOI/DAI '%s/%s' used in Logical Node '%s' does not exist"
#define IDSIED_DOIDOESNOTEXIST      4076    //"The DOI '%s' used in Logical Node '%s' does not exist"
#define IDSIED_CHOOSEDATYPE         4077    //"Please choose a DA Type"
#define IDSIED_CHOOSEDOTYPE         4078    //"Please choose a DO Type"
#define IDSIED_DOTYPEUSED           4079    //"This DO Type is used in Logical Node '%s'.\nIt can not be deleted."
#define IDSIED_SELECTENUMTYPE       4080    //"Select ENUM Type"
#define IDSIED_EDITTYPE             4081    //"Edit Type"
#define IDSIED_CANONLYRENAMEUSERDO  4082    //"Can only rename the user defined DOs."
#define IDSIED_RCBCANNOTUSEDATASET  4083    //"A RCB can not use a data set (%s) that uses a DA (%s)"
#define IDSIED_NEWBDA               4084    //"NewBDA"
#define IDSIED_NEWDO                4085    //"newDO"
#define IDSIED_TYPEUNKNOWN          4086    //"type unknown"
#define IDSIED_NOTFOUND             4087    //"not found"
#define IDSIED_DESCNOTFOUND         4088    //"description not found"
#define IDSIED_CATNOTFOUND          4089    //"category not found"
#define IDSIED_NBLNTYPES            4090    //"Nb LNode Types"
#define IDSIED_CATFORDO             4091    //"Categories for DO types"
#define IDSIED_ERRORS               4092    //"Errors"
#define IDSIED_NOERRORINFILE        4093    //"No error found in file"
#define IDSIED_STATISTICS           4094    //"Statistics on 'IEC61850 DataTypes.xml'"
#define IDSIED_CHOOSEENUMTYPE       4095    //"Please choose an ENUM Type"
#define IDSIED_CREATEENUMTYPE       4096    //"Create New ENUM Type"
#define IDSIED_BTYPE                4097    //"BType"
#define IDSIED_USED                 4098    //"Used"
#define IDSIED_ARRAY                4099    //"Array"
#define IDSIED_FC                   4100    //"FC"
#define IDSIED_DCHG                 4101    //"Dchg"
#define IDSIED_QCHG                 4102    //"Qchg"
#define IDSIED_DUPD                 4103    //"Dupd"
#define IDSIED_ORD                  4104    //"Ord"
#define IDSIED_CATEGORY             4105    //"Category"
#define IDSIED_TIP_ADDLDEVICE       4106    //"Add Logical Device\nAdd Logical Device"
#define IDSIED_TIP_ADDLNODE         4107    //"Add Logical Node\nAdd Logical Node"
#define IDSIED_TIP_ADDDATASET       4108    //"Add Data Set\nAdd Data Set"
#define IDSIED_TIP_ADDRCONTROLBLOCK 4109    //"Add Report Control Block\nAdd Report Control Block"
#define IDSIED_TIP_ADDGOOSE         4110    //"Add Goose\nAdd Goose"
#define IDSIED_TIP_IMPORTSCL        4111    //"Import SCL file\nImport SCL file"
#define IDSIED_TIP_CHECKTREEP       4112    //"Check Syntax\nCheck Syntax"
#define IDSIED_TIP_EDITCOMMUNICATION 4113   //"Edit communication\nEdit communication"
#define IDSIED_TIP_EDITSERVICES     4114    //"Edit services\nEdit services"
#define IDSIED_TIP_ADDTYPE          4115    //"Add Type\nAdd Type"
#define IDSIED_TIP_DELETETYPE       4116    //"Remove Type\nRemove Type"
#define IDSIED_TIP_IMPORTTYPES      4117    //"Import Types\nImport Types"
#define IDSIED_TIP_TOGGLEOUTPUT     4118    //"Show/Hide Output\nShow/Hide Output"
#define IDSIED_MENU_EDITCOMMUNICATION 4119  //"Edit Communication" 
#define IDSIED_MENU_EDITSERVICES    4120    //Edit Services"               
#define IDSIED_MENU_EDIT            4121    //"Edit"                        
#define IDSIED_MENU_CUT             4122    //"Cut"                         
#define IDSIED_MENU_COPY            4123    //"Copy"                        
#define IDSIED_MENU_PASTE           4124    //"Paste"                       
#define IDSIED_MENU_DELETE          4125    //"Delete"                      
#define IDSIED_MENU_VIEWTYPE        4126    //"View Type"                   
#define IDSIED_MENU_EDITDOI         4127    //"Edit DOI/DAI"                     
#define IDSIED_MENU_ADDLDEVICE      4128    //"Add Logical Device"          
#define IDSIED_MENU_ADDLNODE        4129    //"Add Logical Node"            
#define IDSIED_MENU_ADDDATASET      4130    //"Add Data Set"                
#define IDSIED_MENU_ADDRCONTROLBLOCK 4131   //"Add Report Control Block"
#define IDSIED_MENU_ADDGOOSE        4132    //"Add Goose"                   
#define IDSIED_MENU_EXPANDALL       4133    //"Expand All"                  
#define IDSIED_MENU_COLLAPSEALL     4134    //"Collapse All"                
#define IDSIED_MENU_SHOWUSEDDATASETS 4135   //"Show DataSets use"      
#define IDSIED_MENU_ADDTYPE         4136    //"Add Type..."                 
#define IDSIED_MENU_DELETETYPE      4137    //"Remove Type"                 
#define IDSIED_MENU_DELETEUNUSED    4138    //"Remove Unused Types"        
#define IDSIED_MENU_EDITTYPE        4139    //"Edit Type..."                
#define IDSIED_MENU_IMPORTTYPES     4140    //"Import Types"                
#define IDSIED_MENU_DOTYPES         4141    //"DO Types..."                 
#define IDSIED_MENU_DATYPES         4142    //"DA Types..."                 
#define IDSIED_MENU_ENUMTYPES       4143    //"Enum Types..."   
#define IDSIED_TIP_CREATENODE       4144    //"Create Logical Node and Add\nCreate Logical Node and Add"
#define IDSIED_MENU_CREATENODE      4145    //"Create Logical Node and Add..."
#define IDSIED_DOTYPES              4146    //"DO Types"
#define IDSIED_DATYPES              4147    //"DA Types"
#define IDSIED_ENUMTYPES            4148    //"ENUM Types"
#define IDSIED_SELECTDOTYPE         4149    //"Select DO Type"
#define IDSIED_SELECTDATYPE         4150    //"Select DA Type"
#define IDSIED_MAXDONAME            4151    //"DO '%s': name cannot exceed %d characters"
#define IDSIED_SELECTEDITION        4152    //"Select Edition"
#define IDSIED_EDITION1             4153    //"Edition 1.0"
#define IDSIED_EDITION2             4154    //"Edition 2.0"
#define IDSIED_RENAMEDO             4155    //"Rename DO Type"
#define IDSIED_CREATEDOTYPE         4156    //"Create New DO Type"
#define IDSIED_DAIENUMERROR         4157    //""The DOI/DAI '%s/%s' used in Logical Node '%s' uses an invalid enum value '%s' (Possible values are: %s)""
#define IDSIED_TIP_EXPORTSCL        4158    //"Export SCL file\nExport SCL file"
#define IDSIED_ADDSDO               4159    //"Add SDO"
#define IDSIED_ENUMEMPTY            4160    //"Enum type '%s': no value"
#define IDSIED_LNTYPES              4161    //"LN Types"

//for FF dll, range    5000 - 5999
#define IDSFF_NOREFFOUND            5000    //"No reference found"
#define IDSFF_DISPLAYCROSSREF       5001    //"Cross Ref"


//return from W5RES_DisplayQuestionAll 
#define IDTL_CANCEL 2
#define IDTL_YES    6
#define IDTL_NO     7
#define IDTL_YESALL 500
#define IDTL_NOALL  501

#ifdef __cplusplus
extern "C" {
#endif

#ifdef W5EDITRESDLL
	/* Build the DLL */
	#define W5EDITRES_APICALL __declspec(dllexport)
#else
	#define W5EDITRES_APICALL __declspec(dllimport)
#endif

    //edit boxes
BOOL      W5EDITRES_APICALL  W5RES_SelBlock     ( HWND hWndParent,  //[in] the window parent
                                                  LPCSTR szProject, //[in] project path
                                                  str_W5FB* pStrFB  //[in/out] function block structure
                                                ); //return true if selected block has changed

LPCSTR    W5EDITRES_APICALL  W5RES_SelBlockEx   ( HWND hWndParent,  //[in] the window parent
                                                  LPCSTR szProject, //[in] project path
                                                  LPCSTR szName,    //[in] function block structure
                                                  int *piNbIn,      //[in/out] number of inputs
                                                  int *piNbOut,     //[out] number of outputs
                                                  DWORD *pdwID,     //[out] the block ID (dataase ID or registry ID, see pbInstanciable)
                                                  BOOL *pbInstanciable, //[out] indicates if selected block is instanciable
                                                  BOOL *pbDBObject  //[out] indicates if selected block come from database
                                                ); //return block name if selected block has changed and empty if not


BOOL      W5EDITRES_APICALL  W5RES_EditSpy      ( HWND hWndParent,  //[in] the window parent
                                                  POINT ptPhys,     //[in] open coordinates
                                                  LPCSTR szProject, //[in] project path
                                                  LPCSTR szSymbol,  //[in] symbol name
                                                  LPCSTR szPrg,     //[in] program name where symbol is used
                                                  LPCSTR szInstance,//[in] instance name (can be null)
                                                  LPCSTR szParent   //[in] parent name (can be null)
                                                ); //return true if edit has been open

BOOL      W5EDITRES_APICALL  W5RES_EditVarProps ( HWND hWndParent,  //[in] the window parent
                                                  LPCSTR szProject, //[in] project path
                                                  int nbVar,        //[in] size of arrVar buffer
                                                  DWORD arrVar[]    //[in] the var buffer to set
                                                ); //return true if edit has been open

BOOL      W5EDITRES_APICALL  W5RES_EditInitArray( HWND hWndParent,  //[in] the window parent
                                                  LPCSTR szProject, //[in] project path
                                                  int nbVar,        //[in] size of arrVar buffer
                                                  DWORD arrVar[]    //[in] the var buffer to set
                                                ); //return true if edit has been open

LPCSTR      W5EDITRES_APICALL W5RES_EditGraProps( HWND hWndParent,  //[in] the window parent
                                                  LPCSTR szProject, //[in] project path
                                                  LPCSTR szActType, //[in] graphic object type see X5ScriptsApi.h
                                                  LPCSTR szProperties, //[in] properties to display
                                                  BOOL bAutoDeclareSymbol //[in] true if should declare new variable
                                                  ); //return NULL if properties has not been changed and fill string if properties has changed
LPCSTR    W5EDITRES_APICALL W5RES_EditComment   ( HWND hWndParent,  //[in] the window parent
                                                  POINT ptPhys,     //[in] open coordinates
                                                  LPCSTR szProject, //[in] project path
                                                  LPCSTR szComment, //[in] symbol name
                                                  BOOL bRO          //[in] comment can not be modified
                                                ); //return NULL if comment has not been changed and fill string if comment has changed
BOOL    W5EDITRES_APICALL W5RES_EditStructure(HWND hWndParent,      //[in] the window parent
                                              POINT ptPhys,         //[in] open coordinates
                                              LPCSTR szProject,     //[in] project path
                                              LPCSTR szStructure,   //[in] the structure to edit
                                              BOOL bRO              //[in] if true structure can not be modified
                                              ); //return true if structure has been modified   
LPCSTR  W5EDITRES_APICALL W5RES_EditIterate(HWND hWndParent,        //[in] the window parent
                                            POINT ptPhys,           //[in] open coordinates
                                            int nbItem              //[in] number of items to iterate
                                            ); //return list of iterated values (separate by \n)
LPCSTR  W5EDITRES_APICALL W5RES_EditVarPrefix(HWND hWndParent,        //[in] the window parent
                                              POINT ptPhys,           //[in] open coordinates
                                              BOOL *pbDeclareDB,      //[out] declare variable in database when dialog close
                                              BOOL *pbOffset,         //[out] set offset when dialog close
                                              BOOL *pbRemoveOld,      //[out] remove old variable from configuration
                                              int iModule             //[in] select the current module (for preview) if -1 all modules are selected
                                              ); //return variable prefix
DWORD  W5EDITRES_APICALL W5RES_EditCreateFile(HWND hWndParent,      //[in] the window parent
                                              LPCSTR szProject,     //[in] project path
                                              DWORD dwFileType      //[in] type of the file to create in database
                                              ); //return file handle or 0 if creation has failed
LPCSTR  W5EDITRES_APICALL W5RES_EditInPlace(HWND hWndParent,       //[in] the window parent
                                            LPCSTR szText,         //[in] the text to edit
                                            LPCRECT rctDisplay     //[in] the rectangle where to display dialog (relative to hWndParent coordinates)
                                            ); //return new text or NULL if not edited
LPCSTR  W5EDITRES_APICALL W5RES_EditValue(HWND hWndParent,      //[in] the window parent
                                          POINT ptPhys,         //[in] open coordinates
                                          BOOL bRO,             //[in] if true value can not be modified
                                          int iLen,             //[in] var size in bytes (1/2/4)
                                          BOOL bSigned,         //[in] value can be signed
                                          int nbBitEnable,      //[in] nb of enable bits in binary display
                                          BOOL bReturnHexa,     //[in] if true, return hexadecimal value, if false return decimal value
                                          LPCSTR szValue        //[in] the value to set
                                          ); //return new value


//string translations
void      W5EDITRES_APICALL  W5RES_SetString    ( unsigned int iID, //[in] the string ID (see defines above)
                                                  LPCSTR szTrans    //[in] the translated string to set
                                                );
LPCSTR    W5EDITRES_APICALL  W5RES_LoadString   ( unsigned int iID  //[in] the string ID (see defines above)
                                                ); //return the found string, empty if not found and null if function can not be called
void      W5EDITRES_APICALL  W5RES_SetLangString( unsigned int iID, //[in] the string ID (see defines in W5EditTLApi. h and above)
                                                  LPCSTR szTrans,   //[in] the translated string to set
                                                  LPCSTR szLang     //[in] the language to set (one of LANGUAGE_XXX defines)
                                                );
LPCSTR    W5EDITRES_APICALL  W5RES_GetLangString( unsigned int iID, //[in] the string ID (see defines in W5EditTLApi. h and above)
                                                  LPCSTR szLang     //[in] the language to set (one of LANGUAGE_XXX defines)
                                                );
void      W5EDITRES_APICALL  W5RES_SelectLang   ( LPCSTR szLang     //[in] the language to use (one of LANGUAGE_XXX defines)
                                                );
LPCSTR    W5EDITRES_APICALL  W5RES_GetLang   (  void );             //get the current selected language (one of LANGUAGE_XXX defines)


//message boxes
int       W5EDITRES_APICALL  W5RES_MessageBox(HWND hWnd,
                                              LPCSTR szMessage, 
                                              LPCSTR szTitle, 
                                              UINT nType);
int       W5EDITRES_APICALL  W5RES_DisplayQuestion(HWND hWnd, 
                                                   LPCSTR szQuestion, 
                                                   LPCSTR szTitle);
int       W5EDITRES_APICALL  W5RES_DisplayQuestionAll(HWND hWnd, 
                                                   LPCSTR szQuestion, 
                                                   LPCSTR szTitle);
void      W5EDITRES_APICALL  W5RES_DisplayError(HWND hWnd, 
                                                LPCSTR szError, 
                                                LPCSTR szTitle);
void      W5EDITRES_APICALL  W5RES_DisplayRangeError(HWND hWnd, 
                                                     LPCSTR szTitle, 
                                                     int iMin, 
                                                     int iMax);
void      W5EDITRES_APICALL  W5RES_DisplayWarning(HWND hWnd,
                                                  LPCSTR szWarning, 
                                                  LPCSTR szTitle);
void      W5EDITRES_APICALL  W5RES_DisplayInformation(HWND hWnd, 
                                                      LPCSTR szMessage, 
                                                      LPCSTR szTitle);

#ifdef __cplusplus
}
#endif

#endif // !defined(_W5EDIRESAPI_H_)
