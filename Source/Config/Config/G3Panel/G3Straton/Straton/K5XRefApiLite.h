// K5 Cross-References - exports

// K5XREF Server API

#if !defined(_K5XREFAPILITE_H__INCLUDED_)
#define _K5XREFAPILITE_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef K5XREFDLL
	/* Build the DLL */
	#define K5XREF_APICALL __declspec(dllexport)
#else
	#define K5XREF_APICALL __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// flags - for Find in files

#define K5XREF_OUTPRGONLY       0x00000001  // not implemented
#define K5XREF_WHOLEWORD        0x00000002  // whole word
#define K5XREF_CASES            0x00000004  // case senseitive
#define K5XREF_QUIETFIND        0x00000008  // no project selection box
#define K5XREF_CLEAN            0x00000010  // reserved - don't use that

/////////////////////////////////////////////////////////////////////////////
// flags - for Replace in files

/////////////////////////////////////////////////////////////////////////////
// exports

#define K5XREF_UNUSEDVARS   "//unused//"
#define K5XREF_USEDLIBS     "//libs//"

int K5XREF_APICALL K5XRef_GetVersion	// get API version number
	(
	void
	);									// returns the API version number

int K5XREF_APICALL K5XRef_FindInFiles	// find a string in programs
	(
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szSearched,   				// searched string
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
	);                                  // return: number of occurrences found

int K5XREF_APICALL K5XRef_FindUnusedVars ( // list unused variables
	LPCSTR szProjectPath,				// pathname of the project
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
    );

int K5XREF_APICALL K5XRef_FindMultAssign ( // list multiple assignments of variables
	LPCSTR szProjectPath,				// pathname of the project
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
    );

int K5XREF_APICALL K5XRef_FindInProg	// find a string in 1 program
	(
	LPCSTR szProgramPath,				// pathname of the program
	LPCSTR szProgramName,				// name of the program
	LPCSTR szSearched,   				// searched string
    DWORD  dwLanguage,                  // language (K5DB convention)
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
	);                                  // return: number of occurrences found

int K5XREF_APICALL K5XRef_FindUsedLibs ( // list used library elements
	LPCSTR szProjectPath,				// pathname of the project
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
    );                                  // return: number of occurrences found

int K5XREF_APICALL K5XRef_ReplaceInFiles    // replace everywhere (with dialogs)
	(
	HWND   hwndParent,			        // handle of the parent window
	LPCSTR szProjectPath,				// pathname of the project
	LPCSTR szSearched,   				// proposed searched string or NULL
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
	);                                  // return: number of occurrences found

int K5XREF_APICALL K5XRef_AutoReplaceInFiles    // replace everywhere (AUTO)
    (
	HWND   hwndParent,			        // handle of the parent window
	LPCSTR szProjectPath,				// pathname of the project
    LPCSTR szOld,                       // searched name
    LPCSTR szNew,                       // replacement
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD  dwData                       // reserved - must be 0
    );

/////////////////////////////////////////////////////////////////////////////
// exports - multiple projects

int K5XREF_APICALL K5XRefMP_FindInFiles	// find a string in programs
	(
	LPCSTR szProjectList,				// project list (sep is '\n')
    LPCSTR szDefProject,                // default project or NULL
	LPCSTR szSearched,   				// searched string
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD_PTR dwData                    // reserved - must be 0
	);                                  // return: number of occurrences found

int K5XREF_APICALL K5XRefMP_FindUnusedVars ( // list unused variables
	LPCSTR szProjectList,				// project list (sep is '\n')
    LPCSTR szDefProject,                // default project or NULL
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD_PTR dwData                    // reserved - must be 0
    );

int K5XREF_APICALL K5XRefMP_FindMultAssign ( // list multiple assignments of variables
	LPCSTR szProjectList,				// project list (sep is '\n')
    LPCSTR szDefProject,                // default project or NULL
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD_PTR dwData                    // reserved - must be 0
    );

int K5XREF_APICALL K5XRefMP_FindUsedLibs ( // list used library elements
	LPCSTR szProjectList,				// project list (sep is '\n')
    LPCSTR szDefProject,                // default project or NULL
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD_PTR dwData                    // reserved - must be 0
    );                                  // return: number of occurrences found

int K5XREF_APICALL K5XRefMP_ReplaceInFiles    // replace everywhere (with dialogs)
	(
	HWND   hwndParent,			        // handle of the parent window
	LPCSTR szProjectList,				// project list (sep is '\n')
    LPCSTR szDefProject,                // default project or NULL
	LPCSTR szSearched,   				// proposed searched string or NULL
	HWND   hwndOutputListbox,			// handle of a LISTBOX output
	FILE * fOutputFile,					// handle of an output file
    DWORD  dwFlags,                     // reserved - must be 0
    DWORD_PTR dwData                       // reserved - must be 0
	);                                  // return: number of occurrences found

/////////////////////////////////////////////////////////////////////////////

DWORD_PTR K5XREF_APICALL K5XRV_Load     // create browser
    (
    LPCSTR szXML                        // XML path name
    ); // RETURN: handle of XRV browser or NULL

void K5XREF_APICALL K5XRV_Free          // delete crowser
    (
    DWORD_PTR hXRV                      // XRV browser
    );

LPCSTR K5XREF_APICALL K5XRV_GetRef      // get list of references
    (
    DWORD_PTR hXRV,                     // XRV browser
    LPCSTR szVar,                       // variable name ('/' allowed)
    LPCSTR szParent                     // parent program name for locals
    ); // RETURN: reference list ("\r\n" separated)

void K5XREF_APICALL K5XRV_Build         // build browse info
    (
    LPCSTR szProjectPath,               // project folder path
    LPCSTR szXML                        // output XML path name
    );

BOOL K5XREF_APICALL K5XRV_NeedBuild     // is build needed for an XML ?
    (
    LPCSTR szProjectPath,               // project folder path
    LPCSTR szXML                        // output XML path name
    );

DWORD_PTR K5XREF_APICALL K5XRV_BuildAndLoad ( // build and keep in memory
    LPCSTR szProjectPath,               // project folder path
    LPCSTR szXML                        // output XML path name
    ); // RETURN: handle of XRV browser or NULL

/////////////////////////////////////////////////////////////////////////////

typedef DWORD_PTR   (*PFK5XRV_Load)(LPCSTR);
typedef void        (*PFK5XRV_Free)(DWORD_PTR);
typedef LPCSTR      (*PFK5XRV_GetRef)(DWORD_PTR, LPCSTR, LPCSTR);
typedef void        (*PFK5XRV_Build)(LPCSTR, LPCSTR);
typedef BOOL        (*PFK5XRV_NeedBuild)(LPCSTR, LPCSTR);
typedef DWORD_PTR   (*PFK5XRV_BuildAndLoad)(LPCSTR, LPCSTR);

#define GETK5XRV_Load(h) \
    ((PFK5XRV_Load)GetProcAddress((h), "K5XRV_Load"))
#define GETK5XRV_Free(h) \
    ((PFK5XRV_Free)GetProcAddress((h), "K5XRV_Free"))
#define GETK5XRV_GetRef(h) \
    ((PFK5XRV_GetRef)GetProcAddress((h), "K5XRV_GetRef"))
#define GETK5XRV_Build(h) \
    ((PFK5XRV_Build)GetProcAddress((h), "K5XRV_Build"))
#define GETK5XRV_NeedBuild(h) \
    ((PFK5XRV_NeedBuild)GetProcAddress((h), "K5XRV_NeedBuild"))
#define GETK5XRV_BuildAndLoad(h) \
    ((PFK5XRV_BuildAndLoad)GetProcAddress((h), "K5XRV_BuildAndLoad"))

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_K5XREFAPILITE_H__INCLUDED_)
