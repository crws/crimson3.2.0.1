
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Ladder Diagram Editor Window
//

class CSequentialFlowChartEditorWnd : public CEditorWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CSequentialFlowChartEditorWnd(void);

	protected:
		// Data

		// Accelerator
		CAccelerator m_Accel;

		// Message Map
		AfxDeclareMessageMap();
	
		// Accelerators
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);

		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);

		// Notification Handlers

		// Implementation
		CStratonSFCWnd &GetEditor(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// Sequential Flow Chart Editor Window
//

// Dynamic Class

AfxImplementDynamicClass(CSequentialFlowChartEditorWnd, CEditorWnd);

// Constructor

CSequentialFlowChartEditorWnd::CSequentialFlowChartEditorWnd(void)
{
	m_pEdit = New CStratonSFCWnd;

	m_Accel.Create(L"SequentialFlowChartEditorMenu");
	}

// Message Map

AfxMessageMap(CSequentialFlowChartEditorWnd, CEditorWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)

	AfxDispatchGetInfoType(IDM_VIEW, OnViewGetInfo )
	AfxDispatchControlType(IDM_VIEW, OnViewControl )
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand )

	AfxDispatchGetInfoType(IDM_TOOL, OnToolGetInfo )
	AfxDispatchControlType(IDM_TOOL, OnToolControl )
	AfxDispatchCommandType(IDM_TOOL, OnToolCommand )

	AfxMessageEnd(CSequentialFlowChartEditorWnd)
	};

// Accelerators

BOOL CSequentialFlowChartEditorWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CSequentialFlowChartEditorWnd::OnPostCreate(void)
{
	CEditorWnd::OnPostCreate();
	}

// Command Handlers

BOOL CSequentialFlowChartEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonSFCWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CSequentialFlowChartEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	CStratonSFCWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CSequentialFlowChartEditorWnd::OnViewCommand(UINT uID)
{
	CStratonSFCWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CSequentialFlowChartEditorWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonSFCWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			break;
		}

	AfxTouch(Wnd);

	return FALSE;
	}

BOOL CSequentialFlowChartEditorWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	CStratonSFCWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_INIT_STEP:
		case IDM_TOOL_STEP:
		case IDM_TOOL_TRANSITION:
		case IDM_TOOL_JUMP:
		case IDM_TOOL_DIVERGENCE:
		case IDM_TOOL_DIVERGENCE_MAIN:
		case IDM_TOOL_CONVERGENCE:
		case IDM_TOOL_COMMENT:
			Src.EnableItem(Wnd.CanInsert());
			break;

		case IDM_TOOL_SET_TIMER:
			Src.EnableItem(Wnd.IsSelTrans());
			break;

		case IDM_TOOL_SWAP_STYLE:
			Src.EnableItem(Wnd.CanSwapStyle());
			break;

		case IDM_TOOL_EDIT_REF:
			Src.EnableItem(Wnd.CanEnterSelRef());
			break;

		case IDM_TOOL_RENUMBER:
			Src.EnableItem(Wnd.CanRenumber());
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CSequentialFlowChartEditorWnd::OnToolCommand(UINT uID)
{
	CStratonSFCWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_INIT_STEP:
			Wnd.InsertInitStep();
			break;

		case IDM_TOOL_STEP:
			Wnd.InsertStep();
			break;

		case IDM_TOOL_TRANSITION:
			Wnd.InsertTrans();
			break;

		case IDM_TOOL_SET_TIMER:
			Wnd.SetTimer();
			break;

		case IDM_TOOL_JUMP:
			Wnd.InsertJump();
			break;

		case IDM_TOOL_DIVERGENCE:
			Wnd.InsertDiv();
			break;

		case IDM_TOOL_DIVERGENCE_MAIN:
			Wnd.InsertMainDiv();
			break;

		case IDM_TOOL_CONVERGENCE:
			Wnd.InsertCnv();
			break;

		case IDM_TOOL_COMMENT:
			Wnd.InsertComment();
			break;

		case IDM_TOOL_SWAP_STYLE:
			Wnd.SwapStyle();
			Wnd.Invalidate(FALSE);
			break;

		case IDM_TOOL_EDIT_REF:
			Wnd.EnterSelRef();
			break;

		case IDM_TOOL_RENUMBER:
			Wnd.Renumber();
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Notification Handlers

// Implementation

CStratonSFCWnd & CSequentialFlowChartEditorWnd::GetEditor(void)
{	
	return (CStratonSFCWnd &) *m_pEdit;
	}

// End of File
