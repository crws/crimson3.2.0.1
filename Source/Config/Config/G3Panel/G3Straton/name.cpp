
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Naming Helper
//

// Contructor

CObjectNamer::CObjectNamer(DWORD dwProject)
{
	m_Disambig  = L"_";

	m_dwProject = dwProject;
	}

// Operation

void CObjectNamer::MakeUnique(CString &Name)
{
	if( CanFindName(CError(FALSE), Name) ) {

		UINT c = Name.GetLength();

		UINT d = m_Disambig.GetLength();

		UINT n = 0;

		while( isdigit(Name[c - 1 - n]) ) {

			n++;
			}

		if( Name.Mid(c - n - d, d) == m_Disambig ) {
			
			Name = Name.Left(c - n - d);
			}

		CString r = Name + m_Disambig;

		UINT    v = 0;
		
		for(;;) { 

			Name = r + CPrintf(L"%u", ++v);
			
			if( !CanFindName(CError(FALSE), Name) ) {

				break;
				}
			}		
		}
	}

// Overidable

BOOL CObjectNamer::CanFindName(CError &Error, CString Name)
{
	return FALSE;
	}

BOOL CObjectNamer::Validate(CError &Error, CString Name)
{
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Program Naming Helper
//

// Contructor

CProgramNamer::CProgramNamer(DWORD dwProject, DWORD dwLanguage, DWORD dwSection) : CObjectNamer(dwProject)
{
	m_dwLanguage = dwLanguage;

	m_dwSection  = dwSection;
	}

// Overidable

BOOL CProgramNamer::CanFindName(CError &Error, CString Name)
{
	if( !afxDatabase->CanCreateProgram( m_dwProject, 
					    m_dwLanguage, 
					    m_dwSection, 
					    m_dwParent, 
					    Name
					    ) ) {

		Error.Set(CString(IDS_PROGRAM_WITH_NAME));

		return TRUE;
		}

	return FALSE;
	}

BOOL CProgramNamer::Validate(CError &Error, CString Name)
{
	if( Name.IsEmpty() ) {

		Error.Set(CString(IDS_NAME_IS_EMPTY));
		
		return FALSE;
		}

	if( Name.GetLength() > 32 ) {

		Error.Set(CString(IDS_NAME_IS_TOO_LONG));

		return FALSE;
		}

	BOOL fUnder = FALSE;

	for( UINT n = 0; Name[n]; n++ ) {

		if( Name[n] == '_' ) {

			if( fUnder ) {
				
				Error.Set(CString(IDS_NAME_CONTAINS));

				return FALSE;
				}

			fUnder = TRUE;

			continue;
			}
		
		fUnder = FALSE;

		if( n ? !wisalnum(Name[n]) : !wisalpha(Name[n]) ) {

			Error.Set(CString(IDS_NAME_CONTAINS));

			return FALSE;
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Naming Helper
//

// Contructor

CVariableNamer::CVariableNamer(DWORD dwProject) : CObjectNamer(dwProject)
{
	m_dwGroup = 0;
	}

CVariableNamer::CVariableNamer(DWORD dwProject, DWORD dwGroup) : CObjectNamer(dwProject)
{
	m_dwGroup = dwGroup;
	}

// Overidable

BOOL CVariableNamer::CanFindName(CError &Error, CString Name)
{
	CLongArray Global;

	CStratonGroupHelper(m_dwProject).GetHandles(Global);
		
	if( Global.Find(m_dwGroup) < NOTHING ) {
		
		if( afxDatabase->FindVarExact( m_dwProject, 
					       Name, 
					       0, 
					       0) ) {

			Error.Set(CString(IDS_VARIABLE_WITH));

			return TRUE;
			}
		}
	else {
		if( afxDatabase->FindVarInGroup( m_dwProject, 
						 Global[0], 
						 Name
						 ) ) {

			Error.Set(CString(IDS_VARIABLE_WITH));

			return TRUE;
			}

		if( afxDatabase->FindVarInGroup( m_dwProject, 
						 Global[1], 
						 Name
						 ) ) {

			Error.Set(CString(IDS_VARIABLE_WITH));

			return TRUE;
			}

		if( afxDatabase->FindVarInGroup( m_dwProject, 
						 m_dwGroup, 
						 Name
						 ) ) {

			Error.Set(CString(IDS_VARIABLE_WITH));

			return TRUE;
			}		
		}

	return FALSE;
	}

BOOL CVariableNamer::Validate(CError &Error, CString Name)
{
	if( Name.IsEmpty() ) {

		Error.Set(CString(IDS_NAME_IS_EMPTY));
		
		return FALSE;
		}

	if( Name.GetLength() > 32 ) {

		Error.Set(CString(IDS_NAME_IS_TOO_LONG));

		return FALSE;
		}

	for( UINT n = 0; Name[n]; n++ ) {

		if( Name[n] == '_' ) {

			continue;
			}

		if( n ? !wisalnum(Name[n]) : !wisalpha(Name[n]) ) {

			Error.Set(CString(IDS_NAME_CONTAINS));

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
