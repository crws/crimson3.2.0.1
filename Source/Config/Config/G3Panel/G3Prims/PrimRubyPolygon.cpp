
#include "intern.hpp"

#include "PrimRubyPolygon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyGeom.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Polygon Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPolygon, CPrimRubyGeom);

// Constructor

CPrimRubyPolygon::CPrimRubyPolygon(void)
{
	m_nSides = 0;

	m_Rotate = 0;

	m_AltRad = +100;
	}

// Meta Data

void CPrimRubyPolygon::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_AddInteger(Rotate);
	}

// Path Management

void CPrimRubyPolygon::MakePaths(void)
{
	if( m_nSides ) {

		R2 Rect = m_DrawRect;

		m_pEdge->AdjustRect(Rect);

		CRubyPoint p1(Rect, 1);
		
		CRubyPoint p2(Rect, 4);

		number dd = 360.0 / m_nSides;

		number xMin = +1;
		number yMin = +1;
		number xMax = -1;
		number yMax = -1;

		int k1 = 0;

		for( number d1 = 0; d1 < 360; d1 += dd ) {

			number rf = (k1++ % 2) ? 1.0 : m_AltRad / 100.0;

			number xp = rf * CRubyTrig::Cos(m_Rotate * 0.1 + d1);
			
			number yp = rf * CRubyTrig::Sin(m_Rotate * 0.1 + d1);

			MakeMin(xMin, xp);
			MakeMin(yMin, yp);
			MakeMax(xMax, xp);
			MakeMax(yMax, yp);
			}

		number xr = (p2.m_x - p1.m_x) / (xMax - xMin);
		
		number yr = (p2.m_y - p1.m_y) / (yMax - yMin);

		number xc = p1.m_x - xMin * (p2.m_x - p1.m_x) / (xMax - xMin);
		
		number yc = p1.m_y - yMin * (p2.m_y - p1.m_y) / (yMax - yMin);

		int k2 = 0;

		for( number d2 = 0; d2 < 360; d2 += dd ) {

			number rf = (k2++ % 2) ? 1.0 : m_AltRad / 100.0;

			number xp = xc + rf * xr * CRubyTrig::Cos(m_Rotate * 0.1 + d2);
			
			number yp = yc + rf * yr * CRubyTrig::Sin(m_Rotate * 0.1 + d2);

			m_pathFill.Append(xp, yp);
			}

		m_pathFill.AppendHardBreak();
		}

	CPrimRubyGeom::MakePaths();
	}

// End of File
