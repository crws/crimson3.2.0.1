
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBevelButton_HPP
	
#define	INCLUDE_PrimRubyBevelButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBevelBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitive
//

class CPrimRubyBevelButton : public CPrimRubyBevelBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyBevelButton(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
