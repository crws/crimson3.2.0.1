
#include "intern.hpp"

#include "image.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImageSelectDialog, CStdDialog);
		
// Constructors

CImageSelectDialog::CImageSelectDialog(CUIPrimImage *pUI)
{
	m_pUI = pUI;

	SetName(L"ImageSelectDialog");
	}

// IUnknown

HRESULT CImageSelectDialog::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CImageSelectDialog::AddRef(void)
{
	return 1;
	}

ULONG CImageSelectDialog::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CImageSelectDialog::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( m_pUI->CanAcceptData(pData, *pEffect) ) {

		m_fDrop = TRUE;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	m_fDrop  = FALSE;

	return S_OK;
	}

HRESULT CImageSelectDialog::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = m_fDrop ? DROPEFFECT_LINK : DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CImageSelectDialog::DragLeave(void)
{
	m_DropHelp.DragLeave();

	return S_OK;
	}

HRESULT CImageSelectDialog::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_fDrop ) {

		if( m_pUI->AcceptData(pData) ) {

			EndDialog(TRUE);
			}
		}

	*pEffect = m_fDrop ? DROPEFFECT_LINK : DROPEFFECT_NONE;

	m_fDrop  = FALSE;
	
	return S_OK;
	}

// Message Map

AfxMessageMap(CImageSelectDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchCommand(IDOK, OnCommandOK)

	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CImageSelectDialog)
	};

// Message Handlers

BOOL CImageSelectDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_DropHelp.Bind(m_hWnd, this);

	GetDlgItem(100).SetFont(afxFont(Bolder));

	GetDlgItem(101).SetFont(afxFont(Bolder));
	
	GetDlgItem(102).SetFont(afxFont(Bolder));

	return TRUE;
	}
	
// Command Handlers

BOOL CImageSelectDialog::OnCommandOK(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CImageSelectDialog::OnPasteControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

BOOL CImageSelectDialog::OnPasteCommand(UINT uID)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		DWORD dwEffect;

		if( m_pUI->CanAcceptData(pData, dwEffect) ) {

			if( m_pUI->AcceptData(pData) ) {

				EndDialog(TRUE);
				}
			}

		pData->Release();
		}

	return TRUE;
	}

// End of File
