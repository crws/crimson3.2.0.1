
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPenEdge_HPP
	
#define	INCLUDE_PrimRubyPenEdge_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPenBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

class DLLAPI CPrimRubyPenEdge : public CPrimRubyPenBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPenEdge(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		int GetInnerWidth(void) const;
		int GetOuterWidth(void) const;

		// Operations
		void AdjustRect(R2 &r, UINT m);
		void AdjustRect(R2 &r);

		// Stroking
		BOOL StrokeTrim(CRubyPath &output, CRubyPath const &figure);

		// Drawing
		BOOL Trim(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_TrimWidth;
		UINT         m_TrimMode;
		CPrimColor * m_pTrimColor;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
