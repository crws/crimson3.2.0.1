
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGradButton_HPP
	
#define	INCLUDE_PrimRubyGradButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyPenEdge;

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graduated Button Primitive
//

class CPrimRubyGradButton : public CPrimRubyWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGradButton(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void GetRefs(CPrimRefList &Refs);
		void SetInitState(void);
		void FindTextRect(void);

		// Data Members
		CPrimColor       * m_pColor1;
		CPrimColor       * m_pColor2;
		CPrimRubyPenEdge * m_pEdge;
		CPoint             m_Corner;

	protected:
		// Data Members
		CRubyPath    m_pathFill;
		CRubyPath    m_pathEdge;
		CRubyPath    m_pathTrim;
		CRubyGdiList m_listFill;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listTrim;

		// Meta Data
		void AddMetaData(void);

		// Fast Fill Control
		BOOL UseFastFill(void);

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);
	};

// End of File

#endif
