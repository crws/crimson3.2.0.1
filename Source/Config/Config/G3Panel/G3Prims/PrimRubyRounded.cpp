
#include "intern.hpp"

#include "PrimRubyRounded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Rounded Rectangle Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyRounded, CPrimRubyTrimmed);

// Constructor

CPrimRubyRounded::CPrimRubyRounded(void)
{
	m_style = 1;
	}

// Meta Data

void CPrimRubyRounded::AddMetaData(void)
{
	CPrimRubyTrimmed::AddMetaData();

	Meta_SetName((IDS_ROUNDED_RECTANGLE_2));
	}

// End of File
