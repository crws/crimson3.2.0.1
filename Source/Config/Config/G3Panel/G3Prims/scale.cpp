
#include "intern.hpp"

#include "scale.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Vertical Scale
//

// Dynamic Class

AfxImplementDynamicClass(CPrimVertScale, CPrimRich);

// Constructor

CPrimVertScale::CPrimVertScale(void)
{
	m_uType       = 0x17;

	m_Precise     = 1;

	m_UseAll      = 0;

	m_ShowLabels  = 2;

	m_ShowUnits   = 1;

	m_Orient      = 0;

	m_pLineCol    = New CPrimColor;

	m_pTextCol    = New CPrimColor;

	m_pUnitCol    = New CPrimColor;

	m_TextFont    = fontHei16;

	m_UnitFont    = fontHei16Bold;

	m_FontBase    = 2;

	m_ShowMask    = propLimits | propFormat;
	}

// UI Creation

BOOL CPrimVertScale::OnLoadPages(CUIPageList *pList)
{
	LoadFirstPage(pList);

	CPrimRichPage *pPage = New CPrimRichPage(this, CString(IDS_FIGURE), 2);

	pList->Append(pPage);

	LoadRichPages(pList);
	
	return TRUE;
	}

// UI Update

void CPrimVertScale::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"ShowLabels" ) {

		DoEnables(pHost);
		}

	CPrimRich::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimVertScale::Draw(IGDI *pGDI, UINT uMode)
{
	pGDI->ResetAll();

	pGDI->SetBrushFore(m_pLineCol->GetColor());

	pGDI->SetTextFore (m_pTextCol->GetColor());

	pGDI->SetTextTrans(modeTransparent);

	SelectFont(pGDI, m_TextFont);

	////////

	int x1 = m_DrawRect.x1;
	
	int x2 = m_DrawRect.x2;
	
	int y1 = m_DrawRect.y1;
	
	int y2 = m_DrawRect.y2 - 1;

	int fy = pGDI->GetTextHeight(L"0");

	int fm = fy + 2;

	if( !m_UseAll ) {

		y1 += fy / 2;

		y2 -= fy / 2;
		}
	else
		fm += fy / 2;

	////////

	int    ySize    = y2 - y1;

	double Limit    = min(10, ySize / (fy + 4));

	double DataMin  = GetMinScale();

	double DataMax  = GetMaxScale();

	CScaleHelper Helper(DataMin, DataMax);

	Helper.CalcDecimalStep(Limit);

	int    StepSpan = Helper.GetStepSpan();

	int    Steps    = max(1, StepSpan);

	BOOL   fFlip    = m_Orient == 1;

	////////

	for( int n = 0; n <= Steps; n++ ) {

		double Value = Helper.GetStepValue(n);

		int    yPos  = -1;

		int    xTick = 12;

		if( !m_Precise && StepSpan ) {

			yPos = y2 - (ySize * n) / StepSpan;
			}
		else {
			if( n == 0 ) {

				Value = DataMin;

				yPos  = y2;
				}

			if( n == Steps ) {

				Value = DataMax;

				yPos  = y1;
				}

			if( yPos < 0 ) {

				double f = (Value - DataMin) / (DataMax - DataMin);

				yPos     = y2 - int(ySize * f);
				}
			}

		if( n == 1 ) {

			if( yPos > y2 - fm ) {

				if( !Value) {

					if( fFlip ) {
						
						pGDI->FillRect(x2 - xTick, yPos, x2, yPos + 1);
						}
					else
						pGDI->FillRect(x1, yPos, x1 + xTick, yPos + 1);
					}

				continue;
				}
			}

		if( n == Steps - 1 ) {

			if( yPos < y1 + fm ) {

				if( !Value ) {

					if( fFlip ) {
						
						pGDI->FillRect(x2 - xTick, yPos, x2, yPos + 1);
						}
					else
						pGDI->FillRect(x1, yPos, x1 + xTick, yPos + 1);
					}

				continue;
				}
			}

		UINT    Flags = (m_ShowUnits == 1) ? 0 : fmtBare;

		CString Label = FindValueText(R2I(C3REAL(Value)), typeReal, fmtPad | Flags);

		int     nAdj  = fy / 2 - m_FontBase;

		BOOL    fDraw = (m_ShowLabels > 1);

		if( n == 0 ) { 

			if( m_UseAll ) {
			
				nAdj = fy - m_FontBase * 2;
				}

			fDraw = (m_ShowLabels > 0);

			xTick = 18;
			}

		if( n == Steps ) {

			if( m_UseAll ) {
			
				nAdj = 0;
				}

			fDraw = (m_ShowLabels > 0);

			xTick = 18;
			}

		if( fDraw ) {

			if( fFlip ) {

				int cx = pGDI->GetTextWidth(Label);

				pGDI->TextOut(x2 - 24 - cx, yPos - nAdj, Label);
				}
			else
				pGDI->TextOut(x1 + 24, yPos - nAdj, Label);
			}

		if( fFlip ) {
			
			pGDI->FillRect(x2 - xTick, yPos, x2, yPos + 1);
			}
		else
			pGDI->FillRect(x1, yPos, x1 + xTick, yPos + 1);
		}

	if( m_ShowUnits == 2 ) {

		CString Units = FindValueText(R2I(0.0), typeReal, fmtUnits);

		UINT    uLen  = 0;

		Units.TrimBoth();

		if( (uLen = Units.GetLength()) ) {

			pGDI->SetTextFore(m_pUnitCol->GetColor());

			SelectFont(pGDI, m_UnitFont);

			int cy = pGDI->GetTextHeight(L"0") + 2;

			int xp = x2 - pGDI->GetTextWidth(L"W") / 2; 

			int yp = y1 + (y2 - y1 - uLen * cy) / 2;

			for( UINT n = 0; n < uLen; n++ ) {

				WCHAR a[2] = { Units[n], 0 };

				int cx = pGDI->GetTextWidth(a);

				pGDI->TextOut(xp - cx / 2, yp, a);

				yp += cy;
				}
			}
		}

	if( fFlip ) {
		
		pGDI->FillRect(x2 - 1, y1, x2, y2);
		}
	else
		pGDI->FillRect(x1, y1, x1 + 1, y2);
	}

void CPrimVertScale::SetInitState(void)
{
	CPrimRich::SetInitState();

	m_pLineCol->Set(GetRGB(31,31,31));

	m_pTextCol->Set(GetRGB(31,31,31));
	
	m_pUnitCol->Set(GetRGB(31,31,31));

	SetInitSize(49, 129);
	}

void CPrimVertScale::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_TextFont);

	GetFontRef(Refs, m_UnitFont);
	}

void CPrimVertScale::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_TextFont, uOld, uNew);

	EditFontRef(m_UnitFont, uOld, uNew);
	}

void CPrimVertScale::SetTextColor(COLOR Color)
{
	m_pLineCol->Set(Color);

	m_pTextCol->Set(Color);
	
	m_pUnitCol->Set(Color);
	}

// Download Support

BOOL CPrimVertScale::MakeInitData(CInitData &Init)
{
	CPrimRich::MakeInitData(Init);

	Init.AddByte(BYTE(m_Precise));
	Init.AddByte(BYTE(m_UseAll));
	Init.AddByte(BYTE(m_ShowLabels));
	Init.AddByte(BYTE(m_ShowUnits));
	Init.AddByte(BYTE(m_Orient));
	
	Init.AddItem(itemSimple, m_pLineCol);
	Init.AddItem(itemSimple, m_pTextCol);
	Init.AddItem(itemSimple, m_pUnitCol);
	
	Init.AddWord(WORD(m_TextFont));
	Init.AddWord(WORD(m_UnitFont));
	Init.AddByte(BYTE(m_FontBase));

	return TRUE;
	}

// Meta Data

void CPrimVertScale::AddMetaData(void)
{
	CPrimRich::AddMetaData();

	Meta_AddInteger(Precise);
	Meta_AddInteger(UseAll);
	Meta_AddInteger(ShowLabels);
	Meta_AddInteger(ShowUnits);
	Meta_AddInteger(Orient);
	Meta_AddObject (LineCol);
	Meta_AddObject (TextCol);
	Meta_AddObject (UnitCol);
	Meta_AddInteger(TextFont);
	Meta_AddInteger(UnitFont);
	Meta_AddInteger(FontBase);

	Meta_SetName((IDS_VERTICAL_SCALE));
	}

// Field Requirements

UINT CPrimVertScale::GetNeedMask(void) const
{
	return propLimits | propFormat;
	}

// Implementation

void CPrimVertScale::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("TextFont", m_ShowLabels > 0);

	pHost->EnableUI("UnitFont", m_ShowUnits == 2);

	pHost->EnableUI("TextCol",  m_ShowLabels > 0);

	pHost->EnableUI("UnitCol",  m_ShowUnits == 2);

	pHost->EnableUI("Orient",   m_ShowLabels > 0);
	}

C3REAL CPrimVertScale::GetMinScale(void)
{
	if( m_pValue && m_TagLimits ) {

		CTag * pTag = NULL;

		if( GetTagItem(pTag) ) {

			C3REAL v = I2R(pTag->GetMinValue(typeReal));

			if( pTag->GetDataType() == typeInteger ) {

				AdjustLimit(v);
				}

			return v;
			}
		}

	if( m_pLimitMin ) {

		C3REAL v = I2R(m_pLimitMin->Execute(typeReal));

		if( m_pLimitMin->GetType() == typeInteger ) {

			AdjustLimit(v);
			}
		
		return v;
		}
	
	return 0;
	}

C3REAL CPrimVertScale::GetMaxScale(void)
{
	if( m_pValue && m_TagLimits ) {

		CTag * pTag = NULL;

		if( GetTagItem(pTag) ) {

			C3REAL v = I2R(pTag->GetMaxValue(typeReal));

			if( pTag->GetDataType() == typeInteger ) {

				AdjustLimit(v);
				}

			return v;
			}
		}

	if( m_pLimitMax ) {

		C3REAL v = I2R(m_pLimitMax->Execute(typeReal));

		if( m_pLimitMax->GetType() == typeInteger ) {

			AdjustLimit(v);
			}
		
		return v;
		}
	
	return 100;
	}

BOOL CPrimVertScale::AdjustLimit(C3REAL &v)
{
	CDispFormat *pFormat;

	if( GetDataFormat(pFormat) ) {

		if( pFormat->IsKindOf(AfxRuntimeClass(CDispFormatNumber)) ) {

			CDispFormatNumber *pNum = (CDispFormatNumber *) pFormat;

			v = C3REAL(v / pow(double(10), int(pNum->m_After)));

			return TRUE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Scale Helper
//

// Constructor

CScaleHelper::CScaleHelper(double DataMin, double DataMax)
{
	m_DataMin  = DataMin;
	
	m_DataMax  = DataMax;

	m_DataSpan = fabs(DataMax - DataMin);
	}

// Operations

void CScaleHelper::CalcDecimalStep(double Limit)
{
	m_DrawStep = FindDrawStep(Limit);

	m_StepMin  = int(floor(m_DataMin / m_DrawStep));

	m_StepMax  = int(ceil (m_DataMax / m_DrawStep));
	}

void CScaleHelper::CalcTimeStep(double Limit)
{
	// TODO -- implement

	m_DrawStep = FindDrawStep(Limit);

	m_StepMin  = int(floor(m_DataMin / m_DrawStep));

	m_StepMax  = int(ceil (m_DataMax / m_DrawStep));
	}

// Attributes

double CScaleHelper::GetDrawStep(void)
{
	return m_DrawStep;
	}

double CScaleHelper::GetStepValue(int n)
{
	return (m_StepMin + n) * m_DrawStep;
	}

int CScaleHelper::GetStepCount(void)
{
	int StepSpan = m_StepMax - m_StepMin;

	return max(1, StepSpan);
	}

int CScaleHelper::GetStepSpan(void)
{
	return m_StepMax - m_StepMin;
	}

double CScaleHelper::FindDrawStep(double Limit)
{
	double Range = m_DataSpan;

	//////
	
	double Gap  = Range / Limit;

	double Mult = pow(10.0, floor(log10(Gap)));

	for( int p = 0; p < 2; p++ ) {

		double nList[] = { 1, 2, 5, 10 };

		for( int n = 0; n < 4; n++ ) {

			double Find = nList[n] * Mult;

			if( Gap <= Find ) {

				if( p == 1 ) {
					
					return Find;
					}
				
				if( Range / Find == floor(Range / Find) ) {

					return Find;
					}
				}
			}
		}

	AfxAssert(FALSE);

	return Range;
	}

// End of File
