
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Name Server
//

// Runtime Class

AfxImplementRuntimeClass(CUINameServer, CObject);

// Constructor

CUINameServer::CUINameServer(CUISystem *pSystem) : CNameServer(pSystem)
{
	m_pSystem = pSystem;

	m_pUI     = m_pSystem->m_pUI;

	m_pPages  = m_pUI->m_pPages;
}

// Initialization

void CUINameServer::LoadLibrary(void)
{
	CNameServer::LoadLibrary();

	UINT uGroup = m_pSystem->GetDatabase()->GetSoftwareGroup();

	if( uGroup >= SW_GROUP_3A ) {

		LoadFuncLib1();

		if( uGroup >= SW_GROUP_3B ) {

			LoadFuncLib2();
		}

		LoadIdentLib();
	}
}

// INameServer Methods

BOOL CUINameServer::FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( Type.m_Type == typePage ) {

		if( m_pIdent ) {

			if( m_pIdent->FindIdent(pError, Name, ID, Type) ) {

				return TRUE;
			}
		}

		if( FindPage(pError, Name, ID, Type) ) {

			return TRUE;
		}

		if( CNameServer::FindIdent(pError, Name, ID, Type) ) {

			return TRUE;
		}
	}
	else {
		if( CNameServer::FindIdent(pError, Name, ID, Type) ) {

			return TRUE;
		}

		if( FindPage(pError, Name, ID, Type) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUINameServer::NameIdent(WORD ID, CString &Name)
{
	if( CNameServer::NameIdent(ID, Name) ) {

		return TRUE;
	}

	if( NamePage(ID, Name) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CUINameServer::FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type)
{
	if( !CNameServer::FindProp(pError, Name, ID, PropID, Type) ) {

		CDataRef &Ref = (CDataRef &) ID;

		if( !Ref.t.m_IsTag ) {

			CItem *pItem = m_pSystem->GetDatabase()->GetHandleItem(Ref.t.m_Index);

			if( pItem ) {

				if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

					return FindPageProp(Name, PropID, Type);
				}
			}
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CUINameServer::NameProp(WORD ID, WORD PropID, CString &Name)
{
	if( !CNameServer::NameProp(ID, PropID, Name) ) {

		CDataRef &Ref = (CDataRef &) ID;

		if( !Ref.t.m_IsTag ) {

			CItem *pItem = m_pSystem->GetDatabase()->GetHandleItem(Ref.t.m_Index);

			if( pItem ) {

				if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

					return NamePageProp(PropID, Name);
				}
			}
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CUINameServer::FindClass(CError *pError, CString Name, UINT &Type)
{
	if( Name == L"CPage" ) {

		Type = typePage;

		return TRUE;
	}

	return CNameServer::FindClass(pError, Name, Type);
}

BOOL CUINameServer::NameClass(UINT Type, CString &Name)
{
	switch( Type ) {

		case typePage:

			Name = CString(IDS_DISPLAY_PAGE);

			return TRUE;
	}

	return CNameServer::NameClass(Type, Name);
}

BOOL CUINameServer::SaveClass(UINT Type)
{
	return CNameServer::SaveClass(Type);
}

// Standard Page Properties

PCTXT CUINameServer::m_pPageProps[] = {

	L"Name",  //  1
	L"Label", //  2
	L"Desc",  //  3

};

// Standard Page Properties

BOOL CUINameServer::FindPageProp(CString Name, WORD &PropID, CTypeDef &Type)
{
	for( UINT n = 1; n <= elements(m_pPageProps); n++ ) {

		if( Name == m_pPageProps[n-1] ) {

			if( n >= 1 && n <= 3 ) {

				Type.m_Type  = typeString;

				Type.m_Flags = 0;
			}

			PropID = WORD(n);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUINameServer::NamePageProp(WORD PropID, CString &Name)
{
	if( --PropID < elements(m_pPageProps) ) {

		Name = m_pPageProps[PropID];

		return TRUE;
	}

	return FALSE;
}

// Initialization

void CUINameServer::LoadFuncLib1(void)
{
	CError Error(FALSE);

	AddFunc(Error, CString(IDS_PAGE), 0x2000, L"void GotoPage(class CPage Page)");
	AddFunc(Error, CString(IDS_UI), 0x2001, L"void SetLanguage(int Lang)");
	AddFunc(Error, CString(IDS_UI), 0x2002, L"void Beep(int Freq, int Time)");
	AddFunc(Error, CString(IDS_UI), 0x2003, L"void PlayRTTTL(cstring Tune)");
	AddFunc(Error, CString(IDS_UI), 0x2031, L"void MuteSiren(void)");
	AddFunc(Error, CString(IDS_UI), 0x2032, L"void SirenOn(void)");
	AddFunc(Error, CString(IDS_PAGE), 0x2042, L"void ShowPopup(class CPage Page)");
	AddFunc(Error, CString(IDS_PAGE), 0x2043, L"void HidePopup(void)");
	AddFunc(Error, CString(IDS_UI), 0x204E, L"void DispOn(void)");
	AddFunc(Error, CString(IDS_UI), 0x204F, L"void DispOff(void)");
	AddFunc(Error, CString(IDS_PAGE), 0x2052, L"void GotoPrevious(void)");
	AddFunc(Error, CString(IDS_PAGE), 0x2055, L"void ShowMenu(class CPage Page)");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x2060, L"int GetUpDownData(int Count, int Range) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x2061, L"int GetUpDownStep(int Count, int Range) const");
	AddFunc(Error, CString(IDS_SECURITY), 0x2089, L"void UserLogOn(void)");
	AddFunc(Error, CString(IDS_SECURITY), 0x208A, L"void UserLogOff(void)");
	AddFunc(Error, CString(IDS_SECURITY), 0x208B, L"int  TestAccess(int Rights, cstring Prompt) const");
	AddFunc(Error, CString(IDS_UI), 0x2092, L"void PostKey(int Code, int Transition)");
	AddFunc(Error, CString(IDS_SECURITY), 0x20AA, L"int HasAccess(int Rights) const");
	AddFunc(Error, CString(IDS_UI), 0x20BC, L"int Flash(int Freq) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20BD, L"int ColFlash(int Freq, int Col1, int Col2) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20BE, L"int ColPick2(int Data, int Col1, int Col2) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20BF, L"int ColBlend(float Data, float Min, float Max, int Col2, int Col2) const");
	AddFunc(Error, CString(IDS_PAGE), 0x20C0, L"void GotoNext(void)");
	AddFunc(Error, CString(IDS_PAGE), 0x20C1, L"int CanGotoPrevious(void) const");
	AddFunc(Error, CString(IDS_PAGE), 0x20C2, L"int CanGotoNext(void) const");
	AddFunc(Error, CString(IDS_UI), 0x20CB, L"int PrintScreenToFile(cstring Path, cstring Name, int Comp)");
	AddFunc(Error, CString(IDS_COLOR), 0x20CC, L"int ColGetRed(int rgb) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20CD, L"int ColGetGreen(int rgb) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20CE, L"int ColGetBlue(int rgb) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20CF, L"int ColGetRGB(int r, int g, int b) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20D5, L"int ColPick4(int Data1, int Data2, int Col1, int Col2, int Col3, int Col4) const");
	AddFunc(Error, CString(IDS_COLOR), 0x20D6, L"int ColSelFlash(int Enable, int Freq, int Col1, int Col2, int Col3) const");
	AddFunc(Error, CString(IDS_UI), 0x20DA, L"int GetLanguage(void) const");
	AddFunc(Error, CString(IDS_PAGE), 0x20DC, L"void ShowNested(class CPage Page)");
	AddFunc(Error, CString(IDS_PAGE), 0x20DD, L"void HideAllPopups(void)");
	AddFunc(Error, CString(IDS_PAGE), 0x20DE, L"int ShowModal(class CPage Page)");
	AddFunc(Error, CString(IDS_PAGE), 0x20DF, L"void EndModal(int Code)");
	AddFunc(Error, CString(IDS_SECURITY), 0x20E4, L"int SaveSecurityDatabase(int nMode, cstring File)");
	AddFunc(Error, CString(IDS_SECURITY), 0x20E5, L"int LoadSecurityDatabase(int nMode, cstring File)");
	AddFunc(Error, CString(IDS_SECURITY), 0x20E6, L"cstring GetCurrentUserName(void) const");
	AddFunc(Error, CString(IDS_SECURITY), 0x20E7, L"cstring GetCurrentUserRealName(void) const");
	AddFunc(Error, CString(IDS_SECURITY), 0x20E8, L"int GetCurrentUserRights(void) const");
	AddFunc(Error, CString(IDS_SECURITY), 0x20E9, L"int HasAllAccess(int Rights) const");
}

void CUINameServer::LoadFuncLib2(void)
{
}

void CUINameServer::LoadIdentLib(void)
{
	CError Error(FALSE);

	m_pIdentLib->AddIdent(Error, this, 0x7C00, L"int DispBrightness");
	m_pIdentLib->AddIdent(Error, this, 0x7C01, L"int DispContrast");
	m_pIdentLib->AddIdent(Error, this, 0x7C03, L"const int DispUpdates");
	m_pIdentLib->AddIdent(Error, this, 0x7C04, L"const int DispCount");
	m_pIdentLib->AddIdent(Error, this, 0x7C06, L"const int IsSirenOn");
	m_pIdentLib->AddIdent(Error, this, 0x7C0B, L"const int IsPressed");
	m_pIdentLib->AddIdent(Error, this, 0x7C0E, L"int IconBrightness");
}

// Implementation

BOOL CUINameServer::FindPage(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( Type.m_Type == typePage || Type.m_Type < typeObject ) {

		CDataRef &Ref   = (CDataRef &) ID;

		CItem    *pItem = m_pPages->FindByName(Name);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

				CDispPage *pPage = (CDispPage *) pItem;

				Ref.t.m_IsTag = 0;

				Ref.t.m_Index = pPage->m_Handle;

				Type.m_Type   = typePage;

				Type.m_Flags  = flagConstant;

				return TRUE;
			}
		}

		if( Type.m_Type == typePage ) {

			if( pError && pError->AllowUI() ) {

				if( !pError->GetFlag(L"AutoCreate") ) {

					CItemCreateDialog Dlg(Name, typePage);

					switch( Dlg.Execute() ) {

						case 0:
							pError->SetFlag(L"Shown");

							return FALSE;

						case 2:
							pError->SetFlag(L"AutoCreate");

							break;
					}
				}

				if( m_pSystem->m_pUI->CreatePage(Name) ) {

					if( FindPage(NULL, Name, ID, Type) ) {

						return TRUE;
					}
				}
				else {
					CWnd &Wnd = CWnd::GetActiveWindow();

					Wnd.Error(CString(IDS_PAGE_COULD_NOT_BE));

					pError->SetFlag(L"Shown");
				}
			}
		}
	}

	return FALSE;
}

BOOL CUINameServer::NamePage(WORD ID, CString &Name)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.t.m_IsTag ) {

		UINT uData = Ref.t.m_Index;

		if( !(uData & 0x4000) ) {

			UINT   hItem = (uData & 0x3FFF);

			CItem *pItem = m_pSystem->GetDatabase()->GetHandleItem(hItem);

			if( pItem ) {

				if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

					CDispPage *pPage = (CDispPage *) pItem;

					Name = pPage->m_Name;

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// End of File
