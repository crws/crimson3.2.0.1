
#include "intern.hpp"

#include "PrimRubyFilleted.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Filleted Rectangle Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyFilleted, CPrimRubyTrimmed);

// Constructor

CPrimRubyFilleted::CPrimRubyFilleted(void)
{
	m_style = 2;
	}

// Meta Data

void CPrimRubyFilleted::AddMetaData(void)
{
	CPrimRubyTrimmed::AddMetaData();

	Meta_SetName((IDS_FILLETED));
	}

// End of File
