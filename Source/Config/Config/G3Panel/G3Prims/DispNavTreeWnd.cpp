
#include "intern.hpp"

#include "DispNavTreeWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispCatWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Page Navigation Window
//

// Base Class

#define CBaseClass CNavTreeWnd

// Dynamic Class

AfxImplementDynamicClass(CDispNavTreeWnd, CBaseClass);
		
// Constructor

CDispNavTreeWnd::CDispNavTreeWnd(void) : CBaseClass( L"Pages",
						      AfxRuntimeClass(CDispFolder),
						      AfxRuntimeClass(CDispPage)
						      )
{
	FindCatView();
	}

// Overridables

void CDispNavTreeWnd::OnAttach(void)
{
	CBaseClass::OnAttach();

	m_pSystem  = (CUISystem  *) m_pItem->GetParent();

	m_pManager = (CUIManager *) m_pItem;

	m_pPages    = m_pManager->m_pPages;
	}

BOOL CDispNavTreeWnd::OnNavigate(CString const &Nav)
{
	if( Nav.StartsWith(m_pManager->m_pScratch->GetFixedPath()) ) {

		if( !m_pSelect->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CBaseClass::OnNavigate(m_pPages->GetItem(0U)->GetFixedPath());
			}

		m_pCatView->NavToScratch();

		return TRUE;
		}

	return CBaseClass::OnNavigate(Nav);
	}

// Message Map

AfxMessageMap(CDispNavTreeWnd, CBaseClass)
{
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	
	AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)

	AfxMessageEnd(CDispNavTreeWnd)
	};

// Message Handlers

void CDispNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"DispNavTreeTool"));
		}
	}

UINT CDispNavTreeWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	m_pCatView->NavTreeActive(GetWindowRect());

	return CBaseClass::OnMouseActivate(Parent, uHitTest, uMessage);
	}

// Command Handlers

BOOL CDispNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Info.m_Image   = 0x4000000F;

			Info.m_ToolTip = CString(IDS_NEW_PAGE);

			Info.m_Prompt  = CString(IDS_ADD_NEW_DISPLAY);

			return TRUE;

		case IDM_ITEM_NEW_FOLDER:

			Info.m_Image = MAKELONG(0x0006, 0x4000);

			return FALSE;

		case IDM_ITEM_WATCH:

			Info.m_Image = MAKELONG(0x0036, 0x1000);

			return FALSE;
		}

	return FALSE;
	}

BOOL CDispNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_ITEM_NEW || uID == IDM_ITEM_NEW_NAMED ) {

		Src.EnableItem(!IsItemLocked(m_hSelect));

		return TRUE;
		}

	if( uID == IDM_ITEM_USAGE ) {

		Src.EnableItem(m_pSelect->IsKindOf(m_Class));

		return TRUE;
		}

	if( uID == IDM_ITEM_WATCH ) {

		Src.EnableItem(m_pSelect->IsKindOf(m_Class));

		return TRUE;
		}

	return FALSE;
	}

BOOL CDispNavTreeWnd::OnItemCommand(UINT uID)
{
	if( uID == IDM_ITEM_NEW || uID == IDM_ITEM_NEW_NAMED ) {

		CString Root = GetRoot(TRUE);

		CString Base = L"Page";

		CString Name = L"";

		if( uID == IDM_ITEM_NEW_NAMED ) {

			LPARAM lParam = m_pMsg->lParam;

			PCTXT  pText  = PCTXT(lParam);

			Root = GetRoot(pText, TRUE);

			Name = CString(pText).Mid(Root.GetLength());

			if( !m_MapNames[Root + Name] ) {

				CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

				m_System.ExecCmd(pCmd);

				return TRUE;
				}

			Base = Name;

			for(;;) {

				UINT uLen = Base.GetLength();

				if( !isdigit(Base[uLen-1]) ) {

					break;
					}

				Base = Base.Left(uLen - 1);
				}
			}

		for( UINT n = 1;; n++ ) {

			Name.Printf(L"%s%u", Base, n);

			if( !m_MapNames[Root + Name] ) {

				CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

				m_System.ExecCmd(pCmd);

				break;
				}
			}


		return TRUE;
		}

	if( uID == IDM_ITEM_USAGE ) {

		CDispPage *pItem = (CDispPage *) m_pSelect;

		m_pSystem->FindHandleUsage( CString(IDS_PAGE),
					    pItem->GetName(),
					    pItem->m_Handle
					    );

		return TRUE;
		}

	if( uID == IDM_ITEM_WATCH ) {

		CDispPage *pItem = (CDispPage *) m_pSelect;

		m_pSystem->ClearWatch();

		m_pSystem->AddToWatch(pItem);

		m_pSystem->UpdateWatch(TRUE);

		return TRUE;
		}

	return FALSE;
	}

// Data Object Construction

BOOL CDispNavTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	CDataObject *pMake = New CDataObject;

	if( m_pNamed ) {

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			if( AddItemToStream(Stream, m_hSelect, fRich) ) {
	
				pMake->AddStream(m_cfData, Stream);
				}
			}
		}

	if( fRich ) {

		if( m_pNamed ) {

			pMake->AddText(m_pNamed->GetName());
			}
		}

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CDispNavTreeWnd::AddItemToStream(ITextStream &Stream, HTREEITEM hItem, BOOL fRich)
{
	CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

	CString F2   = m_pList->GetFixedPath();

	CString F3   = CPrintf(L"%8.8X", m_hWnd);

	CString Head = CPrintf(L"%s|%s|%s\r\n", F1, F2, F3);

	Stream.PutLine(Head);

	CTreeFile Tree;

	if( Tree.OpenSave(Stream) ) {

		CPrimRefList Refs;

		AddItemToTreeFile(Tree, Refs, hItem);

		if( fRich ) {

			m_pSystem->SaveRefs(Tree, Refs);
			}

		Tree.Close();

		return TRUE;
		}

	return FALSE;
	}

void CDispNavTreeWnd::AddItemToTreeFile(CTreeFile &Tree, CPrimRefList &Refs, HTREEITEM hItem)
{
	CItem * pItem = GetItemPtr(hItem);

	if( !(IsPrivate() && pItem->GetPrivate()) ) {

		CString Class = pItem->GetClassName();

		Tree.PutObject(Class.Mid(1));

		Tree.PutValue (L"NDX", pItem->GetIndex());

		pItem->PreCopy();

		pItem->Save(Tree);

		if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CDispPage *pPage = (CDispPage *) pItem;

			pPage->GetRefs(Refs);
			}

		Tree.EndObject();

		if( (hItem = m_pTree->GetChild(hItem)) ) {

			while( hItem ) {

				AddItemToTreeFile(Tree, Refs, hItem);

				hItem = m_pTree->GetNext(hItem);
				}
			}
		}
	}

// Data Object Hooks

void CDispNavTreeWnd::OnAcceptInit(CTreeFile &Tree)
{
	m_Refs.Empty();
	}

BOOL CDispNavTreeWnd::OnAcceptName(CTreeFile &Tree, BOOL fLocal, CString Name)
{
	if( m_pSystem->ReadRef(Tree, Name, m_Refs, !fLocal, FALSE) ) {

		return FALSE;
		}

	return TRUE;
	}

void CDispNavTreeWnd::OnAcceptDone(CTreeFile &Tree, HTREEITEM hItem)
{
	if( m_Refs.GetCount() ) {

		for( UINT p = 0; p < 2; p++ ) {

			FixRefsFrom(hItem, p);
			}
		}
	}

// Reference Helpers

void CDispNavTreeWnd::FixRefsFrom(HTREEITEM hItem, UINT uPass)
{
	CItem *pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

		CDispPage *pPage = (CDispPage *) pItem;

		m_pSystem->FixRefs(pPage, m_Refs, uPass);
		}

	if( (hItem = m_pTree->GetChild(hItem)) ) {

		while( hItem ) {

			FixRefsFrom(hItem, uPass);

			hItem = m_pTree->GetNext(hItem);
			}
		}
	}

// Tree Loading

void CDispNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CommsTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

void CDispNavTreeWnd::NewItemSelected(void)
{
	CBaseClass::NewItemSelected();

	m_pCatView->PageSelected(m_pSelect->IsKindOf(AfxRuntimeClass(CDispPage)));

	m_pCatView->NavTreeActive(GetWindowRect());
	}

UINT CDispNavTreeWnd::GetRootImage(void)
{
	return 0x39;
	}

UINT CDispNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		return 0x38;
		}

	if( pItem->IsKindOf(m_Folder) ) {

		return 0x22;
		}

	return 0;
	}

BOOL CDispNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"DispNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"DispNavTreeMissCtxMenu";

	return FALSE;
	}

void CDispNavTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
	if( pItem ) {

		if( pItem->IsKindOf(m_Class) ) {

			CDispPage * pPage = (CDispPage *) pItem;

			UINT        uObj  = pPage->m_Handle;

			m_pSystem->ObjCheck(uObj, FALSE);
			}
		}
	}

void CDispNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		CDispPage * pPage = (CDispPage *) pItem;

		m_pSystem->ObjCheck(pPage->m_Handle, TRUE);
		}

	CBaseClass::OnItemRenamed(pItem);
	}

// Implementation

void CDispNavTreeWnd::FindCatView(void)
{
	CSysProxy Proxy;

	Proxy.Bind();

	m_pCatView = (CDispCatWnd *) Proxy.GetCatView(AfxRuntimeClass(CDispCatWnd));
	}

// End of File
