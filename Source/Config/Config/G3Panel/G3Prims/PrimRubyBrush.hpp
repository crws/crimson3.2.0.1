
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyBrush_HPP
	
#define	INCLUDE_RubyBrush_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Brush
//

class DLLAPI CPrimRubyBrush : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyBrush(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL IsNull(void) const;

		// Operations
		void Set(COLOR Color);
		BOOL Register(UINT uMode);
		void GetRefs(CPrimRefList &Refs);

		// Drawing
		BOOL Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Pattern;
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
