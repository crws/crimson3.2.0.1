
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Primitive
//

CPrim_Schema RCDATA
BEGIN
	L"Visible,Visible,,Coded/Expression,|true,"
	L"Enter an expression to show or hide this primitive."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Primitive with Text
//

CPrimWithText_Schema RCDATA
BEGIN
	L"DataItem,Data,,,,\0"
	L"TextItem,Text,,,,\0"
	L"Action,Action,,,,\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Simple Image
//

CPrimSimpleImage_Schema RCDATA
BEGIN
	L"Keep,Maintain Aspect,,Enum/DropDown,No|Yes,"
	L"Indicate whether to constrain the image size so "
	L"as to maintain the aspect ratio of the source."
	L"\0"
	
	L"Color,Use Colors,,Coded/Expression,|true,"
	L"Enter an expression to control image coloring. An empty expression "
	L"or a value of true will result in a color imaged being drawn, while "
	L"a value of false will result in the image being rendered in "
	L"monochrome as might be used with a disabled button."
	L"\0"

	L"Image,Image,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"\0"
END

CPrimSimpleImage_Page1 RCDATA
BEGIN
	L"G:1,root,Image Options,Color,Keep\0"
	L"G:1,root,Image Selection,Image\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Set
//

CPrimSet_Pages RCDATA
BEGIN
	L"Locking\0"
END

CPrimSet_Schema RCDATA
BEGIN
	L"LockList,Lock Mode,,Enum/RadioCol,Positions and Properties Editable|Positions Locked and Properties Editable|Positions and Properties Locked,"
	L"Select the aspects of the group's contents that are to be locked."
	L"\0"

	L"\0"
END

CPrimSet_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,LockList\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- 2D Movement
//

CPrimMove2D_Pages RCDATA
BEGIN
	L"Movement,Locking\0"
END

CPrimMove2D_Schema RCDATA
BEGIN
	L"PosX,Position,,Coded/Expression,|0,"
	L"Enter the value used to define the contents' horizontal position."
	L"\0"

	L"MinX,Minimum,,Coded/Expression,|0,"
	L"Enter the value that will place the content at the left edge."
	L"\0"

	L"MaxX,Maximum,,Coded/Expression,|100,"
	L"Enter the value that will place the content at the right edge."
	L"\0"

	L"PosY,Position,,Coded/Expression,|0,"
	L"Enter the value used to define the contents' vertical position."
	L"\0"

	L"MinY,Minimum,,Coded/Expression,|0,"
	L"Enter the value that will place the content at the top edge."
	L"\0"

	L"MaxY,Maximum,,Coded/Expression,|100,"
	L"Enter the value that will place the content at the bottom edge."
	L"\0"

	L"\0"
END

CPrimMove2D_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Horizontal,PosX,MinX,MaxX\0"
	L"G:1,root,Vertical,PosY,MinY,MaxY\0"
	L"\0"
END

CPrimMove2D_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,LockList\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Polar Movement
//

CPrimMovePolar_Pages RCDATA
BEGIN
	L"Movement,Locking\0"
END

CPrimMovePolar_Schema RCDATA
BEGIN
	L"PosT,Position,,Coded/Expression,|0,"
	L"Enter the value used to define the contents' angular position."
	L"\0"

	L"MinT,Minimum,,Coded/Expression,|0,"
	L"Enter the value that will result in a position of 0 degrees."
	L"\0"

	L"MaxT,Maximum,,Coded/Expression,|360,"
	L"Enter the value that will result in a position of 360 degress."
	L"\0"

	L"PosR,Position,,Coded/Expression,|0,"
	L"Enter the value used to define the contents' radial position."
	L"\0"

	L"MinR,Minimum,,Coded/Expression,|0,"
	L"Enter the value that will place the contents at 0% radius."
	L"\0"

	L"MaxR,Maximum,,Coded/Expression,|100,"
	L"Enter the value that will place the contents at 100% radius."
	L"\0"

	L"\0"
END

CPrimMovePolar_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Angle,PosT,MinT,MaxT\0"
	L"G:1,root,Radius,PosR,MinR,MaxR\0"
	L"\0"
END

CPrimMovePolar_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,LockList\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Widget
//

CPrimWidget_Schema RCDATA
BEGIN
	L"LockData,Lock Mode,,Enum/RadioCol,Definitions and Values Editable|Definitions Locked and Values Editable|Definitions and Values Locked,"
	L"Select the aspects of the Widget's data items that are to be locked."
	L"\0"

	L"Desc,Description,,String/EditBox,20,"
	L"Enter a description for this Widget. This will be shown in the Resource Pane."
	L"\0"

	L"Cat,Category,,String/EditBox,20,"
	L"Enter a category for this Widget. This will be used to group "
	L"Widgets within the Resource Pane, thereby allowing you to organize "
	L"your Widgets into different types."
	L"\0"

	L"\0"
END

CPrimWidget_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Filing,Desc,Cat\0"
	L"\0"
END

CPrimWidget_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,LockList\0"
	L"G:1,root,Data,LockData\0"
	L"\0"
END

CPrimWidget_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

CPrimWidget_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Actions,Actions,OnSelect,OnRemove,OnSecond,OnUpdate\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Widget Actions
//

CPrimWidgetActions_Schema RCDATA
BEGIN
	L"OnSelect,On Select,,Coded/Action,|None,"
	L"Define an action to be executed when this widget is first displayed."
	L"\0"

	L"OnRemove,On Remove,,Coded/Action,|None,"
	L"Define an action to be executed when this widget is removed from display."
	L"\0"

	L"OnUpdate,On Update,,Coded/Action,|None,"
	L"Define an action to be execute each time the widget is updated."
	L"\0"

	L"OnSecond,On Tick,,Coded/Action,|None,"
	L"Define an action to be execute each second when the widget is displayed."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Widget Property Data
//

CPrimWidgetPropData_Schema RCDATA
BEGIN
	L"Count,Count,,Integer/Pick,0|0||0|80,"
	L"Select the number of data items used by this widget."
	L"\0"

	L"AutoBind,Folder Binding,,Enum/DropDown,Disabled|Enabled,"
	L"Enable or disable folder binding."
	L"\0"

	L"AutoZoom,Details Creation,,Enum/DropDown,Disabled|Enabled,"
	L"Enable or disable details page creation."
	L"\0"

	L"Class,Required Folder Class,,String/EditBox,30,"
	L"Enter the name of the folder class that will be use "
	L"when checking whether a given folder can be bound to "
	L"this Widget."
	L"\0"

	L"SubItem,Binding Prefix,,String/EditBox,20,"
	L"Enter the prefix to be applied to each "
	L"data item during the folder binding process."
	L"\0"

	L"Zoom,Details Widget,,String/EditBox,320||40,"
	L"Enter a list of widget files that will be used "
	L"to create details pages."
	L"\0"

	L"\0"
END

CPrimWidgetPropData_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Extent,Count\0"
	L"\0"
END

CPrimWidgetPropData_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Binding,AutoBind,Class,SubItem\0"
	L"G:1,root,Details,AutoZoom,Zoom\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Widget Property
//

CPrimWidgetProp_Schema RCDATA
BEGIN
	L"Name,Name,,String/EditBox,32||12,"
	L"Enter the name of the data item."
	L"\0"

	L"Bind,Bind To,,String/EditBox,96|Per Name|12,"
	L"Enter the name of the tag to which the data item will be bound."
	L"\0"

	L"Desc,Description,,String/EditBox,32||18,"
	L"Enter the description of the data item."
	L"\0"

	L"Type,Data Type,,Enum/DropDown,Integer|Real|String|Color|Page|Action,"
	L"Select the required data type for the data item."
	L"\0"

	L"Flags,Type Flags,,Bitmask/Pick,1|18|Tag|Writable|Array|Element|No Bind|No Show,"
	L"Define one or more flags to modify the data item type."
	L"\0"

	L"Value,Value,,Coded/Expression,,"
	L"Enter the value for this data item."
	L"\0"

	L"\0"
END

// End of File
