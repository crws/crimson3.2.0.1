
#include "intern.hpp"

#include "resview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Resource Item
//

// Dynamic Class

AfxImplementDynamicClass(CGraphicsResourceItem, CUISystem);

// Constructor

CGraphicsResourceItem::CGraphicsResourceItem(void) : CUISystem(TRUE)
{
	m_pPrims   = New CPrimResourceItem;

	m_pSymbols = New CSymbolResourceItem;

	m_pColors  = New CColorResourceItem;
	}

// Attributes

UINT CGraphicsResourceItem::GetSoftwareGroup(void) const
{
	return 999;
}

// Persistance

void CGraphicsResourceItem::Init(void)
{
	CUISystem::Init();

	m_pPrims->LoadWidgets();
	}

// Meta Data

void CGraphicsResourceItem::AddMetaData(void)
{
	CUISystem::AddMetaData();

	Meta_AddObject(Prims);
	Meta_AddObject(Symbols);
	Meta_AddObject(Colors);
	}

// End of File
