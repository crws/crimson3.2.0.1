
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BUTTON_HPP
	
#define	INCLUDE_BUTTON_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimGradButton;
class CPrimImageBtnBase;
class CPrimImageIndicator;
class CPrimImageIllumButton;
class CPrimImageButton;
class CPrimImageToggle;
class CPrimImageToggle2;
class CPrimImageToggle3;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Graduated Button
//

class CPrimGradButton : public CPrimWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimGradButton(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Overridables
		BOOL  HitTest(P2 Pos);
		void  SetInitState(void);
		void  FindTextRect(void);
		CSize GetMinSize(IGDI *pGDI);
		void  Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;
		CPrimPen   * m_pEdge;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Static Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL Shader(IGDI *pGDI, int p, int c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button Base
//

class CPrimImageBtnBase : public CPrimWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageBtnBase(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void SetImage(UINT n, PCTXT pFile);

		// Overridables
		BOOL HitTest(P2 Pos);
		void FindTextRect(void);
		void Draw(IGDI *pGDI, UINT uMode);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	     m_Keep;
		CPrimImage * m_pImage[4];
		R2           m_ImageRect;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Image Index
		virtual UINT GetIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Indicator
//

class CPrimImageIndicator : public CPrimImageBtnBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageIndicator(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Overridables
		void SetInitState(void);
		BOOL StepAddress(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pState;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Image Index
		UINT GetIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Illuminated Button
//

class CPrimImageIllumButton : public CPrimImageIndicator
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageIllumButton(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void SetInitState(void);
		BOOL StepAddress(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pValue;
		UINT         m_Button;
		UINT	     m_Delay;
		CCodedItem * m_pEnable;
		UINT         m_Local;
		UINT	     m_Protect;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Image Index
		UINT GetIndex(void);

		// Implementation
		BOOL IsMomentary(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button
//

class CPrimImageButton : public CPrimImageBtnBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageButton(void);

		// Overridables
		void  SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Image Index
		UINT GetIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Toggle Base Class
//

class CPrimImageToggle : public CPrimImageBtnBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageToggle(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		BOOL StepAddress(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pValue;
		CCodedItem * m_pEnable;
		UINT         m_Local;
		UINT	     m_Protect;
		UINT         m_Axis;
		UINT	     m_Mode;
		UINT	     m_Delay;
		UINT         m_Default;
		CCodedItem * m_pPress1;
		CCodedItem * m_pRelease1;
		CCodedItem * m_pPress2;
		CCodedItem * m_pRelease2;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		BOOL IsMomentary(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 2-State Toggle
//

class CPrimImageToggle2 : public CPrimImageToggle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageToggle2(void);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_ValA;
		UINT m_ValB;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Image Index
		UINT GetIndex(void);

		// Implementation
		BOOL IsMomentary(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 3-State Toggle
//

class CPrimImageToggle3 : public CPrimImageToggle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImageToggle3(void);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_ValA;
		UINT m_ValB;
		UINT m_ValC;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Image Index
		UINT GetIndex(void);
	};

// End of File

#endif
