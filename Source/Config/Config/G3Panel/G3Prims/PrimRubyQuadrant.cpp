
#include "intern.hpp"

#include "PrimRubyQuadrant.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Quadrant Primitive
//

// Dyanmic Class

AfxImplementDynamicClass(CPrimRubyQuadrant, CPrimRubyPartial);

// Constructor

CPrimRubyQuadrant::CPrimRubyQuadrant(void)
{
	m_fDrop = TRUE;
	}

// Meta Data

void CPrimRubyQuadrant::AddMetaData(void)
{
	CPrimRubyPartial::AddMetaData();

	Meta_SetName((IDS_QUADRANT));
	}

// Path Generation

void CPrimRubyQuadrant::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	CRubyPoint  cp(p1.m_x, p2.m_y);

	CRubyVector rd(p2 - p1);

	if( !m_Radial && m_pEdge->GetWidth() == 1 ) {

		rd.m_x -= 1;
		rd.m_y -= 1;

		CRubyPoint c1(p1);
		CRubyPoint c2(p2);

		c1.m_y += 1;
		c2.m_x -= 1;

		CRubyDraw::Arc(figure, cp, c1, c2, rd);
		}
	else
		CRubyDraw::Arc(figure, cp, p1, p2, rd);

	figure.Append(cp);

	figure.AppendHardBreak();
	}

// End of File
