
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPartial_HPP
	
#define	INCLUDE_PrimRubyPartial_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyOriented.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Partial Primitive
//

class CPrimRubyPartial : public CPrimRubyOriented
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPartial(void);

		// Data Members
		UINT m_Radial;

	protected:
		// Data Members
		BOOL m_fDrop;

		// Meta Data
		void AddMetaData(void);

		// Fast Fill Control
		BOOL UseFastFill(void);

		// Path Management
		void MakePaths(void);
	};

// End of File

#endif
