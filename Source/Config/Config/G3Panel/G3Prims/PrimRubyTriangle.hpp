
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyTriangle_HPP
	
#define	INCLUDE_PrimRubyTriangle_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithHandle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Triangle Primitive
//

class CPrimRubyTriangle : public CPrimRubyWithHandle
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyTriangle(void);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Generation
		void MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
