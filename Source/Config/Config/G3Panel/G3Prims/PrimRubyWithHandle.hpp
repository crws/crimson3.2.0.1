
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyWithHandle_HPP
	
#define	INCLUDE_PrimRubyWithHandle_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyOriented.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Oriented Primitive with 1 Handle
//

class CPrimRubyWithHandle : public CPrimRubyOriented
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyWithHandle(void);

		// Overridables
		BOOL GetHand(UINT uHand, CPrimHand &Hand);

	protected:
		// Data Members
		INT m_Pos1;
		INT m_Max1;

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
