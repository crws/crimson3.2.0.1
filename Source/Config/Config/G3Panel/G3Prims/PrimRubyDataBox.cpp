
#include "intern.hpp"

#include "PrimRubyDataBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Data Box Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyDataBox, CPrimRubyRect);

// Constructor

CPrimRubyDataBox::CPrimRubyDataBox(void)
{
	}

// Operations

void CPrimRubyDataBox::Set(CString Text, CSize MaxSize)
{
	m_pDataItem->Set(Text);

	m_pDataItem->m_Content = 1;

	CString Sizing = m_pDataItem->GetSizingText();

	int     nWidth = m_pDataItem->GetTextWidth(Sizing);

	SetInitSize(8 + nWidth);

	UpdateLayout();
	}

// Overridables

void CPrimRubyDataBox::SetInitState(void)
{
	CPrimRubyRect::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pFill->m_Pattern = 0;

	AddData();

	SetInitSize(54, m_pDataItem->GetFontSize(1) + 1);
	}

void CPrimRubyDataBox::FindTextRect(void)
{
	m_TextRect  = m_DrawRect;

	int nAdjust = m_pEdge->GetInnerWidth();

	DeflateRect(m_TextRect, nAdjust, nAdjust);
	}

CSize CPrimRubyDataBox::GetMinSize(IGDI *pGDI)
{
	CSize Size(8, 8);

	if( m_pDataItem ) {

		// REV3 -- If we've got a drop shadow, this could
		// be too small, but for the moment we let it go
		// to avoid too many red error bars.
	
		Size.cy = m_pDataItem->GetFontSize(1);

		if( m_pDataItem->m_Entry ) {

			Size.cy += 1;
			}
		}

	return Size;
	}

// Meta Data

void CPrimRubyDataBox::AddMetaData(void)
{
	CPrimRubyRect::AddMetaData();

	Meta_SetName((IDS_DATA_BOX_2));
	}

// End of File
