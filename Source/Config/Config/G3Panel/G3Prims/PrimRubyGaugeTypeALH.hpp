
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeALH_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeALH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeAL.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Horizontal Linear Gauge Primitive
//

class CPrimRubyGaugeTypeALH : public CPrimRubyGaugeTypeAL
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeALH(void);

		// Overridables
		void SetInitState(void);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
