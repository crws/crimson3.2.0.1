
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ScratchPadWnd_HPP

#define INCLUDE_ScratchPadWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Scratch Pad Window
//

class DLLAPI CScratchPadWnd : public CViewWnd, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CScratchPadWnd(void);
		
		// Destructor
		~CScratchPadWnd(void);

		// Attributes
		BOOL       IsActive(void) const;
		CViewWnd * GetView(void) const;
		
		// Operations
		void Destroy(void);
		void SetSemiModal(CDialog *pDlg);
		void Show(BOOL fShow);
		void NavTreeActive(CRect const &Rect);
		void NavToScratch(void);
		void DragFromScratch(BOOL fDrag);
		void DragIntoMain(void);
		void ClickOnMain(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		
	protected:
		// Timer IDs
		static UINT m_timerHover;
		static UINT m_timerSelect;

		// Data Members
		DWORD	      m_dwStyle;
		CDispPage   * m_pPage;
		CViewWnd    * m_pView;
		CViewWnd    * m_pMain;
		CBlockerWnd * m_pBlock;
		CDialog     * m_pSemi;
		BOOL	      m_fPinned;
		BOOL	      m_fDrag;
		BOOL	      m_fHide;
		BOOL	      m_fCapture;
		BOOL	      m_fHover;
		BOOL	      m_fTiny;
		BOOL	      m_fActive;
		BOOL	      m_fMoving;
		CPoint	      m_Click;
		CSysProxy     m_System;
		CPoint	      m_TinyPos;
		CPoint	      m_NormPos;
		CSize	      m_TinySize;
		CSize	      m_NormSize;
		CBitmap	      m_Bitmap;
		CCursor	      m_MoveCursor;
		CDropHelper   m_DropHelp;
		CToolTip    * m_pToolTip;
		CFont	      m_Font;
		int	      m_nCaption;
		CToolbarWnd * m_pTool;
		
		// Overribables
		void    OnAttach(void);
		BOOL    OnPreDetach(void);
		void    OnDetach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnGoingIdle(void);
		void OnMainMoved(CRect const *pRect);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		void OnActivate(UINT uCode, BOOL fMinimized, CWnd &Wnd);
		void OnWindowPosChanged(WINDOWPOS &Pos);
		void OnClose(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnSetFocus(CWnd &Wnd);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnPrint(CDC &DC, UINT uFlags);
		UINT OnNCHitTest(CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnShowWindow(BOOL fShow, UINT uStatus);

		// Notification Handlers
		UINT OnToolLinkClick(UINT uID, NMHDR &Info);

		// Command Handlers
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolCommand(UINT uID);
			
		// Implementation
		CRect GetViewRect(void);
		BOOL  FindMainEditor(void);
		void  SetTiny(BOOL fTiny, BOOL fActivate);
		void  SetTiny(BOOL fActivate);
		void  FindTinyRect(void);
		void  FindNormRect(void);
		BOOL  OverlapsMain(void);
		void  InitHover(void);
		void  KillHover(void);
		void  CreateToolTip(void);
		void  DestroyToolTip(void);
		void  MoveToolbar(void);
		CRect GetToolbarRect(void);
	};

// End of File

#endif
