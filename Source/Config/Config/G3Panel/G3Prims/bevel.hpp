
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BEVEL_HPP
	
#define	INCLUDE_BEVEL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimBevelBase;
class CPrimBevel;
class CPrimBevelButton;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel Base Class
//

class CPrimBevelBase : public CPrimWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimBevelBase(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Overridables
		BOOL  HitTest(P2 Pos);
		void  SetInitState(void);
		void  FindTextRect(void);
		CSize GetMinSize(IGDI *pGDI);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_Border;
		CPrimBrush * m_pFace;
		CPrimColor * m_pHilite;
		CPrimColor * m_pShadow;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void FindPoints(P2 *t, P2 *b, R2 &r);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel
//

class CPrimBevel : public CPrimBevelBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimBevel(void);

		// Overridables
		void FindTextRect(void);
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Type;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel Button
//

class CPrimBevelButton : public CPrimBevelBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimBevelButton(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
