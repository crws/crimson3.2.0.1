
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyPatternLib_HPP
	
#define	INCLUDE_RubyPatternLib_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Library
//

class CRubyPatternLib : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRubyPatternLib(void);

		// Destructors
		~CRubyPatternLib(void);

		// Attributes
		UINT    GetCount(void) const;
		UINT    EnumCode(UINT n) const;
		CString EnumName(UINT n) const;

		// Operations
		void ClearList(void);
		void Register(UINT uCode);
		void GetRefs(CPrimRefList &Refs, UINT uCode);

		// Gdi Configuration
		PSHADER SetGdi(IGdi *pGdi, UINT uCode, COLOR Fore, COLOR Back);

		// Windows Preview
		void Draw(CDC &DC, CRect Rect, UINT uCode, COLOR Fore, COLOR Back);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CWordArray m_List;

	protected:
		// Data Members
		CImageManager * m_pImages;
		IGdiWindows   * m_pWin;
		IGdi          * m_pGdi;
		BYTE	      * m_pData;
		UINT	        m_uData;

		// Implementation
		void FindManager(void);

		// Meta Data
		void AddMetaData(void);

		// Static Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL ShaderLinear(IGDI *pGDI, int p, int c);
		static BOOL ShaderMiddle(IGDI *pGDI, int p, int c);
		static BOOL ShaderTable(IGdi *pGdi, int p, int c, int size, int const *pos, int const *col);
		static BOOL ShaderChrome(IGDI *pGDI, int p, int c);
		static BOOL ShaderHorzCylinder(IGDI *pGDI, int p, int c);
		static BOOL ShaderVertCylinder(IGDI *pGDI, int p, int c);
	};

// End of File

#endif
