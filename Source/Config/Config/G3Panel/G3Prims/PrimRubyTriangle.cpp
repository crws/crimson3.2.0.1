
#include "intern.hpp"

#include "PrimRubyTriangle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Triangle Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyTriangle, CPrimRubyWithHandle);

// Constructor

CPrimRubyTriangle::CPrimRubyTriangle(void)
{
	}

// Meta Data

void CPrimRubyTriangle::AddMetaData(void)
{
	CPrimRubyWithHandle::AddMetaData();

	Meta_SetName((IDS_TRIANGLE_2));
	}

// Path Generation

void CPrimRubyTriangle::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	number y1 = p1.m_y + (p2.m_y - p1.m_y) * m_Pos1 / 10000;

	figure.Append(p1.m_x, p1.m_y);
	
	figure.Append(p2.m_x, y1);
	
	figure.Append(p1.m_x, p2.m_y);

	figure.AppendHardBreak();
	}

// End of File
