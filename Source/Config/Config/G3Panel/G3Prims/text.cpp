
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Text Block Page
//

class CPrimTextPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimTextPage(CPrimText *pText, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimText * m_pText;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Text Block Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimTextPage, CUIStdPage);

// Constructor

CPrimTextPage::CPrimTextPage(CPrimText *pText, CString Title, UINT uPage)
{
	m_pText  = pText;

	m_Title  = Title;

	m_Class  = AfxRuntimeClass(CPrimText);

	m_uPage  = uPage;
	}

// Operations

BOOL CPrimTextPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, m_pText);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive Text Block
//

// Dynamic Class

AfxImplementDynamicClass(CPrimText, CPrimBlock);

// Constructor

CPrimText::CPrimText(void)
{
	m_pText   = NULL;

	m_pColor  = New CPrimColor;

	m_pShadow = New CPrimColor;

	m_fError  = FALSE;
	}

// UI Creation

BOOL CPrimText::OnLoadPages(CUIPageList *pList)
{			 
	CPrimTextPage *pPage1 = New CPrimTextPage(this, CString(IDS_TEXT),  1);

	CPrimTextPage *pPage2 = New CPrimTextPage(this, CString(IDS_MORE),  2);

	pList->Append(pPage1);

	pList->Append(pPage2);
	
	return TRUE;
	}

// UI Update

void CPrimText::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CPrimBlock::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimText::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Text" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimBlock::GetTypeData(Tag, Type);
	}

// Attributes

BOOL CPrimText::IsEmpty(void) const
{
	return !m_pText || m_pText->GetSource(TRUE) == L"\"\"";
	}

BOOL CPrimText::IsEditable(void) const
{
	return m_pText->IsStringConst();
	}

int CPrimText::GetLineSize(void) const
{
	return GetTextSize(1);
	}

int CPrimText::GetTextSize(int nLines) const
{
	return GetFontSize(nLines) + nLines * m_Lead;
	}

BOOL CPrimText::GetBoundingRect(R2 &Rect) const
{
	if( !m_fError && !IsEmpty() ) {

		m_Flow.GetBoundingRect(Rect);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimText::AddBoundingRect(R2 &Rect) const
{
	if( !m_fError && !IsEmpty() ) {

		m_Flow.AddBoundingRect(Rect);	

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CPrimText::TextDirty(void)
{
	m_Flow.Dirty();
	}

BOOL CPrimText::Draw(IGDI *pGDI, R2 Rect, UINT uMode)
{
	m_fError = FALSE;

	SelectFont(pGDI);

	pGDI->SetTextTrans(modeTransparent);

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	if( TRUE ) {

		CString Text = UniVisual(m_pText->GetText());

		m_Flow.Flow(pGDI, Text, Rect);

		CTextFlow::CFormat Fmt;

		Fmt.m_AlignH   = m_AlignH;
		
		Fmt.m_AlignV   = m_AlignV;
		
		Fmt.m_Lead     = m_Lead;
		
		Fmt.m_Color    = m_pColor->GetColor();
		
		Fmt.m_Shadow   = m_pShadow->GetColor();
		
		Fmt.m_fMove    = CanMove() && IsPressed();
		
		Fmt.m_MoveDir  = m_MoveDir;
		
		Fmt.m_MoveStep = m_MoveStep;

		if( !m_Flow.Draw(pGDI, Fmt) ) {

			m_fError = TRUE;
			}
		}

	return !m_fError;
	}

void CPrimText::SetTextColor(COLOR Color)
{
	m_pColor->Set(Color);
	}

void CPrimText::Set(PCTXT pText, BOOL fAll)
{
	AfxAssert(m_pText);

	m_pText->SetText(pText, fAll);

	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Defaults");

	COLOR Fore = COLOR(Reg.GetValue(L"TextText", 0x7FFF));

	m_pColor->Set(Fore);
	}

BOOL CPrimText::StepAddress(void)
{
	return StepCoded(m_pText);
	}

// Persistance

void CPrimText::Init(void)
{
	CPrimBlock::Init();

	InitText(L"TEXT");

	m_pColor->Set(GetRGB(31,31,31));
	}

void CPrimText::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString          Name  = Tree.GetName();

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( Name == L"Text" ) {

			if( Tree.IsSimpleValue() ) {

				m_Text = Tree.GetValueAsString();

				continue;
				}

			m_Text.Empty();
			}

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}
	}

void CPrimText::PostLoad(void)
{
	if( m_Text.GetLength() ) {

		InitText(m_Text);

		m_Text.Empty();
		}

	CPrimBlock::PostLoad();
	}

// Download Support

BOOL CPrimText::MakeInitData(CInitData &Init)
{
	CPrimBlock::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pText);
	
	Init.AddItem(itemSimple, m_pColor);
	
	Init.AddItem(itemSimple, m_pShadow);

	return TRUE;
	}

// Implementation

void CPrimText::InitText(CString Text)
{
	CTypeDef Type;

	Type.m_Type  = typeString;

	Type.m_Flags = 0;

	m_pText = New CCodedText;

	m_pText->SetParent(this);

	m_pText->Init();

	m_pText->SetReqType(Type);

	m_pText->SetText(Text, TRUE);
	}

// Meta Data

void CPrimText::AddMetaData(void)
{
	CPrimBlock::AddMetaData();

	Meta_AddVirtual(Text);
	Meta_AddObject (Color);
	Meta_AddObject (Shadow);

	Meta_SetName((IDS_TEXT));
	}

// End of File
