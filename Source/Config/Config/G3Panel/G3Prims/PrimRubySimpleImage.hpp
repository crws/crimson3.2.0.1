
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubySimpleImage_HPP
	
#define	INCLUDE_PrimRubySimpleImage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyShade;

//////////////////////////////////////////////////////////////////////////
//
// Ruby Simple Image Primitive
//

class CPrimRubySimpleImage : public CPrimRubyWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubySimpleImage(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void LoadFromDataObject(IDataObject *pData, CSize MaxSize);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void UpdateLayout(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Data Members
		UINT	         m_Keep;
		CPrimImage     * m_pImage;
		CCodedItem     * m_pColor;
		CPrimRubyShade * m_pShade;
	
	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
