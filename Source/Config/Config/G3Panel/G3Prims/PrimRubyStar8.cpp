
#include "intern.hpp"

#include "PrimRubyStar8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 8-Pointed Star Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyStar8, CPrimRubyStar)

// Constructor

CPrimRubyStar8::CPrimRubyStar8(void)
{
	m_nSides = 16;

	m_AltRad = 40;

	m_Rotate = 225;
	}

// Meta Data

void CPrimRubyStar8::AddMetaData(void)
{
	CPrimRubyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_8));
	}

// End of File
