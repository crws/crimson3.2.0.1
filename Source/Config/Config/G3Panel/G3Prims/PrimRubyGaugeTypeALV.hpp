
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeALV_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeALV_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeAL.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Vertical Linear Gauge Primitive
//

class CPrimRubyGaugeTypeALV : public CPrimRubyGaugeTypeAL
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeALV(void);

		// Overridables
		void SetInitState(void);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
