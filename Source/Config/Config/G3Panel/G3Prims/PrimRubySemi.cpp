
#include "intern.hpp"

#include "PrimRubySemi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Semi Primitive
//

// Dyanmic Class

AfxImplementDynamicClass(CPrimRubySemi, CPrimRubyPartial);

// Constructor

CPrimRubySemi::CPrimRubySemi(void)
{
	}

// Meta Data

void CPrimRubySemi::AddMetaData(void)
{
	CPrimRubyPartial::AddMetaData();

	Meta_SetName((IDS_SEMI_ELLIPSE));
	}

// Path Generation

void CPrimRubySemi::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	number dx = p2.m_x - p1.m_x;

	number dy = p2.m_y - p1.m_y;

	number ka = (!m_Radial && m_pEdge->GetWidth() == 1) ? 1 : 0;

	CRubyPoint  c1(p1.m_x, p1.m_y + ka);

	CRubyPoint  c2(p1.m_x, p2.m_y - ka);

	CRubyPoint  cp(p1.m_x, p1.m_y + dy / 2);

	CRubyVector rd(dx - ka, dy / 2 - ka);

	CRubyDraw::Arc(figure, cp, c1, c2, rd);

	figure.AppendHardBreak();
	}

// End of File
