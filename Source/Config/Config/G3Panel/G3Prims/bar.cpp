
#include "intern.hpp"

#include "bar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyBar, CPrimRich);

// Static Data

UINT   CPrimLegacyBar::m_ShadeMode = 0;

COLOR  CPrimLegacyBar::m_ShadeCol3 = 0;

C3REAL CPrimLegacyBar::m_ShadeData = 0;

// Constructor

CPrimLegacyBar::CPrimLegacyBar(void)
{
	m_pEdge     = New CPrimPen;

	m_Mode      = 0;
	
	m_pFill     = New CPrimBrush;

	m_pColor3   = New CPrimColor;

	m_ShowSP    = 0;

	m_ShowMask  = propLabel | propColor | propLimits | propFormat;
	}

// UI Creation

BOOL CPrimLegacyBar::OnLoadPages(CUIPageList *pList)
{
	LoadFirstPage(pList);

	pList->Append( New CUIStdPage( CString(IDS_OPTIONS),
				       AfxPointerClass(this),
				       2
				       ));

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       3
				       ));

	LoadRichPages(pList);
	
	return TRUE;
	}

// Overridables

void CPrimLegacyBar::SetInitState(void)
{
	CPrimRich::SetInitState();

	m_pFill->Set(GetRGB(12,12,24));

	m_pColor3->Set(GetRGB(12,12,12));
	
	m_pEdge->Set(GetRGB(31,31,31));

	m_pEdge->m_Corner = 1;

	m_Margin.top      = 1;

	m_Margin.left     = 1;
	
	m_Margin.right    = 1;
	
	m_Margin.bottom   = 1;
	}

// Download Support

BOOL CPrimLegacyBar::MakeInitData(CInitData &Init)
{
	CPrimRich::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pEdge);

	Init.AddItem(itemSimple, m_pFill);

	Init.AddItem(itemSimple, m_pColor3);

	Init.AddByte(BYTE(m_Mode));

	Init.AddByte(BYTE(m_ShowSP));

	return TRUE;
	}

// Meta Data

void CPrimLegacyBar::AddMetaData(void)
{
	CPrimRich::AddMetaData();
	
	Meta_AddObject (Edge);
	Meta_AddObject (Fill);
	Meta_AddInteger(Mode);
	Meta_AddObject (Color3);
	Meta_AddInteger(ShowSP);

	Meta_SetName((IDS_BAR));
	}

// Field Requirements

UINT CPrimLegacyBar::GetNeedMask(void) const
{
	UINT uMask = propLimits | propFormat | propColor;
	
	if( m_Content > 0 && m_Content < 3 ) {

		uMask |= propLabel;
		}

	return uMask;
	}

// Shaders

BOOL CPrimLegacyBar::Shader(IGDI *pGDI, int p, int c)
{
	static int nTrip = 0;

	static int nMode = 0;

	if( c ) {

		if( p == 0 ) {

			// LATER -- Check rounding...

			nTrip = int(c * m_ShadeData);

			if( m_ShadeMode == 1 || m_ShadeMode == 4 ) {

				nTrip = c - nTrip;

				nMode = 0;
				}

			if( m_ShadeMode == 2 || m_ShadeMode == 3 ) {

				nMode = 1;
				}

			if( nTrip ) {

				if( nMode == 0 ) {

					pGDI->SetBrushStyle(brushFore);
		
					pGDI->SetBrushFore(m_ShadeCol3);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushNull);
					}

				return TRUE;
				}
			}

		if( p >= nTrip ) {

			if( nMode < 2 ) {

				if( nMode == 0 ) {
	
					pGDI->SetBrushStyle(brushNull);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushFore);

					pGDI->SetBrushFore(m_ShadeCol3);
					}

				nMode = 2;

				return TRUE;
				}
			}

		return FALSE;
		}

	if( m_ShadeMode == 1 || m_ShadeMode == 2 ) {

		return FALSE;
		}

	return TRUE;
	}

// Overridables

void CPrimLegacyBar::DrawSP(IGDI *pGDI, R2 Rect, UINT uMode)
{
	}

// Implementation

C3REAL CPrimLegacyBar::GetValue(CCodedItem *pItem, C3REAL Default)
{
	if( pItem ) {
		
		if( !pItem->IsBroken() ) {

			return I2R(pItem->Execute(typeReal));
			}
		}

	return Default;
	}

BOOL CPrimLegacyBar::PrepFill(void)
{
	C3REAL Min = I2R(GetMinValue(typeReal));

	C3REAL Max = I2R(GetMaxValue(typeReal));

	if( Min != Max ) {

		C3REAL Data = GetValue(m_pValue, 25);

		m_ShadeData = (Data - Min) / (Max - Min);
		}
	else
		m_ShadeData = 0.75;
	
	m_ShadeMode = m_Mode;

	m_ShadeCol3 = m_pColor3->GetColor();

	return TRUE;
	}

void CPrimLegacyBar::FillRect(IGDI *pGDI, R2 Rect)
{
	// OPTIM -- Add an optimization to draw using two
	// rectangles when not using a shader for the fill.

	if( m_Mode ) {

		m_pFill->FillRect(pGDI, Rect);

		if( PrepFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeRect(PassRect(Rect), Shader);
			}

		return;
		}

	m_pFill->FillRect(pGDI, Rect);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyVertBar, CPrimLegacyBar);

// Constructor

CPrimLegacyVertBar::CPrimLegacyVertBar(void)
{
	m_uType = 0x2B;
	}

// Overridables

void CPrimLegacyVertBar::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;	

	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));
		}

	if( m_Content < 3 ) {

		SelectFont(pGDI, m_Font);

		if( m_Content > 0 ) {

			R2 Fill = Rect;
			
			Fill.y1 = Fill.y2 - GetFontSize() - 1;
			
			Rect.y2 = Fill.y1 - 1;

			pGDI->SelectBrush(brushFore);
			
			pGDI->SetForeColor(m_pColor3->GetColor());
			
			pGDI->FillRect(PassRect(Fill));

			pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());		

			pGDI->DrawLine(Rect.x1, Rect.y2, Rect.x2, Rect.y2);

			Fill.y1 = Fill.y1 + 1;

			pGDI->SetTextFore(m_pEdge->m_pColor->GetColor());
			
			pGDI->SetBackMode(modeTransparent);

			DrawLabel(pGDI, Fill);
			}
		}

	pGDI->ResetPen();

	pGDI->ResetBrush();

	FillRect(pGDI, Rect);

	InflateRect( Rect,
		     m_pEdge->m_Width ? 2 : 1,
		     m_pEdge->m_Width ? 2 : 1
		     );

	DrawSP(pGDI, Rect, uMode);

	if( m_Content < 2 ) {
		
		DrawValue(pGDI, Rect);
		}
	}

void CPrimLegacyVertBar::SetInitState(void)
{
	CPrimLegacyBar::SetInitState();

	m_Mode = 1;

	SetInitSize(48, 161);
	}

// Download Support

BOOL CPrimLegacyVertBar::MakeInitData(CInitData &Init)
{
	CPrimLegacyBar::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimLegacyVertBar::AddMetaData(void)
{
	CPrimLegacyBar::AddMetaData();
	
	Meta_SetName((IDS_VERT_BAR));
	}

// Overridables

void CPrimLegacyVertBar::DrawSP(IGDI *pGDI, R2 Rect, UINT uMode)
{
	if( m_ShowSP ) {

		int x1 = Rect.x1;
		int y1 = Rect.y1;
		int x2 = Rect.x2;
		int y2 = Rect.y2;

		pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());
		
		int dv = 1000; /* TODO -- Need to sim setpoint */

		int yp = y2 - (750 - 0) * (y2 - y1) / dv;

		for( int n = 0; n < 4; n++ ) {

			pGDI->DrawLine(x1-n+4, yp-n, x1-n+4, yp+n+1);
			
			pGDI->DrawLine(x2+n-5, yp-n, x2+n-5, yp+n+1);
			}			

		pGDI->SelectPen(penGray);

		pGDI->DrawLine(x1+5, yp, x2-5, yp);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyHorzBar, CPrimLegacyBar);

// Constructor

CPrimLegacyHorzBar::CPrimLegacyHorzBar(void)
{
	m_uType = 0x2C;
	}

// Overridables

void CPrimLegacyHorzBar::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;	

	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));
		}

	if( m_Content < 3 ) {

		SelectFont(pGDI, m_Font);

		if( m_Content > 0 ) {

			CString Label = FindLabelText();

			R2 Fill = Rect;
			
			Fill.x2 = Fill.x1 + GetTextWidth(Label);
			
			Rect.x1 = Fill.x2 + 1;

			pGDI->SelectBrush(brushFore);
			
			pGDI->SetForeColor(m_pColor3->GetColor());
			
			pGDI->FillRect(PassRect(Fill));

			pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());		

			pGDI->DrawLine(Rect.x1-1, Rect.y1, Rect.x1-1, Rect.y2);

			pGDI->SetTextFore(m_pEdge->m_pColor->GetColor());
			
			pGDI->SetBackMode(modeTransparent);

			DrawLabel(pGDI, Fill);
			}
		}

	pGDI->ResetPen();

	pGDI->ResetBrush();

	FillRect(pGDI, Rect);

	InflateRect( Rect,
		     m_pEdge->m_Width ? 2 : 1,
		     m_pEdge->m_Width ? 2 : 1
		     );

	DrawSP(pGDI, Rect, uMode);

	if( m_Content < 2 ) {
		
		DrawValue(pGDI, Rect);
		}
	}

void CPrimLegacyHorzBar::SetInitState(void)
{
	CPrimLegacyBar::SetInitState();

	m_Mode = 3;

	SetInitSize(161, 49);
	}

// Download Support

BOOL CPrimLegacyHorzBar::MakeInitData(CInitData &Init)
{
	CPrimLegacyBar::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimLegacyHorzBar::AddMetaData(void)
{
	CPrimLegacyBar::AddMetaData();

	Meta_SetName((IDS_HORZ_BAR));
	}

// Overridables

void CPrimLegacyHorzBar::DrawSP(IGDI *pGDI, R2 Rect, UINT uMode)
{
	if( m_ShowSP ) {

		int x1 = Rect.x1;
		int y1 = Rect.y1;
		int x2 = Rect.x2;
		int y2 = Rect.y2;

		pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());
		
		int dv = 1000; /* TODO -- Need to sim setpoint */

		int xp = x1 + (750 - 0) * (x2 - x1) / dv;

		for( int n = 0; n < 4; n++ ) {

			pGDI->DrawLine(xp-n, y1-n+4, xp+n+1, y1-n+4);
			
			pGDI->DrawLine(xp-n, y2+n-5, xp+n+1, y2+n-5);
			}			

		pGDI->SelectPen(penGray);

		pGDI->DrawLine(xp, y1+5, xp, y2-5);
		}
	}

// End of File
