
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeBase_HPP
	
#define	INCLUDE_PrimRubyGaugeBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRuby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Base Gauge Primitive
//

class CPrimRubyGaugeBase : public CPrimRuby
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimRubyGaugeBase(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void Draw(IGDI *pGdi, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pValue;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimColor * m_pMajorColor;
		CPrimColor * m_pMinorColor;
		UINT	     m_Major;
		UINT	     m_Minor;
		UINT	     m_PointMode;
		CPrimColor * m_pPointColor;
		UINT	     m_BandShow1;
		CPrimColor * m_pBandColor1;
		CCodedItem * m_pBandMin1;
		CCodedItem * m_pBandMax1;
		UINT	     m_BandShow2;
		CPrimColor * m_pBandColor2;
		CCodedItem * m_pBandMin2;
		CCodedItem * m_pBandMax2;
		UINT	     m_BugShow1;
		CPrimColor * m_pBugColor1;
		CCodedItem * m_pBugValue1;
		UINT	     m_BugShow2;
		CPrimColor * m_pBugColor2;
		CCodedItem * m_pBugValue2;
		UINT	     m_Orient;
		UINT	     m_Reflect;
		UINT	     m_Rotate;

	protected:
		// Data Members
		CRubyDrawPlus m_d;
		CRubyMatrix   m_m;
		number	      m_scale;
		CRubyPoint    m_pointCenter;
		CRubyPath     m_pathMajor;
		CRubyPath     m_pathMinor;
		CRubyPath     m_pathBand1;
		CRubyPath     m_pathBand2;
		CRubyPath     m_pathBug1;
		CRubyPath     m_pathBug2;
		CRubyPath     m_pathOuter;
		CRubyGdiList  m_listMajor;
		CRubyGdiList  m_listMinor;
		CRubyGdiList  m_listBand1;
		CRubyGdiList  m_listBand2;
		CRubyGdiList  m_listBug1;
		CRubyGdiList  m_listBug2;

		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void InitPaths(void);
		void MakeLists(void);

		// Scaling
		number GetValue(CCodedItem *pValue, C3REAL Default);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void SetTransform(void);

		// Overridables
		virtual CRubyPath & GetOuterPath(void) = 0;

		// Shading
		static BOOL  ShaderDark  (IGdi *pGdi, int p, int c);
		static BOOL  ShaderChrome(IGdi *pGdi, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);
	};

// End of File

#endif
