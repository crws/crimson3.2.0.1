
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispNavTreeWnd_HPP

#define INCLUDE_DispNavTreeWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispCatWnd;

//////////////////////////////////////////////////////////////////////////
//
// Display Page Navigation Window
//

class CDispNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CDispNavTreeWnd(void);

	protected:
		// Data Members
		CUISystem   * m_pSystem;
		CUIManager  * m_pManager;
		CPageList   * m_pPages;
		CPrimRefMap   m_Refs;
		CDispCatWnd * m_pCatView;

		// Overridables
		void OnAttach(void);
		BOOL OnNavigate(CString const &Nav);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);

		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
		BOOL AddItemToStream(ITextStream &Stream, HTREEITEM hItem, BOOL fRich);
		void AddItemToTreeFile(CTreeFile &Tree, CPrimRefList &Refs, HTREEITEM hItem);

		// Data Object Hooks
		void OnAcceptInit(CTreeFile &Tree);
		BOOL OnAcceptName(CTreeFile &Tree, BOOL fLocal, CString Name);
		void OnAcceptDone(CTreeFile &Tree, HTREEITEM hItem);

		// Reference Helpers
		void FixRefsFrom(HTREEITEM hItem, UINT uPass);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		void NewItemSelected(void);
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void OnItemDeleted(CItem *pItem, BOOL fExec);
		void OnItemRenamed(CItem *pItem);

		// Implementation
		void FindCatView(void);
	};

// End of File

#endif
