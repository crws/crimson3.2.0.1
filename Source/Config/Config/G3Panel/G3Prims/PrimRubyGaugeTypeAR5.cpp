
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A5 Radial Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR5, CPrimRubyGaugeTypeAR);

// Constructor

CPrimRubyGaugeTypeAR5::CPrimRubyGaugeTypeAR5(void)
{
	m_Reflect = 1;
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR5::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeAR::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR5::AddMetaData(void)
{
	CPrimRubyGaugeTypeAR::AddMetaData();

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE_6));
	}

// Path Management

void CPrimRubyGaugeTypeAR5::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeAR::MakePaths();

	m_bezelBase = 66;

	Rect(m_pathFace,  66, 67);

	Rect(m_pathOuter, 66, 80, 90);
	Rect(m_pathInner, 66, 70, 80);
	Rect(m_pathRing1, 66, 89, 91);
	Rect(m_pathRing2, 66, 79, 80);
	Rect(m_pathRing3, 66, 66, 70);
	Rect(m_pathRing4, 66, 74, 69);
	}

// Implementation

void CPrimRubyGaugeTypeAR5::LoadLayout(void)
{
	m_radiusPivot     =  10 * m_ScalePivot / 100.0;

	m_pointPivot.m_x  =  40 + m_radiusPivot;
	m_pointPivot.m_y  = 160 - m_radiusPivot;
	
	m_angleMin        = +90 - 90;
	m_angleMax        =   0 - 90;
	
	m_radiusOuter.m_x = 110;
	m_radiusOuter.m_y = 110;

	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBug.m_x   = m_PointMode ? 115 : 115;
	m_radiusMajor.m_x = m_PointMode ?  92 :  95;
	m_radiusMinor.m_x = m_PointMode ?  98 : 100;
	m_radiusBand.m_x  = m_PointMode ? 100 : 105;
	m_radiusPoint.m_x = m_PointMode ?  91 : 102;
	m_radiusSweep.m_x = m_PointMode ?  80 : 102;

	CalcRadii();
	}

// End of File
