
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DIAL_HPP
	
#define	INCLUDE_DIAL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyDial;
class CPrimLegacyWholeDial;
class CPrimLegacyHalfDial;
class CPrimLegacyQuadDial;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Dial Gauge
//

class CPrimLegacyDial : public CPrimRich
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyDial(void);
		
		// Initial Values
		void SetInitValues(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);
		
		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void UpdateLayout(void);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	       m_Orient;
		UINT	       m_Flip;
		UINT	       m_Major;
		UINT	       m_Minor;
		INT	       m_Start;
		INT	       m_Sweep;
		CPrimPen     * m_pEdge;
		CPrimBrush   * m_pBack;
		CPrimColor   * m_pFill;

	protected:
		// Data Members
		R2	m_DialRect;
		UINT	m_Entity;
		P2	m_Origin;
		INT	m_nRadius;
		INT	m_nScale;
		INT	m_nRadius1;
		INT	m_nRadius2;
		INT	m_nRadius3;

		// Meta Data
		void AddMetaData(void);

		// Field Requirements
		UINT GetNeedMask(void) const;

		// Overridables
		virtual void FindDialRect(void);
		virtual void FindPolar(void);
		virtual void FindScaleSweep(void);
		virtual void FindScaleRadius(void);

		// Implementation
		void DrawScale(IGDI *pDGI, int nRad1, int nRad2, UINT uMode);
		void DrawRect(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle, UINT uMode);
		void DrawLine(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle, UINT uMode);
		void DrawPointer(IGDI *pGDI, P2 Point, INT Radius, INT nAngle, UINT uMode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Whole Dial Gauge
//

class CPrimLegacyWholeDial : public CPrimLegacyDial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyWholeDial(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		void FindDialRect(void);
		void FindPolar(void);
		void FindScaleSweep(void);
		void FindScaleRadius(void);

		// Implementation
		void DrawLegend(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Half Dial Gauge
//

class CPrimLegacyHalfDial : public CPrimLegacyDial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyHalfDial(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		void FindDialRect(void);
		void FindPolar(void);
		void FindScaleSweep(void);
		void FindScaleRadius(void);

		// Implementation
		void DrawLegend(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Quad Dial Gauge
//

class CPrimLegacyQuadDial : public CPrimLegacyDial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyQuadDial(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		void FindDialRect(void);
		void FindPolar(void);
		void FindScaleSweep(void);
		void FindScaleRadius(void);

		// Implementation
		void DrawLegend(IGDI *pGDI);
	};

// End of File

#endif
