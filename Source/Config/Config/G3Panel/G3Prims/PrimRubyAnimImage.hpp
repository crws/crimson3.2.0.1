
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyAnimImage_HPP
	
#define	INCLUDE_PrimRubyAnimImage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyRect.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Animated Image Primitive
//

class CPrimRubyAnimImage : public CPrimRubyRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyAnimImage(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void SetImage(UINT n, PCTXT pFile);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	     m_Keep;
		UINT         m_Count;
		CCodedItem * m_pValue;
		CCodedItem * m_pColor;
		CCodedItem * m_pShow;
		CPrimImage * m_pImage[10];
		CRect	     m_Margin;
	
	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		R2   GetImageRect(void);
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
