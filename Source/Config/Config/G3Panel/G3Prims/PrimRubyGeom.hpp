
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGeom_HPP
	
#define	INCLUDE_PrimRubyGeom_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyTankFill;
class CPrimRubyPenEdge;

//////////////////////////////////////////////////////////////////////////
//
// Ruby Geometric Primitive
//

class CPrimRubyGeom : public CPrimRubyWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGeom(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		BOOL StepAddress(void);
		void GetRefs(CPrimRefList &Refs);

		// Data Members
		CPrimRubyTankFill * m_pFill;
		CPrimRubyPenEdge  * m_pEdge;

	protected:
		// Data Members
		CRubyPath    m_pathFill;
		CRubyPath    m_pathEdge;
		CRubyPath    m_pathTrim;
		CRubyGdiList m_listFill;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listTrim;

		// Meta Data
		void AddMetaData(void);

		// Fast Fill Control
		virtual BOOL UseFastFill(void);

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);
	};

// End of File

#endif
