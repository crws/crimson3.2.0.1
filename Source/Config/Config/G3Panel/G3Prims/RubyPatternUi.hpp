
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyPatternUI_HPP
	
#define	INCLUDE_RubyPatternUI_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUITextRubyPattern;
class CUIRubyPattern;
class CRubyPatternComboBox;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element for Ruby Pattern
//

class CUITextRubyPattern : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextRubyPattern(void);

	protected:
		// Data Members
		CRubyPatternLib * m_pLib;

		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element for Ruby Pattern
//

class CUIRubyPattern : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIRubyPattern(void);

	protected:
		// Data Members
		CString	               m_Label;
		CLayItemText         * m_pTextLayout;
		CLayItem             * m_pDataLayout;
		CStatic	             * m_pTextCtrl;
		CRubyPatternComboBox * m_pDataCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Implementation
		CRect FindComboRect(void);
		void  LoadList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Combo Box
//

class CRubyPatternComboBox : public CDropComboBox
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRubyPatternComboBox(CUIElement *pUI);

		// Operations
		void LoadList(void);
		void SetFore(COLOR Fore);
		void SetBack(COLOR Back);

	protected:
		// Data Members
		CRubyPatternLib * m_pLib;
		COLOR             m_Fore;
		COLOR	          m_Back;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);
		void OnChar(UINT uCode, DWORD dwFlags);
	};

// End of File

#endif
