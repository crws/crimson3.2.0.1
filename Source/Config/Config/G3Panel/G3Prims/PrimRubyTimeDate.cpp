
#include "intern.hpp"

#include "PrimRubyTimeDate.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Time Date Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyTimeDate, CPrimRubyDataBox);

// Constructor

CPrimRubyTimeDate::CPrimRubyTimeDate(void)
{
	}

// Overridables

void CPrimRubyTimeDate::SetInitState(void)
{
	CPrimRubyDataBox::SetInitState();
	
	m_pDataItem->SetTimeDate();

	CString Sizing = m_pDataItem->GetSizingText();

	int     nWidth = m_pDataItem->GetTextWidth(Sizing);

	SetInitSize(8 + nWidth);
	}

// Meta Data

void CPrimRubyTimeDate::AddMetaData(void)
{
	CPrimRubyDataBox::AddMetaData();

	Meta_SetName((IDS_TIME_AND_DATE));
	}

// End of File
