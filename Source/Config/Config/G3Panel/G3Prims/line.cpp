
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Line
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLine, CPrim);

// Static Data

CPoint CPrimLine::m_HitPos;

BOOL   CPrimLine::m_fHit;

// Constructor

CPrimLine::CPrimLine(void)
{
	// LATER -- Add support for width, style and arrow heads. Note
	// that some combinations won't work. Thick styled lines don't
	// work nicely with GDI2 right now. Also, note that the last
	// pixel that we draw by hand might need to be an end cap...

	m_uType = 0x0E;

	m_pPen  = New CPrimPen;
	}

// Overridables

BOOL CPrimLine::HitTest(P2 Pos)
{
	CRect Rect = GetNormRect();

	if( PtInRect(Rect + 2, CPoint(Pos.x, Pos.y)) ) {

		Rect       = GetRect();

		CPoint p1  = Rect.GetTopLeft();

		CPoint p2  = Rect.GetBottomRight();
		
		m_HitPos.x = Pos.x;

		m_HitPos.y = Pos.y;
	
		m_fHit     = FALSE;
	
		LineDDA(p1.x, p1.y, p2.x, p2.y, FARPROC(HitDDA), LPARAM(this));
	
		HitTestPoint(p2.x, p2.y);
	
		return m_fHit;
		}
		
	return FALSE;
	}

void CPrimLine::Draw(IGDI *pGDI, UINT uMode)
{
	m_pPen->Configure(pGDI);

	pGDI->DrawLine(PassRect(m_DrawRect));
	}

void CPrimLine::SetInitState(void)
{
	m_pPen->Set(GetRGB(31,31,31));
	}

// Meta Data

void CPrimLine::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddObject(Pen);

	Meta_SetName((IDS_LINE));
	}

// Download Support

BOOL CPrimLine::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pPen);

	return TRUE;
	}

// Implementation

void CPrimLine::HitTestPoint(int x, int y)
{
	if( !m_fHit ) {
	
		int cx = m_HitPos.x - x;
		
		int cy = m_HitPos.y - y;
		
		if( cx >= -1 && cx <= +1 ) {

			if( cy >= -2 && cy <= +2 ) {
			
				m_fHit = TRUE;
				}
			}
		}
	}

// DDA Callback

void CALLBACK HitDDA(int x, int y, LPARAM p)
{
	((CPrimLine *) p)->HitTestPoint(x, y);
	}

// End of File

