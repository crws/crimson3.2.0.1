
#include "intern.hpp"

#include "PrimRubyWithHandle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Oriented Primitive with 1 Handle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyWithHandle, CPrimRubyOriented);

// Constructor

CPrimRubyWithHandle::CPrimRubyWithHandle(void)
{
	m_Orient = 2;

	m_Pos1   = 2500;

	m_Max1   = 10000;
	}

// Overridables

BOOL CPrimRubyWithHandle::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		UINT uInset = 0;

		Hand.m_pTag = L"Pos1";

		switch( m_Orient ) {

			case 0: // Right
				Hand.m_uRef  = 101;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos1;
				Hand.m_Clip  = CRect(uInset, 0, uInset, m_Max1);
				break;

			case 1: // Left
			case 4: // Arbitary
				Hand.m_uRef  = 103;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos1;
				Hand.m_Clip  = CRect(uInset, 0, uInset, m_Max1);
				break;

			case 2: // Up
				Hand.m_uRef  = 100;
				Hand.m_Pos.x = m_Pos1;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(0, uInset, m_Max1, uInset);
				break;

			case 3: // Down
				Hand.m_uRef  = 102;
				Hand.m_Pos.x = m_Pos1;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(0, uInset, m_Max1, uInset);
				break;
			}

		Hand.m_uRef ^= m_Reflect;

		return TRUE;
		}

	return FALSE;
	}

// Meta Data

void CPrimRubyWithHandle::AddMetaData(void)
{
	CPrimRubyOriented::AddMetaData();

	Meta_AddInteger(Pos1);

	Meta_SetName((IDS_RUBY_ORIENTED_2));
	}

// End of File
