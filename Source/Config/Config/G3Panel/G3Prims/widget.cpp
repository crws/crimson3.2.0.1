
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Page
//

class CPrimWidgetPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimWidgetPage(CPrimWidget *pWidget);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimWidget         * m_pWidget;
		CPrimWidgetPropList * m_pList;
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimWidgetPage, CUIPage);

// Constructor

CPrimWidgetPage::CPrimWidgetPage(CPrimWidget *pWidget)
{
	m_pWidget = pWidget;

	m_pList   = pWidget->m_pData->m_pList;

	m_Title   = CString(IDS_DATA);
	}

// Operations

BOOL CPrimWidgetPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	BOOL fShow = FALSE;

	if( m_pList->GetItemCount() ) {

		for( INDEX n = m_pList->GetHead(); !m_pList->Failed(n); m_pList->GetNext(n) ) {

			CPrimWidgetProp *pItem = m_pList->GetItem(n);

			if( !(pItem->m_Flags & 32) ) {

				if( !fShow ) {

					pView->StartTable(CString(IDS_VALUES), 1);

					pView->AddColHead(CString(IDS_VALUE));

					fShow = TRUE;
					}

				if( pItem->m_Desc.IsEmpty() ) {

					pView->AddRowHead(pItem->m_Name);
					}
				else
					pView->AddRowHead(pItem->m_Desc);

				CUIData UIData;

				UIData.m_Tag   = L"Value";

				UIData.m_Label = L"Label";

				if( pItem->m_Type <= 1 ) {

					UIData.m_ClassText = AfxNamedClass(L"CUITextCoded");

					UIData.m_ClassUI   = AfxNamedClass(L"CUIExpression");
					}

				if( pItem->m_Type == 2 ) {

					if( !(pItem->m_Flags & 15) ) {

						UIData.m_ClassText = AfxNamedClass(L"CUITextIntlString");

						UIData.m_ClassUI   = AfxNamedClass(L"CUIIntlString");

						UIData.m_Format    = L"30";
						}
					else {
						UIData.m_ClassText = AfxNamedClass(L"CUITextCoded");

						UIData.m_ClassUI   = AfxNamedClass(L"CUIExpression");
						}
					}

				if( pItem->m_Type == 3 ) {

					UIData.m_ClassText = AfxNamedClass(L"CUITextVirtualColor");

					UIData.m_ClassUI   = AfxNamedClass(L"CUIColor");

					UIData.m_Format    = L"n";
					}

				if( pItem->m_Type == 4 ) {

					// LATER -- Is this okay? It stops you using parent widget pages.

					UIData.m_ClassText = AfxNamedClass(L"CUITextDispPage");

					UIData.m_ClassUI   = AfxNamedClass(L"CUIDropPick");

					UIData.m_Format    = L"n";
					}

				if( pItem->m_Type == 5 ) {

					UIData.m_ClassText = AfxNamedClass(L"CUITextCoded");

					UIData.m_ClassUI   = AfxNamedClass(L"CUIAction");
					}

				pView->AddUI(pItem, L"root", &UIData);
				}
			}

		if( fShow ) {

			pView->EndTable();
			}
		}

	if( !fShow ) {

		pView->StartGroup(CString(IDS_VALUES), 1);

		pView->AddNarrative(CString(IDS_THIS_WIDGET_HAS));

		pView->EndGroup(TRUE);
		}

	pView->StartGroup(CString(IDS_DEFINITION), 1);

	pView->AddButton( CString(IDS_EDIT),
			  CString(IDS_EDIT_DATA),
			  L"ButtonEditProps"
			  );

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWidget, CPrimGroup);

// Constructor

CPrimWidget::CPrimWidget(void)
{
	m_uType     = 0x0D;

	m_LockData  = 0;

	m_Desc      = CString(IDS_UNTITLED_WIDGET);

	m_Cat       = CString(IDS_WIDGETS);

	m_pActions  = New CPrimWidgetActions;

	m_pData     = New CPrimWidgetPropData;

	m_pServer   = NULL;
	}

// UI Overridables

BOOL CPrimWidget::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CPrimWidgetPage(this));

	pList->Append(New CUIStdPage(CString(IDS_ACTIONS), AfxPointerClass(this), 4));

	pList->Append(New CUIStdPage(CString(IDS_SHOW),    AfxPointerClass(this), 3));

	pList->Append(New CUIStdPage(CString(IDS_FILING),  AfxPointerClass(this), 1));

	pList->Append(New CUIStdPage(CString(IDS_LOCKING), AfxPointerClass(this), 2));

	return TRUE;
	}

void CPrimWidget::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "ButtonEditProps" ) {

		CItemDialog Dlg(m_pData, CString(IDS_WIDGET_DATA));

		CSystemWnd  &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

		if( System.ExecSemiModal(Dlg) ) {

			m_pData->Recompile();
			}

		pHost->RemakeUI();
		}

	CPrimGroup::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimWidget::IsBound(void) const
{
	return m_pData->GetBindState();
	}

BOOL CPrimWidget::HasZoomPages(void) const
{
	return m_pData->m_AutoZoom && !m_pData->m_Zoom.IsEmpty();
	}

CString CPrimWidget::GetDesc(void) const
{
	if( !m_File.IsEmpty() ) {

		if( m_Desc == CString(IDS_UNTITLED_WIDGET) ) {

			return m_File;
			}

		if( m_Desc.IsEmpty() ) {

			return m_File;
			}
		}

	return m_Desc;
	}

// Operations

void CPrimWidget::SetDataServer(CDataServer *pServer)
{
	m_pServer = pServer;

	m_pAbove  = GetDataServer(pServer);
	}

// Overridables

UINT CPrimWidget::GetBindMode(void)
{
	if( m_pData->m_AutoBind ) {

		return 2;
		}

	if( m_pData->m_Count == 1 ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(0U);

		if( pItem->IsTagRef() ) {

			return 1;
			}
		}

	return 0;
	}

BOOL CPrimWidget::GetBindState(void)
{
	if( m_pData->GetBindState() ) {

		return TRUE;
		}

	return CPrimGroup::GetBindState();
	}

BOOL CPrimWidget::GetBindClass(CString &Class)
{
	Class = m_pData->m_Class;

	return TRUE;
	}

BOOL CPrimWidget::BindToTag(CString Top, CString Tag)
{
	BOOL fOkay = TRUE;

	if( m_pData->m_AutoBind ) {

		if( !m_pData->m_SubItem.IsEmpty() ) {

			CString Sub = m_pData->m_SubItem;

			while( Sub[0] == '^' ) {

				Sub.Delete(0, 1);

				UINT uPos = Tag.FindRev('.');

				if( uPos < NOTHING ) {

					Tag = Tag.Left(uPos);
					}
				else
					Tag.Empty();
				}

			if( !Sub.IsEmpty() ) {

				if( !Tag.IsEmpty() ) {

					Tag += L'.';
					}

				Tag += Sub;
				}
			}

		if( !m_pData->Bind(Top, Tag) ) {

			fOkay = FALSE;
			}

		if( !CPrimGroup::BindToTag(Top, Tag) ) {

			fOkay = FALSE;
			}

		return fOkay;
		}

	if( m_pData->m_Count == 1 ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(0U);

		if( pItem->IsTagRef() ) {

			if( !pItem->Bind(Tag, FALSE) ) {

				fOkay = FALSE;
				}
			}

		return fOkay;
		}

	return TRUE;
	}

void CPrimWidget::ClearBinding(void)
{
	m_pData->ClearBinding();

	CPrimGroup::ClearBinding();
	}

BOOL CPrimWidget::StepAddress(void)
{
	// TODO -- Step widget data?

	return FALSE;
	}

// Base Methods

UINT CPrimWidget::Release(void)
{
	return 0;
	}

// Name Server Methods

BOOL CPrimWidget::FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	UINT c = m_pData->m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(n);

		if( pItem->m_Name == Name ) {

			if( !pItem->IsAction() ) {

				ID = WORD(0xFF80 + n);

				pItem->GetTypeData(Type);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPrimWidget::NameIdent(WORD ID, CString &Name)
{
	UINT c = m_pData->m_pList->GetItemCount();

	if( ID >= 0xFF80 && ID < 0xFF80 + c ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(ID - 0xFF80);

		if( pItem ) {

			if( !pItem->IsAction() ) {

				Name = pItem->m_Name;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPrimWidget::FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type)
{
	UINT c = m_pData->m_pList->GetItemCount();

	if( ID >= 0xFF80 && ID < 0xFF80 + c ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(ID - 0xFF80);

		if( pItem ) {
			
			if( pItem->IsTagRef() ) {

				UINT Data = pItem->GetTagType();

				if( CUINameServer::FindTagProp(Name, Data, PropID, Type) ) {

					return TRUE;
					}
				}

			if( pItem->IsPageRef() ) {

				if( CUINameServer::FindPageProp(Name, PropID, Type) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CPrimWidget::NameProp(WORD ID, WORD PropID, CString &Name)
{
	UINT c = m_pData->m_pList->GetItemCount();

	if( ID >= 0xFF80 && ID < 0xFF80 + c ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(ID - 0xFF80);

		if( pItem ) {
			
			if( pItem->IsTagRef() ) {

				if( CUINameServer::NameTagProp(PropID, Name) ) {
				
					return TRUE;
					}
				}

			if( pItem->IsPageRef() ) {

				if( CUINameServer::NamePageProp(PropID, Name) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CPrimWidget::FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount)
{
	UINT c = m_pData->m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(n);

		if( pItem->m_Name == Name ) {

			if( pItem->IsAction() ) {

				ID     = WORD(0x7F80 + n);

				uCount = 0;

				Type.m_Type  = typeVoid;

				Type.m_Flags = flagActive;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPrimWidget::NameFunction(WORD ID, CString &Name)
{
	UINT c = m_pData->m_pList->GetItemCount();

	if( ID >= 0x7F80 && ID < 0x7F80 + c ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(ID - 0x7F80);

		if( pItem ) {

			if( pItem->IsAction() ) {

				Name = pItem->m_Name;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPrimWidget::EditFunction(WORD &ID, CTypeDef &Type)
{
	return FALSE;
	}

BOOL CPrimWidget::GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type)
{
	return FALSE;
	}

// Data Server Methods

DWORD CPrimWidget::GetData(DWORD ID, UINT Type, UINT Flags)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.x.m_HasBit ) {

		Ref.x.m_HasBit = 0;

		DWORD dwMask   = (1 << Ref.b.m_BitRef);

		DWORD dwData   = GetData(ID, Type, Flags);

		if( dwData & dwMask ) {

			return TRUE;
			}

		return FALSE;
		}

	if( Ref.x.m_IsTag ) {

		UINT c     = m_pData->m_pList->GetItemCount();

		UINT Index = Ref.t.m_Index;

		if( Index >= 0x7F80 && Index < 0x7F80 + c ) {

			CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(Index - 0x7F80);

			if( pItem ) {
				
				if( pItem->m_pValue ) {

					if( Type == typeString ) {

						CCodedText *pText = (CCodedText *) pItem->m_pValue;

						return DWORD(wstrdup(pText->GetText()));
						}
					
					return pItem->m_pValue->Execute(Type);
					}
				}

			if( Type == typeInteger ) {

				return 25;
				}

			if( Type == typeReal ) {

				return R2I(25);
				}

			return GetNull(Type);
			}
		}

	return m_pServer->GetData(ID, Type, Flags);
	}

void CPrimWidget::SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data)
{
	m_pServer->SetData(ID, Type, Flags, Data);
	}

DWORD CPrimWidget::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	UINT Index = (ID & 0x7FFF);

	UINT c     = m_pData->m_pList->GetItemCount();

	if( Index >= 0x7F80 && Index < 0x7F80 + c ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(Index - 0x7F80);

		if( pItem ) {
			
			if( pItem->m_pValue ) {

				if( !pItem->m_pValue->IsBroken() ) {

					if( pItem->IsTagRef() ) {
					
						ID = pItem->m_pValue->GetTagRef();

						return m_pAbove->GetProp(ID, Prop, Type);
						}

					if( pItem->IsPageRef() ) {

						ID = pItem->m_pValue->GetObjectRef();

						return m_pAbove->GetProp(ID, Prop, Type);
						}
					}
				}
			}
		}

	return m_pServer->GetProp(ID, Prop, Type);
	}

PVOID CPrimWidget::GetItem(DWORD ID)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.x.m_IsTag ) {

		UINT c     = m_pData->m_pList->GetItemCount();

		UINT Index = Ref.t.m_Index;

		if( Index >= 0x7F80 && Index < 0x7F80 + c ) {

			CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(Index - 0x7F80);

			if( pItem ) {

				CCodedItem *pValue = pItem->m_pValue;

				if( pValue ) {

					if( pValue->IsTagRef() ) {

						Ref.t.m_Index = pValue->GetTagIndex();

						return m_pAbove->GetItem(ID);
						}
					}
				}

			return NULL;
			}
		}

	return m_pServer->GetItem(ID);
	}

DWORD CPrimWidget::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	return m_pServer->RunFunc(ID, uParam, pParam);
	}

// Download Support

BOOL CPrimWidget::MakeInitData(CInitData &Init)
{
	CPrimGroup::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pActions->m_pOnSelect);

	Init.AddItem(itemVirtual, m_pActions->m_pOnRemove);

	Init.AddItem(itemVirtual, m_pActions->m_pOnUpdate);

	Init.AddItem(itemVirtual, m_pActions->m_pOnSecond);

	Init.AddItem(itemSimple, m_pData);

	return TRUE;
	}

// Item Naming

CString CPrimWidget::GetHumanName(void) const
{
	CString    Name  = m_Desc.IsEmpty() ? CString(IDS_WIDGET) : m_Desc;

	CPrimList *pList = (CPrimList *) GetParent();

	Name += CPrintf(L" %u", pList->GetItemIndex(this));

	return Name;
	}

// Meta Data

void CPrimWidget::AddMetaData(void)
{
	CPrimGroup::AddMetaData();

	Meta_AddInteger(LockData);
	Meta_AddString (Cat);
	Meta_AddString (Desc);
	Meta_AddString (File);
	Meta_AddObject (Actions);
	Meta_AddObject (Data);

	Meta_SetName((IDS_WIDGET));
	}

// Implementation

void CPrimWidget::DoEnables(IUIHost *pHost)
{
	UINT c = m_pData->m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pData->m_pList->GetItem(n);

		pHost->EnableUI(pItem, "Value", m_LockData <= 1);
		}

	pHost->EnableUI("ButtonEditProps", m_LockData == 0);

	pHost->EnableUI("Desc", m_LockData == 0);

	pHost->EnableUI("Cat",  m_LockData == 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Actions
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWidgetActions, CCodedHost);

// Constructor

CPrimWidgetActions::CPrimWidgetActions(void)
{
	m_pOnSelect = NULL;

	m_pOnRemove = NULL;

	m_pOnUpdate = NULL;

	m_pOnSecond = NULL;
	}

// UI Overridables

void CPrimWidgetActions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimWidgetActions::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.StartsWith(L"On") ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Server Access

INameServer * CPrimWidgetActions::GetNameServer(CNameServer *pName)
{
	CPrimWidget *pWidget = (CPrimWidget *) GetParent();

	pName->SetFunc (pWidget);

	pName->SetIdent(pWidget);

	return pName;
	}

IDataServer * CPrimWidgetActions::GetDataServer(CDataServer *pData)
{
	CPrimWidget *pWidget = (CPrimWidget *) GetParent();

	pWidget->SetDataServer(pData);

	return pWidget;
	}

// Download Support

BOOL CPrimWidgetActions::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimWidgetActions::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddVirtual(OnSelect);
	Meta_AddVirtual(OnRemove);
	Meta_AddVirtual(OnUpdate);
	Meta_AddVirtual(OnSecond);

	Meta_SetName((IDS_WIDGET_ACTIONS));
	}

// End of File
