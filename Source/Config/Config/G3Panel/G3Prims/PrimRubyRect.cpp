
#include "intern.hpp"

#include "PrimRubyRect.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Rectangle Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyRect, CPrimRubyGeom);

// Constructor

CPrimRubyRect::CPrimRubyRect(void)
{
	}

// Overridables

void CPrimRubyRect::FindTextRect(void)
{
	m_TextRect = m_DrawRect;

	int nAdjust = m_pEdge->GetInnerWidth();

	DeflateRect(m_TextRect, nAdjust, nAdjust);
	}

// Meta Data

void CPrimRubyRect::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_SetName((IDS_RECTANGLE_2));
	}

// Path Management

void CPrimRubyRect::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyDraw::Rectangle( m_pathFill,
			      CRubyPoint(Rect, 1),
			      CRubyPoint(Rect, 4)
			      );

	CPrimRubyGeom::MakePaths();
	}

// End of File
