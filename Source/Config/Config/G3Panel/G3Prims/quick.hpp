
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_QUICK_HPP
	
#define	INCLUDE_QUICK_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "legacy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimQuickPlot;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Quick Plot
//

class CPrimQuickPlot : public CPrimLegacyFigure
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimQuickPlot(void);

		// Attributes
		BOOL IsTagRef(void) const;
		BOOL GetTagItem(CTag * &pTag) const;

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pTag;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		UINT         m_Align;
		UINT         m_Pad;
		CPrimColor * m_pPen;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif

