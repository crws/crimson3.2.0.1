
#include "intern.hpp"

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Geometric Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyWithText, CPrimWithText);

// Constructor

CPrimRubyWithText::CPrimRubyWithText(void)
{
	}

// Overridables

BOOL CPrimRubyWithText::HitTest(P2 Pos)
{
	R2 rect;

	if( m_pTextItem ) {

		if( !m_pTextItem->GetBoundingRect(rect) ) {

			rect = m_DrawRect;
			}

		if( PtInRect(rect, Pos) ) {

			return TRUE;
			}
		}

	if( m_pDataItem ) {

		if( !m_pDataItem->GetBoundingRect(rect) ) {

			rect = m_DrawRect;
			}

		if( PtInRect(rect, Pos) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

CRect CPrimRubyWithText::GetBoundingRect(void)
{
	CRect Rect;

	Rect.left   = m_bound.x1;
	Rect.top    = m_bound.y1;
	Rect.right  = m_bound.x2;
	Rect.bottom = m_bound.y2;

	return Rect;
	}

void CPrimRubyWithText::SetHand(BOOL fInit)
{
	CPrimWithText::SetHand(fInit);

	UpdateLayout();
	}

void CPrimRubyWithText::UpdateLayout(void)
{
	CPrimWithText::UpdateLayout();

	InitPaths();

	MakePaths();

	MakeLists();
	}

// Download Support

BOOL CPrimRubyWithText::MakeInitData(CInitData &Init)
{
	CPrimWithText::MakeInitData(Init);

	Init.AddWord(WORD(m_bound.x1));
	Init.AddWord(WORD(m_bound.y1));
	Init.AddWord(WORD(m_bound.x2));
	Init.AddWord(WORD(m_bound.y2));

	return TRUE;
	}

// Implementation

BOOL CPrimRubyWithText::AddList(CInitData &Init, CRubyGdiList const &list)
{
	UINT n;

	if( list.m_pCount ) {

		Init.AddWord(WORD(list.m_uCount));

		for( n = 0; n < list.m_uCount; n++ ) {

			Init.AddWord(WORD(list.m_pCount[n]));
			}

		Init.AddWord(WORD(list.m_uList));

		for( UINT n = 0; n < list.m_uList; n++ ) {

			P2 const &p = list.m_pList[n];

			Init.AddWord(WORD(p.x));
		
			Init.AddWord(WORD(p.y));
			}

		return TRUE;
		}

	Init.AddWord(0);

	Init.AddWord(0);

	return FALSE;
	}

// Path Management

void CPrimRubyWithText::InitPaths(void)
{
	}

void CPrimRubyWithText::MakePaths(void)
{
	}

void CPrimRubyWithText::MakeLists(void)
{
	}

// End of File
