
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyStar5_HPP
	
#define	INCLUDE_PrimRubyStar5_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyStar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 5-Pointed Star Primitive
//

class CPrimRubyStar5 : public CPrimRubyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyStar5(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
