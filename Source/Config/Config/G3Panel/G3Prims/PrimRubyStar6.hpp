
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyStar6_HPP
	
#define	INCLUDE_PrimRubyStar6_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyStar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 6-Pointed Star Primitive
//

class CPrimRubyStar6 : public CPrimRubyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyStar6(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
