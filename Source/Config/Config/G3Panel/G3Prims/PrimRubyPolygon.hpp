
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPolygon_HPP
	
#define	INCLUDE_PrimRubyPolygon_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Polygon Primitive
//

class CPrimRubyPolygon : public CPrimRubyGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPolygon(void);

		// Data Members
		INT m_Rotate;
		INT m_AltRad;

	protected:
		// Data Members
		INT m_nSides;

		// Meta Data
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);
	};

// End of File

#endif
