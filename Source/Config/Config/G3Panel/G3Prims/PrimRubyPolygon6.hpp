
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPolygon6_HPP
	
#define	INCLUDE_PrimRubyPolygon6_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPolygon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 6-Sided Polygon Primitive
//

class CPrimRubyPolygon6 : public CPrimRubyPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPolygon6(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
