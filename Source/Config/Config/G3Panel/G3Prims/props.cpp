
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Page Properties Page
//

class CDispPagePropsPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDispPagePropsPage(CString Title, CDispPageProps *pProps, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CDispPageProps * m_pProps;

		// Implementation
		BOOL LoadSecurity(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Page Properties Page
//

// Runtime Class

AfxImplementRuntimeClass(CDispPagePropsPage, CUIStdPage);

// Constructor

CDispPagePropsPage::CDispPagePropsPage(CString Title, CDispPageProps *pProps, UINT uPage)
{
	m_pProps = pProps;

	m_Title  = Title;

	m_Class  = AfxPointerClass(pProps);
	
	m_uPage  = uPage;
	}

// Operations

BOOL CDispPagePropsPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	if( m_uPage == 4 ) {

		LoadSecurity(pView);

		pView->NoRecycle();
		}

	return TRUE;
	}

// Implementation

BOOL CDispPagePropsPage::LoadSecurity(IUICreate *pView)
{
	CSecDesc *pSec = m_pProps->m_pSec;

	if( pSec ) {
	
		CUIPageList List;
		
		List.Append(New CUIStdPage(AfxPointerClass(pSec), 2));

		List[0]->LoadIntoView(pView, pSec);
		
		return TRUE;
		}

	return FALSE;
	}
				 
//////////////////////////////////////////////////////////////////////////
//
// Page Properties
//

// Dynamic Class

AfxImplementDynamicClass(CDispPageProps, CCodedHost);

// Constructor

CDispPageProps::CDispPageProps(void)
{
	m_pLabel     = NULL;
	m_pBack      = New CPrimColor;
	m_pMaster    = NULL;
	m_pOnSelect  = NULL;
	m_pOnRemove  = NULL;
	m_pOnUpdate  = NULL;
	m_pOnSecond  = NULL;
	m_pOnTimeout = NULL;
	m_pPageNext  = NULL;
	m_pPagePrev  = NULL;
	m_pPageExit  = NULL;
	m_AutoEntry  = 0;
	m_EntryOrder = 0;
	m_NoBack     = 0;
	m_Timeout    = 0;
	m_Update     = 0;
	m_PopAlign   = 0;
	m_PopAlignH  = 2;
	m_PopAlignV  = 2;
	m_PopMaster  = 1;
	m_PopKeypad  = 0;
	m_pSec       = New CSecDesc;
	}

// UI Creation

BOOL CDispPageProps::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CDispPagePropsPage(CString(IDS_GENERAL),  this, 1));
	
	pList->Append(New CDispPagePropsPage(CString(IDS_MORE),     this, 2));
	
	pList->Append(New CDispPagePropsPage(CString(IDS_ACTIONS),  this, 3));

	pList->Append(New CDispPagePropsPage(CString(IDS_SECURITY), this, 4));

	return TRUE;
	}

// UI Change

void CDispPageProps::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Master" ) {

		pHost->EnableUI("Back", !m_pMaster);
		}

	if( Tag.IsEmpty() || Tag == "PopAlign" ) {

		pHost->EnableUI("PopAlignH", m_PopAlign);

		pHost->EnableUI("PopAlignV", m_PopAlign);

		pHost->EnableUI("PopKeypad", m_PopAlign);
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispPageProps::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(2) == "On" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	if( Tag == "Label" || Tag == "Desc" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Master" || Tag.Left(4) == "Page" ) {

		Type.m_Type  = typePage;

		Type.m_Flags = flagConstant;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Operations

void CDispPageProps::Validate(BOOL fExpand)
{
	}

// Persistance

void CDispPageProps::Init(void)
{
	CCodedHost::Init();

	m_pBack->Set(0);
	}

// Download Support

BOOL CDispPageProps::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pLabel);

	Init.AddText(m_Desc);
	
	Init.AddItem(itemSimple,  m_pSec);
	
	Init.AddItem(itemSimple,  m_pBack);

	AddPageHandle(Init, m_pMaster);

	Init.AddItem(itemVirtual, m_pOnSelect);
	Init.AddItem(itemVirtual, m_pOnRemove);
	Init.AddItem(itemVirtual, m_pOnUpdate);
	Init.AddItem(itemVirtual, m_pOnSecond);
	Init.AddItem(itemVirtual, m_pOnTimeout);

	AddPageHandle(Init, m_pPageNext);
	AddPageHandle(Init, m_pPagePrev);
	AddPageHandle(Init, m_pPageExit);

	Init.AddByte(BYTE(m_AutoEntry));
	Init.AddByte(BYTE(m_EntryOrder));
	Init.AddByte(BYTE(m_NoBack));
	Init.AddWord(WORD(m_Timeout));
	Init.AddByte(BYTE(m_Update));
	Init.AddByte(BYTE(m_PopAlign));
	Init.AddByte(BYTE(m_PopAlignH));
	Init.AddByte(BYTE(m_PopAlignV));
	Init.AddByte(BYTE(m_PopMaster));
	Init.AddByte(BYTE(m_PopKeypad));

	return TRUE;
	}

// Meta Data

void CDispPageProps::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddVirtual(Label);
	Meta_AddString (Desc);
	Meta_AddObject (Back);
	Meta_AddVirtual(Master);
	Meta_AddVirtual(PageNext);
	Meta_AddVirtual(PagePrev);
	Meta_AddVirtual(PageExit);
	Meta_AddInteger(AutoEntry);
	Meta_AddInteger(EntryOrder);
	Meta_AddInteger(NoBack);
	Meta_AddVirtual(OnSelect);
	Meta_AddVirtual(OnRemove);
	Meta_AddVirtual(OnUpdate);
	Meta_AddVirtual(OnSecond);
	Meta_AddVirtual(OnTimeout);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(Update);
	Meta_AddInteger(PopAlign);
	Meta_AddInteger(PopAlignH);
	Meta_AddInteger(PopAlignV);
	Meta_AddInteger(PopMaster);
	Meta_AddInteger(PopKeypad);
	Meta_AddObject (Sec);

	Meta_SetName((IDS_DISPLAY_PAGE_2));
	}

// Implementation

void CDispPageProps::AddPageHandle(CInitData &Init, CCodedItem *pItem)
{
	if( pItem ) {
		
		if( !pItem->IsBroken() ) {

			UINT hItem = pItem->GetObjectRef();

			if( hItem ) {

				CDispPage *pPage = (CDispPage *) GetDatabase()->GetHandleItem(hItem);

				if( pPage ) {
					
					if( pPage->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

						Init.AddWord(WORD(hItem));

						return;
						}
					}
				}
			}
		}

	Init.AddWord(WORD(0));
	}

// End of File
