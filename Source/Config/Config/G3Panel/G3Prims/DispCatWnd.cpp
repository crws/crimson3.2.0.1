
#include "intern.hpp"

#include "DispCatWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ScratchPadWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Page Category Window
//

// Dynamic Class

AfxImplementDynamicClass(CDispCatWnd, CViewWnd);

// Constructors

CDispCatWnd::CDispCatWnd(void)
{
	m_fCurrent = FALSE;

	m_fActive  = TRUE;

	m_fPageSel = FALSE;

	m_fScratch = FALSE;

	m_pScratch = NULL;

	m_pMain    = NULL;

	ReadConfig();
	}

// Destructors

CDispCatWnd::~CDispCatWnd(void)
{
	SaveConfig();
	}

// Attributes

BOOL CDispCatWnd::IsScratchPadActive(void) const
{
	return m_pScratch->IsActive();
	}

CScratchPadWnd * CDispCatWnd::GetScratchPadWnd(void) const
{
	return m_pScratch;
	}

CPageEditorWnd * CDispCatWnd::GetScratchPadEditor(void) const
{
	return (CPageEditorWnd *) m_pScratch->GetView();
	}

CPageEditorWnd * CDispCatWnd::GetMainEditor(void) const
{
	return (CPageEditorWnd *) m_pMain;
	}

// Operations

void CDispCatWnd::SetMainEditor(CViewWnd *pMain)
{
	m_pMain = pMain;
	}

void CDispCatWnd::PageSelected(BOOL fPage)
{
	m_fPageSel = fPage;

	ShowOrHideScratchPad();
	}

void CDispCatWnd::NavTreeActive(CRect const &Rect)
{
	m_pScratch->NavTreeActive(Rect);
	}

void CDispCatWnd::NavToScratch(void)
{
	m_pScratch->NavToScratch();
	}

// Overridables

void CDispCatWnd::OnAttach(void)
{
	m_pManager = (CUIManager *) m_pItem;

	m_pScratch = New CScratchPadWnd;

	m_pScratch->Attach(m_pManager->m_pScratch->GetItem(0u));
	}

// Message Map

AfxMessageMap(CDispCatWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_ACTIVATEAPP)
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_GLOBAL, OnGlobalCommand)
	
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)
	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchGetInfoType(IDM_VIEW, OnViewGetInfo)

	AfxMessageEnd(CDispCatWnd)
	};

// Accelerators

BOOL CDispCatWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CDispCatWnd::OnPostCreate(void)
{
	m_System.Bind(this);
	}

void CDispCatWnd::OnPreDestroy(void)
{
	m_pScratch->Destroy();

	m_pScratch = NULL;
	}

void CDispCatWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {
		
		Menu.MergeMenu(CMenu(L"DispCatMenu"));
		}
	}

void CDispCatWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	}

void CDispCatWnd::OnSetCurrent(BOOL fCurrent)
{
	m_fCurrent = fCurrent;

	ShowOrHideScratchPad();
	}

void CDispCatWnd::OnActivateApp(BOOL fActive, DWORD dwThread)
{
	m_fActive = fActive;

	ShowOrHideScratchPad();
	}

void CDispCatWnd::OnGoingIdle(void)
{
	m_pScratch->SendMessage(WM_GOINGIDLE);
	}

// Command Handlers

BOOL CDispCatWnd::OnGlobalCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GLOBAL_SEMI_START:

			m_pScratch->SetSemiModal(m_System.GetSemiModal());

			break;

		case IDM_GLOBAL_SEMI_END:

			m_pScratch->SetSemiModal(NULL);

			break;
		}

	return FALSE;
	}

BOOL CDispCatWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID == IDM_VIEW_SCRATCHPAD ) {

		Info.m_Image = MAKELONG(0x003E, 0x1000);
		}

	return FALSE;
	}

BOOL CDispCatWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_VIEW_SCRATCHPAD ) {

		Src.EnableItem(TRUE);

		Src.CheckItem (m_fEnable);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDispCatWnd::OnViewCommand(UINT uID)
{
	if( uID == IDM_VIEW_SCRATCHPAD ) {

		m_fEnable = !m_fEnable;

		ShowOrHideScratchPad();

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CDispCatWnd::ShowOrHideScratchPad(void)
{
	if( m_pScratch ) {

		BOOL fScratch = m_fActive && m_fCurrent && m_fPageSel && m_fEnable;

		if( m_fScratch != fScratch ) {

			m_pScratch->Show(fScratch);
		
			m_fScratch = fScratch;
			}
		}
	}

void CDispCatWnd::ReadConfig(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Scatchpad");

	m_fEnable = Reg.GetValue(L"Show", UINT(1));
	}

void CDispCatWnd::SaveConfig(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Scatchpad");

	Reg.SetValue(L"Show", DWORD(m_fEnable));
	}

// End of File
