
#include "intern.hpp"

#include "PrimRubyPenBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPenBase, CPrimRubyBrush);

// Constructor

CPrimRubyPenBase::CPrimRubyPenBase(void)
{
	m_Width = 1;

	m_Edge  = CRubyStroker::edgeCenter;

	m_Join  = CRubyStroker::joinMiter;
	}

// UI Managament

void CPrimRubyPenBase::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "Width" ) {

			DoEnables(pHost);
			}

		if( Tag == "Pattern" ) {

			DoEnables(pHost);
			}

		if( Tag == "Color1" ) {

			pHost->UpdateUI(this, "Pattern");
			}

		if( Tag == "Color2" ) {

			pHost->UpdateUI(this, "Pattern");
			}
		}

	CPrimRubyBrush::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimRubyPenBase::IsNull(void) const
{
	return m_Width == 0;
	}

int CPrimRubyPenBase::GetWidth(void) const
{
	return m_Width;
	}

int CPrimRubyPenBase::GetInnerWidth(void) const
{
	switch( m_Edge ) {

		case CRubyStroker::edgeCenter:
			
			return (m_Width + 1) / 2;

		case CRubyStroker::edgeInner:

			return m_Width;

		case CRubyStroker::edgeOuter:

			return 0;
		}

	return 0;
	}

int CPrimRubyPenBase::GetOuterWidth(void) const
{
	switch( m_Edge ) {

		case CRubyStroker::edgeCenter:
			
			return (m_Width + 1) / 2;

		case CRubyStroker::edgeOuter:

			return m_Width;

		case CRubyStroker::edgeInner:

			return 0;
		}

	return 0;
	}

// Stroking

BOOL CPrimRubyPenBase::StrokeExact(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		s.SetEdgeMode ((CRubyStroker::EdgeMode)  m_Edge);

		s.SetJoinStyle((CRubyStroker::JoinStyle) m_Join);

		s.StrokeLoop(output, figure, 0, m_Width);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyPenBase::StrokeEdge(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		switch( m_Width ) {
	
			case 1:
				s.SetEdgeMode((CRubyStroker::edgeOuter));

				s.SetJoinStyle((CRubyStroker::joinBevel));

				break;

			case 2:
				s.SetEdgeMode((CRubyStroker::edgeCenter));

				s.SetJoinStyle((CRubyStroker::joinBevel));

				break;

			default:
				s.SetEdgeMode ((CRubyStroker::EdgeMode)  m_Edge);

				s.SetJoinStyle((CRubyStroker::JoinStyle) m_Join);

				break;
			}

		s.StrokeLoop(output, figure, 0, m_Width);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyPenBase::StrokeOpen(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		switch( m_Width ) {
	
			case 1:
				s.SetEdgeMode((CRubyStroker::edgeOuter));

				s.SetJoinStyle((CRubyStroker::joinBevel));

				break;

			case 2:
				s.SetEdgeMode((CRubyStroker::edgeCenter));

				s.SetJoinStyle((CRubyStroker::joinBevel));

				break;

			default:
				s.SetEdgeMode ((CRubyStroker::EdgeMode)  m_Edge);

				s.SetJoinStyle((CRubyStroker::JoinStyle) m_Join);

				break;
			}

		s.SetEndStyle ((CRubyStroker::endFlat));

		s.StrokeOpen(output, figure, 0, m_Width);
		
		return TRUE;
		}

	return FALSE;
	}

// Persistance

void CPrimRubyPenBase::Init(void)
{
	CCodedHost::Init();

	m_pColor1->Set(GetRGB(31,31,31));

	m_pColor2->Set(GetRGB(12,12,24));

	m_Pattern = brushFore;
	}

// Download Support

BOOL CPrimRubyPenBase::MakeInitData(CInitData &Init)
{
	CPrimRubyBrush::MakeInitData(Init);

	Init.AddByte(BYTE(m_Width));
	Init.AddByte(BYTE(m_Edge));
	Init.AddByte(BYTE(m_Join));

	return TRUE;
	}

// Meta Data

void CPrimRubyPenBase::AddMetaData(void)
{
	CPrimRubyBrush::AddMetaData();

	Meta_AddInteger(Width);
	Meta_AddInteger(Edge);
	Meta_AddInteger(Join);

	Meta_SetName((IDS_PEN));
	}

// Implementation

void CPrimRubyPenBase::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Pattern", m_Width > 0);

	pHost->EnableUI(this, "Color1",  m_Width > 0 && m_Pattern > 0 && m_Pattern < 128);

	pHost->EnableUI(this, "Color2",  m_Width > 0 && m_Pattern > 2 && m_Pattern < 128);

	pHost->EnableUI(this, "Edge",    m_Width > 2);

	pHost->EnableUI(this, "Join",    m_Width > 1);
	}

// End of File
