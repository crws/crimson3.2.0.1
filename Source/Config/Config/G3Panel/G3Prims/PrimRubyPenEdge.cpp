
#include "intern.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPenEdge, CPrimRubyPenBase);

// Constructor

CPrimRubyPenEdge::CPrimRubyPenEdge(void)
{
	m_TrimWidth  = 0;

	m_TrimMode   = 0;

	m_pTrimColor = New CPrimColor;
	}

// UI Managament

void CPrimRubyPenEdge::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "Width" ) {

			DoEnables(pHost);
			}

		if( Tag == "TrimWidth" ) {

			DoEnables(pHost);
			}
		}

	CPrimRubyPenBase::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

int CPrimRubyPenEdge::GetInnerWidth(void) const
{
	int n = CPrimRubyPenBase::GetInnerWidth();
	
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		n += (m_TrimWidth + 1) / 2;
		}

	return n;
	}

int CPrimRubyPenEdge::GetOuterWidth(void) const
{
	int n = CPrimRubyPenBase::GetOuterWidth();
	
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		n += (m_TrimWidth + 1) / 2;
		}

	return n;
	}

// Operations

void CPrimRubyPenEdge::AdjustRect(R2 &r, UINT m)
{
	if( m_Width == 1 ) {

		if( m & 1 ) r.x1++;
		if( m & 2 ) r.y1++;
		if( m & 4 ) r.x2--;
		if( m & 8 ) r.y2--;
		}
	}

void CPrimRubyPenEdge::AdjustRect(R2 &r)
{
	if( m_Width ) {

		if( m_Width == 1 ) {

			DeflateRect(r, 1, 1);
			}
		else {
			if( m_Edge == CRubyStroker::edgeInner ) {

				DeflateRect(r, 1, 1);
				}

			if( m_Edge == CRubyStroker::edgeOuter ) {

				InflateRect(r, 1, 1);
				}
			}
		}
	}

// Stroking

BOOL CPrimRubyPenEdge::StrokeTrim(CRubyPath &output, CRubyPath const &figure)
{
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		CRubyStroker s;

		s.SetEdgeMode (CRubyStroker::edgeCenter);

		s.SetJoinStyle(CRubyStroker::joinBevel);

		s.StrokeLoop(output, figure, m_TrimMode, m_TrimWidth);

		return TRUE;
		}

	return FALSE;
	}

// Drawing

BOOL CPrimRubyPenEdge::Trim(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		CRubyGdiLink link(pGdi);

		if( fOver )
			link.OutputSolid(list, m_pTrimColor->GetColor(), 0);
		else
			link.OutputSolid(list, m_pTrimColor->GetColor());

		return TRUE;
		}

	return FALSE;
	}

// Persistance

void CPrimRubyPenEdge::Init(void)
{
	CPrimRubyPenBase::Init();

	m_pTrimColor->Set(GetRGB(15,15,15));
	}

// Download Support

BOOL CPrimRubyPenEdge::MakeInitData(CInitData &Init)
{
	CPrimRubyPenBase::MakeInitData(Init);
	
	Init.AddByte(BYTE(m_TrimWidth));

	Init.AddByte(BYTE(m_TrimMode));

	Init.AddItem(itemSimple, m_pTrimColor);

	return TRUE;
	}

// Meta Data

void CPrimRubyPenEdge::AddMetaData(void)
{
	CPrimRubyPenBase::AddMetaData();

	Meta_AddInteger(TrimWidth);
	Meta_AddInteger(TrimMode);
	Meta_AddObject (TrimColor);

	Meta_SetName(IDS_PEN);
	}

// Implementation

void CPrimRubyPenEdge::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "TrimWidth", m_Width >= 4);

	pHost->EnableUI(this, "TrimMode",  m_Width >= 4 && m_TrimWidth > 0);

	pHost->EnableUI(this, "TrimColor", m_Width >= 4 && m_TrimWidth > 0);
	}

// End of File
