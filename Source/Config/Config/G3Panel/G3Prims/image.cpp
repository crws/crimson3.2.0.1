
#include "intern.hpp"

#include "image.hpp"

#include "button.hpp"

#include "geom.hpp"

#include "PrimRubyShade.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Image
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImage, CUIItem);

// Constructor

CPrimImage::CPrimImage(void)
{
	m_Image = NOTHING;
	}

// Destructor

CPrimImage::~CPrimImage(void)
{
	}

// Creation

CSize CPrimImage::LoadFromFile(CFilename File, CSize MaxSize)
{
	m_Image = m_pImages->LoadFromFile(File, TRUE);

	m_Opts.m_Size = FindInitSize(MaxSize);

	return m_Opts.m_Size;
	}

CSize CPrimImage::LoadFromDataObject(IDataObject *pData, CSize MaxSize)
{
	m_Image = m_pImages->LoadFromDataObject(pData);

	m_Opts.m_Size = FindInitSize(MaxSize);

	return m_Opts.m_Size;
	}

CSize CPrimImage::LoadFromTreeFile(CTreeFile &Tree, CSize MaxSize, BOOL fFixup)
{
	m_Image = m_pImages->LoadFromTreeFile(Tree, FALSE);

	m_Opts.m_Size = FindInitSize(MaxSize);

	return m_Opts.m_Size;
	}

// Attributes

BOOL CPrimImage::CanAcceptDataObject(IDataObject *pData) const
{
	return m_pImages->CanAcceptDataObject(pData);
	}

// Operations

// cppcheck-suppress passedByValue

void CPrimImage::Draw(IGDI *pGDI, R2 Rect, UINT uMode, UINT rop2)
{
	Draw(pGDI, Rect, uMode, rop2, NULL);
	}

void CPrimImage::Draw(IGDI *pGDI, R2 Rect, UINT uMode, UINT rop2, CPrimRubyShade *pShade)
{
	int cx = Rect.x2 - Rect.x1;

	int cy = Rect.y2 - Rect.y1;

	if( cx > 0 && cy > 0 ) {

		int xp = Rect.x1;

		int yp = Rect.y1;

		int rx = cx;

		int ry = cy;

		if( m_Image < NOTHING ) {

			CImageFileItem *pFile = m_pImages->m_pFiles->GetItem(m_Image);

			if( pFile ) {

				CByteArray Data;

				if( pFile->IsTexture() ) {

					CSize Nat = pFile->GetNativeSize();

					if( Nat.cx && Nat.cy ) {

						m_Opts.m_Size = Nat;

						if( pGDI ) {

							CSize  Act = pFile->Render(Data, m_Opts, 32); // !!!!

							CPoint Pos;

							Pos.y = Rect.y1;

							while( Pos.y < Rect.y2 ) {

								Pos.x = Rect.x1;

								while( Pos.x < Rect.x2 ) {

									PCBYTE pData = Data.GetPointer();

									int rop1   = PWORD(pData)[0];

									int stride = PWORD(pData)[1];

									int px     = min(Act.cx, Rect.x2 - Pos.x);

									int py     = min(Act.cy, Rect.y2 - Pos.y);

									pGDI->BitBlt( Pos.x,
										      Pos.y,
										      px,
										      py,
										      stride,
										      pData + 8,
										      rop1 | rop2
										      );

									Pos.x += Act.cx;
									}

								Pos.y += Act.cy;
								}
							}
						}
					}
				else {
					m_Opts.m_Size = CSize(rx, ry);

					CSize Act     = pFile->Render(Data, m_Opts, 32); // !!!!

					if( Act.cx && Act.cy ) {

						if( pGDI ) {

							xp += (cx - Act.cx) / 2;

							yp += (cy - Act.cy) / 2;

							PCBYTE pData  = Data.GetPointer();

							int    rop1   = PWORD(pData)[0];

							int    stride = PWORD(pData)[1];

							if( pShade ) {

								PBYTE pCopy = new BYTE [ stride * Act.cy ];

								memcpy(pCopy, pData + 8, stride * Act.cy);

								pShade->Recolor(pGDI, PDWORD(pCopy), stride / 4, Act.cy);

								pGDI->BitBlt( xp,
									      yp,
									      Act.cx,
									      Act.cy,
									      stride,
									      pCopy,
									      rop1 | rop2
									      );

								delete [] pCopy;
								}
							else {
								pGDI->BitBlt( xp,
									      yp,
									      Act.cx,
									      Act.cy,
									      stride,
									      pData + 8,
									      rop1 | rop2
									      );
								}
							}

						m_Opts.m_Size = Act;
						}
					}
				}
			}
		}
	}

void CPrimImage::GetRefs(CPrimRefList &Refs)
{
	if( m_Image < NOTHING ) {

		Refs.Insert(m_Image | refImage);
		}
	}

void CPrimImage::EditRef(UINT uOld, UINT uNew)
{
	if( uOld == NOTHING ) {

		m_Image |= refPending;

		return;
		}

	if( (m_Image | refImage) == uOld ) {

		m_Image = uNew;
		}
	}

// Persistance

void CPrimImage::Init(void)
{
	FindManager();

	FindKeep();

	CUIItem::Init();
	}

void CPrimImage::PostLoad(void)
{
	FindManager();

	FindKeep();

	CUIItem::PostLoad();
	}

// Download Support

BOOL CPrimImage::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	AfxAssert(m_Opts.m_Size.cx);

	Init.AddWord(WORD(m_pImages->FindView(m_Image, m_Opts)));

	return TRUE;
	}

// Implementation

void CPrimImage::FindManager(void)
{
	CUISystem * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	m_pImages           = pSystem->m_pUI->m_pImages;
	}

BOOL CPrimImage::FindKeep(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pMeta = pItem->FindMetaData(L"Keep");

	if( pMeta ) {

		m_Opts.m_Keep = pMeta->ReadInteger(pItem);

		return TRUE;
		}

	return FALSE;
	}

CSize CPrimImage::FindInitSize(CSize MaxSize)
{
	if( m_Image < NOTHING ) {

		if( !m_pImages->IsTexture(m_Image) ) {

			CSize Size = m_pImages->GetImageNativeSize(m_Image);

			if( Size.cx > MaxSize.cx || Size.cy > MaxSize.cy ) {

				double rx = double(MaxSize.cx) / Size.cx;

				double ry = double(MaxSize.cy) / Size.cy;

				double rr = min(rx, ry);

				Size.cx = int(Size.cx * rr);

				Size.cy = int(Size.cy * rr);
				}

			return Size;
			}
		}

	return CSize(65, 65);
	}

// Meta Data Creation

void CPrimImage::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Image);
	
	Meta_Add(L"Size",    m_Opts.m_Size,   metaPoint  );
	Meta_Add(L"Rotate",  m_Opts.m_Rotate, metaInteger);
	Meta_Add(L"Flip",    m_Opts.m_Flip,   metaInteger);
	Meta_Add(L"Scale",   m_Opts.m_Scale,  metaInteger);
	Meta_Add(L"TransX",  m_Opts.m_dx,     metaInteger);
	Meta_Add(L"TransY",  m_Opts.m_dy,     metaInteger);

	Meta_SetName((IDS_IMAGE));
	}

////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Image
//

// Dynamic Class

AfxImplementDynamicClass(CUITextPrimImage, CUITextElement);

// Constructor

CUITextPrimImage::CUITextPrimImage(void)
{
	m_uFlags = textEdit;
	}

// Overridables

CString CUITextPrimImage::OnGetAsText(void)
{
	CPrimImage * &pImage = (CPrimImage * &) m_pData->GetObject(m_pItem);

	if( pImage ) {

		// REV3 -- Encode transformation, too.

		CString Text = IDS_IMAGE;

		Text += L' ';

		Text += CPrintf("%u", pImage->m_Image);

		return Text;
		}

	return IDS_PUI_NONE;
	}

UINT CUITextPrimImage::OnSetAsText(CError &Error, CString Text)
{
	CPrimImage * &pImage = (CPrimImage * &) m_pData->GetObject(m_pItem);

	if( Text == CString(IDS_PUI_NONE) ) {

		if( pImage ) {

			pImage->Kill();

			delete pImage;

			pImage = NULL;

			return saveChange;
			}

		return saveSame;
		}
	else {
		// REV3 -- Decode transformation, too.

		CString Find = CString(CString(IDS_IMAGE)) + L' ';

		UINT    uLen = Find.GetLength();

		if( Text.Left(uLen) == Find ) {

			PCTXT pValue = PCTXT(Text) + uLen;

			PTXT  pError = NULL;

			UINT  uImage = wcstoul(pValue, &pError, 10);

			if( !*pError ) {

				if( !pImage ) {

					pImage = New CPrimImage;

					pImage->SetParent(m_pItem);

					pImage->Init();

					pImage->m_Image = uImage;

					return saveChange;
					}

				if( pImage->m_Image == uImage ) {

					return saveSame;
					}

				pImage->m_Image = uImage;

				return saveChange;
				}

			Error.Set(CString(IDS_INVALID_IMAGE));

			return saveError;
			}
		}

	return saveSame;
	}

////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Image
//

// Dynamic Class

AfxImplementDynamicClass(CUIPrimImage, CUIControl);

// Constructor

CUIPrimImage::CUIPrimImage(void)
{
	m_pTextLayout = NULL;

	m_pPictLayout = NULL;

	m_pFileLayout = NULL;

	m_pPickLayout = NULL;

	m_pWipeLayout = NULL;

	m_pEditLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pPictCtrl   = New CImagePreviewWnd(this);

	m_pFileCtrl   = New CHotLinkCtrl;

	m_pPickCtrl   = New CHotLinkCtrl;

	m_pWipeCtrl   = New CHotLinkCtrl;

	m_pEditCtrl   = New CHotLinkCtrl;

	m_fEnable     = TRUE;
	}

// Attributes

BOOL CUIPrimImage::HasPrevious(void) const
{
	return watoi(m_UIData.m_Tag.Right(2)) > 1;
	}

BOOL CUIPrimImage::CanCopyPrevious(void) const
{
	return !m_pImage && HasPrevious();
	}

UINT CUIPrimImage::GetImage(void) const
{
	return m_pImage ? m_pImage->m_Image : NOTHING;
	}

// Operations

void CUIPrimImage::GetOpts(CImageOpts &Opts)
{
	AfxAssert(m_pImage);

	Opts = m_pImage->m_Opts;
	}

void CUIPrimImage::SetOpts(CImageOpts &Opts)
{
	AfxAssert(m_pImage);

	m_pImage->m_Opts = Opts;
	}

BOOL CUIPrimImage::CopyPrevious(void)
{
	if( !m_pImage ) {

		UINT n = watoi(m_UIData.m_Tag.Right(2));

		if( n > 1 ) {

			CString          Name  = CPrintf(L"Image%2.2u", n - 1);

			CMetaItem       *pItem = (CMetaItem *) m_pItem;

			CMetaData const *pMeta = pItem->FindMetaData(Name);

			if( pMeta ) {

				CPrimImage *pImage = (CPrimImage *) pMeta->GetObject(m_pItem);

				if( pImage ) {

	     				m_pImage = New CPrimImage;

					m_pImage->SetParent(m_pItem);

					m_pImage->Init();

					m_pImage->m_Opts  = pImage->m_Opts;

					m_pImage->m_Image = pImage->m_Image;

					m_pData->GetObject(m_pItem) = m_pImage;

					OnLoad();

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CUIPrimImage::CopyPreviousOpts(CImageOpts &Opts)
{
	UINT n = watoi(m_UIData.m_Tag.Right(2));

	if( n > 1 ) {

		CString          Name  = CPrintf(L"Image%2.2u", n - 1);

		CMetaItem       *pItem = (CMetaItem *) m_pItem;

		CMetaData const *pMeta = pItem->FindMetaData(Name);

		if( pMeta ) {

			CPrimImage *pImage = (CPrimImage *) pMeta->GetObject(m_pItem);

			CSize Size = Opts.m_Size;

			Opts = pImage->m_Opts;

			Opts.m_Size = Size;

			return TRUE;
			}
		}

	return FALSE;
	}

void CUIPrimImage::DoubleClick(void)
{
	if( GetImage() == NOTHING ) {

		if( CanCopyPrevious() ) {

			CopyPrevious();
			}

		return;
		}

	DoEdit();
	}

// Core Overridables

void CUIPrimImage::OnBind(void)
{
	CUIControl::OnBind();

	m_Name   = GetLabel() + L":";
	
	m_fLimit = (m_UIData.m_Format[0] == 'L');

	FindAspect();

	FindManager();
	}

void CUIPrimImage::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Name, 1, 1);

	m_pPictLayout = New CLayItemSize(CSize(100, 100));

	m_pPickLayout = New CLayItemText(CString(IDS_PICK),   2, 0);

	m_pFileLayout = New CLayItemText(CString(IDS_BROWSE), 2, 0);

	m_pEditLayout = New CLayItemText(CString(IDS_ADJUST), 2, 0);

	m_pWipeLayout = New CLayItemText(CString(IDS_CLEAR),  2, 0);

	CLayFormation *pButtons = New CLayFormCol;

	m_pMainLayout           = New CLayFormRow;

	pButtons->AddItem(New CLayFormPad(m_pPickLayout, horzNone | vertCenter));

	pButtons->AddItem(New CLayFormPad(m_pFileLayout, horzNone | vertCenter));

	pButtons->AddItem(New CLayFormPad(m_pEditLayout, horzNone | vertCenter));

	pButtons->AddItem(New CLayFormPad(m_pWipeLayout, horzNone | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pPictLayout, horzNone | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(pButtons,      horzNone | vertCenter));

	m_pMainLayout->AddItem(New CLayItemSize(CSize(8, 8)));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIPrimImage::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Name,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pPictCtrl->Create( WS_TABSTOP,
			     m_pPictLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pPickCtrl->Create( CString(IDS_PICK),
			     WS_TABSTOP | BS_PUSHBUTTON,
			     m_pPickLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pFileCtrl->Create( CString(IDS_BROWSE),
			     WS_TABSTOP | BS_PUSHBUTTON,
			     m_pFileLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pEditCtrl->Create( CString(IDS_ADJUST),
			     WS_TABSTOP | BS_PUSHBUTTON,
			     m_pEditLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pWipeCtrl->Create( CString(IDS_CLEAR),
			     WS_TABSTOP | BS_PUSHBUTTON,
			     m_pWipeLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pPictCtrl->SetFont(afxFont(Dialog));

	m_pFileCtrl->SetFont(afxFont(Dialog));

	m_pPickCtrl->SetFont(afxFont(Dialog));

	m_pWipeCtrl->SetFont(afxFont(Dialog));

	m_pEditCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pPictCtrl, 0);

	AddControl(m_pFileCtrl);

	AddControl(m_pPickCtrl);

	AddControl(m_pWipeCtrl);

	AddControl(m_pEditCtrl);

	m_pWnd = &Wnd;
	}

void CUIPrimImage::OnMove(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pPictCtrl->MoveWindow(m_pPictLayout->GetRect(), TRUE);

	m_pFileCtrl->MoveWindow(m_pFileLayout->GetRect(), TRUE);

	m_pPickCtrl->MoveWindow(m_pPickLayout->GetRect(), TRUE);

	m_pWipeCtrl->MoveWindow(m_pWipeLayout->GetRect(), TRUE);

	m_pEditCtrl->MoveWindow(m_pEditLayout->GetRect(), TRUE);
	}

void CUIPrimImage::OnEnable(BOOL fEnable)
{
	m_fEnable = fEnable;

	DoEnables();
	}

// Notification Handlers

BOOL CUIPrimImage::OnNotify(UINT uID, UINT uCode)
{
	if( !m_pText->HasFlag(textRead) ) {

		if( uID == m_pFileCtrl->GetID() ) {

			return DoBrowse();
			}

		if( uID == m_pPickCtrl->GetID() ) {

			return DoPick();
			}

		if( uID == m_pWipeCtrl->GetID() ) {

			return DoClear();
			}

		if( uID == m_pEditCtrl->GetID() ) {

			return DoEdit();
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Data Overridables

BOOL CUIPrimImage::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( !m_pText->HasFlag(textRead) ) {

		if( m_pImages->CanAcceptDataObject(pData) ) {

			dwEffect = DROPEFFECT_COPY;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUIPrimImage::OnAcceptData(IDataObject *pData)
{
	if( !m_pImage ) {

		m_pImage = New CPrimImage;

		m_pImage->SetParent(m_pItem);

		m_pImage->Init();

		m_pData->GetObject(m_pItem) = m_pImage;
		}

	CSize MaxSize = m_pSystem->m_DispSize;
	
	m_pImage->LoadFromDataObject(pData, MaxSize);

	m_pImage->SetDirty();

	LoadUI();

	ForceUpdate();

	return TRUE;
	}

void CUIPrimImage::OnLoad(void)
{
	m_pImage = (CPrimImage *) m_pData->GetObject(m_pItem);

	m_pPictCtrl->SetImage(GetImage());

	DoEnables();
	}

// Implementation

void CUIPrimImage::FindAspect(void)
{
	if( m_pItem->IsKindOf(AfxRuntimeClass(CPrim)) ) {

		CPrim *pPrim = (CPrim *) m_pItem;

		m_Aspect = pPrim->GetRect().GetSize();

		return;
		}

	m_Aspect = CSize(100, 100);
	}

void CUIPrimImage::FindManager(void)
{
	m_pSystem  = (CUISystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pImages = m_pSystem->m_pUI->m_pImages;
	}

BOOL CUIPrimImage::DoBrowse(void)
{
	COpenFileDialog Dlg;

	CModule * pApp = afxModule->GetApp();

	CFilename Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Images");

	Dlg.LoadLastPath(L"Image", Path);

	Dlg.SetCaption ( CString(IDS_BROWSE_FOR_IMAGE)
			 );

	Dlg.SetFilter  ( CString(IDS_ALL)       +
			 CString(IDS_JPEG)      +
			 CString(IDS_METAFILE)  +
			 CString(IDS_BITMAP)    +
			 CString(IDS_ALL_FILES)
			 );

	Dlg.SetFilename(m_pImages->GetFilename(GetImage()));

	if( Dlg.Execute(*m_pWnd) ) {

		UINT uImage = NOTHING;

		m_pWnd->UpdateWindow();

		if( TRUE ) {

			afxThread->SetWaitMode(TRUE);

			CString Name = Dlg.GetFilename();

			uImage = m_pImages->LoadFromFile(Name, TRUE);

			afxThread->SetWaitMode(FALSE);
			}

		if( uImage < NOTHING ) {

			if( !m_pImage ) {

				m_pImage = New CPrimImage;

				m_pImage->SetParent(m_pItem);

				m_pImage->Init();

				m_pData->GetObject(m_pItem) = m_pImage;
				}

			m_pImage->m_Image = uImage;

			m_pImage->SetDirty();

			OnLoad();

			Dlg.SaveLastPath(L"Image");
			}
		else {
			CString Text = IDS_FAILED_TO_LOAD;

			m_pWnd->Error(Text);
			}

		m_pWnd->SetFocus();

		return TRUE;
		}

	m_pWnd->SetFocus();

	return FALSE;
	}

BOOL CUIPrimImage::DoPick(void)
{
	CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

	CImageSelectDialog Dlg(this);

	System.ExecSemiModal(Dlg, L"Symbols");

	return TRUE;
	}

BOOL CUIPrimImage::DoClear(void)
{
	if( m_pImage ) {

		m_pImage->Kill();

		delete m_pImage;

		m_pImage = NULL;

		m_pData->GetObject(m_pItem) = m_pImage;
		}

	m_pItem->SetDirty();

	OnLoad();

	return TRUE;
	}

BOOL CUIPrimImage::DoEdit(void)
{
	CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

	CImageEditDialog Dlg(this);

	if( System.ExecSemiModal(Dlg, L"") ) {

		OnLoad();

		return TRUE;
		}

	return FALSE;
	}

void CUIPrimImage::DoEnables(void)
{
	BOOL fImage = (GetImage() < NOTHING);

	BOOL fEdit  = fImage;

	m_pTextCtrl->EnableWindow(m_fEnable);

	m_pPictCtrl->EnableWindow(m_fEnable);

	m_pPickCtrl->EnableWindow(m_fEnable);

	m_pFileCtrl->EnableWindow(m_fEnable);

	m_pEditCtrl->EnableWindow(m_fEnable && !m_fLimit && fEdit);

	m_pWipeCtrl->EnableWindow(m_fEnable && !m_fLimit && fImage);
	}

//////////////////////////////////////////////////////////////////////////
//
// Image Preview Window
//

// Runtime Class

AfxImplementRuntimeClass(CImagePreviewWnd, CCtrlWnd);

// Constructor

CImagePreviewWnd::CImagePreviewWnd(CUIPrimImage *pUI)
{
	m_pUI     = pUI;

	m_uImage = NOTHING;

	m_pFile  = NULL;

	m_uDrop  = 0;
	}

// Destructor

CImagePreviewWnd::~CImagePreviewWnd(void)
{
	}

// IUnknown

HRESULT CImagePreviewWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CImagePreviewWnd::AddRef(void)
{
	return 1;
	}

ULONG CImagePreviewWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CImagePreviewWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( m_pUI ) {

		if( m_pUI->CanAcceptData(pData, *pEffect) ) {

			m_dwEffect = *pEffect;

			SetDrop(1);

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CImagePreviewWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CImagePreviewWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
	}

HRESULT CImagePreviewWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		if( m_pUI->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);
	
	return S_OK;
	}

// Operations

void CImagePreviewWnd::SetImage(UINT uImage)
{
	if( (m_uImage = uImage) < NOTHING ) {

		FindImageManager();

		m_pFile = m_pImages->m_pFiles->GetItem(m_uImage);
		}
	else
		m_pFile = NULL;

	Render();

	Invalidate(TRUE);
	}

// Message Map

AfxMessageMap(CImagePreviewWnd, CCtrlWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_ENABLE)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)

	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CImagePreviewWnd)
	};

// Message Handlers

void CImagePreviewWnd::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
	}

void CImagePreviewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( IsEnabled() ) {

		if( m_pFile ) {

			CRect Rect = GetClientRect() - 2;

			CRect Draw = FindDrawRect();

			DC.FillRect(Rect, afxBrush(TabFace));

			DC.BitBlt  (Draw, m_Bitmap, CPoint(0, 0), SRCCOPY);
			}
		else {
			DC.Select(afxFont(Dialog));

			DC.SetBkMode(TRANSPARENT);

			DC.SetTextColor(afxColor(WHITE));

			CString Text1, Text2;

			if( m_pUI->CanCopyPrevious() ) {

				Text1 = CString(IDS_DOUBLECLICK_TO);

				Text2 = CString(IDS_COPY_PREVIOUS);
				}
			else {
				Text1 = CString(IDS_NO_IMAGE);

				Text2 = CString(IDS_SELECTED);
				}

			CSize Size1 = DC.GetTextExtent(Text1);

			CSize Size2 = DC.GetTextExtent(Text2);

			CRect Rect  = GetClientRect() - 3;

			DC.FillRect(Rect, afxBrush(BLACK));

			DC.TextOut (Rect.GetTopLeft() + (Rect.GetSize() - Size1) / 2 - CSize(0, 10), Text1);

			DC.TextOut (Rect.GetTopLeft() + (Rect.GetSize() - Size2) / 2 + CSize(0, 10), Text2);

			DC.Deselect();
			}
		}
	else {
		CRect Rect = GetClientRect() - 2;

		DC.FillRect(Rect, afxBrush(TabFace));
		}
	}

BOOL CImagePreviewWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	DC.FrameRect(Rect--, afxBrush(3dShadow));

	DC.FrameRect(Rect--, afxBrush(TabFace));
	
	DC.FrameRect(Rect--, afxBrush(TabFace));

	return TRUE;
	}

void CImagePreviewWnd::OnSize(UINT uType, CSize Size)
{
	CRect Rect = FindDrawRect();

	CSize View = Rect.GetSize();

	m_Bitmap.Create(CClientDC(ThisObject), View);

	Render();

	Invalidate(TRUE);
	}

void CImagePreviewWnd::OnEnable(BOOL fEnable)
{
	Invalidate(TRUE);
	}

void CImagePreviewWnd::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	m_pUI->DoubleClick();
	}

// Command Handlers

BOOL CImagePreviewWnd::OnPasteControl(UINT uID, CCmdSource &Src)
{
	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			DWORD dwEffect;

			if( m_pUI->CanAcceptData(pData, dwEffect) ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

BOOL CImagePreviewWnd::OnPasteCommand(UINT uID)
{
	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			if( m_pUI->AcceptData(pData) ) {

				pData->Release();

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

// Implementation

CRect CImagePreviewWnd::FindDrawRect(void)
{
	CRect Rect = GetClientRect() - 3;

	Rect.MatchAspect(m_pUI->m_Aspect);

	return Rect;
	}

void CImagePreviewWnd::Render(void)
{
	if( m_Bitmap.IsValid() ) {

		if( m_pFile ) {

			CMemoryDC DC;

			DC.Select(m_Bitmap);

			CPoint Pos  = CPoint(0, 0);

			CSize  Size = m_Bitmap.GetSize();

			CRect  Rect = CRect(Pos, Size);

			DC.FillRect(Rect, afxBrush(BLACK));

			CImageOpts Opts;

			m_pUI->GetOpts(Opts);

			Opts.m_Size = Size;

			if( !m_pFile->Preview(DC, Pos, Opts) ) {

				DC.Select(afxFont(Dialog));

				DC.SetTextColor(afxColor(WHITE));

				DC.SetBkColor(afxColor(BLACK));

				CString Text = IDS_NOT_AVAILABLE;

				CSize   Ext  = DC.GetTextExtent(Text);

				CPoint  Pos  = Rect.GetTopLeft() + (Rect.GetSize() - Ext) / 2;

				DC.TextOut(Pos, Text);

				DC.Deselect();
				}

			DC.Deselect();
			}
		}
	}

void CImagePreviewWnd::FindImageManager(void)
{
	CDatabase *pDbase  = m_pUI->GetItem()->GetDatabase();

	CUISystem *pSystem = (CUISystem *) pDbase->GetSystemItem();

	m_pImages = pSystem->m_pUI->m_pImages;
	}

BOOL CImagePreviewWnd::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Image Editing Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CImageEditDialog, CStdDialog);
		
// Constructors

CImageEditDialog::CImageEditDialog(CUIPrimImage *pUI)
{
	// IDEA -- Add other proprties to the options, such as
	// alpha, color adjustment etc, and support them here.

	m_pUI = pUI;

	SetName(L"ImageEditDlg");

	CCtrlWnd::LoadControlClass(ICC_BAR_CLASSES);
	}

// Message Map

AfxMessageMap(CImageEditDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_HSCROLL)
	AfxDispatchMessage(WM_VSCROLL)

	AfxDispatchCommand(IDOK, OnCommandOK)
	AfxDispatchCommand(500,  OnCommandReset)
	AfxDispatchCommand(501,  OnCommandCopyPrev)
	AfxDispatchCommand(600,  OnCommandShow)
	AfxDispatchCommand(700,  OnCommandFlipH)
	AfxDispatchCommand(701,  OnCommandFlipV)

	AfxMessageEnd(CImageEditDialog)
	};

// Message Handlers

BOOL CImageEditDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_uImage = m_pUI->GetImage();

	m_pUI->GetOpts(m_Opts);

	CWnd    &Draw = GetDlgItem(100);

	CButton &Copy = (CButton &) GetDlgItem(501);

	CButton &Show = (CButton &) GetDlgItem(600);

	Draw.ShowWindow(SW_HIDE);

	Copy.EnableWindow(m_pUI->HasPrevious());

	Show.SetCheck(TRUE);

	FindManager();

	LoadBars();

	LoadChecks();

	return TRUE;
	}

void CImageEditDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = FindDrawRect();

	CPoint    Pos  = CPoint(0, 0);

	CSize     Size = Rect.GetSize();

	CBitmap   Bitmap(DC, Size);

	CMemoryDC WorkDC;

	WorkDC.Select(Bitmap);

	WorkDC.FillRect(Size, afxBrush(BLACK));

	m_Opts.m_Size = Size;

	m_pFile->Preview(WorkDC, Pos, m_Opts);

	CButton &Show = (CButton &) GetDlgItem(600);

	if( Show.IsChecked() ) {

		WorkDC.FillRect(Size.cx / 2, 0, Size.cx / 2 + 1, Size.cy, afxBrush(BLUE));

		WorkDC.FillRect(0, Size.cy / 2, Size.cx, Size.cy / 2 + 1, afxBrush(BLUE));
		}

	DC.BitBlt(Rect, WorkDC, Pos, SRCCOPY);

	WorkDC.Deselect();
	}

void CImageEditDialog::OnHScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	OnVScroll(uCode, nPos, Ctrl);
	}

void CImageEditDialog::OnVScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( Ctrl.GetID() == 200 ) {

		m_Opts.m_dx = ((CTrackBar &) Ctrl).GetPos();

		Invalidate(FALSE);
		}

	if( Ctrl.GetID() == 201 ) {

		m_Opts.m_dy = ((CTrackBar &) Ctrl).GetPos();

		Invalidate(FALSE);
		}

	if( Ctrl.GetID() == 300 ) {

		m_Opts.m_Scale = 2000 - ((CTrackBar &) Ctrl).GetPos();

		CWnd &ValZ = GetDlgItem(350);

		ValZ.SetWindowText(CPrintf(L"%d%%", m_Opts.m_Scale / 10));

		Invalidate(FALSE);
		}

	if( Ctrl.GetID() == 400 ) {

		m_Opts.m_Rotate = ((CTrackBar &) Ctrl).GetPos();

		CWnd &ValR = GetDlgItem(450);

		ValR.SetWindowText(CPrintf(L"%+d%c", m_Opts.m_Rotate, 0xB0));

		Invalidate(FALSE);
		}
	}

// Command Handlers

BOOL CImageEditDialog::OnCommandOK(UINT uID)
{
	m_pUI->SetOpts(m_Opts);

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CImageEditDialog::OnCommandReset(UINT uID)
{
	m_Opts = CImageOpts();

	LoadBars();

	LoadChecks();

	Invalidate(FALSE);

	return TRUE;
	}

BOOL CImageEditDialog::OnCommandCopyPrev(UINT uID)
{
	m_pUI->CopyPreviousOpts(m_Opts);

	LoadBars();

	LoadChecks();

	Invalidate(FALSE);

	return TRUE;
	}

BOOL CImageEditDialog::OnCommandShow(UINT uID)
{
	Invalidate(FALSE);

	return TRUE;
	}

BOOL CImageEditDialog::OnCommandFlipH(UINT uID)
{
	if( ((CButton &) GetDlgItem(uID)).IsChecked() ) {

		m_Opts.m_Flip |=  1;
		}
	else
		m_Opts.m_Flip &= ~1;

	Invalidate(FALSE);

	return TRUE;
	}

BOOL CImageEditDialog::OnCommandFlipV(UINT uID)
{
	if( ((CButton &) GetDlgItem(uID)).IsChecked() ) {

		m_Opts.m_Flip |=  2;
		}
	else
		m_Opts.m_Flip &= ~2;

	Invalidate(FALSE);

	return TRUE;
	}

// Implementation

CRect CImageEditDialog::FindDrawRect(void)
{
	CWnd  &Wnd = GetDlgItem(100);

	CRect Rect = Wnd.GetClientRect() - 1;

	Wnd.ClientToScreen(Rect);

	ScreenToClient(Rect);

	Rect.MatchAspect(m_pUI->m_Aspect);

	return Rect;
	}

void CImageEditDialog::FindManager(void)
{
	CUISystem *pSystem;
	
	pSystem   = (CUISystem *) m_pUI->GetItem()->GetDatabase()->GetSystemItem();

	m_pImages = pSystem->m_pUI->m_pImages;

	m_pFile   = m_pImages->m_pFiles->GetItem(m_uImage);
	}

void CImageEditDialog::LoadBars(void)
{
	CTrackBar &BarH = (CTrackBar &) GetDlgItem(200);

	BarH.SetTickFreq(100);

	BarH.SetPageSize(100);

	BarH.SetLineSize(5);

	BarH.SetRange(UINT(-1000), UINT(+1000), TRUE);
	
	BarH.SetPos(m_Opts.m_dx, TRUE);

	////////

	CTrackBar &BarV = (CTrackBar &) GetDlgItem(201);
	
	BarV.SetTickFreq(100);

	BarV.SetPageSize(100);

	BarV.SetLineSize(5);

	BarV.SetRange(UINT(-1000), UINT(+1000), TRUE);
	
	BarV.SetPos(m_Opts.m_dy, TRUE);

	////////

	CTrackBar &BarZ = (CTrackBar &) GetDlgItem(300);

	CWnd      &ValZ = GetDlgItem(350);
	
	BarZ.SetTickFreq(100);

	BarZ.SetPageSize(100);

	BarZ.SetLineSize(10);

	BarZ.SetRange(UINT(100), UINT(+1900), TRUE);
	
	BarZ.SetPos(2000 - m_Opts.m_Scale, TRUE);

	ValZ.SetWindowText(CPrintf(L"%d%%", m_Opts.m_Scale / 10));

	////////

	CTrackBar &BarR = (CTrackBar &) GetDlgItem(400);

	CWnd      &ValR = GetDlgItem(450);

	BarR.SetTickFreq(15);

	BarR.SetPageSize(15);

	BarR.SetRange(UINT(-180), UINT(+180), TRUE);
	
	BarR.SetPos(m_Opts.m_Rotate, TRUE);

	ValR.SetWindowText(CPrintf(L"%+d%c", m_Opts.m_Rotate, 0xB0));
	}

void CImageEditDialog::LoadChecks(void)
{
	CButton &FlipH = (CButton &) GetDlgItem(700);
	
	CButton &FlipV = (CButton &) GetDlgItem(701);

	FlipH.SetCheck(m_Opts.m_Flip & 1);

	FlipV.SetCheck(m_Opts.m_Flip & 2);
	}

// End of File
