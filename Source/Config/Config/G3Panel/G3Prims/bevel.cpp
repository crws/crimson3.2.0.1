
#include "intern.hpp"

#include "bevel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CPrimBevelBase, CPrimWithText);

// Constructor

CPrimBevelBase::CPrimBevelBase(void)
{
	m_Border  = 4;

	m_pFace   = New CPrimBrush;

	m_pHilite = New CPrimColor;

	m_pShadow = New CPrimColor;
	}

// UI Overridables

BOOL CPrimBevelBase::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       2
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// Overridables

BOOL CPrimBevelBase::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

void CPrimBevelBase::SetInitState(void)
{
	m_pFace  ->Set(GetRGB(12,12,24));

	m_pHilite->Set(GetRGB(18,18,30));

	m_pShadow->Set(GetRGB( 6, 6,16));
	}

void CPrimBevelBase::FindTextRect(void)
{
	BOOL fHoney  = C3OemFeature(L"OemSD", FALSE);

	int  nExtra  = fHoney ? 1 : 0;

	int  nAdjust = m_Border + nExtra;

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust;
	}

CSize CPrimBevelBase::GetMinSize(IGDI *pGDI)
{
	CSize Size(16, 16);

	if( m_pTextItem ) {
	
		Size.cy = m_pTextItem->GetLineSize() + 2 * (m_Border + 1);
		}

	return Size;
	}

// Download Support

BOOL CPrimBevelBase::MakeInitData(CInitData &Init)
{
	CPrimWithText::MakeInitData(Init);

	Init.AddByte(BYTE(m_Border));

	Init.AddItem(itemSimple, m_pFace);

	Init.AddItem(itemSimple, m_pHilite);

	Init.AddItem(itemSimple, m_pShadow);

	return TRUE;
	}

// Meta Data

void CPrimBevelBase::AddMetaData(void)
{
	CPrimWithText::AddMetaData();

	Meta_AddInteger(Border);
	Meta_AddObject (Face);
	Meta_AddObject (Hilite);
	Meta_AddObject (Shadow);

	Meta_SetName((IDS_BEVEL_BASE_CLASS));
	}

// Implementation

void CPrimBevelBase::FindPoints(P2 *t, P2 *b, R2 &r)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, m_Border - 1, m_Border - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;

	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel
//

// Dynamic Class

AfxImplementDynamicClass(CPrimBevel, CPrimBevelBase);

// Constructor

CPrimBevel::CPrimBevel(void)
{
	m_uType = 0x09;
	
	m_Type  = 0;
	}

// Overridables

void CPrimBevel::FindTextRect(void)
{
	int nCount  = (m_Type == 2) ? 2 : 1;

	int nAdjust = (m_Border * nCount) + 1;

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust;
	}

void CPrimBevel::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	P2 h[6];
	
	P2 s[6];

	FindPoints(h, s, Rect);

	pGDI->SelectBrush(brushFore);

	pGDI->SetBrushFore(m_pShadow->GetColor());

	pGDI->FillPolygon((m_Type == 1) ? h : s, 6, 0);

	pGDI->SetBrushFore(m_pHilite->GetColor());

	pGDI->FillPolygon((m_Type == 1) ? s : h, 6, 0);

	if( m_Type == 2 ) {

		FindPoints(s, h, Rect);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(m_pShadow->GetColor());

		pGDI->FillPolygon(s, 6, 0);

		pGDI->SetBrushFore(m_pHilite->GetColor());

		pGDI->FillPolygon(h, 6, 0);
		}

	m_pFace->FillRect(pGDI, Rect);

	CPrimBevelBase::Draw(pGDI, uMode);
	}

// Download Support

BOOL CPrimBevel::MakeInitData(CInitData &Init)
{
	CPrimBevelBase::MakeInitData(Init);

	Init.AddByte(BYTE(m_Type));

	return TRUE;
	}

// Meta Data

void CPrimBevel::AddMetaData(void)
{
	CPrimBevelBase::AddMetaData();

	Meta_AddInteger(Type);

	Meta_SetName((IDS_PANEL));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Button with Bevel
//

// Dynamic Class

AfxImplementDynamicClass(CPrimBevelButton, CPrimBevelBase);

// Constructor

CPrimBevelButton::CPrimBevelButton(void)
{
	m_uType    = 0x15;
	
	m_uActMode = actAlways;
	}

// Overridables

void CPrimBevelButton::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	P2 h[6];
	
	P2 s[6];

	FindPoints(h, s, Rect);

	pGDI->SelectBrush(brushFore);

	pGDI->SetBrushFore(m_pShadow->GetColor());

	BOOL p = IsPressed();

	pGDI->FillPolygon(p ? h : s, 6, 0);

	pGDI->SetBrushFore(m_pHilite->GetColor());

	pGDI->FillPolygon(p ? s : h, 6, 0);

	m_pFace->FillRect(pGDI, Rect);

	CPrimBevelBase::Draw(pGDI, uMode);
	}

void CPrimBevelButton::SetInitState(void)
{
	CPrimBevelBase::SetInitState();

	AddText();

	m_pTextItem->m_Lead = 0;

	m_pTextItem->Set(L"TEXT", TRUE);

	SetInitSize(64, 32);
	}

// Download Support

BOOL CPrimBevelButton::MakeInitData(CInitData &Init)
{
	CPrimBevelBase::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimBevelButton::AddMetaData(void)
{
	CPrimBevelBase::AddMetaData();

	Meta_SetName((IDS_BEVEL_BUTTON));
	}

// End of File
