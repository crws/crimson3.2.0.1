
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAR1_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAR1_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeAR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Gas Gauge Primitive
//

class CPrimRubyGaugeTypeAR1 : public CPrimRubyGaugeTypeAR
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeAR1(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);

		// Implementation
		void LoadLayout(void);
	};

// End of File

#endif
