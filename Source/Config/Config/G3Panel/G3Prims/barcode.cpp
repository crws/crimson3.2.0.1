
#include "intern.hpp"

#include <g3barcode.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Barcode
//

// Dynamic Class

AfxImplementDynamicClass(CPrimBarcode, CPrim);

// Constructor

CPrimBarcode::CPrimBarcode(void)
{
	m_uType    = 0x36;

	m_fError   = FALSE;

	m_pValue   = NULL;

	m_Code     = 58;

	m_Border   = 0;

	m_pColor1  = New CPrimColor;

	m_pColor2  = New CPrimColor;

	m_NoShrink = 0;

	m_NoGrow   = 0;

	m_NoNonInt = 1;

	m_AlignH   = 1;

	m_AlignV   = 1;
}

// UI Update

void CPrimBarcode::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == L"NoNonInt" ) {

			if( m_NoNonInt ) {

				if( !m_NoShrink ) {

					m_NoShrink = 1;

					pHost->UpdateUI(this, L"NoShrink");
				}

				pHost->EnableUI(this, L"NoShrink", FALSE);
			}
			else
				pHost->EnableUI(this, L"NoShrink", TRUE);
		}
	}

	return CPrim::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimBarcode::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Value" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CPrim::GetTypeData(Tag, Type);
}

// Overridables

void CPrimBarcode::Draw(IGDI *pGDI, UINT uMode)
{
	m_fError = FALSE;

	COLOR Paper = m_pColor1->GetColor();

	COLOR Ink   = m_pColor2->GetColor();

	pGDI->SetBrushFore(Paper);

	pGDI->SelectBrush(brushFore);

	pGDI->FillRect(PassRect(m_DrawRect));

	CString Text = m_pValue ? m_pValue->GetText() : L"12345678";

	if( !Text.IsEmpty() ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1 - 2 * m_Border;

		int cy = m_DrawRect.y2 - m_DrawRect.y1 - 2 * m_Border;

		int xp = m_DrawRect.x1 + m_Border;

		int yp = m_DrawRect.y1 + m_Border;

		if( cx > 0 &&  cy > 0 ) {

			PBYTE pData = NULL;

			int   nStep = 0;

			int   xSize = 0;

			int   ySize = 0;

			UINT  uMask = GetMask();

			UINT  uLoop = (uMode == drawThumb) ? 2 : 1;

			for( UINT p = 0; p < uLoop; p++ ) {

				if( p ) {

					uMask &= ~1;
					uMask &= ~4;
				}

				Barcode_Render(Text, m_Code, cx, cy, uMask, pData, nStep, xSize, ySize);

				if( pData ) {

					if( xSize <= cx && ySize <= cy ) {

						pGDI->ResetFont();

						pGDI->SetTextFore(Ink);

						pGDI->SetTextBack(Paper);

						pGDI->SetTextTrans(modeOpaque);

						xp += m_AlignH * (cx - xSize) / 2;

						yp += m_AlignV * (cy - ySize) / 2;

						pGDI->TextOut(xp, yp, L""); // Configure GDI

						pGDI->CharBlt(xp, yp, xSize, ySize, nStep, pData);

						delete pData;

						DrawNotSupported(pGDI);

						return;
					}

					delete pData;
				}
			}
		}

		m_fError = TRUE;
	}
}

void CPrimBarcode::SetInitState(void)
{
	CPrim::SetInitState();

	m_pColor1->Set(GetRGB(31, 31, 31));

	m_pColor2->Set(GetRGB(0, 0, 0));
}

BOOL CPrimBarcode::HasError(void)
{
	return m_fError || CPrim::HasError();
}

BOOL CPrimBarcode::IsSupported(void)
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
}

// Persistance

// Download Support

BOOL CPrimBarcode::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);
	Init.AddWord(WORD(m_Code));
	Init.AddWord(WORD(m_Border));
	Init.AddItem(itemSimple, m_pColor1);
	Init.AddItem(itemSimple, m_pColor2);
	Init.AddByte(BYTE(m_NoShrink));
	Init.AddByte(BYTE(m_NoGrow));
	Init.AddByte(BYTE(m_NoNonInt));
	Init.AddByte(BYTE(m_AlignH));
	Init.AddByte(BYTE(m_AlignV));

	return TRUE;
}

// Meta Data

void CPrimBarcode::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddInteger(Code);
	Meta_AddInteger(Border);
	Meta_AddObject(Color1);
	Meta_AddObject(Color2);
	Meta_AddInteger(NoShrink);
	Meta_AddInteger(NoGrow);
	Meta_AddInteger(NoNonInt);
	Meta_AddInteger(AlignH);
	Meta_AddInteger(AlignV);

	Meta_SetName(IDS_BARCODE);
}

// Implementation

UINT CPrimBarcode::GetMask(void)
{
	UINT uMask = 0;

	if( m_NoShrink ) uMask |= 1;
	if( m_NoGrow ) uMask |= 2;
	if( m_NoNonInt ) uMask |= 4;

	return uMask;
}

void CPrimBarcode::DrawNotSupported(IGDI *pGDI)
{
	if( !IsSupported() ) {

		R2 Work = m_DrawRect;

		CString Text = IDS_NOT_SUPPORTED;

		pGDI->SelectFont(fontHei16);

		int cx = pGDI->GetTextWidth(Text);

		int cy = pGDI->GetTextHeight(Text);

		int px = (Work.x1 + Work.x2 - cx) / 2;

		int py = (Work.y1 + Work.y2 - cy) / 2;

		pGDI->SetTextFore(GetRGB(255, 0, 0));

		pGDI->SetBackMode(modeTransparent);

		pGDI->TextOut(px, py, Text);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Barcode Symbology
//

class CUITextBarcodeSymbology : public CUITextEnumPick
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUITextBarcodeSymbology(void);

protected:
	// Overridables
	void OnBind(void);

	// Implementation
	void AddData(UINT Data, CString Text);
	void AddData(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Barcode Symbology
//

// Dynamic Class

AfxImplementDynamicClass(CUITextBarcodeSymbology, CUITextEnumPick);

// Constructor

CUITextBarcodeSymbology::CUITextBarcodeSymbology(void)
{
}

// Overridables

void CUITextBarcodeSymbology::OnBind(void)
{
	CUITextElement::OnBind();

	AddData();
}

// Implementation

void CUITextBarcodeSymbology::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
}

void CUITextBarcodeSymbology::AddData(void)
{
	if( TRUE ) {

		AddData(58, L"QR Code");

		return;
	}

	AddData(71, L"Data Matrix");
//	AddData( 92, L"Aztec Code");
//	AddData(  1, L"Code 11");
	AddData(2, L"Code 2 of 5 Standard");
	AddData(3, L"Code 2 of 5 Interleaved");
	AddData(4, L"Code 2 of 5 IATA");
	AddData(6, L"Code 2 of 5 Data Logic");
	AddData(7, L"Code 2 of 5 Industrial");
	AddData(8, L"Code 3 of 9");
	AddData(9, L"Code 3 of 9 Extended");
//	AddData( 13, L"EAN");
//	AddData( 16, L"GS1-128 (UCC.EAN-128)");
//	AddData( 18, L"Codabar");
	AddData(20, L"Code 128");
//	AddData( 21, L"Deutshe Post Leitcode");
//	AddData( 22, L"Deutshe Post Identcode");
//	AddData( 23, L"Code 16K");
//	AddData( 24, L"Code 49");
//	AddData( 25, L"Code 93");
//	AddData( 28, L"Flattermarken");
//	AddData( 29, L"GS1 DataBar-14");
//	AddData( 30, L"GS1 DataBar Limited");
//	AddData( 31, L"GS1 DataBar Extended");
//	AddData( 32, L"Telepen Alpha");
	AddData(34, L"UPC A");
//	AddData( 37, L"UPC E");
//	AddData( 40, L"PostNet");
//	AddData( 47, L"MSI Plessey");
//	AddData( 49, L"FIM");
//	AddData( 50, L"LOGMARS");
//	AddData( 51, L"Pharmacode One-Track");
//	AddData( 52, L"PZN");
//	AddData( 53, L"Pharmacode Two-Track");
//	AddData( 55, L"PDF417");
//	AddData( 56, L"PDF417 Truncated");
//	AddData( 57, L"Maxicode");
//	AddData( 60, L"Code 128 (Subset B)");
//	AddData( 63, L"Australia Post Standard Customer");
//	AddData( 66, L"Australia Post Reply Paid");
//	AddData( 67, L"Australia Post Routing");
//	AddData( 68, L"Australia Post Redirection");
//	AddData( 69, L"ISBN (EAN-13 with verification stage)");
//	AddData( 70, L"Royal Mail 4 State (RM4SCC)");
//	AddData( 72, L"EAN-14");
//	AddData( 75, L"NVE-18");
//	AddData( 76, L"Japanese Postal Code");
//	AddData( 77, L"Korea Post");
//	AddData( 79, L"GS1 DataBar-14 Stacked");
//	AddData( 80, L"GS1 DataBar-14 Stacked Omnidirectional");
//	AddData( 81, L"GS1 DataBar Expanded Stacked");
//	AddData( 82, L"PLANET");
//	AddData( 84, L"MicroPDF417");
//	AddData( 85, L"USPS OneCode");
//	AddData( 86, L"Plessey Code");
//	AddData( 87, L"Telepen Numeric");
//	AddData( 89, L"ITF-14");
//	AddData( 90, L"Dutch Post KIX Code");
//	AddData( 93, L"DAFT Code");
//	AddData( 97, L"Micro QR Code");
//	AddData( 98, L"HIBC Code 128");
//	AddData( 99, L"HIBC Code 39");
//	AddData(102, L"HIBC Data Matrix");
//	AddData(104, L"HIBC QR Code");
//	AddData(106, L"HIBC PDF417");
//	AddData(108, L"HIBC MicroPDF417");
//	AddData(112, L"HIBC Aztec Code");
//	AddData(128, L"Aztec Runes");
//	AddData(129, L"Code 32");
//	AddData(140, L"Channel Code");
//	AddData(141, L"Code One");
//	AddData(142, L"Grid Matrix");
}

// End of File
