
#include "intern.hpp"

#include "resview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Symbol Resource
//

// Dynamic Class

AfxImplementDynamicClass(CSymbolResourceItem, CMetaItem);

// Constructor

CSymbolResourceItem::CSymbolResourceItem(void)
{
	}

// UI Creation

CViewWnd * CSymbolResourceItem::CreateView(UINT uType)
{
	if( uType == viewResource ) {

		return New CSymbolResourceWnd;
		}						    

	return CMetaItem::CreateView(uType);
	}

//////////////////////////////////////////////////////////////////////////
//
// Symbol Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CSymbolResourceWnd, CImageResourceWnd);

// Color Conversion

#define To888(c) (GetRED(c) << 0 | GetGREEN(c) << 8 | GetBLUE(c) << 16)

// Clipboard Formats

UINT CSymbolResourceWnd::m_cfEMF = RegisterClipboardFormat(L"C31MetaFile");

UINT CSymbolResourceWnd::m_cfBMP = RegisterClipboardFormat(L"C31Bitmap");

UINT CSymbolResourceWnd::m_cfTEX = RegisterClipboardFormat(L"C31Texture");

UINT CSymbolResourceWnd::m_cfSym = RegisterClipboardFormat(L"C31Symbol");

// Constructor

CSymbolResourceWnd::CSymbolResourceWnd(void)
{
	m_CatIcon   = 0x2000000B;

	m_LargeSize = CSize(193, 193);
	
	m_pIndex    = NULL;
	}

// Destructor

CSymbolResourceWnd::~CSymbolResourceWnd(void)
{
	SaveFavorites();

	delete [] m_pIndex;
	}

// Message Map

AfxMessageMap(CSymbolResourceWnd, CImageResourceWnd)
{
	AfxDispatchMessage(WM_RBUTTONDOWN)

	AfxDispatchControlType(IDM_RES, OnResControl)
	AfxDispatchCommandType(IDM_RES, OnResCommand)

	AfxMessageEnd(CSymbolResourceWnd)
	};

// Message Handlers

void CSymbolResourceWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	if( m_uMode == 1 ) {

		if( m_uHover < NOTHING ) {

			m_uPress = m_uHover;

			HideToolTip();

			KillTimer(m_timerPreview);

			KillTimer(m_timerCheck);
		
			CString Name = L"SymLibCtx";

			CMenu   Load = CMenu(PCTXT(Name));

			if( Load.IsValid() ) {

				CMenu &Menu = Load.GetSubMenu(0);

				Menu.MakeOwnerDraw(FALSE);
			
				ClientToScreen(Pos);

				Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
						     Pos,
						     afxMainWnd->GetHandle()
						     );

				Menu.FreeOwnerDraw();

				CPoint Pos = GetCursorPos();

				ScreenToClient(Pos);

				OnMouseMove(0, Pos);
				}

			Invalidate(GetHoverRect(m_uPress), TRUE);
			}
		}
	}

// Command Handlers

BOOL CSymbolResourceWnd::OnResControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_RES_FAVORITE:

			if( m_uPress < NOTHING ) {

				CSym *pSym = m_SymList[m_uPress];

				if( m_uMode == 1 ) {

					if( m_pIndex[m_uCat] < NOTHING ) {

						Src.CheckItem(pSym->m_fMark);
						}
					else
						Src.CheckItem(TRUE);

					Src.EnableItem(TRUE);
					}
				}

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CSymbolResourceWnd::OnResCommand(UINT uID)
{
	switch( uID ) {

		case IDM_RES_FAVORITE:

			if( m_uPress < NOTHING ) {

				CSym *pSym = m_SymList[m_uPress];

				if( m_uMode == 1 ) {

					UINT  uCat  = m_SymList[m_uPress]->m_uCat;

					UINT  uSlot = m_SymList[m_uPress]->m_uSlot;

					DWORD uData = MAKELONG(uSlot, uCat);

					INDEX Index = m_FavList.Find(uData);

					if( !m_FavList.Failed(Index) ) {

						m_FavList.Remove(Index);
						}
					else
						m_FavList.Insert(uData);

					if( m_pIndex[m_uCat] < NOTHING ) {

						pSym->m_fMark = !pSym->m_fMark;

						CRect     Space = CRect(m_ThumbSize);

						CMemoryDC WorkDC;

						WorkDC.Select(m_pImageThumb[m_uPress]);

						if( !OnGetAlpha(m_uPress) ) {

							WorkDC.FillRect(Space, afxBrush(MAGENTA));

							OnDrawImage(WorkDC, Space, m_uPress);

							m_pImageThumb[m_uPress].SetAlpha(m_pImageBits[m_uPress]);
							}
						else {
							memset(m_pImageBits[m_uPress], 0, 4 * Space.cx() * Space.cy());

							OnDrawImage(WorkDC, Space, m_uPress);
							}

						WorkDC.Deselect();
						}
					else {
						ThumbsLeave();

						ThumbsEnter();
						}

					Invalidate(FALSE);
					}
				}
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Image Hooks

void CSymbolResourceWnd::OnBuildCatList(void)
{
	UINT uCount = Sym_GetCatCount();

	UINT uCat;

	for( uCat = 0; uCat < uCount; uCat++ ) {

		CString Desc = Sym_GetCatDesc(uCat);

		Desc += CPrintf(L"|%u", uCat);

		m_CatName.Append(Desc);
		}

	m_pIndex = New UINT [ 1 + uCount ];

	m_CatName.Sort();

	for( uCat = 0; uCat < uCount; uCat++ ) {

		CString Line = m_CatName[uCat];

		CString Desc = Line.StripToken('|');

		m_pIndex[uCat+1] = watoi(Line);

		m_CatName.SetAt(uCat, Desc);
		}

	m_pIndex[0] = NOTHING;

	m_CatName.Insert(0, CString(IDS_FAVORITES));

	LoadFavorites();

	LoadBadSymbols();
	}

void CSymbolResourceWnd::OnLoadImages(void)
{
	if( m_uMode == 1 ) {

		UINT  uCount;

		UINT  uIndex;

		INDEX FavNdx = NULL;

		if( (uIndex = m_pIndex[m_uCat]) < NOTHING ) {

			uCount  = Sym_GetCatSlots(uIndex);

			m_Title = Sym_GetCatDesc(uIndex);
			}
		else {
			uCount  = m_FavList.GetCount();

			FavNdx  = m_FavList.GetHead();

			m_Title = CString(IDS_FAVORITES);
			}

		for( UINT uScan = 0; uScan < uCount; uScan++ ) {

			UINT uCat, uSlot;

			if( uIndex == NOTHING ) {

				uCat  = HIWORD(m_FavList[FavNdx]);

				uSlot = LOWORD(m_FavList[FavNdx]);

				m_FavList.GetNext(FavNdx);
				}
			else {
				uCat  = uIndex;

				uSlot = uScan;

				if( !Sym_IsValid(uCat, uSlot) ) {

					continue;
					}

				if( IsBadSymbol(uCat, uSlot) ) {
					
					continue;
					}
				}

			CSym *pSym = New CSym;

			pSym->m_uCat  = uCat;

			pSym->m_uSlot = uSlot;

			pSym->m_fMark = FALSE;

			pSym->m_uType = Sym_GetType(uCat, uSlot);

			pSym->m_Text  = Sym_GetDesc(uCat, uSlot);

			if( uIndex < NOTHING ) {

				DWORD uData = MAKELONG(uSlot, uCat);

				if( !m_FavList.Failed(m_FavList.Find(uData)) ) {

					pSym->m_fMark = TRUE;
					}
				}

			switch( pSym->m_uType ) {

				case typeBMP:
				case typeTEX:

					pSym->m_hBits = Sym_LoadBitmap(uCat, uSlot);

					pSym->m_uBits = Sym_LoadData  (uCat, uSlot, pSym->m_pBits);

					break;

				case typeEMF:

					pSym->m_hMeta = Sym_LoadMetaFile(uCat, uSlot);

					break;

				case typeXAML:

					pSym->m_Xaml.Printf(L"SYM:%u,%u,6", uCat, Sym_GetHandle(uCat, uSlot));

					break;

				default:

					delete pSym;

					continue;
				}

			if( Sym_CanShade(uCat, uSlot) ) {

				if( pSym->m_uType == typeEMF ) {

						Recolor( pSym->m_hMeta,
								GetRGB(12,12,24),
								GetRGB(24,24,24),
								TRUE
								);
						}

				if( pSym->m_uType == typeXAML ) {

					Recolor( pSym->m_Xaml,
							GetRGB(12,12,24),
							1
							);
					}
				}

			m_SymList.Append(pSym);
			}
		}

	if( m_uMode == 2 ) {

		UINT uCat  = m_pIndex[m_uCat];

		UINT uSlot = m_uImage;

		if( uCat == NOTHING ) {

			INDEX FavNdx = m_FavList.GetHead();

			while( uSlot-- ) {

				m_FavList.GetNext(FavNdx);
				}

			uCat  = HIWORD(m_FavList[FavNdx]);

			uSlot = LOWORD(m_FavList[FavNdx]);
			}

		UINT uType = Sym_GetType(uCat, uSlot);

		if( uType == typeBMP || uType == typeTEX ) {

			CSym *pSym = New CSym;

			pSym->m_uCat  = uCat;

			pSym->m_uSlot = uSlot;

			pSym->m_fMark = FALSE;

			pSym->m_uType = uType;

			pSym->m_Text  = Sym_GetDesc   (uCat, uSlot);

			pSym->m_hBits = Sym_LoadBitmap(uCat, uSlot);

			pSym->m_uBits = Sym_LoadData  (uCat, uSlot, pSym->m_pBits);

			m_SymList.Append(pSym);
			}

		if( uType == typeEMF ) {

			UINT uCount = GetColSchemeCount();

			BOOL fShade = Sym_CanShade(uCat, uSlot);

			UINT uBase  = fShade ? 0 : 1;

			UINT uMode  = fShade ? 1 : 0;

			for( UINT uScan = 0; uScan < uCount + uBase; uScan++ ) {

				CSym *pSym = New CSym;

				pSym->m_uCat  = uCat;

				pSym->m_uSlot = uSlot;

				pSym->m_fMark = FALSE;

				pSym->m_uType = uType;

				pSym->m_Text  = Sym_GetDesc     (uCat, uSlot);

				pSym->m_hMeta = Sym_LoadMetaFile(uCat, uSlot);

				if( uScan >= uBase ) {

					UINT uScheme = uScan - uBase;

					Recolor( pSym->m_hMeta,
						 GetColSchemeFill(uScheme),
						 GetColSchemeEdge(uScheme),
						 uMode
						 );
					}

				m_SymList.Append(pSym);
				}
			}

		if( uType == typeXAML ) {

			UINT uCount = GetColSchemeCount();

			BOOL fShade = Sym_CanShade(uCat, uSlot);

			UINT uBase  = fShade ? 0 : 1;

			UINT uMode  = 1;

			for( UINT uScan = 0; uScan < uCount + uBase; uScan++ ) {

				CSym *pSym = New CSym;

				pSym->m_uCat  = uCat;

				pSym->m_uSlot = uSlot;

				pSym->m_fMark = FALSE;

				pSym->m_uType = uType;

				pSym->m_Text  = Sym_GetDesc(uCat, uSlot);

				pSym->m_Xaml  = CPrintf( L"SYM:%u,%u,6", 
							 uCat, 
							 Sym_GetHandle(uCat, uSlot)
							 );

				if( uScan >= uBase ) {

					UINT uScheme = uScan - uBase;

					Recolor( pSym->m_Xaml,
						 GetColSchemeFill(uScheme),
						 uMode
						 );
					}

				m_SymList.Append(pSym);
				}
			}

		m_Title = m_SymList[0]->m_Text;
		}

	m_uImageCount = m_SymList.GetCount();
	}

void CSymbolResourceWnd::OnFreeImages(void)
{
	for( UINT n = 0; n < m_SymList.GetCount(); n++ ) {

		CSym *pSym = m_SymList[n];

		switch( pSym->m_uType ) {

			case typeBMP:
			case typeTEX:

				DeleteObject(pSym->m_hBits);

				delete [] pSym->m_pBits;

				break;

			case typeEMF:

				DeleteEnhMetaFile(pSym->m_hMeta);

				break;
			}

		delete pSym;
		}
	
	m_SymList.Empty();
	}

CString CSymbolResourceWnd::OnGetImageText(UINT uImage)
{
	return m_SymList[uImage]->m_Text;
	}

void CSymbolResourceWnd::OnAddImageData(CDataObject *pData, UINT uImage)
{
	CSym * pSym     = m_SymList[uImage];

	DWORD  dwHandle = 0;

	switch( pSym->m_uType ) {

		case typeBMP:

			pData->AddGlobalData(m_cfBMP, pSym->m_pBits, pSym->m_uBits);

			break;

		case typeTEX:

			pData->AddGlobalData(m_cfTEX, pSym->m_pBits, pSym->m_uBits);

			break;

		case typeEMF:

			pData->AddEnhMetaFile(m_cfEMF, pSym->m_hMeta);

			break;

		case typeXAML:

			pData->AddText(m_cfSym, pSym->m_Xaml);

			return;
		}

	if( m_uMode == 1 || (m_uMode == 2 && uImage == 0) ) {

		dwHandle = Sym_GetHandle(pSym->m_uCat, pSym->m_uSlot);

		pData->AddText(m_cfSym, CPrintf( L"SYM:%u,%u,%u",
						 pSym->m_uCat,
						 dwHandle,
						 pSym->m_uType
						 ));
		}
	}

void CSymbolResourceWnd::OnDrawImage(CDC &DC, CRect Rect, UINT uImage)
{
	CSym *pSym  = m_SymList[uImage];

	if( pSym->m_uType == typeBMP || pSym->m_uType == typeTEX ) {

		CRect Work = Rect - 2;

		BITMAPINFOHEADER *pHead = (BITMAPINFOHEADER *) pSym->m_pBits;

		CSize Size = CSize(pHead->biWidth, pHead->biHeight);

		CSize Draw = Work.GetSize();

		if( pSym->m_uType == typeTEX ) {

			CMemoryDC WorkDC(DC);

			HGDIOBJ   hPrev = WorkDC.SelectObject(pSym->m_hBits);

			CPoint    Pos   = Work.GetTopLeft();

			while( Pos.y < Work.bottom ) {

				Pos.x  = Work.left;

				int cy = min(Size.cy, Work.bottom - Pos.y);

				while( Pos.x < Work.right ) {

					int cx = min(Size.cx, Work.right  - Pos.x);

					BitBlt( DC,
						Pos.x,
						Pos.y,
						cx,
						cy,
						WorkDC,
						0,
						0,
						SRCCOPY
						);

					Pos.x += cx;
					}

				Pos.y += cy;
				}

			if( pSym->m_fMark ) {

				DrawFavMarker(DC, Rect);
				}

			WorkDC.SelectObject(hPrev);
			}
		else {
			double xDiv = double(Draw.cx) / Size.cx;

			double yDiv = double(Draw.cy) / Size.cy;

			double nDiv = min(xDiv, yDiv);

			MakeMin(nDiv, 1.0);

			Draw.cx = int(Size.cx * nDiv);

			Draw.cy = int(Size.cy * nDiv);

			Work.left += (Work.cx() - Draw.cx) / 2;

			Work.top  += (Work.cy() - Draw.cy) / 2;

			GpBitmap *pImage = NULL;

			GdipCreateBitmapFromHBITMAP( pSym->m_hBits,
						     NULL,
						     &pImage
						     );

			GpGraphics *pGraph = NULL;

			GdipCreateFromHDC(DC.GetHandle(), &pGraph);

			GdipDrawImageRectRectI( pGraph,
						pImage,
						Work.left,
						Work.top,
						Draw.cx,
						Draw.cy,
						0,
						0,
						Size.cx,
						Size.cy,
						UnitPixel,
						NULL,
						NULL,
						NULL
						);

			if( pSym->m_fMark ) {

				DrawFavMarker(pGraph, Rect);
				}

			GdipDeleteGraphics(pGraph);

			GdipDisposeImage(pImage);
			}
		}

	if( pSym->m_uType == typeEMF ) {

		CRect Work = Rect;

		ENHMETAHEADER Head;

		GetEnhMetaFileHeader(pSym->m_hMeta, sizeof(Head), &Head);

		int cx = Head.rclBounds.right  - Head.rclBounds.left;

		int cy = Head.rclBounds.bottom - Head.rclBounds.top;

		if( cx > cy ) {

			int ty = Work.cx() * cy / cx;

			int dy = Work.cy() - ty;

			if( dy > 0 ) {

				Work.top    += (dy + 1) / 2;

				Work.bottom -= (dy + 0) / 2;
				}
			}

		if( cy > cx ) {

			int tx = Work.cy() * cx / cy;

			int dx = Work.cx() - tx;

			if( dx > 0 ) {

				Work.left  += (dx + 1) / 2;

				Work.right -= (dx + 0) / 2;
				}
			}

		PlayEnhMetaFile(DC, pSym->m_hMeta, Work - 2);

		if( pSym->m_fMark ) {

			DrawFavMarker(DC, Rect);
			}
		}

	if( pSym->m_uType == typeXAML ) {

		CString File = pSym->m_Xaml;

		if( File.StartsWith(L"SYM:") ) {

			CStringArray List;

			File.Mid(4).Tokenize(List, ',');

			UINT  uCat     = wcstoul(List[0], NULL, 10);
			DWORD dwHandle = wcstoul(List[1], NULL, 10);
		//	UINT  uType    = wcstoul(List[2], NULL, 10);
			UINT  uMode    = wcstoul(List[3], NULL, 10);
			DWORD Fill     = wcstoul(List[4], NULL, 16);
			DWORD Back     = wcstoul(List[5], NULL, 16);

			UINT  uSlot    = Sym_FindByHandle(uCat, dwHandle);

			CSize Size     = Rect.GetSize();

			if( Size.cx < 0 ) Size.cx = 200;

			if( Size.cy < 0 ) Size.cy = 200;

			CByteArray Data;

			if( Sym_LoadPng(uCat, uSlot, uMode, Fill, Back, Size, TRUE, Data) ) {

				UINT    uSize = Data.GetCount();

				HGLOBAL hCopy = GlobalAlloc(GHND, uSize);

				AfxAssume(hCopy);

				PBYTE   pCopy = PBYTE(GlobalLock(hCopy));

				memcpy(pCopy, Data.GetPointer(), uSize);

				GlobalUnlock(hCopy);

				IStream *pStream = NULL;

				GpImage *pImage  = NULL;

				CreateStreamOnHGlobal(hCopy, TRUE, &pStream);

				GdipLoadImageFromStream(pStream, &pImage);

				if( pImage ) {

					CRect Work = Rect;

					int cx = 0;

					int cy = 0;

					GdipGetImageWidth (pImage, PUINT(&cx));

					GdipGetImageHeight(pImage, PUINT(&cy));

					if( cx > cy ) {

						int ty = Work.cx() * cy / cx;

						int dy = Work.cy() - ty;

						if( dy > 0 ) {

							Work.top    += (dy + 1) / 2;

							Work.bottom -= (dy + 0) / 2;
							}
						}

					if( cy > cx ) {

						int tx = Work.cy() * cx / cy;

						int dx = Work.cx() - tx;

						if( dx > 0 ) {

							Work.left  += (dx + 1) / 2;

							Work.right -= (dx + 0) / 2;
							}
						}

					GpGraphics *pGraph = NULL;

					GdipCreateFromHDC(DC.GetHandle(), &pGraph);

					GdipDrawImageRectI( pGraph,
							    pImage,
							    Work.left,
							    Work.top,
							    Work.cx(),
							    Work.cy()
							    );

					if( pSym->m_fMark ) {

						DrawFavMarker(pGraph, Rect);
						}

					GdipDeleteGraphics(pGraph);

					GdipDisposeImage(pImage);
					}

				pStream->Release();
				}
			else {
				CRect Draw = Rect - 1;

				CPrintf Text(L"Sym %u", dwHandle);

				DC.Select(afxFont(Dialog));

				DC.RoundRect(Draw, 18);

				CSize Extent = DC.GetTextExtent(Text);

				int px = (Draw.GetWidth() - Extent.cx) / 2;

				int py = (Draw.GetHeight() - Extent.cy) / 2;

				DC.TextOut(px, py, Text);

				DC.Deselect();
				}
			}
		else {
			CRect Draw = Rect;

			DC.RoundRect(Draw, 18);
			}
		}
	}

BOOL CSymbolResourceWnd::OnGetAlpha(UINT uImage)
{
	CSym *pSym = m_SymList[uImage];

	switch( pSym->m_uType ) {
		
		case typeBMP:
			return TRUE;
			
		case typeTEX:
			return FALSE;

		case typeEMF:
			return FALSE;

		case typeXAML:
			return TRUE;
		}

	return FALSE;
	}

// Implementation

void CSymbolResourceWnd::Recolor(CString &Xaml, COLOR Fill, UINT uMode)
{
	if( Xaml.StartsWith(L"SYM:") ) {

		CStringArray List;

		Xaml.Mid(4).Tokenize(List, ',');

		UINT  uCat     = wcstoul(List[0], NULL, 10);
		DWORD dwHandle = wcstoul(List[1], NULL, 10);
		UINT  uType    = wcstoul(List[2], NULL, 10);

		uMode = 1;

		Xaml.Printf( L"SYM:%u,%u,%u,%u,FF%6.6X",
			     uCat,
			     dwHandle,
			     uType,
			     uMode,
			     ConvertRev(Fill, 255)
			     );
		}
	}

void CSymbolResourceWnd::Recolor(HENHMETAFILE &hMeta, COLOR Fill, COLOR Edge, UINT uMode)
{
	UINT  uSize = GetEnhMetaFileBits(hMeta, 0, NULL);

	PBYTE pData = New BYTE [ uSize ];

	PBYTE pWalk = pData;

	GetEnhMetaFileBits(hMeta, uSize, pData);

	DeleteEnhMetaFile (hMeta);

	for(;;) {

		EMR *pRec = (EMR *) pWalk;

		if( pRec->iType == EMR_EOF ) {

			break;
			}

		if( pRec->iType == EMR_CREATEBRUSHINDIRECT ) {

			EMRCREATEBRUSHINDIRECT *p = (EMRCREATEBRUSHINDIRECT *) pRec;

			int nWeight   = (uMode == 1) ? 255 : GetWeight(p->lb.lbColor);

			p->lb.lbColor = ConvertFwd(Fill, nWeight);
			}

		if( pRec->iType == EMR_CREATEPEN ) {

			EMRCREATEPEN *p = (EMRCREATEPEN *) pRec;

			int nWeight       = (uMode == 1) ? 255 : GetWeight(p->lopn.lopnColor);

			p->lopn.lopnColor = ConvertFwd(Edge, nWeight);
			}

		pWalk += pRec->nSize;
		}

	hMeta = SetEnhMetaFileBits(uSize, pData);

	delete[] pData;
	}

int CSymbolResourceWnd::GetWeight(DWORD rgb)
{
	BYTE r = GetRValue(rgb);
	BYTE g = GetGValue(rgb);
	BYTE b = GetBValue(rgb);

	int  n = 0;

	if( r ) n++;
	if( g ) n++;
	if( b ) n++;

	return n ? int(r+g+b) / n : 0;
	}

BOOL CSymbolResourceWnd::IsGrey(DWORD rgb)
{
	BYTE r = GetRValue(rgb);
	BYTE g = GetGValue(rgb);
	BYTE b = GetBValue(rgb);

	return r == g && g == b;
	}

DWORD CSymbolResourceWnd::ConvertFwd(COLOR Col, int nWeight)
{
	BYTE r = BYTE(GetRED  (Col) * nWeight / 255);
	BYTE g = BYTE(GetGREEN(Col) * nWeight / 255);
	BYTE b = BYTE(GetBLUE (Col) * nWeight / 255);

	r *= 8;
	g *= 8;
	b *= 8;

	if( r ) r += 4;
	if( g ) g += 4;
	if( b ) b += 4;

	return RGB(r,g,b);
	}

DWORD CSymbolResourceWnd::ConvertRev(COLOR Col, int nWeight)
{
	BYTE r = BYTE(GetRED  (Col) * nWeight / 255);
	BYTE g = BYTE(GetGREEN(Col) * nWeight / 255);
	BYTE b = BYTE(GetBLUE (Col) * nWeight / 255);

	r *= 8;
	g *= 8;
	b *= 8;

	if( r ) r += 4;
	if( g ) g += 4;
	if( b ) b += 4;

	return RGB(b,g,r);
	}

UINT CSymbolResourceWnd::GetColSchemeCount(void)
{
	CCommsSystem  *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CColorManager *pColor  = pSystem->m_pColor;

	UINT ng = pColor->GetGroupCount();

	UINT nc = 0;

	for( UINT g = 0; g < ng; g++ ) {

		if( g < 2 || g > 5 ) {

			nc += pColor->GetCount(g);
			}
		}

	return nc - 1;
	}

COLOR CSymbolResourceWnd::GetColSchemeFill(UINT uScheme)
{
	CCommsSystem  *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CColorManager *pColor  = pSystem->m_pColor;

	UINT ng = pColor->GetGroupCount();

	UINT nc = 0;

	for( UINT g = 0; g < ng; g++ ) {

		UINT tg;

		if( g < ng - 6 ) {

			tg = 6 + g;
			}
		else
			tg = g - ng + 2;

		if( tg < 2 || tg > 5 ) {

			if( tg == 0 ) {

				uScheme = uScheme + 1;
				}

			if( uScheme >= nc && uScheme < nc + pColor->GetCount(tg) ) {

				return pColor->GetColor(tg, uScheme - nc);
				}

			nc += pColor->GetCount(tg);
			}
		}

	return 0;
	}

COLOR CSymbolResourceWnd::GetColSchemeEdge(UINT uScheme)
{
	return GetRGB(24,24,24);
	}

void CSymbolResourceWnd::LoadFavorites(void)
{
	CModule * pApp  = afxModule->GetApp();

	CFilename Path  = pApp->GetFolder(CSIDL_LOCAL_APPDATA, NULL);

	FILE    * pFile = _wfopen(Path.WithName(L"favsym.dat"), L"rb");

	if( pFile ) {

		while( !feof(pFile) ) {

			UINT  uCat     = NOTHING;

			DWORD dwHandle = NOTHING;

			fread(&uCat,     sizeof(uCat),     1, pFile);

			fread(&dwHandle, sizeof(dwHandle), 1, pFile);

			if( uCat < NOTHING ) {

				UINT uSlot = Sym_FindByHandle(uCat, dwHandle);

				if( uSlot < NOTHING ) {

					DWORD uData = MAKELONG(uSlot, uCat);

					m_FavList.Insert(uData);
					}
				}
			else
				break;
			}

		fclose(pFile);
		}
	}

void CSymbolResourceWnd::SaveFavorites(void)
{
	CModule * pApp  = afxModule->GetApp();

	CFilename Name  = pApp->GetFolder(CSIDL_LOCAL_APPDATA, NULL).WithName(L"favsym.dat");

	FILE    * pFile = _wfopen(Name, L"wb");

	if( pFile ) {

		INDEX Index = m_FavList.GetHead();

		while( !m_FavList.Failed(Index) ) {

			UINT  uCat     = HIWORD(m_FavList[Index]);

			UINT  uSlot    = LOWORD(m_FavList[Index]);

			DWORD dwHandle = Sym_GetHandle(uCat, uSlot);

			fwrite(&uCat,     sizeof(uCat),     1, pFile);

			fwrite(&dwHandle, sizeof(dwHandle), 1, pFile);

			m_FavList.GetNext(Index);
			}

		fclose(pFile);
		}
	}

void CSymbolResourceWnd::DrawFavMarker(CDC &DC, CRect Rect)
{
	GpGraphics *pGraph = NULL;

	GdipCreateFromHDC(DC.GetHandle(), &pGraph);

	DrawFavMarker(pGraph, Rect);

	GdipDeleteGraphics(pGraph);
	}

void CSymbolResourceWnd::DrawFavMarker(GpGraphics *pGraph, CRect Rect)
{
	Rect.left   = Rect.left   + 2;

	Rect.bottom = Rect.bottom - 2;

	Rect.right  = Rect.left   + 8;

	Rect.top    = Rect.bottom - 8;

	GpPath *pPath = NULL;

	GdipCreatePath(FillModeAlternate, &pPath);

	GdipAddPathRectangle( pPath,
			      Gdiplus::REAL(Rect.left),
			      Gdiplus::REAL(Rect.top),
			      Gdiplus::REAL(Rect.right - Rect.left),
			      Gdiplus::REAL(Rect.bottom - Rect.top)
			      );

	GpSolidFill *pBrush = NULL;

	GpPen       *pPen   = NULL;

	GdipCreateSolidFill(0xF0E040 | ALPHA_MASK, &pBrush);

	GdipCreatePen1     (0x404040 | ALPHA_MASK, 1.0, UnitPixel, &pPen);

	GdipFillPath(pGraph, pBrush, pPath);

	GdipDrawPath(pGraph, pPen, pPath);

	GdipDeletePen(pPen);

	GdipDeleteBrush(pBrush);
	}

void CSymbolResourceWnd::LoadBadSymbols(void)
{
	m_BadList.Insert(MAKELONG( 46, 5));
	m_BadList.Insert(MAKELONG(121, 9));
	m_BadList.Insert(MAKELONG( 50,20));
	m_BadList.Insert(MAKELONG(  7,38));
	m_BadList.Insert(MAKELONG(  9,38));
	m_BadList.Insert(MAKELONG( 65,42));
	m_BadList.Insert(MAKELONG( 25,61));
	m_BadList.Insert(MAKELONG( 18,70));
	m_BadList.Insert(MAKELONG( 17,57));
	}

BOOL CSymbolResourceWnd::IsBadSymbol(UINT uCat, UINT uSlot)
{
	DWORD uData = MAKELONG(uSlot, uCat);

	INDEX Index = m_BadList.Find(uData);

	return !m_BadList.Failed(Index);
	}

// End of File
