
#include "intern.hpp"

#include "PrimRubyBalloon.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Balloon Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyBalloon, CPrimRubyOriented);

// Constructor

CPrimRubyBalloon::CPrimRubyBalloon(void)
{
	m_Orient = 0;

	m_Min2   =  2000;
	m_Max2   =  8000;
	m_Pos2   =  5000;

	m_Min3   =     0;
	m_Max3   = 10000;
	m_Pos3   =  2500;

	m_Min1.x =   500;
	m_Min1.y =   500;
	m_Max1.x =  4000;
	m_Max1.y =  m_Pos2 / 2;
	m_Pos1.x =  1000;
	m_Pos1.y =  1000;
	}

// Meta Data

void CPrimRubyBalloon::AddMetaData(void)
{
	CPrimRubyOriented::AddMetaData();

	Meta_AddPoint  (Pos1);
	Meta_AddInteger(Pos2);
	Meta_AddInteger(Pos3);

	Meta_SetName((IDS_BALLOON_2));
	}

// Overridables

void CPrimRubyBalloon::UpdateLayout(void)
{
	m_Max1.y = m_Pos2 / 2;

	MakeMin(m_Pos1.y, m_Max1.y);

	CPrimRubyOriented::UpdateLayout();
	}

BOOL CPrimRubyBalloon::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		Hand.m_pTag = L"Pos1";

		switch( m_Orient ) {

			case 0: // Down
			case 4: // Arbitary
				Hand.m_uRef  = 100;
				Hand.m_Pos.x = m_Pos1.x;
				Hand.m_Pos.y = m_Pos1.y;
				Hand.m_Clip  = CRect(m_Min1.x, m_Min1.y, m_Max1.x, m_Max1.y);
				break;

			case 1: // Up
				Hand.m_uRef  = 102;
				Hand.m_Pos.x = m_Pos1.x;
				Hand.m_Pos.y = m_Pos1.y;
				Hand.m_Clip  = CRect(m_Min1.x, m_Min1.y, m_Max1.x, m_Max1.y);
				break;

			case 2: // Right
				Hand.m_uRef  = 103;
				Hand.m_Pos.x = m_Pos1.x;
				Hand.m_Pos.y = m_Pos1.y;
				Hand.m_Clip  = CRect(m_Min1.x, m_Min1.y, m_Max1.x, m_Max1.y);
				break;

			case 3: // Left
				Hand.m_uRef  = 101;
				Hand.m_Pos.x = m_Pos1.x;
				Hand.m_Pos.y = m_Pos1.y;
				Hand.m_Clip  = CRect(m_Min1.x, m_Min1.y, m_Max1.x, m_Max1.y);
				break;
			}

		Hand.m_uRef ^= m_Reflect;

		return TRUE;
		}

	if( uHand == 1 ) {

		UINT uInset = 0;

		Hand.m_pTag = L"Pos2";

		switch( m_Orient ) {

			case 0: // Down
			case 4: // Arbitary
				Hand.m_uRef  = 100;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos2;
				Hand.m_Clip  = CRect(uInset, m_Min2, uInset, m_Max2);
				break;

			case 1: // Up
				Hand.m_uRef  = 102;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos2;
				Hand.m_Clip  = CRect(uInset, m_Min2, uInset, m_Max2);
				break;

			case 2: // Right
				Hand.m_uRef  = 103;
				Hand.m_Pos.x = m_Pos2;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(m_Min2, uInset, m_Max2, uInset);
				break;

			case 3: // Left
				Hand.m_uRef  = 101;
				Hand.m_Pos.x = m_Pos2;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(m_Min2, uInset, m_Max2, uInset);
				break;
			}

		Hand.m_uRef ^= m_Reflect;

		return TRUE;
		}

	if( uHand == 2 ) {

		UINT uInset = 0;

		Hand.m_pTag = L"Pos3";

		switch( m_Orient ) {

			case 0: // Down
			case 4: // Arbitary
				Hand.m_uRef  = 103;
				Hand.m_Pos.x = m_Pos3;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(m_Min3, uInset, m_Max3, uInset);
				break;

			case 1: // Up
				Hand.m_uRef  = 101;
				Hand.m_Pos.x = m_Pos3;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(m_Min3, uInset, m_Max3, uInset);
				break;

			case 2: // Right
				Hand.m_uRef  = 102;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos3;
				Hand.m_Clip  = CRect(uInset, m_Min3, uInset, m_Max3);
				break;

			case 3: // Left
				Hand.m_uRef  = 100;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos3;
				Hand.m_Clip  = CRect(uInset, m_Min3, uInset, m_Max3);
				break;
			}

		Hand.m_uRef ^= m_Reflect;

		return TRUE;
		}

	return FALSE;
	}

void CPrimRubyBalloon::FindTextRect(void)
{
	m_TextRect = m_DrawRect;

	int nAdjust = m_pEdge->GetInnerWidth();

	DeflateRect(m_TextRect, nAdjust, nAdjust);

	int cx = m_TextRect.x2 - m_TextRect.x1;

	int cy = m_TextRect.y2 - m_TextRect.y1;

	switch( m_Orient ) {

		case 0: // Down
			if( m_Reflect == 2 || m_Reflect == 3 )
				m_TextRect.y1 = m_TextRect.y2 - (m_Pos2 * cy) / 10000;
			else
				m_TextRect.y2 = m_TextRect.y1 + (m_Pos2 * cy) / 10000;
			break;

		case 1: // Up
			if( m_Reflect == 2 || m_Reflect == 3 )
				m_TextRect.y2 = m_TextRect.y1 + (m_Pos2 * cy) / 10000;
			else
				m_TextRect.y1 = m_TextRect.y2 - (m_Pos2 * cy) / 10000;
			break;

		case 2: // Right
			if( m_Reflect == 1 || m_Reflect == 2 )
				m_TextRect.x1 = m_TextRect.x2 - (m_Pos2 * cx) / 10000;
			else
				m_TextRect.x2 = m_TextRect.x1 + (m_Pos2 * cx) / 10000;
			break;

		case 3: // Left
			if( m_Reflect == 1 || m_Reflect == 2 )
				m_TextRect.x2 = m_TextRect.x1 + (m_Pos2 * cx) / 10000;
			else
				m_TextRect.x1 = m_TextRect.x2 - (m_Pos2 * cx) / 10000;
			break;
		}

	// TODO -- All of this is a lot of work!
	}

// Path Generation

void CPrimRubyBalloon::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	CRubyVector dp = p2 - p1;

	CRubyVector r;

	r.m_x = (m_Pos1.x * dp.m_x) / 10000;
	r.m_y = (m_Pos1.y * dp.m_y) / 10000;

	CRubyPoint b2, c1, c2, c3, c4, c5, c6, c7, c8, p3, p4, cp, pi, pa, pb, pc;

	b2.m_x = p2.m_x;
	b2.m_y = p1.m_y + (m_Pos2 * dp.m_y) / 10000;

	c1.m_x = p1.m_x + r.m_x;
	c1.m_y = p1.m_y ;

	c2.m_x = b2.m_x - r.m_x;
	c2.m_y = p1.m_y ;

	c3.m_x = b2.m_x ;
	c3.m_y = p1.m_y + r.m_y;

	c4.m_x = b2.m_x ;
	c4.m_y = b2.m_y - r.m_y;

	c5.m_x = b2.m_x - r.m_x;
	c5.m_y = b2.m_y ;

	c6.m_x = p1.m_x + r.m_x;
	c6.m_y = b2.m_y ;

	c7.m_x = p1.m_x ;
	c7.m_y = b2.m_y - r.m_y;

	c8.m_x = p1.m_x ;
	c8.m_y = p1.m_y + r.m_y;

	p3.m_x = b2.m_x;
	p3.m_y = p1.m_y;

	p4.m_x = p1.m_x;
	p4.m_y = b2.m_y;

	cp.m_x = c2.m_x;
	cp.m_y = c3.m_y;

	CRubyDraw::Arc(figure, cp, c2, c3, +r);

	cp.m_x = c5.m_x;
	cp.m_y = c4.m_y;

	CRubyDraw::Arc(figure, cp, c4, c5, +r);
	
	pi.m_x = (p1.m_x + p2.m_x) / 2;
	pi.m_y = b2.m_y;

	pa.m_x = pi.m_x + dp.m_x / 10;
	pa.m_y = pi.m_y ;

	pb.m_x = p1.m_x + (m_Pos3 * dp.m_x) / 10000;
	pb.m_y = p2.m_y;

	pc.m_x = pi.m_x - dp.m_x / 10;
	pc.m_y = pi.m_y ;

	figure.Append(pa);
	figure.Append(pb);
	figure.Append(pc);

	cp.m_x = c6.m_x;
	cp.m_y = c7.m_y;

	CRubyDraw::Arc(figure, cp, c6, c7, +r);

	cp.m_x = c1.m_x;
	cp.m_y = c8.m_y;

	CRubyDraw::Arc(figure, cp, c8, c1, +r);

	figure.AppendHardBreak();
	}

// End of File
