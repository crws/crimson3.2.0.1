
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubySemi_HPP
	
#define	INCLUDE_PrimRubySemi_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPartial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Semi Primitive
//

class CPrimRubySemi : public CPrimRubyPartial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubySemi(void);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Generation
		void MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
