
#include "intern.hpp"

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Generic Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimViewer, CPrim);

// Constructor

CPrimViewer::CPrimViewer(void)
{
	m_pBack    = New CPrimColor;

	m_FontWork = fontHei16;

	m_FontMenu = fontHei16;
}

// Initial Values

void CPrimViewer::SetInitValues(void)
{
	m_pBack->SetInitial(GetRGB(31, 31, 31));
}

// UI Update

void CPrimViewer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	CPrim::OnUIChange(pHost, pItem, Tag);
}

// Overridables

void CPrimViewer::Draw(IGDI *pGDI, UINT uMode)
{
	MakeList();

	SelectFont(pGDI, m_FontMenu);

	pGDI->SetTextFore(GetRGB(0, 0, 0));

	pGDI->SetTextTrans(modeTransparent);

	R2 Menu = m_DrawRect;

	R2 Work = Menu;

	Menu.top    = Menu.bottom - GetButtonHeight(pGDI);

	Work.bottom = Menu.top;

	pGDI->ResetBrush();

	pGDI->SetBrushFore(m_pBack->GetColor());

	pGDI->FillRect(PassRect(Work));

	pGDI->SetBrushFore(GetRGB(16, 16, 16));

	pGDI->FillRect(PassRect(Menu));

	int c = m_List.GetCount();

	for( int n = 0; n < c; n++ ) {

		R2 Rect;

		Rect.x1 = Menu.x1 + (n + 0) * (Menu.x2 - Menu.x1) / c + 1;

		Rect.x2 = Menu.x1 + (n + 1) * (Menu.x2 - Menu.x1) / c - 1;

		Rect.y1 = Menu.y1 + 1;

		Rect.y2 = Menu.y2 - 1;

		DrawButton(pGDI, Rect, m_List[n]);
	}

	SelectFont(pGDI, m_FontWork);

	pGDI->TextOut(Work.x1 + 1, Work.y1 + 1, UniVisual(m_Work));

	DrawNotSupported(pGDI);
}

void CPrimViewer::SetInitState(void)
{
	CPrim::SetInitState();

	SetInitSize(320, 240);
}

void CPrimViewer::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_FontWork);

	GetFontRef(Refs, m_FontMenu);
}

void CPrimViewer::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_FontWork, uOld, uNew);

	EditFontRef(m_FontMenu, uOld, uNew);
}

// Download Support

BOOL CPrimViewer::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddWord(WORD(m_FontWork));

	Init.AddWord(WORD(m_FontMenu));

	Init.AddItem(itemSimple, m_pBack);

	return TRUE;
}

// Meta Data

void CPrimViewer::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddInteger(FontWork);
	Meta_AddInteger(FontMenu);
	Meta_AddObject(Back);

	Meta_SetName((IDS_GENERIC_VIEWER));
}

// Overridables

BOOL CPrimViewer::MakeList(void)
{
	return FALSE;
}

// Drawing Helpers

int CPrimViewer::GetButtonHeight(IGDI *pGDI)
{
	return pGDI->GetTextHeight(L"X") + 16;
}

void CPrimViewer::DrawButton(IGDI *pGDI, R2 Rect, CString Text)
{
	R2 OuterRect = Rect;

	R2 InnerRect = OuterRect;

	int bb = 4;

	DeflateRect(InnerRect, bb, bb);

	CRubyPoint p1(OuterRect, 1);
	CRubyPoint p2(OuterRect, 3);
	CRubyPoint w1(InnerRect, 1);
	CRubyPoint w2(InnerRect, 3);

	CRubyPath m_pathFill, m_pathHilite, m_pathShadow;

	CRubyDraw::Rectangle(m_pathFill, w1, w2);

	////////

	m_pathHilite.Append(CRubyPoint(p1.m_x, p1.m_y));
	m_pathHilite.Append(CRubyPoint(w1.m_x, w1.m_y));
	m_pathHilite.Append(CRubyPoint(w1.m_x, w2.m_y));
	m_pathHilite.Append(CRubyPoint(p1.m_x, p2.m_y));

	m_pathHilite.AppendHardBreak();

	m_pathHilite.Append(CRubyPoint(p1.m_x, p1.m_y));
	m_pathHilite.Append(CRubyPoint(p2.m_x, p1.m_y));
	m_pathHilite.Append(CRubyPoint(w2.m_x, w1.m_y));
	m_pathHilite.Append(CRubyPoint(w1.m_x, w1.m_y));

	m_pathHilite.AppendHardBreak();

	////////

	m_pathShadow.Append(CRubyPoint(p2.m_x, p1.m_y));
	m_pathShadow.Append(CRubyPoint(p2.m_x, p2.m_y));
	m_pathShadow.Append(CRubyPoint(w2.m_x, w2.m_y));
	m_pathShadow.Append(CRubyPoint(w2.m_x, w1.m_y));

	m_pathShadow.AppendHardBreak();

	m_pathShadow.Append(CRubyPoint(p2.m_x, p2.m_y));
	m_pathShadow.Append(CRubyPoint(p1.m_x, p2.m_y));
	m_pathShadow.Append(CRubyPoint(w1.m_x, w2.m_y));
	m_pathShadow.Append(CRubyPoint(w2.m_x, w2.m_y));

	m_pathShadow.AppendHardBreak();

	////////

	CRubyGdiList m_listFill, m_listHilite, m_listShadow;

	m_listFill.Load(m_pathFill, true);

	m_listHilite.Load(m_pathHilite, true);

	m_listShadow.Load(m_pathShadow, true);

	////////

	CRubyGdiLink gdi(pGDI);

	gdi.OutputSolid(m_listHilite, GetRGB(24, 24, 24), 255);

	gdi.OutputSolid(m_listShadow, GetRGB(8, 8, 8), 255);

	gdi.OutputSolid(m_listFill, GetRGB(16, 16, 16), 255);

	////////

	int tx = pGDI->GetTextWidth(Text);

	int ty = pGDI->GetTextHeight(Text);

	int xp = Rect.x1 + (Rect.x2 - Rect.x1 - tx) / 2;

	int yp = Rect.y1 + (Rect.y2 - Rect.y1 - ty) / 2;

	pGDI->TextOut(xp, yp+1, UniVisual(Text));
}

void CPrimViewer::FindPoints(P2 *t, P2 *b, R2 &r, int s)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, s - 1, s - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;

	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
}

// Implementation

void CPrimViewer::DoEnables(IUIHost *pHost)
{
}

BOOL CPrimViewer::SelectFont(IGDI *pGDI, UINT Font)
{
	if( Font < 0x100 ) {

		pGDI->SelectFont(Font);

		return TRUE;
	}

	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	return pFonts->Select(pGDI, Font);
}

void CPrimViewer::DrawNotSupported(IGDI *pGDI)
{
	if( !IsSupported() ) {

		R2 Menu = m_DrawRect;

		R2 Work = Menu;

		Menu.top     = Menu.bottom - GetButtonHeight(pGDI);

		Work.bottom  = Menu.top;

		CString Text = IDS_NOT_SUPPORTED;

		pGDI->SelectFont(fontHei16);

		int cx = pGDI->GetTextWidth(Text);

		int cy = pGDI->GetTextHeight(Text);

		int px = (Work.x1 + Work.x2 - cx) / 2;

		int py = (Work.y1 + Work.y2 - cy) / 2;

		pGDI->SetTextFore(GetRGB(255, 0, 0));

		pGDI->TextOut(px, py, Text);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Alarm Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimAlarmViewer, CPrimViewer);

// Constructor

CPrimAlarmViewer::CPrimAlarmViewer(void)
{
	m_uType        = 0x1A;
	m_IncTime      = 1;
	m_IncMarks     = 1;
	m_IncNum       = 0;
	m_ColInactive  = MAKELONG(GetRGB(31, 31, 31), GetRGB(0, 16, 0));
	m_ColActive    = MAKELONG(GetRGB(31, 0, 0), GetRGB(31, 31, 31));
	m_ColAccepted  = MAKELONG(GetRGB(15, 15, 0), GetRGB(31, 31, 31));
	m_ColWaitAcc   = MAKELONG(GetRGB(0, 0, 15), GetRGB(31, 31, 31));
	m_pTxtEmpty    = NULL;
	m_pBtnPgUp     = NULL;
	m_pBtnPgDn     = NULL;
	m_pBtnPrev     = NULL;
	m_pBtnNext     = NULL;
	m_pBtnMute     = NULL;
	m_pBtnAccept   = NULL;
	m_pBtnHelp     = NULL;
	m_ShowPage     = 0;
	m_ShowMute     = 1;
	m_ShowAccept   = 1;
	m_ShowHelp     = 1;
	m_pOnHelp      = NULL;
	m_pFormat      = New CDispFormatTimeDate;
	m_Reverse      = 1;
	m_AutoScroll   = 30;
	m_UsePriority  = 0;
	m_ColPriority[0] = MAKELONG(GetRGB(31, 0, 0), GetRGB(31, 31, 31));
	m_ColPriority[1] = MAKELONG(GetRGB(0, 0, 0), GetRGB(31, 31, 31));
	m_ColPriority[2] = MAKELONG(GetRGB(31, 31, 0), GetRGB(31, 31, 31));
	m_ColPriority[3] = MAKELONG(GetRGB(0, 0, 31), GetRGB(31, 31, 31));
	m_ColPriority[4] = MAKELONG(GetRGB(31, 0, 31), GetRGB(31, 31, 31));
	m_ColPriority[5] = MAKELONG(GetRGB(0, 31, 31), GetRGB(31, 31, 31));
	m_ColPriority[6] = MAKELONG(GetRGB(15, 15, 15), GetRGB(31, 31, 31));
	m_ColPriority[7] = MAKELONG(GetRGB(8, 8, 8), GetRGB(31, 31, 31));
}

// Initial Values

void CPrimAlarmViewer::SetInitValues(void)
{
	CPrimViewer::SetInitValues();

	SetInitial(L"BtnPgUp", m_pBtnPgUp, L"\"PgUp\"");
	SetInitial(L"BtnPgDn", m_pBtnPgDn, L"\"PgDn\"");
	SetInitial(L"BtnPrev", m_pBtnPrev, L"\"Prev\"");
	SetInitial(L"BtnNext", m_pBtnNext, L"\"Next\"");
	SetInitial(L"BtnMute", m_pBtnMute, L"\"Mute\"");
	SetInitial(L"BtnAccept", m_pBtnAccept, L"\"Accept\"");
	SetInitial(L"BtnHelp", m_pBtnHelp, L"\"Help\"");
	SetInitial(L"TxtEmpty", m_pTxtEmpty, L"\"No Active Alarms\"");
}

// UI Update

void CPrimAlarmViewer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == L"UsePriority" ) {

		DoEnables(pHost);
	}

	if( Tag == L"Back" ) {

		COLOR Back = m_pBack->GetColor();

		m_ColActive    = MAKELONG(LOWORD(m_ColActive), Back);

		m_ColAccepted  = MAKELONG(LOWORD(m_ColAccepted), Back);

		m_ColWaitAcc   = MAKELONG(LOWORD(m_ColWaitAcc), Back);

		for( UINT n = 0; n < elements(m_ColPriority); n++ ) {

			m_ColPriority[n] = MAKELONG(LOWORD(m_ColPriority[n]), Back);

			pHost->UpdateUI(CPrintf(L"ColPriority%d", n+1));
		}

		pHost->UpdateUI(L"ColActive");

		pHost->UpdateUI(L"ColAccepted");

		pHost->UpdateUI(L"ColWaitAcc");
	}

	if( Tag.Left(4) == L"Show" ) {

		DoEnables(pHost);
	}

	if( Tag == L"IncTime" ) {

		DoEnables(pHost);
	}

	CPrimViewer::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimAlarmViewer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag.Left(3) == L"Txt" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"OnHelp" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Download Support

BOOL CPrimAlarmViewer::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	Init.AddByte(BYTE(m_IncTime));
	Init.AddByte(BYTE(m_IncMarks));
	Init.AddByte(BYTE(m_IncNum));

	Init.AddLong(m_ColInactive);

	Init.AddByte(BYTE(m_UsePriority));

	if( m_UsePriority ) {

		for( UINT n = 0; n < elements(m_ColPriority); n++ ) {

			Init.AddLong(LONG(m_ColPriority[n]));
		}
	}
	else
		Init.AddLong(m_ColActive);

	Init.AddLong(m_ColAccepted);
	Init.AddLong(m_ColWaitAcc);

	Init.AddItem(itemVirtual, m_pTxtEmpty);

	Init.AddItem(itemVirtual, m_pBtnPgUp);
	Init.AddItem(itemVirtual, m_pBtnPgDn);
	Init.AddItem(itemVirtual, m_pBtnPrev);
	Init.AddItem(itemVirtual, m_pBtnNext);
	Init.AddItem(itemVirtual, m_pBtnMute);
	Init.AddItem(itemVirtual, m_pBtnAccept);
	Init.AddItem(itemVirtual, m_pBtnHelp);

	Init.AddByte(BYTE(m_ShowPage));
	Init.AddByte(BYTE(m_ShowMute));
	Init.AddByte(BYTE(m_ShowAccept));
	Init.AddByte(BYTE(m_ShowHelp));

	Init.AddItem(itemVirtual, m_pOnHelp);

	Init.AddItem(itemSimple, m_pFormat);

	Init.AddByte(BYTE(m_Reverse));

	Init.AddLong(m_AutoScroll);

	return TRUE;
}

// Meta Data

void CPrimAlarmViewer::AddMetaData(void)
{
	CPrimViewer::AddMetaData();

	Meta_AddInteger(IncTime);
	Meta_AddInteger(IncMarks);
	Meta_AddInteger(IncNum);
	Meta_AddInteger(ColInactive);
	Meta_AddInteger(ColActive);
	Meta_AddInteger(ColAccepted);
	Meta_AddInteger(ColWaitAcc);
	Meta_AddVirtual(TxtEmpty);
	Meta_AddVirtual(BtnPgUp);
	Meta_AddVirtual(BtnPgDn);
	Meta_AddVirtual(BtnPrev);
	Meta_AddVirtual(BtnNext);
	Meta_AddVirtual(BtnMute);
	Meta_AddVirtual(BtnAccept);
	Meta_AddVirtual(BtnHelp);
	Meta_AddInteger(ShowPage);
	Meta_AddInteger(ShowMute);
	Meta_AddInteger(ShowAccept);
	Meta_AddInteger(ShowHelp);
	Meta_AddVirtual(OnHelp);
	Meta_AddObject(Format);
	Meta_AddInteger(Reverse);
	Meta_AddInteger(AutoScroll);

	Meta_AddInteger(UsePriority);

	for( UINT n = 0; n < elements(m_ColPriority); n++ ) {

		Meta_Add(CPrintf(L"ColPriority%d", n+1), m_ColPriority[n], metaInteger);
	}

	Meta_SetName((IDS_ALARM_VIEWER));
}

// Overridables

BOOL CPrimAlarmViewer::MakeList(void)
{
	m_List.Empty();

	if( m_ShowPage ) {

		m_List.Append(m_pBtnPgUp->GetText());

		m_List.Append(m_pBtnPgDn->GetText());
	}

	if( TRUE ) {

		m_List.Append(m_pBtnPrev->GetText());

		m_List.Append(m_pBtnNext->GetText());
	}

	if( m_ShowMute ) {

		m_List.Append(m_pBtnMute->GetText());
	}

	if( m_ShowAccept ) {

		m_List.Append(m_pBtnAccept->GetText());
	}

	if( m_ShowHelp ) {

		m_List.Append(m_pBtnHelp->GetText());
	}

	m_Work = "ALARM VIEWER";

	return TRUE;
}

// Implementation

void CPrimAlarmViewer::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("BtnPgUp", m_ShowPage);

	pHost->EnableUI("BtnPgDn", m_ShowPage);

	pHost->EnableUI("BtnMute", m_ShowMute);

	pHost->EnableUI("BtnAccept", m_ShowAccept);

	pHost->EnableUI("BtnHelp", m_ShowHelp);

	pHost->EnableUI("OnHelp", m_ShowHelp);

	pHost->EnableUI(m_pFormat, m_IncTime);

	pHost->EnableUI(L"ColActive", m_UsePriority == 0);

	for( UINT n = 0; n < 8; n++ ) {

		pHost->EnableUI(CPrintf(L"ColPriority%d", n+1), m_UsePriority == 1);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Event Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimEventViewer, CPrimViewer);

// Constructor

CPrimEventViewer::CPrimEventViewer(void)
{
	m_uType      = 0x1B;
	m_IncTime    = 1;
	m_IncMarks   = 1;
	m_IncNum     = 0;
	m_IncType    = 1;
	m_AutoScroll = 30;
	m_ColEmpty   = MAKELONG(GetRGB(0, 0, 0), GetRGB(31, 31, 31));
	m_ColEvent   = MAKELONG(GetRGB(0, 0, 0), GetRGB(31, 31, 31));
	m_ColAlarm   = MAKELONG(GetRGB(31, 0, 0), GetRGB(31, 31, 31));
	m_ColAccept  = MAKELONG(GetRGB(15, 15, 0), GetRGB(31, 31, 31));
	m_ColClear   = MAKELONG(GetRGB(0, 15, 0), GetRGB(31, 31, 31));
	m_pTxtEmpty  = NULL;
	m_pBtnPgUp   = NULL;
	m_pBtnPgDn   = NULL;
	m_pBtnPrev   = NULL;
	m_pBtnNext   = NULL;
	m_pBtnClear  = NULL;
	m_ShowPage   = 0;
	m_ShowClear  = 1;
	m_pUseClear  = NULL;
	m_pFormat    = New CDispFormatTimeDate;
}

// Initial Values

void CPrimEventViewer::SetInitValues(void)
{
	CPrimViewer::SetInitValues();

	SetInitial(L"BtnPgUp", m_pBtnPgUp, L"\"PgUp\"");
	SetInitial(L"BtnPgDn", m_pBtnPgDn, L"\"PgDn\"");
	SetInitial(L"BtnPrev", m_pBtnPrev, L"\"Prev\"");
	SetInitial(L"BtnNext", m_pBtnNext, L"\"Next\"");
	SetInitial(L"BtnClear", m_pBtnClear, L"\"Clear\"");
	SetInitial(L"TxtEmpty", m_pTxtEmpty, L"\"No Events\"");
}

// UI Update

void CPrimEventViewer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag.Left(4) == L"Show" ) {

		DoEnables(pHost);
	}

	if( Tag == L"IncTime" ) {

		DoEnables(pHost);
	}

	if( Tag == L"Back" ) {

		COLOR Back  = m_pBack->GetColor();

		m_ColEmpty  = MAKELONG(LOWORD(m_ColEmpty), Back);

		m_ColEvent  = MAKELONG(LOWORD(m_ColEvent), Back);

		m_ColAlarm  = MAKELONG(LOWORD(m_ColAlarm), Back);

		m_ColAccept = MAKELONG(LOWORD(m_ColAccept), Back);

		m_ColClear  = MAKELONG(LOWORD(m_ColClear), Back);

		pHost->UpdateUI(L"ColEmpty");

		pHost->UpdateUI(L"ColEvent");

		pHost->UpdateUI(L"ColAlarm");

		pHost->UpdateUI(L"ColAccept");

		pHost->UpdateUI(L"ColClear");
	}

	CPrimViewer::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimEventViewer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag.Left(3) == L"Txt" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Download Support

BOOL CPrimEventViewer::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	Init.AddByte(BYTE(m_IncTime));
	Init.AddByte(BYTE(m_IncMarks));
	Init.AddByte(BYTE(m_IncNum));
	Init.AddByte(BYTE(m_IncType));

	Init.AddItem(itemVirtual, m_pTxtEmpty);

	Init.AddItem(itemVirtual, m_pBtnPgUp);
	Init.AddItem(itemVirtual, m_pBtnPgDn);
	Init.AddItem(itemVirtual, m_pBtnPrev);
	Init.AddItem(itemVirtual, m_pBtnNext);
	Init.AddItem(itemVirtual, m_pBtnClear);

	Init.AddByte(BYTE(m_ShowPage));
	Init.AddByte(BYTE(m_ShowClear));

	Init.AddItem(itemVirtual, m_pUseClear);

	Init.AddItem(itemSimple, m_pFormat);

	Init.AddLong(m_ColEmpty);
	Init.AddLong(m_ColEvent);
	Init.AddLong(m_ColAlarm);
	Init.AddLong(m_ColAccept);
	Init.AddLong(m_ColClear);

	Init.AddLong(m_AutoScroll);

	return TRUE;
}

// Meta Data

void CPrimEventViewer::AddMetaData(void)
{
	CPrimViewer::AddMetaData();

	Meta_AddInteger(IncTime);
	Meta_AddInteger(IncMarks);
	Meta_AddInteger(IncNum);
	Meta_AddInteger(IncType);
	Meta_AddInteger(AutoScroll);
	Meta_AddInteger(ColEmpty);
	Meta_AddInteger(ColEvent);
	Meta_AddInteger(ColAlarm);
	Meta_AddInteger(ColAccept);
	Meta_AddInteger(ColClear);
	Meta_AddVirtual(TxtEmpty);
	Meta_AddVirtual(BtnPgUp);
	Meta_AddVirtual(BtnPgDn);
	Meta_AddVirtual(BtnPrev);
	Meta_AddVirtual(BtnNext);
	Meta_AddVirtual(BtnClear);
	Meta_AddInteger(ShowPage);
	Meta_AddInteger(ShowClear);
	Meta_AddVirtual(UseClear);
	Meta_AddObject(Format);

	Meta_SetName((IDS_EVENT_VIEWER));
}

// Overridables

BOOL CPrimEventViewer::MakeList(void)
{
	m_List.Empty();

	if( m_ShowPage ) {

		m_List.Append(m_pBtnPgUp->GetText());

		m_List.Append(m_pBtnPgDn->GetText());
	}

	if( TRUE ) {

		m_List.Append(m_pBtnPrev->GetText());

		m_List.Append(m_pBtnNext->GetText());
	}

	if( m_ShowClear ) {

		m_List.Append(m_pBtnClear->GetText());
	}

	m_Work = "EVENT VIEWER";

	return TRUE;
}

// Implementation

void CPrimEventViewer::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("BtnPgUp", m_ShowPage);

	pHost->EnableUI("BtnPgDn", m_ShowPage);

	pHost->EnableUI("BtnClear", m_ShowClear);

	pHost->EnableUI("UseClear", m_ShowClear);
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- File Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimFileViewer, CPrimViewer);

// Constructor

CPrimFileViewer::CPrimFileViewer(void)
{
	m_uType      = 0x1C;
	m_pRoot      = NULL;
	m_FontText   = fontSwiss0712;
	m_Number     = 1;
	m_Sort       = 1;
	m_IncCSV     = 1;
	m_IncTXT     = 0;
	m_IncLOG     = 0;
	m_pTxtEmpty  = NULL;
	m_pTxtCannot = NULL;
	m_pTxtEnd    = NULL;
	m_pBtnDown   = NULL;
	m_pBtnUp     = NULL;
	m_pBtnPrev   = NULL;
	m_pBtnNext   = NULL;
	m_pBtnScan   = NULL;
	m_pBtnLeft   = NULL;
	m_pBtnRight  = NULL;
	m_ShowScan   = 1;
	m_ShowLeft   = 1;
}

// Initial Values

void CPrimFileViewer::SetInitValues(void)
{
	CPrimViewer::SetInitValues();

	SetInitial(L"Root", m_pRoot, L"\"/LOGS/LOG1\"");
	SetInitial(L"BtnDown", m_pBtnDown, L"\"Down\"");
	SetInitial(L"BtnUp", m_pBtnUp, L"\"Up\"");
	SetInitial(L"BtnPrev", m_pBtnPrev, L"\"Prev\"");
	SetInitial(L"BtnNext", m_pBtnNext, L"\"Next\"");
	SetInitial(L"BtnScan", m_pBtnScan, L"\"Scan\"");
	SetInitial(L"BtnLeft", m_pBtnLeft, L"\"<\"");
	SetInitial(L"BtnRight", m_pBtnRight, L"\">\"");
	SetInitial(L"TxtEmpty", m_pTxtEmpty, L"\"No Files in Directory\"");
	SetInitial(L"TxtCannot", m_pTxtCannot, L"\"Cannot Load File\"");
	SetInitial(L"TxtEnd", m_pTxtEnd, L"\"End of File\"");
}

// UI Update

void CPrimFileViewer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag.Left(4) == L"Show" ) {

		DoEnables(pHost);
	}

	CPrimViewer::OnUIChange(pHost, pItem, Tag);
}

// Overridables

void CPrimFileViewer::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_FontText);

	CPrimViewer::GetRefs(Refs);
}

void CPrimFileViewer::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_FontText, uOld, uNew);

	CPrimViewer::EditRef(uOld, uNew);
}

// Type Access

BOOL CPrimFileViewer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag.Left(3) == L"Txt" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"Root" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Download Support

BOOL CPrimFileViewer::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	UINT uFrom = Init.GetCount();

	Init.AddWord(0);

	Init.AddItem(itemVirtual, m_pRoot);

	Init.AddWord(WORD(m_FontText));

	Init.AddByte(BYTE(m_Number));
	Init.AddByte(BYTE(m_Sort));

	Init.AddByte(BYTE(m_IncCSV));
	Init.AddByte(BYTE(m_IncTXT));
	Init.AddByte(BYTE(m_IncLOG));

	Init.AddItem(itemVirtual, m_pTxtEmpty);
	Init.AddItem(itemVirtual, m_pTxtCannot);
	Init.AddItem(itemVirtual, m_pTxtEnd);

	Init.AddItem(itemVirtual, m_pBtnDown);
	Init.AddItem(itemVirtual, m_pBtnUp);
	Init.AddItem(itemVirtual, m_pBtnPrev);
	Init.AddItem(itemVirtual, m_pBtnNext);
	Init.AddItem(itemVirtual, m_pBtnScan);
	Init.AddItem(itemVirtual, m_pBtnLeft);
	Init.AddItem(itemVirtual, m_pBtnRight);

	Init.AddByte(BYTE(m_ShowScan));
	Init.AddByte(BYTE(m_ShowLeft));

	UINT uSize = Init.GetCount() - uFrom;

	uSize -= sizeof(WORD);

	Init.SetWord(uFrom, WORD(uSize));

	return TRUE;
}

// Meta Data

void CPrimFileViewer::AddMetaData(void)
{
	CPrimViewer::AddMetaData();

	Meta_AddVirtual(Root);
	Meta_AddInteger(FontText);
	Meta_AddInteger(Number);
	Meta_AddInteger(Sort);
	Meta_AddInteger(IncCSV);
	Meta_AddInteger(IncTXT);
	Meta_AddInteger(IncLOG);
	Meta_AddVirtual(TxtEmpty);
	Meta_AddVirtual(TxtCannot);
	Meta_AddVirtual(TxtEnd);
	Meta_AddVirtual(BtnDown);
	Meta_AddVirtual(BtnUp);
	Meta_AddVirtual(BtnPrev);
	Meta_AddVirtual(BtnNext);
	Meta_AddVirtual(BtnScan);
	Meta_AddVirtual(BtnLeft);
	Meta_AddVirtual(BtnRight);
	Meta_AddInteger(ShowScan);
	Meta_AddInteger(ShowLeft);

	Meta_SetName((IDS_FILE_VIEWER));
}

// Overridables

BOOL CPrimFileViewer::MakeList(void)
{
	m_List.Empty();

	m_List.Append(m_pBtnUp->GetText());

	m_List.Append(m_pBtnDown->GetText());

	m_List.Append(m_pBtnPrev->GetText());

	m_List.Append(m_pBtnNext->GetText());

	if( m_ShowScan ) {

		m_List.Append(m_pBtnScan->GetText());
	}

	if( m_ShowLeft ) {

		m_List.Append(m_pBtnLeft->GetText());

		m_List.Append(m_pBtnRight->GetText());
	}

	m_Work = "FILE VIEWER";

	return TRUE;
}

BOOL CPrimFileViewer::IsSupported(void)
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
}

// Implementation

void CPrimFileViewer::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("BtnScan", m_ShowScan);

	pHost->EnableUI("BtnLeft", m_ShowLeft);

	pHost->EnableUI("BtnRight", m_ShowLeft);
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Trend Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimTrendViewer, CPrimViewer);

// Constructor

CPrimTrendViewer::CPrimTrendViewer(void)
{
	m_uType         = 0x1D;
	m_pLog          = NULL;
	m_Width         = 4;
	m_pPenMask      = NULL;
	m_pFillMask     = NULL;
	m_PenWeight     = 3;
	m_ShowData      = 0;
	m_ShowCursor    = 0;
	m_DataBox       = 0;
	m_FontTitle     = fontHei16Bold;
	m_FontData      = fontHei16;
	m_GridTime      = 0;
	m_GridMode      = 0;
	m_GridMajor     = 10;
	m_GridMinor     = 2;
	m_pGridMin      = NULL;
	m_pGridMax      = NULL;
	m_pBars         = NULL;
	m_Precise       = 1;
	m_pColTitle     = New CPrimColor;
	m_pColLabel     = New CPrimColor;
	m_pColData      = New CPrimColor;
	m_pColMajor     = New CPrimColor;
	m_pColMinor     = New CPrimColor;
	m_pColCursor    = New CPrimColor;
	m_pColPen[0x0]  = New CPrimColor;
	m_pColPen[0x1]  = New CPrimColor;
	m_pColPen[0x2]  = New CPrimColor;
	m_pColPen[0x3]  = New CPrimColor;
	m_pColPen[0x4]  = New CPrimColor;
	m_pColPen[0x5]  = New CPrimColor;
	m_pColPen[0x6]  = New CPrimColor;
	m_pColPen[0x7]  = New CPrimColor;
	m_pColPen[0x8]  = New CPrimColor;
	m_pColPen[0x9]  = New CPrimColor;
	m_pColPen[0xA]  = New CPrimColor;
	m_pColPen[0xB]  = New CPrimColor;
	m_pColPen[0xC]  = New CPrimColor;
	m_pColPen[0xD]  = New CPrimColor;
	m_pColPen[0xE]  = New CPrimColor;
	m_pColPen[0xF]  = New CPrimColor;
	m_pColFill[0x0] = New CPrimColor;
	m_pColFill[0x1] = New CPrimColor;
	m_pColFill[0x2] = New CPrimColor;
	m_pColFill[0x3] = New CPrimColor;
	m_pColFill[0x4] = New CPrimColor;
	m_pColFill[0x5] = New CPrimColor;
	m_pColFill[0x6] = New CPrimColor;
	m_pColFill[0x7] = New CPrimColor;
	m_pColFill[0x8] = New CPrimColor;
	m_pColFill[0x9] = New CPrimColor;
	m_pColFill[0xA] = New CPrimColor;
	m_pColFill[0xB] = New CPrimColor;
	m_pColFill[0xC] = New CPrimColor;
	m_pColFill[0xD] = New CPrimColor;
	m_pColFill[0xE] = New CPrimColor;
	m_pColFill[0xF] = New CPrimColor;
	m_pBtnPgLeft    = NULL;
	m_pBtnLeft      = NULL;
	m_pBtnLive      = NULL;
	m_pBtnRight     = NULL;
	m_pBtnPgRight   = NULL;
	m_pBtnIn        = NULL;
	m_pBtnOut       = NULL;
	m_pBtnLoad      = NULL;
	m_pFormat       = New CDispFormatTimeDate;
	m_fUseLoad      = 0;
	m_fUseFill      = FALSE;
}

// Initial Values

void CPrimTrendViewer::SetInitValues(void)
{
	m_pBack->SetInitial(GetRGB(8, 8, 8));

	CPrimViewer::SetInitValues();

	m_pColTitle->SetInitial(GetRGB(31, 31, 31));
	m_pColLabel->SetInitial(GetRGB(31, 31, 31));
	m_pColData->SetInitial(GetRGB(31, 31, 31));
	m_pColMajor->SetInitial(GetRGB(16, 16, 16));
	m_pColMinor->SetInitial(GetRGB(0, 0, 0));
	m_pColCursor->SetInitial(GetRGB(31, 0, 0));

	m_pColPen[0x0]->SetInitial(GetRGB(31, 0, 0));
	m_pColPen[0x1]->SetInitial(GetRGB(0, 31, 0));
	m_pColPen[0x2]->SetInitial(GetRGB(31, 31, 0));
	m_pColPen[0x3]->SetInitial(GetRGB(0, 0, 31));
	m_pColPen[0x4]->SetInitial(GetRGB(31, 0, 31));
	m_pColPen[0x5]->SetInitial(GetRGB(0, 31, 31));
	m_pColPen[0x6]->SetInitial(GetRGB(16, 0, 0));
	m_pColPen[0x7]->SetInitial(GetRGB(0, 16, 0));

	// REV3 -- Find some better defaults!

	m_pColPen[0x8]->SetInitial(GetRGB(31, 0, 0));
	m_pColPen[0x9]->SetInitial(GetRGB(0, 31, 0));
	m_pColPen[0xA]->SetInitial(GetRGB(31, 31, 0));
	m_pColPen[0xB]->SetInitial(GetRGB(0, 0, 31));
	m_pColPen[0xC]->SetInitial(GetRGB(31, 0, 31));
	m_pColPen[0xD]->SetInitial(GetRGB(0, 31, 31));
	m_pColPen[0xE]->SetInitial(GetRGB(16, 0, 0));
	m_pColPen[0xF]->SetInitial(GetRGB(0, 16, 0));

	m_pColFill[0x0]->SetInitial(GetRGB(31, 0, 0));
	m_pColFill[0x1]->SetInitial(GetRGB(0, 31, 0));
	m_pColFill[0x2]->SetInitial(GetRGB(31, 31, 0));
	m_pColFill[0x3]->SetInitial(GetRGB(0, 0, 31));
	m_pColFill[0x4]->SetInitial(GetRGB(31, 0, 31));
	m_pColFill[0x5]->SetInitial(GetRGB(0, 31, 31));
	m_pColFill[0x6]->SetInitial(GetRGB(16, 0, 0));
	m_pColFill[0x7]->SetInitial(GetRGB(0, 16, 0));
	m_pColFill[0x8]->SetInitial(GetRGB(31, 0, 0));
	m_pColFill[0x9]->SetInitial(GetRGB(0, 31, 0));
	m_pColFill[0xA]->SetInitial(GetRGB(31, 31, 0));
	m_pColFill[0xB]->SetInitial(GetRGB(0, 0, 31));
	m_pColFill[0xC]->SetInitial(GetRGB(31, 0, 31));
	m_pColFill[0xD]->SetInitial(GetRGB(0, 31, 31));
	m_pColFill[0xE]->SetInitial(GetRGB(16, 0, 0));
	m_pColFill[0xF]->SetInitial(GetRGB(0, 16, 0));

	SetInitial(L"BtnPgLeft", m_pBtnPgLeft, L"\"<<\"");
	SetInitial(L"BtnLeft", m_pBtnLeft, L"\"<\"");
	SetInitial(L"BtnLive", m_pBtnLive, L"\"Live\"");
	SetInitial(L"BtnRight", m_pBtnRight, L"\">\"");
	SetInitial(L"BtnPgRight", m_pBtnPgRight, L"\">>\"");
	SetInitial(L"BtnIn", m_pBtnIn, L"\"In\"");
	SetInitial(L"BtnOut", m_pBtnOut, L"\"Out\"");
	SetInitial(L"BtnLoad", m_pBtnLoad, L"\"Load\"");

	((CDispFormatTimeDate *) m_pFormat)->m_Mode = 3;
}

// Overridables

void CPrimTrendViewer::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_FontTitle);

	GetFontRef(Refs, m_FontData);

	CPrimViewer::GetRefs(Refs);
}

void CPrimTrendViewer::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_FontTitle, uOld, uNew);

	EditFontRef(m_FontData, uOld, uNew);

	CPrimViewer::EditRef(uOld, uNew);
}

// UI Update

void CPrimTrendViewer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag.Left(4) == L"Show" || Tag == L"GridMode" || Tag == L"UseFill" ) {

		DoEnables(pHost);
	}

	CPrimViewer::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimTrendViewer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"GridMin" || Tag == L"GridMax" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"Log" ) {

		Type.m_Type  = typeLog;

		Type.m_Flags = flagConstant;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Download Support

BOOL CPrimTrendViewer::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	AddLogIndex(Init, m_pLog);

	Init.AddByte(BYTE(m_Width));

	Init.AddItem(itemVirtual, m_pPenMask);
	Init.AddItem(itemVirtual, m_pFillMask);

	Init.AddByte(BYTE(m_PenWeight));
	Init.AddByte(BYTE(m_ShowData));
	Init.AddByte(BYTE(m_ShowCursor));
	Init.AddByte(BYTE(m_DataBox));
	Init.AddByte(BYTE(m_fUseFill));

	Init.AddWord(WORD(m_FontTitle));
	Init.AddWord(WORD(m_FontData));

	Init.AddByte(BYTE(m_GridTime));
	Init.AddByte(BYTE(m_GridMode));
	Init.AddByte(BYTE(m_GridMajor));
	Init.AddByte(BYTE(m_GridMinor));

	Init.AddItem(itemVirtual, m_pGridMin);
	Init.AddItem(itemVirtual, m_pGridMax);
	Init.AddItem(itemVirtual, m_pBars);

	Init.AddByte(BYTE(m_Precise));

	Init.AddItem(itemSimple, m_pColTitle);
	Init.AddItem(itemSimple, m_pColLabel);
	Init.AddItem(itemSimple, m_pColData);
	Init.AddItem(itemSimple, m_pColMajor);
	Init.AddItem(itemSimple, m_pColMinor);
	Init.AddItem(itemSimple, m_pColCursor);

	Init.AddItem(itemSimple, m_pColPen[0x0]);
	Init.AddItem(itemSimple, m_pColPen[0x1]);
	Init.AddItem(itemSimple, m_pColPen[0x2]);
	Init.AddItem(itemSimple, m_pColPen[0x3]);
	Init.AddItem(itemSimple, m_pColPen[0x4]);
	Init.AddItem(itemSimple, m_pColPen[0x5]);
	Init.AddItem(itemSimple, m_pColPen[0x6]);
	Init.AddItem(itemSimple, m_pColPen[0x7]);
	Init.AddItem(itemSimple, m_pColPen[0x8]);
	Init.AddItem(itemSimple, m_pColPen[0x9]);
	Init.AddItem(itemSimple, m_pColPen[0xA]);
	Init.AddItem(itemSimple, m_pColPen[0xB]);
	Init.AddItem(itemSimple, m_pColPen[0xC]);
	Init.AddItem(itemSimple, m_pColPen[0xD]);
	Init.AddItem(itemSimple, m_pColPen[0xE]);
	Init.AddItem(itemSimple, m_pColPen[0xF]);

	Init.AddItem(itemSimple, m_pColFill[0x0]);
	Init.AddItem(itemSimple, m_pColFill[0x1]);
	Init.AddItem(itemSimple, m_pColFill[0x2]);
	Init.AddItem(itemSimple, m_pColFill[0x3]);
	Init.AddItem(itemSimple, m_pColFill[0x4]);
	Init.AddItem(itemSimple, m_pColFill[0x5]);
	Init.AddItem(itemSimple, m_pColFill[0x6]);
	Init.AddItem(itemSimple, m_pColFill[0x7]);
	Init.AddItem(itemSimple, m_pColFill[0x8]);
	Init.AddItem(itemSimple, m_pColFill[0x9]);
	Init.AddItem(itemSimple, m_pColFill[0xA]);
	Init.AddItem(itemSimple, m_pColFill[0xB]);
	Init.AddItem(itemSimple, m_pColFill[0xC]);
	Init.AddItem(itemSimple, m_pColFill[0xD]);
	Init.AddItem(itemSimple, m_pColFill[0xE]);
	Init.AddItem(itemSimple, m_pColFill[0xF]);

	Init.AddItem(itemVirtual, m_pBtnPgLeft);
	Init.AddItem(itemVirtual, m_pBtnLeft);
	Init.AddItem(itemVirtual, m_pBtnLive);
	Init.AddItem(itemVirtual, m_pBtnRight);
	Init.AddItem(itemVirtual, m_pBtnPgRight);
	Init.AddItem(itemVirtual, m_pBtnIn);
	Init.AddItem(itemVirtual, m_pBtnOut);
	Init.AddItem(itemVirtual, m_pBtnLoad);

	Init.AddItem(itemSimple, m_pFormat);

	return TRUE;
}

// Meta Data

void CPrimTrendViewer::AddMetaData(void)
{
	CPrimViewer::AddMetaData();

	Meta_AddVirtual(Log);
	Meta_AddInteger(Width);
	Meta_AddVirtual(PenMask);
	Meta_AddVirtual(FillMask);
	Meta_AddInteger(PenWeight);
	Meta_AddInteger(ShowData);
	Meta_AddInteger(ShowCursor);
	Meta_AddInteger(DataBox);
	Meta_AddInteger(FontTitle);
	Meta_AddInteger(FontData);
	Meta_AddInteger(GridTime);
	Meta_AddInteger(GridMode);
	Meta_AddInteger(GridMajor);
	Meta_AddInteger(GridMinor);
	Meta_AddVirtual(GridMin);
	Meta_AddVirtual(GridMax);
	Meta_AddVirtual(Bars);
	Meta_AddInteger(Precise);
	Meta_AddObject(ColTitle);
	Meta_AddObject(ColLabel);
	Meta_AddObject(ColData);
	Meta_AddObject(ColMajor);
	Meta_AddObject(ColMinor);
	Meta_AddObject(ColCursor);
	Meta_AddBoolean(UseFill);

	Meta_Add(L"ColPen0", m_pColPen[0x0], metaObject);
	Meta_Add(L"ColPen1", m_pColPen[0x1], metaObject);
	Meta_Add(L"ColPen2", m_pColPen[0x2], metaObject);
	Meta_Add(L"ColPen3", m_pColPen[0x3], metaObject);
	Meta_Add(L"ColPen4", m_pColPen[0x4], metaObject);
	Meta_Add(L"ColPen5", m_pColPen[0x5], metaObject);
	Meta_Add(L"ColPen6", m_pColPen[0x6], metaObject);
	Meta_Add(L"ColPen7", m_pColPen[0x7], metaObject);
	Meta_Add(L"ColPen8", m_pColPen[0x8], metaObject);
	Meta_Add(L"ColPen9", m_pColPen[0x9], metaObject);
	Meta_Add(L"ColPenA", m_pColPen[0xA], metaObject);
	Meta_Add(L"ColPenB", m_pColPen[0xB], metaObject);
	Meta_Add(L"ColPenC", m_pColPen[0xC], metaObject);
	Meta_Add(L"ColPenD", m_pColPen[0xD], metaObject);
	Meta_Add(L"ColPenE", m_pColPen[0xE], metaObject);
	Meta_Add(L"ColPenF", m_pColPen[0xF], metaObject);

	Meta_Add(L"ColFill0", m_pColFill[0x0], metaObject);
	Meta_Add(L"ColFill1", m_pColFill[0x1], metaObject);
	Meta_Add(L"ColFill2", m_pColFill[0x2], metaObject);
	Meta_Add(L"ColFill3", m_pColFill[0x3], metaObject);
	Meta_Add(L"ColFill4", m_pColFill[0x4], metaObject);
	Meta_Add(L"ColFill5", m_pColFill[0x5], metaObject);
	Meta_Add(L"ColFill6", m_pColFill[0x6], metaObject);
	Meta_Add(L"ColFill7", m_pColFill[0x7], metaObject);
	Meta_Add(L"ColFill8", m_pColFill[0x8], metaObject);
	Meta_Add(L"ColFill9", m_pColFill[0x9], metaObject);
	Meta_Add(L"ColFillA", m_pColFill[0xA], metaObject);
	Meta_Add(L"ColFillB", m_pColFill[0xB], metaObject);
	Meta_Add(L"ColFillC", m_pColFill[0xC], metaObject);
	Meta_Add(L"ColFillD", m_pColFill[0xD], metaObject);
	Meta_Add(L"ColFillE", m_pColFill[0xE], metaObject);
	Meta_Add(L"ColFillF", m_pColFill[0xF], metaObject);

	Meta_AddVirtual(BtnPgLeft);
	Meta_AddVirtual(BtnLeft);
	Meta_AddVirtual(BtnLive);
	Meta_AddVirtual(BtnRight);
	Meta_AddVirtual(BtnPgRight);
	Meta_AddVirtual(BtnIn);
	Meta_AddVirtual(BtnOut);
	Meta_AddVirtual(BtnLoad);
	Meta_AddObject(Format);

	Meta_SetName((IDS_TREND_VIEWER));
}

// Overridables

BOOL CPrimTrendViewer::MakeList(void)
{
	m_List.Empty();

	m_List.Append(m_pBtnPgLeft->GetText());

	m_List.Append(m_pBtnLeft->GetText());

	m_List.Append(m_pBtnLive->GetText());

	m_List.Append(m_pBtnRight->GetText());

	m_List.Append(m_pBtnPgRight->GetText());

	m_List.Append(m_pBtnIn->GetText());

	m_List.Append(m_pBtnOut->GetText());

	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		if( m_fUseLoad ) {

			m_List.Append(m_pBtnLoad->GetText());
		}
	}

	m_Work = "TREND VIEWER";

	return TRUE;
}

// Implementation

void CPrimTrendViewer::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("GridMajor", m_GridMode == 1 || m_GridMode == 2);

	pHost->EnableUI("GridMinor", m_GridMode == 2);

	pHost->EnableUI("GridMin", m_GridMode == 3);

	pHost->EnableUI("GridMax", m_GridMode == 3);

	pHost->EnableUI("Precise", m_GridMode == 3);

	pHost->EnableUI("ColMajor", m_GridMode >= 1 || m_GridTime >= 1);

	pHost->EnableUI("ColMinor", m_GridMode == 2 || m_GridTime >= 1);

	pHost->EnableUI("ColData", m_ShowData);

	pHost->EnableUI("ColCursor", m_ShowCursor);

	pHost->EnableUI("FontData", m_ShowData || m_ShowCursor);

	pHost->EnableUI("DataBox", m_ShowData || m_ShowCursor);

	pHost->EnableUI("FillMask", m_fUseFill);

	pHost->EnableUI("ColFill0", m_fUseFill);
	pHost->EnableUI("ColFill1", m_fUseFill);
	pHost->EnableUI("ColFill2", m_fUseFill);
	pHost->EnableUI("ColFill3", m_fUseFill);
	pHost->EnableUI("ColFill4", m_fUseFill);
	pHost->EnableUI("ColFill5", m_fUseFill);
	pHost->EnableUI("ColFill6", m_fUseFill);
	pHost->EnableUI("ColFill7", m_fUseFill);
	pHost->EnableUI("ColFill8", m_fUseFill);
	pHost->EnableUI("ColFill9", m_fUseFill);
	pHost->EnableUI("ColFillA", m_fUseFill);
	pHost->EnableUI("ColFillB", m_fUseFill);
	pHost->EnableUI("ColFillC", m_fUseFill);
	pHost->EnableUI("ColFillD", m_fUseFill);
	pHost->EnableUI("ColFillE", m_fUseFill);
	pHost->EnableUI("ColFillF", m_fUseFill);
}

// Implementation

void CPrimTrendViewer::AddLogIndex(CInitData &Init, CCodedItem *pItem)
{
	if( pItem ) {

		if( !pItem->IsBroken() ) {

			UINT hItem = pItem->Execute(typeInteger);

			if( hItem ) {

				if( HIBYTE(hItem) ) {

					Init.AddWord(WORD(1 + (hItem & 0xFF)));
				}
				else
					Init.AddWord(WORD(hItem));

				return;
			}
		}
	}

	Init.AddWord(WORD(0));
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- User Manager
//

// Dynamic Class

AfxImplementDynamicClass(CPrimUserManager, CPrimViewer);

// Constructor

CPrimUserManager::CPrimUserManager(void)
{
	m_uType      = 0x1E;
	m_pBtnPrev   = NULL;
	m_pBtnNext   = NULL;
	m_pBtnPass   = NULL;
	m_AutoScroll = 30;
}

// Initial Values

void CPrimUserManager::SetInitValues(void)
{
	CPrimViewer::SetInitValues();

	SetInitial(L"BtnPrev", m_pBtnPrev, L"\"Prev\"");
	SetInitial(L"BtnNext", m_pBtnNext, L"\"Next\"");
	SetInitial(L"BtnPass", m_pBtnPass, L"\"Set Pass\"");
}

// UI Update

void CPrimUserManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	CPrimViewer::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimUserManager::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Download Support

BOOL CPrimUserManager::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pBtnPrev);
	Init.AddItem(itemVirtual, m_pBtnNext);
	Init.AddItem(itemVirtual, m_pBtnPass);

//	Init.AddLong(m_AutoScroll);

	Init.AddLong(0);

	return TRUE;
}

// Meta Data

void CPrimUserManager::AddMetaData(void)
{
	CPrimViewer::AddMetaData();

	Meta_AddVirtual(BtnPrev);
	Meta_AddVirtual(BtnNext);
	Meta_AddVirtual(BtnPass);
	Meta_AddInteger(AutoScroll);

	Meta_SetName((IDS_USER_MANAGER));
}

// Overridables

BOOL CPrimUserManager::MakeList(void)
{
	m_List.Empty();

	m_List.Append(m_pBtnPrev->GetText());

	m_List.Append(m_pBtnNext->GetText());

	m_List.Append(m_pBtnPass->GetText());

	m_Work = "USER MANAGER";

	return TRUE;
}

// Implementation

void CPrimUserManager::DoEnables(IUIHost *pHost)
{
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Calibration
//

// Dynamic Class

AfxImplementDynamicClass(CPrimTouchCalib, CPrim);

// Constructor

CPrimTouchCalib::CPrimTouchCalib(void)
{
	m_uType      = 0x1F;

	m_pOnFailure = NULL;

	m_pOnSuccess = NULL;
}

// Overridables

void CPrimTouchCalib::Draw(IGDI *pGDI, UINT uMode)
{
	pGDI->ResetBrush();

	pGDI->SetBrushFore(GetRGB(16, 16, 16));

	pGDI->FillRect(PassRect(m_DrawRect));

	pGDI->ResetFont();

	pGDI->SelectFont(fontHei16Bold);

	pGDI->SetTextTrans(modeTransparent);

	int xp = m_DrawRect.x1 + 4;

	int yp = m_DrawRect.y1 + 4;

	pGDI->TextOut(xp, yp, L"Touch Calibration");
}

void CPrimTouchCalib::SetInitState(void)
{
	CPrim::SetInitState();

	CUISystem * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	SetInitSize(pSystem->m_DispSize);
}

// Type Access

BOOL CPrimTouchCalib::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(2) == L"On" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
	}

	return FALSE;
}

// Download Support

BOOL CPrimTouchCalib::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pOnSuccess);

	Init.AddItem(itemVirtual, m_pOnFailure);

	return TRUE;
}

// Meta Data

void CPrimTouchCalib::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddVirtual(OnFailure);
	Meta_AddVirtual(OnSuccess);

	Meta_SetName((IDS_TOUCH_CALIBRATION));
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Tester
//

// Dynamic Class

AfxImplementDynamicClass(CPrimTouchTester, CPrim);

// Constructor

CPrimTouchTester::CPrimTouchTester(void)
{
	m_uType = 0x20;

	m_pBack = New CPrimColor;
}

// Overridables

void CPrimTouchTester::Draw(IGDI *pGDI, UINT uMode)
{
	pGDI->ResetBrush();

	pGDI->SetBrushFore(m_pBack->GetColor());

	pGDI->FillRect(PassRect(m_DrawRect));

	pGDI->ResetFont();

	pGDI->SelectFont(fontHei16Bold);

	pGDI->SetTextTrans(modeTransparent);

	int xp = m_DrawRect.x1 + 4;

	int yp = m_DrawRect.y1 + 4;

	pGDI->TextOut(xp, yp, L"Touch Tester");
}

void CPrimTouchTester::SetInitState(void)
{
	CPrim::SetInitState();

	m_pBack->SetInitial(GetRGB(16, 16, 16));

	CUISystem *pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	SetInitSize(pSystem->m_DispSize);
}

// Download Support

BOOL CPrimTouchTester::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pBack);

	return TRUE;
}

// Meta Data

void CPrimTouchTester::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddObject(Back);

	Meta_SetName((IDS_TOUCH_TESTER));
}

// End of File
