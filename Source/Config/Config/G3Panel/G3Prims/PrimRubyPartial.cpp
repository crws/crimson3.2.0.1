
#include "intern.hpp"

#include "PrimRubyPartial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Partial Primitive
//

// Dyanmic Class

AfxImplementDynamicClass(CPrimRubyPartial, CPrimRubyOriented);

// Constructor

CPrimRubyPartial::CPrimRubyPartial(void)
{
	m_Radial = 0;

	m_fDrop  = FALSE;
	}

// Meta Data

void CPrimRubyPartial::AddMetaData(void)
{
	CPrimRubyOriented::AddMetaData();

	Meta_AddInteger(Radial);

	Meta_SetName((IDS_RUBY_PARTIAL));
	}

// Fast Fill Control

BOOL CPrimRubyPartial::UseFastFill(void)
{
	return m_Radial ? CPrimRubyOriented::UseFastFill() : FALSE;
	}

// Path Management

void CPrimRubyPartial::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	if( m_pEdge->IsNull() ) {

		MakeFigure(Rect);
		}
	else {
		if( m_Radial ) {

			m_pEdge->AdjustRect(Rect);

			MakeFigure(Rect);

			m_pEdge->StrokeEdge(m_pathEdge, m_pathFill);

			m_pEdge->StrokeTrim(m_pathTrim, m_pathEdge);
			}
		else {
			MakeFigure(Rect);

			if( m_fDrop ) {

				// We have to drop the last point from the list
				// or we don't get the right result. This is only
				// needed for quadrants. We used to add a dummy
				// last point for semis, but the co-linearity
				// test remove it for us!

				CRubyPath path;

				if( m_Reflect & 1 ) {

					path.Append(m_pathFill, 1, m_pathFill.GetCount() - 1);
					}
				else
					path.Append(m_pathFill, 0, m_pathFill.GetCount() - 1);

				m_pEdge->StrokeOpen(m_pathEdge, path);
				}
			else
				m_pEdge->StrokeOpen(m_pathEdge, m_pathFill);

			// TODO -- Should the trim be on the arc only? At
			// the moment, the ends of the edge are trimmed,
			// which may not be what is required!!!!

			m_pEdge->StrokeTrim(m_pathTrim, m_pathEdge);
			}
		}

	m_pathFill.GetBoundingRect(m_bound);

	m_pathEdge.AddBoundingRect(m_bound);

	m_pathTrim.AddBoundingRect(m_bound);
	}

// End of File
