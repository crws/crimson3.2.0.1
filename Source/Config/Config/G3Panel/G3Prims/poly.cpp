
#include "intern.hpp"

#include "poly.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPoly, CPrimGeom);

// Constructor

CPrimPoly::CPrimPoly(void)
{
	m_uType    = 0x08;

	m_uCount   = 0;

	m_dwRound  = 0;

	m_pSpinPos = NULL;

	m_pSpinMin = NULL;

	m_pSpinMax = NULL;

	m_pList    = NULL;

	m_Orient   = 0;
	}

// Destructor

CPrimPoly::~CPrimPoly(void)
{
	FreePoints();
	}

// UI Creation

BOOL CPrimPoly::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       1
				       ));

	if( !m_dwRound ) {

		pList->Append( New CUIStdPage( CString(IDS_ANIMATION),
					       AfxPointerClass(this),
					       2
					       ));
		}

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       3
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// UI Update

void CPrimPoly::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Spin") ) {

		DoEnables(pHost);
		}

	CPrimGeom::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimPoly::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.StartsWith(L"Spin") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimGeom::GetTypeData(Tag, Type);
	}

// Overridables

BOOL CPrimPoly::HitTest(P2 Pos)
{
	return CPrimGeom::HitTest(Pos);
	}

void CPrimPoly::FindTextRect(void)
{
	// Maybe adjust goes into findlayout??? !!!!

	CPrimGeom::FindTextRect();

	AdjustTextRect();
	}

void CPrimPoly::Draw(IGDI *pGDI, UINT uMode)
{
	if( m_uCount ) {

		if( m_pSpinPos ) {

			int xp = (m_DrawRect.x1 + m_DrawRect.x2) / 2;

			int yp = (m_DrawRect.y1 + m_DrawRect.y2) / 2;

			int cx = (m_DrawRect.x2 - m_DrawRect.x1) / 2;

			int cy = (m_DrawRect.y2 - m_DrawRect.y1) / 2;

			int sm = 0;

			for( UINT n = 0; n < m_uCount; n++ ) {

				int dx = abs(m_pList[n].x - xp);

				int dy = abs(m_pList[n].y - yp);

				int ds = int(sqrt(double(dx*dx+dy*dy))+1);

				MakeMax(sm, ds);
				}

			M3 m;

			pGDI->MakeIdentity(m);

			m.e[0][0] *= min(cx,cy)-2;
			m.e[1][1] *= min(cx,cy)-2;

			m.e[0][0] /= sm;
			m.e[1][1] /= sm;

			C3REAL pos = m_pSpinPos ? I2R(m_pSpinPos->ExecVal()) : 0;

			C3REAL min = m_pSpinMin ? I2R(m_pSpinMin->ExecVal()) : 0;

			C3REAL max = m_pSpinMax ? I2R(m_pSpinMax->ExecVal()) : 360;

			if( min - max ) {

				pGDI->AddMovement (-xp, -yp);

				pGDI->AddTransform(m);

				pGDI->AddRotation (int(360 * (pos - min) / (max - min)));

				pGDI->AddMovement (+xp, +yp);
				}
			}

		m_pFill->FillPolygon(pGDI, m_pList, m_uCount, m_dwRound);

		m_pEdge->DrawPolygon(pGDI, m_pList, m_uCount, m_dwRound, uMode);

		pGDI->SetIdentity();
		}

	CPrimWithText::Draw(pGDI, uMode);
	}

void CPrimPoly::UpdateLayout(void)
{
	CPrimGeom::UpdateLayout();

	FreePoints();

	MakePoints();
	}

// Download Support

BOOL CPrimPoly::MakeInitData(CInitData &Init)
{
	CPrimGeom::MakeInitData(Init);

	Init.AddByte(BYTE(m_uCount));

	for( UINT n = 0; n < m_uCount; n++ ) {

		Init.AddWord(WORD(m_pList[n].x));

		Init.AddWord(WORD(m_pList[n].y));
		}

	Init.AddLong(m_dwRound);

	Init.AddItem(itemVirtual, m_pSpinPos);

	Init.AddItem(itemVirtual, m_pSpinMin);
	
	Init.AddItem(itemVirtual, m_pSpinMax);

	return TRUE;
	}

// Property Save Filter

BOOL CPrimPoly::SaveProp(CString const &Tag) const
{
	if( Tag.StartsWith(L"Spin") ) {

		if( m_pSpinPos ) {

			return TRUE;
			}

		return FALSE;
		}

	return CPrimGeom::SaveProp(Tag);
	}

// Implementation

void CPrimPoly::MakePoints(void)
{
	CRect Rect  = GetRect()   - m_pEdge->GetAdjust();

	Rect.right  = Rect.right  - 1;

	Rect.bottom = Rect.bottom - 1;

	CSize  Max  = Rect.GetSize();

	if( m_Orient / 2 == 1 ) {

		Swap(Max.cx, Max.cy);

		MakePoints(Max);

		for( UINT n = 0; n < m_uCount; n++ ) {

			Swap(m_pList[n].x, m_pList[n].y);
			}

		if( m_Orient % 2 == 1 ) {

			for( UINT n = 0; n < m_uCount; n++ ) {

			//	m_pList[n].x = 0      + m_pList[n].x;
				m_pList[n].y = Max.cx - m_pList[n].y;
				}
			}
		else
			m_dwRound ^= 0xFFFF0000;
		}
	else {
		MakePoints(Max);

		if( m_Orient % 2 == 1 ) {

			for( UINT n = 0; n < m_uCount; n++ ) {

				m_pList[n].x = Max.cx - m_pList[n].x;
			//	m_pList[n].y = 0      + m_pList[n].y;
				}

			m_dwRound ^= 0xFFFF0000;
			}
		}

	for( UINT n = 0; n < m_uCount; n++ ) {

		m_pList[n].x = Rect.left  + m_pList[n].x;
		m_pList[n].y = Rect.top   + m_pList[n].y;
		}
	}

void CPrimPoly::FreePoints(void)
{
	if( m_pList ) {

		delete [] m_pList;

		m_uCount = 0;

		m_pList  = NULL;
		}
	}

void CPrimPoly::AdjustTextRect(void)
{
	if( m_Orient ) {

		int x1 = m_TextRect.x1 - m_DrawRect.x1;
		int x2 = m_DrawRect.x2 - m_TextRect.x2;
		int y1 = m_TextRect.y1 - m_DrawRect.y1;
		int y2 = m_DrawRect.y2 - m_TextRect.y2;

		switch( m_Orient ) {

			case 1:
				Swap(x1, x2);
				break;

			case 2:
				Swap(x1, y1);
				Swap(x2, y2);
				break;

			case 3:
				Swap(x2, y1);
				Swap(x1, y2);
				break;
			}

		m_TextRect.x1 = m_DrawRect.x1 + x1;
		m_TextRect.x2 = m_DrawRect.x2 - x2;
		m_TextRect.y1 = m_DrawRect.y1 + y1;
		m_TextRect.y2 = m_DrawRect.y2 - y2;
		}
	}

// Generation

void CPrimPoly::MakePoints(CSize Max)
{
	// NOTE -- Override this to create the point set. If you are going
	// to use curved edges, generate the points clockwise for edges that
	// face outwards, or anti-clockwise for edges that face inwards. To
	// invert the sense of this, set ROUND_FLIP in m_dwRound.

	AfxAssert(FALSE);
	}

// Meta Data

void CPrimPoly::AddMetaData(void)
{
	CPrimGeom::AddMetaData();

	Meta_AddInteger(Orient);
	Meta_AddVirtual(SpinPos);
	Meta_AddVirtual(SpinMin);
	Meta_AddVirtual(SpinMax);

	Meta_SetName((IDS_POLY));
	}

// Implementation

void CPrimPoly::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"SpinMin", !!m_pSpinPos);

	pHost->EnableUI(this, L"SpinMax", !!m_pSpinPos);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Poylgon or Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolygon, CPrimPoly);

// Constructor

CPrimPolygon::CPrimPolygon(void)
{
	m_uType  = 0x16;

	m_nSides = 0;

	m_Rotate = -900;

	m_AltRad = 100;
	}

// Generation

void CPrimPolygon::MakePoints(CSize Max)
{
	if( m_nSides ) {

		m_uCount    = m_nSides;

		m_pList     = New P2 [ m_uCount ];

		double  pi  = acos(double(-1));

		double *px  = New double [ m_uCount ];
		
		double *py  = New double [ m_uCount ];

		double xMin = 0;

		double xMax = 0;
		
		double yMin = 0;

		double yMax = 0;

		int n;

		for( n = 0; n < m_nSides; n++ ) {

			double a = pi * 2 / m_nSides * n + (m_Rotate * pi / 1800);

			double r = (n % 2) ? 1.00 : (m_AltRad / 100.0);

			px[n] = r * cos(a);
			py[n] = r * sin(a);

			MakeMin(xMin, px[n]);
			MakeMax(xMax, px[n]);
			MakeMin(yMin, py[n]);
			MakeMax(yMax, py[n]);
			}

		for( n = 0; n < m_nSides; n++ ) {

			double x = (px[n] - xMin) / (xMax - xMin);

			double y = (py[n] - yMin) / (yMax - yMin);

			m_pList[n].x = int(Max.cx * x + 0.5 * sgn(x));

			m_pList[n].y = int(Max.cy * y + 0.5 * sgn(x));
			}

		delete [] px;

		delete [] py;

		return;
		}

	AfxAssert(FALSE);
	}

// Meta Data

void CPrimPolygon::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_AddInteger(Rotate);

	Meta_SetName((IDS_POLYGON));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Triangle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolygon3, CPrimPolygon);

// Constructor

CPrimPolygon3::CPrimPolygon3(void)
{
	m_nSides = 3;

	m_Rotate = -900;
	}
	
// Meta Data

void CPrimPolygon3::AddMetaData(void)
{
	CPrimPolygon::AddMetaData();

	Meta_SetName((IDS_TRIANGLE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Penatgon
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolygon5, CPrimPolygon);

// Constructor

CPrimPolygon5::CPrimPolygon5(void)
{
	m_nSides = 5;

	m_Rotate = -900;
	}
	
// Meta Data

void CPrimPolygon5::AddMetaData(void)
{
	CPrimPolygon::AddMetaData();

	Meta_SetName((IDS_PENTAGON));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Hexagon
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolygon6, CPrimPolygon);

// Constructor

CPrimPolygon6::CPrimPolygon6(void)
{
	m_nSides = 6;

	m_Rotate = -900;
	}
	
// Meta Data

void CPrimPolygon6::AddMetaData(void)
{
	CPrimPolygon::AddMetaData();

	Meta_SetName((IDS_HEXAGON));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Octagon
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolygon8, CPrimPolygon);

// Constructor

CPrimPolygon8::CPrimPolygon8(void)
{
	m_nSides = 8;

	m_Rotate = 225;
	}
	
// Meta Data

void CPrimPolygon8::AddMetaData(void)
{
	CPrimPolygon::AddMetaData();

	Meta_SetName((IDS_OCTAGON));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Polygonal Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyStar, CPrimPolygon);

// Constructor

CPrimPolyStar::CPrimPolyStar(void)
{
	}
	
// Meta Data

void CPrimPolyStar::AddMetaData(void)
{
	CPrimPolygon::AddMetaData();

	Meta_AddInteger(AltRad);

	Meta_SetName((IDS_POLYGONAL_STAR));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Three Pointed Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyStar3, CPrimPolyStar);

// Constructor

CPrimPolyStar3::CPrimPolyStar3(void)
{
	m_nSides = 6;

	m_AltRad = 25;

	m_Rotate = +900;
	}
	
// Meta Data

void CPrimPolyStar3::AddMetaData(void)
{
	CPrimPolyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_1));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Four Pointed Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyStar4, CPrimPolyStar);

// Constructor

CPrimPolyStar4::CPrimPolyStar4(void)
{
	m_nSides = 8;

	m_AltRad = 30;

	m_Rotate = +900;
	}
	
// Meta Data

void CPrimPolyStar4::AddMetaData(void)
{
	CPrimPolyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_2));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Five Pointed Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyStar5, CPrimPolyStar);

// Constructor

CPrimPolyStar5::CPrimPolyStar5(void)
{
	m_nSides = 10;

	m_AltRad = 35;

	m_Rotate = +900;
	}
	
// Meta Data

void CPrimPolyStar5::AddMetaData(void)
{
	CPrimPolyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_3));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Six Pointed Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyStar6, CPrimPolyStar);

// Constructor

CPrimPolyStar6::CPrimPolyStar6(void)
{
	m_nSides = 12;

	m_AltRad = 40;

	m_Rotate = 0;
	}
	
// Meta Data

void CPrimPolyStar6::AddMetaData(void)
{
	CPrimPolyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_4));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Eight Pointed Star
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyStar8, CPrimPolyStar);

// Constructor

CPrimPolyStar8::CPrimPolyStar8(void)
{
	m_nSides = 16;

	m_AltRad = 40;

	m_Rotate = 225;
	}
	
// Meta Data

void CPrimPolyStar8::AddMetaData(void)
{
	CPrimPolyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_5));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Arrow
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyArrow, CPrimPoly);

// Constructor

CPrimPolyArrow::CPrimPolyArrow(void)
{
	}
	
// Generation

void CPrimPolyArrow::MakePoints(CSize Max)
{
	m_uCount = 8;

	m_pList  = New P2 [ m_uCount ];

	int y2 = (Max.cy + 0) / 2;
	int y3 = (Max.cy + 1) / 2;

	int y1  = y2 - Max.cy / 6;
	int y4  = y3 + Max.cy / 6;
	
	int x1  = Max.cx - y2;

	m_pList[0].x = 0;
	m_pList[0].y = y1;

	m_pList[1].x = x1;
	m_pList[1].y = y1;

	m_pList[2].x = x1;
	m_pList[2].y = 0;

	m_pList[3].x = Max.cx;
	m_pList[3].y = y2;

	m_pList[4].x = Max.cx;
	m_pList[4].y = y3;

	m_pList[5].x = x1;
	m_pList[5].y = Max.cy;

	m_pList[6].x = x1;
	m_pList[6].y = y4;

	m_pList[7].x = 0;
	m_pList[7].y = y4;
	}

// Meta Data

void CPrimPolyArrow::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_SetName((IDS_ARROW));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Trimmed Rectangle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyTrimRect, CPrimPoly);

// Constructor

CPrimPolyTrimRect::CPrimPolyTrimRect(void)
{
	m_Corner = CPoint(16, 16);
	}

// Overridables

void CPrimPolyTrimRect::FindTextRect(void)
{
	int xStep = 3 * m_Corner.cx / 4;

	int yStep = 3 * m_Corner.cy / 4;

	int nLeft = m_TextRect.y2 - m_TextRect.y1 - 2 * yStep;

	int nTrip = GetTripSize();

	if( nLeft <= nTrip ) {

		xStep = m_Corner.cx;

		yStep = 2;
		}

	m_TextRect.x1 = m_DrawRect.x1 + xStep;
	
	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - xStep;
	
	m_TextRect.y2 = m_DrawRect.y2 - yStep;

	AdjustTextRect();
	}

BOOL CPrimPolyTrimRect::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		Hand.m_Pos  = m_Corner;

		Hand.m_pTag = L"Corner";

		Hand.m_uRef = 0;

		Hand.m_Clip = CRect( m_pEdge->GetAdjust(),
				     m_pEdge->GetAdjust(),
				     cx / 2 - 2,
				     cy / 2 - 2
				     );

		return TRUE;
		}

	return FALSE;
	}

void CPrimPolyTrimRect::SetHand(BOOL fInit)
{
	CPrimPoly::FreePoints();

	if( fInit ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		m_Corner.cx = cx / 4;

		m_Corner.cy = cy / 4;
		}

	CPrimPoly::MakePoints();

	CPrimWithText::SetHand(fInit);
	}

// Generation

void CPrimPolyTrimRect::MakePoints(CSize Max)
{
	m_uCount  = 8;

	m_pList   = New P2 [ m_uCount ];

	int xCorn = min(m_Corner.cx, Max.cx / 2 - 2);

	int yCorn = min(m_Corner.cy, Max.cy / 2 - 2);

	m_pList[0].x = 0;
	m_pList[0].y = yCorn;

	m_pList[1].x = xCorn,
	m_pList[1].y = 0;

	m_pList[2].x = Max.cx - xCorn;
	m_pList[2].y = 0;

	m_pList[3].x = Max.cx;
	m_pList[3].y = yCorn;

	m_pList[4].x = Max.cx;
	m_pList[4].y = Max.cy - yCorn;

	m_pList[5].x = Max.cx - xCorn;
	m_pList[5].y = Max.cy;

	m_pList[6].x = xCorn;
	m_pList[6].y = Max.cy;

	m_pList[7].x = 0;
	m_pList[7].y = Max.cy - yCorn;
	}

// Meta Data

void CPrimPolyTrimRect::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_AddPoint(Corner);

	Meta_SetName((IDS_TRIMMED_RECTANGLE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Rounded Rectangle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyRoundRect, CPrimPolyTrimRect);

// Constructor

CPrimPolyRoundRect::CPrimPolyRoundRect(void)
{
	}

// Overridables
	
void CPrimPolyRoundRect::FindTextRect(void)
{
	int xStep = 1 * m_Corner.cx / 2;

	int yStep = 1 * m_Corner.cy / 2;

	int nLeft = m_TextRect.y2 - m_TextRect.y1 - 2 * yStep;

	int nTrip = GetTripSize();

	if( nLeft <= nTrip ) {

		xStep = m_Corner.cx;

		yStep = 2;
		}

	m_TextRect.x1 = m_DrawRect.x1 + xStep;
	
	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - xStep;
	
	m_TextRect.y2 = m_DrawRect.y2 - yStep;

	AdjustTextRect();
	}

// Generation

void CPrimPolyRoundRect::MakePoints(CSize Max)
{
	CPrimPolyTrimRect::MakePoints(Max);

	m_dwRound = 0x000055;
	}

// Meta Data

void CPrimPolyRoundRect::AddMetaData(void)
{
	CPrimPolyTrimRect::AddMetaData();

	Meta_SetName((IDS_ROUNDED_RECTANGLE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Plaque
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyPlaque, CPrimPolyTrimRect);

// Constructor

CPrimPolyPlaque::CPrimPolyPlaque(void)
{
	}
	
// Overridables
	
void CPrimPolyPlaque::FindTextRect(void)
{
	int xStep = 7 * m_Corner.cx / 8;

	int yStep = 7 * m_Corner.cy / 8;

	int nLeft = m_TextRect.y2 - m_TextRect.y1 - 2 * yStep;

	int nTrip = GetTripSize();

	if( nLeft <= nTrip ) {

		xStep = m_Corner.cx;

		yStep = 2;
		}

	m_TextRect.x1 = m_DrawRect.x1 + xStep;
	
	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - xStep;
	
	m_TextRect.y2 = m_DrawRect.y2 - yStep;

	AdjustTextRect();
	}

// Generation

void CPrimPolyPlaque::MakePoints(CSize Max)
{
	CPrimPolyTrimRect::MakePoints(Max);

	m_dwRound = 0x550055;
	}

// Meta Data

void CPrimPolyPlaque::AddMetaData(void)
{
	CPrimPolyTrimRect::AddMetaData();

	Meta_SetName((IDS_PLAQUE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Semi Trimmed Rectangle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolySemiTrimRect, CPrimPoly);

// Constructor

CPrimPolySemiTrimRect::CPrimPolySemiTrimRect(void)
{
	m_Corner = CPoint(16, 16);
	}

// Overridables

void CPrimPolySemiTrimRect::FindTextRect(void)
{
	int nEdge = m_pEdge->GetWidth() + 1;

	int yStep = 3 * m_Corner.cy / 4;

	m_TextRect.x1 = m_DrawRect.x1 + nEdge;
	
	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - nEdge;
	
	m_TextRect.y2 = m_DrawRect.y2 - yStep;

	AdjustTextRect();
	}

BOOL CPrimPolySemiTrimRect::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		Hand.m_Pos  = m_Corner;

		Hand.m_pTag = L"Corner";

		Hand.m_uRef = 0;

		Hand.m_Clip = CRect( m_pEdge->GetAdjust(),
				     m_pEdge->GetAdjust(),
				     cx / 2 - 2,
				     cy / 2 - 2
				     );

		return TRUE;
		}

	return FALSE;
	}

void CPrimPolySemiTrimRect::SetHand(BOOL fInit)
{
	CPrimPoly::FreePoints();

	if( fInit ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		m_Corner.cx = cx / 4;

		m_Corner.cy = cy / 4;
		}

	CPrimPoly::MakePoints();

	CPrimWithText::SetHand(fInit);
	}

// Generation

void CPrimPolySemiTrimRect::MakePoints(CSize Max)
{
	m_uCount  = 6;

	m_pList   = New P2 [ m_uCount ];

	int xCorn = min(m_Corner.cx, Max.cx / 2 - 2);

	int yCorn = min(m_Corner.cy, Max.cy / 2 - 2);

	m_pList[0].x = 0;
	m_pList[0].y = yCorn;

	m_pList[1].x = xCorn,
	m_pList[1].y = 0;

	m_pList[2].x = Max.cx;
	m_pList[2].y = 0;

	m_pList[3].x = Max.cx;
	m_pList[3].y = Max.cy;

	m_pList[4].x = xCorn;
	m_pList[4].y = Max.cy;

	m_pList[5].x = 0;
	m_pList[5].y = Max.cy - yCorn;
	}

// Meta Data

void CPrimPolySemiTrimRect::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_AddPoint(Corner);

	Meta_SetName((IDS_SEMI_TRIMMED));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Semi Rounded Rectangle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolySemiRoundRect, CPrimPolySemiTrimRect);

// Constructor

CPrimPolySemiRoundRect::CPrimPolySemiRoundRect(void)
{
	}

// Overridables
	
void CPrimPolySemiRoundRect::FindTextRect(void)
{
	int nEdge = m_pEdge->GetWidth() + 1;

	int yStep = 1 * m_Corner.cy / 2;

	m_TextRect.x1 = m_DrawRect.x1 + nEdge;
	
	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - nEdge;
	
	m_TextRect.y2 = m_DrawRect.y2 - yStep;

	AdjustTextRect();
	}

// Generation

void CPrimPolySemiRoundRect::MakePoints(CSize Max)
{
	CPrimPolySemiTrimRect::MakePoints(Max);

	m_dwRound = 0x000011;
	}

// Meta Data

void CPrimPolySemiRoundRect::AddMetaData(void)
{
	CPrimPolySemiTrimRect::AddMetaData();

	Meta_SetName((IDS_SEMI_ROUNDED));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Semi Plaque
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolySemiPlaque, CPrimPolySemiTrimRect);

// Constructor

CPrimPolySemiPlaque::CPrimPolySemiPlaque(void)
{
	}
	
// Overridables
	
void CPrimPolySemiPlaque::FindTextRect(void)
{
	int nEdge = m_pEdge->GetWidth() + 1;

	int yStep = 7 * m_Corner.cy / 8;

	m_TextRect.x1 = m_DrawRect.x1 + nEdge;
	
	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - nEdge;
	
	m_TextRect.y2 = m_DrawRect.y2 - yStep;

	AdjustTextRect();
	}

// Generation

void CPrimPolySemiPlaque::MakePoints(CSize Max)
{
	CPrimPolySemiTrimRect::MakePoints(Max);

	m_dwRound = 0x110011;
	}

// Meta Data

void CPrimPolySemiPlaque::AddMetaData(void)
{
	CPrimPolySemiTrimRect::AddMetaData();

	Meta_SetName((IDS_SEMI_PLAQUE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Balloon
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyBalloon, CPrimPoly);

// Constructor

CPrimPolyBalloon::CPrimPolyBalloon(void)
{
	m_Corner = CPoint(16, 16);

	m_Base   = 8;

	m_Point  = 8;
	}

// Overridables

void CPrimPolyBalloon::FindTextRect(void)
{
	int xStep = 3 * m_Corner.cx / 4;

	int yStep = 3 * m_Corner.cy / 4;

	int ySize = m_DrawRect.y2 - m_DrawRect.y1;

	int yBase = min(m_Base, ySize - 8);

	int nLeft = yBase - 2 * yStep;

	int nTrip = GetTripSize();

	if( nLeft <= nTrip ) {

		xStep = m_Corner.cx;

		yStep = 2;
		}

	m_TextRect.x1 = m_DrawRect.x1 + xStep;

	m_TextRect.y1 = m_DrawRect.y1 + yStep;

	m_TextRect.x2 = m_DrawRect.x2 - xStep;
	
	m_TextRect.y2 = m_DrawRect.y1 + yBase - yStep;

	AdjustTextRect();
	}

BOOL CPrimPolyBalloon::GetHand(UINT uHand, CPrimHand &Hand)
{
	int cx = m_DrawRect.x2 - m_DrawRect.x1;

	int cy = m_DrawRect.y2 - m_DrawRect.y1;

	if( uHand == 0) {

		Hand.m_Pos  = m_Corner;

		Hand.m_pTag = L"Corner";

		Hand.m_uRef = 0;

		Hand.m_Clip = CRect(0, 0, cx / 3, cy / 3);

		return TRUE;
		}

	if( uHand == 1 ) {

		Hand.m_Pos.x = cx / 2;

		Hand.m_Pos.y = m_Base;

		Hand.m_pTag  = L"Base";

		Hand.m_uRef  = 0;

		Hand.m_Clip  = CRect(cx / 2, 8, cx / 2, cy - 8);

		return TRUE;
		}

	if( uHand == 2 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		Hand.m_Pos.x = m_Point;

		Hand.m_Pos.y = 0;

		Hand.m_pTag  = L"Point";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 0, cx - 1, 0);

		return TRUE;
		}

	return FALSE;
	}

void CPrimPolyBalloon::SetHand(BOOL fInit)
{
	if( fInit ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		m_Corner.cx = 1 * cx / 6;

		m_Corner.cy = 1 * cy / 6;

		m_Base      = 2 * cy / 3;

		m_Point     = 1 * cx / 4;
		}

	CPrimPoly::SetHand(fInit);
	}

// Generation

void CPrimPolyBalloon::MakePoints(CSize Max)
{
	m_uCount   = 11;

	m_dwRound  = 0x000215;

	m_pList    = New P2 [ m_uCount ];

	int yBase  = min(m_Base, Max.cy - 8);

	int xCorn  = min(m_Corner.cx, Max.cx / 3);

	int yCorn  = min(m_Corner.cy, yBase  / 3);

	int xPoint = min(m_Point, Max.cx);

	int xGap   = min(Max.cx / 8, Max.cx / 2 - xCorn);

	m_pList[ 0].x = 0;
	m_pList[ 0].y = yCorn;

	m_pList[ 1].x = xCorn,
	m_pList[ 1].y = 0;

	m_pList[ 2].x = Max.cx - xCorn;
	m_pList[ 2].y = 0;

	m_pList[ 3].x = Max.cx;
	m_pList[ 3].y = yCorn;

	m_pList[ 4].x = Max.cx;
	m_pList[ 4].y = yBase - yCorn;

	m_pList[ 5].x = Max.cx - xCorn;
	m_pList[ 5].y = yBase;

	m_pList[ 6].x = Max.cx / 2 + xGap;
	m_pList[ 6].y = yBase;

	m_pList[ 7].x = xPoint,
	m_pList[ 7].y = Max.cy;

	m_pList[ 8].x = Max.cx / 2 - xGap;
	m_pList[ 8].y = yBase;

	m_pList[ 9].x = xCorn;
	m_pList[ 9].y = yBase;

	m_pList[10].x = 0;
	m_pList[10].y = yBase - yCorn;
	}

// Meta Data

void CPrimPolyBalloon::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_AddPoint  (Corner);
	Meta_AddInteger(Base);
	Meta_AddInteger(Point);

	Meta_SetName((IDS_BALLOON));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Round Tank
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyRoundTank, CPrimPoly);

// Constructor

CPrimPolyRoundTank::CPrimPolyRoundTank(void)
{
	m_Base = 8;
	}

// Overridables

void CPrimPolyRoundTank::FindTextRect(void)
{
	int nAdjust   = m_pEdge->GetWidth();

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust - m_Base;

	AdjustTextRect();
	}

BOOL CPrimPolyRoundTank::GetHand(UINT uHand, CPrimHand &Hand)
{
	int cy = m_DrawRect.y2 - m_DrawRect.y1;

	if( uHand == 0 ) {

		Hand.m_Pos.x = 0;

		Hand.m_Pos.y = m_Base;

		Hand.m_pTag  = L"Base";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 8, 0, cy - 8);

		return TRUE;
		}

	return FALSE;
	}

void CPrimPolyRoundTank::SetHand(BOOL fInit)
{
	if( fInit ) {

		int cx = m_DrawRect.y2 - m_DrawRect.y1;

		m_Base = 1 * cx / 2;
		}

	CPrimPoly::SetHand(fInit);
	}

// Generation

void CPrimPolyRoundTank::MakePoints(CSize Max)
{
	m_uCount   = 5;

	m_dwRound  = 0x0C;

	m_pList    = New P2 [ m_uCount ];

	int yBase  = Max.cy - min(m_Base, Max.cy - 8);

	m_pList[ 0].x = 0;
	m_pList[ 0].y = 0;

	m_pList[ 1].x = Max.cx,
	m_pList[ 1].y = 0;

	m_pList[ 2].x = Max.cx;
	m_pList[ 2].y = yBase;

	m_pList[ 3].x = Max.cx / 2;
	m_pList[ 3].y = Max.cy;

	m_pList[ 4].x = 0;
	m_pList[ 4].y = yBase;
	}

// Meta Data

void CPrimPolyRoundTank::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_AddInteger(Base);

	Meta_SetName((IDS_ROUND_TANK));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Conical Tank
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPolyConicalTank, CPrimPoly);

// Constructor

CPrimPolyConicalTank::CPrimPolyConicalTank(void)
{
	m_Base = 8;

	m_Flat = 8;
	}

// Overridables

void CPrimPolyConicalTank::FindTextRect(void)
{
	int nAdjust   = m_pEdge->GetWidth();

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust - m_Base;

	AdjustTextRect();
	}

BOOL CPrimPolyConicalTank::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		Hand.m_Pos.x = 0;

		Hand.m_Pos.y = m_Base;

		Hand.m_pTag  = L"Base";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 8, 0, cy - 8);

		return TRUE;
		}

	if( uHand == 1 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;
	
		Hand.m_Pos.x = m_Flat;

		Hand.m_Pos.y = 0;

		Hand.m_pTag  = L"Flat";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 0, cx / 2, 0);

		return TRUE;
		}

	return FALSE;
	}

void CPrimPolyConicalTank::SetHand(BOOL fInit)
{
	if( fInit ) {

		int cx = m_DrawRect.y2 - m_DrawRect.y1;

		m_Base = 1 * cx / 3;

		m_Flat = 1 * cx / 4;
		}

	CPrimPoly::SetHand(fInit);
	}

// Generation

void CPrimPolyConicalTank::MakePoints(CSize Max)
{
	m_uCount   = 6;

	m_dwRound  = 0;

	m_pList    = New P2 [ m_uCount ];

	int yBase  = Max.cy - min(m_Base, Max.cy - 8);

	m_pList[ 0].x = 0;
	m_pList[ 0].y = 0;

	m_pList[ 1].x = Max.cx,
	m_pList[ 1].y = 0;

	m_pList[ 2].x = Max.cx;
	m_pList[ 2].y = yBase;

	m_pList[ 3].x = Max.cx - m_Flat;
	m_pList[ 3].y = Max.cy;

	m_pList[ 4].x = m_Flat;
	m_pList[ 4].y = Max.cy;

	m_pList[ 5].x = 0;
	m_pList[ 5].y = yBase;
	}

// Meta Data

void CPrimPolyConicalTank::AddMetaData(void)
{
	CPrimPoly::AddMetaData();

	Meta_AddInteger(Base);
	Meta_AddInteger(Flat);

	Meta_SetName((IDS_CONICAL_TANK));
	}

// End of File
