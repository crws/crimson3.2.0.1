
#include "intern.hpp"

#include "imgdlg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Image Management Dialog
//

// TODO -- Track item properly during operations.

// REV3 -- Switch from dialog to dynamically sized window.

// Runtime Class

AfxImplementRuntimeClass(CImageManagerDialog, CStdToolbarDialog);

// Constructor

CImageManagerDialog::CImageManagerDialog(CImageManager *pImages)
{
	m_pImages = pImages;

	m_pList   = m_pImages->m_pFiles;

	m_fRead   = m_pImages->GetDatabase()->IsReadOnly();

	m_pView   = New CListView;

	m_uSort   = 0;

	m_fFlip   = FALSE;

	m_fWarn   = TRUE;

	m_pWorkDC = NULL;

	SetName(L"ImageManagerDialog");
	}

// Destructor

CImageManagerDialog::~CImageManagerDialog(void)
{
	FreePreviewData();
	}

// Message Map

AfxMessageMap(CImageManagerDialog, CStdToolbarDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_UPDATEUI)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchGetInfoType(0xC0, OnToolGetInfo)
	AfxDispatchControlType(0xC0, OnToolControl)
	AfxDispatchCommandType(0xC0, OnToolCommand)

	AfxDispatchCommand(IDCANCEL, OnCancel)
	
	AfxDispatchNotify (200, LVN_ITEMCHANGED, OnItemChanged)
	AfxDispatchNotify (200, LVN_COLUMNCLICK, OnColumnClick)

	AfxMessageEnd(CImageManagerDialog)
	};

// Message Handlers

BOOL CImageManagerDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	MakeList();

	FindPreviewData();

	LoadList();

	ShowList();

	return FALSE;
	}

void CImageManagerDialog::OnUpdateUI(void)
{
	m_pTB->AddGadget(New CButtonGadget(0xC001, 0x10000002, CString(IDS_EXPORT)));

	m_pTB->AddGadget(New CButtonGadget(0xC002, 0x1000002A, CString(IDS_REPLACE)));

	m_pTB->AddGadget(New CRidgeGadget);
	
	m_pTB->AddGadget(New CButtonGadget(0xC003, 0x10000002, CString(IDS_EXPORT_MISSING)));

	m_pTB->AddGadget(New CRidgeGadget);
	
	m_pTB->AddGadget(New CButtonGadget(0xC004, 0x10000029, CString(IDS_PURGE_UNUSED)));

	m_pTB->AddGadget(New CRidgeGadget);
	}

void CImageManagerDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_pWorkDC ) {

		DC.BitBlt( m_Preview,
			   *m_pWorkDC,
			   CPoint(0, 0),
			   SRCCOPY
			   );
		}
	}

// Command Handlers

BOOL CImageManagerDialog::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	return TRUE;
	}

BOOL CImageManagerDialog::OnToolControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case 0xC001:

			Src.EnableItem(m_pFile && m_pFile->IsValid() && !m_pFile->IsOnDisk());

			break;

		case 0xC002:
	
			Src.EnableItem(!m_fRead && m_pFile && m_pFile->m_Used);

			break;

		case 0xC003:

			Src.EnableItem(m_uMiss ? TRUE : FALSE);

			break;

		case 0xC004:

			Src.EnableItem(!m_fRead && (m_uUsed < m_uCount || m_pImages->m_pViews->GetItemCount()));

			break;
		}

	return TRUE;
	}

BOOL CImageManagerDialog::OnToolCommand(UINT uID)
{
	if( !m_fRead ) {

		switch( uID ) {

			case 0xC001:

				OnExport();

				break;

			case 0xC002:

				OnReplace();

				break;

			case 0xC003:

				OnExportAll();

				break;

			case 0xC004:

				OnPurge();

				break;
			}

		return TRUE;
		}

	return TRUE;
	}

BOOL CImageManagerDialog::OnCancel(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CImageManagerDialog::OnExport(void)
{
	DoExport();

	return TRUE;
	}

BOOL CImageManagerDialog::OnReplace(void)
{
	CImageSelectItem *pItem = New CImageSelectItem;

	pItem->SetParent(m_pImages);

	pItem->Init();

	pItem->m_pImage = New CPrimImage;

	pItem->m_pImage->SetParent(pItem);

	pItem->m_pImage->Init();

	CString Title  = CString(IDS_REPLACE_IMAGE);

	UINT    uImage = m_pFile->GetIndex();

	pItem->m_pImage->m_Image = uImage;

	CItemDialog Dialog(pItem, Title);

	if( Dialog.Execute(ThisObject) ) {

		if( pItem->m_pImage->m_Image != uImage ) {

			if( Warn() ) {

				UINT uOld = uImage | refImage;

				UINT uNew = pItem->m_pImage->m_Image;

				CUIManager *pUI = (CUIManager *) m_pImages->GetParent();

				pUI->m_pPages->EditRef(uOld, uNew);

				pUI->SetDirty();

				m_pList->RemoveItem(m_pFile);

				m_uFile = pItem->m_pImage->m_Image;

				LoadList();

				DoEnables();
				}
			}
		}

	pItem->Kill();

	delete pItem;

	return TRUE;
	}

BOOL CImageManagerDialog::OnExportAll(void)
{
	CString Text = IDS_EXPORT_ALL_IMAGE;

	if( YesNo(Text) == IDYES ) {

		afxThread->SetWaitMode(TRUE);

		for( UINT n = 0; n < m_uCount; n++ ) {

			CListViewItem Item(LVIF_PARAM);

			m_pView->GetItem(n, 0, Item);

			m_uFile = n;

			m_pFile = (CImageFileItem *) Item.lParam;

			if( m_pFile->IsValid() ) {
				
				if( !m_pFile->IsOnDisk() ) {

					if( !m_pFile->m_File.IsEmpty() ) {

						if( !DoExport() ) {

							break;
							}
						}
					}
				}
			}

		afxThread->SetWaitMode(FALSE);

		LoadList();

		DoEnables();
		}

	return TRUE;
	}

BOOL CImageManagerDialog::OnPurge(void)
{
	if( Warn() ) {

		UINT uCount = m_pList->GetIndexCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CImageFileItem *pFile = m_pList->GetItem(n);

			if( pFile ) {

				if( !pFile->m_Used ) {

					m_pList->DeleteItem(pFile);
					}
				}
			}

		LoadList();

		m_pImages->m_pViews->DeleteAllItems(TRUE);
		}

	return TRUE;
	}

// Notification Handlers

void CImageManagerDialog::OnItemChanged(UINT uID, NMLISTVIEW &Info)
{
	if( Info.uNewState & LVIS_SELECTED ) {

		m_pFile = (CImageFileItem *) Info.lParam;

		m_uFile = Info.iItem;

		ShowPreview();

		DoEnables();
		}
	}

void CImageManagerDialog::OnColumnClick(UINT uID, NMLISTVIEW &Info)
{
	if( m_uSort == UINT(Info.iSubItem) ) {

		m_fFlip = !m_fFlip;
		}
	else {
		m_uSort = Info.iSubItem;

		m_fFlip = FALSE;
		}

	m_pView->SortItems(FARPROC(SortStatic), LPARAM(this));
	}

// Sorting

int CImageManagerDialog::SortList(LPARAM p1, LPARAM p2)
{
	CImageFileItem *f1 = (CImageFileItem *) p1;
	
	CImageFileItem *f2 = (CImageFileItem *) p2;

	switch( m_uSort ) {

		case 0:
			return SortData(f1, f2, "SN");

		case 1:
			return SortData(f1, f2, "N");

		case 2:
			return SortData(f1, f2, "TSN");

		case 3:
			return SortData(f1, f2, "USN");

		case 4:
			return SortData(f1, f2, "VSN");

		case 5:
			return SortData(f1, f2, "DSN");
		}

	return 0;
	}

int CImageManagerDialog::SortData(CImageFileItem *f1, CImageFileItem *f2, PCSTR pList)
{
	while( *pList ) {

		int n = SortData(f1, f2, *pList);

		if( n ) {

			return m_fFlip ? -n : +n;
			}

		pList++;
		}

	return 0;
	}

int CImageManagerDialog::SortData(CImageFileItem *f1, CImageFileItem *f2, char cKey)
{
	if( cKey == 'S' ) {

		return wstrcmp(f1->GetSourceType(), f2->GetSourceType());
		}

	if( cKey == 'N' ) {

		return wstrcmp(f1->GetSourceName(), f2->GetSourceName());
		}

	if( cKey == 'T' ) {

		return intcmp(f1->m_Type, f2->m_Type);
		}

	if( cKey == 'U' ) {

		return intcmp(f1->m_Used, f2->m_Used);
		}


	if( cKey == 'V' ) {

		return intcmp(f1->IsValid(), f2->IsValid());
		}


	if( cKey == 'D' ) {

		return intcmp(f1->IsOnDisk(), f2->IsOnDisk());
		}

	return 0;
	}

// Sort Function

int CImageManagerDialog::SortStatic(LPARAM p1, LPARAM p2, LPARAM ps)
{
	return ((CImageManagerDialog *) ps)->SortList(p1, p2);
	}

// Implementation

void CImageManagerDialog::MakeList(void)
{
	CWnd  &Wnd = GetDlgItem(100);

	CRect Rect = Wnd.GetWindowRect();

	ScreenToClient(Rect);

	DWORD dwStyle = LVS_REPORT        |
			LVS_SHOWSELALWAYS |
			LVS_SINGLESEL     |
			WS_BORDER         |
			WS_TABSTOP        ;

	m_pView->Create( dwStyle,
			 Rect,
			 ThisObject,
			 200
			 );

	DWORD dwExStyle = LVS_EX_FULLROWSELECT;

	m_pView->SetExtendedListViewStyle( dwExStyle,
					   dwExStyle
					   );

	int cx = Rect.cx() / 32;

	int c0 = 4 * cx;

	int c2 = 4 * cx;
	
	int c3 = 3 * cx;
	
	int c4 = 3 * cx;
	
	int c5 = 3 * cx;
	
	int c1 = Rect.cx() - c0 - c2 - c3 - c4 - c5 - 20;

	CListViewColumn Col0(0, LVCFMT_LEFT, c0, CString(IDS_SOURCE));

	CListViewColumn Col1(1, LVCFMT_LEFT, c1, CString(IDS_NAME));

	CListViewColumn Col2(2, LVCFMT_LEFT, c2, CString(IDS_TYPE));
	
	CListViewColumn Col3(3, LVCFMT_LEFT, c3, CString(IDS_USED));

	CListViewColumn Col4(4, LVCFMT_LEFT, c4, CString(IDS_VALID));
	
	CListViewColumn Col5(5, LVCFMT_LEFT, c5, CString(IDS_DISK));

	m_pView->InsertColumn(0, Col0);
	
	m_pView->InsertColumn(1, Col1);
	
	m_pView->InsertColumn(2, Col2);
	
	m_pView->InsertColumn(3, Col3);
	
	m_pView->InsertColumn(4, Col4);
	
	m_pView->InsertColumn(5, Col5);

	Wnd.DestroyWindow(TRUE);
	}

void CImageManagerDialog::LoadList(void)
{
	m_pImages->UpdateUsed();

	m_pView->DeleteAllItems();

	m_uCount = 0;

	m_uMiss  = 0;

	m_uUsed  = 0;

	UINT uCount = m_pList->GetIndexCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CImageFileItem *pFile = m_pList->GetItem(n);

		if( pFile ) {

			CString From  = pFile->GetSourceType();

			CString Name  = pFile->GetSourceName();
			 
			CPrintf Type  = pFile->GetTypeName();

			CString Used  = pFile->m_Used     ? CString(IDS_YES) : CString(IDS_NO);

			CString Valid = pFile->IsValid () ? CString(IDS_YES) : CString(IDS_NO);
			
			CString Disk  = pFile->IsOnDisk() ? CString(IDS_YES) : CString(IDS_NO);

			int i = m_pView->InsertItem(CListViewItem(n, 0, From, 0, LPARAM(pFile)));

			m_pView->SetItem(CListViewItem(i, 1, Name,  0));
			
			m_pView->SetItem(CListViewItem(i, 2, Type,  0));
			
			m_pView->SetItem(CListViewItem(i, 3, Used,  0));
			
			m_pView->SetItem(CListViewItem(i, 4, Valid, 0));
			
			m_pView->SetItem(CListViewItem(i, 5, Disk,  0));

			if( pFile->m_Used ) {

				m_uUsed++;
				}

			if( pFile->IsValid() ) {
				
				if( !pFile->IsOnDisk() ) {

					if( !pFile->m_File.IsEmpty() ) {

						m_uMiss++;
						}
					}
				}

			m_uCount++;
			}
		}

	m_pView->SortItems(FARPROC(SortStatic), LPARAM(this));

	m_pFile = NULL;

	m_uFile = 0;

	if( m_uCount ) {

		m_pView->SetItemState(m_uFile, LVIS_SELECTED, LVIS_SELECTED);
		}

	DoEnables();
	}

void CImageManagerDialog::ShowList(void)
{
	m_pView->ShowWindow(SW_SHOW);

	m_pView->SetFocus();
	}

void CImageManagerDialog::UpdateUsed(void)
{
	m_pImages->UpdateUsed();

	m_uUsed = 0;

	UINT c  = m_pView->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CListViewItem Item(LVIF_PARAM);

		m_pView->GetItem(n, 0, Item);

		CImageFileItem *pFile = (CImageFileItem *) Item.lParam;

		CString         Used  = pFile->m_Used ? CString(IDS_YES) : CString(IDS_NO);

		m_pView->SetItem(CListViewItem(n, 3, Used, 0));

		if( pFile->m_Used ) {

			m_uUsed++;
			}
		}
	}

void CImageManagerDialog::DoEnables(void)
{
	SendMessage(WM_GOINGIDLE);
	}

BOOL CImageManagerDialog::Warn(void)
{
	if( m_fWarn ) {

		CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

		if( System.KillUndoList() ) {

			m_fWarn = FALSE;

			m_pImages->m_pViews->DeleteAllItems(TRUE);

			return TRUE;
			}

		return FALSE;
		}

	m_pImages->m_pViews->DeleteAllItems(TRUE);

	return TRUE;
	}

void CImageManagerDialog::FindPreviewData(void)
{
	CWnd  &Wnd = GetDlgItem(101);

	CRect Rect = Wnd.GetWindowRect() - 8;

	ScreenToClient(Rect);

	CClientDC DC(ThisObject);

	CSize Size = Rect.GetSize();

	m_Preview  = Rect;

	m_pWorkDC  = New CMemoryDC(DC);

	m_pBitmap  = New CBitmap  (DC, Size.cx, Size.cy);

	m_pWorkDC->Select(*m_pBitmap);
	}

void CImageManagerDialog::ShowPreview(void)
{
	CSize Size = m_Preview.GetSize();

	CRect Rect = Size;

	m_pWorkDC->FillRect(Rect, afxBrush(BLACK));

	if( m_pFile ) {

		CImageOpts Opts;

		if( !m_pFile->IsTexture() ) {

			if( !m_pFile->IsMetaFile() ) {

				Opts.m_Keep = 1;
				}
			}

		Opts.m_Size.cx = min(m_pFile->m_Max.cx, Size.cx - 8);

		Opts.m_Size.cy = min(m_pFile->m_Max.cy, Size.cy - 8);

		CPoint Pos;

		Pos.x = (Size.cx - Opts.m_Size.cx) / 2;

		Pos.y = (Size.cy - Opts.m_Size.cy) / 2;

		m_pFile->Preview( *m_pWorkDC,
				  Pos,
				  Opts
				  );
		}

	Invalidate(m_Preview, FALSE);
	}

void CImageManagerDialog::FreePreviewData(void)
{
	m_pWorkDC->Deselect();

	delete m_pBitmap;

	delete m_pWorkDC;
	}

BOOL CImageManagerDialog::DoExport(void)
{
	CFilename File = L"";

	BOOL      fHad = FALSE;

	if( !m_pFile->m_File.IsEmpty() ) {

		File = m_pFile->GetFilename();

		if( !File.CreatePath() ) {

			CString Text = IDS_UNABLE_TO_CREATE;

			Error(Text);

			return FALSE;
			}

		fHad = TRUE;
		}
	else {
		CFilename Path = m_pImages->m_RelBase;

		UINT      uLen = Path.GetLength();

		CSaveFileDialog::LoadLastPath(L"Image", Path);

		CSaveFileDialog Dialog;

		CString Ext = m_pFile->SuggestExt();

		if( Ext.IsEmpty() ) {

			Dialog.SetFilter(CString(IDS_ALL_FILES));
			}
		else {
			CString a = Ext;

			CString b = Ext;

			a.MakeUpper();

			CPrintf Filter( CString(IDS_FMT_IMAGE),
					a,
					b
					);

			Dialog.SetFilter(Filter);
			}

		if( Dialog.ExecAndCheck(ThisObject) ) {

			File = Dialog.GetFilename();

			if( File.Left(uLen) == Path ) {

				File = File.Mid(uLen);

				File = L".\\" + File;
				}

			if( !m_fRead ) {

				m_pFile->m_File = File;

				m_pFile->SetDirty();
				}

			CString From = m_pFile->GetSourceType();

			CString Name = m_pFile->GetSourceName();

			m_pView->SetItem(CListViewItem(m_uFile, 0, From, 0));

			m_pView->SetItem(CListViewItem(m_uFile, 1, Name, 0));

			Dialog.SaveLastPath(L"Image");
			}
		}

	if( !File.IsEmpty() ) {

		HANDLE hFile = File.OpenWrite();

		if( hFile < INVALID_HANDLE_VALUE ) {

			PCBYTE pData = m_pFile->m_Data.GetPointer();

			DWORD  uSize = m_pFile->m_Data.GetCount();

			afxThread->SetWaitMode(TRUE);

			WriteFile(hFile, pData, uSize, &uSize, NULL);

			CloseHandle(hFile);

			afxThread->SetWaitMode(FALSE);

			m_pView->SetItem(CListViewItem(m_uFile, 5, CString(IDS_YES), 0));

			if( fHad ) {

				m_uMiss -= 1;
				}
	
			DoEnables();

			return TRUE;
			}

		CString Text = IDS_UNABLE_TO_WRITE;

		Error(Text);
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Image Selection Item
//

// Runtime Class

AfxImplementRuntimeClass(CImageSelectItem, CUIItem);

// Constructor

CImageSelectItem::CImageSelectItem(void)
{
	m_pImage = NULL;
	}

// Meta Data

void CImageSelectItem::AddMetaData(void)
{
	Meta_AddVirtual(Image);
	}

// End of File
