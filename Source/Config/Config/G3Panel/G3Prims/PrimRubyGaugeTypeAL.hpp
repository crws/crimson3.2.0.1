
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAL_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Linear Gauge Primitive
//

class CPrimRubyGaugeTypeAL : public CPrimRubyGaugeTypeA
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeAL(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void Draw(IGDI *pGdi, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_PointStyle;
		R2   m_Margin;

	protected:
		// Data Members
		number	      m_lineFact;
		number	      m_posMin;
		number	      m_posMax;
		number	      m_posPoint;
		number	      m_posSweep;
		number	      m_posMajor;
		number	      m_posMinor;
		number        m_posOuter;
		number	      m_posBug;
		number	      m_posBand;
		number	      m_posCenter;
		CRubyPath     m_pathPoint;
		CRubyGdiList  m_listPoint;

		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);

		// Scaling
		number GetPos(CCodedItem *pValue, C3REAL Default);

		// Path Preparation
		void PrepareTicks(void);
		void PreparePoint(void);
		BOOL PrepareBand(CRubyPath &path, BOOL fShow, CCodedItem *pMin, CCodedItem *pMax);
		BOOL PrepareBug(CRubyPath &path, BOOL fShow, CCodedItem *pValue);

		// Drawing Helpers
		void Rect(CRubyPath &path, number cx, number cy);
		void Rect(CRubyPath &path, number cx, number cy, number w);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void LoadLayout(void);
	};

// End of File

#endif
