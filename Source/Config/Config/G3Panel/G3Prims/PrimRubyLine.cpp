
#include "intern.hpp"

#include "PrimRubyLine.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenLine.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Line Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyLine, CPrimRuby);

// Constructor

CPrimRubyLine::CPrimRubyLine(void)
{
	m_uType = 0x80;

	m_pLine = New CPrimRubyPenLine;

	m_pEdge = New CPrimRubyPenEdge;
	}

// Overridables

BOOL CPrimRubyLine::HitTest(P2 Pos)
{
	if( PtInRect(m_bound, Pos) ) {

		if( m_pathTest.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathEdge.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathTrim.HitTest(Pos, 0) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

void CPrimRubyLine::Draw(IGDI *pGDI, UINT uMode)
{
	m_pLine->Register(uMode);

	m_pEdge->Register(uMode);

	m_pLine->Fill(pGDI, m_listLine, !UseFastFill());

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	m_pEdge->Trim(pGDI, m_listTrim, TRUE);
	}

void CPrimRubyLine::GetRefs(CPrimRefList &Refs)
{
	CPrim::GetRefs(Refs);

	m_pLine->GetRefs(Refs);
	}

void CPrimRubyLine::SetInitState(void)
{
	CPrim::SetInitState();

	m_pLine->Set(GetRGB(31,31,31));

	m_pLine->m_Width = 4;

	m_pEdge->Set(GetRGB(31,31,31));

	m_pEdge->m_Width = 0;
	}

// Meta Data

void CPrimRubyLine::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddObject(Line);
	Meta_AddObject(Edge);

	Meta_SetName((IDS_LINE_2));
	}

// Download Support

BOOL CPrimRubyLine::MakeInitData(CInitData &Init)
{
	CPrimRuby::MakeInitData(Init);

	Init.AddByte(BYTE(UseFastFill()));

	Init.AddItem(itemSimple, m_pLine);
	Init.AddItem(itemSimple, m_pEdge);

	AddList(Init, m_listLine);
	AddList(Init, m_listEdge);
	AddList(Init, m_listTrim);

	return TRUE;
	}

// Fast Fill Control

BOOL CPrimRubyLine::UseFastFill(void)
{
	if( m_pEdge->m_Width ) {

		if( m_pEdge->m_Edge == CRubyStroker::edgeOuter ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Path Management

void CPrimRubyLine::InitPaths(void)
{
	m_pathTest.Empty();

	m_pathLine.Empty();

	m_pathEdge.Empty();

	m_pathTrim.Empty();
	}

void CPrimRubyLine::MakePaths(void)
{
	CRubyPath  line;

	CRubyPoint p1(m_DrawRect, 1);

	CRubyPoint p2(m_DrawRect, 2);

	if( m_pLine->m_End1 >= 100 || m_pLine->m_End2 >= 100 ) {

		CRubyVector v  = p2 - p1;

		v.MakeUnit();

		if( m_pLine->m_End1 >= 100 ) {

			p1 += v * CRubyStroker::GetArrowLength(m_pLine->m_End1, m_pLine->m_Width);
			}

		if( m_pLine->m_End2 >= 100 ) {

			p2 -= v * CRubyStroker::GetArrowLength(m_pLine->m_End2, m_pLine->m_Width);
			}
		}

	line.Append(p1);

	line.Append(p2);

	line.AppendHardBreak();

	m_pLine->Stroke(m_pathLine, line);
	
	m_pEdge->StrokeEdge(m_pathEdge, m_pathLine);

	m_pEdge->StrokeTrim(m_pathTrim, m_pathEdge);

	if( m_pLine->m_Width >= 4 ) {

		m_pathTest = m_pathLine;
		}
	else {
		int old = m_pLine->m_Width;

		m_pLine->m_Width = 4;

		m_pLine->Stroke(m_pathTest, line);

		m_pLine->m_Width = old;
		}

	m_pathTest.GetBoundingRect(m_bound);

	m_pathEdge.AddBoundingRect(m_bound);

	m_pathTrim.AddBoundingRect(m_bound);
	}

void CPrimRubyLine::MakeLists(void)
{
	m_listLine.Load(m_pathLine, !UseFastFill());

	m_listEdge.Load(m_pathEdge, true);

	m_listTrim.Load(m_pathTrim, true);
	}

// End of File
