
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPenBase_HPP
	
#define	INCLUDE_PrimRubyPenBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBrush.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

class DLLAPI CPrimRubyPenBase : public CPrimRubyBrush
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPenBase(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL IsNull(void) const;
		int  GetWidth(void) const;
		int  GetInnerWidth(void) const;
		int  GetOuterWidth(void) const;

		// Stroking
		BOOL StrokeExact(CRubyPath &output, CRubyPath const &figure);
		BOOL StrokeEdge (CRubyPath &output, CRubyPath const &figure);
		BOOL StrokeOpen (CRubyPath &output, CRubyPath const &figure);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Width;
		UINT m_Edge;
		UINT m_Join;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
