
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BAR_HPP
	
#define	INCLUDE_BAR_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyBar;
class CPrimLegacyVertBar;
class CPrimLegacyHorzBar;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar
//

class CPrimLegacyBar : public CPrimRich
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyBar(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Field Requirements
		UINT GetNeedMask(void) const;

		// Data Members
		CPrimPen       * m_pEdge;
		UINT             m_Mode;
		CPrimBrush     * m_pFill;
		CPrimColor     * m_pColor3;
		UINT             m_ShowSP;

	protected:
		// Static Data
		static UINT   m_ShadeMode;
		static COLOR  m_ShadeCol3;
		static C3REAL m_ShadeData;

		// Shader
		static BOOL Shader(IGDI *pGDI, int p, int c);

		// Meta Data
		void AddMetaData(void);

		// Overridables
		virtual void DrawSP(IGDI *pGDI, R2 Rect, UINT uMode);

		// Implementation
		C3REAL GetValue(CCodedItem *pItem, C3REAL Default);
		BOOL   PrepFill(void);
		void   FillRect (IGDI *pGDI, R2 Rect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar
//

class CPrimLegacyVertBar : public CPrimLegacyBar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyVertBar(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		void DrawSP(IGDI *pGDI, R2 Rect, UINT uMode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar
//

class CPrimLegacyHorzBar : public CPrimLegacyBar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyHorzBar(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		void DrawSP(IGDI *pGDI, R2 Rect, UINT uMode);
	};

// End of File

#endif
