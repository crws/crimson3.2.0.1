
#include "intern.hpp"

#include "PrimRubyPolygon8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 8-Sided Polygon Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPolygon8, CPrimRubyPolygon)

// Constructor

CPrimRubyPolygon8::CPrimRubyPolygon8(void)
{
	m_nSides = 8;

	m_Rotate = +225;
	}

// Meta Data

void CPrimRubyPolygon8::AddMetaData(void)
{
	CPrimRubyPolygon::AddMetaData();

	Meta_SetName(IDS_OCTAGON_2);
	}

// End of File
