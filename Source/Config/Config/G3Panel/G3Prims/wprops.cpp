
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property Data Page
//

class CPrimWidgetPropDataPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimWidgetPropDataPage(CPrimWidgetPropData *pData);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimWidgetPropData * m_pData;
		CPrimWidgetPropList * m_pList;

		// Implementation
		void LoadCount  (IUICreate *pView);
		void LoadBinding(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property Data Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimWidgetPropDataPage, CUIPage);

// Constructor

CPrimWidgetPropDataPage::CPrimWidgetPropDataPage(CPrimWidgetPropData *pData)
{
	m_pData = pData;

	m_pList = m_pData->m_pList;

	m_Title = CString(IDS_DATA);
	}

// Operations

BOOL CPrimWidgetPropDataPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadCount(pView);

	if( m_pList->GetItemCount() ) {

		BOOL fBind = m_pData->m_AutoBind;

		pView->StartTable(CString(IDS_DEFINITIONS), fBind ? 5 : 4);

		pView->AddColHead(CString(IDS_NAME));

		pView->AddColHead(CString(IDS_DESCRIPTION));

		if( fBind ) {

			pView->AddColHead(CString(IDS_BIND_TO));
			}

		pView->AddColHead(CString(IDS_DATA_TYPE));

		pView->AddColHead(CString(IDS_FLAGS));

		UINT r = 1;

		for( INDEX n = m_pList->GetHead(); !m_pList->Failed(n); m_pList->GetNext(n) ) {

			CPrimWidgetProp *pItem = m_pList->GetItem(n);

			pView->AddRowHead(CPrintf(L"%u", r++));

			pView->AddUI(pItem, L"root", L"Name");

			pView->AddUI(pItem, L"root", L"Desc");

			if( fBind ) {

				pView->AddUI(pItem, L"root", L"Bind");
				}

			pView->AddUI(pItem, L"root", L"Type");

			pView->AddUI(pItem, L"root", L"Flags");
			}

		pView->EndTable();
		}
	else {
		pView->StartGroup(CString(IDS_DEFINITIONS), 1);

		pView->AddNarrative(CString(IDS_THIS_WIDGET_HAS));

		pView->EndGroup(TRUE);
		}

	LoadBinding(pView);

	return TRUE;
	}

// Implementation

void CPrimWidgetPropDataPage::LoadCount(IUICreate *pView)
{
	CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pData), 1);

	pPage->LoadIntoView(pView, m_pData);

	delete pPage;
	}

void CPrimWidgetPropDataPage::LoadBinding(IUICreate *pView)
{
	CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pData), 2);

	pPage->LoadIntoView(pView, m_pData);

	delete pPage;
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property Data
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWidgetPropData, CUIItem);

// Constructor

CPrimWidgetPropData::CPrimWidgetPropData(void)
{
	m_Count    = 0;

	m_AutoBind = 0;

	m_AutoZoom = 0;

	m_pList    = New CPrimWidgetPropList;
	}

// UI Creation

BOOL CPrimWidgetPropData::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CPrimWidgetPropDataPage(this));

	return FALSE;
	}

// UI Update

void CPrimWidgetPropData::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Count" ) {

		UINT uPrev = m_pList->GetItemCount();

		if( m_Count != uPrev ) {

			m_pList->SetItemCount(m_Count);

			while( uPrev < m_Count ) {

				CPrimWidgetProp *pItem = m_pList->GetItem(uPrev++);

				if( pItem->m_Name.IsEmpty() ) {

					pItem->m_Name.Printf(L"Data%u", uPrev);

					pItem->m_Desc.Printf(L"Data %u", uPrev);
					}
				}
			}

		pHost->RemakeUI();
		}

	if( Tag == "AutoBind" ) {

		pHost->RemakeUI();
		}

	if( Tag == "AutoZoom" ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimWidgetPropData::GetBindState(void) const
{
	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pList->GetItem(n);

		if( pItem->GetBindState() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

BOOL CPrimWidgetPropData::Bind(CString Top, CString Tag)
{
	BOOL s = TRUE;

	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pList->GetItem(n);

		CString Code;

		if( m_AutoZoom ) {

			if( pItem->IsDetailsRef() ) {

				continue;
				}
			}

		if( pItem->m_Bind.IsEmpty() ) {

			Code += Tag;

			if( !Code.IsEmpty() ) {

				Code += L'.';
				}

			Code += pItem->m_Name;
			}
		else {
			CString Bind = pItem->m_Bind;

			if( Bind.Left(2) == L"::" ) {

				if( Bind == L"::Path" ) {

					Code += L"\"";

					Code += Tag;

					Code += L"\"";
					}

				if( Bind == L"::TopPath" ) {

					Code += L"\"";

					Code += Top;

					Code += L"\"";
					}

				if( Bind == L"::Name" ) {

					Code += L"\"";

					Code += Tag.Mid(Tag.FindRev('.')+1);

					Code += L"\"";
					}

				if( Bind == L"::TopName" ) {

					Code += L"\"";

					Code += Top.Mid(Top.FindRev('.')+1);

					Code += L"\"";
					}
				}
			else {
				Code += Tag;

				while( Bind[0] == '^' ) {

					Bind.Delete(0, 1);

					UINT uPos = Code.FindRev('.');

					if( uPos < NOTHING ) {

						Code = Code.Left(uPos);
						}
					else
						Code.Empty();
					}

				if( !Bind.IsEmpty() ) {

					if( !Code.IsEmpty() ) {

						Code += L'.';
						}

					Code += Bind;
					}
				}
			}

		if( !pItem->Bind(Code, FALSE) ) {

			s = FALSE;
			}
		}

	return s;
	}

void CPrimWidgetPropData::ClearBinding(void)
{
	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pList->GetItem(n);

		pItem->ClearBinding();
		}
	}

void CPrimWidgetPropData::Recompile(void)
{
	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CPrimWidgetProp *pItem = m_pList->GetItem(n);

		if( pItem->m_pValue ) {

			CTypeDef Type;

			pItem->GetTypeData(Type);

			pItem->m_pValue->SetReqType(Type);

			pItem->m_pValue->Recompile(TRUE);
			}
		}
	}

// Persistance

void CPrimWidgetPropData::Init(void)
{
	CUIItem::Init();

	m_pList->SetItemCount(m_Count);

	for( UINT n = 0; n < m_Count; n++ ) {

		CPrimWidgetProp *pItem = m_pList->GetItem(n);

		pItem->m_Name.Printf(L"Data%u",  n+1);

		pItem->m_Desc.Printf(L"Data %u", n+1);
		}
	}

// Download Support

BOOL CPrimWidgetPropData::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pList);

	return TRUE;
	}

// Meta Data

void CPrimWidgetPropData::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Count);
	Meta_AddInteger(AutoBind);
	Meta_AddInteger(AutoZoom);
	Meta_AddString (Class);
	Meta_AddString (SubItem);
	Meta_AddString (Zoom);
	Meta_AddCollect(List);

	Meta_SetName((IDS_GROUP_PROPERTY));
	}

// Implementation

void CPrimWidgetPropData::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Class",    m_AutoBind);

	pHost->EnableUI("SubItem",  m_AutoBind);

	pHost->EnableUI("AutoZoom", m_AutoBind);

	pHost->EnableUI("Zoom",     m_AutoBind && m_AutoZoom);
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property List
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWidgetPropList, CItemIndexList);

// Constructor

CPrimWidgetPropList::CPrimWidgetPropList(void)
{
	m_Class = AfxRuntimeClass(CPrimWidgetProp);
	}

// Item Access

CPrimWidgetProp * CPrimWidgetPropList::GetItem(INDEX Index) const
{
	return (CPrimWidgetProp *) CItemIndexList::GetItem(Index);
	}

CPrimWidgetProp * CPrimWidgetPropList::GetItem(UINT uPos) const
{
	return (CPrimWidgetProp *) CItemIndexList::GetItem(uPos);
	}

// Item Lookup

CPrimWidgetProp * CPrimWidgetPropList::FindName(CString const &Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CPrimWidgetProp *pProp = GetItem(n);

		if( pProp->m_Name == Name ) {

			return pProp;
			}

		GetNext(n);
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWidgetProp, CCodedHost);

// Constructor

CPrimWidgetProp::CPrimWidgetProp(void)
{
	// LATER -- Add a translatable string?

	m_Type   = 1;

	m_Flags  = 0;

	m_pValue = NULL;
	}

// UI Update

void CPrimWidgetProp::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Type" ) {

		if( m_Type >= 2 && m_Flags ) {

			m_Flags = 0;

			pHost->UpdateUI("Flags");
			}

		DoEnables(pHost);
		}

	if( Tag == "Value" ) {

		CPrimWidget *pWidget = (CPrimWidget *) GetParent(3);

		pWidget->OnUIChange(pHost, pItem, L"PrimProp");
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimWidgetProp::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		if( GetTypeData(Type) ) {

			return TRUE;
			}
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

BOOL CPrimWidgetProp::GetTypeData(CTypeDef &Type)
{
	if( m_Type <= 2 ) {

		Type.m_Flags = 0;

		switch( m_Type ) {

			case 0:
				Type.m_Type = typeInteger;
				break;

			case 1:
				Type.m_Type = typeReal;
				break;

			case 2:
				Type.m_Type = typeString;
				break;
			}

		if( m_Flags & 1 ) {

			Type.m_Flags |= flagTagRef;
			}

		if( m_Flags & 2 ) {

			Type.m_Flags |= flagWritable;
			}

		if( m_Flags & 4 ) {

			Type.m_Flags |= flagArray;

			Type.m_Flags |= flagExtended;
			}

		if( m_Flags & 8 ) {

			Type.m_Flags |= flagElement;
			}

		return TRUE;
		}

	if( m_Type == 3 ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( m_Type == 4 ) {

		Type.m_Type  = typePage;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( m_Type == 5 ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;
		
		return TRUE;
		}

	return FALSE;
	}

// Attributes

BOOL CPrimWidgetProp::GetBindState(void) const
{
	if( m_pValue ) {
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWidgetProp::IsDetailsRef(void) const
{
	if( IsPageRef() ) {

		if( m_Name.Left(7) == L"Details" ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimWidgetProp::IsExpression(void) const
{
	if( m_Type <= 2 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWidgetProp::IsAction(void) const
{
	if( m_Type == 5 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWidgetProp::IsPageRef(void) const
{
	if( m_Type == 4 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWidgetProp::IsTagRef(void) const
{
	if( m_Flags & 1 ) {

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimWidgetProp::GetTagType(void) const
{
	if( m_pValue ) {

		return m_pValue->GetType();
		}

	return typeInteger;
	}

// Operations

BOOL CPrimWidgetProp::Bind(CString Code, BOOL fForce)
{
	if( !fForce ) {

		if( !IsExpression() ) {
			
			return TRUE;
			}

		if( m_Flags & 16 ) {

			return TRUE;
			}
		}

	if( !m_pValue ) {

		CTypeDef Type;

		GetTypeData(Type);

		m_pValue = New CCodedItem;

		m_pValue->SetParent(this);

		m_pValue->Init();

		m_pValue->SetReqType(Type);

		if( !m_pValue->Compile(Code) ) {

			m_pValue->Kill();

			delete m_pValue;

			m_pValue = NULL;

			return FALSE;
			}

		return TRUE;
		}

	if( !m_pValue->Compile(Code) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CPrimWidgetProp::ClearBinding(void)
{
	if( !IsExpression() ) {
		
		if( !IsPageRef() ) {

			return FALSE;
			}
		}
	else {
		if( m_Flags & 16 ) {

			return FALSE;
			}
		}

	if( m_pValue ) {

		m_pValue->Kill();

		delete m_pValue;

		m_pValue = NULL;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CPrimWidgetProp::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_Type));
	
	Init.AddByte(BYTE(m_Flags));

	Init.AddItem(itemVirtual, m_pValue);

	return TRUE;
	}

// Meta Data Creation

void CPrimWidgetProp::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddString (Name);
	Meta_AddString (Bind);
	Meta_AddString (Desc);
	Meta_AddInteger(Type);
	Meta_AddInteger(Flags);
	Meta_AddVirtual(Value);

	Meta_SetName((IDS_WIDGET_PRIMITIVE));
	}

// Implementation

void CPrimWidgetProp::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Flags", m_Type <= 2);
	}

// End of File
