
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBevel_HPP
	
#define	INCLUDE_PrimRubyBevel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBevelBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitive
//

class CPrimRubyBevel : public CPrimRubyBevelBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyBevel(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
