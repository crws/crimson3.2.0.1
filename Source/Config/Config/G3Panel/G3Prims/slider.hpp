
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SLIDER_HPP
	
#define	INCLUDE_SLIDER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacySlider;
class CPrimLegacyVertSlider;
class CPrimLegacyHorzSlider;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Slider
//

class CPrimLegacySlider : public CPrimRich
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacySlider(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_Mode;
		UINT         m_Accel;
		CPrimPen   * m_pEdge;
		CPrimBrush * m_pKnob;
		CPrimColor * m_pBack;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Field Requirements
		UINT GetNeedMask(void) const;

		// Implementation
		void   DrawBase(IGDI *pGDI, R2 &Rect);
		void   DrawBtnFrame(IGDI *pGDI, R2 &Rect);
		void   FindPoints(P2 *t, P2 *b, R2 &r, int s);
		C3REAL FindPosition(void);
		void   DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Slider
//

class CPrimLegacyVertSlider : public CPrimLegacySlider
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyVertSlider(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DrawSlot(IGDI *pGDI, R2 &Rect);
		void DrawBtn1(IGDI *pGDI, R2 Rect);
		void DrawBtn2(IGDI *pGDI, R2 Rect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Slider
//

class CPrimLegacyHorzSlider : public CPrimLegacySlider
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyHorzSlider(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DrawSlot(IGDI *pGDI, R2 &Rect);
		void DrawBtn1(IGDI *pGDI, R2 Rect);
		void DrawBtn2(IGDI *pGDI, R2 Rect);
	};

// End of File

#endif
