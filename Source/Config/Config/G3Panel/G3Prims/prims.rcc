
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Geometric Figure
//

CPrimGeom_Schema RCDATA
BEGIN
	L"Fill,Fill,,,,\0"
	L"Edge,Edge,,,,\0"

	L"\0"
END

CPrimGeom_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"\0"
END

CPrimGeom_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Rectangle
//

CPrimRect_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color,Corner\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Wedge and Ellipse Quadrant
//

CPrimWedge_Schema RCDATA
BEGIN
	L"Orient,Position,,Enum/DropDown,Top-Right|Top-Left|Bottom-Left|Bottom-Right,"
	L"Specify the location of the element within the bounding rectangle."
	L"\0"

	L"\0"
END

CPrimWedge_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"G:1,root,Orientation,Orient\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Ellipse Half
//

CPrimEllipseHalf_Schema RCDATA
BEGIN
	L"Orient,Position,,Enum/DropDown,Right|Top|Left|Bottom,"
	L"Specify the location of the element within the bounding rectangle."
	L"\0"

	L"\0"
END

CPrimEllipseHalf_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"G:1,root,Orientation,Orient\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Animated Image
//

CPrimAnimImage_Schema RCDATA
BEGIN
	L"Count,Image Count,,Integer/EditBox,|0||1|10,"
	L"Define the number of images to be shown by this primitive."
	L"\0"

	L"Value,Image Select,,Coded/Expression,,"
	L"Define an expression to choose the image to be displayed."
	L"\0"

	L"Visible,Show Item,,Coded/Expression,|true,"
	L"Enter an expression to show or hide the entire primitive."
	L"\0"

	L"Color,Use Colors,,Coded/Expression,|true,"
	L"Enter an expression to control image coloring. An empty expression "
	L"or a value of true will result in a color imaged being drawn, while "
	L"a value of false will result in the image being rendered in "
	L"monochrome as might be used with a disabled button."
	L"\0"

	L"Show,Show Image,,Coded/Expression,|true,"
	L"Enter an expression to show or hide the image."
	L"\0"

	L"Keep,Maintain Aspect,,Enum/DropDown,No|Yes,"
	L"Indicate whether to constrain the image size so "
	L"as to maintain the aspect ratio of the source."
	L"\0"
	
	L"Image01,Image 0,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image02,Image 1,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image03,Image 2,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image04,Image 3,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image05,Image 4,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image06,Image 5,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image07,Image 6,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image08,Image 7,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image09,Image 8,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image10,Image 9,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"\0"
END

CPrimAnimImage_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,Count,Value,Color,Show,Keep\0"
	L"G:2,root,Images,Image01,Image02,Image03,Image04,Image05,Image06,Image07,Image08,Image09,Image10\0"
	L"\0"
END

CPrimAnimImage_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"\0"
END

CPrimAnimImage_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Filled Polylines
//

CPrimPoly_Schema RCDATA
BEGIN
	L"Orient,Mirror,,Enum/DropDown,None|Horizontal|Diagonal|Both,"
	L"Select a transformation to be applied to the figure."
	L"\0"

	L"SpinPos,Position,,Coded/Expression,|0,"
	L"Enter the value used to define how much the primitive is rotated."
	L"\0"

	L"SpinMin,Minimum,,Coded/Expression,|0,"
	L"Enter the value that corresponds to a rotation of 0 degrees."
	L"\0"

	L"SpinMax,Maximum,,Coded/Expression,|360,"
	L"Enter the value that corresponds to a rotation of 360 degrees."
	L"\0"

	L"\0"
END

CPrimPoly_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Rotation,SpinPos,SpinMin,SpinMax\0"
	L"\0"
END

CPrimPoly_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

CPrimPolygon_Schema RCDATA
BEGIN
	L"Rotate,Rotation,,Integer/EditBox,|1|deg|-3600|+3600,"
	L"Define the amount by which the figure is to be rotated."
	L"\0"

	L"\0"
END

CPrimPolygon_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"G:1,root,Adjustment,Rotate\0"
	L"\0"
END

CPrimPolyStar_Schema RCDATA
BEGIN
	L"AltRad,Inner Radius,,Integer/EditBox,|0|%|10|90,"
	L"Define the pointedness of the star."
	L"\0"

	L"\0"
END

CPrimPolyStar_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"G:1,root,Adjustment,Rotate,AltRad\0"
	L"\0"
END

CPrimPolyArrow_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"G:1,root,Orientation,Orient\0"
	L"\0"
END

CPrimPolySemiTrimRect_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Fill,Fill Behavior,Mode,Value,Min,Max\0"
	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"G:1,root,Orientation,Orient\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Line
//

CPrimLine_Pages RCDATA
BEGIN
	L"Format\0"
END

CPrimLine_Schema RCDATA
BEGIN
	L"\0"
END

CPrimLine_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Pen,Line Format,Width,Color\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Barcode
//

CPrimBarcode_Pages RCDATA
BEGIN
	L"Format,Show\0"
END

CPrimBarcode_Schema RCDATA
BEGIN
	L"Value,Value,,Coded/Expression,|\x002212345678\x0022,"
	L"Enter a string expression to be rendered as a barcode.\n"
	L"Barcode strings are limited to 1400 characters."
	L"\0"

	L"Code,Symbology,,BarcodeSymbology/DropDown,,"
	L"Select the required barcode type."
	L"\0"

	L"Color1,Paper,,Color/Color,,"
	L"Define the color used for the background of the barcode."
	L"\0"

	L"Color2,Ink,,Color/Color,,"
	L"Define the color used for the ink of the barcode."
	L"\0"

	L"Border,Border,,Integer/EditBox,|0|pixels|0|64,"
	L"Select the width of the border around the barcode."
	L"\0"

	L"NoShrink,Shrink Image To Fit,,Enum/DropDown,Enabled|Disabled,"
	L"Enable or disabling reducing the barcode size to fit the figure."
	L"\0"

	L"NoGrow,Grow Image To Fit,,Enum/DropDown,Enabled|Disabled,"
	L"Enable or disabling reducing the barcode size to fit the figure."
	L"\0"

	L"NoNonInt,Non-Integral Scaling,,Enum/DropDown,Enabled|Disabled,"
	L"Enable or disabling scaling the barcode by non-integral factors."
	L"\0"

	L"AlignH,Horizontal Position,,Enum/DropDown,Left|Center|Right,"
	L"Select the position of the barcode within the figure."
	L"\0"

	L"AlignV,Vertical Position,,Enum/DropDown,Top|Middle|Bottom,"
	L"Select the position of the barcode within the figure."
	L"\0"

	L"\0"
END

CPrimBarcode_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data,Value\0"
	L"G:1,root,Format,Code,Color1,Color2,Border\0"
	L"G:1,root,Scaling,NoNonInt,NoShrink,NoGrow\0"
	L"G:1,root,Layout,AlignH,AlignV\0"
	L"\0"
END

CPrimBarcode_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Bevel Base Class
//

CPrimBevelBase_Schema RCDATA
BEGIN
	L"Border,Edge Width,,Integer/EditBox,|0||1|10,"
	L"Define the width of the edge drawn around the primitive."
	L"\0"

	L"Hilite,Highlight,,Color/Color,,"
	L"Select the color of the highlighted portions of the edge."
	L"\0"

	L"Shadow,Shadow,,Color/Color,,"
	L"Select the color of the shadowed portions of the edge."
	L"\0"

	L"\0"
END

CPrimBevelBase_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Face,Face Format,Pattern,Color1,Color2\0"
	L"G:1,root,Edge Style,Border\0"
	L"G:1,root,Edge Format,Hilite,Shadow\0"
	L"\0"
END

CPrimBevelBase_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Bevel
//

CPrimBevel_Schema RCDATA
BEGIN
	L"Type,Edge Style,,Enum/DropDown,Raised|Sunken|Border,"
	L"Select the required edge type."
	L"\0"

	L"\0"
END

CPrimBevel_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Face,Face Format,Pattern,Color1,Color2\0"
	L"G:1,root,Edge Style,Type,Border\0"
	L"G:1,root,Edge Format,Hilite,Shadow\0"
	L"\0"
END

CPrimBevel_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Graduated Button
//

CPrimGradButton_Schema RCDATA
BEGIN
	L"Color1,Color 1,,Color/Color,,"
	L"Define the upper color of the fill used for the button."
	L"\0"

	L"Color2,Color 2,,Color/Color,,"
	L"Define the lower color of the fill used for the button."
	L"\0"

	L"\0"
END

CPrimGradButton_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Face Format,Color1,Color2\0"
	L"G:1,Edge,Edge Format,Width,Color\0"
	L"\0"
END

CPrimGradButton_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Image Button Base
//

CPrimImageBtnBase_Schema RCDATA
BEGIN
	L"Keep,Maintain Aspect,,Enum/DropDown,No|Yes,"
	L"Indicate whether to constrain the image size so "
	L"as to maintain the aspect ratio of the source."
	L"\0"
	
	L"\0"
END

CPrimImageBtnBase_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Illuminated Button
//

CPrimImageIllumButton_Schema RCDATA
BEGIN
	L"Value,Control,,Coded/LValue,,"
	L"Define the expression to be changed by the button."
	L"\0"

	L"State,Status,,Coded/Expression,|Same as Control,"
	L"Define the expression used to control the illumination "
	L"state of the button. If this entry is left blank, the Control "
	L"expression will be used automatically."
	L"\0"

	L"Enable,Enable,,Coded/Expression,|true,"
	L"Enter an expression to enable or disable this button."
	L"\0"

	L"Local,Remote,,Enum/DropDown,Enabled|Disabled,"
	L"Indicate whether the button can be activated via the web server."
	L"\0"

	L"Protect,Protection,,Enum/DropDown,None|Confirmed|Locked|Hard Locked,"
	L"Indicate whether a prompt should appear before this action is executed. Confirmed mode will "
	L"display a message box, and then perform the action once confirmation is provided. Locked mode "
	L"will display a message box allowing the user to unlock the action; the button or primitive "
	L"must then be pressed again to actually perform the action. Hard Locked mode is similar to "
	L"locked mode, except that the action will relock once it has been performed."
	L"\0"

	L"Button,Operation,,Enum/DropDown,Toggle|Latching|NO Momentary|NC Momentary|Turn On|Turn Off|Custom,"
	L"Select the operation mode required for this button."
	L"\0"

	L"Delay,Hold Time,,Integer/EditBox,|0|ms|0|1000,"
	L"Specify the minimum time period for which a momentary state must be active. This "
	L"feature can be used to ensure that changes to data are communicated to a remote "
	L"device before they are reversed."
	L"\0"

	L"Image01,Up and Off,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image02,Down and Off,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image03,Up and On,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image04,Down and On,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"\0"
END

CPrimImageIllumButton_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,Button,Value,State,Protect,Enable,Local,Keep,Delay\0"
	L"G:2,root,Images,Image01,Image02,Image03,Image04\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Indicator
//

CPrimImageIndicator_Schema RCDATA
BEGIN
	L"State,Status,,Coded/Expression,,"
	L"Define the expression used to control the illumination of the indicator."
	L"\0"

	L"Image01,Off,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image02,On,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"\0"
END

CPrimImageIndicator_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,State,Keep\0"
	L"G:2,root,Images,Image01,Image02\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Image Button
//

CPrimImageButton_Schema RCDATA
BEGIN
	L"Image01,Up,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image02,Down,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"\0"
END

CPrimImageButton_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,Keep\0"
	L"G:2,root,Images,Image01,Image02\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Image Toggle Base Class
//

CPrimImageToggle_Schema RCDATA
BEGIN
	L"Value,Value,,Coded/Expression,,"
	L"Define the expression to be controlled by this switch."
	L"\0"

	L"Enable,Enable,,Coded/Expression,|true,"
	L"Enter an expression to enable or disable this switch."
	L"\0"

	L"Local,Remote,,Enum/DropDown,Enabled|Disabled,"
	L"Indicate whether the switch can be activated via the web server."
	L"\0"

	L"Protect,Protection,,Enum/DropDown,None|Confirmed|Locked|Hard Locked,"
	L"Indicate whether a prompt should appear before this action is executed. Confirmed mode will "
	L"display a message box, and then perform the action once confirmation is provided. Locked mode "
	L"will display a message box allowing the user to unlock the action; the button or primitive "
	L"must then be pressed again to actually perform the action. Hard Locked mode is similar to "
	L"locked mode, except that the action will relock once it has been performed."
	L"\0"

	L"Axis,Division,,Enum/DropDown,Vertical|Horizontal,"
	L"Indicate how the images are to be considered to be "
	L"divided when deciding which portion of the switch "
	L"was activated by a touch."
	L"\0"

	L"Press1,On Pressed,,Coded/Action,|None,"
	L"Define the custom action to be executed when segment A is pressed."
	L"\0"

	L"Release1,On Released,,Coded/Action,|None,"
	L"Define the custom action to be executed when segment A is released."
	L"\0"

	L"Press2,On Pressed,,Coded/Action,|None,"
	L"Define the custom action to be executed when segment B is pressed."
	L"\0"

	L"Release2,On Released,,Coded/Action,|None,"
	L"Define the custom action to be executed when segment B is released."
	L"\0"

	L"Delay,Hold Time,,Integer/EditBox,|0|ms|0|1000,"
	L"Specify the minimum time period for which a momentary state must be active. This "
	L"feature can be used to ensure that changes to data are communicated to a remote "
	L"device before they are reversed."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- 2-State Toggle
//

CPrimImageToggle2_Schema RCDATA
BEGIN
	L"Mode,Actions,,Enum/DropDown,Automatic|Automatic (A)-B|Automatic A-(B)||User-Defined,"
	L"Select the operation mode required for this switch."
	L"\0"

	L"Image01,A,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image02,B,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"ValA,Value A,,Integer/EditBox,|0||0|999999,"
	L"Enter the value of the Control expression that will correspond to State A."
	L"\0"

	L"ValB,Value B,,Integer/EditBox,|0||0|999999,"
	L"Enter the value of the Control expression that will correspond to State B."
	L""
	L"\0"

	L"Default,Default,,Enum/DropDown,State A|State B,"
	L"Enter the state to display if neither Value A or Value B are matched."
	L"\0"

	L"\0"
END

CPrimImageToggle2_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,Mode,Value,Protect,Enable,Local,Axis,Keep,Delay\0"
	L"G:2,root,Images,Image01,Image02\0"
	L"\0"
END

CPrimImageToggle2_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Values,ValA,ValB,Default\0"
	L"G:1,root,Segment A,Press1,Release1\0"
	L"G:1,root,Segment B,Press2,Release2\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- 3-State Toggle
//

CPrimImageToggle3_Schema RCDATA
BEGIN
	L"Mode,Actions,,Enum/DropDown,Automatic|Automatic (A)-B-C|Automatic A-B-(C)|Automatic (A)-B-(C)|User-Defined,"
	L"Select the operation mode required for this switch."
	L"\0"

	L"Image01,A,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image02,B,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"Image03,C,,PrimImage/PrimImage,,"
	L""
	L"\0"

	L"ValA,Value A,,Integer/EditBox,|0||0|999999,"
	L"Enter the value of the Control expression that will correspond to State A."
	L"\0"

	L"ValB,Value B,,Integer/EditBox,|0||0|999999,"
	L"Enter the value of the Control expression that will correspond to State B."
	L"\0"

	L"ValC,Value C,,Integer/EditBox,|0||0|999999,"
	L"Enter the value of the Control expression that will correspond to State C."
	L"\0"

	L"Default,Default,,Enum/DropDown,State A|State B|State C,"
	L"Enter the state to display if none of Value A, Value B or Value C are matched."
	L"\0"

	L"\0"
END

CPrimImageToggle3_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,Mode,Value,Protect,Enable,Local,Axis,Keep,Delay\0"
	L"G:3,root,Images,Image01,Image02,Image03\0"
	L"\0"
END

CPrimImageToggle3_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Values,ValA,ValB,ValC,Default\0"
	L"G:1,root,Segment A,Press1,Release1\0"
	L"G:1,root,Segment C,Press2,Release2\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Rich Primitive
//

CPrimRich_Schema RCDATA
BEGIN
	L"Value,Value,,Coded/Expression,,"
	L"Enter the data value on which this primitive is to be based."
	L"\0"

	L"Entry,Operation,,Enum/DropDown,Display Only|Data Entry,"
	L"Indicate whether data entry mode should be enabled."
	L"\0"

	L"Flash,Flash On Alarm,,Enum/DropDown,No|Yes,"
	L"Indicate whether the field should flash if "
	L"the controlling data is in an alarm state."
	L"\0"

	L"Content,Contents,,Enum/DropDown,Data Value|Label and Data|Label Text|Nothing,"
	L"Indicate whether the field should display the data value, "
	L"the data value and its label, just the label or nothing at all."
	L"\0"

	L"Font,Text Font,,Font/DropPick,,"
	L"Select the font to be used to display the information."
	L"\0"

	L"TagLabel,Label,,Enum/Check,No|Yes,"
	L"Check this box to get the label from the controlling tag."
	L"\0"

	L"TagLimits,Limits,,Enum/Check,No|Yes,"
	L"Check this box to get the data limits from the controlling tag."
	L"\0"

	L"TagFormat,Format,,Enum/Check,No|Yes,"
	L"Check this box to get the data format from the controlling tag."
	L"\0"

	L"TagColor,Colors,,Enum/Check,No|Yes,"
	L"Check this box to get the data coloring from the controlling tag."
	L"\0"

	L"Enable,Enable,,Coded/Expression,|true,"
	L"Define an expression to enable or disable data entry."
	L"\0"

	L"OnSetFocus,On Selected,,Coded/Action,|None,"
	L"Define an action to be executed when the primitive is selected."
	L"\0"

	L"OnKillFocus,On Deselected,,Coded/Action,|None,"
	L"Define an action to be executed when the primitive is deselected."
	L"\0"

	L"OnComplete,On Entry Complete,,Coded/Action,|None,"
	L"Define an action to be executed when data entry is completed."
	L"\0"

	L"Label,Label,,IntlString/IntlString,24,"
	L"Define the label used for this primitive."
	L"\0"

	L"LimitMin,Minimum Value,,Coded/Expression,|None,"
	L"Define the minimum limit for the data value."
	L"\0"

	L"LimitMax,Maximum Value,,Coded/Expression,|None,"
	L"Define the maximum limit for the data value."
	L"\0"

	L"FormType,Format Type,,FormatType/Pick,,"
	L"Select the format used to display the data value."
	L"\0"

	L"ColType,Color Type,,ColorType/Pick,,"
	L"Select the coloring to used to display the data value."
	L"\0"

	L"UseBack,Background,,Enum/DropDown,Transparent|Opaque,"
	L"Indicate whether the selected coloring's background color "
	L"should be used, or whether the text should be transparent."
	L"\0"

	L"TextColor,Text Color,,Color/Color,"
	L"Select the color of the text."
	L"\0"

	L"TextShadow,Drop Shadow,,Color/Color,n,"
	L"Select the color of any drop shadow to be drawn."
	L"\0"

	L"\0"
END

CPrimRich_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Source,Value\0"
	L"G:1,root,Field Type,Entry\0"
	L"G:4,root,Get From Tag,TagLimits,TagLabel,TagFormat,TagColor\0"
	L"\0"
END

CPrimRich_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Control,Enable\0"
	L"G:1,root,Actions,OnSetFocus,OnKillFocus,OnComplete\0"
	L"\0"
END

CPrimRich_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Limits,LimitMin,LimitMax\0"
	L"\0"
END

CPrimRich_Page5 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Label,Label\0"
	L"\0"
END

CPrimRich_Page6 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Format Type,FormType\0"
	L"\0"
END

CPrimRich_Page7 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Color Type,ColType,UseBack\0"
	L"\0"
END

CPrimRich_Page8 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Color Type,ColType\0"
	L"\0"
END

CPrimRich_Page9 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Color Usage,UseBack\0"
	L"\0"
END

CPrimRich_Page10 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Text Colors,TextColor,TextShadow\0"
	L"\0"
END

CPrimRich_Page11 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Vertical Scale
//

CPrimVertScale_Schema RCDATA
BEGIN
	L"Precise,Limit Values,,Enum/DropDown,Rounded|Precise,"
	L"Define how the limit values will be chosen. Precise mode will "
	L"use the exact values entered, while Rounded mode will allow "
	L"the primitive to choose values that better fit the tick mark "
	L"spacing."
	L"\0"

	L"UseAll,Limit Positions,,Enum/DropDown,Aligned|Shifted,"
	L"Define how the scale values are to be drawn. Aligned mode will "
	L"keep the values aligned with the tick marks, resulting in not "
	L"all of the primitive's vertical extent being allocate to the "
	L"scale itself. Shifted mode will move the outer two labels "
	L"inwards to allow the scale to fill the whole of the primitive."
	L"\0"

	L"ShowLabels,Show Labels,,Enum/DropDown,Do Not Show|Show Limits|Show All,"
	L"Select whether value labels should be shown next to the scale."
	L"\0"

	L"ShowUnits,Show Units,,Enum/DropDown,Do Not Show|Show With Labels|Show Beside Scale,"
	L"Select whether units should be shown, and where they should be positioned."
	L"\0"

	L"Orient,Orientation,,Enum/DropDown,Scale Values Face Right|Scale Values Face Left,"
	L"Select whether the scale values should appear facing left or facing right."
	L"\0"

	L"LineCol,Line Color,,Color/Color,,"
	L"Select the color of the scale itself."
	L"\0"

	L"TextCol,Label Color,,Color/Color,,"
	L"Select the color of the scale labels."
	L"\0"

	L"UnitCol,Units Color,,Color/Color,,"
	L"Select the color of the units text."
	L"\0"

	L"TextFont,Label Font,,Font/DropPick,,"
	L"Select the font for the scale labels."
	L"\0"

	L"UnitFont,Units Font,,Font/DropPick,,"
	L"Select the font for the units text."
	L"\0"

	L"FontBase,Label Shift,,Integer/EditBox,|0||0|32,"
	L"Enter a value to shift the scale labels to allow "
	L"them to better align with the tick marks."
	L"\0"

	L"\0"
END

CPrimVertScale_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Config Source,Value\0"
	L"G:4,root,Get From Tag,TagLimits,TagFormat\0"
	L"G:1,root,Scale Options,ShowLabels,ShowUnits,Orient,Precise,UseAll\0"
	L"\0"
END

CPrimVertScale_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,LineCol,TextCol,UnitCol\0"
	L"G:1,root,Fonts,TextFont,UnitFont\0"
	L"G:1,root,Adjustment,FontBase\0"
	L"\0"
END

// End of File
