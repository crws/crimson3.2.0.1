
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPolygon3_HPP
	
#define	INCLUDE_PrimRubyPolygon3_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPolygon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 3-Sided Polygon Primitive
//

class CPrimRubyPolygon3 : public CPrimRubyPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPolygon3(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
