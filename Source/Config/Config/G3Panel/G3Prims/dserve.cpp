
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

// Runtime Class

AfxImplementRuntimeClass(CUIDataServer, CDataServer);

// Constructor

CUIDataServer::CUIDataServer(CUISystem *pSystem) : CDataServer(pSystem)
{
	m_pSystem = pSystem;

	m_pUI     = m_pSystem->m_pUI;

	m_pPages  = m_pUI->m_pPages;
	}

// IDataServer Methods

DWORD CUIDataServer::GetData(DWORD ID, UINT Type, UINT Flags)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.x.m_IsTag ) {

		if( !Ref.b.m_Block ) {

			switch( Ref.t.m_Index ) {

				case 0x7C00: return 100;
				case 0x7C01: return 100;
				case 0x7C03: return 10;
				case 0x7C04: return 0;
				case 0x7C06: return 0;
				case 0x7C0B: return 0;
				case 0x7C0E: return 100;
				}
			}
		}

	return CDataServer::GetData(ID, Type, Flags);
	}

DWORD CUIDataServer::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.t.m_IsTag ) {

		CItem *pItem = m_pSystem->GetDatabase()->GetHandleItem(Ref.t.m_Index);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

				CDispPage *pPage = (CDispPage *) pItem;

				return pPage->GetProp(Prop, Type);
				}
			}
		}

	return CDataServer::GetProp(ID, Prop, Type);
	}

DWORD CUIDataServer::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	if( ID == 0x20BD ) {

		return ColFlash( pParam[0],
				 pParam[1],
				 pParam[2]
				 );
		}

	if( ID == 0x20BE ) {

		return ColPick2( pParam[0],
				 pParam[1],
				 pParam[2]
				 );
		}

	if( ID == 0x20BF ) {

		return ColBlend( I2R(pParam[0]),
				 I2R(pParam[1]),
				 I2R(pParam[2]),
				 pParam[3],
				 pParam[4]
				 );
		}

	if( ID == 0x20D5 ) {

		return ColPick4( pParam[0],
				 pParam[1],
				 pParam[2],
				 pParam[3],
				 pParam[4],
				 pParam[5]
				 );
		}

	if( ID == 0x20D6 ) {

		return ColSelFlash( pParam[0],
				    pParam[1],
				    pParam[2],
				    pParam[3],
				    pParam[4]
				    );
		}

	if( ID == 0x20DA ) {

		return m_pSystem->m_pLang->GetCurrentSlot();
		}

	if( ID == 0x20E6 ) {

		return DWORD(wstrdup(L""));
		}

	if( ID == 0x20E7 ) {

		return DWORD(wstrdup(L""));
		}

	return CDataServer::RunFunc(ID, uParam, pParam);
	}

// Implementation

C3INT CUIDataServer::ColFlash(C3INT n, C3INT c1, C3INT c2)
{
	return c1;
	}

C3INT CUIDataServer::ColPick2(C3INT n, C3INT c1, C3INT c2)
{
	return n ? c1 : c2;
	}

C3INT CUIDataServer::ColBlend(C3REAL d, C3REAL f, C3REAL t, C3INT c1, C3INT c2)
{
	if( t - f ) {

		MakeMin(d, t);

		MakeMax(d, f);

		int c  = 1000;

		int p  = int(c * (d - f) / (t - f));

		int ra = GetRED  (c1);
		int ga = GetGREEN(c1);
		int ba = GetBLUE (c1);

		int rb = GetRED  (c2);
		int gb = GetGREEN(c2);
		int bb = GetBLUE (c2);

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return GetRGB(rc, gc, bc);
		}

	return c1;
	}

C3INT CUIDataServer::ColPick4(C3INT n1, C3INT n2, C3INT c1, C3INT c2, C3INT c3, C3INT c4)
{
	return n2 ? (n1 ? c1 : c2) : (n1 ? c3 : c4);
	}

C3INT CUIDataServer::ColSelFlash(C3INT n1, C3INT n2, C3INT c1, C3INT c2, C3INT c3)
{
	return n1 ? c2 : c1;
	}

// End of File
