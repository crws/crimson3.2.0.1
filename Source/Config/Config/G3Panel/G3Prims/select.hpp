
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SELECT_HPP
	
#define	INCLUDE_SELECT_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacySelector;
class CPrimLegacySelectorTwo;
class CPrimLegacySelectorMulti;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Selector Switch
//

class CPrimLegacySelector : public CPrimRich
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacySelector(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimPen   * m_pEdge;
		CPrimBrush * m_pPanel;
		CPrimBrush * m_pKnob;
		CPrimColor * m_pBack;
		UINT         m_ShowStates;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Field Requirements
		UINT GetNeedMask(void) const;

		// Implementation
		void DrawKnob(IGDI *pGDI, R2  Rect, int nPos, UINT uMode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Two-State Selector Switch
//

class CPrimLegacySelectorTwo : public CPrimLegacySelector
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacySelectorTwo(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Multi Selector Switch
//

class CPrimLegacySelectorMulti : public CPrimLegacySelector
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacySelectorMulti(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		UINT GetCount(void);
	};

// End of File

#endif
