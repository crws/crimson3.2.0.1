
#include "intern.hpp"

#include "pdf.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- PDF Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPDFViewer, CPrimViewer);

// Constructor

CPrimPDFViewer::CPrimPDFViewer(void)
{
	m_uType       = 0x33;
	m_FontTitle   = fontHei16;
	m_pFile       = NULL;
	m_pTxtCannot  = NULL;
	m_pTxtWorking = NULL;
	m_pBtnLeft    = NULL;
	m_pBtnRight   = NULL;
	m_pBtnPrev    = NULL;
	m_pBtnNext    = NULL;
	m_pBtnZoomIn  = NULL;
	m_pBtnZoomOut = NULL;
	}

// Initial Values

void CPrimPDFViewer::SetInitValues(void)
{
	CPrimViewer::SetInitValues();

	SetInitial(L"TxtCannot", m_pTxtCannot,   L"\"Cannot Load File\"");
	SetInitial(L"TxtCannot", m_pTxtWorking,  L"\"Working\"");

	SetInitial(L"BtnLeft",    m_pBtnLeft,    L"\"<<\"");
	SetInitial(L"BtnRight",   m_pBtnRight,   L"\">>\"");
	SetInitial(L"BtnPrev",    m_pBtnPrev,    L"\"Up\"");
	SetInitial(L"BtnNext",    m_pBtnNext,    L"\"Down\"");
	SetInitial(L"BtnZoomIn",  m_pBtnZoomIn,  L"\"In\"");
	SetInitial(L"BtnZoomOut", m_pBtnZoomOut, L"\"Out\"");
	}

// Overridables

void CPrimPDFViewer::SetInitState(void)
{
	CPrim::SetInitState();

	MakeList();

	SetInitSize(320, 240);
	}

void CPrimPDFViewer::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_FontTitle);

	CPrimViewer::GetRefs(Refs);
	}

void CPrimPDFViewer::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_FontTitle, uOld, uNew);

	CPrimViewer::EditRef(uOld, uNew);
	}

// Type Access

BOOL CPrimPDFViewer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"File" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.Left(3) == L"Txt" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Download Support

BOOL CPrimPDFViewer::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	UINT uFrom = Init.GetCount();
	
	Init.AddWord(0);	

	Init.AddItem(itemVirtual, m_pFile);
	
	Init.AddItem(itemVirtual, m_pTxtCannot);
	Init.AddItem(itemVirtual, m_pTxtWorking);
	
	Init.AddItem(itemVirtual, m_pBtnLeft);
	Init.AddItem(itemVirtual, m_pBtnRight);
	Init.AddItem(itemVirtual, m_pBtnPrev);
	Init.AddItem(itemVirtual, m_pBtnNext);
	Init.AddItem(itemVirtual, m_pBtnZoomIn);
	Init.AddItem(itemVirtual, m_pBtnZoomOut);

	UINT uSize = Init.GetCount() - uFrom;

	uSize -= sizeof(WORD);
	
	Init.SetWord(uFrom, WORD(uSize));
	
	return TRUE;
	}

// Meta Data

void CPrimPDFViewer::AddMetaData(void)
{
	CPrimViewer::AddMetaData();
	
	Meta_AddInteger(FontTitle);
	Meta_AddVirtual(File);
	Meta_AddVirtual(TxtCannot);
	Meta_AddVirtual(TxtWorking);
	Meta_AddVirtual(BtnLeft);
	Meta_AddVirtual(BtnRight);
	Meta_AddVirtual(BtnPrev);
	Meta_AddVirtual(BtnNext);
	Meta_AddVirtual(BtnZoomIn);
	Meta_AddVirtual(BtnZoomOut);
	
	Meta_SetName(IDS_PDF_VIEWER);
	}

// Overridables

BOOL CPrimPDFViewer::MakeList(void)
{
	m_List.Empty();

	m_List.Append(m_pBtnLeft   ->GetText());

	m_List.Append(m_pBtnRight  ->GetText());

	m_List.Append(m_pBtnPrev   ->GetText());

	m_List.Append(m_pBtnNext   ->GetText());

	m_List.Append(m_pBtnZoomIn ->GetText());

	m_List.Append(m_pBtnZoomOut->GetText());

	m_Work = "PDF VIEWER";

	return TRUE;
	}

BOOL CPrimPDFViewer::IsSupported(void)
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
	}

// Attributes

int CPrimPDFViewer::GetTextWidth(PCTXT pText) const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = pFonts->GetWidth(m_FontMenu, pText);

	nSize += m_Margin.left;

	nSize += m_Margin.right;

	return nSize;
	}

// End of File
