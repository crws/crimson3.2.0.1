
#include "intern.hpp"

#include "PrimRubyGradButton.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graduated Button Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGradButton, CPrimRubyWithText);

// Constructor

CPrimRubyGradButton::CPrimRubyGradButton(void)
{
	m_uType    = 0x86;

	m_pColor1  = New CPrimColor;

	m_pColor2  = New CPrimColor;

	m_pEdge    = New CPrimRubyPenEdge;

	m_Corner   = CPoint(8, 8);

	m_uActMode = actAlways;
	}

// UI Overridables

BOOL CPrimRubyGradButton::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       2
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// Overridables

BOOL CPrimRubyGradButton::HitTest(P2 Pos)
{
	if( CPrimRubyWithText::HitTest(Pos) ) {

		return TRUE;
		}

	if( PtInRect(m_bound, Pos) ) {

		if( m_pathFill.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathEdge.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathTrim.HitTest(Pos, 0) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

void CPrimRubyGradButton::Draw(IGDI *pGDI, UINT uMode)
{
	CRubyGdiLink    link(pGDI);

	CUISystem       *pSys = (CUISystem *) m_pDbase->GetSystemItem();

	CRubyPatternLib *pLib = pSys->m_pUI->m_pPatterns;

	PSHADER pShader = pLib->SetGdi( pGDI,
					17,
					IsPressed() ? m_pColor2->GetColor() : m_pColor1->GetColor(),
					IsPressed() ? m_pColor1->GetColor() : m_pColor2->GetColor()
					);

	if( !UseFastFill() ) {

		link.OutputShade(m_listFill, pShader, 0);
		}
	else
		link.OutputShade(m_listFill, pShader);

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	m_pEdge->Trim(pGDI, m_listTrim, TRUE);

	CPrimRubyWithText::Draw(pGDI, uMode);
	}

BOOL CPrimRubyGradButton::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		Hand.m_Pos  = m_Corner;

		Hand.m_pTag = L"Corner";

		Hand.m_uRef = 0;

		Hand.m_Clip = CRect( 0,
				     0,
				     cx / 2 - 2,
				     cy / 2 - 2
				     );

		return TRUE;
		}

	return FALSE;
	}

void CPrimRubyGradButton::GetRefs(CPrimRefList &Refs)
{
	CPrimRubyWithText::GetRefs(Refs);

	m_pEdge->GetRefs(Refs);
	}

void CPrimRubyGradButton::SetInitState(void)
{
	CPrimRubyWithText::SetInitState();

	m_pColor1->Set(GetRGB(18,18,30));

	m_pColor2->Set(GetRGB( 6, 6,16));
	
	m_pEdge->m_Join = 0;

	AddText();

	m_pTextItem->m_Lead    = 0;

	m_pTextItem->m_MoveDir = 1;

	m_pTextItem->Set(L"TEXT", TRUE);

	SetInitSize(73, 41);
	}

void CPrimRubyGradButton::FindTextRect(void)
{
	m_TextRect  = m_DrawRect;

	int nAdjust = m_pEdge->GetInnerWidth();

	DeflateRect(m_TextRect, nAdjust, nAdjust);
	}

// Download Support

BOOL CPrimRubyGradButton::MakeInitData(CInitData &Init)
{
	CPrimRubyWithText::MakeInitData(Init);

	Init.AddByte(BYTE(UseFastFill()));

	Init.AddItem(itemSimple, m_pColor1);
	Init.AddItem(itemSimple, m_pColor2);
	Init.AddItem(itemSimple, m_pEdge);

	AddList(Init, m_listFill);
	AddList(Init, m_listEdge);
	AddList(Init, m_listTrim);

	return TRUE;
	}

// Meta Data

void CPrimRubyGradButton::AddMetaData(void)
{
	CPrimRubyWithText::AddMetaData();

	Meta_AddObject(Color1);
	Meta_AddObject(Color2);
	Meta_AddObject(Edge);
	Meta_AddPoint (Corner);

	Meta_SetName((IDS_GRADUATED_BUTTON_2));
	}

// Fast Fill Control

BOOL CPrimRubyGradButton::UseFastFill(void)
{
	if( m_pEdge->m_Width ) {

		if( m_pEdge->m_Edge == CRubyStroker::edgeOuter ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Path Management

void CPrimRubyGradButton::InitPaths(void)
{
	m_pathFill.Empty();

	m_pathEdge.Empty();

	m_pathTrim.Empty();
	}

void CPrimRubyGradButton::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyPoint  p1(Rect, 1);

	CRubyPoint  p2(Rect, 4);

	CRubyVector rd(m_Corner.x, m_Corner.y);

	MakeMin(rd.m_x, (p2.m_x - p1.m_x) / 2 - 2);

	MakeMin(rd.m_y, (p2.m_y - p1.m_y) / 2 - 2);

	MakeMax(rd.m_x, 0);

	MakeMax(rd.m_y, 0);

	CRubyDraw::Trimmed(m_pathFill, p1, p2, rd, 1, 0);

	m_pEdge->StrokeEdge(m_pathEdge, m_pathFill);

	m_pEdge->StrokeTrim(m_pathTrim, m_pathEdge);

	m_pathFill.GetBoundingRect(m_bound);

	m_pathEdge.AddBoundingRect(m_bound);

	m_pathTrim.AddBoundingRect(m_bound);
	}

void CPrimRubyGradButton::MakeLists(void)
{
	m_listFill.Load(m_pathFill, !UseFastFill());

	m_listEdge.Load(m_pathEdge, true);

	m_listTrim.Load(m_pathTrim, true);
	}

// End of File
