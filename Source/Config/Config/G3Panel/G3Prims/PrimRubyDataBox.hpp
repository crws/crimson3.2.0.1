
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyDataBox_HPP
	
#define	INCLUDE_PrimRubyDataBox_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyRect.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Data Box Primitive
//

class CPrimRubyDataBox : public CPrimRubyRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyDataBox(void);

		// Operations
		void Set(CString Text, CSize MaxSize);

		// Overridables
		void  SetInitState(void);
		void  FindTextRect(void);
		CSize GetMinSize(IGDI *pGDI);
	
	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
