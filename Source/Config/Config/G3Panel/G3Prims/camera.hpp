
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CAMERA_HPP
	
#define	INCLUDE_CAMERA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Camera
//

class CPrimCamera : public CPrimGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimCamera(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void  Draw(IGDI *pGDI, UINT uMode);
		void  SetInitState(void);
		CSize GetMinSize(IGDI *pGDI);
		BOOL  IsSupported(void);

		// Configuration
		UINT m_Device;
		UINT m_Scale;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT m_uPort;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void    DrawDevice(IGDI *pGDI);
		void    DrawNotSupported(IGDI *pGDI);		
		CString FindDeviceName(void);
		UINT    FindDevicePort(void);
	};

// End of File

#endif
