
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text Reflow Object
//

// Constructor

CTextFlow::CTextFlow(void)
{
	m_Rect.x1 = -1;

	ClearBoundingRect();
	}

// Attributes

void CTextFlow::GetBoundingRect(R2 &Rect) const
{
	Rect = m_Bound;
	}

void CTextFlow::AddBoundingRect(R2 &Rect) const
{
	MakeMin(Rect.x1, m_Bound.x1);
	MakeMin(Rect.y1, m_Bound.y1);
	MakeMax(Rect.x2, m_Bound.x2);
	MakeMax(Rect.y2, m_Bound.y2);
	}

// Operations

void CTextFlow::Dirty(void)
{
	m_Rect.x1 = -1;
	}

BOOL CTextFlow::Flow(IGDI *pGDI, CUnicode const &Text, R2 const &Rect)
{
	BOOL fFlow = FALSE;

	if( memcmp(&Rect, &m_Rect, sizeof(R2)) ) { 

		if( m_Size.cx != Rect.x2 - Rect.x1 ) {

			fFlow = TRUE;
			}

		m_Rect    = Rect;

		m_Size.cx = Rect.x2 - Rect.x1;

		m_Size.cy = Rect.y2 - Rect.y1;
		}

	if( m_Text.CompareC(Text) ) {

		m_Text = Text;

		fFlow  = TRUE;
		}

	if( fFlow ) {

		m_Lines.Empty();

		int   xWide = m_Rect.x2 - m_Rect.x1;

		PCUTF pText = Text;

		int   nPos  = 0;

		while( pText[nPos] ) {

			CRange Range;

			Range.m_nFrom = nPos;

			int xPos = 0;

			for(;;) {

				WCHAR cData = pText[nPos];

				if( cData ) {

					if( cData == '|' ) {

						Range.m_nTo = nPos++;

						break;
						}
					else {
						WCHAR t[] = { cData, 0 };

						int xSize = pGDI->GetTextWidth(t);

						if( xPos + xSize > xWide ) {

							Range.m_nTo = nPos;

							if( cData == 0x20 ) {

								Range.m_nTo = nPos;

								nPos++;
								}
							else {
								for( int s = nPos; s > Range.m_nFrom; s-- ) {

									if( IsBreak(pText[s]) ) {

										nPos = s + 1;

										break;
										}
									}

								Range.m_nTo = nPos;
								}

							break;
							}

						xPos += xSize;

						nPos += 1;

						continue;
						}
					}

				Range.m_nTo = nPos;

				break;
				}

			if( nPos == Range.m_nFrom ) {

				break;
				}

			if( !(pText[nPos] == 0x00 || pText[nPos] == 0x20) ) {

				while( Range.m_nTo > Range.m_nFrom ) {

					if( pText[Range.m_nTo - 1] == 0x20 ) {

						Range.m_nTo--;

						continue;
						}

					break;
					}
				}

			m_Lines.Append(Range);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTextFlow::Draw(IGDI *pGDI, CFormat const &Fmt)
{
	ClearBoundingRect();

	pGDI->SetTextTrans(modeTransparent);

	PCUTF pText  = m_Text;

	UINT  uCount = m_Lines.GetCount();

	BOOL  fMoveX = Fmt.m_fMove && Fmt.m_MoveDir > 1;

	BOOL  fMoveY = Fmt.m_fMove && Fmt.m_MoveDir > 0;

	int   xOrg   = m_Rect.x1;

	int   yOrg   = m_Rect.y1;

	int   nHalf  = Fmt.m_Lead / 2;

	int   yLine  = Fmt.m_Lead + pGDI->GetTextHeight(L"X");
		
	int   ySize  = uCount * yLine;

	int   yPos   = Fmt.m_AlignV < 5 ? nHalf + max(0, (Fmt.m_AlignV * (m_Size.cy - ySize)) / 4) : nHalf;

	if( fMoveX ) {

		xOrg += Fmt.m_MoveStep;
		}

	if( fMoveY ) {

		yOrg += Fmt.m_MoveStep;
		}

	for( UINT n = 0; n < uCount; n++ ) {

		if( yPos + yLine - nHalf <= m_Size.cy ) {

			MakeMin(m_Bound.y1, yOrg + yPos);

			CRange const &Range = m_Lines[n];

			if( Range.m_nTo > Range.m_nFrom ) {

				UINT     uLen  = Range.m_nTo - Range.m_nFrom;

				PCUTF    pLine = pText + Range.m_nFrom;

				CUnicode Line  = CUnicode(pLine, uLen);

				int      xSize = pGDI->GetTextWidth(Line);

				int      xPos  = (Fmt.m_AlignH * (m_Size.cx - xSize)) / 2;

				if( !(Fmt.m_Shadow & 0x8000) ) {

					pGDI->SetTextFore(Fmt.m_Shadow);

					pGDI->TextOut(xOrg + xPos + 1, yOrg + yPos + 1, Line);
					}

				pGDI->SetTextFore(Fmt.m_Color);

				pGDI->TextOut(xOrg + xPos, yOrg + yPos, Line);

				MakeMin(m_Bound.x1, xOrg + xPos);
				
				MakeMax(m_Bound.x2, xOrg + xPos + xSize);
				}

			if( Fmt.m_AlignV == 7 && n == uCount - 2 ) {

				yPos = m_Size.cy - yLine; 
				}
			else 
				yPos += yLine;

			MakeMax(m_Bound.y2, yOrg + yPos);

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

void CTextFlow::SetBoundingRect(int x1, int y1, int cx, int cy)
{
	m_Bound.x1 = x1;

	m_Bound.y1 = y1;

	m_Bound.x2 = x1 + cx;

	m_Bound.y2 = y1 + cy;
	}

// Implementation

BOOL CTextFlow::IsBreak(WCHAR cData)
{
	switch( cData ) {

		case 0x0020:
		case 0x3001:
		case 0x3002:
		case 0xFF0C:
		case 0xFF0E:
		case 0xFF1A:
		case 0xFF1B:

			return TRUE;
		}

	return FALSE;
	}

void CTextFlow::ClearBoundingRect(void)
{
	m_Bound.x1 = +1000000;
	m_Bound.y1 = +1000000;
	m_Bound.x2 = -1000000;
	m_Bound.y2 = -1000000;
	}

// End of File
