
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyTankFill_HPP
	
#define	INCLUDE_RubyTankFill_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBrush.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Tank Fill
//

class DLLAPI CPrimRubyTankFill : public CPrimRubyBrush
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyTankFill(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		void Set(COLOR Color);
		BOOL StepAddress(void);

		// Drawing
		BOOL Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Mode;
		CCodedItem * m_pValue;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimColor * m_pColor3;

	protected:
		// Data Members
		BOOL m_fAutoLimits;

		// Static Data
		static UINT   m_ShadeMode;
		static COLOR  m_ShadeCol3;
		static C3REAL m_ShadeData;

		// Shader
		static BOOL Shader(IGDI *pGDI, int p, int c);

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void   DoEnables(IUIHost *pHost);
		C3REAL GetValue(CCodedItem *pItem, C3REAL Default);
		BOOL   HasAutoLimits(void);
		BOOL   PrepTankFill(void);
	};

// End of File

#endif
