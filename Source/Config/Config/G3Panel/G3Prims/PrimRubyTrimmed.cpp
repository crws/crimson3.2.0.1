
#include "intern.hpp"

#include "PrimRubyTrimmed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Trimmed Rectangle Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyTrimmed, CPrimRubyGeom);

// Constructor

CPrimRubyTrimmed::CPrimRubyTrimmed(void)
{
	m_style  = 0;

	m_Skip   = 0;

	m_Corner = CPoint(16, 16);
	}

// Overridables

void CPrimRubyTrimmed::FindTextRect(void)
{
	// I don't like the way this works!!!

	int xStep = 3 * m_Corner.cx / 4;

	int yStep = 3 * m_Corner.cy / 4;

	int nLeft = m_TextRect.y2 - m_TextRect.y1 - 2 * yStep;

	int nTrip = GetTripSize();

	if( nLeft <= nTrip ) {

		xStep = m_Corner.cx;

		yStep = 2;
		}

	m_TextRect = m_DrawRect;

	DeflateRect(m_TextRect, xStep, yStep);
	}

BOOL CPrimRubyTrimmed::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		Hand.m_Pos  = m_Corner;

		Hand.m_pTag = L"Corner";

		Hand.m_uRef = 0;

		Hand.m_Clip = CRect( 0,
				     0,
				     cx / 2 - 2,
				     cy / 2 - 2
				     );

		return TRUE;
		}

	return FALSE;
	}

void CPrimRubyTrimmed::SetHand(BOOL fInit)
{
	if( fInit ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		m_Corner.cx = cx / 4;

		m_Corner.cy = cy / 4;
		}

	CPrimRubyGeom::SetHand(fInit);
	}

// Meta Data

void CPrimRubyTrimmed::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_AddPoint  (Corner);
	Meta_AddInteger(Skip);

	Meta_SetName((IDS_TRIMMED_RECTANGLE_2));
	}

// Path Management

void CPrimRubyTrimmed::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyPoint  p1(Rect, 1);

	CRubyPoint  p2(Rect, 4);

	CRubyVector r(m_Corner.cx, m_Corner.cy);

	MakeMin(r.m_x, (p2.m_x - p1.m_x) / 2 - 2);

	MakeMin(r.m_y, (p2.m_y - p1.m_y) / 2 - 2);

	MakeMax(r.m_x, 0);

	MakeMax(r.m_y, 0);

	CRubyDraw::Trimmed(m_pathFill, p1, p2, r, m_style, m_Skip);

	CPrimRubyGeom::MakePaths();
	}

// End of File
