
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyFilleted_HPP
	
#define	INCLUDE_PrimRubyFilleted_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyTrimmed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Filleted Rectangle Primitive
//

class CPrimRubyFilleted : public CPrimRubyTrimmed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyFilleted(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
