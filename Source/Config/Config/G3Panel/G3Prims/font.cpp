
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Font
//

class DLLAPI CUITextFont : public CUITextEnum
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUITextFont(void);

protected:
	// Data Members
	CFontManager * m_pFonts;

	// Overridables
	void OnBind(void);
	BOOL OnEnumValues(CStringArray &List);
	BOOL OnExpand(CWnd &Wnd);

	// Implementation
	void FindManager(void);
	UINT LoadFonts(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Font
//

// Dynamic Class

AfxImplementDynamicClass(CUITextFont, CUITextEnum);

// Constructor

CUITextFont::CUITextFont(void)
{
	m_uFlags |= textExpand | textRefresh;
}

// Overridables

void CUITextFont::OnBind(void)
{
	CUITextEnum::OnBind();

	FindManager();

	LoadFonts();
}

BOOL CUITextFont::OnEnumValues(CStringArray &List)
{
	m_Enum.Empty();

	LoadFonts();

	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		List.Append(m_Enum[n].m_Text);
	}

	return TRUE;
}

BOOL CUITextFont::OnExpand(CWnd &Wnd)
{
	// LATER -- Preload the font dialog with the current selection.

	CFontDialog Dlg;

	Dlg.SetMaxSize(300);

	Dlg.TrueTypeOnly();

	if( Dlg.Execute(Wnd) ) {

		afxThread->SetWaitMode(TRUE);

		UINT uIndex = m_pFonts->Create(Dlg);

		afxThread->SetWaitMode(FALSE);

		m_pData->WriteInteger(m_pItem, uIndex);

		m_pItem->SetDirty();

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CUITextFont::FindManager(void)
{
	CUISystem *pSystem = (CUISystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pFonts = pSystem->m_pUI->m_pFonts;
}

UINT CUITextFont::LoadFonts(void)
{
	CArray <UINT> List;

	m_pFonts->EnumSystem(List);

	m_pFonts->EnumCustom(List);

	UINT c = List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT    uFont = List[n];

		CString Name  = m_pFonts->GetName(uFont);

		AddData(uFont, Name);
	}

	return c;
}

// End of File
