
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Single Image
//

// Dynamic Class

AfxImplementDynamicClass(CPrimSimpleImage, CPrimWithText);

// Constructor

CPrimSimpleImage::CPrimSimpleImage(void)
{
	m_uType  = 0x0B;

	m_Keep   = 0;

	m_pColor = NULL;

	m_pImage = NULL;
	}

// Destructor

CPrimSimpleImage::~CPrimSimpleImage(void)
{
	}

// UI Overridables

BOOL CPrimSimpleImage::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_IMAGE),
				       AfxPointerClass(this),
				       1
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// UI Update

void CPrimSimpleImage::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == L"Keep" ) {

		if( m_pImage ) {

			m_pImage->m_Opts.m_Keep = m_Keep;

			pHost->UpdateUI(L"Image");
			}
		}

	CPrimWithText::OnUIChange(pHost, pItem, Tag);
	}

// Operations

void CPrimSimpleImage::LoadFromDataObject(IDataObject *pData, CSize MaxSize)
{
	if( !m_pImage ) {

		m_pImage = New CPrimImage;

		m_pImage->SetParent(this);

		m_pImage->Init();
		}

	SetRect(m_pImage->LoadFromDataObject(pData, MaxSize));
	}

// Overridables

void CPrimSimpleImage::Draw(IGDI *pGDI, UINT uMode)
{
	if( m_pImage ) {

		UINT rop  = 0;
		
		if( m_pColor ) {
			
			if( !m_pColor->ExecVal() ) {

				rop = ropDisable;
				}
			}

		m_pImage->Draw(pGDI, m_DrawRect, uMode, rop);
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCTXT pt = L"IMAGE";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimWithText::Draw(pGDI, uMode);
	}

void CPrimSimpleImage::GetRefs(CPrimRefList &Refs)
{
	if( m_pImage ) {

		m_pImage->GetRefs(Refs);
		}

	CPrimWithText::GetRefs(Refs);
	}

void CPrimSimpleImage::EditRef(UINT uOld, UINT uNew)
{
	if( m_pImage ) {

		m_pImage->EditRef(uOld, uNew);
		}

	CPrimWithText::EditRef(uOld, uNew);
	}

// Download Support

BOOL CPrimSimpleImage::MakeInitData(CInitData &Init)
{
	CPrimWithText::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pColor);

	Init.AddItem(itemVirtual, m_pImage);

	return TRUE;
	}

// Meta Data Creation

void CPrimSimpleImage::AddMetaData(void)
{
	CPrimWithText::AddMetaData();

	Meta_AddVirtual(Color);

	Meta_AddInteger(Keep);

	Meta_AddVirtual(Image);

	Meta_SetName((IDS_SIMPLE_IMAGE));
	}

// End of File
