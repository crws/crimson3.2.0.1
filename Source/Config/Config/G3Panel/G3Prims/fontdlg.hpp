
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FONTDLG_HPP
	
#define	INCLUDE_FONTDLG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Integer Comparison
//

#define	intcmp(a,b)	(((a)==(b))?0:(((a)<(b))?-1:+1))

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CFontManagerDialog;
class CFontSelectItem;

//////////////////////////////////////////////////////////////////////////
//
// Font Management Dialog
//

class CFontManagerDialog : public CStdToolbarDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CFontManagerDialog(CFontManager *pFonts);

		// Destructor
		~CFontManagerDialog(void);

	public:
		// Data Members
		CFontManager * m_pFonts;
		CUISystem    * m_pSystem;
		CFontList    * m_pList;
		CListView    * m_pView;
		UINT	       m_uItem;
		BOOL	       m_fRead;
		BOOL	       m_fSystem;
		UINT	       m_uCount;
		UINT	       m_uUsed;
		UINT           m_uSort;
		BOOL	       m_fFlip;
		BOOL	       m_fWarned;
		CRect	       m_Preview;
		IGdiWindows  * m_pWin;
		IGdi	     * m_pGDI;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnUpdateUI(void);
		void OnPaint(void);

		// Command Handlers
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);
		BOOL OnCancel(UINT uID);
		BOOL OnEdit(void);
		BOOL OnDelete(void);
		BOOL OnReplace(void);
		BOOL OnPurge(void);

		// Notification Handlers
		void OnItemChanged(UINT uID, NMLISTVIEW &Info);
		void OnColumnClick(UINT uID, NMLISTVIEW &Info);
		void OnListDblClk (UINT uID, NMHDR      &Info);

		// Sorting
		int SortList(LPARAM p1, LPARAM p2);
		int SortData(UINT f1, UINT f2, PCSTR pList);
		int SortData(UINT f1, UINT f2, char cKey);

		// Sort Function
		static int __stdcall SortStatic(LPARAM p1, LPARAM p2, LPARAM ps);

		// Implementation
		void MakeRepl(void);
		void MakeList(void);
		void LoadList(void);
		void ShowList(void);
		void UpdateUsed(void);
		void DoEnables(void);
		BOOL Warn(void);
		void FindPreviewData(void);
		void ShowPreview(void);
		void FreePreviewData(void);
		void EnumFonts(CArray <UINT> &List);
		UINT GetFont(UINT uItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Font Selection Item
//

class CFontSelectItem : public CUIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CFontSelectItem(void);

		// Data Members
		UINT m_Font;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
