
#include "intern.hpp"

#include "fontdlg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Font Manager
//

// Dynamic Class

AfxImplementDynamicClass(CFontManager, CMetaItem);

// Constructor

CFontManager::CFontManager(void)
{
	m_pFonts = New CFontList;
	}

// Destructor

CFontManager::~CFontManager(void)
{
	}

// Operations

UINT CFontManager::Create(CFontDialog &Dlg)
{
	UINT uCount = m_pFonts->GetIndexCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CFontItem *pFont = m_pFonts->GetItem(n);

		if( pFont ) {
			
			if( pFont->IsSame(Dlg) ) {

				return 0x100 + n;
				}
			}
		}

	CFontItem *pFont = New CFontItem;

	m_pFonts->AppendItem(pFont);

	pFont->Create(Dlg);

	return pFont->GetFontID();
	}

UINT CFontManager::Create(CString Face, UINT uSize, BOOL fBold)
{
	UINT uCount = m_pFonts->GetIndexCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CFontItem *pFont = m_pFonts->GetItem(n);

		if( pFont ) {
			
			if( pFont->IsSame(Face, uSize, fBold) ) {

				return 0x100 + n;
				}
			}
		}

	CFontItem *pFont = New CFontItem;

	m_pFonts->AppendItem(pFont);

	pFont->Create(Face, uSize, fBold);

	return pFont->GetFontID();
	}

UINT CFontManager::LoadFromTreeFile(CTreeFile &Tree)
{
	CFontItem *pFont = New CFontItem;

	pFont->SetParent(m_pFonts);

	if( pFont->LoadFromTreeFile(Tree) ) {

		UINT n = FindMatch(pFont);

		if( n == NOTHING ) {

			if( m_pFonts->AppendItem(pFont) ) {

				return pFont->GetFontID();
				}
			}

		pFont->Kill();

		delete pFont;

		return 0x100 + n;
		}

	pFont->Kill();

	delete pFont;

	return NOTHING;
	}

BOOL CFontManager::EnumSystem(CArray <UINT> &List)
{
	static UINT Fonts[] = {
	
		fontSwiss0406,
		fontSwiss0708,
		fontSwiss0512,
		fontSwiss0712,
		fontSwiss1216,
		fontTimes0808,
		fontTimes0812,
		fontTimes1612,
		fontHei10,
		fontHei12,
		fontHei14,
		fontHei16,
		fontHei18,
		fontHei20,
		fontHei24,
		fontHei10Bold,
		fontHei12Bold,
		fontHei14Bold,
		fontHei16Bold,
		fontHei18Bold,
		fontHei20Bold,
		fontHei24Bold,
		fontBig24,
		fontBig32,
		fontBig64,
		fontBig96
		};

	CUISystem *pSystem = (CUISystem *) m_pDbase->GetSystemItem();

	for( UINT f = 0; f < elements(Fonts); f++ ) {

		UINT uFont = Fonts[f];

		if( pSystem->HasFont(uFont) ) {
			
			List.Append(uFont);
			}
		}

	return TRUE;
	}

BOOL CFontManager::EnumCustom(CArray <UINT> &List)
{
	INDEX Index = m_pFonts->GetHead();

	while( !m_pFonts->Failed(Index) ) {

		CFontItem *pFont = m_pFonts->GetItem(Index);

		List.Append(pFont->GetFontID());

		m_pFonts->GetNext(Index);
		}

	return TRUE;
	}

void CFontManager::InvokeDialog(CViewWnd &Wnd)
{
	CFontManagerDialog Dialog(this);

	Dialog.Execute(Wnd);
	}

void CFontManager::UpdateUsed(void)
{
	m_Used.Empty();

	m_pFonts->MarkAsUnused();

	CUIManager *pUI = (CUIManager *) GetParent();

	pUI->m_pPages->ScanFonts(this);
	}

void CFontManager::PurgeUnused(void)
{
	UINT uCount = m_pFonts->GetIndexCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CFontItem *pFont = m_pFonts->GetItem(n);

		if( pFont ) {

			if( !pFont->m_Used ) {

				m_pFonts->DeleteItem(pFont);
				}
			}
		}
	}

BOOL CFontManager::MarkUsed(CPrimRefList const &Refs)
{
	INDEX Index = Refs.GetHead();

	while( !Refs.Failed(Index) ) {

		UINT r = Refs[Index];

		if( r & refFont ) {

			UINT uFont = (r & ~refFont);

			MarkUsed(uFont);
			}

		Refs.GetNext(Index);
		}

	return TRUE;
	}

BOOL CFontManager::MarkUsed(UINT uFont)
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			pFont->m_Used = 1;

			return TRUE;
			}

		return FALSE;
		}

	INDEX Find = m_Used.FindName(uFont);

	if( !m_Used.Failed(Find) ) {

		m_Used.SetData(Find, TRUE);

		return TRUE;
		}

	m_Used.Insert(uFont, TRUE);

	return TRUE;
	}

BOOL CFontManager::ClearUsed(UINT uFont)
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			pFont->m_Used = 0;

			return TRUE;
			}

		return FALSE;
		}

	INDEX Find = m_Used.FindName(uFont);

	if( !m_Used.Failed(Find) ) {

		m_Used.SetData(Find, FALSE);

		return TRUE;
		}

	m_Used.Insert(uFont, FALSE);

	return TRUE;
	}

// Font Location

CFontItem * CFontManager::GetFont(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		return pFont;
		}

	return NULL;
	}

// Font Selection

BOOL CFontManager::Select(IGDI *pGDI, UINT uFont)
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			pGDI->SelectFont(pFont);

			return TRUE;
			}

		pGDI->SelectFont(fontDefault);

		return FALSE;
		}

	pGDI->SelectFont(uFont);

	return TRUE;
	}

// Font Matching

UINT CFontManager::FindMatch(CFontItem *pFont) const
{
	UINT uCount = m_pFonts->GetIndexCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CFontItem *pTest = m_pFonts->GetItem(n);

		if( pTest && pTest->IsSame(pFont) ) {

			return n;
			}
		}

	return NOTHING;
	}

// Font Attributes

BOOL CFontManager::IsCustom(UINT uFont) const
{
	return uFont >= 0x100;
	}

BOOL CFontManager::IsSystem(UINT uFont) const
{
	return !IsCustom(uFont);
	}

BOOL CFontManager::IsNumeric(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			if( pFont->m_Glyphs == glyphNumeric ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	switch( uFont ) {

		case fontBig24:     
		case fontBig32:     
		case fontBig64:     
		case fontBig96:

			return TRUE;
		}

	return FALSE;
	}

BOOL CFontManager::IsSmooth(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			return pFont->IsSmooth();
			}

		return FALSE;
		}

	return uFont >= fontBig24 && uFont <= fontBig96;
	}

BOOL CFontManager::IsComplete(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			if( (pFont->m_Glyphs & glyphComplete) == glyphComplete ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	switch( uFont ) {

		case fontHei14:
		case fontHei16:
		case fontHei18:
		case fontHei20:
		case fontHei24:
		case fontHei14Bold:
		case fontHei16Bold:
		case fontHei18Bold:
		case fontHei20Bold:
		case fontHei24Bold:

			return TRUE;
		}

	return FALSE;
	}

BOOL CFontManager::IsLegacy(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			if( pFont->m_Glyphs == glyphLegacy ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CFontManager::HasGlyphs(UINT uFont, UINT uMask) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			if( pFont->m_Glyphs & uMask ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	switch( uFont ) {

		case fontHei14:
		case fontHei16:
		case fontHei18:
		case fontHei20:
		case fontHei24:
		case fontHei14Bold:
		case fontHei16Bold:
		case fontHei18Bold:
		case fontHei20Bold:
		case fontHei24Bold:

			return TRUE;
		}

	return FALSE;
	}

BOOL CFontManager::IsUsed(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			return pFont->m_Used;
			}

		return FALSE;
		}

	INDEX Find = m_Used.FindName(uFont);

	if( !m_Used.Failed(Find) ) {

		return m_Used.GetData(Find);
		}

	return FALSE;
	}

CString CFontManager::GetOptions(UINT uFont) const
{
	CString Opts;

	if( IsSmooth(uFont) ) {

		if( !Opts.IsEmpty() ) {

			Opts += L", ";
			}

		Opts += CString(IDS_ANTIALIASED);
		}

	if( IsNumeric(uFont) ) {

		if( !Opts.IsEmpty() ) {

			Opts += L", ";
			}

		Opts += CString(IDS_NUMERIC);
		}
	else {
		if( !IsComplete(uFont) ) {

			if( IsLegacy(uFont) ) {

				if( !Opts.IsEmpty() ) {

					Opts += L", ";
					}

				Opts += CString(IDS_LEGACY_MODE);
				}
			else {
				if( HasGlyphs(uFont, glyphExtended | glyphExtAccented) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_EXTENDED);
					}

				if( HasGlyphs(uFont, glyphHiragana) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_HIRAGANA);
					}

				if( HasGlyphs(uFont, glyphFullKatakana | glyphHalfKatakana) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_KATAKANA);
					}

				if( HasGlyphs(uFont, glyphPinYin) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_PINYIN);
					}

				if( HasGlyphs(uFont, glyphJamo) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_JAMO);
					}

				if( HasGlyphs(uFont, glyphHangul) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_HANGUL);
					}

				if( HasGlyphs(uFont, glyphHebrew) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_HEBREW);
					}

				if( HasGlyphs(uFont, glyphIdeographs) ) {

					if( !Opts.IsEmpty() ) {

						Opts += L", ";
						}

					Opts += CString(IDS_CJK_IDEOGRAPHS);
					}
				}
			}
		else {
			if( !Opts.IsEmpty() ) {

				Opts += L", ";
				}

			Opts += CString(IDS_COMPLETE);
			}
		}

	if( Opts.IsEmpty() ) {

		Opts = CString(IDS_PUI_NONE);
		}

	return Opts;
	}

CString CFontManager::GetFace(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			return pFont->m_Face;
			}

		return IDS_UNKNOWN_FONT;
		}
	
	if( uFont == fontDefault ) {

		uFont = fontHei16;
		}

	switch( uFont ) {

		case fontSwiss0406:
		case fontSwiss0708:
		case fontSwiss0512:
		case fontSwiss0712:
		case fontSwiss1216:
			
			return L"Swiss";
		
		case fontTimes0808:
		case fontTimes0812:
		case fontTimes1612:
			
			return L"Times";

		case fontHei10:
		case fontHei12:
		case fontHei14:
		case fontHei16:
		case fontHei18:
		case fontHei20:
		case fontHei24:
			
			return L"Hei";

		case fontHei10Bold:
		case fontHei12Bold:
		case fontHei14Bold:
		case fontHei16Bold:
		case fontHei18Bold:
		case fontHei20Bold:
		case fontHei24Bold:
			
			return L"Hei";
		
		case fontBig24:     
		case fontBig32:     
		case fontBig64:     
		case fontBig96:
			
			return IDS_NUMERIC_ONLY;
		}

	return IDS_UNKNOWN_FONT;
	}


CString CFontManager::GetName(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			return pFont->Describe();
			}

		return IDS_UNKNOWN_FONT;
		}

	if( uFont == fontDefault ) {

		uFont = fontHei16;
		}

	switch( uFont ) {

		case fontSwiss0406: return L"Swiss 4x6"  ;
		case fontSwiss0708: return L"Swiss 7x8"  ;
		case fontSwiss0512: return L"Swiss 5x12" ;
		case fontSwiss0712: return L"Swiss 7x12" ;
		case fontSwiss1216: return L"Swiss 12x16";
		
		case fontTimes0808: return L"Times 8x8"  ;
		case fontTimes0812: return L"Times 8x12" ;
		case fontTimes1612: return L"Times 16x12";

		case fontHei10:     return IDS_HEI_REGULAR;
		case fontHei12:     return IDS_HEI_REGULAR_2;
		case fontHei14:     return IDS_HEI_REGULAR_3;
		case fontHei16:     return IDS_HEI_REGULAR_4;
		case fontHei18:     return IDS_HEI_REGULAR_5;
		case fontHei20:     return IDS_HEI_REGULAR_6;
		case fontHei24:     return IDS_HEI_REGULAR_7;

		case fontHei10Bold: return IDS_HEI_BOLD;
		case fontHei12Bold: return IDS_HEI_BOLD_2;
		case fontHei14Bold: return IDS_HEI_BOLD_3;
		case fontHei16Bold: return IDS_HEI_BOLD_4;
		case fontHei18Bold: return IDS_HEI_BOLD_5;
		case fontHei20Bold: return IDS_HEI_BOLD_6;
		case fontHei24Bold: return IDS_HEI_BOLD_7;
		
		case fontBig24:     return IDS_NUMERIC_ONLY_2;
		case fontBig32:     return IDS_NUMERIC_ONLY_3;
		case fontBig64:     return IDS_NUMERIC_ONLY_4;
		case fontBig96:     return IDS_NUMERIC_ONLY_5;
		}

	return IDS_UNKNOWN_FONT;
	}

// Font Metrics

int CFontManager::GetHeight(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			return pFont->GetGlyphHeight(0);
			}

		return 16;
		}

	switch( uFont ) {

		case fontSwiss0406: return 6;
		case fontSwiss0708: return 8;
		case fontSwiss0512: return 12;
		case fontSwiss0712: return 12;
		case fontSwiss1216: return 16;
		case fontTimes0808: return 8;
		case fontTimes0812: return 12;
		case fontTimes1612: return 12;
		case fontHei10:	    return 10;
		case fontHei12:	    return 12;
		case fontHei14:	    return 14;
		case fontHei16:	    return 16;
		case fontHei18:	    return 18;
		case fontHei20:	    return 20;
		case fontHei24:	    return 24;
		case fontHei10Bold: return 10;
		case fontHei12Bold: return 12;
		case fontHei14Bold: return 14;
		case fontHei16Bold: return 16;
		case fontHei18Bold: return 18;
		case fontHei20Bold: return 20;
		case fontHei24Bold: return 24;
		case fontBig24:	    return 24;
		case fontBig32:	    return 32;
		case fontBig64:	    return 64;
		case fontBig96:	    return 96;

		case fontDefault:   return 16;

		}

	return 16;
	}

int CFontManager::GetWidth(UINT uFont, PCTXT pText) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {
	
			int xSize = 0;

			while( *pText ) {

				xSize += pFont->GetGlyphWidth(*pText++);
				}

			return xSize;
			}

		return 0;
		}

	if( uFont >= fontSwiss0406 && uFont <= fontTimes1612 ) {

		int n = wstrlen(pText);

		switch( uFont ) {

			case fontSwiss0406: return n * 4;
			case fontSwiss0708: return n * 7;
			case fontSwiss0512: return n * 5;
			case fontSwiss0712: return n * 7;
			case fontSwiss1216: return n * 12;
			case fontTimes0808: return n * 8;
			case fontTimes0812: return n * 8;
			case fontTimes1612: return n * 16;

			}

		return n * 16;
		}

	if( uFont == fontDefault ) {

		uFont = fontHei16;
		}

	if( uFont >= fontHei16 && uFont <= fontHei24Bold ) {

		int       ySize = GetHeight(uFont);

		BOOL      fBold = (uFont - fontHei16) % 2;

		IGDIFont *pFont = (fBold ? Create_HeiBold : Create_HeiFont)(ySize);

		int       xSize = 0;

		while( *pText ) {

			xSize += pFont->GetGlyphWidth(*pText++);
			}

		pFont->Release();

		return xSize;
		}

	if( uFont >= fontBig24 && uFont <= fontBig96 ) {

		IGDIFont *pFont = Create_BigFont(GetHeight(uFont));

		int       xSize = 0;

		while( *pText ) {

			xSize += pFont->GetGlyphWidth(*pText++);
			}

		pFont->Release();

		return xSize;
		}

	return 0;
	}

// Font Weight

BOOL CFontManager::IsBold(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			return pFont->m_Bold;
			}

		return FALSE;
		}

	if( uFont == fontDefault ) {

		uFont = fontHei16;
		}

	switch( uFont ) {

		case fontHei10Bold:
		case fontHei12Bold:
		case fontHei14Bold:
		case fontHei16Bold:
		case fontHei18Bold:
		case fontHei20Bold:
		case fontHei24Bold:

			return TRUE;
		}

	return FALSE;
	}

BOOL CFontManager::CanClearBold(UINT uFont) const
{
	return ClearBold(uFont) < NOTHING;
	}

BOOL CFontManager::CanSetBold(UINT uFont) const
{
	return SetBold(uFont) < NOTHING;
	}

BOOL CFontManager::CanFlipBold(UINT uFont) const
{
	return FlipBold(uFont) < NOTHING;
	}

UINT CFontManager::SetBold(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			INDEX Index = m_pFonts->GetHead();

			while( !m_pFonts->Failed(Index) ) {

				CFontItem *pFind = m_pFonts->GetItem(Index);

				if( pFind->m_Face == pFont->m_Face ) {

					if( pFind->m_Size == pFont->m_Size ) {

						if( pFind->m_Bold ) {

							return pFind->GetFontID();
							}
						}
					}

				m_pFonts->GetNext(Index);
				}
			}

		return NOTHING;
		}
	else {
		UINT uNext = NOTHING;

		if( uFont == fontDefault ) {

			uFont = fontHei16;
			}

		switch( uFont ) {

			case fontHei10: uNext = fontHei10Bold; break;
			case fontHei12: uNext = fontHei12Bold; break;
			case fontHei14: uNext = fontHei14Bold; break;
			case fontHei16: uNext = fontHei16Bold; break;
			case fontHei18: uNext = fontHei18Bold; break;
			case fontHei20: uNext = fontHei20Bold; break;
			case fontHei24: uNext = fontHei24Bold; break;
			}

		if( uNext < NOTHING ) {

			CUISystem *pSystem = (CUISystem *) m_pDbase->GetSystemItem();

			if( pSystem->HasFont(uNext) ) {

				return uNext;
				}
			}
		}

	return NOTHING;
	}

UINT CFontManager::ClearBold(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			INDEX Index = m_pFonts->GetHead();

			while( !m_pFonts->Failed(Index) ) {

				CFontItem *pFind = m_pFonts->GetItem(Index);

				if( pFind->m_Face == pFont->m_Face ) {

					if( pFind->m_Size == pFont->m_Size ) {

						if( !pFind->m_Bold ) {

							return pFind->GetFontID();
							}
						}
					}

				m_pFonts->GetNext(Index);
				}
			}

		return NOTHING;
		}
	else {
		UINT uNext = NOTHING;

		if( uFont == fontDefault ) {

			uFont = fontHei16;
			}

		switch( uFont ) {

			case fontHei10Bold: uNext = fontHei10; break;
			case fontHei12Bold: uNext = fontHei12; break;
			case fontHei14Bold: uNext = fontHei14; break;
			case fontHei16Bold: uNext = fontHei16; break;
			case fontHei18Bold: uNext = fontHei18; break;
			case fontHei20Bold: uNext = fontHei20; break;
			case fontHei24Bold: uNext = fontHei24; break;
			}

		if( uNext < NOTHING ) {

			CUISystem *pSystem = (CUISystem *) m_pDbase->GetSystemItem();

			if( pSystem->HasFont(uNext) ) {

				return uNext;
				}
			}
		}

	return NOTHING;
	}

UINT CFontManager::FlipBold(UINT uFont) const
{
	return IsBold(uFont) ? ClearBold(uFont) : SetBold(uFont);
	}

// Font Sizing

BOOL CFontManager::CanMakeLarger(UINT uFont) const
{
	return GetLarger(uFont) < NOTHING;
	}

BOOL CFontManager::CanMakeSmaller(UINT uFont) const
{
	return GetSmaller(uFont) < NOTHING;
	}

UINT CFontManager::GetLarger(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			CFontItem *pNear = NULL;

			UINT       uNear = NOTHING;

			INDEX      Index = m_pFonts->GetHead();

			while( !m_pFonts->Failed(Index) ) {

				CFontItem *pFind = m_pFonts->GetItem(Index);

				if( pFind->m_Face == pFont->m_Face ) {

					if( pFind->m_Bold == pFont->m_Bold ) {

						if( pFind->m_Size > pFont->m_Size ) {

							UINT d = pFind->m_Size - pFont->m_Size;

							if( d < uNear ) {

								uNear = d;

								pNear = pFind;
								}
							}
						}
					}

				m_pFonts->GetNext(Index);
				}

			if( pNear ) {

				return pNear->GetFontID();
				}
			}

		return NOTHING;
		}
	else {
		if( uFont == fontDefault ) {

			uFont = fontHei16;
			}

		for(;;) {

			UINT uNext = NOTHING;

			switch( uFont ) {

				case fontSwiss0406: uNext = uFont + 1; break;
				case fontSwiss0712: uNext = uFont + 1; break;
				case fontSwiss1216: uNext = uFont + 1; break;
				case fontSwiss0708: uNext = uFont + 1; break;

				case fontTimes0808: uNext = uFont + 1; break;
				case fontTimes0812: uNext = uFont + 1; break;

				case fontHei10:	    uNext = fontHei12; break;
				case fontHei12:     uNext = fontHei14; break;
				case fontHei14:	    uNext = fontHei16; break;
				case fontHei16:	    uNext = fontHei18; break;
				case fontHei18:	    uNext = fontHei20; break;
				case fontHei20:	    uNext = fontHei24; break;

				case fontHei10Bold: uNext = fontHei12Bold; break;
				case fontHei12Bold: uNext = fontHei14Bold; break;
				case fontHei14Bold: uNext = fontHei16Bold; break;
				case fontHei16Bold: uNext = fontHei18Bold; break;
				case fontHei18Bold: uNext = fontHei20Bold; break;
				case fontHei20Bold: uNext = fontHei24Bold; break;

				case fontBig24:     uNext = uFont + 1; break;
				case fontBig32:     uNext = uFont + 1; break;
				case fontBig64:     uNext = uFont + 1; break;
				}

			if( uNext < NOTHING ) {

				CUISystem *pSystem = (CUISystem *) m_pDbase->GetSystemItem();

				if( pSystem->HasFont(uNext) ) {

					return uNext;
					}

				uFont = uNext;

				continue;
				}

			break;
			}
		}

	return NOTHING;
	}

UINT CFontManager::GetSmaller(UINT uFont) const
{
	if( IsCustom(uFont) ) {

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont ) {

			CFontItem *pNear = NULL;

			UINT       uNear = NOTHING;

			INDEX      Index = m_pFonts->GetHead();

			while( !m_pFonts->Failed(Index) ) {

				CFontItem *pFind = m_pFonts->GetItem(Index);

				if( pFind->m_Face == pFont->m_Face ) {

					if( pFind->m_Bold == pFont->m_Bold ) {

						if( pFind->m_Size < pFont->m_Size ) {

							UINT d = pFont->m_Size - pFind->m_Size;

							if( d < uNear ) {

								uNear = d;

								pNear = pFind;
								}
							}
						}
					}

				m_pFonts->GetNext(Index);
				}

			if( pNear ) {

				return pNear->GetFontID();
				}
			}

		return NOTHING;
		}
	else {
		if( uFont == fontDefault ) {

			uFont = fontHei16;
			}

		for(;;) {

			UINT uNext = NOTHING;

			switch( uFont ) {

				case fontSwiss0708: uNext = uFont - 1; break;
				case fontSwiss0512: uNext = uFont - 1; break;
				case fontSwiss0712: uNext = uFont - 1; break;
				case fontSwiss1216: uNext = uFont - 1; break;

				case fontTimes0812: uNext = uFont - 1; break;
				case fontTimes1612: uNext = uFont - 1; break;

				case fontHei12:     uNext = fontHei10; break;
				case fontHei14:	    uNext = fontHei12; break;
				case fontHei16:	    uNext = fontHei14; break;
				case fontHei18:	    uNext = fontHei16; break;
				case fontHei20:	    uNext = fontHei18; break;
				case fontHei24:	    uNext = fontHei20; break;

				case fontHei12Bold: uNext = fontHei10Bold; break;
				case fontHei14Bold: uNext = fontHei12Bold; break;
				case fontHei16Bold: uNext = fontHei14Bold; break;
				case fontHei18Bold: uNext = fontHei16Bold; break;
				case fontHei20Bold: uNext = fontHei18Bold; break;
				case fontHei24Bold: uNext = fontHei20Bold; break;

				case fontBig32:     uNext = uFont - 1; break;
				case fontBig64:     uNext = uFont - 1; break;
				case fontBig96:     uNext = uFont - 1; break;
				}

			if( uNext < NOTHING ) {

				CUISystem *pSystem = (CUISystem *) m_pDbase->GetSystemItem();

				if( pSystem->HasFont(uNext) ) {

					return uNext;
					}

				uFont = uNext;

				continue;
				}

			break;
			}
		}

	return NOTHING;
	}

// Download Support

BOOL CFontManager::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	UINT uCount = m_pFonts->GetIndexCount();

	Init.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		CFontItem *pFont = m_pFonts->GetItem(n);

		if( pFont ) {

			Init.AddWord(WORD(pFont->m_Handle));

			continue;
			}

		Init.AddWord(0xFFFF);
		}

	return TRUE;
	}

// Meta Data Creation

void CFontManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Fonts);
	}

// End of File
