
#include "intern.hpp"

#include "PrimRubyTankFill.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Tank Fill
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyTankFill, CPrimRubyBrush);

// Static Data

UINT   CPrimRubyTankFill::m_ShadeMode = 0;

COLOR  CPrimRubyTankFill::m_ShadeCol3 = 0;

C3REAL CPrimRubyTankFill::m_ShadeData = 0;

// Constructor

CPrimRubyTankFill::CPrimRubyTankFill(void)
{
	m_Mode    = 0;

	m_pValue  = NULL;

	m_pMin    = NULL;

	m_pMax    = NULL;

	m_pColor3 = New CPrimColor;
	}

// UI Managament

void CPrimRubyTankFill::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			m_fAutoLimits = HasAutoLimits();

			DoEnables(pHost);
			}

		if( Tag == "Mode" ) {

			DoEnables(pHost);
			}

		if( Tag == "Value" ) {

			if( m_fAutoLimits ) {

				if( m_pValue && m_pValue->IsTagRef() ) {

					CString Tag = m_pValue->GetTagName();

					InitCoded(pHost, L"Min", m_pMin, Tag + L".Min");

					InitCoded(pHost, L"Max", m_pMax, Tag + L".Max");

					m_fAutoLimits = TRUE;

					return;
					}
				}

			m_fAutoLimits = HasAutoLimits();
			}

		if( Tag == "Min" || Tag == "Max" ) {

			m_fAutoLimits = HasAutoLimits();
			}
		}

	CPrimRubyBrush::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimRubyTankFill::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" || Tag == "Min" || Tag == "Max" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimRubyBrush::GetTypeData(Tag, Type);
	}

// Operations

void CPrimRubyTankFill::Set(COLOR Color)
{
	CPrimRubyBrush::Set(Color);

	m_pColor3->Set(GetRGB(12,12,12));
	}

BOOL CPrimRubyTankFill::StepAddress(void)
{
	BOOL f1 = StepCoded(m_pValue);

	BOOL f2 = StepCoded(m_pMin);
	
	BOOL f3 = StepCoded(m_pMax);

	return f1 || f2 || f3;
	}

// Drawing

BOOL CPrimRubyTankFill::Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( !IsNull() ) {

		// TODO -- This technique relies on over-drawing the fill to
		// mask the bits we're not using. But that is dangerous with
		// anti-aliased primitives as it can result in artefacts!!!!

		CPrimRubyBrush::Fill(pGdi, list, fOver);

		if( PrepTankFill() ) {

			CRubyGdiLink link(pGdi);

			if( fOver )
				link.OutputShade(list, Shader, 0);
			else
				link.OutputShade(list, Shader);
			}

		return TRUE;
		}

	return FALSE;
	}

// Persistance

void CPrimRubyTankFill::Init(void)
{
	CPrimRubyBrush::Init();

	m_pColor3->Set(GetRGB(12,12,12));
	}

// Download Support

BOOL CPrimRubyTankFill::MakeInitData(CInitData &Init)
{
	CPrimRubyBrush::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));

	if( m_Mode ) {

		Init.AddItem(itemVirtual, m_pValue);
		
		Init.AddItem(itemVirtual, m_pMin);
		
		Init.AddItem(itemVirtual, m_pMax);

		Init.AddItem(itemSimple,  m_pColor3);
		}

	return TRUE;
	}

// Meta Data

void CPrimRubyTankFill::AddMetaData(void)
{
	CPrimRubyBrush::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddVirtual(Value);
	Meta_AddVirtual(Min);
	Meta_AddVirtual(Max);
	Meta_AddObject (Color3);

	Meta_SetName(IDS_TANK_FILL);
	}

// Implementation

void CPrimRubyTankFill::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Color3", m_Mode > 0);

	pHost->EnableUI(this, "Value",  m_Mode > 0);

	pHost->EnableUI(this, "Min",    m_Mode > 0);

	pHost->EnableUI(this, "Max",    m_Mode > 0);
	}

C3REAL CPrimRubyTankFill::GetValue(CCodedItem *pItem, C3REAL Default)
{
	if( pItem ) {
		
		if( !pItem->IsBroken() ) {

			return I2R(pItem->Execute(typeReal));
			}
		}

	return Default;
	}

BOOL CPrimRubyTankFill::HasAutoLimits(void)
{
	if( m_pMin && m_pMax ) {

		if( m_pValue ) {
		
			CString c1 = m_pValue->GetSource(FALSE);

			CString c2 = m_pMin->GetSource  (FALSE);

			CString c3 = m_pMax->GetSource  (FALSE);

			if( c2 == c1 + L".Min" ) {

				if( c3 == c1 + L".Max" ) {

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( m_pMin || m_pMax ) {

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CPrimRubyTankFill::PrepTankFill(void)
{
	if( m_Mode ) {

		C3REAL Min = GetValue(m_pMin, 0);

		C3REAL Max = GetValue(m_pMax, 100);

		if( Min != Max ) {

			C3REAL Data = GetValue(m_pValue, 25);

			m_ShadeData = (Data - Min) / (Max - Min);
			}
		else
			m_ShadeData = 0.75;
	
		m_ShadeMode = m_Mode;

		m_ShadeCol3 = m_pColor3->GetColor();

		return TRUE;
		}

	return FALSE;
	}

// Shaders

BOOL CPrimRubyTankFill::Shader(IGDI *pGDI, int p, int c)
{
	static int nTrip = 0;

	static int nMode = 0;

	if( c ) {

		if( p == 0 ) {

			nTrip = int(c * m_ShadeData);

			if( m_ShadeMode == 1 || m_ShadeMode == 4 ) {

				nTrip = c - nTrip;

				nMode = 0;
				}

			if( m_ShadeMode == 2 || m_ShadeMode == 3 ) {

				nMode = 1;
				}

			if( nTrip ) {

				if( nMode == 0 ) {

					pGDI->SetBrushStyle(brushFore);
		
					pGDI->SetBrushFore(m_ShadeCol3);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushNull);
					}

				return TRUE;
				}
			}

		if( p >= nTrip ) {

			if( nMode < 2 ) {

				if( nMode == 0 ) {
	
					pGDI->SetBrushStyle(brushNull);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushFore);

					pGDI->SetBrushFore(m_ShadeCol3);
					}

				nMode = 2;

				return TRUE;
				}
			}

		return FALSE;
		}

	if( m_ShadeMode == 1 || m_ShadeMode == 2 ) {

		return FALSE;
		}

	return TRUE;
	}

// End of File
