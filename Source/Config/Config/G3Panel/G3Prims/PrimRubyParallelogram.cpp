
#include "intern.hpp"

#include "PrimRubyParallelogram.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Parallelogram Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyParallelogram, CPrimRubyWithHandle);

// Constructor

CPrimRubyParallelogram::CPrimRubyParallelogram(void)
{
	m_Max1 = 9500;
	}

// Meta Data

void CPrimRubyParallelogram::AddMetaData(void)
{
	CPrimRubyWithHandle::AddMetaData();

	Meta_SetName((IDS_PARALLELOGRAM));
	}

// Path Generation

void CPrimRubyParallelogram::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	number y1 = p1.m_y + (p2.m_y - p1.m_y) * m_Pos1 / 10000;

	number y2 = p2.m_y - (p2.m_y - p1.m_y) * m_Pos1 / 10000;

	figure.Append(p1.m_x, p1.m_y);
	
	figure.Append(p2.m_x, y1);
	
	figure.Append(p2.m_x, p2.m_y);
	
	figure.Append(p1.m_x, y2);

	figure.AppendHardBreak();
	}

// End of File
