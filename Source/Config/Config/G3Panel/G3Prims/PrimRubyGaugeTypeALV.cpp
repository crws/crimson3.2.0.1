
#include "intern.hpp"

#include "PrimRubyGaugeTypeALV.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Vertical Linear Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeALV, CPrimRubyGaugeTypeAL);

// Constructor

CPrimRubyGaugeTypeALV::CPrimRubyGaugeTypeALV(void)
{
	m_Orient = 0;
	}

// Overridables

void CPrimRubyGaugeTypeALV::SetInitState(void)
{
	CPrimRubyGaugeTypeAL::SetInitState();

	SetInitSize(120, 300);
	}

// Meta Data Creation

void CPrimRubyGaugeTypeALV::AddMetaData(void)
{
	CPrimRubyGaugeTypeAL::AddMetaData();

	Meta_SetName((IDS_TYPE_VERTICAL));
	}

// End of File
