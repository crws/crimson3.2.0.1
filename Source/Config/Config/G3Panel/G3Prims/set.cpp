
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Set Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimSet, CPrim);

// Constructor

CPrimSet::CPrimSet(void)
{
	m_pList    = New CPrimList;

	m_LockList = 0;

	m_fSkip    = FALSE;
	}

// Attributes

CRect CPrimSet::GetUsedRect(void) const
{
	return m_pList->GetUsedRect();
	}

// Operations

void CPrimSet::SetSkip(BOOL fSkip)
{
	m_fSkip = fSkip;
	}

// Overridables

BOOL CPrimSet::HitTest(P2 Pos)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		if( m_pList->GetItem(Index)->HitTest(Pos) ) {

			return TRUE;
			}

		m_pList->GetNext(Index);
		}

	return FALSE;
	}

CRect CPrimSet::GetBoundingRect(void)
{
	CRect Rect;
	
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		Rect |= m_pList->GetItem(Index)->GetBoundingRect();

		m_pList->GetNext(Index);
		}

	return Rect;
	}

void CPrimSet::Draw(IGDI *pGDI, UINT uMode)
{
	if( uMode != drawSet ) {

		INDEX Index = m_pList->GetHead();

		while( !m_pList->Failed(Index) ) {

			m_pList->GetItem(Index)->Draw(pGDI, uMode);

			m_pList->GetNext(Index);
			}
		}
	}

void CPrimSet::GetRefs(CPrimRefList &Refs)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		m_pList->GetItem(Index)->GetRefs(Refs);

		m_pList->GetNext(Index);
		}

	CPrim::GetRefs(Refs);
	}

void CPrimSet::EditRef(UINT uOld, UINT uNew)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		m_pList->GetItem(Index)->EditRef(uOld, uNew);

		m_pList->GetNext(Index);
		}

	CPrim::EditRef(uOld, uNew);
	}

void CPrimSet::Validate(BOOL fExpand)
{
	m_pList->Validate(fExpand);
	}

void CPrimSet::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	m_pList->TagCheck(Done, Tags);
	}

BOOL CPrimSet::StepAddress(void)
{
	BOOL  fStep = FALSE;

	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		if( pPrim->StepAddress() ) {

			fStep = TRUE;
			}

		m_pList->GetNext(Index);
		}

	return fStep;
	}

// Persistance

void CPrimSet::PostLoad(void)
{
	CPrim::PostLoad();

	// NOTE -- This code removes zero-sized primitives that sometimes
	// get into the database, and screw-up conversion and other processes
	// fail due to divide-by-zero errors. !!!!

	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		if( IsRectEmpty(pPrim->GetNormRect()) ) {

			INDEX Remove = Index;

			m_pList->GetNext(Index);

			m_pList->RemoveItem(Remove);

			pPrim->Kill();

			delete pPrim;
			}

		m_pList->GetNext(Index);
		}
	}

// Property Save Filter

BOOL CPrimSet::SaveProp(CString const &Tag) const
{
	if( Tag == "List" ) {

		return !m_fSkip;
		}

	return CPrim::SaveProp(Tag);
	}

// Download Support

BOOL CPrimSet::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pList);

	return TRUE;
	}

// Meta Data

void CPrimSet::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddCollect(List);
	Meta_AddInteger(LockList);

	Meta_SetName((IDS_SET));
	}

// End of File
