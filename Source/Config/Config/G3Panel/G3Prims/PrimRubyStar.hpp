
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyStar_HPP
	
#define	INCLUDE_PrimRubyStar_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPolygon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Star Primitive
//

class CPrimRubyStar : public CPrimRubyPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyStar(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
