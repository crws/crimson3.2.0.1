
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SCALE_HPP
	
#define	INCLUDE_SCALE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimVertScale;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Scale
//

class CPrimVertScale : public CPrimRich
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimVertScale(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);
		void SetTextColor(COLOR Color);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_Precise;
		UINT         m_UseAll;
		UINT         m_ShowLabels;
		UINT         m_ShowUnits;
		UINT         m_Orient;
		CPrimColor * m_pLineCol;
		CPrimColor * m_pTextCol;
		CPrimColor * m_pUnitCol;
		UINT         m_TextFont;
		UINT         m_UnitFont;
		UINT	     m_FontBase;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Field Requirements
		UINT GetNeedMask(void) const;

		// Implementation
		void   DoEnables(IUIHost *pHost);
		C3REAL GetMinScale(void);
		C3REAL GetMaxScale(void);
		BOOL   AdjustLimit(C3REAL &v);
	};

//////////////////////////////////////////////////////////////////////////
//
// Scale Helper
//

class CScaleHelper
{
	public:
		// Constructor
		CScaleHelper(double Min, double Max);

		// Operations
		void CalcDecimalStep(double Limit);
		void CalcTimeStep(double Limit);
		
		// Attributes
		double GetDrawStep(void);
		double GetStepValue(int n);		
		int    GetStepCount(void);
		int    GetStepSpan(void);
		
	protected:
		// Data Members
		double m_DataMin;
		double m_DataMax;
		double m_DataSpan;
		int    m_StepMin;
		int    m_StepMax;
		double m_DrawStep;

		// Implementation
		double FindDrawStep(double Limit);
	};

// End of File

#endif
