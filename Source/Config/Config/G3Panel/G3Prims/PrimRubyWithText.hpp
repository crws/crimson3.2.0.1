
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyWithText_HPP
	
#define	INCLUDE_PrimRubyWithText_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive with Text
//

class CPrimRubyWithText : public CPrimWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyWithText(void);

		// Overridables
		BOOL  HitTest(P2 Pos);
		CRect GetBoundingRect(void);
		void  SetHand(BOOL fInit);
		void  UpdateLayout(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		R2 m_bound;

		// Implementation
		BOOL AddList(CInitData &Init, CRubyGdiList const &list);

		// Path Management
		virtual void InitPaths(void);
		virtual void MakePaths(void);
		virtual void MakeLists(void);
	};

// End of File

#endif
