
#include "intern.hpp"

#include "resview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Coloritives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Color Resource
//

// Dynamic Class

AfxImplementDynamicClass(CColorResourceItem, CMetaItem);

// Constructor

CColorResourceItem::CColorResourceItem(void)
{
	}

// UI Creation

CViewWnd * CColorResourceItem::CreateView(UINT uType)
{
/*	if( uType == viewResource ) {

		return New CColorResourceWnd;
		}						    

*/	return CMetaItem::CreateView(uType);
	}

// End of File
