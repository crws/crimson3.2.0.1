
#include "intern.hpp"

#include "PrimRubyGaugeTypeAL.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Linear Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAL, CPrimRubyGaugeTypeA);

// Constructor

CPrimRubyGaugeTypeAL::CPrimRubyGaugeTypeAL(void)
{
	m_uType      = 0xC1;

	m_PointStyle = CRubyStroker::arrowMediumOuter;

	m_lineFact   = 1.0;

	m_Margin.x1  = 0;

	m_Margin.y1  = 0;

	m_Margin.x2  = 0;
	
	m_Margin.y2  = 0;
	}

// UI Managament

void CPrimRubyGaugeTypeAL::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "PointMode" ) {

			DoEnables(pHost);
			}
		}

	CPrimRubyGaugeTypeA::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimRubyGaugeTypeAL::Draw(IGDI *pGdi, UINT uMode)
{
	CPrimRubyGaugeTypeA::Draw(pGdi, uMode);

	CRubyGdiLink gdi(pGdi);

	gdi.OutputSolid(m_listPoint, m_pPointColor->GetColor(), 0);
	}

// Download Support

BOOL CPrimRubyGaugeTypeAL::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeA::MakeInitData(Init);

	Init.AddByte(BYTE(m_PointStyle));

	AddNumber(Init, m_lineFact);
	AddNumber(Init, m_posMin);
	AddNumber(Init, m_posMax);
	AddNumber(Init, m_posPoint);
	AddNumber(Init, m_posSweep);
	AddNumber(Init, m_posMajor);
	AddNumber(Init, m_posMinor);
	AddNumber(Init, m_posOuter);
	AddNumber(Init, m_posBug);
	AddNumber(Init, m_posBand);
	AddNumber(Init, m_posCenter);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAL::AddMetaData(void)
{
	CPrimRubyGaugeTypeA::AddMetaData();

	Meta_AddInteger(PointStyle);

	Meta_AddRect   (Margin);

	Meta_SetName((IDS_TYPE_LINEAR_GAUGE));
	}

// Path Management

void CPrimRubyGaugeTypeAL::InitPaths(void)
{
	CPrimRubyGaugeTypeA::InitPaths();

	m_pathPoint.Empty();
	}

void CPrimRubyGaugeTypeAL::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeA::MakePaths();

	PrepareTicks();

	PreparePoint();

	PrepareBand(m_pathBand1, m_BandShow1, m_pBandMin1, m_pBandMax1);

	PrepareBand(m_pathBand2, m_BandShow2, m_pBandMin2, m_pBandMax2);

	PrepareBug (m_pathBug1,  m_BugShow1,  m_pBugValue1);

	PrepareBug (m_pathBug2,  m_BugShow2,  m_pBugValue2);

	Rect(m_pathFace,  40, 100);

	Rect(m_pathOuter, 53, 113, 10);
	Rect(m_pathInner, 43, 103, 10);
	Rect(m_pathRing1, 62, 122,  2);
	Rect(m_pathRing2, 52, 112,  1);
	Rect(m_pathRing3, 39,  99,  4);
	Rect(m_pathRing4, 42, 102,  5);
	}

void CPrimRubyGaugeTypeAL::MakeLists(void)
{
	CPrimRubyGaugeTypeA::MakeLists();

	m_pathPoint.Transform(m_m);

	m_listPoint.Load(m_pathPoint,  true);
	}

// Scaling

number CPrimRubyGaugeTypeAL::GetPos(CCodedItem *pValue, C3REAL Default)
{
	number v = GetValue(pValue, Default);

	number a = GetValue(m_pMin, 0);

	number b = GetValue(m_pMax, 100);

	if( num_isnan(v) || num_isnan(a) || num_isnan(b) ) {

		return m_posMin;
		}

	if( !num_equal(a, b) ) {

		MakeMin(v, Max(a, b));

		MakeMax(v, Min(a, b));

		return m_posMin + (m_posMax - m_posMin) * (v - a) / (b - a);
		}

	return m_posMin;
	}

// Path Preparation

void CPrimRubyGaugeTypeAL::PrepareTicks(void)
{
	number s  = fabs(m_posMax - m_posMin) / (m_Major * m_Minor);

	int    q  = 0;

	number a1 = min(m_posMin, m_posMax);

	number a2 = max(m_posMin, m_posMax);

	for( number t = a1; t < a2 + s / 2; t += s, q += 1 ) {

		if( q % m_Minor ) {

			CRubyPoint p1(m_posCenter - m_posMinor, t);
			
			CRubyPoint p2(m_posCenter + m_posMinor, t);

			if( m_Minor % 2 == 0 && q % m_Minor == m_Minor / 2 ) {

				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.5, CRubyStroker::endFlat);
				}
			else
				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.0, CRubyStroker::endFlat);
			}
		else {
			CRubyPoint p1(m_posCenter - m_posMajor, t);
			
			CRubyPoint p2(m_posCenter + m_posMajor, t);

			m_d.Line(m_pathMajor, p1, p2, m_lineFact * 2.0, CRubyStroker::endFlat);
			}
		}
	}

void CPrimRubyGaugeTypeAL::PreparePoint(void)
{
	if( m_PointMode == 0 ) {

		number t = GetPos(m_pValue, 25);

		CRubyPoint p1(m_posCenter - m_posPoint, t);
		CRubyPoint p2(m_posCenter + m_posPoint, t);
		
		m_d.Line(m_pathPoint, p1, p2, m_lineFact * 4.0, m_PointStyle, m_PointStyle);
		}

	if( m_PointMode == 1 ) {

		number t1 = GetPos(m_pMin,   0);
		number t2 = GetPos(m_pValue, 25);

		CRubyPoint p1(m_posCenter - m_posSweep, t1);
		CRubyPoint p2(m_posCenter + m_posSweep, t1);
		CRubyPoint p3(m_posCenter + m_posSweep, t2);
		CRubyPoint p4(m_posCenter - m_posSweep, t2);

		m_pathPoint.Append(p1);
		m_pathPoint.Append(p2);
		m_pathPoint.Append(p3);
		m_pathPoint.Append(p4);

		m_pathPoint.AppendHardBreak();
		}
	}

BOOL CPrimRubyGaugeTypeAL::PrepareBand(CRubyPath &path, BOOL fShow, CCodedItem *pMin, CCodedItem *pMax)
{
	if( fShow ) {

		number t1 = GetPos(pMin,  75);
		number t2 = GetPos(pMax, 100);

		CRubyPoint p1(m_posCenter + m_posOuter, t1);
		CRubyPoint p2(m_posCenter + m_posBand,  t1);
		CRubyPoint p3(m_posCenter + m_posBand,  t2);
		CRubyPoint p4(m_posCenter + m_posOuter, t2);

		path.Append(p1);
		path.Append(p2);
		path.Append(p3);
		path.Append(p4);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyGaugeTypeAL::PrepareBug(CRubyPath &path, BOOL fShow, CCodedItem *pValue)
{
	if( fShow ) {

		number t1 = GetPos(pValue,  75);
		number t2 = t1 - 4;
		number t3 = t1 + 4;

		CRubyPoint p1(m_posCenter - m_posOuter, t1);
		CRubyPoint p2(m_posCenter - m_posBug,   t2);
		CRubyPoint p3(m_posCenter - m_posBug,   t3);

		path.Append(p1);
		path.Append(p2);
		path.Append(p3);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

// Drawing Helpers

void CPrimRubyGaugeTypeAL::Rect(CRubyPath &path, number cx, number cy)
{
	CRubyPoint p1, p2, p3, p4;

	p1 = m_pointCenter;
	p2 = m_pointCenter;
	p3 = m_pointCenter;
	p4 = m_pointCenter;

	p1.m_x -= cx;
	p2.m_x -= cx;
	p3.m_x += cx;
	p4.m_x += cx;

	p1.m_y -= cy;
	p2.m_y += cy;
	p3.m_y += cy;
	p4.m_y -= cy;

	path.Append(p1);
	path.Append(p2);
	path.Append(p3);
	path.Append(p4);

	path.AppendHardBreak();
	}

void CPrimRubyGaugeTypeAL::Rect(CRubyPath &path, number cx, number cy, number w)
{
	Rect(path, -cx, -cy);

	path.SoftenLastBreak();

	cx += w;
	cy += w;

	Rect(path, +cx, +cy);
	}

// Implementation

void CPrimRubyGaugeTypeAL::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"PointStyle", !m_PointMode);
	}

void CPrimRubyGaugeTypeAL::LoadLayout(void)
{
	m_posMin = 100 + 85 * ((100 - m_Margin.y2) / 100.0);
	m_posMax = 100 - 85 * ((100 - m_Margin.y1) / 100.0);
	
	number total = (200 - m_Margin.x1 - m_Margin.x2) / 200.0;
	number share = (100 - m_Margin.x2) / 200.0;

	m_posPoint  = 24 * total;
	m_posSweep  = 10 * total;
	m_posMajor  = 20 * total;
	m_posMinor  = 15 * total;
	m_posOuter  = 25 * total;
	m_posBug    = 32 * total;
	m_posBand   = 30 * total;

	m_posCenter = m_pointCenter.m_x - m_posBug + 64 * share;
	}

// End of File
