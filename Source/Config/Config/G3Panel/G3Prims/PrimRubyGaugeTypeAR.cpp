
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Radial Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR, CPrimRubyGaugeTypeA);

// Constructor

CPrimRubyGaugeTypeAR::CPrimRubyGaugeTypeAR(void)
{
	m_uType        = 0xC0;

	m_PointStyle   = CRubyStroker::endPoint;
	
	m_pColorPivot1 = New CPrimColor;
	
	m_pColorPivot2 = New CPrimColor;

	m_lineFact     = 1.0;

	m_ScaleActive  = 100;

	m_ScalePivot   = 100;
	}

// UI Managament

void CPrimRubyGaugeTypeAR::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "PointMode" || Tag == "GaugeStyle" ) {

			DoEnables(pHost);
			}
		}

	CPrimRubyGaugeTypeA::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimRubyGaugeTypeAR::Draw(IGDI *pGdi, UINT uMode)
{
	CPrimRubyGaugeTypeA::Draw(pGdi, uMode);

	CRubyGdiLink gdi(pGdi);

	gdi.OutputSolid(m_listPoint, m_pPointColor->GetColor(), 0);

	if( !m_PointMode ) {

		gdi.OutputSolid(m_listPivot1, GetColor(0), 0);

		gdi.OutputSolid(m_listPivot2, GetColor(1), 0);
		}
	}

void CPrimRubyGaugeTypeAR::SetInitState(void)
{
	CPrimRubyGaugeTypeA::SetInitState();

	m_pColorPivot1->Set(GetRGB(8,8,8));
	
	m_pColorPivot2->Set(GetRGB(0,0,0));
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeA::MakeInitData(Init);

	Init.AddByte(BYTE(m_PointStyle));

	Init.AddWord(GetColor(0));
	Init.AddWord(GetColor(1));

	AddList(Init, m_listPivot1);
	AddList(Init, m_listPivot2);

	AddPoint (Init, m_pointPivot);
	AddNumber(Init, m_radiusPivot);
	AddNumber(Init, m_angleMin);
	AddNumber(Init, m_angleMax);
	AddNumber(Init, m_lineFact);
	AddVector(Init, m_radiusBug);
	AddVector(Init, m_radiusOuter);
	AddVector(Init, m_radiusMajor);
	AddVector(Init, m_radiusMinor);
	AddVector(Init, m_radiusBand);
	AddVector(Init, m_radiusPoint);
	AddVector(Init, m_radiusSweep);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR::AddMetaData(void)
{
	CPrimRubyGaugeTypeA::AddMetaData();

	Meta_AddInteger(PointStyle);
	Meta_AddObject (ColorPivot1);
	Meta_AddObject (ColorPivot2);
	Meta_AddInteger(ScaleActive);
	Meta_AddInteger(ScalePivot);

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE));
	}

// Path Management

void CPrimRubyGaugeTypeAR::InitPaths(void)
{
	CPrimRubyGaugeTypeA::InitPaths();

	m_pathPoint .Empty();
	m_pathPivot1.Empty();
	m_pathPivot2.Empty();
	}

void CPrimRubyGaugeTypeAR::MakePaths(void)
{
	CPrimRubyGaugeTypeA::MakePaths();

	if( !m_PointMode ) {

		m_d.Circle(m_pathPivot1, m_pointPivot, m_radiusPivot, m_scale);

		number w = m_lineFact * 2 * m_ScalePivot / 100.0;

		m_d.m_s.StrokeLoop(m_pathPivot2, m_pathPivot1, 0, w);
		}

	PrepareTicks();

	PreparePoint();

	PrepareBand(m_pathBand1, m_BandShow1, m_pBandMin1, m_pBandMax1);

	PrepareBand(m_pathBand2, m_BandShow2, m_pBandMin2, m_pBandMax2);

	PrepareBug (m_pathBug1,  m_BugShow1,  m_pBugValue1);

	PrepareBug (m_pathBug2,  m_BugShow2,  m_pBugValue2);
	}

void CPrimRubyGaugeTypeAR::MakeLists(void)
{
	CPrimRubyGaugeTypeA::MakeLists();

	m_pathPoint .Transform(m_m);
	m_pathPivot1.Transform(m_m);
	m_pathPivot2.Transform(m_m);

	m_listPoint .Load(m_pathPoint,  true);
	m_listPivot1.Load(m_pathPivot1, true);
	m_listPivot2.Load(m_pathPivot2, true);
	}

// Scaling

number CPrimRubyGaugeTypeAR::GetAngle(CCodedItem *pValue, C3REAL Default)
{
	number v = GetValue(pValue, Default);

	number a = GetValue(m_pMin, 0);

	number b = GetValue(m_pMax, 100);

	if( num_isnan(v) || num_isnan(a) || num_isnan(b) ) {

		return m_angleMin;
		}

	if( !num_equal(a, b) ) {

		MakeMin(v, Max(a, b));

		MakeMax(v, Min(a, b));

		return m_angleMin + (m_angleMax - m_angleMin) * (v - a) / (b - a);
		}

	return m_angleMin;
	}

// Path Preparation

void CPrimRubyGaugeTypeAR::PrepareTicks(void)
{
	number s  = fabs(m_angleMax - m_angleMin) / (m_Major * m_Minor);

	int    q  = 0;

	number a1 = min(m_angleMin, m_angleMax);

	number a2 = max(m_angleMin, m_angleMax);

	for( number t = a1; t < a2 + s / 2; t += s, q += 1 ) {

		if( q % m_Minor ) {

			CRubyVector cs(t);

			CRubyPoint  p1(m_pointPivot + (cs << m_radiusOuter));

			CRubyPoint  p2(m_pointPivot + (cs << m_radiusMinor));

			if( m_Minor % 2 == 0 && q % m_Minor == m_Minor / 2 ) {

				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.5, CRubyStroker::endFlat);
				}
			else
				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.0, CRubyStroker::endFlat);
			}
		else {
			CRubyVector cs(t);

			CRubyPoint  p1(m_pointPivot + (cs << m_radiusOuter));

			CRubyPoint  p2(m_pointPivot + (cs << m_radiusMajor));

			m_d.Line(m_pathMajor, p1, p2, m_lineFact * 2.0, CRubyStroker::endFlat);
			}
		}
	}

void CPrimRubyGaugeTypeAR::PreparePoint(void)
{
	if( m_PointMode == 0 ) {

		CRubyVector cs(GetAngle(m_pValue, 25));

		CRubyPoint  pp(m_pointPivot + (cs << m_radiusPoint));

		m_d.Line(m_pathPoint, m_pointPivot, pp, m_lineFact * 5.0, CRubyStroker::arrowNone, m_PointStyle);
		}

	if( m_PointMode == 1 ) {

		number t1 = GetAngle(m_pMin,   0);

		number t2 = GetAngle(m_pValue, 25);

		m_d.Arc(m_pathPoint, m_pointPivot, t1, t2, m_radiusPoint, m_scale);

		m_d.Arc(m_pathPoint, m_pointPivot, t2, t1, m_radiusSweep, m_scale);

		m_pathPoint.AppendHardBreak();
		}
	}

BOOL CPrimRubyGaugeTypeAR::PrepareBand(CRubyPath &path, BOOL fShow, CCodedItem *pMin, CCodedItem *pMax)
{
	if( fShow ) {

		number t1 = GetAngle(pMin,  75);

		number t2 = GetAngle(pMax, 100);

		m_d.Arc(path, m_pointPivot, t1, t2, m_radiusBand, m_scale);

		m_d.Arc(path, m_pointPivot, t2, t1, m_radiusOuter, m_scale);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyGaugeTypeAR::PrepareBug(CRubyPath &path, BOOL fShow, CCodedItem *pValue)
{
	if( fShow ) {

		number t1 = GetAngle(pValue, 50);

		number t2 = t1 - 2.5;
		number t3 = t1 + 2.5;

		CRubyPoint p1 = m_pointPivot + (CRubyVector(t1) << m_radiusOuter);
		CRubyPoint p2 = m_pointPivot + (CRubyVector(t2) << m_radiusBug  );
		CRubyPoint p3 = m_pointPivot + (CRubyVector(t3) << m_radiusBug  );

		path.Append(p1);
		path.Append(p2);
		path.Append(p3);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

// Drawing Helpers

void CPrimRubyGaugeTypeAR::Ring(CRubyPath &path, number r1, number r2)
{
	m_d.Circle(path, m_pointCenter, ScaleBezel(r1), m_scale);

	path.SoftenLastBreak();

	m_d.Circle(path, m_pointCenter, ScaleBezel(r2), m_scale);
	}

void CPrimRubyGaugeTypeAR::Semi(CRubyPath &path, number rb, number r1)
{
	rb = ScaleBezel(rb);
	r1 = ScaleBezel(r1);

	CRubyPoint p1, p2, p3, p4;

	p1 = m_pointCenter;
	p2 = m_pointCenter;

	p1.m_x -= r1;
	p2.m_x += r1;

	p3 = p1;
	p4 = p2;

	p3.m_y += fabs(r1) - rb;
	p4.m_y += fabs(r1) - rb;

	m_d.Arc(path, m_pointCenter, p1, p2, r1, m_scale);

	path.Append(p4);
	path.Append(p3);

	path.AppendHardBreak();
	}

void CPrimRubyGaugeTypeAR::Semi(CRubyPath &path, number rb, number r1, number r2)
{
	Semi(path, rb, +r1);

	path.SoftenLastBreak();

	Semi(path, rb, -r2);
	}

void CPrimRubyGaugeTypeAR::Quad(CRubyPath &path, number rb, number r1)
{
	rb = ScaleBezel(rb);
	r1 = ScaleBezel(r1);

	CRubyPoint p1, p2, p3, p4, p5;

	p1 = m_pointCenter;
	p2 = m_pointCenter;

	p1.m_y -= fabs(r1);
	p2.m_x += fabs(r1);

	p3 = p1;
	p4 = m_pointCenter;
	p5 = p2;

	p3.m_x -= fabs(r1) - rb;
	p4.m_x -= fabs(r1) - rb;
	p4.m_y += fabs(r1) - rb;
	p5.m_y += fabs(r1) - rb;

	if( r1 > 0 ) {

		m_d.Arc(path, m_pointCenter, p1, p2, r1, m_scale);

		path.Append(p5);
		path.Append(p4);
		path.Append(p3);
		}
	else {
		m_d.Arc(path, m_pointCenter, p2, p1, r1, m_scale);

		path.Append(p3);
		path.Append(p4);
		path.Append(p5);
		}

	path.AppendHardBreak();
	}

void CPrimRubyGaugeTypeAR::Quad(CRubyPath &path, number rb, number r1, number r2)
{
	Quad(path, rb, +r1);

	path.SoftenLastBreak();

	Quad(path, rb, -r2);
	}

void CPrimRubyGaugeTypeAR::Rect(CRubyPath &path, number rb, number r1)
{
	rb = ScaleBezel(rb);
	r1 = ScaleBezel(r1);

	CRubyPoint p1, p2, p3, p4;

	p1 = m_pointCenter;
	p2 = m_pointCenter;
	p3 = m_pointCenter;
	p4 = m_pointCenter;

	p1.m_x -= r1;
	p2.m_x -= r1;
	p3.m_x += r1;
	p4.m_x += r1;

	p1.m_y -= r1;
	p2.m_y += r1;
	p3.m_y += r1;
	p4.m_y -= r1;

	path.Append(p1);
	path.Append(p2);
	path.Append(p3);
	path.Append(p4);

	path.AppendHardBreak();
	}

void CPrimRubyGaugeTypeAR::Rect(CRubyPath &path, number rb, number r1, number r2)
{
	Rect(path, rb, +r1);

	path.SoftenLastBreak();

	Rect(path, rb, -r2);
	}

void CPrimRubyGaugeTypeAR::Half(CRubyPath &path, number rb, number r1)
{
	rb = ScaleBezel(rb);
	r1 = ScaleBezel(r1);

	CRubyPoint p1, p2, p3, p4;

	p1 = m_pointCenter;
	p2 = m_pointCenter;
	p3 = m_pointCenter;
	p4 = m_pointCenter;

	p1.m_x -= r1;
	p2.m_x -= r1;
	p3.m_x += r1;
	p4.m_x += r1;

	p1.m_y -= fabs(r1);
	p2.m_y += fabs(r1) - rb;
	p3.m_y += fabs(r1) - rb;
	p4.m_y -= fabs(r1);

	path.Append(p1);
	path.Append(p2);
	path.Append(p3);
	path.Append(p4);

	path.AppendHardBreak();
	}

void CPrimRubyGaugeTypeAR::Half(CRubyPath &path, number rb, number r1, number r2)
{
	Half(path, rb, +r1);

	path.SoftenLastBreak();

	Half(path, rb, -r2);
	}

// Color Schemes

COLOR CPrimRubyGaugeTypeAR::GetColor(UINT n)
{
	if( m_GaugeStyle == 0 ) {

		switch( n ) {

			case 0: return m_pColorPivot1->GetColor();
			case 1: return m_pColorPivot2->GetColor();
			}
		}

	switch( n ) {

		case 0: return GetRGB(8,8,8);
		case 1: return GetRGB(0,0,0);
		}

	return 0;
	}

// Implementation

void CPrimRubyGaugeTypeAR::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"PointStyle",  !m_PointMode);

	pHost->EnableUI(this, L"ColorPivot1", !m_GaugeStyle);

	pHost->EnableUI(this, L"ColorPivot2", !m_GaugeStyle);
	}

void CPrimRubyGaugeTypeAR::CalcRadii(void)
{
	number s = m_ScaleActive / 100.0;

	m_radiusOuter.m_x *= s;
	m_radiusOuter.m_y *= s;
	m_radiusBug.m_x   *= s;
	m_radiusMajor.m_x *= s;
	m_radiusMinor.m_x *= s;
	m_radiusBand.m_x  *= s;
	m_radiusPoint.m_x *= s;
	m_radiusSweep.m_x *= s;
	
	number k = m_radiusOuter.m_y / m_radiusOuter.m_x;

	m_radiusBug.m_y   = k * m_radiusBug.m_x;
	m_radiusMajor.m_y = k * m_radiusMajor.m_x;
	m_radiusMinor.m_y = k * m_radiusMinor.m_x;
	m_radiusBand.m_y  = k * m_radiusBand.m_x;
	m_radiusPoint.m_y = k * m_radiusPoint.m_x;
	m_radiusSweep.m_y = k * m_radiusSweep.m_x;
	}

// End of File
