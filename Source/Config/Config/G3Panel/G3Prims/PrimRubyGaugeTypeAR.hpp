
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAR_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAR_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Radial Gauge Primitive
//

class CPrimRubyGaugeTypeAR : public CPrimRubyGaugeTypeA
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeAR(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void Draw(IGDI *pGdi, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_PointStyle;
		CPrimColor * m_pColorPivot1;
		CPrimColor * m_pColorPivot2;
		UINT	     m_ScaleActive;
		UINT	     m_ScalePivot;

	protected:
		// Data Members
		CRubyPoint    m_pointPivot;
		number	      m_radiusPivot;
		number	      m_angleMin;
		number	      m_angleMax;
		number	      m_lineFact;
		CRubyVector   m_radiusBug;
		CRubyVector   m_radiusOuter;
		CRubyVector   m_radiusMajor;
		CRubyVector   m_radiusMinor;
		CRubyVector   m_radiusBand;
		CRubyVector   m_radiusPoint;
		CRubyVector   m_radiusSweep;
		CRubyPath     m_pathPoint;
		CRubyPath     m_pathPivot1;
		CRubyPath     m_pathPivot2;
		CRubyGdiList  m_listPoint;
		CRubyGdiList  m_listPivot1;
		CRubyGdiList  m_listPivot2;

		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);

		// Scaling
		number GetAngle(CCodedItem *pValue, C3REAL Default);

		// Path Preparation
		void PrepareTicks(void);
		void PreparePoint(void);
		BOOL PrepareBand(CRubyPath &path, BOOL fShow, CCodedItem *pMin, CCodedItem *pMax);
		BOOL PrepareBug(CRubyPath &path, BOOL fShow, CCodedItem *pValue);

		// Drawing Helpers
		void Ring(CRubyPath &path, number r1, number r2);
		void Semi(CRubyPath &path, number rb, number r1);
		void Semi(CRubyPath &path, number rb, number r1, number r2);
		void Quad(CRubyPath &path, number rb, number r1);
		void Quad(CRubyPath &path, number rb, number r1, number r2);
		void Rect(CRubyPath &path, number rb, number r1);
		void Rect(CRubyPath &path, number rb, number r1, number r2);
		void Half(CRubyPath &path, number rb, number r1);
		void Half(CRubyPath &path, number rb, number r1, number r2);

		// Color Schemes
		COLOR GetColor(UINT n);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void CalcRadii(void);
	};

// End of File

#endif
