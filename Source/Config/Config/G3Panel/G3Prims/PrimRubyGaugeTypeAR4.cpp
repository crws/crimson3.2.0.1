
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Semi Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR4, CPrimRubyGaugeTypeAR);

// Constructor

CPrimRubyGaugeTypeAR4::CPrimRubyGaugeTypeAR4(void)
{
	m_lineFact = 0.7;
	}

// Overridables

void CPrimRubyGaugeTypeAR4::SetInitState(void)
{
	CPrimRubyGaugeTypeAR::SetInitState();

	SetInitSize(300, 200);
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR4::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeAR::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR4::AddMetaData(void)
{
	CPrimRubyGaugeTypeAR::AddMetaData();

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE_5));
	}

// Path Management

void CPrimRubyGaugeTypeAR4::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeAR::MakePaths();

	m_bezelBase = 66;

	Semi(m_pathFace,  66, 67);

	Semi(m_pathOuter, 66, 80, 90);
	Semi(m_pathInner, 66, 70, 80);
	Semi(m_pathRing1, 66, 89, 91);
	Semi(m_pathRing2, 66, 79, 80);
	Semi(m_pathRing3, 66, 66, 70);
	Semi(m_pathRing4, 66, 74, 69);
	}

// Implementation

void CPrimRubyGaugeTypeAR4::LoadLayout(void)
{
	m_radiusPivot     =   8 * m_ScalePivot / 100.0;

	m_pointPivot.m_x  = 100;
	m_pointPivot.m_y  =  98 - m_radiusPivot;
	
	m_angleMin        = -90 - 90;
	m_angleMax        = +90 - 90;
	
	m_radiusOuter.m_x = 55;
	m_radiusOuter.m_y = 55;

	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBug.m_x   = m_PointMode ? 60 : 60;
	m_radiusMajor.m_x = m_PointMode ? 47 : 40;
	m_radiusMinor.m_x = m_PointMode ? 49 : 45;
	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBand.m_x  = m_PointMode ? 50 : 50;
	m_radiusPoint.m_x = m_PointMode ? 46 : 47;
	m_radiusSweep.m_x = m_PointMode ? 35 : 47;

	CalcRadii();
	}

// End of File
