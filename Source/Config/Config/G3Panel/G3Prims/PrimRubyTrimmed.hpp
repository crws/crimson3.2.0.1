
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyTrimmed_HPP
	
#define	INCLUDE_PrimRubyTrimmed_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Trimmed Rectangle Primitive
//

class CPrimRubyTrimmed : public CPrimRubyGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyTrimmed(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);
	
	protected:
		// Data Members
		int   m_style;
		UINT  m_Skip;
		CSize m_Corner;
	
		// Meta Data
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);
	};

// End of File

#endif
