
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyConicalTank_HPP
	
#define	INCLUDE_PrimRubyConicalTank_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Conical Tank Primitive
//

class CPrimRubyConicalTank : public CPrimRubyGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyConicalTank(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);

		// Data Members
		INT m_Base;
		INT m_Flat;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);
	};

// End of File

#endif
