
#include "intern.hpp"

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Geometric Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGeom, CPrimRubyWithText);

// Constructor

CPrimRubyGeom::CPrimRubyGeom(void)
{
	m_uType = 0x81;

	m_pFill = New CPrimRubyTankFill;

	m_pEdge = New CPrimRubyPenEdge;
	}

// UI Overridables

BOOL CPrimRubyGeom::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_EDGE),
				       AfxPointerClass(this),
				       2
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       3
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// Overridables

BOOL CPrimRubyGeom::HitTest(P2 Pos)
{
	if( CPrimRubyWithText::HitTest(Pos) ) {

		return TRUE;
		}

	if( PtInRect(m_bound, Pos) ) {

		if( !m_pFill->IsNull() ) {

			if( m_pathFill.HitTest(Pos, 0) ) {

				return TRUE;
				}
			}

		if( m_pathEdge.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathTrim.HitTest(Pos, 0) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

void CPrimRubyGeom::Draw(IGDI *pGDI, UINT uMode)
{
	m_pFill->Register(uMode);

	m_pEdge->Register(uMode);

	m_pFill->Fill(pGDI, m_listFill, !UseFastFill()),

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	m_pEdge->Trim(pGDI, m_listTrim, TRUE);

	CPrimRubyWithText::Draw(pGDI, uMode);
	}

void CPrimRubyGeom::GetRefs(CPrimRefList &Refs)
{
	CPrimRubyWithText::GetRefs(Refs);

	m_pFill->GetRefs(Refs);

	m_pEdge->GetRefs(Refs);
	}

BOOL CPrimRubyGeom::StepAddress(void)
{
	BOOL f1 = m_pFill->StepAddress();

	BOOL f2 = CPrimRubyWithText::StepAddress();

	return f1 || f2;
	}

// Download Support

BOOL CPrimRubyGeom::MakeInitData(CInitData &Init)
{
	CPrimRubyWithText::MakeInitData(Init);

	Init.AddByte(BYTE(UseFastFill()));

	Init.AddItem(itemSimple, m_pFill);
	Init.AddItem(itemSimple, m_pEdge);

	AddList(Init, m_listFill);
	AddList(Init, m_listEdge);
	AddList(Init, m_listTrim);

	return TRUE;
	}

// Meta Data

void CPrimRubyGeom::AddMetaData(void)
{
	CPrimRubyWithText::AddMetaData();

	Meta_AddObject(Fill);
	Meta_AddObject(Edge);

	Meta_SetName(IDS_GEOMETRIC);
	}

// Fast Fill Control

BOOL CPrimRubyGeom::UseFastFill(void)
{
	if( m_pEdge->m_Width ) {

		if( m_pEdge->m_Width > 1 ) {

			// It would be really nice to fix this... !!!!

			if( m_pEdge->m_Edge == CRubyStroker::edgeInner ) {

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Path Management

void CPrimRubyGeom::InitPaths(void)
{
	m_pathFill.Empty();

	m_pathEdge.Empty();

	m_pathTrim.Empty();
	}

void CPrimRubyGeom::MakePaths(void)
{
	m_pEdge->StrokeEdge(m_pathEdge, m_pathFill);

	m_pEdge->StrokeTrim(m_pathTrim, m_pathEdge);

	m_pathFill.GetBoundingRect(m_bound);

	m_pathEdge.AddBoundingRect(m_bound);

	m_pathTrim.AddBoundingRect(m_bound);
	}

void CPrimRubyGeom::MakeLists(void)
{
	m_listFill.Load(m_pathFill, !UseFastFill());

	m_listEdge.Load(m_pathEdge, true);

	m_listTrim.Load(m_pathTrim, true);
	}

// End of File
