
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_COLEXPR_HPP
	
#define	INCLUDE_COLEXPR_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CColorExprItem;
class CColPick2Item;
class CColPick4Item;
class CColBlendItem;
class CColFlashItem;

//////////////////////////////////////////////////////////////////////////
//
// Color Expression
//

class CColorExprItem : public CCodedHost
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColorExprItem(void);

		// Overridables
		virtual CString GetExpr(void);
		virtual BOOL    SetExpr(CString Text);

	protected:
		// Implementation
		CString GetSource(CCodedItem *pItem, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// 2-State Color Expression
//

class CColPick2Item : public CColorExprItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColPick2Item(void);

		// Overridables
		CString GetExpr(void);
		BOOL    SetExpr(CString Text);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Public Data
		CCodedItem * m_pData;
		UINT         m_Col1;
		UINT         m_Col2;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 4-State Color Expression
//

class CColPick4Item : public CColorExprItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColPick4Item(void);

		// Overridables
		CString GetExpr(void);
		BOOL    SetExpr(CString Text);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Public Data
		CCodedItem * m_pData1;
		CCodedItem * m_pData2;
		UINT         m_Col1;
		UINT         m_Col2;
		UINT         m_Col3;
		UINT         m_Col4;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Blended Color Expression
//

class CColBlendItem : public CColorExprItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColBlendItem(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		CString GetExpr(void);
		BOOL    SetExpr(CString Text);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Public Data
		CCodedItem * m_pData;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		UINT         m_Col1;
		UINT         m_Col2;

	protected:
		// Data Members
		BOOL m_fAutoLimits;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL HasAutoLimits(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Flashing Color Expression
//

class CColFlashItem : public CColorExprItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CColFlashItem(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		CString GetExpr(void);
		BOOL    SetExpr(CString Text);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Public Data
		CCodedItem * m_pFlash;
		CCodedItem * m_pRate;
		UINT         m_Col1;
		UINT         m_Col2;
		UINT         m_Col3;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
