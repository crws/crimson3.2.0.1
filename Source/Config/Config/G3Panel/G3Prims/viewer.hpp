
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_VIEWER_HPP
	
#define	INCLUDE_VIEWER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimViewer;
class CPrimAlarmViewer;
class CPrimEventViewer;
class CPrimFileViewer;
class CPrimTrendViewer;
class CPrimUserManager;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Generic Viewer
//

class CPrimViewer : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimViewer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimColor * m_pBack;
		UINT         m_FontWork;
		UINT         m_FontMenu;

	protected:
		// Data Members
		CStringArray m_List;
		CString      m_Work;

		// Meta Data
		void AddMetaData(void);

		// Overridables
		virtual BOOL MakeList(void);

		// Drawing Helpers
		int  GetButtonHeight(IGDI *pGDI);
		void DrawButton(IGDI *pGDI, R2 Rect, CString Text);
		void FindPoints(P2 *t, P2 *b, R2 &r, int s);

		// Implementation
		void DoEnables(IUIHost *pHost);
		BOOL SelectFont(IGDI *pGDI, UINT uFont);
		void DrawNotSupported(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Alarm Viewer
//

class CPrimAlarmViewer : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimAlarmViewer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	      m_IncTime;
		UINT	      m_IncMarks;
		UINT	      m_IncNum;
		UINT	      m_ColInactive;
		UINT	      m_ColActive;
		UINT	      m_ColAccepted;
		UINT	      m_ColWaitAcc;
		CCodedText  * m_pTxtEmpty;
		CCodedText  * m_pBtnPgUp;
		CCodedText  * m_pBtnPgDn;
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnMute;
		CCodedText  * m_pBtnAccept;
		CCodedText  * m_pBtnHelp;
		UINT          m_ShowPage;
		UINT          m_ShowMute;
		UINT          m_ShowHelp;
		UINT          m_ShowAccept;
		CCodedItem  * m_pOnHelp;
		CDispFormat * m_pFormat;
		UINT	      m_Reverse;
		UINT	      m_AutoScroll;
		UINT	      m_UsePriority;
		UINT	      m_ColPriority[8];

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Event Viewer
//

class CPrimEventViewer : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimEventViewer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	      m_IncTime;
		UINT	      m_IncMarks;
		UINT          m_IncNum;
		UINT	      m_IncType;
		UINT	      m_ColEmpty;
		UINT	      m_ColEvent;
		UINT	      m_ColAlarm;
		UINT	      m_ColAccept;
		UINT	      m_ColClear;
		UINT          m_AutoScroll;
		CCodedText  * m_pTxtEmpty;
		CCodedText  * m_pBtnPgUp;
		CCodedText  * m_pBtnPgDn;
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnClear;
		UINT          m_ShowPage;
		UINT          m_ShowClear;
		CCodedItem  * m_pUseClear;
		CDispFormat * m_pFormat;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- File Viewer
//

class CPrimFileViewer : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimFileViewer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedText * m_pRoot;
		UINT         m_FontText;
		UINT	     m_Number;
		UINT         m_Sort;
		UINT	     m_IncCSV;
		UINT	     m_IncTXT;
		UINT	     m_IncLOG;
		CCodedText * m_pTxtEmpty;
		CCodedText * m_pTxtCannot;
		CCodedText * m_pTxtEnd;
		CCodedText * m_pBtnDown;
		CCodedText * m_pBtnUp;
		CCodedText * m_pBtnPrev;
		CCodedText * m_pBtnNext;
		CCodedText * m_pBtnScan;
		CCodedText * m_pBtnLeft;
		CCodedText * m_pBtnRight;
		UINT         m_ShowScan;
		UINT         m_ShowLeft;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);
		BOOL IsSupported(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- User Manager
//

class CPrimUserManager : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimUserManager(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnPass;
		UINT	      m_AutoScroll;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Trend Viewer
//

class CPrimTrendViewer : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimTrendViewer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem  * m_pLog;
		UINT	      m_Width;
		CCodedItem  * m_pPenMask;
		CCodedItem  * m_pFillMask;
		UINT	      m_PenWeight;
		UINT          m_ShowData;
		UINT          m_ShowCursor;
		UINT          m_DataBox;
		BOOL          m_fUseFill;
		UINT	      m_FontTitle;
		UINT	      m_FontData;
		UINT	      m_GridTime;
		UINT	      m_GridMode;
		UINT	      m_GridMinor;
		UINT	      m_GridMajor;
		CCodedItem  * m_pGridMin;
		CCodedItem  * m_pGridMax;
		CCodedItem  * m_pBars;
		UINT	      m_Precise;
		CPrimColor  * m_pColTitle;
		CPrimColor  * m_pColLabel;
		CPrimColor  * m_pColData;
		CPrimColor  * m_pColMajor;
		CPrimColor  * m_pColMinor;
		CPrimColor  * m_pColCursor;
		CPrimColor  * m_pColPen[16];
		CPrimColor  * m_pColFill[16];
		CCodedText  * m_pBtnPgLeft;
		CCodedText  * m_pBtnLeft;
		CCodedText  * m_pBtnLive;
		CCodedText  * m_pBtnRight;
		CCodedText  * m_pBtnPgRight;
		CCodedText  * m_pBtnIn;
		CCodedText  * m_pBtnOut;
		CCodedText  * m_pBtnLoad;
		CDispFormat * m_pFormat;
		UINT	      m_fUseLoad;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		// Implementation
		void AddLogIndex(CInitData &Init, CCodedItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimTouchCalib;
class CPrimTouchTester;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Calibration
//

class CPrimTouchCalib : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimTouchCalib(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pOnFailure;
		CCodedItem * m_pOnSuccess;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Tester
//

class CPrimTouchTester : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimTouchTester(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimColor * m_pBack;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
