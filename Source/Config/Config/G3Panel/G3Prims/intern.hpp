
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3prims.hpp"

#include "gdiplus.hpp"

#include "intern.hxx"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Helper Functions
//

#define MulDivRound(a, b, c)	(((a)*(b)+(c)/2)/(c))

#define sgn(x)			((x)?(((x)<0)?-1:+1):0)

// End of File

#endif
