
#include "intern.hpp"

#include "PrimRubyGaugeTypeALH.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Horizontal Linear Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeALH, CPrimRubyGaugeTypeAL);

// Constructor

CPrimRubyGaugeTypeALH::CPrimRubyGaugeTypeALH(void)
{
	m_Orient = 3;
	}

// Overridables

void CPrimRubyGaugeTypeALH::SetInitState(void)
{
	CPrimRubyGaugeTypeAL::SetInitState();

	SetInitSize(300, 120);
	}

// Meta Data Creation

void CPrimRubyGaugeTypeALH::AddMetaData(void)
{
	CPrimRubyGaugeTypeAL::AddMetaData();

	Meta_SetName((IDS_TYPE_HORIZONTAL));
	}

// End of File
