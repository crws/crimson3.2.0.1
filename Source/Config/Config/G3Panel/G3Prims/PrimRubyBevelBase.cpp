
#include "intern.hpp"

#include "PrimRubyBevelBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyBrush.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby BevelBase Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyBevelBase, CPrimRubyWithText);

// Constructor

CPrimRubyBevelBase::CPrimRubyBevelBase(void)
{
	m_pFill   = New CPrimRubyBrush;

	m_pEdge   = New CPrimRubyPenEdge;

	m_pHilite = New CPrimColor;

	m_pShadow = New CPrimColor;

	m_Border  = 1000;

	m_Style   = 0;
	}

// UI Overridables

BOOL CPrimRubyBevelBase::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_LINE_2),
				       AfxPointerClass(this),
				       2
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       3
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// Overridables

BOOL CPrimRubyBevelBase::HitTest(P2 Pos)
{
	if( CPrimRubyWithText::HitTest(Pos) ) {

		return TRUE;
		}

	if( PtInRect(m_bound, Pos) ) {

		if( !m_pFill->IsNull() ) {

			if( m_pathFill.HitTest(Pos, 0) ) {

				return TRUE;
				}
			}

		if( m_pathHilite.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathShadow.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathEdge.HitTest(Pos, 0) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

void CPrimRubyBevelBase::Draw(IGDI *pGDI, UINT uMode)
{
	m_pFill->Register(uMode);

	m_pEdge->Register(uMode);

	CRubyGdiLink link(pGDI);

	if( m_Style == 0 ) {

		link.OutputSolid(m_listHilite, m_pHilite->GetColor(), 0);

		link.OutputSolid(m_listShadow, m_pShadow->GetColor(), 0);
		}

	if( m_Style == 1 ) {

		link.OutputSolid(m_listHilite, m_pShadow->GetColor(), 0);

		link.OutputSolid(m_listShadow, m_pHilite->GetColor(), 0);
		}

	m_pFill->Fill(pGDI, m_listFill, TRUE);

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	CPrimRubyWithText::Draw(pGDI, uMode);
	}

void CPrimRubyBevelBase::GetRefs(CPrimRefList &Refs)
{
	CPrimRubyWithText::GetRefs(Refs);

	m_pFill->GetRefs(Refs);

	m_pEdge->GetRefs(Refs);
	}

void CPrimRubyBevelBase::SetInitState(void)
{
	CPrimRubyWithText::SetInitState();

	m_pFill  ->Set(GetRGB(12,12,24));

	m_pHilite->Set(GetRGB(18,18,30));

	m_pShadow->Set(GetRGB( 6, 6,16));

	m_pEdge  ->Set(GetRGB( 4, 4,12));
	
	m_pEdge->m_Join = 0;
	}

void CPrimRubyBevelBase::UpdateLayout(void)
{
	number cx = m_RealRect.m_x2 - m_RealRect.m_x1;

	number cy = m_RealRect.m_y2 - m_RealRect.m_y1;

	m_BordMin = 200;

	m_BordMax = int(5000 * min(cx, cy) / cx);

	MakeMin(m_Border, m_BordMax);

	MakeMax(m_Border, m_BordMin);

	CPrimRubyWithText::UpdateLayout();
	}

void CPrimRubyBevelBase::FindTextRect(void)
{
	m_TextRect  = m_DrawRect;

	int nBorder = (m_TextRect.x2 - m_TextRect.x1) * m_Border / 10000;

	int nAdjust = m_pEdge->GetInnerWidth() + nBorder;

	DeflateRect(m_TextRect, nAdjust, nAdjust);
	}

BOOL CPrimRubyBevelBase::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		UINT uInset = 0;

		Hand.m_pTag = L"Border";

		Hand.m_uRef  = 100;
		Hand.m_Pos.x = m_Border;
		Hand.m_Pos.y = uInset;
		Hand.m_Clip  = CRect(m_BordMin, uInset, m_BordMax, uInset);

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CPrimRubyBevelBase::MakeInitData(CInitData &Init)
{
	CPrimRubyWithText::MakeInitData(Init);

	Init.AddByte(BYTE(m_Style));

	Init.AddItem(itemSimple, m_pFill);
	Init.AddItem(itemSimple, m_pEdge);
	Init.AddItem(itemSimple, m_pHilite);
	Init.AddItem(itemSimple, m_pShadow);

	AddList(Init, m_listFill);
	AddList(Init, m_listEdge);
	AddList(Init, m_listHilite);
	AddList(Init, m_listShadow);

	return TRUE;
	}

// Meta Data

void CPrimRubyBevelBase::AddMetaData(void)
{
	CPrimRubyWithText::AddMetaData();

	Meta_AddObject (Fill);
	Meta_AddObject (Edge);
	Meta_AddObject (Hilite);
	Meta_AddObject (Shadow);
	Meta_AddInteger(Border);
	Meta_AddInteger(Style);

	Meta_SetName((IDS_BEVEL_BASE));
	}

// Path Management

void CPrimRubyBevelBase::InitPaths(void)
{
	m_pathFill.Empty();

	m_pathEdge.Empty();

	m_pathHilite.Empty();

	m_pathShadow.Empty();
	}

void CPrimRubyBevelBase::MakePaths(void)
{
	R2 OuterRect = m_DrawRect;

	R2 InnerRect = OuterRect;

	int cx = OuterRect.x2 - OuterRect.x1;

	int bb = cx * m_Border / 10000;

	DeflateRect(InnerRect, bb, bb);

	CRubyPoint p1(OuterRect, 1);
	CRubyPoint p2(OuterRect, 3);
	CRubyPoint w1(InnerRect, 1);
	CRubyPoint w2(InnerRect, 3);

	CRubyDraw::Rectangle(m_pathFill, w1, w2);

	////////

	m_pathHilite.Append(CRubyPoint(p1.m_x, p1.m_y));
	m_pathHilite.Append(CRubyPoint(w1.m_x, w1.m_y));
	m_pathHilite.Append(CRubyPoint(w1.m_x, w2.m_y));
	m_pathHilite.Append(CRubyPoint(p1.m_x, p2.m_y));
	
	m_pathHilite.AppendHardBreak();

	m_pathHilite.Append(CRubyPoint(p1.m_x, p1.m_y));
	m_pathHilite.Append(CRubyPoint(p2.m_x, p1.m_y));
	m_pathHilite.Append(CRubyPoint(w2.m_x, w1.m_y));
	m_pathHilite.Append(CRubyPoint(w1.m_x, w1.m_y));
	
	m_pathHilite.AppendHardBreak();

	////////

	m_pathShadow.Append(CRubyPoint(p2.m_x, p1.m_y));
	m_pathShadow.Append(CRubyPoint(p2.m_x, p2.m_y));
	m_pathShadow.Append(CRubyPoint(w2.m_x, w2.m_y));
	m_pathShadow.Append(CRubyPoint(w2.m_x, w1.m_y));
	
	m_pathShadow.AppendHardBreak();

	m_pathShadow.Append(CRubyPoint(p2.m_x, p2.m_y));
	m_pathShadow.Append(CRubyPoint(p1.m_x, p2.m_y));
	m_pathShadow.Append(CRubyPoint(w1.m_x, w2.m_y));
	m_pathShadow.Append(CRubyPoint(w2.m_x, w2.m_y));
	
	m_pathShadow.AppendHardBreak();

	////////

	m_pEdge->StrokeExact(m_pathEdge, m_pathHilite);

	m_pEdge->StrokeExact(m_pathEdge, m_pathShadow);

	////////

	m_pathFill.GetBoundingRect  (m_bound);

	m_pathHilite.AddBoundingRect(m_bound);

	m_pathShadow.AddBoundingRect(m_bound);

	m_pathEdge.AddBoundingRect  (m_bound);
	}

void CPrimRubyBevelBase::MakeLists(void)
{
	m_listFill.Load  (m_pathFill,   true);

	m_listEdge.Load  (m_pathEdge,   true);

	m_listHilite.Load(m_pathHilite, true);

	m_listShadow.Load(m_pathShadow, true);
	}

// End of File
