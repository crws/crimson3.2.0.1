
#include "intern.hpp"

#include "PrimRubyPolygon3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 3-Sided Polygon Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPolygon3, CPrimRubyPolygon)

// Constructor

CPrimRubyPolygon3::CPrimRubyPolygon3(void)
{
	m_nSides = 3;

	m_Rotate = -900;
	}

// Meta Data

void CPrimRubyPolygon3::AddMetaData(void)
{
	CPrimRubyPolygon::AddMetaData();

	Meta_SetName(IDS_TRIANGLE_2);
	}

// End of File
