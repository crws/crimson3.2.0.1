
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Event Map
//

// Dynamic Class

AfxImplementDynamicClass(CEventMap, CItemList);

// Constructor

CEventMap::CEventMap(void)
{
	m_Class = AfxRuntimeClass(CEventEntry);
	}

// Destructor

CEventMap::~CEventMap(void)
{
	}

// Item Location

CEventEntry * CEventMap::GetItem(INDEX Index) const
{
	return (CEventEntry *) CItemList::GetItem(Index);
	}

// Attributes

BOOL CEventMap::HasEntry(UINT uCode) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEventEntry *pEvent = GetItem(n);

		if( pEvent->m_Code == uCode ) {

			return TRUE;
			}

		GetNext(n);
		}

	return FALSE;
	}

BOOL CEventMap::IsBroken(UINT uCode) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEventEntry *pEvent = GetItem(n);

		if( pEvent->m_Code == uCode ) {

			return pEvent->IsBroken();
			}

		GetNext(n);
		}

	return FALSE;
	}

CEventEntry * CEventMap::GetEntry(UINT uCode) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEventEntry *pEvent = GetItem(n);

		if( pEvent->m_Code == uCode ) {

			return pEvent;
			}

		GetNext(n);
		}

	return NULL;
	}

// Operations

void CEventMap::ForceEntry(UINT uCode)
{
	CEventEntry *pEvent = GetEntry(uCode);

	if( !pEvent ) {

		pEvent = New CEventEntry;

		pEvent->m_Code = uCode;

		AppendItem(pEvent);
		}
	}

void CEventMap::CleanEntry(UINT uCode)
{
	CEventEntry *pEvent = GetEntry(uCode);

	if( pEvent ) {
		
		if( pEvent->IsNull() ) {

			DeleteItem(pEvent);
			}
		}
	}

void CEventMap::Validate(BOOL fExpand)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEventEntry *pEvent = GetItem(n);

		pEvent->Validate(fExpand);

		GetNext(n);
		}
	}

void CEventMap::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEventEntry *pEvent = GetItem(n);

		pEvent->TagCheck(Done, Tags);

		GetNext(n);
		}
	}

void CEventMap::PostConvert(void)
{
	CUISystem *pSystem  = (CUISystem *) m_pDbase->GetSystemItem();

	UINT	   Keys     = pSystem->m_SoftKeys;

	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEventEntry *pEvent = GetItem(n);

		BOOL         fKeep  = TRUE;

		if( m_pDbase->HasFlag(L"IconLedsOnly") ) {
			
			fKeep = FALSE;
			}

		if( pEvent->m_Code >= 0x80 + Keys && pEvent->m_Code <= 0x8F ) {

			fKeep = FALSE;
			}

		if( pEvent->m_Code == 0xA2 && Keys == 0 ) {

			fKeep = FALSE;
			}

		if( !fKeep ) {

			INDEX d = n;

			GetNext(n);

			DeleteItem(d);

			continue;
			}

		GetNext(n);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Event Entry
//

// Dynamic Class

AfxImplementDynamicClass(CEventEntry, CPrimAction);

// Constructor

CEventEntry::CEventEntry(void)
{
	m_Code   = 0;

	m_fTouch = FALSE;
	}

// Destructor

CEventEntry::~CEventEntry(void)
{
	}

// Download Support

BOOL CEventEntry::MakeInitData(CInitData &Init)
{
	CPrimAction::MakeInitData(Init);

	Init.AddWord(WORD(m_Code));

	return TRUE;
	}

// Meta Data Creation

void CEventEntry::AddMetaData(void)
{
	CPrimAction::AddMetaData();

	Meta_AddInteger(Code);

	Meta_SetName((IDS_EVENT_ENTRY));
	}

//////////////////////////////////////////////////////////////////////////
//
// Dual Event Wrapper
//

// Dynamic Class

AfxImplementDynamicClass(CDualEvent, CUIItem);

// Constructor

CDualEvent::CDualEvent(void)
{
	m_pGlobal = NULL;

	m_pLocal  = NULL;
	}

// Destructor

CDualEvent::~CDualEvent(void)
{
	m_pList = NULL;
	}

// UI Creation

BOOL CDualEvent::OnLoadPages(CUIPageList *pList)
{			 
	m_pLocal ->LoadPage(pList, CString(IDS_LOCAL));

	m_pGlobal->LoadPage(pList, CString(IDS_GLOBAL));
	
	return TRUE;
	}

// Operations

void CDualEvent::SimpleInit(void)
{
	AddMeta();
	}

// Meta Data Creation

void CDualEvent::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddObject(Global);
	Meta_AddObject(Local);
	}

// End of File
