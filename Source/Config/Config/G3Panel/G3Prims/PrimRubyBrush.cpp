
#include "intern.hpp"

#include "PrimRubyBrush.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Brush
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyBrush, CCodedHost);

// Constructor

CPrimRubyBrush::CPrimRubyBrush(void)
{
	m_Pattern = brushFore;

	m_pColor1 = New CPrimColor;

	m_pColor2 = New CPrimColor;
	}

// UI Managament

void CPrimRubyBrush::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "Pattern" ) {

			DoEnables(pHost);
			}

		if( Tag == "Color1" ) {

			pHost->UpdateUI(this, "Pattern");
			}

		if( Tag == "Color2" ) {

			pHost->UpdateUI(this, "Pattern");
			}

		if( !Tag.IsEmpty() ) {

			CPrim *pPrim = (CPrim *) GetParent(AfxRuntimeClass(CPrim));

			pPrim->OnUIChange(pHost, pItem, L"PrimProp");
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimRubyBrush::IsNull(void) const
{
	return m_Pattern == brushNull;
	}

// Operations

void CPrimRubyBrush::Set(COLOR Color)
{
	if( Color == GetRGB(31,31,31) ) {

		m_pColor1->Set(Color);

		m_pColor2->Set(GetRGB(0,0,0));
		}
	else {
		m_pColor1->Set(Color);

		m_pColor2->Set(GetRGB(31,31,31));
		}

	m_Pattern = brushFore;
	}

BOOL CPrimRubyBrush::Register(UINT uMode)
{
	if( uMode == drawWhole ) {

		if( m_Pattern >= 128 ) {

			CUISystem       *pSys = (CUISystem *) m_pDbase->GetSystemItem();

			CRubyPatternLib *pLib = pSys->m_pUI->m_pPatterns;

			pLib->Register(m_Pattern);

			return TRUE;
			}
		}

	return FALSE;
	}

void CPrimRubyBrush::GetRefs(CPrimRefList &Refs)
{
	if( m_Pattern >= 128 ) {

		CUISystem       *pSys = (CUISystem *) m_pDbase->GetSystemItem();

		CRubyPatternLib *pLib = pSys->m_pUI->m_pPatterns;

		pLib->GetRefs(Refs, m_Pattern);
		}
	}

// Drawing

BOOL CPrimRubyBrush::Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( !IsNull() ) {

		CRubyGdiLink link(pGdi);

		CUISystem       *pSys = (CUISystem *) m_pDbase->GetSystemItem();

		CRubyPatternLib *pLib = pSys->m_pUI->m_pPatterns;

		PSHADER pShader = pLib->SetGdi( pGdi,
						m_Pattern,
						m_pColor1->GetColor(),
						m_pColor2->GetColor()
						);

		if( fOver )
			link.OutputShade(list, pShader, 0);
		else
			link.OutputShade(list, pShader);

		return TRUE;
		}

	return FALSE;
	}

// Persistance

void CPrimRubyBrush::Init(void)
{
	CUIItem::Init();

	m_pColor1->Set(GetRGB(12,12,24));

	m_pColor2->Set(GetRGB(31,31,31));
	}

// Download Support

BOOL CPrimRubyBrush::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_Pattern));

	Init.AddItem(itemSimple, m_pColor1);

	Init.AddItem(itemSimple, m_pColor2);

	return TRUE;
	}

// Meta Data

void CPrimRubyBrush::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Pattern);
	Meta_AddObject (Color1);
	Meta_AddObject (Color2);

	Meta_SetName(IDS_BRUSH);
	}

// Implementation

void CPrimRubyBrush::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Color1", m_Pattern > 0 && m_Pattern < 128);

	pHost->EnableUI(this, "Color2", m_Pattern > 2 && m_Pattern < 128);
	}

// End of File
