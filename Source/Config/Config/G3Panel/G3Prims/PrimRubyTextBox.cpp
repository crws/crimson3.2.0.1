
#include "intern.hpp"

#include "PrimRubyTextBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Text Box Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyTextBox, CPrimRubyRect);

// Constructor

CPrimRubyTextBox::CPrimRubyTextBox(void)
{
	}

// Operations

void CPrimRubyTextBox::Set(CString Text, CSize MaxSize)
{
	// REV3 -- Allow tabs in pasted text.

	CStringArray Lines;

	Text.Replace('\t', ' ');

	Text.Replace('|',  '?');

	Text.Tokenize(Lines, L"\r\n");

	Text.Empty();

	int  cx = 0;

	UINT cl = Lines.GetCount();

	UINT n;

	for( n = 0; n < cl; n++ ) {

		CString Line = Lines[n];

		if( n < cl - 1 || !Line.IsEmpty() ) {

			if( !Text.IsEmpty() ) {

				Text += L'|';
				}

			Text += Line;
			}

		MakeMax(cx, m_pTextItem->GetTextWidth(Line) + 8);
		}

	if( cx <= 3 * MaxSize.cx / 4 ) {

		SetInitSize(cx, m_pTextItem->GetFontSize(n));

		UpdateLayout();
		}
	else {
		SetInitSize(MaxSize / 2 );

		UpdateLayout();
		}

	m_pTextItem->Set(Text, TRUE);
	}

// Overridables

void CPrimRubyTextBox::SetInitState(void)
{
	CPrimRubyRect::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pFill->m_Pattern = 0;

	AddText();

	m_pTextItem->m_Lead = 0;

	SetInitSize(54, m_pTextItem->GetLineSize());
	}

void CPrimRubyTextBox::FindTextRect(void)
{
	m_TextRect = m_DrawRect;

	int nAdjust = m_pEdge->GetInnerWidth();

	DeflateRect(m_TextRect, nAdjust, nAdjust);
	}

CSize CPrimRubyTextBox::GetMinSize(IGDI *pGDI)
{
	CSize Size(8, 8);

	if( m_pTextItem ) {
	
		Size.cy = m_pTextItem->GetLineSize();
		}

	return Size;
	}

// Meta Data

void CPrimRubyTextBox::AddMetaData(void)
{
	CPrimRubyRect::AddMetaData();

	Meta_SetName((IDS_TEXT_BOX_2));
	}

// End of File
