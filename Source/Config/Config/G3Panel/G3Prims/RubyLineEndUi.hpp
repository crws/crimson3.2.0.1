
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyLineEndUI_HPP
	
#define	INCLUDE_RubyLineEndUI_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUITextRubyLineEnd;
class CUIRubyLineEnd;
class CRubyLineEndComboBox;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element for Ruby Line End
//

class CUITextRubyLineEnd : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextRubyLineEnd(void);

	protected:
		// Overridables
		void OnBind(void);

		// Implementation
		void AddItem(UINT Code, CString Name);
		void AddData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element for Ruby Line End
//

class CUIRubyLineEnd : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIRubyLineEnd(void);

	protected:
		// Data Members
		CString	               m_Label;
		CLayItemText         * m_pTextLayout;
		CLayItem             * m_pDataLayout;
		CStatic	             * m_pTextCtrl;
		CRubyLineEndComboBox * m_pDataCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Implementation
		CRect FindComboRect(void);
		void  LoadList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ruby Line End Combo Box
//

class CRubyLineEndComboBox : public CDropComboBox
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRubyLineEndComboBox(CUIElement *pUI);

		// Destructor
		~CRubyLineEndComboBox(void);

		// Operations
		void LoadList(void);

	protected:
		// Data Members
		IGdiWindows * m_pWin;
		IGdi        * m_pGdi;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);
		void OnChar(UINT uCode, DWORD dwFlags);

		// Implementation
		void AddItem(UINT uCode, CString Name);
	};

// End of File

#endif
