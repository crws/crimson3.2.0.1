
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive List
//

// Runtime Class

AfxImplementRuntimeClass(CPrimList, CItemList);

// Constructor

CPrimList::CPrimList(void)
{
	}

// Item Access

CPrim * CPrimList::GetItem(INDEX Index) const
{
	return (CPrim *) CItemList::GetItem(Index);
	}

CPrim * CPrimList::GetItem(UINT uPos) const
{
	return (CPrim *) CItemList::GetItem(uPos);
	}

// List Operations

CPrim * CPrimList::RemoveItem(INDEX Index)
{
	if( !m_List.Failed(Index) ) {

		CPrim *pPrim = GetItem(Index);

		FreeIndex(pPrim, Index);

		m_List.Remove(Index);

		SetDirty();

		return pPrim;
		}

	return NULL;
	}

CPrim * CPrimList::RemoveItem(CPrim *pPrim)
{
	INDEX Index = FindItemIndex(pPrim);

	return RemoveItem(Index);
	}

// Searching

INDEX CPrimList::FindFixedName(CString const &Fixed)
{
	// OPTIM -- Should we keep a dictionary?

	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		if( GetItem(Index)->GetFixedName() == Fixed ) {

			return Index;
			}

		GetNext(Index);
		}

	return Index;
	}

INDEX CPrimList::FindFixedPath(CString const &Fixed)
{
	// OPTIM -- Should we keep a dictionary?

	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		if( GetItem(Index)->GetFixedPath() == Fixed ) {

			return Index;
			}

		GetNext(Index);
		}

	return Index;
	}

// Attributes

CRect CPrimList::GetUsedRect(void) const
{
	CRect Rect;

	for( INDEX Index = GetHead(); !Failed(Index); GetNext(Index) ) {

		CPrim *pPrim = GetItem(Index);

		CRect  Work  = pPrim->GetNormRect();

		Rect |= Work;
		}

	return Rect;
	}

UINT CPrimList::GetItemIndex(CPrim const *pPrim) const
{
	UINT n = 1;

	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CPrim *pFind = GetItem(Index);

		if( pFind == pPrim ) {

			break;
			}

		if( AfxPointerClass(pFind) == AfxPointerClass(pPrim) ) {

			n++;
			}

		GetNext(Index);
		}

	return n;
	}

// Operations

void CPrimList::Draw(IGDI *pGDI, UINT uMode)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->Draw(pGDI, uMode);

		GetNext(Index);
		}
	}

void CPrimList::SizePrims(CRect const &OldRect, CRect const &NewRect)
{
	CPoint Step;

	BOOL   fSize = FALSE;

	if( NewRect.left == OldRect.left || NewRect.right == OldRect.right ) {

		if( NewRect.top == OldRect.top || NewRect.bottom == OldRect.bottom ) {

			CRect Used = GetUsedRect();

			if( !NewRect.Encloses(Used) ) {

				if( NewRect.left > Used.left ) {

					Step.x += NewRect.left - Used.left;
					}

				if( NewRect.right < Used.right ) {

					Step.x += NewRect.right - Used.right;
					}

				if( NewRect.top > Used.top ) {

					Step.y += NewRect.top - Used.top;
					}

				if( NewRect.bottom < Used.bottom ) {

					Step.y += NewRect.bottom - Used.bottom;
					}
				}

			fSize = TRUE;
			}
		}

	if( !fSize ) {
		
		Step = NewRect.GetTopLeft() - OldRect.GetTopLeft();
		}

	if( Step.x || Step.y ) {

		for( INDEX Index = GetHead(); !Failed(Index); GetNext(Index) ) {

			CPrim * pPrim = GetItem(Index);

			CRect PrimOld = pPrim->GetRect();

			CRect PrimNew = PrimOld + Step;

			pPrim->SetRect(PrimOld, PrimNew);
			}
		}
	}

void CPrimList::Validate(BOOL fExpand)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->Validate(fExpand);

		GetNext(Index);
		}
	}

void CPrimList::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->TagCheck(Done, Tags);

		GetNext(Index);
		}
	}

void CPrimList::GetRefs(CPrimRefList &Refs)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->GetRefs(Refs);

		GetNext(Index);
		}
	}

void CPrimList::EditRef(UINT uOld, UINT uNew)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->EditRef(uOld, uNew);

		GetNext(Index);
		}
	}

void CPrimList::PostConvert(void)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->PostConvert();

		GetNext(Index);
		}
	}

// End of File
