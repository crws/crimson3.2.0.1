
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyStar8_HPP
	
#define	INCLUDE_PrimRubyStar8_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyStar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 8-Pointed Star Primitive
//

class CPrimRubyStar8 : public CPrimRubyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyStar8(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
