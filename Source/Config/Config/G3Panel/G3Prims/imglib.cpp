
#include "intern.hpp"

#include "resview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Image Resource Window
//

static int const xScroll = 18;

static int const yLine   = 24;

static int const yTitle  = 24;

// Dynamic Class

AfxImplementDynamicClass(CImageResourceWnd, CViewWnd);

// Static Data

UINT CImageResourceWnd::m_timerCheck   = CWnd::AllocTimerID();

UINT CImageResourceWnd::m_timerPreview = CWnd::AllocTimerID();

UINT CImageResourceWnd::m_timerQuick   = CWnd::AllocTimerID();

// Constructor

CImageResourceWnd::CImageResourceWnd(void)
{
	m_uMode       = NOTHING;

	m_uImageCount = 0;

	m_uZoom       = 2;

	m_fZoom       = TRUE;

	m_LargeSize   = CSize(129, 129);

	m_CatIcon     = 0x20000009;

	m_fShowTip    = TRUE;

	m_pToolTip    = NULL;

	m_uHover      = NOTHING;

	m_fQuick      = FALSE;

	m_fPress      = FALSE;

	m_pScroll     = New CScrollBar;

	m_nTopPixel   = 0;

	m_nTopSave    = 0;

	m_CopyCursor.Create(L"CopyCursor");

	m_LinkCursor.Create(L"LinkCursor");
	}

// Destructor

CImageResourceWnd::~CImageResourceWnd(void)
{
	}

// IUnknown

HRESULT CImageResourceWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropSource ) {

			*ppObject = (IDropSource *) this;

			return S_OK;
			}

		if( iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CImageResourceWnd::AddRef(void)
{
	return 1;
	}

ULONG CImageResourceWnd::Release(void)
{
	return 1;
	}

// IDropSource

HRESULT CImageResourceWnd::QueryContinueDrag(BOOL fEscape, DWORD dwKeys)
{
	if( !fEscape ) {

		if( dwKeys & MK_RBUTTON ) {

			return DRAGDROP_S_CANCEL;
			}

		if( dwKeys & MK_LBUTTON ) {

			return S_OK;
			}

		return DRAGDROP_S_DROP;
		}

	return DRAGDROP_S_CANCEL;
	}
        
HRESULT CImageResourceWnd::GiveFeedback(DWORD dwEffect)
{
	if( dwEffect == DROPEFFECT_LINK ) {

		SetCursor(m_LinkCursor);
		
		return S_OK;
		}

	if( dwEffect == DROPEFFECT_COPY ) {

		SetCursor(m_CopyCursor);

		return S_OK;
		}

	SetCursor(LoadCursor(NULL, IDC_NO));

	return S_OK;
	}

// Message Map

AfxMessageMap(CImageResourceWnd, CViewWnd)
{
	// LATER -- We don't handle any keyboard messages
	// and don't really have the concept of a current
	// position beyond the mouse hover. Do we need to
	// support the keyboard? If you're editing, you're
	// using the mouse, but for the category view it
	// might be nice to support up / down etc. I also
	// don't like the way scrolling is implemented,
	// and the way it moves when switching modes.

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_VSCROLL)

	AfxDispatchNotify(0, TTN_SHOW,      OnToolShow)
	AfxDispatchNotify(0, TTN_POP,	    OnToolPop )
	AfxDispatchNotify(0, NM_CUSTOMDRAW, OnToolDraw)

	AfxDispatchControlType(IDM_RES, OnResControl)
	AfxDispatchCommandType(IDM_RES, OnResCommand)

	AfxMessageEnd(CImageResourceWnd)
	};

// Message Handlers

void CImageResourceWnd::OnPostCreate(void)
{
	OnBuildCatList();

	CreateScrollBar();

	PlaceScrollBar();

	SetMode(0);

	InitMonitor();
	}

void CImageResourceWnd::OnDestroy(void)
{
	StopMonitor();

	SetMode(NOTHING);
	}

void CImageResourceWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ImageResourceTool"));
		}
	}

BOOL CImageResourceWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	CRect Work = Rect;

	Work.bottom = Rect.top = Rect.top + yTitle;

	DC.FillRect (Work, CBrush(CColor(0xF0,0xF0,0xF0)));

	DC.FrameRect(Rect, afxBrush(WHITE));

	return TRUE;
	}

void CImageResourceWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect  = GetClientRect();

	Rect.bottom = Rect.top + yTitle;

	CRect Text  = Rect;

	DC.Select(afxFont(Bolder));

	DC.SetBkMode(TRANSPARENT);

	if( m_pScroll->IsWindowVisible() ) {

		Text.right -= xScroll;
		}

	Text.left += 8;

	DC.DrawText( m_Title,
		     Text,
		     DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS | DT_NOPREFIX
		     );

	DC.Deselect();

	DC.ExcludeClipRect(Rect);

	switch( m_uMode ) {

		case 0:
			CatListDraw(DC, DC.GetPaintRect());
			break;

		case 1:
		case 2:
			ThumbsDraw(DC, DC.GetPaintRect());
			break;
		}
	}

void CImageResourceWnd::OnSize(UINT uCode, CSize Size)
{
	PlaceScrollBar();

	LayoutWindow();

	if( m_pScroll->IsWindowVisible() ) {

		int nMax = m_pScroll->GetRangeMax() - m_pScroll->GetPageSize();

		MakeMin(m_nTopPixel, nMax);

		m_pScroll->SetScrollPos(m_nTopPixel, TRUE);
		}
	}

void CImageResourceWnd::OnCancelMode(void)
{
	HideToolTip();

	KillTimer(m_timerPreview);

	KillTimer(m_timerCheck);
	}

void CImageResourceWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( m_uHover < NOTHING ) {

		HideToolTip();

		KillTimer(m_timerPreview);

		KillTimer(m_timerCheck);

		m_DragPos = Pos;

		m_fPress  = TRUE;

		m_uPress  = m_uHover;

		Invalidate(GetHoverRect(m_uPress), TRUE);

		SetCapture();
		}
	}

void CImageResourceWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_fPress ) {

		ReleaseCapture();

		m_fPress = FALSE;

		Invalidate(GetHoverRect(m_uPress), TRUE);

		if( m_uPress == m_uHover ) {

			if( m_uMode == 1 ) {

				if( OnZoomImage(m_uPress) ) {

					m_uImage = m_uPress;

					SetMode(2);
					}
				}

			if( m_uMode == 0 ) {

				m_uCat = m_uPress;

				SetMode(1);
				}
			}

		OnMouseMove(uFlags, Pos);
		}
	}

void CImageResourceWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	BOOL fDrag = FALSE;

	if( m_fPress && m_uMode ) {

		CPoint Step = m_DragPos - Pos;

		if( abs(Step.x) < 2 && abs(Step.y) < 2 ) {

			return;
			}

		ReleaseCapture();

		m_fPress = FALSE;

		afxMainWnd->PostMessage(WM_CANCELMODE);

		DragItem();

		fDrag = TRUE;

		Pos   = GetCursorPos();

		ScreenToClient(Pos);
		}

	if( m_uHover < NOTHING ) {

		CRect Rect = GetHoverRect(m_uHover);

		if( Rect.PtInRect(Pos) ) {

			return;
			}

		EndHover(!fDrag);

		Invalidate(Rect, TRUE);
		}

	if( IsWindowVisible() ) {

		UINT uPos = HitTestHover(Pos);

		if( uPos < NOTHING ) {

			CRect Rect = GetHoverRect(uPos);

			if( Rect.PtInRect(Pos) ) {

				Invalidate(Rect, TRUE);

				KillTimer (m_timerQuick);

				SetTimer  (m_timerPreview, m_fQuick ? 100 : 500);

				SetTimer  (m_timerCheck, 10);

				m_uHover = uPos;
				}
			}
		}
	}

void CImageResourceWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( m_uHover < NOTHING ) {

		if( uID == m_timerPreview ) {

			if( m_pToolTip && m_fShowTip ) {

				m_pToolTip->TrackActivate(TRUE, m_hWnd, 1);
				}

			KillTimer(uID);
			}

		if( uID == m_timerCheck ) {

			CPoint Pos = GetCursorPos();

			ScreenToClient(Pos);

			if( !GetClientRect().PtInRect(Pos) ) {

				EndHover(FALSE);
				}
			else
				SetTimer(m_timerCheck, 10);
			}
		}

	if( uID == m_timerQuick ) {

		m_fQuick = FALSE;

		KillTimer(uID);
		}
	}

void CImageResourceWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	if( m_pScroll->IsWindowVisible() ) {

		int nStep = m_uMode ? m_ImageSize.cy : yLine;

		int nPos  = m_nTopPixel - nStep * nDelta / 120;

		OnVScroll(SB_THUMBPOSITION, nPos, *m_pScroll);
		}
	}

void CImageResourceWnd::OnVScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( &Ctrl == m_pScroll ) {

		int nOld  = m_nTopPixel;

		int nNew  = nOld;

		int nMax  = m_pScroll->GetRangeMax() - m_pScroll->GetPageSize();

		int nLine = m_uMode ? m_ImageSize.cy : yLine;

		switch( uCode ) {

			case SB_LEFT:

				nNew = 0;

				break;

			case SB_RIGHT:

				nNew = nMax;

				break;

			case SB_LINELEFT:

				nNew -= nLine;
				
				break;

			case SB_LINERIGHT:

				nNew += nLine;
				
				break;

			case SB_PAGELEFT:

				nNew -= m_pScroll->GetPageSize();
				
				break;

			case SB_PAGERIGHT :

				nNew += m_pScroll->GetPageSize();
				
				break;

			case SB_THUMBPOSITION:
			case SB_THUMBTRACK:

				nNew = nPos;
				
				break;
			}
		
		MakeMax(nNew, 0);

		MakeMin(nNew, nMax);

		if( nNew - nOld ) {

			EndHover(TRUE);

			m_nTopPixel = nNew;

			m_pScroll->SetScrollPos(nNew, TRUE);

			Invalidate(TRUE);
			}
		}
	}

// Notification Handlers

UINT CImageResourceWnd::OnToolShow(UINT uID, NMHDR &Info)
{
	// LATER -- Add support for overlay tip to show the
	// entire cat name if it is too wide for the window.

	CRect  Rect = GetHoverRect(m_uHover);

	CPoint Pos  = Rect.GetCentre();

	CSize  Size = m_LargeSize + CSize(6, 24);

	CRect  Show = CRect(Pos.x - Size.cx, Pos.y, Pos.x, Pos.y + Size.cy);

	ClientToScreen(Show);

	m_pToolTip->MoveWindow(Show, FALSE);
	
	m_pToolTip->ShowWindow(SW_SHOWNA);

	return 1;
	}

UINT CImageResourceWnd::OnToolPop(UINT uID, NMHDR &Info)
{
	m_pToolTip->ShowWindow(SW_HIDE);

	return 0;
	}

UINT CImageResourceWnd::OnToolDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	// NOTE -- Behavior is different on Win2K.

	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		if( afxWin2K ) {

			return CDRF_NOTIFYPOSTPAINT;
			}

		DrawToolTip(Info);

		return CDRF_SKIPDEFAULT;
		}

	if( Info.dwDrawStage == CDDS_POSTPAINT ) {

		if( afxWin2K ) {

			DrawToolTip(Info);

			return CDRF_SKIPDEFAULT;
			}

		return CDRF_DODEFAULT;
		}

	return CDRF_DODEFAULT;
	}

// Command Handlers

BOOL CImageResourceWnd::OnResControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_RES_HOME:

			Src.EnableItem(m_uMode > 0);

			break;

		case IDM_RES_BACK:

			Src.EnableItem(m_uMode > 0);

			break;

		case IDM_RES_ZOOM_OUT:

			Src.EnableItem(m_uMode > 0 && m_fZoom && m_uZoom < 4);

			break;

		case IDM_RES_ZOOM_IN:

			Src.EnableItem(m_uMode > 0 && m_fZoom && m_uZoom > 0);

			break;

		case IDM_RES_NEXT_CAT:

			Src.EnableItem(m_uMode == 1 && m_uCat < m_CatName.GetCount() - 1);

			break;

		case IDM_RES_PREV_CAT:

			Src.EnableItem(m_uMode == 1 && m_uCat > 0);

			break;

		case IDM_RES_BALLOON:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_fShowTip);

			break;

		case IDM_RES_REFRESH:

			Src.EnableItem(TRUE);

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CImageResourceWnd::OnResCommand(UINT uID)
{
	switch( uID ) {

		case IDM_RES_HOME:

			SetMode(0);

			break;

		case IDM_RES_BACK:

			SetMode(m_uMode - 1);

			break;

		case IDM_RES_BALLOON:

			m_fShowTip = !m_fShowTip;

			break;

		case IDM_RES_ZOOM_OUT:

			SetZoom(m_uZoom + 1);

			break;

		case IDM_RES_ZOOM_IN:

			SetZoom(m_uZoom - 1);

			break;

		case IDM_RES_NEXT_CAT:

			SetMode(NOTHING);

			m_uCat = m_uCat + 1;

			SetMode(1);

			break;

		case IDM_RES_PREV_CAT:

			SetMode(NOTHING);

			m_uCat = m_uCat - 1;

			SetMode(1);

			break;

		case IDM_RES_REFRESH:

			OnRefresh();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Tool Tips

void CImageResourceWnd::CreateToolTip(void)
{
	m_pToolTip    = New CToolTip;

	DWORD dwStyle = TTS_ALWAYSTIP;

	HWND  hWnd    = afxMainWnd->GetHandle();

	m_pToolTip->Create(hWnd, dwStyle);
	}

void CImageResourceWnd::AddSingleTool(void)
{
	CToolInfo Info(m_hWnd, 1, CRect(0, 0, 1, 1), L"TIP", TTF_TRACK);

	m_pToolTip->AddTool(Info);
	}

void CImageResourceWnd::DrawToolTip(NMCUSTOMDRAW &Info)
{
	CWnd & Wnd  = CWnd::FromHandle(Info.hdr.hwndFrom);

	CRect  Rect = Wnd.GetClientRect();

	CRect  Text = Rect;

	CRect  Show = Rect;
	
	CDC DC(Info.hdc);

	DC.FrameRect(Rect--, afxBrush(WHITE));

	DC.FillRect (Rect--, afxBrush(BLACK));

	Text.bottom = Show.top = Show.top + 16;

	OnDrawImage(DC, Show - 4, m_uHover);

	DC.Select(afxFont(Bolder));

	DC.GradVert(Text - 1, afxColor(Blue1), afxColor(Blue3));

	DC.SetBkMode(TRANSPARENT);

	DC.DrawText( OnGetImageText(m_uHover),
		     Text - 2,
		     DT_SINGLELINE | DT_END_ELLIPSIS  | DT_NOPREFIX
		     );

	DC.Deselect();
	}

void CImageResourceWnd::HideToolTip(void)
{
	if( m_pToolTip ) {

		m_pToolTip->TrackActivate(FALSE, m_hWnd, 1);
		}
	}

void CImageResourceWnd::DestroyToolTip(void)
{
	m_pToolTip->DestroyWindow(TRUE);

	m_pToolTip = NULL;
	}

// Data Object Creation

BOOL CImageResourceWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject * pMake = New CDataObject;

	OnAddImageData(pMake, m_uHover);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

// Drag Support

void CImageResourceWnd::DragItem(void)
{
	IDataObject *pData = NULL;

	if( MakeDataObject(pData) ) {

		FindDragMetrics();

		MakeDragImage();

		CDragHelper *pHelp = New CDragHelper;

		pHelp->AddImage(pData, m_DragImage, m_DragSize, m_DragOffset);

		delete pHelp;

		DWORD dwAllow  = DROPEFFECT_LINK | DROPEFFECT_COPY;

		DWORD dwResult = DROPEFFECT_NONE;

		HideToolTip();

		DoDragDrop(pData, this, dwAllow, &dwResult);

		pData->Release();
		}
	}

void CImageResourceWnd::FindDragMetrics(void)
{
	CRect Rect   = GetHoverRect(m_uHover);

	ClientToScreen(Rect);

	m_DragPos    = GetCursorPos();

	m_DragSize   = CSize(128, 128);

	m_DragOffset = m_DragSize / 2;
	}

void CImageResourceWnd::MakeDragImage(void)
{
	CMemoryDC DC;

	CRect Rect  = m_DragSize;

	PBYTE pBits = NULL;

	m_DragImage.Create(m_DragSize, 1, 32, pBits);

	DC.Select(m_DragImage);

	if( OnGetAlpha(m_uHover) ) {

		DC.FillRect(Rect, afxBrush(MAGENTA));

		OnDrawImage(DC, Rect, m_uHover);

		m_DragImage.SetAlpha(pBits);
		}
	else
		OnDrawImage(DC, Rect, m_uHover);

	OnDrawImage(DC, Rect, m_uHover);
	
	DC.Deselect();
	}

// Folder Monitoring

BOOL CImageResourceWnd::InitMonitor(void)
{
	CStringArray List;

	if( OnMonitor(List) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CMonitor *pMonitor = New CMonitor(this, List[n]);

			pMonitor->Create();

			m_Monitor.Append(pMonitor);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CImageResourceWnd::StopMonitor(void)
{
	if( m_Monitor.GetCount() ) {

		for( UINT n = 0; n < m_Monitor.GetCount(); n++ ) {

			CMonitor *pMonitor = m_Monitor[n];

			pMonitor->Terminate(INFINITE);

			delete pMonitor;
			}

		return TRUE;
		}

	return FALSE;
	}

// Mode Control

void CImageResourceWnd::SetMode(UINT uMode)
{
	if( m_uMode - uMode ) {

		if( m_uMode == 1 && uMode == 2 ) {

			m_Title = OnGetImageText(m_uImage);
			}

		switch( m_uMode ) {

			case 0:
				CatListLeave();
				break;

			case 1:
			case 2:
				ThumbsLeave();
				break;
			}

		switch( m_uMode = uMode ) {

			case 0:
				CatListEnter();
				break;

			case 1:
			case 2:
				ThumbsEnter();
				break;
			}

		LayoutWindow();

		Invalidate(TRUE);
		}
	}

// Cat List Mode

void CImageResourceWnd::CatListEnter(void)
{
	m_Title     = CString(IDS_CATEGORIES);

	m_nTopPixel = m_nTopSave;

	m_pScroll->SetScrollPos(m_nTopPixel, TRUE);
	}

void CImageResourceWnd::CatListLeave(void)
{
	m_nTopSave = m_nTopPixel;
	}

void CImageResourceWnd::CatListDraw(CDC &DC, CRect Draw)
{
	// LATER -- Use a different icons for favorites,
	// and perhaps for the other categories, too?

	CRect View = GetClientRect() - 1;

	CRect Cell = View;

	UINT  uPos = m_nTopPixel / yLine;

	int   nTop = m_nTopPixel % yLine;

	Cell.top  -= nTop - yTitle;

	DC.Select(afxFont(Dialog));

	DC.SetBkMode(TRANSPARENT);

	while( uPos < m_CatName.GetCount() ) {

		Cell.bottom = Cell.top + yLine;

		DC.FillRect(Cell, afxBrush(WHITE));

		CRect Text   = Cell;

		CRect Icon   = Cell;

		UINT  uFlags = MF_POPUP;

		if( m_pScroll->IsWindowVisible() ) {

			Text.right -= xScroll;
			}

		if( m_fPress ) {
			
			if( uPos == m_uPress ) {

				DC.FrameRect(Text - 1, afxColor(3dShadow));

				if( uPos == m_uHover ) {

					DC.GradVert( Text - 2,
						     afxColor(NavBar1),
						     afxColor(NavBar2)
						     );
					}
				else {
					DC.GradVert( Text - 2,
						     afxColor(NavBar4),
						     afxColor(NavBar3)
						     );
					}

				uFlags |= MF_HILITE;
				}
			}
		else {
			if( uPos == m_uHover ) {

				DC.FrameRect( Text - 1,
					      afxColor(3dShadow)
					      );

				DC.GradVert( Text - 2,
					     afxColor(NavBar4),
					     afxColor(NavBar3)
					     );

				uFlags |= MF_HILITE;
				}
			}

		Text.left += 32;

		Icon.left += 4;
		
		Icon.right = Icon.left + yLine;

		DC.DrawText( m_CatName[uPos],
			     Text,
			     DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS | DT_NOPREFIX
			     );

		afxButton->Draw(DC, Icon, m_CatIcon, L"", uFlags);

		Cell.top = Cell.bottom;

		uPos++;
		}

	Cell.bottom = View.bottom;

	DC.FillRect(Cell, afxBrush(WHITE));

	DC.Deselect();
	}

void CImageResourceWnd::CatListLayout(void)
{
	CRect Client = GetClientRect() - 1;

	Client.top  += yTitle;

	int   nLast  = m_CatName.GetCount() * yLine;

	if( Client.cy() < nLast ) {

		m_pScroll->SetScrollRange(0, nLast, FALSE);

		m_pScroll->SetPageSize(Client.cy(), FALSE);

		m_pScroll->ShowWindow(SW_SHOW);

		m_pScroll->Invalidate(TRUE);

		MakeMin(m_nTopPixel, nLast);
		}
	else {
		m_pScroll->ShowWindow(SW_HIDE);

		m_nTopPixel = 0;
		}

	Invalidate(TRUE);
	}

UINT CImageResourceWnd::CatListHitTest(CPoint Pos)
{
	UINT nPos = (Pos.y - yTitle + m_nTopPixel) / yLine;

	if( nPos >= 0 ) {

		UINT uPos = nPos;

		if( uPos < m_CatName.GetCount() ) {

			return uPos;
			}
		}

	return NOTHING;
	}

CRect CImageResourceWnd::CatListGetRect(UINT uHover)
{
	CRect View  = GetClientRect() - 1;

	View.top   += yTitle;

	View.top   -= m_nTopPixel;

	View.top   += uHover * yLine;

	View.bottom = View.top + yLine;

	return View;
	}

// Thumbail Mode

void CImageResourceWnd::ThumbsEnter(void)
{
	afxThread->SetWaitMode(TRUE);

	OnLoadImages();

	m_pImageThumb = New CBitmap [ m_uImageCount ];

	m_pImageBits  = New PBYTE   [ m_uImageCount ];

	m_nTopPixel   = 0;

	m_pScroll->SetScrollPos(m_nTopPixel, TRUE);

	ThumbsCreate();

	CreateToolTip();

	AddSingleTool();

	afxThread->SetWaitMode(FALSE);
	}

void CImageResourceWnd::ThumbsLeave(void)
{
	DestroyToolTip();

	OnFreeImages();

	delete [] m_pImageThumb;

	delete [] m_pImageBits;
	}

void CImageResourceWnd::ThumbsDraw(CDC &DC, CRect Draw)
{
	CRect View = GetClientRect() - 1;

	if( m_uImageCount ) {

		CRect Cell = View;

		UINT  uPos = m_nTopPixel / m_ImageSize.cy;

		int   nTop = m_nTopPixel % m_ImageSize.cy;

		uPos      *= m_uViewCols;

		Cell.top  -= nTop - yTitle;

		for( UINT r = 0;; ) {

			Cell.left   = View.left;

			Cell.bottom = Cell.top + m_ImageSize.cy;

			for( UINT c = 0;; ) {

				Cell.right = Cell.left + m_ImageSize.cx;

				if( Draw.Overlaps(Cell) ) {

					CRect Work = Cell;

					DC.FrameRect(Work--, afxBrush(WHITE));

					UINT uLimit = OnZoomImage(uPos) ? 2 : 1;

					if( m_fPress && m_uMode < uLimit ) {

						if( uPos == m_uPress ) {

							DC.FrameRect(Work--, afxColor(3dShadow));

							if( uPos == m_uHover ) {

								DC.GradVert( Work--,
									     afxColor(NavBar1),
									     afxColor(NavBar2)
									     );
								}
							else {
								DC.GradVert( Work--,
									     afxColor(NavBar4),
									     afxColor(NavBar3)
									     );
								}
							}
						else {
							DC.FrameRect(Work--, afxBrush(3dLight));

							DC.FillRect (Work--, afxBrush(3dShadow));
							}
						}
					else {
						if( uPos == m_uHover ) {

							DC.FrameRect( Work--,
								      afxColor(3dShadow)
								      );

							DC.GradVert( Work--,
								     afxColor(NavBar4),
								     afxColor(NavBar3)
								     );
							}
						else {
							DC.FrameRect(Work--, afxBrush(3dLight));

							DC.FillRect (Work--, afxBrush(3dShadow));
							}
						}

					int xExtra = Work.cx() - m_ThumbSize.cx;
					int yExtra = Work.cy() - m_ThumbSize.cy;

					Work.left   += (xExtra + 0) / 2;
					Work.right  -= (xExtra + 1) / 2;
					Work.top    += (yExtra + 0) / 2;
					Work.bottom -= (yExtra + 1) / 2;

					DC.AlphaBlend(Work, m_pImageThumb[uPos], CPoint(0, 0));
					}

				if( ++uPos == m_uImageCount || ++c == m_uViewCols ) {

					Cell.left  = Cell.right;

					Cell.right = View.right;

					DC.FillRect(Cell, afxBrush(WHITE));

					break;
					}

				Cell.left = Cell.right;
				}

			if( uPos == m_uImageCount || r++ > m_uViewRows ) {

				Cell.left   = View.left;

				Cell.top    = Cell.bottom;

				Cell.bottom = View.bottom;

				DC.FillRect(Cell, afxBrush(WHITE));

				break;
				}

			Cell.top = Cell.bottom;
			}
		}
	else
		DC.FillRect(View, afxBrush(WHITE));
	}

void CImageResourceWnd::ThumbsLayout(void)
{
	CRect Client = GetClientRect() - 1;

	Client.top  += yTitle;

	m_uViewCols    = 1 + (m_fZoom ? m_uZoom : 1);

	m_ImageSize.cx = Client.cx() / m_uViewCols;

	m_ImageSize.cy = m_ImageSize.cx;
		
	m_uViewRows    = Client.cy() / m_ImageSize.cy;

	MakeMax(m_uViewRows, 1);

	if( m_uViewRows * m_uViewCols < m_uImageCount ) {

		Client.right   = Client.right - xScroll;

		m_ImageSize.cx = Client.cx() / m_uViewCols;

		m_ImageSize.cy = m_ImageSize.cx;
		
		m_uViewRows    = Client.cy() / m_ImageSize.cy;

		UINT uLast     = (m_uImageCount + m_uViewCols - 1) / m_uViewCols;

		int  nLast     = uLast * m_ImageSize.cy;

		m_pScroll->SetScrollRange(0, nLast, FALSE);

		m_pScroll->SetPageSize(Client.cy(), FALSE);

		m_pScroll->ShowWindow(SW_SHOW);

		m_pScroll->Invalidate(TRUE);

		MakeMin(m_nTopPixel, nLast);
		}
	else {
		m_pScroll->ShowWindow(SW_HIDE);

		m_nTopPixel = 0;
		}

	ThumbsCreate();

	Invalidate(TRUE);
	}

UINT CImageResourceWnd::ThumbsHitTest(CPoint Pos)
{
	Pos += CSize(-1, m_nTopPixel - yTitle);

	if( m_uMode ) {

		Pos /= m_ImageSize;

		if( Pos.x >= 0 && Pos.x < int(m_uViewCols) ) {

			UINT n = Pos.x + Pos.y * m_uViewCols;

			if( n < m_uImageCount ) {

				return n;
				}
			}
		}
	else {
		UINT n = Pos.y / yLine;

		if( n < m_CatName.GetCount() ) {

			return n;
			}
		}

	return NOTHING;
	}

CRect CImageResourceWnd::ThumbsGetRect(UINT uHover)
{
	CSize  Size = m_ImageSize;

	CPoint Pos  = CPoint(1, 1);

	Pos += CPoint(0, yTitle);

	Pos -= CPoint(0, m_nTopPixel);

	Pos += Size * CSize( uHover % m_uViewCols,
			     uHover / m_uViewCols
			     );

	return CRect(Pos, Size);
	}

void CImageResourceWnd::ThumbsCreate(void)
{
	if( m_ImageSize.cx && m_ImageSize.cy ) {

		m_ThumbSize = m_ImageSize - 8;

		CRect Space = CRect(m_ThumbSize);

		CMemoryDC WorkDC;

		for( UINT n = 0; n < m_uImageCount; n++ ) {

			m_pImageThumb[n].Create( m_ThumbSize.cx,
					         m_ThumbSize.cy,
						 1,
						 32,
						 m_pImageBits[n]
						 );

			WorkDC.Select(m_pImageThumb[n]);

			if( !OnGetAlpha(n) ) {

				WorkDC.FillRect(Space, afxBrush(MAGENTA));

				OnDrawImage(WorkDC, Space, n);

				m_pImageThumb[n].SetAlpha(m_pImageBits[n]);
				}
			else
				OnDrawImage(WorkDC, Space, n);

			WorkDC.Deselect();
			}
		}
	}

// Implementation

void CImageResourceWnd::LayoutWindow(void)
{
	switch( m_uMode ) {

		case 0:
			CatListLayout();
			break;

		case 1:
		case 2:
			ThumbsLayout();
			break;
		}
	}

UINT CImageResourceWnd::HitTestHover(CPoint Pos)
{
	switch( m_uMode ) {

		case 0:
			return CatListHitTest(Pos);

		case 1:
		case 2:
			return ThumbsHitTest(Pos);
		}

	return 0;
	}

CRect CImageResourceWnd::GetHoverRect(UINT uHover)
{
	switch( m_uMode ) {

		case 0:
			return CatListGetRect(uHover);

		case 1:
		case 2:
			return ThumbsGetRect(uHover);
		}

	return CRect();
	}

CRect CImageResourceWnd::GetScrollRect(void)
{
	CRect Rect = GetClientRect();

	Rect.left  = Rect.right - xScroll;

	return Rect;
	}

void CImageResourceWnd::CreateScrollBar(void)
{
	m_pScroll->Create(SB_VERT, GetScrollRect(), ThisObject, 0);
	}

void CImageResourceWnd::PlaceScrollBar(void)
{
	m_pScroll->MoveWindow(GetScrollRect(), TRUE);
	}

void CImageResourceWnd::EndHover(BOOL fQuick)
{
	HideToolTip();

	KillTimer(m_timerPreview);

	KillTimer(m_timerCheck);

	if( fQuick ) {

		m_fQuick = TRUE;

		SetTimer(m_timerQuick, 200);
		}
	else
		m_fQuick = FALSE;

	if( m_uHover < NOTHING ) {

		CRect Rect = GetHoverRect(m_uHover);

		Invalidate(Rect, TRUE);

		m_uHover = NOTHING;
		}
	}

void CImageResourceWnd::SetZoom(UINT uZoom)
{
	if( m_uZoom - uZoom ) {

		m_uZoom = uZoom;

		LayoutWindow();

		if( m_pScroll->IsWindowVisible() ) {

			int nMax = m_pScroll->GetRangeMax() - m_pScroll->GetPageSize();

			MakeMin(m_nTopPixel, nMax);

			m_pScroll->SetScrollPos(m_nTopPixel, TRUE);
			}
		}
	}

// Image Hooks

void CImageResourceWnd::OnBuildCatList(void)
{
	m_CatName.Append(L"Category One");

	m_CatName.Append(L"Category Two");

	m_CatName.Append(L"Category Three");
	}

BOOL CImageResourceWnd::OnMonitor(CStringArray &List)
{
	return FALSE;
	}

void CImageResourceWnd::OnRefresh(void)
{
	}

void CImageResourceWnd::OnLoadImages(void)
{
	}

void CImageResourceWnd::OnFreeImages(void)
{
	}

CString CImageResourceWnd::OnGetImageText(UINT uImage)
{
	return CPrintf(L"Image %u", 1 + uImage);
	}

void CImageResourceWnd::OnAddImageData(CDataObject *pData, UINT uImage)
{
	}

void CImageResourceWnd::OnDrawImage(CDC &DC, CRect Rect, UINT uImage)
{
	}

BOOL CImageResourceWnd::OnGetAlpha(UINT uImage)
{
	return FALSE;
	}

BOOL CImageResourceWnd::OnZoomImage(UINT uImage)
{
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Directory Monitor Thread
//

// Runtime Class

AfxImplementRuntimeClass(CImageResourceWnd::CMonitor, CRawThread);

// Constructor

CImageResourceWnd::CMonitor::CMonitor(CWnd *pWnd, CFilename const &Path)
{
	m_pWnd      = pWnd;

	m_Path      = Path;

	m_FileEvent = FindFirstChangeNotification( m_Path.Left(m_Path.GetLength()-1),
						   FALSE,
						   FILE_NOTIFY_CHANGE_FILE_NAME
						   );
	MakeList();
	}

// Destructor

CImageResourceWnd::CMonitor::~CMonitor(void)
{
	if( m_FileEvent.IsValid() ) {

		FindCloseChangeNotification(m_FileEvent);

		m_FileEvent.Detach();
		}
	}

// Attributes

BOOL CImageResourceWnd::CMonitor::GetChangeList(CStringArray &List)
{
	if( m_FileEvent.IsValid() ) {

		m_Mutex.WaitForObject(INFINITE);

		List.Append(m_Changes);

		m_Changes.Empty();

		m_Mutex.Release();
		}

	return !List.IsEmpty();
	}

// Overridables

UINT CImageResourceWnd::CMonitor::OnExec(void)
{
	if( m_FileEvent.IsValid() ) {

		CWaitableList List( m_TermEvent,
				    m_FileEvent
				    );

		for(;;) {

			List.WaitForAnyObject();

			if( List.GetObjectIndex() == 0 ) {

				return 0;
				}

			if( List.GetObjectIndex() == 1 ) {

				for(;;) {

					FindNextChangeNotification(m_FileEvent);

					if( List.WaitForAnyObject(500) == waitTimeout ) {

						break;
						}

					if( List.GetObjectIndex() == 0 ) {

						return 0;
						}
					}

				m_Mutex.WaitForObject(INFINITE);

				TestList();

				if( !m_Changes.IsEmpty() ) {

					m_pWnd->PostMessage(WM_AFX_COMMAND, IDM_RES_REFRESH);
					}

				m_Mutex.Release();
				}
			}

		return 0;
		}

	m_TermEvent.WaitForObject();

	return 0;
	}

// Implmentation

void CImageResourceWnd::CMonitor::MakeList(void)
{
	if( m_FileEvent.IsValid() ) {

		m_Map.Empty();

		WIN32_FIND_DATA Data;

		HANDLE hFind = FindFirstFile(m_Path + L"*.wid31", &Data);

		if( hFind != INVALID_HANDLE_VALUE ) {

			do {
				WIN32_FILE_ATTRIBUTE_DATA Attr;

				CString Name = m_Path + Data.cFileName;

				if( GetFileAttributesEx(Name, GetFileExInfoStandard, &Attr) ) {

					DWORD Time = Attr.ftLastWriteTime.dwLowDateTime;

					m_Map.Insert(Name, Time);
					}

				} while( FindNextFile(hFind, &Data) );

			FindClose(hFind);
			}
		}
	}

BOOL CImageResourceWnd::CMonitor::TestList(void)
{
	CTree <CString> Read;

	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(m_Path + L"*.wid31", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			WIN32_FILE_ATTRIBUTE_DATA Attr;

			CString Name = m_Path + Data.cFileName;

			if( GetFileAttributesEx(Name, GetFileExInfoStandard, &Attr) ) {

				DWORD Time  = Attr.ftLastWriteTime.dwLowDateTime;

				INDEX Index = m_Map.FindName(Name);

				if( m_Map.Failed(Index) ) {

					m_Map.Insert(Name, Time);

					m_Changes.Append(Name);
					}
				else {
					if( m_Map.GetData(Index) != Time ) {

						m_Map.SetData(Index, Time);

						m_Changes.Append(Name);
						}
					}

				Read.Insert(Name);
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}

	if( TRUE ) {

		INDEX Index = m_Map.GetHead();

		while( !m_Map.Failed(Index) ) {

			INDEX   Next = Index;

			CString Name = m_Map.GetName(Index);

			m_Map.GetNext(Next);

			if( Read.Failed(Read.Find(Name)) ) {

				m_Map.Remove(Index);

				m_Changes.Append(Name);
				}

			Index = Next;
			}
		}

	return TRUE;
	}

// End of File
