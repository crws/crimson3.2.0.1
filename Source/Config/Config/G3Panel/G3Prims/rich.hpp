
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RICH_HPP
	
#define	INCLUDE_RICH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimRich;

//////////////////////////////////////////////////////////////////////////
//
// Rich Property Codes
//

enum {
	propEntry  = 0x01,
	propLimits = 0x02,
	propLabel  = 0x04,
	propFormat = 0x08,
	propColor  = 0x10,
	};

//////////////////////////////////////////////////////////////////////////
//
// Rich Legacy Primitive Page
//

class CPrimRichPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimRichPage(CPrimRich *pData, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimRich * m_pData;

		// Implementation
		BOOL LoadFormat(IUICreate *pView);
		BOOL LoadColor (IUICreate *pView);
		BOOL LoadNone  (IUICreate *pView, PCTXT pGroup, PCTXT pWhat);
		BOOL LoadPage  (IUICreate *pView, UINT uSub);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rich Base Class
//

class CPrimRich : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRich(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL ShowProp(UINT Prop) const;
		BOOL NeedProp(UINT Prop) const;
		BOOL CanWrite(void) const;
		BOOL IsTagRef(void) const;
		BOOL IsUnboundTag(void) const;
		BOOL GetTagItem(CTag * &pTag) const;
		BOOL GetTagLabel(CCodedText * &pLabel) const;
		BOOL GetTagFormat(CDispFormat * &pFormat) const;
		BOOL GetTagColor(CDispColor * &pColor) const;
		BOOL GetDataLabel(CCodedText * &pLabel) const;
		BOOL GetDataFormat(CDispFormat * &pFormat) const;
		BOOL GetDataColor(CDispColor * &pColor) const;
		BOOL HasTagLabel(void) const;
		BOOL HasTagLimits(void) const;
		BOOL HasTagFormat(void) const;
		BOOL HasTagColor(void) const;
		BOOL UseTagLimits(void) const;
		BOOL UseTagLabel(void) const;
		BOOL UseTagFormat(void) const;		
		BOOL UseTagColor(void) const;
		BOOL HasOwnFormat(void) const;

		// Limit Access
		DWORD GetMinValue(UINT Type) const;
		DWORD GetMaxValue(UINT Type) const;

		// Overridables
		void SetInitState(void);
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem *  m_pValue;
		UINT	      m_Entry;
		UINT	      m_TagLimits;
		UINT	      m_TagLabel;
		UINT	      m_TagFormat;
		UINT	      m_TagColor;
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pValidate;
		CCodedItem  * m_pOnSetFocus;
		CCodedItem  * m_pOnKillFocus;
		CCodedItem  * m_pOnComplete;
		CCodedText  * m_pLabel;
		CCodedItem  * m_pLimitMin;
		CCodedItem  * m_pLimitMax;
		UINT	      m_FormType;
		UINT	      m_ColType;
		UINT	      m_UseBack;
		CDispFormat * m_pFormat;
		CDispColor  * m_pColor;
		CPrimColor  * m_pTextColor;
		CPrimColor  * m_pTextShadow;
		INT           m_AlignH;
		INT           m_AlignV;
		UINT          m_Font;
		UINT          m_Content;
		UINT          m_Flash;
		CRect         m_Margin;

	protected:
		// Data Members
		CCodedItem * m_pLast;
		UINT	     m_ShowMask;
		CLASS        m_Format;		

		// Meta Data
		void AddMetaData(void);
		
		// Type Updates
		void UpdateLimitTypes(IUIHost *pHost);
		void UpdateChildTypes(void);

		// Child Updates
		BOOL SetFormatClass(CDispFormat * &pFormat, CLASS Class);
		BOOL SetColorClass (CDispColor *  &pColor,  CLASS Class);

		// Copying from Tag
		BOOL CopyTagLabel(IUIHost *pHost);
		BOOL CopyTagFormat(void);
		BOOL CopyTagColor(void);

		// Color Extraction
		void FindColors(IGDI *pGDI, COLOR &Color, COLOR &Shadow, DWORD Data, UINT Type, BOOL fEntry);

		// Text Extraction
		CString FindLabelText(void);
		CString FindValueText(DWORD Data, UINT Type, UINT Flags);

		// Operations
		BOOL SelectFont(IGDI *pGDI, UINT Font);
		void DrawLabel(IGDI *pGDI, R2 Rect);
		void DrawValue(IGDI *pGDI, R2 Rect);

		// Attributes
		int GetFontSize(void) const;
		int GetTextWidth(PCTXT pText) const;

		// Field Requirements
		virtual UINT GetNeedMask(void) const;

		// Implementation
		COLOR FindCompColor(COLOR c);
		void  LimitTypes(IUIHost *pHost);
		void  KillUnused(IUIHost *pHost);
		BOOL  KillFormat(void);
		BOOL  KillColor(void);
		BOOL  LoadFirstPage(CUIPageList *pList);
		BOOL  LoadRichPages(CUIPageList *pList);
		void  DoEnables(IUIHost *pHost);
	};

// End of File

#endif
