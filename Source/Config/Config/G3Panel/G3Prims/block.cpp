
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Block
//

// Dynamic Class

AfxImplementDynamicClass(CPrimBlock, CCodedHost);

// Constructor

CPrimBlock::CPrimBlock(void)
{
	m_AlignH   = 1;
	
	m_AlignV   = 2;

	m_Lead     = 2;

	m_Font     = fontHei16;

	m_MoveDir  = 0;

	m_MoveStep = 2;

	m_fError   = FALSE;
	}

// UI Update

void CPrimBlock::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT n = 0;
		
		LimitEnum(pHost, L"AlignV", n, 0x01F);

		DoEnables(pHost);
		}

	if( Tag == "MoveDir" ) {

		DoEnables(pHost);
		}

	if( !Tag.IsEmpty() ) {

		CPrim *pPrim = (CPrim *) GetParent();

		pPrim->OnUIChange(pHost, pItem, L"PrimProp");
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimBlock::HasError(void) const
{
	return m_fError;
	}

int CPrimBlock::GetFontSize(int nLines) const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = nLines * pFonts->GetHeight(m_Font);

	nSize += m_Margin.top;

	nSize += m_Margin.bottom;

	return nSize;
	}

int CPrimBlock::GetTextWidth(PCTXT pText) const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = pFonts->GetWidth(m_Font, pText);

	nSize += m_Margin.left;

	nSize += m_Margin.right;

	return nSize;
	}

// Operations

void CPrimBlock::GetRefs(CPrimRefList &Refs)
{
	Refs.Insert(m_Font | refFont);
	}

void CPrimBlock::EditRef(UINT uOld, UINT uNew)
{
	if( uOld == NOTHING ) {

		if( m_Font >= 0x100 ) {

			m_Font |= refPending;
			}

		return;
		}

	if( (m_Font | refFont) == uOld ) {

		m_Font = uNew;
		}
	}

BOOL CPrimBlock::SelectFont(IGDI *pGDI)
{
	if( m_Font < 0x100 ) {

		pGDI->SelectFont(m_Font);

		return TRUE;
		}

	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	return pFonts->Select(pGDI, m_Font);
	}

// Download Support

BOOL CPrimBlock::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_AlignH));
	Init.AddByte(BYTE(m_AlignV));
	Init.AddWord(WORD(m_Lead));
	Init.AddWord(WORD(m_Font));
	Init.AddByte(BYTE(m_MoveDir));
	Init.AddByte(BYTE(m_MoveStep));
	Init.AddWord(WORD(m_Margin.left));
	Init.AddWord(WORD(m_Margin.top));
	Init.AddWord(WORD(m_Margin.right));
	Init.AddWord(WORD(m_Margin.bottom));

	return TRUE;
	}

// Meta Data

void CPrimBlock::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(AlignH);
	Meta_AddInteger(AlignV);
	Meta_AddInteger(Lead);
	Meta_AddInteger(Font);
	Meta_AddInteger(MoveDir);
	Meta_AddInteger(MoveStep);
	Meta_AddRect   (Margin);

	Meta_SetName((IDS_TAG_BLOCK));
	}

// Implementation

BOOL CPrimBlock::IsPressed(void)
{
	CPrim *pPrim = (CPrim *) GetParent();

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrim)) ) {

		return pPrim->IsPressed();
		}

	return FALSE;
	}

BOOL CPrimBlock::CanMove(void)
{
	if( m_AlignH == 1 && m_AlignV < 5 ) {

		CPrimWithText *pPrim = (CPrimWithText *) GetParent();

		if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

			if( pPrim->HasAction() ) {

				return TRUE;
				}

			if( pPrim->GetActionMode() == actNone ) {

				// LATER -- Does this always mean we're a button?

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPrimBlock::CanEdit(void)
{
	CPrimWithText *pPrim = (CPrimWithText *) GetParent();

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

		if( pPrim->GetActionMode() == actNone ) {

			// LATER -- Does this always mean we're a button?

			return FALSE;
			}

		if( pPrim->HasAction() ) {

			if( pPrim->m_pAction->m_Mode ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

void CPrimBlock::DoEnables(IUIHost *pHost)
{
	BOOL fMove = CanMove();

	pHost->EnableUI(this, "MoveDir",  fMove);

	pHost->EnableUI(this, "MoveStep", fMove && m_MoveDir);
	}

// End of File
