
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyTimeDate_HPP
	
#define	INCLUDE_PrimRubyTimeDate_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyDataBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Time Date Primitive
//

class CPrimRubyTimeDate : public CPrimRubyDataBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyTimeDate(void);

		// Overridables
		void SetInitState(void);
	
	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
