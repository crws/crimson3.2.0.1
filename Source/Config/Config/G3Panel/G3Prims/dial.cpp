
#include "intern.hpp"

#include "dial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Dial Gauge
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyDial, CPrimRich);

// Constructor

CPrimLegacyDial::CPrimLegacyDial(void)
{
	m_Orient    = 0;

	m_Entity    = etWhole;

	m_Flip      = 1;
	
	m_Major     = 6;

	m_Minor     = 2;

	m_pEdge     = New CPrimPen;

	m_pBack     = New CPrimBrush;

	m_pFill     = New CPrimColor;
	
	m_AlignH    = 1;
	
	m_AlignV    = 0;

	// LATER -- If we clip the text, we can turn this back on.

	m_Content   = 3;

	m_ShowMask  = propFormat | propLimits | propColor | propLabel;
	}

// Initial Values

void CPrimLegacyDial::SetInitValues(void)
{
	m_pFill->SetInitial(GetRGB( 8, 8, 8));
	}

// UI Creation

BOOL CPrimLegacyDial::OnLoadPages(CUIPageList *pList)
{
	LoadFirstPage(pList);

	pList->Append( New CUIStdPage( CString(IDS_OPTIONS),
				       AfxPointerClass(this),
				       2
				       ));

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       3
				       ));

	LoadRichPages(pList);
	
	return TRUE;
	}

// UI Update

void CPrimLegacyDial::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == L"Orient" ) {

		FindScaleSweep();
		}

	CPrimRich::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimLegacyDial::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DialRect;

	pGDI->ResetBrush();

	m_pBack->FillEllipse(pGDI, Rect, m_Orient + m_Entity);
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawEllipse(pGDI, Rect, m_Orient + m_Entity, uMode);
		}

	pGDI->ResetBrush();

	DrawScale(pGDI, m_nRadius1, m_nRadius2, uMode);	
	
	DrawPointer(pGDI, m_Origin, m_nRadius3, m_Start, uMode);
	}

void CPrimLegacyDial::SetInitState(void)
{
	CPrimRich::SetInitState();

	m_pEdge->m_Corner = 0;
	
	m_pEdge->Set(GetRGB(31,31,31));

	m_pBack->Set(GetRGB(12,12,24));

	SetInitSize(100, 100);
	}

void CPrimLegacyDial::UpdateLayout(void)
{
	CPrimRich::UpdateLayout();

	FindDialRect();

	FindPolar();

	FindScaleRadius();
	}

// Persistance

void CPrimLegacyDial::Init(void)
{
	CPrimRich::Init();

	FindScaleSweep();
	}

// Download Support

BOOL CPrimLegacyDial::MakeInitData(CInitData &Init)
{
	CPrimRich::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pEdge);

	Init.AddItem(itemSimple, m_pBack);

	Init.AddItem(itemSimple, m_pFill);

	Init.AddWord(WORD(m_DialRect.x1));
	Init.AddWord(WORD(m_DialRect.y1));
	Init.AddWord(WORD(m_DialRect.x2));
	Init.AddWord(WORD(m_DialRect.y2));

	Init.AddByte(BYTE(m_Entity));
	Init.AddByte(BYTE(m_Orient));

	Init.AddByte(BYTE(m_Flip));

	Init.AddByte(BYTE(m_Major));
	Init.AddByte(BYTE(m_Minor));

	Init.AddWord(WORD(m_Start));
	Init.AddWord(WORD(m_Sweep));

	Init.AddWord(WORD(m_Origin.x));
	Init.AddWord(WORD(m_Origin.y));

	Init.AddWord(WORD(m_nRadius1));
	Init.AddWord(WORD(m_nRadius2));
	Init.AddWord(WORD(m_nRadius3));

	return TRUE;
	}

// Meta Data

void CPrimLegacyDial::AddMetaData(void)
{
	CPrimRich::AddMetaData();

	Meta_AddInteger(Orient);
	Meta_AddInteger(Major);
	Meta_AddInteger(Flip);
	Meta_AddInteger(Minor);
	Meta_AddInteger(Start);
	Meta_AddInteger(Sweep);
	Meta_AddObject (Fill);
	Meta_AddObject (Back);
	Meta_AddObject (Edge);

	Meta_SetName((IDS_DIAL_GAUGE));
	}

// Field Requirements

UINT CPrimLegacyDial::GetNeedMask(void) const
{
	UINT uMask = propLimits | propFormat | propColor;
	
	if( m_Content > 0 && m_Content < 3 ) {

		uMask |= propLabel;
		}

	return uMask;
	}

// Overridables

void CPrimLegacyDial::FindDialRect(void)
{
	}

void CPrimLegacyDial::FindPolar(void)
{
	}

void CPrimLegacyDial::FindScaleSweep(void)
{
	}

void CPrimLegacyDial::FindScaleRadius(void)
{
	}

// Implementation

void CPrimLegacyDial::DrawScale(IGDI *pGDI, int nRad1, int nRad2, UINT uMode)
{
	int  nRad3 = (nRad1 + nRad2) / 2;

	int   nDiv = m_Major * m_Minor;

	for( int n = 0; n <= nDiv; n ++ ) {
		
		int nAngle = m_Start - MulDiv(m_Sweep, n, nDiv);
		
		if( n % m_Minor ) {

			DrawLine( pGDI, 
				  m_Origin, 
				  m_Flip ? nRad3 : nRad1, 
				  m_Flip ? nRad2 : nRad3, 
				  nAngle, 
				  uMode
				  );
			}
		else		
			DrawRect( pGDI, 
				  m_Origin, 
				  nRad1, 
				  nRad2, 
				  nAngle, 
				  uMode
				  );
		}
	}

void CPrimLegacyDial::DrawRect(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle, UINT uMode)
{
	double Theta = nAngle / 180.0 * acos(double(-1));

	double   Cos = cos(Theta);
	double   Sin = sin(Theta);

	double  nLen = nRad2 - nRad1;

	double nWid1 = 4;
	double nWid2 = nWid1 / 2;

	P2 Pt[4];

	Pt[0].x = int(Org.x   + nRad1*Cos - nWid2*Sin + 0.5);
	Pt[0].y = int(Org.y   - nRad1*Sin - nWid2*Cos + 0.5);

	Pt[1].x = int(Pt[0].x + nLen*Cos + 0.5);
	Pt[1].y = int(Pt[0].y - nLen*Sin + 0.5);

	Pt[2].x = int(Pt[1].x + nWid1*Sin + 0.5);
	Pt[2].y = int(Pt[1].y + nWid1*Cos + 0.5);

	Pt[3].x = int(Pt[2].x - nLen*Cos + 0.5);
	Pt[3].y = int(Pt[2].y + nLen*Sin + 0.5);
	
	pGDI->SetBrushFore(m_pFill->GetColor());

	pGDI->FillPolygon(Pt, elements(Pt), 0);
	
	m_pEdge->DrawPolygon(pGDI, Pt, elements(Pt), 0, uMode);
	}

void CPrimLegacyDial::DrawLine(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle, UINT uMode)
{
	double Theta = nAngle / 180.0 * acos(double(-1));

	double   Cos = cos(Theta);
	double   Sin = sin(Theta);

	P2 Pt[2];

	Pt[0].x = int(Org.x + nRad1*Cos + 0.5);
	Pt[0].y = int(Org.y - nRad1*Sin + 0.5);

	Pt[1].x = int(Org.x + nRad2*Cos + 0.5);
	Pt[1].y = int(Org.y - nRad2*Sin + 0.5);

	pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());
	
	pGDI->DrawLine(PassPoint(Pt[0]), PassPoint(Pt[1]));
	}

void CPrimLegacyDial::DrawPointer(IGDI *pGDI, P2 Org, INT nRadius, INT nAngle, UINT uMode)
{
	double Theta = nAngle / 180.0 * acos(double(-1)); 

	double   Cos = cos(Theta);
	double   Sin = sin(Theta);

	double nRad1 = double(nRadius) * 0.75;
	double nRad2 = double(nRadius);

	double nWid1 = 2.0;
	double nWid2 = 3.5;

	P2 Pt[5];

	Pt[0].x = int(Org.x - nWid1*Sin + 0.5);
	Pt[0].y = int(Org.y - nWid1*Cos + 0.5);

	Pt[1].x = int(Org.x + nRad1*Cos - nWid2*Sin + 0.5);
	Pt[1].y = int(Org.y - nRad1*Sin - nWid2*Cos + 0.5);
	
	Pt[2].x = int(Org.x + nRad2*Cos + 0.5);
	Pt[2].y = int(Org.y - nRad2*Sin + 0.5);
	
	Pt[3].x = int(Org.x + nRad1*Cos + nWid2*Sin + 0.5);
	Pt[3].y = int(Org.y - nRad1*Sin + nWid2*Cos + 0.5);

	Pt[4].x = int(Org.x + nWid1*Sin + 0.5);
	Pt[4].y = int(Org.y + nWid1*Cos + 0.5);
	
	pGDI->SetBrushFore(m_pFill->GetColor());

	pGDI->FillPolygon(Pt, elements(Pt), 0);
	
	m_pEdge->DrawPolygon(pGDI, Pt, elements(Pt), 0, uMode);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Whole Dial Gauge
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyWholeDial, CPrimLegacyDial);

// Constructor

CPrimLegacyWholeDial::CPrimLegacyWholeDial(void)
{
	m_uType = 0x24;
	}

// Overridables

void CPrimLegacyWholeDial::Draw(IGDI *pGDI, UINT uMode)
{
	CPrimLegacyDial::Draw(pGDI, uMode);

	DrawLegend(pGDI);
	}

// Meta Data

void CPrimLegacyWholeDial::AddMetaData(void)
{
	CPrimLegacyDial::AddMetaData();

	Meta_SetName((IDS_DIAL_GAUGE_WHOLE));
	}

// Overridables

void CPrimLegacyWholeDial::FindDialRect(void)
{
	int    x1 = m_DrawRect.x1;
	
	int    y1 = m_DrawRect.y1;
	
	int    x2 = m_DrawRect.x2;
	
	int    y2 = m_DrawRect.y2;

	int xSize = (x2 - x1);

	int ySize = (y2 - y1);

	int nDiam = min( xSize, ySize );	

	if( xSize > ySize ) {

		m_DialRect.x1 = x1 + (xSize - nDiam) / 2;

		m_DialRect.y1 = y1;
		}
	else {
		m_DialRect.x1 = x1;

		m_DialRect.y1 = y1 + (ySize - nDiam) / 2;
		}

	m_DialRect.x2 = m_DialRect.x1 + nDiam;

	m_DialRect.y2 = m_DialRect.y1 + nDiam;
	}

void CPrimLegacyWholeDial::FindPolar(void)
{
	m_nRadius  = (m_DialRect.x2 - m_DialRect.x1) / 2;

	m_Origin.x = (m_DialRect.x2 + m_DialRect.x1) / 2;

	m_Origin.y = (m_DialRect.y2 + m_DialRect.y1) / 2;
	}

void CPrimLegacyWholeDial::FindScaleSweep(void)
{
	m_Start     = 225;
	
	m_Sweep     = 270;
	}

void CPrimLegacyWholeDial::FindScaleRadius(void)
{
	m_nScale   = m_nRadius - 5;

	m_nRadius1 = (m_nScale * 3) / 4;;
	
	m_nRadius2 = (m_nScale * 1) / 1;
	
	m_nRadius3 = m_nRadius1 - 4;
	}

void CPrimLegacyWholeDial::DrawLegend(IGDI *pGDI)
{
	if( m_Content < 3 ) {

		SelectFont(pGDI, m_Font);

		R2 Work;

		Work.x1 = m_Origin.x;

		Work.y1 = m_Origin.y + 10;

		Work.x2 = Work.x1;
		
		Work.y2 = Work.y1;

		int    ySize = GetFontSize();

		DWORD Data   = 0;

		UINT  Type   = typeVoid;

		COLOR Color  = 0xFFFF;
		
		COLOR Shadow = 0xFFFF;

		if( m_pValue ) {

			Data = m_pValue->ExecVal();

			Type = m_pValue->GetType();
			}

		FindColors( pGDI,
			    Color,
			    Shadow,
			    Data,
			    Type,
			    FALSE
			    );

		if( m_Content > 0 ) {

			CString Label = FindLabelText();

			int xSize = GetTextWidth(Label);

			Work.x1  -= xSize / 2;
			
			Work.x2  += xSize / 2;
			
			Work.y2  += ySize;

			DrawLabel(pGDI, Work);

			Work.y1 += ySize;
			}

		if( m_Content < 2 ) {

			Work.x1 = Work.x2 = m_Origin.x;

			DWORD Data    = 0;

			UINT  Type    = typeVoid;

			CString Value = FindValueText(Data, Type, fmtPad);

			int xSize = GetTextWidth(Value);
			
			Work.x1  -= xSize / 2;
			
			Work.x2  += xSize / 2;
			
			Work.y2  += ySize;
			
			DrawValue(pGDI, Work);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Half Dial Gauge
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyHalfDial, CPrimLegacyDial);

// Constructor

CPrimLegacyHalfDial::CPrimLegacyHalfDial(void)
{
	m_uType  = 0x25;

	m_Orient = 1;
	
	m_Entity = etHalf1;
	}

// Overridables

void CPrimLegacyHalfDial::Draw(IGDI *pGDI, UINT uMode)
{
	CPrimLegacyDial::Draw(pGDI, uMode);
	
	if( m_pEdge->m_Width ) {

		R2 Rect = m_DialRect;

		Rect.x2--;

		switch( m_Orient ) {

			case 0:
				Rect.right = Rect.left + m_pEdge->m_Width;

				break;

			case 1:
				Rect.top = Rect.bottom - m_pEdge->m_Width;

				break;

			case 2:
				Rect.left = Rect.right - m_pEdge->m_Width;

				break;

			case 3:
				Rect.bottom = Rect.top + m_pEdge->m_Width;

				break;
			}

		m_pEdge->DrawRect(pGDI, Rect, uMode);
		}

	DrawLegend(pGDI);
	}

// Meta Data

void CPrimLegacyHalfDial::AddMetaData(void)
{
	CPrimLegacyDial::AddMetaData();

	Meta_SetName((IDS_DIAL_GAUGE_HALF));
	}

// Overridables

void CPrimLegacyHalfDial::FindDialRect(void)
{
	int      x1 = m_DrawRect.x1;
	
	int      y1 = m_DrawRect.y1;
	
	int      x2 = m_DrawRect.x2;
	
	int      y2 = m_DrawRect.y2;

	int   xSize = (x2 - x1);

	int   ySize = (y2 - y1);

	int nRadius = 0;

	P2      Pt1 = { x1, y1 };

	P2      Pt2 = { x2, y2 };

	switch( m_Orient ) {
		
		case 0:
			nRadius = min( xSize, ySize / 2 );

			if( xSize < ySize ) {

				Pt1.y += (ySize / 2) - nRadius;;

				Pt2.y -= (ySize / 2) - nRadius;
				}

			Pt2.x -= xSize - nRadius;
			
			break;
		
		case 1:
			nRadius = min( xSize / 2, ySize );
			
			if( xSize > ySize ) {
				
				Pt1.x += (xSize / 2) - nRadius;	

				Pt2.x -= (xSize / 2) - nRadius;;
				}

			Pt1.y += ySize - nRadius;
			
			break;
		
		case 2:
			nRadius = min( xSize, ySize / 2 );
			
			if( xSize < ySize ) {

				Pt1.y += (ySize / 2) - nRadius;

				Pt2.y -= (ySize / 2) - nRadius;
				}

			Pt1.x += xSize - nRadius;

			break;
		
		case 3:
			nRadius = min( xSize / 2, ySize );

			if( xSize > ySize ) {

				Pt1.x += (xSize / 2) - nRadius;

				Pt2.x -= (xSize / 2) - nRadius;	
				}

			Pt2.y -= ySize - nRadius;

			break;
		}

	m_DialRect.x1 = Pt1.x;

	m_DialRect.y1 = Pt1.y;

	m_DialRect.x2 = Pt2.x;

	m_DialRect.y2 = Pt2.y;
	}

void CPrimLegacyHalfDial::FindPolar(void)
{
	m_nRadius  = min(m_DialRect.x2 - m_DialRect.x1, m_DialRect.y2 - m_DialRect.y1);

	switch( m_Orient ) {

		case 0:
			m_Origin.x = m_DialRect.left;

			m_Origin.y = m_DialRect.top + m_nRadius;

			break;

		case 1:
			m_Origin.x = m_DialRect.left + m_nRadius;

			m_Origin.y = m_DialRect.bottom;

			break;

		case 2:
			m_Origin.x = m_DialRect.right;

			m_Origin.y = m_DialRect.top + m_nRadius;

			break;

		case 3:
			m_Origin.x = m_DialRect.left + m_nRadius;

			m_Origin.y = m_DialRect.top;

			break;
		}

	int    dx[] = { +10,   0, -10,   0 };

	int    dy[] = {   0, -10,   0, +10 };

	m_Origin.x += dx[m_Orient];

	m_Origin.y += dy[m_Orient];
	}

void CPrimLegacyHalfDial::FindScaleSweep(void)
{
	m_Start = (m_Orient + 1) * 90;

	m_Sweep = 180;
	}

void CPrimLegacyHalfDial::FindScaleRadius(void)
{
	m_nScale   = m_nRadius - 15;

	m_nRadius1 = (m_nScale * 3) / 4;;
	
	m_nRadius2 = (m_nScale * 1) / 1;
	
	m_nRadius3 = m_nRadius1 - 4;
	}

void CPrimLegacyHalfDial::DrawLegend(IGDI *pGDI)
{
	if( m_Content < 3 ) {

		SelectFont(pGDI, m_Font);

		R2   Work = m_DialRect;

		int ySize = GetFontSize();

		int  dx[] = { +10,   0, -10,   0 };

		int  dy[] = {   0, -10,   0, +10 };

		Work.x1   = m_Origin.x + dx[m_Orient];

		Work.y1   = m_Origin.y + dy[m_Orient];

		switch( m_Orient ) {

			case 0:
				Work.y1 -= (m_Content == 1) ? ySize : ySize / 2;

				break;

			case 1:
				Work.y1 -= (m_Content == 1) ? (2 * ySize) : ySize;

				break;

			case 2:
				Work.y1 -= (m_Content == 1) ? ySize : ySize / 2;

				break;

			case 3:
				break;
			}

		Work.x2 = Work.x1;
		
		Work.y2 = Work.y1;

		DWORD Data   = 0;

		UINT  Type   = typeVoid;

		COLOR Color  = 0xFFFF;
		
		COLOR Shadow = 0xFFFF;

		if( m_pValue ) {

			Data = m_pValue->ExecVal();

			Type = m_pValue->GetType();
			}

		FindColors( pGDI,
			    Color,
			    Shadow,
			    Data,
			    Type,
			    FALSE
			    );

		if( m_Content > 0 ) {

			CString Label = FindLabelText();

			int xSize = GetTextWidth(Label);

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					break;
				
				case 1:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				
				case 2:
					Work.x1 -= xSize;
					
					break;
				
				case 3:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				}

			Work.y2 += ySize;

			DrawLabel(pGDI, Work);

			Work.y1 = Work.y2;
			}

		if( m_Content < 2 ) {

			DWORD Data    = 0;

			UINT  Type    = typeVoid;

			CString Value = FindValueText(Data, Type, fmtPad);

			int xSize = GetTextWidth(Value);

			Work.x1 = m_Origin.x + dx[m_Orient];

			Work.x2 = Work.x1;

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					
					break;
				
				case 1:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				
				case 2:
					Work.x1 -= xSize;
					
					break;
				
				case 3:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				}

			Work.y2 += ySize;

			DrawValue(pGDI, Work);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Quad Dial Gauge
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyQuadDial, CPrimLegacyDial);

// Constructor

CPrimLegacyQuadDial::CPrimLegacyQuadDial(void)
{
	m_uType  = 0x26;

	m_Orient = 1;
	
	m_Entity = etQuad1;
	}

// Overridables

void CPrimLegacyQuadDial::Draw(IGDI *pGDI, UINT uMode)
{
	CPrimLegacyDial::Draw(pGDI, uMode);
	
	if( m_pEdge->m_Width ) {

		R2  Rect1 = m_DialRect;

		R2  Rect2 = m_DialRect;

		switch( m_Orient ) {

			case 0:
				Rect1.top   = Rect2.bottom - m_pEdge->m_Width;

				Rect2.right = Rect2.left   + m_pEdge->m_Width;

				break;

			case 1:
				Rect1.left  = Rect1.right  - m_pEdge->m_Width;

				Rect2.top   = Rect2.bottom - m_pEdge->m_Width;

				break;

			case 2:
				Rect1.bottom = Rect1.top   + m_pEdge->m_Width;

				Rect2.left   = Rect2.right - m_pEdge->m_Width;

				break;

			case 3:
				Rect1.right  = Rect1.left  + m_pEdge->m_Width;

				Rect2.bottom = Rect2.top   + m_pEdge->m_Width;

				break;
			}

		m_pEdge->DrawRect(pGDI, Rect1, uMode);

		m_pEdge->DrawRect(pGDI, Rect2, uMode);
		}

	DrawLegend(pGDI);
	}

// Meta Data

void CPrimLegacyQuadDial::AddMetaData(void)
{
	CPrimLegacyDial::AddMetaData();

	Meta_SetName((IDS_DIAL_GAUGE_QUAD));
	}

// Overridables

void CPrimLegacyQuadDial::FindDialRect(void)
{
	int      x1 = m_DrawRect.x1;
	
	int      y1 = m_DrawRect.y1;
	
	int      x2 = m_DrawRect.x2;
	
	int      y2 = m_DrawRect.y2;

	int   xSize = (x2 - x1);

	int   ySize = (y2 - y1);

	int nRadius = min( xSize, ySize );

	P2      Pt1 = { x1, y1 };

	P2      Pt2 = { x2, y2 };
	
	switch( m_Orient ) {

		case 0:
			Pt1.y += ySize - nRadius;

			Pt2.x -= xSize - nRadius;

			break;

		case 1:
			Pt1.x += xSize - nRadius;	
			
			Pt1.y += ySize - nRadius;

			break;

		case 2:
			Pt1.x += xSize - nRadius;

			Pt2.y -= ySize - nRadius;

			break;

		case 3:
			Pt2.x -= xSize - nRadius;	
			
			Pt2.y -= ySize - nRadius;

			break;
		}

	m_DialRect.x1 = Pt1.x;

	m_DialRect.y1 = Pt1.y;

	m_DialRect.x2 = Pt2.x;

	m_DialRect.y2 = Pt2.y;
	}

void CPrimLegacyQuadDial::FindPolar(void)
{
	m_nRadius  = min(m_DialRect.x2 - m_DialRect.x1, m_DialRect.y2 - m_DialRect.y1);

	switch( m_Orient ) {

		case 0:
			m_Origin.x = m_DialRect.left;

			m_Origin.y = m_DialRect.bottom;

			break;

		case 1:
			m_Origin.x = m_DialRect.right;

			m_Origin.y = m_DialRect.bottom;

			break;

		case 2:
			m_Origin.x = m_DialRect.right;

			m_Origin.y = m_DialRect.top;

			break;

		case 3:
			m_Origin.x = m_DialRect.left;

			m_Origin.y = m_DialRect.top;

			break;
		}

	int dx[] = { +10, -10, -10, +10 };

	int dy[] = { -10, -10, +10, +10 };

	m_Origin.x += dx[m_Orient];

	m_Origin.y += dy[m_Orient];
	}

void CPrimLegacyQuadDial::FindScaleSweep(void)
{
	m_Start = (m_Orient + 1) * 90;

	m_Sweep = 90;
	}

void CPrimLegacyQuadDial::FindScaleRadius(void)
{
	m_nScale   = m_nRadius - 20;

	m_nRadius1 = (m_nScale * 3) / 4;;
	
	m_nRadius2 = (m_nScale * 1) / 1;
	
	m_nRadius3 = m_nRadius1 - 4;
	}

// Implementation

void CPrimLegacyQuadDial::DrawLegend(IGDI *pGDI)
{
	if( m_Content < 3 ) {

		SelectFont(pGDI, m_Font);

		R2   Work;

		int ySize = GetFontSize();

		int  dx[] = { +10, -10, -10, +10 };

		int  dy[] = { -10, -10, +10, +10 };

		Work.x1   = m_Origin.x + dx[m_Orient];

		Work.y1   = m_Origin.y + dy[m_Orient];

		Work.x2 = Work.x1;

		switch( m_Orient ) {

			case 0:				
				Work.y1 -= (m_Content == 1) ? (2 * ySize) : ySize;

				break;

			case 1:
				Work.y1 -= (m_Content == 1) ? (2 * ySize) : ySize;

				break;

			case 2:
				
				break;

			case 3:
				break;
			}

		Work.y2 = Work.y1;

		DWORD Data   = 0;

		UINT  Type   = typeVoid;

		COLOR Color  = 0xFFFF;
		
		COLOR Shadow = 0xFFFF;

		if( m_pValue ) {

			Data = m_pValue->ExecVal();

			Type = m_pValue->GetType();
			}

		FindColors( pGDI,
			    Color,
			    Shadow,
			    Data,
			    Type,
			    FALSE
			    );

		if( m_Content > 0 ) {

			CString Label = FindLabelText();

			int     xSize = GetTextWidth(Label);

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					break;
				
				case 1:
					Work.x1 -= xSize;
					break;
				
				case 2:
					Work.x1 -= xSize;
					break;
				
				case 3:
					Work.x2 += xSize;
					break;
				}

			Work.y2 += ySize;

			DrawLabel(pGDI, Work);

			Work.y1  = Work.y2;
			}

		if( m_Content < 2 ) {

			DWORD Data    = 0;

			UINT  Type    = typeVoid;

			CString Value = FindValueText(Data, Type, fmtPad);

			int     xSize = GetTextWidth(Value);

			Work.x1 = m_Origin.x + dx[m_Orient];

			Work.x2 = Work.x1;

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					break;
				
				case 1:
					Work.x1 -= xSize;
					break;
				
				case 2:
					Work.x1 -= xSize;
					break;
				
				case 3:
					Work.x2 += xSize;
					break;
				}

			Work.y2 += ySize;

			DrawValue(pGDI, Work);
			}
		}
	}

// End of File
