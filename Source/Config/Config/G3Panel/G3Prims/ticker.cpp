
#include "intern.hpp"

#include "ticker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Alarm Ticker
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyAlarmTicker, CPrim);

// Constructor

CPrimLegacyAlarmTicker::CPrimLegacyAlarmTicker(void)
{
	m_uType           = 0x21;
	m_Font            = fontHei16;	
	m_pEdge           = New CPrimPen;
	m_NoAlarm         = MAKELONG(GetRGB( 0, 0, 0), GetRGB( 0,31, 0));
	m_Alarm           = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0, 0));
	m_Accept          = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31, 0));
	m_UseNoAlarmLabel = 1;
	m_IncCount        = 1;
	m_IncTime         = 1;
	m_UsePriority     = 0;
	m_AcceptBlink     = 0;
	m_PC1             = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0, 0));
	m_PC2             = MAKELONG(GetRGB(31,31,31), GetRGB( 0, 0, 0));
	m_PC3             = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31, 0));
	m_PC4             = MAKELONG(GetRGB(31,31,31), GetRGB( 0, 0,31));
	m_PC5             = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0,31));
	m_PC6             = MAKELONG(GetRGB(31,31,31), GetRGB( 0,31,31));
	m_PC7             = MAKELONG(GetRGB(31,31,31), GetRGB(15,15,15));
	m_PC8             = MAKELONG(GetRGB(31,31,31), GetRGB( 8, 8, 8));
	m_pNoAlarmLabel   = NULL;
	m_pFormat         = New CDispFormatTimeDate;
	m_EnableBlink     = 1;
	m_pBlinkRate      = NULL;
	m_pAlarmTransition= NULL;
	}

// Initial Values

void CPrimLegacyAlarmTicker::SetInitValues(void)
{
	CPrim::SetInitValues();

	SetInitial(L"NoAlarmLabel",    m_pNoAlarmLabel, L"\"No Active Alarms\"");
	SetInitial(L"BlinkRate",       m_pBlinkRate, 500);
	SetInitial(L"AlarmTransition", m_pAlarmTransition, 2);
	}

// UI Update

void CPrimLegacyAlarmTicker::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"UsePriority" ) {

		DoEnables(pHost);
		}

	if( Tag == L"IncTime" ) {

		DoEnables(pHost);
		}

	if( Tag == L"UseNoAlarmLabel" ) {

		DoEnables(pHost);
		}

	if ( Tag == L"EnableBlink" ) {
		
		DoEnables(pHost);
		}

	CPrim::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimLegacyAlarmTicker::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"NoAlarmLabel" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrim::GetTypeData(Tag, Type);
	}

// Overridables

void CPrimLegacyAlarmTicker::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	pGDI->SetBrushFore(HIWORD(m_NoAlarm));

	pGDI->FillRect(PassRect(Rect));

	if( m_UseNoAlarmLabel == 1 ) {

		Rect.left   += m_Margin.left;

		Rect.right  -= m_Margin.right;
		
		Rect.top    += m_Margin.top;
		
		Rect.bottom -= m_Margin.bottom;

		SelectFont(pGDI, m_Font);

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SetTextFore(LOWORD(m_NoAlarm));

		CString Text = spaceHair + m_pNoAlarmLabel->GetText() + spaceHair;

		int cy = pGDI->GetTextHeight(Text);

		int xp = Rect.x1;

		int yp = (Rect.y2 + Rect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, UniVisual(Text));
		}
	}

void CPrimLegacyAlarmTicker::SetInitState(void)
{
	CPrim::SetInitState();

	m_Margin.top      = 9;

	m_Margin.left     = 9;

	m_Margin.bottom   = 9;

	m_Margin.right    = 9;

	m_pEdge->m_Corner = 1;

	SetInitSize(2 + GetTextWidth(m_pNoAlarmLabel->GetText()));
	}

void CPrimLegacyAlarmTicker::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_Font);
	}

void CPrimLegacyAlarmTicker::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_Font, uOld, uNew);
	}

// Download Support

BOOL CPrimLegacyAlarmTicker::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pEdge);
	
	Init.AddWord(WORD(m_Font));

	Init.AddByte(BYTE(m_IncCount));

	Init.AddByte(BYTE(m_IncTime));
	
	Init.AddLong(LONG(m_NoAlarm));

	Init.AddLong(LONG(m_Accept));

	Init.AddByte(BYTE(m_AcceptBlink));

	Init.AddByte(BYTE(m_UsePriority));

	if( m_UsePriority ) {

		Init.AddLong(LONG(m_PC1));

		Init.AddLong(LONG(m_PC2));

		Init.AddLong(LONG(m_PC3));

		Init.AddLong(LONG(m_PC4));

		Init.AddLong(LONG(m_PC5));

		Init.AddLong(LONG(m_PC6));

		Init.AddLong(LONG(m_PC7));

		Init.AddLong(LONG(m_PC8));
		}
	else {
		Init.AddLong(LONG(m_Alarm));
		}

	Init.AddByte(BYTE(m_UseNoAlarmLabel));
	
	Init.AddItem(itemVirtual, m_pNoAlarmLabel);

	Init.AddItem(itemSimple, m_pFormat);
	
	Init.AddWord(WORD(m_Margin.left));
	Init.AddWord(WORD(m_Margin.top));
	Init.AddWord(WORD(m_Margin.right));
	Init.AddWord(WORD(m_Margin.bottom));

	Init.AddItem(itemVirtual, m_pBlinkRate);
	Init.AddItem(itemVirtual, m_pAlarmTransition);
	Init.AddByte(BYTE(m_EnableBlink));

	return TRUE;
	}

// Meta Data

void CPrimLegacyAlarmTicker::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddObject (Edge);
	Meta_AddInteger(Font);
	Meta_AddInteger(NoAlarm);
	Meta_AddInteger(Alarm);
	Meta_AddInteger(Accept);
	Meta_AddInteger(AcceptBlink);
	Meta_AddInteger(UseNoAlarmLabel);
	Meta_AddInteger(IncCount);
	Meta_AddInteger(IncTime);
	Meta_AddVirtual(NoAlarmLabel);
	Meta_AddInteger(UsePriority);
	Meta_AddInteger(PC1);
	Meta_AddInteger(PC2);
	Meta_AddInteger(PC3);
	Meta_AddInteger(PC4);
	Meta_AddInteger(PC5);
	Meta_AddInteger(PC6);
	Meta_AddInteger(PC7);
	Meta_AddInteger(PC8);
	Meta_AddObject (Format);
	Meta_AddRect   (Margin);
	Meta_AddInteger(EnableBlink);
	Meta_AddVirtual(BlinkRate);
	Meta_AddVirtual(AlarmTransition);
	
	Meta_SetName((IDS_ALARM_TICKER));	
	}

// Implementation

void CPrimLegacyAlarmTicker::DoEnables(IUIHost *pHost)
{
	BOOL fPriority = (m_UsePriority == 1);
	
	pHost->EnableUI(m_pFormat,        m_IncTime);

	pHost->EnableUI(L"Alarm",        !fPriority);

	pHost->EnableUI(L"NoAlarmLabel",  m_UseNoAlarmLabel == 1);

	pHost->EnableUI(L"BlinkRate",     m_EnableBlink == 1);

	pHost->EnableUI(L"AcceptBlink",   m_EnableBlink == 1);

	for( UINT n = 0; n < 8; n ++ ) {
	
		pHost->EnableUI(CPrintf(L"PC%d", n+1),  fPriority);
		}
	}

int CPrimLegacyAlarmTicker::GetTextHeight(void)const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = pFonts->GetHeight(m_Font);

	nSize += m_Margin.top;

	nSize += m_Margin.bottom;

	return nSize;
	}

int CPrimLegacyAlarmTicker::GetTextWidth(PCTXT pText) const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = pFonts->GetWidth(m_Font, pText);

	nSize += m_Margin.left;

	nSize += m_Margin.right;

	return nSize;
	}

BOOL CPrimLegacyAlarmTicker::SelectFont(IGDI *pGDI, UINT Font)
{
	if( Font < 0x100 ) {

		pGDI->SelectFont(Font);

		return TRUE;
		}

	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	return pFonts->Select(pGDI, Font);
	}

// End of File
