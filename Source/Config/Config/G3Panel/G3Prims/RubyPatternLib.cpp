
#include "intern.hpp"

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Library
//

// Static Data

BOOL  CRubyPatternLib::m_ShadeMode = 0;

COLOR CRubyPatternLib::m_ShadeCol1 = 0;

COLOR CRubyPatternLib::m_ShadeCol2 = 0;

// Dynamic Class

AfxImplementDynamicClass(CRubyPatternLib, CMetaItem);

// Constructors

CRubyPatternLib::CRubyPatternLib(void)
{
	for( UINT n = 0; n < 128; n++ ) {

		m_List.Append(0);
		}

	m_pWin  = Create_GdiWindowsA888(32, 32);

	m_pGdi  = m_pWin->GetGdi();

	m_pData = NULL;

	m_uData = 0;
	}

// Destructors

CRubyPatternLib::~CRubyPatternLib(void)
{
	if( m_pGdi ) {

		m_pGdi->Release();
		}

	delete [] m_pData;
	}

// Attributes

UINT CRubyPatternLib::GetCount(void) const
{
	return 21 + 33;
	}

UINT CRubyPatternLib::EnumCode(UINT n) const
{
	static UINT Table[] = {

		brushNull,
		brushFore,

		16,
		17,
		18,
		19,

		20,
		21,
		22,

		brushGray25,
		brushGray50,
		brushGray75,
		brushHatchF,
		brushHatchB,
		brushHatchFB,
		brushHatchBB,
		brushHatchH,
		brushHatchV,
		brushHatchX,
		brushHatchD,
		brushWave
		};

	if( n >= elements(Table) ) {

		return 128 + n - elements(Table);
		}

	return Table[n];
	}

CString CRubyPatternLib::EnumName(UINT n) const
{
	static CString Table[] = {

		CString(IDS_PUI_FILL_NO),
		CString(IDS_PUI_WHITE),

		CString(IDS_GRADUATED_FILL_1),
		CString(IDS_GRADUATED_FILL_2),
		CString(IDS_GRADUATED_FILL_3),
		CString(IDS_GRADUATED_FILL_4),

		CString(IDS_SHINY_AND_CHROME),
		CString(IDS_HORZ_CYLINDER),
		CString(IDS_VERT_CYLINDER),

		CString(IDS_PUI_GRAY_25),
		CString(IDS_PUI_GRAY_50),
		CString(IDS_PUI_GRAY_75),
		CString(IDS_PUI_H_FOR),
		CString(IDS_PUI_H_BACK),
		CString(IDS_HATCH_FORWARD),
		CString(IDS_HATCH_BACKWARD),
		CString(IDS_PUI_H_HOR),
		CString(IDS_PUI_H_VER),
		CString(IDS_PUI_H_ORTH),
		CString(IDS_PUI_H_DIAG),
		CString(IDS_PUI_WAVE)
		};

	if( n >= elements(Table) ) {

		CString Name = Sym_GetDesc(35, n - elements(Table));

		UINT n = 0;

		for(;;) {

			UINT p = Name.Find(L' ', n);

			if( p == NOTHING ) {

				break;
				}

			Name.SetAt(p + 1, wtoupper(Name.GetAt(p + 1)));

			n = p + 1;
			}

		Name.Replace(L"Horizontal", L"Horz");

		Name.Replace(L"Vertical",   L"Vert");

		Name.Replace(L" Texture",   L"");

		return Name;
		}

	return Table[n];
	}

// Operations

void CRubyPatternLib::ClearList(void)
{
	for( UINT n = 0; n < 128; n++ ) {

		m_List.SetAt(n, 0);
		}
	}

void CRubyPatternLib::Register(UINT uCode)
{
	if( uCode >= 128 ) {

		UINT  uCat  = 35;

		UINT  uSlot = uCode - 128;

		if( !m_List[uSlot] ) {

			DWORD dwHand = Sym_GetHandle(uCat, uSlot);

			UINT  uType  = 1;

			UINT  uFile  = m_pImages->LoadFromFile(CPrintf(L"SYM:%u,%u,%u", uCat, dwHand, uType), FALSE);

			if( uFile < NOTHING ) {

				m_List.SetAt(uSlot, WORD(uFile + 1));
				}
			}
		}
	}

void CRubyPatternLib::GetRefs(CPrimRefList &Refs, UINT uCode)
{
	if( uCode >= 128 ) {

		UINT uSlot = uCode - 128;

		UINT uFile = m_List[uSlot] - 1;

		Refs.Insert(uFile | refImage);
		}
	}

// Gdi Configuration

PSHADER CRubyPatternLib::SetGdi(IGdi *pGdi, UINT uCode, COLOR Fore, COLOR Back)
{
	if( uCode <= 14 ) {

		pGdi->SelectBrush(uCode);

		pGdi->SetBrushFore(Fore);

		pGdi->SetBrushBack(Back);

		return NULL;
		}

	if( uCode <= 19 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeMode = uCode % 4 / 2;

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return (uCode % 2) ? ShaderLinear : ShaderMiddle;
		}

	if( uCode == 20 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return ShaderChrome;
		}

	if( uCode == 21 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return ShaderHorzCylinder;
		}

	if( uCode == 22 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return ShaderVertCylinder;
		}

	if( uCode <= 160 ) {

		int cx, cy;

		HBITMAP hSource = Sym_LoadBitmap(35, uCode - 128, cx, cy);

		if( hSource ) {

			if( TRUE ) {

				PBYTE   pBits = NULL;

				CBitmap Bitmap(+cx, -cy, 1, 32, pBits);

				CMemoryDC DC;

				DC.Select(Bitmap);

				DC.BitBlt( 0, 0, cx, cy,
					   CBitmap::FromHandle(hSource),
					   0, 0,
					   SRCCOPY
					   );

				DC.Deselect();

				DeleteObject(hSource);

				UINT uCopy = cx * cy * 4;

				if( uCopy > m_uData ) {

					delete m_pData;

					m_uData = uCopy;

					m_pData = New BYTE [ m_uData ];
					}

				// cppcheck-suppress nullPointer

				memcpy(m_pData, pBits, uCopy);
				}

			pGdi->SetBrushBits(m_pData, cx, cy);

			return NULL;
			}

		pGdi->SelectBrush(brushNull);

		return NULL;
		}

	pGdi->SelectBrush(brushNull);

	return NULL;
	}

// Windows Preview

void CRubyPatternLib::Draw(CDC &DC, CRect Rect, UINT uCode, COLOR Fore, COLOR Back)
{
	if( uCode >= 128 ) {

		HBITMAP hBitmap = Sym_LoadBitmap(35, uCode - 128);

		DC.BitBlt( Rect,
			   CBitmap::FromHandle(hBitmap),
			   CPoint(0, 0),
			   SRCCOPY
			   );

		DeleteObject(hBitmap);

		return;
		}

	if( uCode ) {

		if( Fore == GetRGB(31,31,31) ) {

			if( uCode == brushFore ) {

				DC.FrameRect(Rect--, afxBrush(BLACK));
				}
			else {
				if( Back == GetRGB(31,31,31) ) {

					DC.FrameRect(Rect--, afxBrush(BLACK));
					}
				}
			}

		PSHADER pShader = SetGdi(m_pGdi, uCode, Fore, Back);

		m_pGdi->ShadeRect(0, 0, Rect.cx(), Rect.cy(), pShader);

		DC.BitBlt( Rect,
			   CDC::FromHandle(m_pWin->GetDC()),
			   CPoint(0, 0),
			   SRCCOPY
			   );

		return;
		}

	DC.FrameRect(Rect--, afxBrush(BLACK));

	DC.MoveTo(Rect.GetTopRight());

	DC.LineTo(Rect.GetBottomLeft());
	}

// Persistance

void CRubyPatternLib::Init(void)
{
	FindManager();

	ClearList();

	CMetaItem::Init();
	}

void CRubyPatternLib::PostLoad(void)
{
	FindManager();

	CMetaItem::PostLoad();
	}

// Download Support

BOOL CRubyPatternLib::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	for( UINT n = 0; n < 128; n++ ) {

		if( m_List[n] ) {

			UINT            uFile = m_List[n] - 1;

			CImageFileItem *pFile = m_pImages->m_pFiles->GetItem(uFile);

			if( pFile ) {

				CImageOpts      Opts;

				Opts.m_Size = pFile->GetNativeSize();

				Init.AddWord(WORD(m_pImages->FindView(uFile , Opts)));

				continue;
				}
			}

		Init.AddWord(0);
		}

	return TRUE;
	}

// Implementation

void CRubyPatternLib::FindManager(void)
{
	CUISystem * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	m_pImages           = pSystem->m_pUI->m_pImages;
	}

// Meta Data

void CRubyPatternLib::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddWide(List);

	Meta_SetName((IDS_PATTERN_LIBRARY));
	}

// Color Mixing

COLOR CRubyPatternLib::Mix16(COLOR a, COLOR b, int p, int c)
{
	if( c ) {

		if( p <= 0 ) {

			return a;
			}

		if( p >= c ) {

			return b;
			}

		int ra = GetRED  (a);
		int ga = GetGREEN(a);
		int ba = GetBLUE (a);

		int rb = GetRED  (b);
		int gb = GetGREEN(b);
		int bb = GetBLUE (b);

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return GetRGB(rc, gc, bc);
		}

	return a;
	}

DWORD CRubyPatternLib::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	if( c ) {

		if( p <= 0 ) {

			return MAKELONG(MAKEWORD(ba, ga), MAKEWORD(ra, 0xFF));
			}

		if( p >= c ) {

			return MAKELONG(MAKEWORD(bb, gb), MAKEWORD(rb, 0xFF));
			}

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
		}

	return MAKELONG(MAKEWORD(ba, ga), MAKEWORD(ra, 0xFF));
	}

// Shaders

BOOL CRubyPatternLib::ShaderLinear(IGDI *pGdi, int p, int c)
{
	static COLOR q16 = 0;
	static DWORD q32 = 0;
	static BOOL  f32 = 0;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k32 - q32 ) {

				pGdi->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k16 - q16 ) {

				pGdi->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGdi->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	return m_ShadeMode;
	}

BOOL CRubyPatternLib::ShaderMiddle(IGDI *pGdi, int p, int c)
{
	static COLOR q16 = 0;
	static DWORD q32 = 0;
	static BOOL  f32 = 0;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol2, m_ShadeCol1, abs(c/2-p), c/2);

			if( k32 - q32 ) {

				pGdi->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(m_ShadeCol2, m_ShadeCol1, abs(c/2-p), c/2);

			if( k16 - q16 ) {

				pGdi->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGdi->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	return m_ShadeMode;
	}

BOOL CRubyPatternLib::ShaderTable(IGdi *pGdi, int p, int c, int size, int const *pos, int const *col)
{
	static COLOR q16 = 0;
	static DWORD q32 = 0;
	static BOOL  f32 = 0;

	static COLOR ca  = 0;
	static COLOR cb  = 0;
	static UINT  n   = 0;
	
	if( c ) {

		int pc = (c - p) * 1000 / c;

		if( n ) {

			if( pc <= pos[n] ) {

				while( pc <= pos[n] ) {
			
					n--;
					}

				ca = Mix16(m_ShadeCol1, m_ShadeCol2, col[n+0], 31);
				
				cb = Mix16(m_ShadeCol1, m_ShadeCol2, col[n+1], 31);
				}
			}

		if( f32 ) {

			DWORD k32 = Mix32(ca, cb, pc - pos[n], pos[n+1] - pos[n]);

			if( k32 - q32 ) {

				pGdi->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(ca, cb, pc - pos[n], pos[n+1] - pos[n]);

			if( k16 - q16 ) {

				pGdi->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGdi->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	n   = size - 1;

	return FALSE;
	}

BOOL CRubyPatternLib::ShaderChrome(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0, 250, 300, 400, 500, 900, 1000 };
	static int col[] = { 0,  10,   2,  31,  29,  16,   31 };

	BOOL f = ShaderTable(pGdi, p, c, elements(pos), pos, col);

	return c ? f : FALSE;
	}

BOOL CRubyPatternLib::ShaderHorzCylinder(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0,	100, 200, 300, 400, 500, 600, 700, 800,	900, 1000 };
	static int col[] = { 0,	 19,  25,  28,	30,  31,  30,  28,  25,	 19,	0 };

	BOOL f = ShaderTable(pGdi, p, c, elements(pos), pos, col);

	return c ? f : FALSE;
	}

BOOL CRubyPatternLib::ShaderVertCylinder(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0,	100, 200, 300, 400, 500, 600, 700, 800,	900, 1000 };
	static int col[] = { 0,	 19,  25,  28,	30,  31,  30,  28,  25,	 19,	0 };

	BOOL f = ShaderTable(pGdi, p, c, elements(pos), pos, col);

	return c ? f : TRUE;
	}

// End of File
