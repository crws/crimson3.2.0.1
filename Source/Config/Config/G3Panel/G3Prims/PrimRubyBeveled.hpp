
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBeveled_HPP
	
#define	INCLUDE_PrimRubyBeveled_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyTrimmed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Beveled Rectangle Primitive
//

class CPrimRubyBeveled : public CPrimRubyTrimmed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyBeveled(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
