			   
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Fixed Color
//

class CUITextFixedColor : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextFixedColor(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL    Parse(CError &Error, CString Text, UINT &Color);
		CString Format(COLOR Color);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Fixed Color
//

class CUIFixedColor : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIFixedColor(void);

	protected:
		// Data Members
		CString	         m_Label;
		CLayItemText   * m_pTextLayout;
		CLayItem       * m_pListLayout;
		CLayItem       * m_pPickLayout;
		CStatic	       * m_pTextCtrl;
		CColorComboBox * m_pListCtrl;
		CButton	       * m_pPickCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnShow(BOOL fShow);
		void OnMove(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect FindListRect(void);
		void  LoadLists(void);
		BOOL  PickColor(COLOR &Color);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Fixed Color
//

// Dynamic Class

AfxImplementDynamicClass(CUITextFixedColor, CUITextElement);

// Constructor

CUITextFixedColor::CUITextFixedColor(void)
{
	m_uFlags = textEdit | textExpand;
	}

// Overridables

void CUITextFixedColor::OnBind(void)
{
	CUITextElement::OnBind();

	m_uWidth = 20;

	m_uLimit = 20;
	}

CString CUITextFixedColor::OnGetAsText(void)
{
	COLOR   Data = COLOR(m_pData->ReadInteger(m_pItem));

	CString Text = Format(Data);

	return Text;
	}

UINT CUITextFixedColor::OnSetAsText(CError &Error, CString Text)
{
	UINT Data = 0;

	if( Parse(Error, Text, Data) ) {
		
		UINT Prev = m_pData->ReadInteger(m_pItem);

		if( Prev == Data ) {

			return saveSame;
			}

		m_pData->WriteInteger(m_pItem, Data);

		return saveChange;
		}

	return saveError;
	}

// Implementation

BOOL CUITextFixedColor::Parse(CError &Error, CString Text, UINT &Color)
{
	Text.TrimBoth();

	if( Text.StartsWith(L"0x") ) {

		PCTXT pHex = PCTXT(Text) + 2;

		Color = wcstol(pHex, NULL, 16);

		return TRUE;
		}

	Error.Set(CString(IDS_YOU_HAVE_ENTERED));
	
	return FALSE;
	}

CString CUITextFixedColor::Format(COLOR Color)
{
	return CPrintf(L"0x%4.4X", Color);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Fixed Color
//

// Dynamic Class

AfxImplementDynamicClass(CUIFixedColor, CUIControl);

// Constructor

CUIFixedColor::CUIFixedColor(void)
{
	m_pTextLayout = NULL;

	m_pListLayout = NULL;

	m_pPickLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pListCtrl   = New CColorComboBox(this);

	m_pPickCtrl   = New CButton;
	}

// Core Overridables

void CUIFixedColor::OnBind(void)
{
	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}

	CUIControl::OnBind();
	}

void CUIFixedColor::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pListLayout = New CLayItemText(L"0123456789012345", 4, 1);

	m_pPickLayout = New CLayItemText(CString(IDS_PICK), CSize(6, 4), CSize(1, 1));

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pListLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pPickLayout, horzLeft | vertCenter));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIFixedColor::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create(	m_Label,
				0,
				m_pTextLayout->GetRect(),
				Wnd,
				0
				);

	m_pListCtrl->Create(	WS_TABSTOP	   |
				WS_VSCROLL	   |
				CBS_DROPDOWNLIST   |
				CBS_OWNERDRAWFIXED,
				FindListRect(),
				Wnd,
				uID++
				);

	m_pPickCtrl->Create( CString(IDS_PICK),
			     WS_TABSTOP | BS_PUSHBUTTON,
			     m_pPickLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pListCtrl->SetFont(afxFont(Dialog));

	m_pPickCtrl->SetFont(afxFont(Dialog));

	LoadLists();

	AddControl(m_pTextCtrl);

	AddControl(m_pListCtrl);

	AddControl(m_pPickCtrl);
	}

void CUIFixedColor::OnShow(BOOL fShow)
{
	UINT uCount = m_List.GetCount();

	UINT uCode  = fShow ? SW_SHOW : SW_HIDE;

	for( UINT n = 0; n < uCount; n++ ) {

		CCtrlWnd *pCtrl = m_List[n];

		pCtrl->ShowWindow(uCode);
		}
	}

void CUIFixedColor::OnMove(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pListCtrl->MoveWindow(FindListRect(), TRUE);

	m_pPickCtrl->MoveWindow(m_pPickLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUIFixedColor::OnLoad(void)
{
	COLOR Data = COLOR(m_pData->ReadInteger(m_pItem));

	m_pListCtrl->SetColor(Data);
	}

UINT CUIFixedColor::OnSave(BOOL fUI)
{
	COLOR Data = m_pListCtrl->GetColor();

	DWORD Prev = m_pData->ReadInteger(m_pItem);

	if( Prev != Data ) {

		m_pData->WriteInteger(m_pItem, Data);

		return saveChange;
		}

	return saveSame;
	}

// Notification Handlers

BOOL CUIFixedColor::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		CString Text  = m_pText->GetAsText().Mid(2);

		COLOR   Color = COLOR(wcstol(Text, NULL, 16));

		if( PickColor(Color) ) {

			CString Text = CPrintf(L"0x%4.4X", Color);

			StdSave(TRUE, Text);

			ForceUpdate();

			m_pListCtrl->Invalidate(FALSE);
			
			return TRUE;
			}

		return FALSE;
		}

	if( uID == m_pListCtrl->GetID() ) {

		if( uCode == CBN_SELCHANGE ) {

			BOOL fHit = FALSE;

			if( uID == m_pListCtrl->GetID() ) {

				fHit = TRUE;
				}

			if( !m_fBusy ) {
				
				if( fHit ) {
					
					switch( SaveUI(FALSE) ) {

						case saveChange:
							
							return TRUE;

						case saveSame:
							
							return FALSE;
						}

					AfxAssert(FALSE);
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

CRect CUIFixedColor::FindListRect(void)
{
	CRect Rect  = m_pListLayout->GetRect();

	Rect.top    = Rect.top - 1;

	Rect.bottom = Rect.top + 8 * Rect.cy();

	return Rect;
	}

void CUIFixedColor::LoadLists(void)
{
	m_pListCtrl->LoadList();
	}

BOOL CUIFixedColor::PickColor(COLOR &Color)
{
	CColorDialog Dlg;

	Dlg.ShowRainbowOnly();

	Dlg.SetColor(C3GetWinColor(Color));

	if( Dlg.Execute(*m_pPickCtrl) ) {

		Color = C3GetGdiColor(Dlg.GetColor());

		CDatabase     *pDbase  = m_pItem->GetDatabase();

		CUISystem     *pSystem = (CUISystem *) pDbase->GetSystemItem();

		CColorManager *pColor  = pSystem->m_pColor;

		pColor->MarkColor(Color);

		m_pPickCtrl->SetFocus();

		return TRUE;
		}

	m_pPickCtrl->SetFocus();
	
	return FALSE;
	}

// End of File
