
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TICKER_HPP
	
#define	INCLUDE_TICKER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyAlarmTicker;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Alarm Ticker
//

class CPrimLegacyAlarmTicker : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyAlarmTicker(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimPen    * m_pEdge;
		UINT          m_Font;
		UINT	      m_NoAlarm;
		UINT	      m_Alarm;
		UINT	      m_Accept;
		UINT	      m_AcceptBlink;
		UINT	      m_UseNoAlarmLabel;
		UINT	      m_IncCount;
		UINT	      m_IncTime;
		UINT          m_UsePriority;
		UINT          m_PC1;
		UINT          m_PC2;
		UINT          m_PC3;
		UINT          m_PC4;
		UINT          m_PC5;
		UINT          m_PC6;
		UINT          m_PC7;
		UINT          m_PC8;
		CDispFormat * m_pFormat;
		CCodedText  * m_pNoAlarmLabel;
		UINT          m_EnableBlink;
		CCodedItem  * m_pBlinkRate;
		CCodedItem  * m_pAlarmTransition;
	
	protected:
		// Data Members
		CRect m_Margin;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		int  GetTextHeight(void)       const;
		int  GetTextWidth(PCTXT pText) const;
		BOOL SelectFont(IGDI *pGDI, UINT Font);
	};

// End of File

#endif
