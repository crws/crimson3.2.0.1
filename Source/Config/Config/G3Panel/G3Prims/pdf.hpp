
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PDF_HPP
	
#define	INCLUDE_PDF_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- PDF Viewer
//

class CPrimPDFViewer : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPDFViewer(void);

		// Initial Values
		void SetInitValues(void);

		// Overridables
		void SetInitState(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data
		UINT	      m_FontTitle;
		CCodedText  * m_pFile;
		CCodedText  * m_pTxtCannot;
		CCodedText  * m_pTxtWorking;
		CCodedText  * m_pBtnLeft;
		CCodedText  * m_pBtnRight;
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnZoomIn;
		CCodedText  * m_pBtnZoomOut;

	protected:
		// Data Members
		CRect          m_Margin;

		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);
		BOOL IsSupported(void);

		// Attributes
		int  GetTextWidth(PCTXT pText) const;
	};

// End of File

#endif
