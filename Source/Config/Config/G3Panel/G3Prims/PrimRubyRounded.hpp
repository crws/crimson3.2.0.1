
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyRounded_HPP
	
#define	INCLUDE_PrimRubyRounded_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyTrimmed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Rounded Rectangle Primitive
//

class CPrimRubyRounded : public CPrimRubyTrimmed
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyRounded(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
