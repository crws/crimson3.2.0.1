
#include "intern.hpp"

#include "geom.hpp"

#include "PrimRubyLine.hpp"

#include "PrimRubyTextBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrim, CCodedHost);

// Static Data

CPrimWidget * CPrim::m_pAdopter = NULL;

// Constructor

CPrim::CPrim(void)
{
	// LATER -- Add properties to lock size and position?

	m_uType = 0;

	m_RealRect.m_x1 = 0;
	m_RealRect.m_y1 = 0;
	m_RealRect.m_x2 = 0;
	m_RealRect.m_y2 = 0;

	m_pVisible = NULL;

	m_fPressed = FALSE;
	}

// Destructor

CPrim::~CPrim(void)
{
	}

// UI Update

void CPrim::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this || Tag == "PrimProp" || Tag == "FormatObject" || Tag == "ColorObject" ) {

		if( pHost->HasWindow() ) {

			CWnd &Wnd  = pHost->GetWindow();

			CWnd &Find = Wnd.GetParent(2);
		
			if( Find.IsKindOf(AfxRuntimeClass(CPrimDialog)) ) {

				CPrimDialog &Dlg = (CPrimDialog &) Find;

				Dlg.PrimChanged();
				}
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrim::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Visible" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Server Access

INameServer * CPrim::GetNameServer(CNameServer *pName)
{
	CPrimWidget *pWidget = FindWidget();

	if( pWidget ) {

		pName->SetFunc (pWidget);

		pName->SetIdent(pWidget);
		}

	if( CanBeAdopted() ) {

		pName->SetFunc (m_pAdopter);

		pName->SetIdent(m_pAdopter);
		}

	return pName;
	}

IDataServer * CPrim::GetDataServer(CDataServer *pData)
{
	CPrimWidget *pWidget = FindWidget();

	if( pWidget ) {

		pWidget->SetDataServer(pData);

		return pWidget;
		}

	if( CanBeAdopted() ) {

		m_pAdopter->SetDataServer(pData);

		return m_pAdopter;
		}

	return pData;
	}

// Attributes

R2R CPrim::GetReal(void) const
{
	return m_RealRect;
	}

CRect CPrim::GetRect(void) const
{
	CRect Rect;

	Rect.left   = m_DrawRect.x1;
	Rect.top    = m_DrawRect.y1;
	Rect.right  = m_DrawRect.x2;
	Rect.bottom = m_DrawRect.y2;

	return Rect;
	}

CRect CPrim::GetNormRect(void) const
{
	CRect Rect = GetRect();

	Rect.Normalize();

	if( !Rect.cx() ) Rect.right  += 1;

	if( !Rect.cy() ) Rect.bottom += 1;

	return Rect;
	}

BOOL CPrim::IsSet(void) const
{
	return IsKindOf(AfxRuntimeClass(CPrimSet));
	}

BOOL CPrim::IsMove(void) const
{
	return IsKindOf(AfxRuntimeClass(CPrimMove));
	}

BOOL CPrim::IsGroup(void) const
{
	return IsKindOf(AfxRuntimeClass(CPrimGroup));
	}

BOOL CPrim::IsWidget(void) const
{
	return IsKindOf(AfxRuntimeClass(CPrimWidget));
	}

BOOL CPrim::IsLine(void) const
{
	return IsKindOf(AfxRuntimeClass(CPrimLine)) || IsKindOf(AfxRuntimeClass(CPrimRubyLine));
	}

BOOL CPrim::IsTextBox(void) const
{
	return IsKindOf(AfxRuntimeClass(CPrimTextBox)) || IsKindOf(AfxRuntimeClass(CPrimRubyTextBox));
	}

BOOL CPrim::HasImages(void) const
{
	return CString(GetClassName()).Find(L"Image") < NOTHING;
	}

BOOL CPrim::HasProps(void) const
{
	// TODO -- Is there anything that doesn't have props?

	return TRUE;
	}

BOOL CPrim::IsPressed(void) const
{
	return m_fPressed;
	}

// Operations

BOOL CPrim::HitTest(CPoint Pos)
{
	P2 p2 = { Pos.x, Pos.y };

	return HitTest(p2);
	}

void CPrim::SetReal(R2R const &NewRect)
{
	m_RealRect = NewRect;

	UpdateLayout();
	}

void CPrim::SetRect(CRect const &NewRect)
{
	m_RealRect.m_x1 = NewRect.left;

	m_RealRect.m_y1 = NewRect.top;

	m_RealRect.m_x2 = NewRect.right;

	m_RealRect.m_y2 = NewRect.bottom;

	UpdateLayout();
	}

void CPrim::SetPressed(BOOL fPressed)
{
	m_fPressed = fPressed;
	}

// Overridables

BOOL CPrim::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

CRect CPrim::GetBoundingRect(void)
{
	return GetNormRect();
	}

void CPrim::Draw(IGDI *pGDI, UINT uMode)
{
	}

void CPrim::SetRect(CRect const &OldRect, CRect const &NewRect)
{
	// This function is called to adjust the position of the primitive
	// during a move or a size. The old and new rectangles may reflect
	// a group or a multiple item selection, so they may be larger than
	// this primitive. We thus consider how this item would scale in
	// size and position within the larger rectangles.

	m_RealRect.m_x1 -= OldRect.left;
	m_RealRect.m_y1 -= OldRect.top;
	m_RealRect.m_x2 -= OldRect.left;
	m_RealRect.m_y2 -= OldRect.top;

	if( NewRect.cx() != OldRect.cx() ) {

		m_RealRect.m_x1 *= NewRect.cx();
		m_RealRect.m_x2 *= NewRect.cx();
		m_RealRect.m_x1 /= OldRect.cx();
		m_RealRect.m_x2 /= OldRect.cx();
		}

	if( NewRect.cy() != OldRect.cy() ) {

		m_RealRect.m_y1 *= NewRect.cy();
		m_RealRect.m_y2 *= NewRect.cy();
		m_RealRect.m_y1 /= OldRect.cy();
		m_RealRect.m_y2 /= OldRect.cy();
		}

	m_RealRect.m_x1 += NewRect.left;
	m_RealRect.m_y1 += NewRect.top;
	m_RealRect.m_x2 += NewRect.left;
	m_RealRect.m_y2 += NewRect.top;

	UpdateLayout();
	}

void CPrim::SetInitState(void)
{
	}

void CPrim::UpdateLayout(void)
{
	FindDrawRect();

	FindTextRect();
	}

void CPrim::FindTextRect(void)
{
	}

CSize CPrim::GetMinSize(IGDI *pGDI)
{
	return CSize(4, 4);
	}

BOOL CPrim::GetHand(UINT uHand, CPrimHand &Hand)
{
	return FALSE;
	}

void CPrim::SetHand(BOOL fInit)
{
	FindTextRect();
	}

void CPrim::GetRefs(CPrimRefList &Refs)
{
	}

void CPrim::EditRef(UINT uOld, UINT uNew)
{
	}

UINT CPrim::GetBindMode(void)
{
	return 0;
	}

BOOL CPrim::GetBindState(void)
{
	return FALSE;
	}

BOOL CPrim::GetBindClass(CString &Class)
{
	return FALSE;
	}

BOOL CPrim::BindToTag(CString Top, CString Tag)
{
	return TRUE;
	}

void CPrim::ClearBinding(void)
{
	}

void CPrim::Validate(BOOL fExpand)
{
	}

void CPrim::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	}

void CPrim::SetTextColor(COLOR Color)
{
	}

BOOL CPrim::StepAddress(void)
{
	return FALSE;
	}

BOOL CPrim::HasError(void)
{
	return HasBrokenCode();
	}

BOOL CPrim::IsSupported(void)
{
	return TRUE;
	}

void CPrim::PostConvert(void)
{
	}

// Item Naming

CString CPrim::GetHumanName(void) const
{
	CString    Name  = CMetaItem::GetHumanName();

	CPrimList *pList = (CPrimList *) GetParent();

	if( pList->IsKindOf(AfxRuntimeClass(CPrimList)) ) {

		UINT uPos = pList->GetItemIndex(this);

		Name += CPrintf(L" %u", uPos);
		}

	return Name;
	}

// Persistance

void CPrim::Init(void)
{
	CCodedHost::Init();

	m_RealRect.m_x1 = 0;
	m_RealRect.m_y1 = 0;
	m_RealRect.m_x2 = 57;
	m_RealRect.m_y2 = 57;

	SetInitState();

	UpdateLayout();
	}

void CPrim::Load(CTreeFile &Tree)
{
	// We don't save DrawRect or InitRect anyomre, but have to
	// handle older database by loading DrawRect into RealRect.

	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString		 Name  = Tree.GetName();

		CMetaData const *pMeta = NULL;

		if( Name == L"DrawRect" ) {

			Name = L"RealRect";
			}

		if( Name == L"InitRect" ) {

			CRect Rect;

			Tree.GetValue(Rect);

			continue;
			}

		if( (pMeta = m_pList->FindData(Name)) ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

void CPrim::PostLoad(void)
{
	// Check for old version with rect in DrawRect !!!!

	CCodedHost::PostLoad();

	UpdateLayout();
	}

// Download Support

BOOL CPrim::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_uType));

	CCodedHost::MakeInitData(Init);

	Init.AddWord(WORD(m_DrawRect.x1));
	Init.AddWord(WORD(m_DrawRect.y1));
	Init.AddWord(WORD(m_DrawRect.x2));
	Init.AddWord(WORD(m_DrawRect.y2));

	Init.AddItem(itemVirtual, m_pVisible);

	return TRUE;
	}

// Meta Data

void CPrim::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddR2R    (RealRect);
//	Meta_AddRect   (DrawRect);
//	Meta_AddRect   (InitRect);
	Meta_AddVirtual(Visible);

	Meta_SetName((IDS_PRIMITIVE));
	}

// Widget Location

CPrimWidget * CPrim::FindWidget(void)
{
	CLASS   Widget = AfxRuntimeClass(CPrimWidget);

	CLASS   Group  = AfxRuntimeClass(CPrimSet);

	CItem * pItem  = this;

	while( pItem ) {

		pItem = pItem->GetParent(2);

		if( pItem->IsKindOf(Widget) ) {

			return (CPrimWidget *) pItem;
			}

		if( pItem->IsKindOf(Group) ) {

			continue;
			}

		break;
		}

	return NULL;
	}

// Implementation

void CPrim::SetInitSize(int cx)
{
	m_RealRect.m_x2 = m_RealRect.m_x1 + cx;
	}

void CPrim::SetInitSize(int cx, int cy)
{
	m_RealRect.m_x2 = m_RealRect.m_x1 + cx;

	m_RealRect.m_y2 = m_RealRect.m_y1 + cy;
	}

void CPrim::SetInitSize(CSize Size)
{
	m_RealRect.m_x2 = m_RealRect.m_x1 + Size.cx;

	m_RealRect.m_y2 = m_RealRect.m_y1 + Size.cy;
	}

void CPrim::FindDrawRect(void)
{
	// DrawRect is now calculated from RealRect, and only exists
	// as a distinct member to avoid a lot of modification to the
	// existing code. The only way it should be set is by setting
	// RealRect and then calling this function.

	m_DrawRect.x1 = int(m_RealRect.m_x1 + 0.5);
	m_DrawRect.y1 = int(m_RealRect.m_y1 + 0.5);
	m_DrawRect.x2 = int(m_RealRect.m_x2 + 0.5);
	m_DrawRect.y2 = int(m_RealRect.m_y2 + 0.5);
	}

BOOL CPrim::GetFontRef(CPrimRefList &Refs, UINT Font)
{
	Refs.Insert(Font | refFont);

	return TRUE;
	}

BOOL CPrim::GetImageRef(CPrimRefList &Refs, UINT Image)
{
	if( Image < NOTHING ) {

		Refs.Insert(Image | refImage);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrim::EditFontRef(UINT &Font, UINT uOld, UINT uNew)
{
	if( uOld == NOTHING ) {

		if( Font >= 0x100 ) {

			Font |= refPending;
			}

		return TRUE;
		}

	if( (Font | refFont) == uOld ) {

		Font = uNew;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrim::EditImageRef(UINT &Image, UINT uOld, UINT uNew)
{
	if( uOld == NOTHING ) {

		if( Image < NOTHING ) {

			Image |= refPending;
			}

		return TRUE;
		}

	if( (Image | refImage) == uOld ) {

		Image = uNew;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrim::CanBeAdopted(void)
{
	// NOTE -- This is subtle. It is used to allow prims to access
	// an adopted parent when being dragged so as to make sure their
	// widget properties bind. We can only use this if the adopter is
	// not us or one of our children (!) or we'll end up in a loop. The
	// latter rejected condition occurs when the adopter is a widget
	// within another widget, as the outer widget tries to be adopted!

	if( m_pAdopter ) {

		CItem *pScan = m_pAdopter;

		while( pScan ) {

			if( pScan == this ) {

				return FALSE;
				}

			pScan = pScan->GetParent();
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrim::SelectFont(IGDI *pGDI, UINT Font)
{
	if( Font < 0x100 ) {

		pGDI->SelectFont(Font);

		return TRUE;
		}

	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	return pFonts->Select(pGDI, Font);
	}

// End of File
