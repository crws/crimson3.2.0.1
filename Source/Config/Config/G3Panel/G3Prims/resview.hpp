
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RESVIEW_HPP
	
#define	INCLUDE_RESVIEW_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "geom.hpp"
#include "poly.hpp"
#include "bar.hpp"
#include "bevel.hpp"
#include "button.hpp"
#include "dial.hpp"
#include "graph.hpp"
#include "legacy.hpp"
#include "scale.hpp"
#include "select.hpp"
#include "slider.hpp"
#include "viewer.hpp"
#include "ticker.hpp"
#include "quick.hpp"
#include "pdf.hpp"
#include "camera.hpp"

#include "PrimRubyLine.hpp"
#include "PrimRubyGeom.hpp"
#include "PrimRubyRect.hpp"
#include "PrimRubyEllipse.hpp"
#include "PrimRubyRounded.hpp"
#include "PrimRubyBeveled.hpp"
#include "PrimRubyFilleted.hpp"
#include "PrimRubyPolygon.hpp"
#include "PrimRubyPolygon3.hpp"
#include "PrimRubyPolygon5.hpp"
#include "PrimRubyPolygon6.hpp"
#include "PrimRubyPolygon8.hpp"
#include "PrimRubyStar.hpp"
#include "PrimRubyStar4.hpp"
#include "PrimRubyStar5.hpp"
#include "PrimRubyStar6.hpp"
#include "PrimRubyStar8.hpp"
#include "PrimRubyWedge.hpp"
#include "PrimRubyArrow.hpp"
#include "PrimRubyBalloon.hpp"
#include "PrimRubyTriangle.hpp"
#include "PrimRubyParallelogram.hpp"
#include "PrimRubyTrapezium.hpp"
#include "PrimRubyConicalTank.hpp"
#include "PrimRubyRoundTank.hpp"
#include "PrimRubyQuadrant.hpp"
#include "PrimRubySemi.hpp"
#include "PrimRubyBevel.hpp"
#include "PrimRubyBevelButton.hpp"
#include "PrimRubyGradButton.hpp"
#include "PrimRubyTextBox.hpp"
#include "PrimRubyDataBox.hpp"
#include "PrimRubyTimeDate.hpp"
#include "PrimRubyAnimImage.hpp"
#include "PrimRubyGaugeTypeAR1.hpp"
#include "PrimRubyGaugeTypeAR2.hpp"
#include "PrimRubyGaugeTypeAR3.hpp"
#include "PrimRubyGaugeTypeAR4.hpp"
#include "PrimRubyGaugeTypeAR5.hpp"
#include "PrimRubyGaugeTypeAR6.hpp"
#include "PrimRubyGaugeTypeALV.hpp"
#include "PrimRubyGaugeTypeALH.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGraphicsResourceItem;
class CPrimResourceItem;
class CColorResourceItem;
class CSymbolResourceItem;

//////////////////////////////////////////////////////////////////////////
//
// Graphics Resource Item
//

class CGraphicsResourceItem : public CUISystem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGraphicsResourceItem(void);

		// Persistance
		void Init(void);

		// Attributes
		UINT GetSoftwareGroup(void) const;

	protected:
		// Data Members
		CPrimResourceItem   * m_pPrims;
		CSymbolResourceItem * m_pSymbols;
		CColorResourceItem  * m_pColors;

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Resource Item
//

class CPrimResourceItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimResourceItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		BOOL LoadWidgets(void);
		BOOL LoadWidget(CFilename Base, CFilename Full, INDEX Before);
		void KillWidgets(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Data Members
		CPrimList * m_pLib;

	protected:
		// Data Members
		CUISystem * m_pSystem;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void LoadWidgets(CString Base, CString Path, CSize Target);
		BOOL AcceptPrims(ITextStream &Stream, CString File, CSize Target, INDEX Before);
		void Normalize(CPrim *pPrim);
	};

//////////////////////////////////////////////////////////////////////////
//
// Symbol Resource Item
//

class CSymbolResourceItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSymbolResourceItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Color Resource Item
//

class CColorResourceItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CColorResourceItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CImageResourceWnd;
class CPrimResourceWnd;
class CSymbolResourceWnd;

//////////////////////////////////////////////////////////////////////////
//
// Image Resource Window
//

class CImageResourceWnd : public CViewWnd, public IDropSource
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImageResourceWnd(void);

		// Destructor
		~CImageResourceWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropSource
		HRESULT METHOD QueryContinueDrag(BOOL fEscape, DWORD dwKeys);
		HRESULT METHOD GiveFeedback(DWORD dwEffect);

	protected:
		// Child Classes
		class CMonitor;

		// Type Definitions
		typedef CArray <CMonitor *> CMonList;

		// Static Data
		static UINT m_timerCheck;
		static UINT m_timerPreview;
		static UINT m_timerQuick;

		// Selection Mode
		UINT m_uMode;
		UINT m_uCat;
		UINT m_uImage;

		// Category List
		CStringArray m_CatName;

		// Image List
		UINT      m_uImageCount;
		CBitmap * m_pImageThumb;
		PBYTE   * m_pImageBits;

		// Layout Data
		CScrollBar * m_pScroll;
		int          m_nTopPixel;
		int          m_nTopSave;
		UINT         m_uViewRows;
		UINT         m_uViewCols;

		// Data Members
		CSize        m_ThumbSize;
		CSize        m_ImageSize;
		CSize        m_LargeSize;
		CString	     m_Title;
		UINT	     m_uZoom;
		BOOL	     m_fZoom;
		BOOL         m_fShowTip;
		CToolTip   * m_pToolTip;
		UINT         m_uHover;
		BOOL         m_fQuick;
		BOOL	     m_fPress;
		UINT	     m_uPress;
		DWORD        m_CatIcon;
		CMonList     m_Monitor;

		// Drag Context
		CCursor	   m_CopyCursor;
		CCursor	   m_LinkCursor;
		CPoint	   m_DragPos;
		CSize	   m_DragOffset;
		CSize	   m_DragSize;
		CBitmap    m_DragImage;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
		void OnCancelMode(void);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);

		// Notification Handlers
		UINT OnToolShow(UINT uID, NMHDR &Info);
		UINT OnToolPop(UINT uID, NMHDR &Info);
		UINT OnToolDraw(UINT uID, NMCUSTOMDRAW &Info);

		// Command Handlers
		BOOL OnResControl(UINT uID, CCmdSource &Src);
		BOOL OnResCommand(UINT uID);

		// Tool Tips
		void CreateToolTip(void);
		void AddSingleTool(void);
		void DrawToolTip(NMCUSTOMDRAW &Info);
		void HideToolTip(void);
		void DestroyToolTip(void);

		// Data Object Creation
		BOOL MakeDataObject(IDataObject * &pData);

		// Drag Support
		void DragItem(void);
		void FindDragMetrics(void);
		void MakeDragImage(void);

		// Folder Monitoring
		BOOL InitMonitor(void);
		BOOL StopMonitor(void);

		// Mode Control
		void SetMode(UINT uMode);

		// Cat List Mode
		void  CatListEnter(void);
		void  CatListLeave(void);
		void  CatListDraw(CDC &DC, CRect Draw);
		void  CatListLayout(void);
		UINT  CatListHitTest(CPoint Pos);
		CRect CatListGetRect(UINT uHover);

		// Thumbail Mode
		void  ThumbsEnter(void);
		void  ThumbsLeave(void);
		void  ThumbsDraw(CDC &DC, CRect Draw);
		void  ThumbsLayout(void);
		UINT  ThumbsHitTest(CPoint Pos);
		CRect ThumbsGetRect(UINT uHover);
		void  ThumbsCreate(void);

		// Implementation
		void  LayoutWindow(void);
		UINT  HitTestHover(CPoint Pos);
		CRect GetHoverRect(UINT uHover);
		CRect GetScrollRect(void);
		void  CreateScrollBar(void);
		void  PlaceScrollBar(void);
		void  EndHover(BOOL fQuick);
		void  SetZoom(UINT uZoom);

		// Image Hooks
		virtual void    OnBuildCatList(void);
		virtual BOOL	OnMonitor(CStringArray &List);
		virtual void    OnRefresh(void);
		virtual void    OnLoadImages(void);
		virtual void    OnFreeImages(void);
		virtual CString OnGetImageText(UINT uImage);
		virtual void    OnAddImageData(CDataObject *pData, UINT uImage);
		virtual BOOL    OnZoomImage(UINT uImage);
		virtual void    OnDrawImage(CDC &DC, CRect Rect, UINT uImage);
		virtual BOOL    OnGetAlpha(UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Directory Monitor Thread
//

class CImageResourceWnd::CMonitor : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CMonitor(CWnd *pWnd, CFilename const &Path);

		// Destructor
		~CMonitor(void);

		// Overridables
		UINT OnExec(void);

		// Attributes
		BOOL GetChangeList(CStringArray &List);

	protected:
		// File Data Map
		typedef CMap <CString, DWORD> CFileMap;

		// Data Members
		CWnd *       m_pWnd;
		CString      m_Path;
		CWaitable    m_FileEvent;
		CFileMap     m_Map;
		CMutex       m_Mutex;
		CStringArray m_Changes;

		// Implmentation
		void MakeList(void);
		BOOL TestList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Symbol Resource Window
//

class CSymbolResourceWnd : public CImageResourceWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSymbolResourceWnd(void);

		// Destructor
		~CSymbolResourceWnd(void);

	protected:
		// Clipboard Formats
		static UINT m_cfEMF;
		static UINT m_cfBMP;
		static UINT m_cfTEX;
		static UINT m_cfSym;

		// Symbol Data
		struct CSym
		{
			UINT	     m_uCat;
			UINT	     m_uSlot;
			BOOL	     m_fMark;
			UINT         m_uType;
			HENHMETAFILE m_hMeta;
			HBITMAP      m_hBits;
			PBYTE	     m_pBits;
			UINT         m_uBits;
			CString	     m_Xaml;
			CString	     m_Text;
			};

		// Symbol List
		typedef CArray <CSym *> CSymList;

		// Favorites
		typedef CTree <DWORD> CFavList;

		// Data Members
		CSymList m_SymList;
		CFavList m_FavList;
		CFavList m_BadList;
		UINT *   m_pIndex;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnRButtonDown(UINT uFlags, CPoint Pos);

		// Command Handlers
		BOOL OnResControl(UINT uID, CCmdSource &Src);
		BOOL OnResCommand(UINT uID);

		// Image Hooks
		void    OnBuildCatList(void);
		void    OnLoadImages(void);
		void    OnFreeImages(void);
		CString OnGetImageText(UINT uImage);
		void    OnAddImageData(CDataObject *pData, UINT uImage);
		void    OnDrawImage(CDC &DC, CRect Rect, UINT uImage);
		BOOL    OnGetAlpha(UINT uImage);

		// Implementation
		void  Recolor(CString &Xaml, COLOR Fill, UINT uMode);
		void  Recolor(HENHMETAFILE &hMeta, COLOR Fill, COLOR Edge, UINT uMode);
		int   GetWeight(DWORD rgb);
		BOOL  IsGrey(DWORD rgb);
		DWORD ConvertFwd(COLOR Col, int nWeight);
		DWORD ConvertRev(COLOR Col, int nWeight);
		UINT  GetColSchemeCount(void);
		COLOR GetColSchemeFill(UINT uScheme);
		COLOR GetColSchemeEdge(UINT uScheme);
		void  LoadFavorites(void);
		void  SaveFavorites(void);
		void  DrawFavMarker(CDC &DC, CRect Rect);
		void  DrawFavMarker(GpGraphics *pGraph, CRect Rect);
		void  LoadBadSymbols(void);
		BOOL  IsBadSymbol(UINT uCat, UINT uSlot);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Resource Window
//

class CPrimResourceWnd : public CImageResourceWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimResourceWnd(void);

		// Destructor
		~CPrimResourceWnd(void);

		// Primitive Info
		struct CPrimInfo
		{
			CString	m_Text;
			CPrim * m_pPrim;
			UINT    m_uMode;
			UINT    m_uIcon;
			BOOL    m_fZoom;
			};

		// Drawing Modes
		enum
		{
			modeNone,
			modeSimple,
			modeWhole,
			modeAspect,
			modeScale,
			modeIcon
			};

	protected:
		// Child Classes
		class CPrimCat;
		class CPrimCatWidget;
		class CPrimCatCore;
		class CPrimCatRuby;
		class CPrimCatGauges;
		class CPrimCatArrows;
		class CPrimCatPolygons;
		class CPrimCatBalloons;
		class CPrimCatSemiTrim;
		class CPrimCatViewers;
		class CPrimCatGraphs;
		class CPrimCatLegacy;
		class CPrimCatImageBased;
		class CPrimCatIllumButtons;
		class CPrimCatIndicators;
		class CPrimCatButtons;
		class CPrimCatToggle2;
		class CPrimCatToggle3;
		class CPrimCatSelect2;
		class CPrimCatSelect3;

		// Primitive List
		typedef CArray <CPrimCat  *> CPrimCatList;
		typedef CArray <CPrimInfo *> CPrimDataList;

		// Data Members
		CPrimResourceItem * m_pItem;
		CUISystem	  * m_pSystem;
		CPrimList	  * m_pLib;
		IGdiWindows	  * m_pWin;
		IGdi		  * m_pGdi;
		UINT		    m_uFixed;
		CPrimCatList	    m_CatList;
		CPrimDataList	    m_PrimList;

		// Overridables
		void OnAttach(void);

		// Image Hooks
		void    OnBuildCatList(void);
		void    OnRefresh(void);
		BOOL    OnMonitor(CStringArray &List);
		void    OnLoadImages(void);
		void    OnFreeImages(void);
		CString OnGetImageText(UINT uImage);
		void    OnAddImageData(CDataObject *pData, UINT uImage);
		BOOL    OnZoomImage(UINT uImage);
		void    OnDrawImage(CDC &DC, CRect Rect, UINT uImage);
		BOOL    OnGetAlpha(UINT uImage);

		// Drawing Helpers
		void DrawWhole(CDC &DC, CRect Rect, CPrim *pPrim);
		void DrawAspect(CDC &DC, CRect Rect, CPrim *pPrim);
		void DrawScale(CDC &DC, CRect Rect, CPrim *pPrim);
		void DrawIcon(CDC &DC, CRect Rect, UINT uIcon);

		// Data Object Construction
		BOOL AddPrimToDataObject(CDataObject *pData, CPrim *pPrim);
		BOOL AddPrimToStream(ITextStream &Stream, CPrim *pPrim);
		void AddPrimToTreeFile(CTreeFile &Tree, CPrim *pPrim);

		// Implementation
		void MakeWidgetCats(void);

		// Category Objects
		CPrimCat * MakePrimCat(UINT uCat);
		BOOL       UseWidget(CPrimWidget *pWidget);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Category Base Class
//

class CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCat(void);

		// Destructor
		virtual ~CPrimCat(void);

		// Overridables
		virtual CString GetTitle(void);
		virtual BOOL    DoNotDelete(void);
		virtual BOOL    LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage);
		virtual BOOL    LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage, UINT uStyle);
		virtual CLASS   GetClass(UINT uImage);
		virtual BOOL    CanColor(UINT uImage);
		virtual BOOL	CanZoom(void);
		virtual void    Adjust(CPrimInfo *pInfo, UINT uImage);

		// Implementation
		void  Bind(CItem *pItem);
		BOOL  StdLoad(CPrimInfo * &pInfo, CItem *pParent, UINT uImage);
		BOOL  AdjustPolygon(CPrimInfo *pInfo, UINT uImage);
		UINT  GetSchemeCount(CLASS Class);
		BOOL  ApplyColors(CPrim *pPrim);
		BOOL  ApplyColors(CPrim *pPrim, UINT uScheme);
		COLOR GetColor(UINT uScheme, UINT uShade);

	protected:
		// Data Members
		CItem *m_pItem;
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Category
//

class CPrimResourceWnd::CPrimCatWidget : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatWidget(CPrimList *pLib, CString Cat);

		// Overridables
		CString GetTitle(void);
		BOOL    DoNotDelete(void);
		BOOL    LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage);

	protected:
		// Data Members
		CPrimList * m_pLib;
		CString     m_Cat;
		CUISystem * m_pSystem;
		INDEX       m_n;

		// Implementation
		BOOL UseWidget(CPrimWidget *pWidget);
	};

//////////////////////////////////////////////////////////////////////////
//
// Core Primitive Category
//

class CPrimResourceWnd::CPrimCatCore : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatCore(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		BOOL    CanColor(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Category
//

class CPrimResourceWnd::CPrimCatRuby : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatRuby(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		BOOL    CanColor(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ruby Gauges Category
//

class CPrimResourceWnd::CPrimCatGauges : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatGauges(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		BOOL    CanColor(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Arrows Primitive Category
//

class CPrimResourceWnd::CPrimCatArrows : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatArrows(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Poylgons Primitive Category
//

class CPrimResourceWnd::CPrimCatPolygons : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatPolygons(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Balloons Primitive Category
//

class CPrimResourceWnd::CPrimCatBalloons : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatBalloons(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Semi-Trimmed Primitive Category
//

class CPrimResourceWnd::CPrimCatSemiTrim : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatSemiTrim(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Viewers Primitive Category
//

class CPrimResourceWnd::CPrimCatViewers : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatViewers(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
		BOOL	CanZoom(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphs Primitive Category
//

class CPrimResourceWnd::CPrimCatGraphs : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatGraphs(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Legacy Primitive Category
//

class CPrimResourceWnd::CPrimCatLegacy : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatLegacy(void);

		// Overridables
		CString GetTitle(void);
		CLASS   GetClass(UINT uImage);
		BOOL    CanColor(UINT uImage);
		void    Adjust(CPrimInfo *pInfo, UINT uImage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Image-Based Primitive Category
//

class CPrimResourceWnd::CPrimCatImageBased : public CPrimResourceWnd::CPrimCat
{
	public:
		// Constructor
		CPrimCatImageBased(void);

		// Overridables
		CString GetTitle(void);
		BOOL    LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage);
		BOOL    LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage, UINT uStyle);

	protected:
		// Data Members
		CString m_Title;
		CLASS   m_Class;
		UINT    m_State[4];

		// Overridables
		virtual CString GetTypeDesc(UINT uType);
		virtual UINT    GetTypeCategory(UINT uType);
		virtual PUINT   GetTypeData(UINT uType, UINT uStyle);
		virtual void    Adjust(CPrim *pPrim, UINT uType);

		// Implementation
		CString MakeName(UINT uCat, UINT uHandle);
	};

//////////////////////////////////////////////////////////////////////////
//
// Illuminated Buttons Category
//

class CPrimResourceWnd::CPrimCatIllumButtons : public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatIllumButtons(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
	};

//////////////////////////////////////////////////////////////////////////
//
// Illuminated Category
//

class CPrimResourceWnd::CPrimCatIndicators : public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatIndicators(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
	};

//////////////////////////////////////////////////////////////////////////
//
// Buttons Category
//

class CPrimResourceWnd::CPrimCatButtons : public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatButtons(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
	};

//////////////////////////////////////////////////////////////////////////
//
// 2-State Toggle Category
//

class CPrimResourceWnd::CPrimCatToggle2 : public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatToggle2(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
	};

//////////////////////////////////////////////////////////////////////////
//
// 3-State Toggle Category
//

class CPrimResourceWnd::CPrimCatToggle3: public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatToggle3(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
	};

//////////////////////////////////////////////////////////////////////////
//
// 2-State Selector Category
//

class CPrimResourceWnd::CPrimCatSelect2 : public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatSelect2(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
		void    Adjust(CPrim *pPrim, UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// 3-State Selector Category
//

class CPrimResourceWnd::CPrimCatSelect3: public CPrimResourceWnd::CPrimCatImageBased
{
	public:
		// Constructor
		CPrimCatSelect3(void);

	protected:
		// Overridables
		CString GetTypeDesc(UINT uType);
		UINT    GetTypeCategory(UINT uType);
		PUINT   GetTypeData(UINT uType, UINT uStyle);
		void    Adjust(CPrim *pPrim, UINT uType);
	};

// End of File

#endif
