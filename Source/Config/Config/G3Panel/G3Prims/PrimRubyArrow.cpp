
#include "intern.hpp"

#include "PrimRubyArrow.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Arrow Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyArrow, CPrimRubyOriented);

// Constructor

CPrimRubyArrow::CPrimRubyArrow(void)
{
	m_Min1 = 0;
	m_Max1 = 4500;
	m_Pos1 = 3333;

	m_Min2 =  500;
	m_Max2 = 9500;
	m_Pos2 = 5000;
	}

// Meta Data

void CPrimRubyArrow::AddMetaData(void)
{
	CPrimRubyOriented::AddMetaData();

	Meta_AddInteger(Pos1);
	Meta_AddInteger(Pos2);

	Meta_SetName((IDS_ARROW_2));
	}

// Overridables

void CPrimRubyArrow::UpdateLayout(void)
{
	number cx = m_RealRect.m_x2 - m_RealRect.m_x1;
	number cy = m_RealRect.m_y2 - m_RealRect.m_y1;

	switch( m_Orient ) {

		case 0:
		case 1:
		case 4:
			m_Min2 = int( 500 * cx / cy);
			m_Max2 = int(9500 * cx / cy);
			break;

		case 2:
		case 3:
			m_Min2 = int( 500 * cy / cx);
			m_Max2 = int(9500 * cy / cx);
			break;
		}

	MakeMax(m_Pos2, m_Min2);
	MakeMin(m_Pos2, m_Max2);

	CPrimRubyOriented::UpdateLayout();
	}

BOOL CPrimRubyArrow::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		UINT uInset = 0;

		Hand.m_pTag = L"Pos1";

		switch( m_Orient ) {

			case 0: // Right
				Hand.m_uRef  = 100;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos1;
				Hand.m_Clip  = CRect(uInset, 0, uInset, m_Max1);
				break;

			case 1: // Left
			case 4: // Arbitary
				Hand.m_uRef  = 102;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos1;
				Hand.m_Clip  = CRect(uInset, 0, uInset, m_Max1);
				break;

			case 2: // Up
				Hand.m_uRef  = 103;
				Hand.m_Pos.x = m_Pos1;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(0, uInset, m_Max1, uInset);
				break;

			case 3: // Down
				Hand.m_uRef  = 101;
				Hand.m_Pos.x = m_Pos1;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(0, uInset, m_Max1, uInset);
				break;
			}

		Hand.m_uRef ^= m_Reflect;

		return TRUE;
		}

	if( uHand == 1 ) {

		UINT uInset = 0;

		Hand.m_pTag = L"Pos2";

		switch( m_Orient ) {

			case 0: // Right
				Hand.m_uRef  = 201;
				Hand.m_Pos.x = m_Pos2;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(m_Min2, uInset, m_Max2, uInset);
				break;

			case 1: // Left
			case 4: // Arbitary
				Hand.m_uRef  = 203;
				Hand.m_Pos.x = m_Pos2;
				Hand.m_Pos.y = uInset;
				Hand.m_Clip  = CRect(m_Min2, uInset, m_Max2, uInset);
				break;

			case 2: // Up
				Hand.m_uRef  = 200;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos2;
				Hand.m_Clip  = CRect(uInset, m_Min2, uInset, m_Max2);
				break;

			case 3: // Down
				Hand.m_uRef  = 202;
				Hand.m_Pos.x = uInset;
				Hand.m_Pos.y = m_Pos2;
				Hand.m_Clip  = CRect(uInset, m_Min2, uInset, m_Max2);
				break;
			}

		Hand.m_uRef ^= m_Reflect;

		return TRUE;
		}

	return FALSE;
	}

// Path Generation

void CPrimRubyArrow::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	CRubyVector dp = p2 - p1;

	number dx = (m_Pos2 * dp.m_y) / 10000;
	
	number dy = (m_Pos1 * dp.m_y) / 10000;
	
	number cy = dp.m_y / 2;

	figure.Append(p2.m_x - dx, p2.m_y     );
	figure.Append(p2.m_x - dx, p2.m_y - dy);
	figure.Append(p1.m_x     , p2.m_y - dy);
	figure.Append(p1.m_x     , p1.m_y + dy);
	figure.Append(p2.m_x - dx, p1.m_y + dy);
	figure.Append(p2.m_x - dx, p1.m_y     );
	figure.Append(p2.m_x,      p1.m_y + cy);

	figure.AppendHardBreak();
	}

// End of File
