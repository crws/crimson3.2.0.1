
#include "intern.hpp"

#include "RubyLineEndUi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Extra Height
//

static const int extraHeight = 16;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element for Ruby Pattern
//

// Dynamic Class

AfxImplementDynamicClass(CUITextRubyLineEnd, CUITextEnum);

// Constructor

CUITextRubyLineEnd::CUITextRubyLineEnd(void)
{
	}

// Overridables

void CUITextRubyLineEnd::OnBind(void)
{
	CUITextElement::OnBind();
	
	AddData();
	}

// Implementation

void CUITextRubyLineEnd::AddItem(UINT Code, CString Name)
{
	CEntry Enum;

	Enum.m_Data = Code;

	Enum.m_Text = Name;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextRubyLineEnd::AddData(void)
{
	AddItem(  0, CString(IDS_FLAT));
	AddItem(  1, CString(IDS_SQUARE));
	AddItem(  2, CString(IDS_ROUNDED));
	AddItem(  3, CString(IDS_POINTED));
	AddItem(100, CString(IDS_SMALL_ARROW));
	AddItem(101, CString(IDS_MEDIUM_ARROW));
	AddItem(102, CString(IDS_LARGE_ARROW));
	AddItem(103, CString(IDS_SMALL_BARB));
	AddItem(104, CString(IDS_MEDIUM_BARB));
	AddItem(105, CString(IDS_LARGE_BARB));
	AddItem(106, CString(IDS_SMALL_TAIL));
	AddItem(107, CString(IDS_MEDIUM_TAIL));
	AddItem(108, CString(IDS_LARGE_TAIL));
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element for Ruby Pattern
//

// Dynamic Class

AfxImplementDynamicClass(CUIRubyLineEnd, CUIControl);

// Constructor

CUIRubyLineEnd::CUIRubyLineEnd(void)
{
	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CRubyLineEndComboBox(this);
	}

// Core Overridables

void CUIRubyLineEnd::OnBind(void)
{
	m_Label = GetLabel() + L':';

	CUIElement::OnBind();
	}

void CUIRubyLineEnd::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayDropDown(m_pText, TRUE, 32, 6 + extraHeight);

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem( New CLayFormPad (m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIRubyLineEnd::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP         |
			     WS_VSCROLL         |
			     CBS_DROPDOWNLIST   |
			     CBS_OWNERDRAWFIXED |
			     CBS_HASSTRINGS,
			     FindComboRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, CBN_SELCHANGE);

	LoadList();
	}

void CUIRubyLineEnd::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(FindComboRect(), TRUE);
	}

// Data Overridables

void CUIRubyLineEnd::OnLoad(void)
{
	m_pDataCtrl->SelectStringExact(m_pText->GetAsText());
	}

UINT CUIRubyLineEnd::OnSave(BOOL fUI)
{
	CString Text = m_pDataCtrl->GetWindowText();

	Text.TrimLeft();

	return StdSave(fUI, Text);
	}

// Implementation

CRect CUIRubyLineEnd::FindComboRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.top    = Rect.top  - 1;

	Rect.bottom = Rect.top  + 8 * Rect.cy();

	return Rect;
	}

void CUIRubyLineEnd::LoadList(void)
{
	m_pDataCtrl->LoadList();
	}

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Combo Box
//

// Dynamic Class

AfxImplementRuntimeClass(CRubyLineEndComboBox, CDropComboBox);

// Constructor

CRubyLineEndComboBox::CRubyLineEndComboBox(CUIElement *pUI) : CDropComboBox(pUI)
{
	m_pWin = Create_GdiWindowsA888(1024, 256);

	m_pGdi = m_pWin->GetGdi();
	}

// Constructor

CRubyLineEndComboBox::~CRubyLineEndComboBox(void)
{
	m_pGdi->Release();
	}

// Operations

void CRubyLineEndComboBox::LoadList(void)
{
	SetRedraw(FALSE);

	SendMessage(CB_SETMINVISIBLE, 1);

	AddItem(  0, CString(IDS_FLAT));
	AddItem(  1, CString(IDS_SQUARE));
	AddItem(  2, CString(IDS_ROUNDED));
	AddItem(  3, CString(IDS_POINTED));
	AddItem(100, CString(IDS_SMALL_ARROW));
	AddItem(101, CString(IDS_MEDIUM_ARROW));
	AddItem(102, CString(IDS_LARGE_ARROW));
	AddItem(103, CString(IDS_SMALL_BARB));
	AddItem(104, CString(IDS_MEDIUM_BARB));
	AddItem(105, CString(IDS_LARGE_BARB));
	AddItem(106, CString(IDS_SMALL_TAIL));
	AddItem(107, CString(IDS_MEDIUM_TAIL));
	AddItem(108, CString(IDS_LARGE_TAIL));

	SendMessage(CB_SETMINVISIBLE, 10);

	SetRedraw(TRUE);
	}

// Message Map

AfxMessageMap(CRubyLineEndComboBox, CDropComboBox)
{
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxMessageEnd(CRubyLineEndComboBox)
	};

// Message Handlers

void CRubyLineEndComboBox::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	Item.itemHeight = DC.GetTextExtent(L"ABC").cy + 2 + extraHeight;

	DC.Deselect();
	}

void CRubyLineEndComboBox::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
	CDC & DC = CDC::FromHandle(Item.hDC);

	CDC & GD = CDC::FromHandle(m_pWin->GetDC());

	CRect Rect = Item.rcItem;

	CRect Work = CRect(Rect.GetSize());

	GD.Select(DC.GetCurrentFont());

	if( Item.itemState & ODS_DISABLED ) {

		GD.SetTextColor(afxColor(3dShadow));

		GD.SetBkColor(afxColor(3dFace));

		GD.FillRect(Work, afxBrush(3dShadow));
		}
	else {
		if( Item.itemState & ODS_SELECTED ) {

			GD.SetTextColor(afxColor(SelectText));

			GD.SetBkColor(afxColor(SelectBack));
			}
		else {
			GD.SetTextColor(afxColor(WindowText));

			GD.SetBkColor(afxColor(WindowBack));
			}
		}

	if( Item.itemID < NOTHING ) {

		CString Text = GetLBText(Item.itemID);

		CSize   Size = DC.GetTextExtent(Text);

		CPoint  Pos  = CPoint(8, 0);

		Pos.x += 5 * Work.cy() / 2;

		Pos.y += (Work.cy() - Size.cy) / 2;

		GD.ExtTextOut( Pos,
			       ETO_OPAQUE | ETO_CLIPPED,
			       Work, Text
			       );
		}

	if( Item.itemID < NOTHING ) {

		CRect Fill = Work;

		Fill.right = Fill.left + 5 * Fill.cy() / 2;

		number wd  = (Item.itemData < 100) ? 16 : 6;

		int    a1  = (m_pUI->GetFormat() == L"1") ? Item.itemData : -1;

		int    a2  = (m_pUI->GetFormat() == L"1") ? -1 : Item.itemData;

		number yp  = int(Fill.cy() / 2);

		number x1  = 4;

		number x2  = Fill.cx() - 4;

		CRubyStroker stroker;

		x1 += (a1 < 0) ? 8 : stroker.GetArrowLength(a1 ? a1 : 1, wd);

		x2 -= (a2 < 0) ? 8 : stroker.GetArrowLength(a2 ? a2 : 1, wd);

		CRubyPath line, path;

		line.Append(x1, yp);

		line.Append(x2, yp);

		line.AppendHardBreak();

		stroker.SetEndStyle(stroker.endArrow);

		stroker.SetArrowStyle(a1, a2);

		stroker.StrokeOpen(path, line, 0, wd);

		CRubyGdiList list;

		list.Load(path, true);

		if( Item.itemState & ODS_DISABLED ) {

			CRubyGdiLink(m_pGdi).OutputSolid(list, GetRGB(24,24,24), 0);
			}
		else
			CRubyGdiLink(m_pGdi).OutputSolid(list, GetRGB(0,0,0), 0);
		}

	DC.BitBlt(Rect, GD, CPoint(0, 0), SRCCOPY);

	if( Item.itemState & ODS_FOCUS ) {

		DC.DrawFocusRect(Rect);
		}

	GD.Deselect();
	}

// Implementation

void CRubyLineEndComboBox::AddItem(UINT uCode, CString Name)
{
	AddString(Name, DWORD(uCode));
	}

// End of File
