
#include "intern.hpp"

#include "PrimRubyOriented.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Oriented Primitive
//

// Dyanmic Class

AfxImplementDynamicClass(CPrimRubyOriented, CPrimRubyGeom);

// Constructor

CPrimRubyOriented::CPrimRubyOriented(void)
{
	m_Orient  = 1;

	m_Reflect = 0;

	m_Rotate  = 450;
	}

// UI Managament

void CPrimRubyOriented::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Orient" ) {

			pHost->EnableUI(this, "Rotate", m_Orient == 4);
			}
		}

	CPrimRubyGeom::OnUIChange(pHost, pItem, Tag);
	}

// Meta Data

void CPrimRubyOriented::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_AddInteger(Orient);
	Meta_AddInteger(Reflect);
	Meta_AddInteger(Rotate);

	Meta_SetName((IDS_RUBY_ORIENTED));
	}

// Path Management

void CPrimRubyOriented::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	MakeFigure(Rect);

	CPrimRubyGeom::MakePaths();
	}

void CPrimRubyOriented::MakeFigure(R2 const &Rect)
{
	// Create the path.

	CRubyPoint p1(Rect, 1);

	CRubyPoint p2(Rect, 4);

	if( m_Orient == 2 || m_Orient == 3 ) {

		Swap(p1.m_x, p1.m_y);

		Swap(p2.m_x, p2.m_y);
		}

	MakePath(m_pathFill, p1, p2);

	// Check for transformations.

	if( m_Orient || m_Reflect ) {

		// Create an identity transformation.

		CRubyMatrix mat;
	
		// Move the center of the figure to the origin.

		R2R rect;

		m_pathFill.GetBoundingRect(rect);

		mat.AddTranslation( -0.5 * (rect.m_x1 + rect.m_x2),
				    -0.5 * (rect.m_y1 + rect.m_y2)
				    );

		// Apply any rotations.

		switch( m_Orient ) {

			// Goofy values but they are what the 
			// graphics editor is expecting to see!

			case 1:
				mat.AddRotation(180);

				break;

			case 2:
				mat.AddRotation(270);

				break;

			case 3:
				mat.AddRotation(90);

				break;

			case 4:
				mat.AddRotation(INT(m_Rotate) / 10.0);

				break;
			}

		// Apply any reflections.

		switch( m_Reflect ) {

			// Likewise!

			case 1:
				mat.AddReflectVert();

				break;

			case 2:
				mat.AddReflectVert();

				mat.AddReflectHorz();

				break;

			case 3:
				mat.AddReflectHorz();

				break;
			}

		// Transform the fill path;

		m_pathFill.Transform(mat);

		// Find the transformed bounding rectangle.

		m_pathFill.GetBoundingRect(rect);

		// Start a new transformation.

		mat.SetIdentity();

		// Recenter the figure.

		mat.AddTranslation( -0.5 * (rect.m_x1 + rect.m_x2),
				    -0.5 * (rect.m_y1 + rect.m_y2)
				    );

		// Scale the figure to fit the drawing rectangle.

		mat.AddScaleFactor( (Rect.x2 - Rect.x1 - 1) / fabs(rect.m_x2 - rect.m_x1),
				    (Rect.y2 - Rect.y1 - 1) / fabs(rect.m_y2 - rect.m_y1)
				    );

		// And move the figure to the correct location.

		mat.AddTranslation( (Rect.x1 + Rect.x2) / 2,
				    (Rect.y1 + Rect.y2) / 2
				    );


		// Transform the fill path;

		m_pathFill.Transform(mat);
		}
	}

// Path Generation

void CPrimRubyOriented::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	// Overload this in the child class to create the actual figure.
	}

// End of File
