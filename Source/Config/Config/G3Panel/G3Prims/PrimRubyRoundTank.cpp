
#include "intern.hpp"

#include "PrimRubyRoundTank.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyGeom.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Round Tank Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyRoundTank, CPrimRubyGeom);

// Constructor

CPrimRubyRoundTank::CPrimRubyRoundTank(void)
{
	m_Base = 8;
	}

// Overridables

void CPrimRubyRoundTank::FindTextRect(void)
{
	int nAdjust   = m_pEdge->GetWidth();

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust - m_Base;

	/*AdjustTextRect(); !!!!*/
	}

BOOL CPrimRubyRoundTank::GetHand(UINT uHand, CPrimHand &Hand)
{
	int cy = m_DrawRect.y2 - m_DrawRect.y1;

	if( uHand == 0 ) {

		Hand.m_Pos.x = 0;

		Hand.m_Pos.y = m_Base;

		Hand.m_pTag  = L"Base";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 8, 0, cy - 8);

		return TRUE;
		}

	return FALSE;
	}

void CPrimRubyRoundTank::SetHand(BOOL fInit)
{
	if( fInit ) {

		int cx = m_DrawRect.y2 - m_DrawRect.y1;

		m_Base = 1 * cx / 2;
		}

	CPrimRubyGeom::SetHand(fInit);
	}

// Meta Data

void CPrimRubyRoundTank::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_AddInteger(Base);

	Meta_SetName(IDS_ROUND_TANK_2);
	}

// Path Management

void CPrimRubyRoundTank::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyPoint p1(Rect, 1);
	
	CRubyPoint p2(Rect, 4);

	number yBase = p2.m_y - min(m_Base, p2.m_y - 8);

	m_pathFill.Append(p1.m_x, p1.m_y);

	m_pathFill.Append(p2.m_x, p1.m_y);

	CRubyPoint  p3(p2.m_x, yBase);

	CRubyPoint  p4(p1.m_x, yBase);

	CRubyPoint  cp((p1.m_x + p2.m_x) / 2, yBase);

	CRubyVector rd((p2.m_x - p1.m_x) / 2, m_Base);

	CRubyDraw::Arc(m_pathFill, cp, p3, p4, rd);

	m_pathFill.AppendHardBreak();

	CPrimRubyGeom::MakePaths();
	}

// End of File
