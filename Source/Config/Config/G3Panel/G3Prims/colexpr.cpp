
#include "intern.hpp"

#include "colexpr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Color Expression
//

// Runtime Class

AfxImplementRuntimeClass(CColorExprItem, CCodedHost);

// Constructor

CColorExprItem::CColorExprItem(void)
{
	}

// Overridables

CString CColorExprItem::GetExpr(void)
{
	return L"";
	}

BOOL CColorExprItem::SetExpr(CString Text)
{
	return FALSE;
	}

// Implementation

CString CColorExprItem::GetSource(CCodedItem *pItem, CString Text)
{
	return pItem ? pItem->GetSource(TRUE) : Text;
	}

//////////////////////////////////////////////////////////////////////////
//
// 2-State Color Expression
//

// Runtime Class

AfxImplementRuntimeClass(CColPick2Item, CColorExprItem);

// Constructor

CColPick2Item::CColPick2Item(void)
{
	m_pData = NULL;
	
	m_Col1	= GetRGB(0x00,0x0F,0x00);
	
	m_Col2	= GetRGB(0x1F,0x00,0x00);
	}

// Overridables

CString CColPick2Item::GetExpr(void)
{
	CString Text;

	Text += L"ColPick2";

	Text += L"(";

	Text += GetSource(m_pData, L"false");
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col1);
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col2);
	
	Text += L")";

	return Text;
	}

BOOL CColPick2Item::SetExpr(CString Text)
{
	if( !Text.IsEmpty() ) {

		Text = Text.Mid (Text.Find('(')   + 1);

		Text = Text.Left(Text.GetLength() - 1);

		CStringArray List;

		Text.Tokenize(List, L',', L'(', L')');

		if( List.GetCount() == 3 ) {

			SetInitial(L"Data", m_pData, List[0]);

			m_Col1 = COLOR(wcstol(List[1].Mid(2), NULL, 16));

			m_Col2 = COLOR(wcstol(List[2].Mid(2), NULL, 16));

			return TRUE;
			}
		}

	return FALSE;
	}

// Type Access

BOOL CColPick2Item::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeLogical;

	Type.m_Flags = 0;

	return FALSE;
	}

// Meta Data

void CColPick2Item::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Data);
	Meta_AddInteger(Col1);
	Meta_AddInteger(Col2);

	Meta_SetName((IDS_STATE_COLOR));
	}

//////////////////////////////////////////////////////////////////////////
//
// 4-State Color Expression
//

// Runtime Class

AfxImplementRuntimeClass(CColPick4Item, CColorExprItem);

// Constructor

CColPick4Item::CColPick4Item(void)
{
	m_pData1 = NULL;

	m_pData2 = NULL;
	
	m_Col1	= GetRGB(0x00,0x1F,0x00);

	m_Col2	= GetRGB(0x1F,0x00,0x00);
	
	m_Col3	= GetRGB(0x00,0x0F,0x00);

	m_Col4	= GetRGB(0x0F,0x00,0x00);
	}

// Overridables

CString CColPick4Item::GetExpr(void)
{
	CString Text;

	Text += L"ColPick4";

	Text += L"(";

	Text += GetSource(m_pData1, L"false");
		
	Text += L",";

	Text += GetSource(m_pData2, L"false");
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col1);
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col2);
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col3);
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col4);
	
	Text += L")";

	return Text;
	}

BOOL CColPick4Item::SetExpr(CString Text)
{
	if( !Text.IsEmpty() ) {

		Text = Text.Mid (Text.Find('(')   + 1);

		Text = Text.Left(Text.GetLength() - 1);

		CStringArray List;

		Text.Tokenize(List, L',', L'(', L')');

		if( List.GetCount() == 6 ) {

			SetInitial(L"Data1", m_pData1, List[0]);

			SetInitial(L"Data2", m_pData2, List[1]);

			m_Col1 = COLOR(wcstol(List[2].Mid(2), NULL, 16));

			m_Col2 = COLOR(wcstol(List[3].Mid(2), NULL, 16));

			m_Col3 = COLOR(wcstol(List[4].Mid(2), NULL, 16));

			m_Col4 = COLOR(wcstol(List[5].Mid(2), NULL, 16));

			return TRUE;
			}
		}

	return FALSE;
	}

// Type Access

BOOL CColPick4Item::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeLogical;

	Type.m_Flags = 0;

	return FALSE;
	}

// Meta Data

void CColPick4Item::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Data1);
	Meta_AddVirtual(Data2);
	Meta_AddInteger(Col1);
	Meta_AddInteger(Col2);
	Meta_AddInteger(Col3);
	Meta_AddInteger(Col4);

	Meta_SetName((IDS_STATE_COLOR_2));
	}

//////////////////////////////////////////////////////////////////////////
//
// Blended Color Expression
//

// Runtime Class

AfxImplementRuntimeClass(CColBlendItem, CColorExprItem);

// Constructor

CColBlendItem::CColBlendItem(void)
{
	m_pData = NULL;
	
	m_pMin  = NULL;
	
	m_pMax  = NULL;
	
	m_Col1	= GetRGB(0x00,0x00,0x1F);
	
	m_Col2	= GetRGB(0x1F,0x00,0x00);
	}

// UI Update

void CColBlendItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			m_fAutoLimits = HasAutoLimits();
			}

		if( Tag == "Data" ) {

			if( m_fAutoLimits ) {

				if( m_pData && m_pData->IsTagRef() ) {

					CString Tag = m_pData->GetTagName();

					InitCoded(pHost, L"Min", m_pMin, Tag + L".Min");

					InitCoded(pHost, L"Max", m_pMax, Tag + L".Max");

					m_fAutoLimits = TRUE;

					return;
					}
				}

			m_fAutoLimits = HasAutoLimits();
			}

		if( Tag == "Min" || Tag == "Max" ) {

			m_fAutoLimits = HasAutoLimits();
			}
		}
	}

// Overridables

CString CColBlendItem::GetExpr(void)
{
	CString Text;

	Text += L"ColBlend";

	Text += L"(";

	Text += GetSource(m_pData, L"0");
	
	Text += L",";

	Text += GetSource(m_pMin,  L"0");
	
	Text += L",";

	Text += GetSource(m_pMax,  L"100");
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col1);
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col2);
	
	Text += L")";

	return Text;
	}

BOOL CColBlendItem::SetExpr(CString Text)
{
	if( !Text.IsEmpty() ) {

		Text = Text.Mid (Text.Find('(')   + 1);

		Text = Text.Left(Text.GetLength() - 1);

		CStringArray List;

		Text.Tokenize(List, L',', L'(', L')');

		if( List.GetCount() == 5 ) {

			SetInitial(L"Data", m_pData, List[0]);

			SetInitial(L"Min",  m_pMin,  List[1]);

			SetInitial(L"Max",  m_pMax,  List[2]);

			m_Col1 = COLOR(wcstol(List[3].Mid(2), NULL, 16));

			m_Col2 = COLOR(wcstol(List[4].Mid(2), NULL, 16));

			return TRUE;
			}
		}

	return FALSE;
	}

// Type Access

BOOL CColBlendItem::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeReal;

	Type.m_Flags = 0;

	return FALSE;
	}

// Meta Data

void CColBlendItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Data);
	Meta_AddVirtual(Min);
	Meta_AddVirtual(Max);
	Meta_AddInteger(Col1);
	Meta_AddInteger(Col2);

	Meta_SetName((IDS_BLENDED_COLOR));
	}

// Implementation

BOOL CColBlendItem::HasAutoLimits(void)
{
	if( m_pMin && m_pMax ) {

		if( m_pData ) {
		
			CString c1 = m_pData->GetSource(FALSE);

			CString c2 = m_pMin ->GetSource(FALSE);

			CString c3 = m_pMax ->GetSource(FALSE);

			if( c2 == c1 + L".Min" ) {

				if( c3 == c1 + L".Max" ) {

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( m_pMin || m_pMax ) {

		return FALSE;
		}
	
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Flashing Color Expression
//

// Runtime Class

AfxImplementRuntimeClass(CColFlashItem, CColorExprItem);

// Constructor

CColFlashItem::CColFlashItem(void)
{
	m_pFlash = NULL;

	m_pRate  = NULL;
	
	m_Col1	 = GetRGB(0x00,0x00,0x0F);
	
	m_Col2	 = GetRGB(0x00,0x0F,0x00);
	
	m_Col3	 = GetRGB(0x1F,0x00,0x00);
	}

// UI Update

void CColFlashItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Flash" ) {

			pHost->EnableUI(this, L"Col1", m_pFlash ? TRUE : FALSE);
			}
		}
	}

// Overridables

CString CColFlashItem::GetExpr(void)
{
	CString Text;

	if( m_pFlash ) {

		Text += L"ColSelFlash";

		Text += L"(";

		Text += GetSource(m_pFlash, L"true");
		
		Text += L",";

		Text += GetSource(m_pRate,  L"1");
	
		Text += L",";

		Text += CPrintf(L"0x%4.4X", m_Col1);
	
		Text += L",";
		}
	else {
		Text += L"ColFlash";

		Text += L"(";

		Text += GetSource(m_pRate,  L"1");
	
		Text += L",";
		}
	
	Text += CPrintf(L"0x%4.4X", m_Col2);
	
	Text += L",";
	
	Text += CPrintf(L"0x%4.4X", m_Col3);
	
	Text += L")";

	return Text;
	}

BOOL CColFlashItem::SetExpr(CString Text)
{
	if( !Text.IsEmpty() ) {

		Text = Text.Mid (Text.Find('(')   + 1);

		Text = Text.Left(Text.GetLength() - 1);

		CStringArray List;

		Text.Tokenize(List, L',', L'(', L')');

		if( List.GetCount() == 3 ) {

			SetInitial(L"Rate", m_pRate, List[0]);

			m_Col2 = COLOR(wcstol(List[1].Mid(2), NULL, 16));

			m_Col3 = COLOR(wcstol(List[2].Mid(2), NULL, 16));

			return TRUE;
			}

		if( List.GetCount() == 5 ) {

			SetInitial(L"Flash", m_pFlash, List[0]);

			SetInitial(L"Rate",  m_pRate,  List[1]);

			m_Col1 = COLOR(wcstol(List[2].Mid(2), NULL, 16));

			m_Col2 = COLOR(wcstol(List[3].Mid(2), NULL, 16));

			m_Col3 = COLOR(wcstol(List[4].Mid(2), NULL, 16));

			return TRUE;
			}
		}

	return FALSE;
	}

// Type Access

BOOL CColFlashItem::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return FALSE;
	}

// Meta Data

void CColFlashItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Flash);
	Meta_AddVirtual(Rate);
	Meta_AddInteger(Col1);
	Meta_AddInteger(Col2);
	Meta_AddInteger(Col3);

	Meta_SetName((IDS_FLASHING_COLOR));
	}

// End of File
