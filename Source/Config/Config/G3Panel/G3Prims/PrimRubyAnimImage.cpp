
#include "intern.hpp"

#include "PrimRubyAnimImage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Animated Image Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyAnimImage, CPrimRubyRect);

// Constructor

CPrimRubyAnimImage::CPrimRubyAnimImage(void)
{
	m_uType  = 0x83;

	m_Keep   = 0;

	m_Count  = 1;

	m_pValue = NULL;

	m_pColor = NULL;

	m_pShow  = NULL;

	memset(m_pImage, 0, sizeof(m_pImage));
	}

// UI Overridables

BOOL CPrimRubyAnimImage::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_IMAGES),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_MORE),
				       AfxPointerClass(this),
				       3
				       ));

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       2
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// UI Update

void CPrimRubyAnimImage::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Keep" ) {

		for( UINT n = 0; n < elements(m_pImage); n++ ) {

			if( m_pImage[n] ) {

				m_pImage[n]->m_Opts.m_Keep = m_Keep;

				pHost->UpdateUI(CPrintf(L"Image%2.2u", 1 + n));
				}
			}
		}

	if( Tag == L"Count" ) {

		DoEnables(pHost);
		}

	CPrimRubyRect::OnUIChange(pHost, pItem, Tag);
	}

// Operations

void CPrimRubyAnimImage::SetImage(UINT n, PCTXT pFile)
{
	CPrimImage * &pImage = m_pImage[n];

	if( !pImage ) {

		pImage = New CPrimImage;

		pImage->SetParent(this);

		pImage->Init();
		}

	if( n == 0 ) {

		CSize Max = CSize(500, 500);

		SetInitSize(pImage->LoadFromFile(pFile, Max));

		UpdateLayout();
		}
	else {
		CSize Max;

		Max.cx = m_DrawRect.x2 - m_DrawRect.x1;

		Max.cy = m_DrawRect.y2 - m_DrawRect.y1;

		pImage->LoadFromFile(pFile, Max);
		}
	}

// Overridables

BOOL CPrimRubyAnimImage::HitTest(P2 Pos)
{
	if( CPrimRubyWithText::HitTest(Pos) ) {

		return TRUE;
		}

	if( PtInRect(m_bound, Pos) ) {

		// TOOD -- Hit test the image alpha channel? !!!!

		if( m_pathFill.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathEdge.HitTest(Pos, 0) ) {

			return TRUE;
			}

		if( m_pathTrim.HitTest(Pos, 0) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

void CPrimRubyAnimImage::Draw(IGDI *pGDI, UINT uMode)
{
	if( TRUE ) {

		m_pFill->Register(uMode);

		m_pEdge->Register(uMode);

		m_pFill->Fill(pGDI, m_listFill, !UseFastFill()),

		m_pEdge->Fill(pGDI, m_listEdge, TRUE);

		m_pEdge->Trim(pGDI, m_listTrim, TRUE);
		}

	if( TRUE ) {

		UINT n   = NOTHING;

		R2  Rect = GetImageRect();

		for( UINT i = 0; i < elements(m_pImage); i++ ) {

			if( m_pImage[i] ) {

				m_pImage[i]->Draw(NULL, Rect, uMode, 0U);

				n = i;
				}
			}

		if( n < elements(m_pImage) ) {

			int  nImg = 0;

			UINT rop  = 0;
		
			if( m_pColor ) {
			
				if( !m_pColor->ExecVal() ) {

					rop = ropDisable;
					}
				}

			if( m_pValue ) {

				nImg = m_pValue->ExecVal();

				nImg = nImg % int(m_Count);
				}

			if( m_pImage[nImg] ) {

				m_pImage[nImg]->Draw(pGDI, Rect, uMode, rop);
				}
			}
		else {
			pGDI->ResetFont();

			pGDI->SetTextTrans(modeTransparent);

			PCTXT pt = L"IMG";

			int   cx = pGDI->GetTextWidth(pt);

			int   cy = pGDI->GetTextHeight(pt);

			int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

			int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

			pGDI->TextOut(xp, yp, pt);
			}
		}

	CPrimRubyWithText::Draw(pGDI, uMode);
	}

void CPrimRubyAnimImage::SetInitState(void)
{
	CPrimRubyRect::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pFill->m_Pattern = 0;
	}

void CPrimRubyAnimImage::GetRefs(CPrimRefList &Refs)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {
			
			m_pImage[n]->GetRefs(Refs);
			}
		}

	m_pFill->GetRefs(Refs);

	CPrimRubyWithText::GetRefs(Refs);
	}

void CPrimRubyAnimImage::EditRef(UINT uOld, UINT uNew)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {
			
			m_pImage[n]->EditRef(uOld, uNew);
			}
		}

	CPrimRubyWithText::EditRef(uOld, uNew);
	}

// Download Support

BOOL CPrimRubyAnimImage::MakeInitData(CInitData &Init)
{
	CPrimRubyRect::MakeInitData(Init);

	Init.AddByte(BYTE(m_Count));

	for( UINT n = 0; n < m_Count; n++ ) {

		Init.AddItem(itemVirtual, m_pImage[n]);
		}

	Init.AddItem(itemVirtual, m_pValue);
	Init.AddItem(itemVirtual, m_pColor);
	Init.AddItem(itemVirtual, m_pShow);

	Init.AddByte(BYTE(m_Margin.left));
	Init.AddByte(BYTE(m_Margin.top));
	Init.AddByte(BYTE(m_Margin.right));
	Init.AddByte(BYTE(m_Margin.bottom));

	return TRUE;
	}

// Meta Data

void CPrimRubyAnimImage::AddMetaData(void)
{
	CPrimRubyRect::AddMetaData();

	Meta_AddInteger(Keep);
	Meta_AddInteger(Count);
	Meta_AddVirtual(Value);
	Meta_AddVirtual(Color);
	Meta_AddVirtual(Show);

	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		UINT uOffset = (PBYTE(m_pImage + n) - PBYTE(this));

		AddMeta(CPrintf(L"Image%2.2u", 1 + n), metaVirtual, uOffset);
		}

	Meta_AddRect(Margin);

	Meta_SetName((IDS_ANIMATED_IMAGE_2));
	}

// Implementation

R2 CPrimRubyAnimImage::GetImageRect(void)
{
	R2  Rect = m_DrawRect;

	int nAdj = m_pEdge->GetInnerWidth();

	DeflateRect(Rect, nAdj, nAdj);

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	return Rect;
	}

void CPrimRubyAnimImage::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Value",   m_Count > 1);
	pHost->EnableUI("Image01", m_Count > 0);
	pHost->EnableUI("Image02", m_Count > 1);
	pHost->EnableUI("Image03", m_Count > 2);
	pHost->EnableUI("Image04", m_Count > 3);
	pHost->EnableUI("Image05", m_Count > 4);
	pHost->EnableUI("Image06", m_Count > 5);
	pHost->EnableUI("Image07", m_Count > 6);
	pHost->EnableUI("Image08", m_Count > 7);
	pHost->EnableUI("Image09", m_Count > 8);
	pHost->EnableUI("Image10", m_Count > 9);
	}

// End of File
