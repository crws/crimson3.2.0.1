
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Brush
//

// Dynamic Class

AfxImplementDynamicClass(CPrimTankFill, CPrimBrush);

// Static Data

COLOR  CPrimTankFill::m_ShadeCol3 = 0;

C3REAL CPrimTankFill::m_ShadeData = 0;

// Constructor

CPrimTankFill::CPrimTankFill(void)
{
	m_Mode    = 0;

	m_pValue  = NULL;

	m_pMin    = NULL;

	m_pMax    = NULL;

	m_pColor3 = New CPrimColor;
	}

// UI Update

void CPrimTankFill::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			m_fAutoLimits = HasAutoLimits();

			DoEnables(pHost);
			}

		if( Tag == "Mode" ) {

			DoEnables(pHost);
			}

		if( Tag == "Value" ) {

			if( m_fAutoLimits ) {

				if( m_pValue && m_pValue->IsTagRef() ) {

					CString Tag = m_pValue->GetTagName();

					InitCoded(pHost, L"Min", m_pMin, Tag + L".Min");

					InitCoded(pHost, L"Max", m_pMax, Tag + L".Max");

					m_fAutoLimits = TRUE;

					return;
					}
				}

			m_fAutoLimits = HasAutoLimits();
			}

		if( Tag == "Min" || Tag == "Max" ) {

			m_fAutoLimits = HasAutoLimits();
			}
		}

	CPrimBrush::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimTankFill::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" || Tag == "Min" || Tag == "Max" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimBrush::GetTypeData(Tag, Type);
	}

// Operations

void CPrimTankFill::Set(COLOR Color)
{
	CPrimBrush::Set(Color);

	m_pColor3->Set(GetRGB(12,12,12));
	}

BOOL CPrimTankFill::StepAddress(void)
{
	BOOL f1 = StepCoded(m_pValue);

	BOOL f2 = StepCoded(m_pMin);
	
	BOOL f3 = StepCoded(m_pMax);

	return f1 || f2 || f3;
	}

// Drawing

void CPrimTankFill::FillRect(IGDI *pGDI, R2 Rect)
{
	// OPTIM -- Add an optimization to draw using two
	// rectangles when not using a shader for the fill.

	if( m_Mode ) {

		CPrimBrush::FillRect(pGDI, Rect);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeRect(PassRect(Rect), Shader);
			}

		return;
		}

	CPrimBrush::FillRect(pGDI, Rect);
	}

void CPrimTankFill::FillEllipse(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( m_Mode ) {

		CPrimBrush::FillEllipse(pGDI, Rect, uType);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeEllipse(PassRect(Rect), uType, Shader);
			}

		return;
		}

	CPrimBrush::FillEllipse(pGDI, Rect, uType);
	}

void CPrimTankFill::FillWedge(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( m_Mode ) {

		CPrimBrush::FillWedge(pGDI, Rect, uType);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeWedge(PassRect(Rect), uType, Shader);
			}

		return;
		}

	CPrimBrush::FillWedge(pGDI, Rect, uType);
	}

void CPrimTankFill::FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound)
{
	if( m_Mode ) {

		CPrimBrush::FillPolygon(pGDI, pList, uCount, dwRound);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadePolygon(pList, uCount, dwRound, Shader);
			}

		return;
		}

	CPrimBrush::FillPolygon(pGDI, pList, uCount, dwRound);
	}

// Persistance

void CPrimTankFill::Init(void)
{
	CPrimBrush::Init();

	m_pColor3->Set(GetRGB(12,12,12));
	}

// Shaders

BOOL CPrimTankFill::Shader(IGDI *pGDI, int p, int c)
{
	static int nTrip = 0;

	static int nMode = 0;

	if( c ) {

		if( p == 0 ) {

			// LATER -- Check rounding...

			nTrip = int(c * m_ShadeData);

			if( m_ShadeMode == 1 || m_ShadeMode == 4 ) {

				nTrip = c - nTrip;

				nMode = 0;
				}

			if( m_ShadeMode == 2 || m_ShadeMode == 3 ) {

				nMode = 1;
				}

			if( nTrip ) {

				if( nMode == 0 ) {

					pGDI->SetBrushStyle(brushFore);
		
					pGDI->SetBrushFore(m_ShadeCol3);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushNull);
					}

				return TRUE;
				}
			}

		if( p >= nTrip ) {

			if( nMode < 2 ) {

				if( nMode == 0 ) {
	
					pGDI->SetBrushStyle(brushNull);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushFore);

					pGDI->SetBrushFore(m_ShadeCol3);
					}

				nMode = 2;

				return TRUE;
				}
			}

		return FALSE;
		}

	if( m_ShadeMode == 1 || m_ShadeMode == 2 ) {

		return FALSE;
		}

	return TRUE;
	}

// Download Support

BOOL CPrimTankFill::MakeInitData(CInitData &Init)
{
	CPrimBrush::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));

	if( m_Mode ) {

		Init.AddItem(itemVirtual, m_pValue);
		
		Init.AddItem(itemVirtual, m_pMin);
		
		Init.AddItem(itemVirtual, m_pMax);

		Init.AddItem(itemSimple,  m_pColor3);
		}

	return TRUE;
	}

// Meta Data

void CPrimTankFill::AddMetaData(void)
{
	CPrimBrush::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddVirtual(Value);
	Meta_AddVirtual(Min);
	Meta_AddVirtual(Max);
	Meta_AddObject (Color3);

	Meta_SetName((IDS_TANK_FILL));
	}

// Implementation

void CPrimTankFill::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Color3", m_Mode > 0);
	pHost->EnableUI("Value",  m_Mode > 0);
	pHost->EnableUI("Min",    m_Mode > 0);
	pHost->EnableUI("Max",    m_Mode > 0);
	}

C3REAL CPrimTankFill::GetValue(CCodedItem *pItem, C3REAL Default)
{
	if( pItem ) {
		
		if( !pItem->IsBroken() ) {

			return I2R(pItem->Execute(typeReal));
			}
		}

	return Default;
	}

BOOL CPrimTankFill::HasAutoLimits(void)
{
	if( m_pMin && m_pMax ) {

		if( m_pValue ) {
		
			CString c1 = m_pValue->GetSource(FALSE);

			CString c2 = m_pMin->GetSource  (FALSE);

			CString c3 = m_pMax->GetSource  (FALSE);

			if( c2 == c1 + L".Min" ) {

				if( c3 == c1 + L".Max" ) {

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( m_pMin || m_pMax ) {

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CPrimTankFill::PrepTankFill(void)
{
	C3REAL Min = GetValue(m_pMin, 0);

	C3REAL Max = GetValue(m_pMax, 100);

	if( Min != Max ) {

		C3REAL Data = GetValue(m_pValue, 25);

		m_ShadeData = (Data - Min) / (Max - Min);
		}
	else
		m_ShadeData = 0.75;
	
	m_ShadeMode = m_Mode;

	m_ShadeCol3 = m_pColor3->GetColor();

	return TRUE;
	}

// End of File
