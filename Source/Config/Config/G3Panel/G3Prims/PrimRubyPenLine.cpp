
#include "intern.hpp"

#include "PrimRubyPenLine.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Line Pen
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPenLine, CPrimRubyPenBase);

// Constructor

CPrimRubyPenLine::CPrimRubyPenLine(void)
{
	m_End1 = CRubyStroker::endFlat;

	m_End2 = CRubyStroker::endFlat;
	}

// UI Managament

void CPrimRubyPenLine::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Width" ) {

			DoEnables(pHost);
			}
		}

	CPrimRubyPenBase::OnUIChange(pHost, pItem, Tag);
	}

// Stroking

BOOL CPrimRubyPenLine::Stroke(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		s.SetEdgeMode  ((CRubyStroker::EdgeMode ) m_Edge);

		s.SetJoinStyle ((CRubyStroker::JoinStyle) m_Join);

		if( m_End1 == m_End2 && m_End1 < 100 ) {

			s.SetEndStyle((CRubyStroker::EndStyle) m_End1);
			}
		else {
			s.SetEndStyle  (CRubyStroker::endArrow);

			s.SetArrowStyle(m_End1, m_End2);
			}

		s.StrokeOpen(output, figure, 0, m_Width);

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CPrimRubyPenLine::MakeInitData(CInitData &Init)
{
	CPrimRubyPenBase::MakeInitData(Init);

	Init.AddByte(BYTE(m_End1));
	Init.AddByte(BYTE(m_End2));

	return TRUE;
	}

// Meta Data

void CPrimRubyPenLine::AddMetaData(void)
{
	CPrimRubyPenBase::AddMetaData();

	Meta_AddInteger(End1);
	Meta_AddInteger(End2);

	Meta_SetName(IDS_PEN);
	}

// Implementation

void CPrimRubyPenLine::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "End1", m_Width > 0);
	pHost->EnableUI(this, "End2", m_Width > 0);
	}

// End of File
