
#include "intern.hpp"

#include "PrimRubyBevelButton.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyBrush.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyBevelButton, CPrimRubyBevelBase);

// Constructor

CPrimRubyBevelButton::CPrimRubyBevelButton(void)
{
	m_uType    = 0x85;

	m_uActMode = actAlways;
	}

// Overridables

void CPrimRubyBevelButton::Draw(IGDI *pGDI, UINT uMode)
{
	m_Style = IsPressed();

	CPrimRubyBevelBase::Draw(pGDI, uMode);
	}

void CPrimRubyBevelButton::SetInitState(void)
{
	CPrimRubyBevelBase::SetInitState();

	AddText();

	m_pTextItem->m_Lead    = 0;

	m_pTextItem->m_MoveDir = 1;

	m_pTextItem->Set(L"TEXT", TRUE);

	m_pEdge->m_Width = 0;

	SetInitSize(73, 41);
	}

// Meta Data

void CPrimRubyBevelButton::AddMetaData(void)
{
	CPrimRubyBevelBase::AddMetaData();

	Meta_SetName((IDS_BEVEL_BUTTON_2));
	}

// End of File
