
#include "intern.hpp"

#include "PrimRubyGaugeTypeA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeA, CPrimRubyGaugeBase);

// Constructor

CPrimRubyGaugeTypeA::CPrimRubyGaugeTypeA(void)
{
	m_GaugeStyle  = 1;

	m_pColorFace  = New CPrimColor;
	m_pColorOuter = New CPrimColor;
	m_pColorRing1 = New CPrimColor;
	m_pColorRing2 = New CPrimColor;
	m_pColorRing3 = New CPrimColor;
	m_pColorRing4 = New CPrimColor;

	m_ScaleBezel    = 100;

	m_bezelBase   = 0;
	}

// UI Managament

void CPrimRubyGaugeTypeA::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "GaugeStyle" ) {

			DoEnables(pHost);
			}
		}

	CPrimRubyGaugeBase::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

BOOL CPrimRubyGaugeTypeA::HitTest(P2 Pos)
{
	if( PtInRect(m_bound, Pos) ) {

		if( m_pathRing1.HitTest(Pos, 1) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

void CPrimRubyGaugeTypeA::Draw(IGDI *pGdi, UINT uMode)
{
	CRubyGdiLink gdi(pGdi);

	if( m_GaugeStyle < 200 ) {

		if( GetColor(1) & 0x8000 ) {

			gdi.OutputSolid(m_listFace,   GetColor(0),  0);
			gdi.OutputShade(m_listInner,  ShaderDark,   0);
			gdi.OutputShade(m_listOuter,  ShaderChrome, 0);
			}
		else {
			gdi.OutputSolid(m_listFace,   GetColor(0), 0);
			gdi.OutputShade(m_listInner,  ShaderDark,  0);
			gdi.OutputSolid(m_listOuter,  GetColor(1), 0);
			}

		gdi.OutputSolid(m_listRing1,  GetColor(2), 0);
		gdi.OutputSolid(m_listRing2,  GetColor(3), 0);
		gdi.OutputSolid(m_listRing3,  GetColor(4), 0);
		gdi.OutputSolid(m_listRing4,  GetColor(5), 0);
		}

	CPrimRubyGaugeBase::Draw(pGdi, uMode);
	}

void CPrimRubyGaugeTypeA::SetInitState(void)
{
	CPrimRubyGaugeBase::SetInitState();

	m_pColorFace ->Set(GetRGB(31,31,31));
	m_pColorOuter->Set(GetRGB(20,20,20));
	m_pColorRing1->Set(GetRGB(16,16,16));
	m_pColorRing2->Set(GetRGB(24,24,24));
	m_pColorRing3->Set(GetRGB(29,29,29));
	m_pColorRing4->Set(GetRGB(30,30,30));

	SetInitSize(200, 200);
	}

// Download Support

BOOL CPrimRubyGaugeTypeA::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeBase::MakeInitData(Init);

	Init.AddWord(GetColor(0));
	Init.AddWord(GetColor(1));
	Init.AddWord(GetColor(2));
	Init.AddWord(GetColor(3));
	Init.AddWord(GetColor(4));
	Init.AddWord(GetColor(5));

	AddList(Init, m_listFace);
	AddList(Init, m_listOuter);
	AddList(Init, m_listInner);
	AddList(Init, m_listRing1);
	AddList(Init, m_listRing2);
	AddList(Init, m_listRing3);
	AddList(Init, m_listRing4);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeA::AddMetaData(void)
{
	CPrimRubyGaugeBase::AddMetaData();

	Meta_AddInteger(GaugeStyle);
	Meta_AddObject (ColorFace);
	Meta_AddObject (ColorOuter);
	Meta_AddObject (ColorRing1);
	Meta_AddObject (ColorRing2);
	Meta_AddObject (ColorRing3);
	Meta_AddObject (ColorRing4);
	Meta_AddInteger(ScaleBezel);

	Meta_SetName((IDS_TYPE_GAUGE));
	}

// Path Management

void CPrimRubyGaugeTypeA::InitPaths(void)
{
	CPrimRubyGaugeBase::InitPaths();

	m_pathFace  .Empty();
	m_pathOuter .Empty();
	m_pathInner .Empty();
	m_pathRing1 .Empty();
	m_pathRing2 .Empty();
	m_pathRing3 .Empty();
	m_pathRing4 .Empty();
	}

void CPrimRubyGaugeTypeA::MakeLists(void)
{
	FindBoundingRect();

	CPrimRubyGaugeBase::MakeLists();

	m_pathFace  .Transform(m_m);
	m_pathOuter .Transform(m_m);
	m_pathInner .Transform(m_m);
	m_pathRing1 .Transform(m_m);
	m_pathRing2 .Transform(m_m);
	m_pathRing3 .Transform(m_m);
	m_pathRing4 .Transform(m_m);

	m_listFace  .Load(m_pathFace,   true);
	m_listOuter .Load(m_pathOuter,  true);
	m_listInner .Load(m_pathInner,  true);
	m_listRing1 .Load(m_pathRing1,  true);
	m_listRing2 .Load(m_pathRing2,  true);
	m_listRing3 .Load(m_pathRing3,  true);
	m_listRing4 .Load(m_pathRing4,  true);

	FindBoundingRect();
	}

// Color Schemes

COLOR CPrimRubyGaugeTypeA::GetColor(UINT n)
{
	if( m_GaugeStyle == 0 ) {

		switch( n ) {

			case 0: return m_pColorFace ->GetColor();
			case 1: return m_pColorOuter->GetColor();
			case 2: return m_pColorRing1->GetColor();
			case 3: return m_pColorRing2->GetColor();
			case 4: return m_pColorRing3->GetColor();
			case 5: return m_pColorRing4->GetColor();
			}
		}

	if( m_GaugeStyle == 1 ) {

		switch( n ) {

			case 0: return GetRGB(31,31,31);
			case 1: return 0x8000;
			case 2: return GetRGB(16,16,16);
			case 3: return GetRGB(31,31,31);
			case 4: return GetRGB(29,29,29);
			case 5: return GetRGB(30,30,30);
			}
		}

	if( m_GaugeStyle == 2 ) {

		switch( n ) {

			case 0: return GetRGB(31,31,31);
			case 1: return GetRGB(20,20,20);
			case 2: return GetRGB(16,16,16);
			case 3: return GetRGB(24,24,24);
			case 4: return GetRGB(29,29,29);
			case 5: return GetRGB(30,30,30);
			}
		}

	if( m_GaugeStyle == 3 ) {

		switch( n ) {

			case 0: return GetRGB(31,31,31);
			case 1: return GetRGB(16,16,16);
			case 2: return GetRGB(12,12,12);
			case 3: return GetRGB(20,20,20);
			case 4: return GetRGB(29,29,29);
			case 5: return GetRGB(30,30,30);
			}
		}

	if( m_GaugeStyle == 4 ) {

		switch( n ) {

			case 0: return GetRGB(31,31,31);
			case 1: return GetRGB(12,12,12);
			case 2: return GetRGB( 8, 8, 8);
			case 3: return GetRGB(16,16,16);
			case 4: return GetRGB(29,29,29);
			case 5: return GetRGB(30,30,30);
			}
		}

	if( m_GaugeStyle == 5 ) {

		switch( n ) {

			case 0: return GetRGB(31,31,31);
			case 1: return GetRGB( 4, 4, 4);
			case 2: return GetRGB( 2, 2, 2);
			case 3: return GetRGB(12,12,12);
			case 4: return GetRGB(29,29,29);
			case 5: return GetRGB(30,30,30);
			}
		}

	if( m_GaugeStyle == 200 || m_GaugeStyle == 201 ) {

		return 0x8000;
		}

	return 0;
	}

// Bezel Scaling

number CPrimRubyGaugeTypeA::ScaleBezel(number n)
{
	if( n < 0 ) {

		return -ScaleBezel(-n);
		}

	if( n > m_bezelBase ) {

		return m_bezelBase + ((n - m_bezelBase) * m_ScaleBezel / 100.0);
		}

	return n;
	}

// Implementation

void CPrimRubyGaugeTypeA::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"ColorFace",  !m_GaugeStyle);
	pHost->EnableUI(this, L"ColorOuter", !m_GaugeStyle);
	pHost->EnableUI(this, L"ColorRing1", !m_GaugeStyle);
	pHost->EnableUI(this, L"ColorRing2", !m_GaugeStyle);
	pHost->EnableUI(this, L"ColorRing3", !m_GaugeStyle);
	pHost->EnableUI(this, L"ColorRing4", !m_GaugeStyle);
	}

void CPrimRubyGaugeTypeA::FindBoundingRect(void)
{
	GetOuterPath().GetBoundingRect(m_bound);
}

// Overridables

CRubyPath & CPrimRubyGaugeTypeA::GetOuterPath(void)
{
	return (m_GaugeStyle == 200) ? m_pathRing4 : m_pathRing1;

}

// End of File
