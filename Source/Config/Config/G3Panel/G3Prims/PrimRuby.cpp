
#include "intern.hpp"

#include "PrimRuby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRuby, CPrim);

// Constructor

CPrimRuby::CPrimRuby(void)
{
	}

// Overridables

CRect CPrimRuby::GetBoundingRect(void)
{
	CRect Rect;

	Rect.left   = m_bound.x1;
	Rect.top    = m_bound.y1;
	Rect.right  = m_bound.x2;
	Rect.bottom = m_bound.y2;

	return Rect;
	}

void CPrimRuby::SetHand(BOOL fInit)
{
	CPrim::SetHand(fInit);

	UpdateLayout();
	}

void CPrimRuby::UpdateLayout(void)
{
	CPrim::UpdateLayout();

	InitPaths();

	MakePaths();

	MakeLists();
	}

// Download Support

BOOL CPrimRuby::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddWord(WORD(m_bound.x1));
	Init.AddWord(WORD(m_bound.y1));
	Init.AddWord(WORD(m_bound.x2));
	Init.AddWord(WORD(m_bound.y2));

	return TRUE;
	}

// Implementation

BOOL CPrimRuby::AddList(CInitData &Init, CRubyGdiList const &list)
{
	if( list.m_pCount ) {

		Init.AddWord(WORD(list.m_uCount));

		for( UINT c = 0; c < list.m_uCount; c++ ) {

			Init.AddWord(WORD(list.m_pCount[c]));
			}

		Init.AddWord(WORD(list.m_uList));

		for( UINT l = 0; l < list.m_uList; l++ ) {

			P2 const &p = list.m_pList[l];

			Init.AddWord(WORD(p.x));
		
			Init.AddWord(WORD(p.y));
			}

		return TRUE;
		}

	Init.AddWord(0);

	Init.AddWord(0);

	return FALSE;
	}

BOOL CPrimRuby::AddNumber(CInitData &Init, number n)
{
	// cppcheck-suppress invalidPointerCast

	DWORD const *p = (DWORD const *) &n;

	Init.AddLong(p[0]);

	Init.AddLong(p[1]);

	return TRUE;
	}

BOOL CPrimRuby::AddPoint(CInitData &Init, CRubyPoint const &p)
{
	AddNumber(Init, p.m_x);
	
	AddNumber(Init, p.m_y);

	return TRUE;
	}

BOOL CPrimRuby::AddVector(CInitData &Init, CRubyVector const &v)
{
	AddNumber(Init, v.m_x);
	
	AddNumber(Init, v.m_y);

	return TRUE;
	}

BOOL CPrimRuby::AddMatrix(CInitData &Init, CRubyMatrix const &m)
{
	Init.AddByte(BYTE(m.m_fIdent));

	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {
			
			AddNumber(Init, m.m_m.m_e[i][j]);
			}
		}

	return TRUE;
	}

// Path Management

void CPrimRuby::InitPaths(void)
{
	}

void CPrimRuby::MakePaths(void)
{
	}

void CPrimRuby::MakeLists(void)
{
	}

// End of File
