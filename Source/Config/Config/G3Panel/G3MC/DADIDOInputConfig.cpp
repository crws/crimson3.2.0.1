
#include "intern.hpp"

#include "dadidoinputconfig.hpp"

#include "dadidoinputwnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOInputConfig, CCommsItem);

// Property List

CCommsList const CDADIDOInputConfig::m_CommsList[] = {

	{ 0, "Type1",      PROPID_TYPE1,      usageWriteInit,  IDS_NAME_CT1  },
	{ 0, "Type2",      PROPID_TYPE2,      usageWriteInit,  IDS_NAME_CT2  },
	{ 0, "Type3",      PROPID_TYPE3,      usageWriteInit,  IDS_NAME_CT3  },
	{ 0, "Type4",      PROPID_TYPE4,      usageWriteInit,  IDS_NAME_CT4  },
	{ 0, "Type5",      PROPID_TYPE5,      usageWriteInit,  IDS_NAME_CT5  },
	{ 0, "Type6",      PROPID_TYPE6,      usageWriteInit,  IDS_NAME_CT6  },
	{ 0, "Type7",      PROPID_TYPE7,      usageWriteInit,  IDS_NAME_CT7  },
	{ 0, "Type8",      PROPID_TYPE8,      usageWriteInit,  IDS_NAME_CT8  },

	{ 0, "Mode1",      PROPID_MODE1,      usageWriteInit,  IDS_NAME_CT1  },
	{ 0, "Mode2",      PROPID_MODE2,      usageWriteInit,  IDS_NAME_CT2  },
	{ 0, "Mode3",      PROPID_MODE3,      usageWriteInit,  IDS_NAME_CT3  },
	{ 0, "Mode4",      PROPID_MODE4,      usageWriteInit,  IDS_NAME_CT4  },
	{ 0, "Mode5",      PROPID_MODE5,      usageWriteInit,  IDS_NAME_CT5  },
	{ 0, "Mode6",      PROPID_MODE6,      usageWriteInit,  IDS_NAME_CT6  },
	{ 0, "Mode7",      PROPID_MODE7,      usageWriteInit,  IDS_NAME_CT7  },
	{ 0, "Mode8",      PROPID_MODE8,      usageWriteInit,  IDS_NAME_CT8  },

	{ 0, "Filter1",	   PROPID_FILTER1,    usageWriteInit,  IDS_NAME_IF1  },
	{ 0, "Filter2",	   PROPID_FILTER2,    usageWriteInit,  IDS_NAME_IF2  },
	{ 0, "Filter3",	   PROPID_FILTER3,    usageWriteInit,  IDS_NAME_IF3  },
	{ 0, "Filter4",	   PROPID_FILTER4,    usageWriteInit,  IDS_NAME_IF4  },
	{ 0, "Filter5",	   PROPID_FILTER5,    usageWriteInit,  IDS_NAME_IF5  },
	{ 0, "Filter6",	   PROPID_FILTER6,    usageWriteInit,  IDS_NAME_IF6  },
	{ 0, "Filter7",	   PROPID_FILTER7,    usageWriteInit,  IDS_NAME_IF7  },
	{ 0, "Filter8",	   PROPID_FILTER8,    usageWriteInit,  IDS_NAME_IF8  },

	{ 0, "Timebase1",  PROPID_TIMEBASE1,  usageWriteInit,  IDS_NAME_TB1  },
	{ 0, "Timebase2",  PROPID_TIMEBASE2,  usageWriteInit,  IDS_NAME_TB2  },
	{ 0, "Timebase3",  PROPID_TIMEBASE3,  usageWriteInit,  IDS_NAME_TB3  },
	{ 0, "Timebase4",  PROPID_TIMEBASE4,  usageWriteInit,  IDS_NAME_TB4  },
	{ 0, "Timebase5",  PROPID_TIMEBASE5,  usageWriteInit,  IDS_NAME_TB5  },
	{ 0, "Timebase6",  PROPID_TIMEBASE6,  usageWriteInit,  IDS_NAME_TB6  },
	{ 0, "Timebase7",  PROPID_TIMEBASE7,  usageWriteInit,  IDS_NAME_TB7  },
	{ 0, "Timebase8",  PROPID_TIMEBASE8,  usageWriteInit,  IDS_NAME_TB8  },

};

// Constructor

CDADIDOInputConfig::CDADIDOInputConfig(void)
{
	m_Type1      = 0;
	m_Type2      = 0;
	m_Type3      = 0;
	m_Type4      = 0;
	m_Type5      = 0;
	m_Type6      = 0;
	m_Type7      = 0;
	m_Type8      = 0;
	m_Mode1      = 0;
	m_Mode2      = 0;
	m_Mode3      = 0;
	m_Mode4      = 0;
	m_Mode5      = 0;
	m_Mode6      = 0;
	m_Mode7      = 0;
	m_Mode8      = 0;
	m_Filter1    = 0;
	m_Filter2    = 0;
	m_Filter3    = 0;
	m_Filter4    = 0;
	m_Filter5    = 0;
	m_Filter6    = 0;
	m_Filter7    = 0;
	m_Filter8    = 0;
	m_Timebase1  = 0;
	m_Timebase2  = 0;
	m_Timebase3  = 0;
	m_Timebase4  = 0;
	m_Timebase5  = 0;
	m_Timebase6  = 0;
	m_Timebase7  = 0;
	m_Timebase8  = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDADIDOInputConfig::GetPageCount(void)
{
	return 1;
}

CString CDADIDOInputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(L"Inputs");
	}

	return L"";
}

CViewWnd * CDADIDOInputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDADIDOInputWnd;
	}

	return NULL;
}

// Conversion

BOOL CDADIDOInputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 8; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"InputMode%d", n+1)) ) {

				// Legacy

				ConvertMode(n);

				continue;
			}

			if( ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"InpMode%d", n+1)) ) {

				// Graphite

				continue;
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Meta Data Creation

void CDADIDOInputConfig::AddMetaData(void)
{
	Meta_AddInteger(Type1);
	Meta_AddInteger(Type2);
	Meta_AddInteger(Type3);
	Meta_AddInteger(Type4);
	Meta_AddInteger(Type5);
	Meta_AddInteger(Type6);
	Meta_AddInteger(Type7);
	Meta_AddInteger(Type8);

	Meta_AddInteger(Mode1);
	Meta_AddInteger(Mode2);
	Meta_AddInteger(Mode3);
	Meta_AddInteger(Mode4);
	Meta_AddInteger(Mode5);
	Meta_AddInteger(Mode6);
	Meta_AddInteger(Mode7);
	Meta_AddInteger(Mode8);

	Meta_AddInteger(Filter1);
	Meta_AddInteger(Filter2);
	Meta_AddInteger(Filter3);
	Meta_AddInteger(Filter4);
	Meta_AddInteger(Filter5);
	Meta_AddInteger(Filter6);
	Meta_AddInteger(Filter7);
	Meta_AddInteger(Filter8);

	Meta_AddInteger(Timebase1);
	Meta_AddInteger(Timebase2);
	Meta_AddInteger(Timebase3);
	Meta_AddInteger(Timebase4);
	Meta_AddInteger(Timebase5);
	Meta_AddInteger(Timebase6);
	Meta_AddInteger(Timebase7);
	Meta_AddInteger(Timebase8);

	CCommsItem::AddMetaData();
}

// Implementation

void CDADIDOInputConfig::ConvertMode(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Mode%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT uPrev   = pData->ReadInteger(this);

		UINT uData[] = { 2, 1, 0 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
