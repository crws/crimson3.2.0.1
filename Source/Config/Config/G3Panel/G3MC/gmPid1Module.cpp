
#include "intern.hpp"

#include "slcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

class CGMPID1Module : public CSLCModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMPID1Module(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMPID1Module, CSLCModule);

// Constructor

CGMPID1Module::CGMPID1Module(void)
{
	m_FirmID = FIRM_GMPID1;

	m_Ident  = LOBYTE(ID_GMPID1);

	m_Model  = "GMPID1";

	m_Power  = 33;

	m_Conv.Insert(L"Legacy", L"CSLCModule");

	m_Conv.Insert(L"Manticore", L"CDAPID1Module");

	m_Conv.Insert(CString(IDS_MODULE_LOOP), CString(IDS_MODULE_LOOP1));
}

// Download Support

void CGMPID1Module::MakeConfigData(CInitData &Init)
{
	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);
	
	CSLCModule::MakeConfigData(Init);
	}

// End of File
