
#include "intern.hpp"

#include "DAAO8MainWnd.hpp"

#include "DAAO8Module.hpp"

#include "DAAO8Output.hpp"

#include "DAAO8OutputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Universal Analog Input Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAAO8MainWnd, CProxyViewWnd);

// Constructor

CDAAO8MainWnd::CDAAO8MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
}

// Overridables

void CDAAO8MainWnd::OnAttach(void)
{
	m_pItem = (CDAAO8Module *) CProxyViewWnd::m_pItem;

	AddOutputConfig();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDAAO8MainWnd::AddOutputConfig(void)
{
	CDAAO8OutputConfig *pConfig = m_pItem->m_pOutputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

// End of File
