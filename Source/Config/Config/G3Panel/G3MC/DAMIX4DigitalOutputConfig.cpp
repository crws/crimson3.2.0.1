
#include "intern.hpp"

#include "DAMix4DigitalOutputConfig.hpp"

#include "DAMix4DigitalOutputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4DigitalOutputConfig, CCommsItem);

// Property List

CCommsList const CDAMix4DigitalOutputConfig::m_CommsList[] = {

	{ 0, "Enable1",  PROPID_ENABLE1,   usageWriteInit,  IDS_NAME_TPE1 },
	{ 0, "Enable2",  PROPID_ENABLE2,   usageWriteInit,  IDS_NAME_TPE2 },
	{ 0, "Enable3",  PROPID_ENABLE3,   usageWriteInit,  IDS_NAME_TPE3 },

	{ 0, "Mode1",    PROPID_POMODE1,   usageWriteInit,  IDS_NAME_TPC1 },
	{ 0, "Mode2",    PROPID_POMODE2,   usageWriteInit,  IDS_NAME_TPC2 },
	{ 0, "Mode3",    PROPID_POMODE3,   usageWriteInit,  IDS_NAME_TPC3 },

};

// Constructor

CDAMix4DigitalOutputConfig::CDAMix4DigitalOutputConfig(void)
{
	m_Enable1 = 0;
	m_Enable2 = 0;
	m_Enable3 = 0;
	m_Mode1   = 0;
	m_Mode2   = 0;
	m_Mode3   = 0;
	m_Value1  = 0;
	m_Value2  = 0;
	m_Value3  = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAMix4DigitalOutputConfig::GetPageCount(void)
{
	return 1;
}

CString CDAMix4DigitalOutputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return L"Digital Outputs";
	}

	return L"";
}

CViewWnd * CDAMix4DigitalOutputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDAMix4DigitalOutputConfigWnd;
	}

	return NULL;
}

// Conversion

BOOL CDAMix4DigitalOutputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 8; n++ ) {

			ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"OutMode%d", n+1));
		}

		return TRUE;
	}

	return FALSE;
}

// Meta Data Creation

void CDAMix4DigitalOutputConfig::AddMetaData(void)
{
	Meta_AddInteger(Enable1);
	Meta_AddInteger(Enable2);
	Meta_AddInteger(Enable3);

	Meta_AddInteger(Mode1);
	Meta_AddInteger(Mode2);
	Meta_AddInteger(Mode3);

	Meta_AddInteger(Value1);
	Meta_AddInteger(Value2);
	Meta_AddInteger(Value3);

	CCommsItem::AddMetaData();
}

// End of File
