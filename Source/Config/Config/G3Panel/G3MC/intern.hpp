
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3mc.hpp"

#include "intern.hxx"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Model IDs Graphite/Manticore
//

#define ID_GMDN		0x1003
#define ID_GMCAN	0x1004
#define ID_GMPID1	0x1006
#define ID_GMPID2	0x1007
#define ID_GMUIN4	0x1008
#define ID_GMOUT4	0x1009
#define ID_GMDIO14	0x100A
#define ID_GMTC8	0x100B
#define ID_GMINI8	0x100C
#define ID_GMINV8	0x100D
#define ID_GMRTD6	0x100E
#define ID_GMSG1	0x100F
#define ID_GMJ1939	0x1010
#define ID_GMRS		0x1011
#define ID_GMRC		0x1012
#define ID_DADIDO	0x1020
#define ID_DAUIN6	0x1022
#define ID_DAAO8	0x1023
#define ID_DADIRO	0x1024
#define ID_DAPID1_RA	0x1027
#define ID_DAPID1_SA	0x1029
#define ID_DAPID2_SM	0x1028
#define ID_DAPID2_RO	0x102A
#define ID_DAPID2_SO	0x102B
#define ID_DARO8	0x102C
#define ID_DAMIX4	0x102D
#define ID_DAMIX2	0x102E
#define ID_DASG1	0x10FF	// !!! Check This

//////////////////////////////////////////////////////////////////////////
//
// Firmware IDs Graphite/Manticore
//

#define FIRM_GMPID1	1		// GMPID1.bin
#define FIRM_GMPID2	2		// GMPID2.bin
#define FIRM_GMUIN4	3		// GMUIN4.bin
#define FIRM_GMOUT4	4		// GMOUT4.bin
#define FIRM_GMDIO14	5		// GMDIO14.bin
#define FIRM_GMIN8	6		// GMIN8.bin
#define FIRM_GMRTD6	7		// GMRTD6.bin
#define FIRM_GMSG	8		// GMSG.bin
#define FIRM_GMCN	9		// GMCN.bin
#define FIRM_GMDN	10		// GMDN.bin
#define FIRM_GMJ1939	11		// GMJ1939.bin
#define FIRM_GMPB	12		// GMPB.bin
#define FIRM_GMRS	13		// GMRS.bin
#define FIRM_GMDNP3	14		// GMDNP3.bin
#define FIRM_GMCDL	15		// GMCDL.bin
#define FIRM_GMRC	16		// GMRC.bin
#define FIRM_DADIDO	17		// DADIDO.bin
#define FIRM_DADIRO	18		// DADIRO.bin
#define FIRM_DAUIN6	19		// DAUIN6.bin
#define FIRM_DAPID1	20		// DAPID1.bin
#define FIRM_DAPID2	21		// DAPID2.bin
#define FIRM_DASG1	22		// DASG1.bin
#define FIRM_DAAO8	23		// DAAO8.bin
#define FIRM_DARO8	24		// DARO8.bin
#define FIRM_DAMIX4	25		// DAMIX4.bin
#define FIRM_DAMIX2	26		// DAMIX2.bin

//////////////////////////////////////////////////////////////////////////
//
// Model IDs Legacy
//

#define	ID_MASTER	0
#define ID_CSPID1	1
#define ID_CSPID2	2
#define ID_CSTC8	3
#define ID_CSINI8	4
#define ID_CSINV8	5
#define ID_CSDIO14	6
#define ID_CSRTD6	7
#define ID_CSOPI8	8
#define ID_CSOPV8	9
#define ID_CSINI8L	10
#define ID_CSINV8L	11
#define ID_CSSG		12
#define ID_CSOUT4	13
#define ID_CSTC8ISO	14

//////////////////////////////////////////////////////////////////////////
//
// Firmware IDs Legacy
//

#define	FIRM_NONE	0
#define FIRM_PID1	1
#define FIRM_PID2	2
#define FIRM_8IN	3
#define FIRM_DIO14	4
#define FIRM_RTD6	5
#define FIRM_8INL	6
#define FIRM_SG		7
#define FIRM_OUT4	8
#define FIRM_TC8ISO	9
#define FIRM_UIN4	10

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CObjectItem;
class CCommsItem;
class CGenericModule;

//////////////////////////////////////////////////////////////////////////
//
// Property Usage Codes
//

enum
{
	usageRead	= 0,
	usageWriteInit	= 1,
	usageWriteUser	= 2,
	usageWriteBoth	= 3
	};

//////////////////////////////////////////////////////////////////////////
//
// Object Information
//

struct CObjectData
{
	BYTE	    ID;
	CString	    Name;
	CCommsItem *pItem;
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Data for API
//

struct CCommsData
{
	WORD	Group;
	CString	UserName;
	CString	PropName;
	WORD	PropID;
	UINT	Usage;
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Data for Initialization
//

struct CCommsList
{
	WORD	Group;
	PCSTR	pName;
	WORD	PropID;
	UINT	Usage;
	UINT    UserID;
	};

//////////////////////////////////////////////////////////////////////////
//
// Item with Comms Objects
//

class CObjectItem : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Comms Object Access
		virtual UINT GetObjectCount(void);
		virtual BOOL GetObjectData(UINT uIndex, CObjectData &Data);
		virtual BOOL HasUserData(UINT uIndex);

		// Comms Object Helpers
		UINT FindObject(CString Name);
		UINT FindObject(BYTE ID);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Item with Comms Data
//

class CCommsItem : public CMetaItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCommsItem(void);

		// Operations
		void SetID(UINT ID);

		// Group Names
		virtual CString GetGroupName(WORD Group);

		// Comms Schema Access
		UINT GetCommsDataCount(void);
		BOOL GetCommsData(UINT uIndex, CCommsData &Data);
		UINT FindCommsData(CString Name);
		UINT FindCommsData(WORD PropID);

		// Conversion
		virtual BOOL Convert(CPropValue *pValue);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT	           m_ID;
		CCommsList const * m_pCommsData;
		UINT	           m_uCommsCount;

		// Property Filter
		virtual BOOL IncludeProp(WORD PropID);

		// Data Scaling
		virtual DWORD GetIntProp(PCTXT pTag);

		// Validation
		void CheckCommsData(void);
		void CheckComm(void);
		void CheckMeta(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Module
//

class CGenericModule : public CObjectItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGenericModule(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Management
		virtual CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT    GetObjectCount(void);
		BOOL    GetObjectData(UINT uIndex, CObjectData &Data);
		CString GetObjectConv(CString Name) const;

		// Conversion
		virtual BOOL Convert(CPropValue *pValue);

		// Conversion Class
		CString GetClassConv(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CGuid   m_Guid;
		CString m_Model;
		BYTE    m_FirmID;
		UINT	m_Ident;
		UINT    m_Drop;
		UINT    m_Slot;
		UINT    m_Power;

	protected:
		// Data
		CMap<CString, CString>	m_Conv;

		// Meta Data Creation
		void AddMetaData(void);

		// Dirty Management
		void OnSetDirty(void);

		// Download Data
		virtual void MakeConfigData(CInitData &Init);
		virtual void MakeObjectData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite Generic Module
//

class CGraphiteGenericModule : public CGenericModule
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CGraphiteGenericModule(void);

protected:
	// Download Data
	void MakeConfigData(CInitData &Init);
};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CModuleMultiViewWnd;

//////////////////////////////////////////////////////////////////////////
//								
// Module Multi View Window
//

class CModuleMultiViewWnd : public CMultiViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CModuleMultiViewWnd(void);

	protected:
		// Overridables
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRackDriver;
class CRackDeviceOptions;
class CRackAddrDialog;

//////////////////////////////////////////////////////////////////////////
//
// Rack Comms Driver
//

class CRackDriver : public ICommsDriver
{
	public:
		// Constructor
		CRackDriver(void);

		// Destructor
		~CRackDriver(void);

		// IBase
		UINT Release(void);

		// Driver Data
		WORD	GetID(void);
		UINT	GetType(void);
		CString GetString(UINT ID);
		UINT    GetFlags(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Tag Import
		BOOL MakeTags(IMakeTags * pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem *pDrvCfg);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem *pDrvCfg);
		BOOL SelectAddress(HWND	hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL SelectAddress(HWND	hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem *pDrvCfg);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT	uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
		BOOL IsMappingDisabled(void);
		BOOL IsAddrNamed(CItem *pConfig, CAddress const &Addr);

		// Notifications
		void NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);
		void NotifyInit(CItem * pConfig);

	protected:
		// Data Members
		CGenericModule * m_pModule;
		CCommsItem     * m_pItem;
		UINT	         m_uObject;
		UINT             m_uExtra;
		UINT	         m_uGroup;
		UINT	         m_uIndex;

		// Module Location
		BOOL FindModule(CItem *pConfig);

		// Implementation
		UINT GetType(WORD PropID);
		UINT GetRest(WORD PropID);
		UINT GetExtra(BYTE ID);

		// Friends
		friend class CRackAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Rack Device Options
//

class CRackDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRackDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rack Address Selection Dialog
//

class CRackAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRackAddrDialog(CRackDriver &Driver, CAddress &Addr, CItem *pConfig);

	protected:
		// Data Members
		CRackDriver  * m_pDriver;
		CObjectItem  * m_pModule;
		CItem	     * m_pConfig;
		CAddress     * m_pAddr;
		BYTE	       m_ObjectID;
		UINT	       m_uGroup;
		WORD	       m_PropID;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnObjectChange(UINT uID, CWnd &Wnd);
		void OnGroupChange(UINT uID, CWnd &Wnd);
		void OnPropChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void SetCaption(void);
		void LoadObjectList(BOOL fInit);
		void LoadGroupList(BOOL fInit);
		void LoadPropList(BOOL fInit);
	};

/////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CFirmwareList;
class CFirmwareFile;

//////////////////////////////////////////////////////////////////////////
//
// Firmware Collection
//

class CFirmwareList : public CItemList
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CFirmwareList(void);

		// Download Support
		void PrepareData (void);
		BOOL MakeInitData(CInitData &Init);

		// Item Access
		CFirmwareFile * GetItem(BYTE  FirmID) const;
		CFirmwareFile * GetItem(INDEX Index ) const;

	protected:
		// Implementation
		void MarkNotUsed(void);
		void ScanRack(void);
		void ScanComms(void);
		void DeleteOld(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Firmware File
//

class CFirmwareFile : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFirmwareFile(void);

		// Data Members
		UINT m_fUsed;
		UINT m_Handle;
		UINT m_FirmID;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		CString GetProc(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Property Loader
//

class CPropConverter
{
	public:
		// Constructor
		CPropConverter(void);

		// Destructor
		~CPropConverter(void);

		// Operations
		BOOL LoadFromItem(CMetaItem const *pItem);

		// Data Members
		CPropValue m_Root;

	protected:
		// Data Members
		CString m_Last;
		CString m_Path;

		// Implementation
		BOOL LoadProp(CPropValue *pRoot, CMetaData const *pMeta, PVOID pItem);
		BOOL LoadObject (CPropValue *pRoot, CMetaItem const *pItem);
		BOOL LoadCollect(CPropValue *pRoot, CMetaItem const *pItem);
		BOOL LoadVirtual(CPropValue *pRoot, CMetaItem const *pItem);

		CString ReadString(CMetaData const *pMeta, PVOID pItem);
		CString ReadInteger(CMetaData const *pMeta, PVOID pItem);
	};

// End of File

#endif
