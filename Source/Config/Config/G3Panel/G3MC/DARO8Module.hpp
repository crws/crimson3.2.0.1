
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DARO8Module_HPP

#define INCLUDE_DARO8Module_HPP

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDARO8Output;
class CDARO8OutputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DARO8 Module
//

class CDARO8Module : public CManticoreGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDARO8Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Item Properties
		CDARO8Output       * m_pOutput;
		CDARO8OutputConfig * m_pOutputConfig;

	protected:

		// Implementation
		void AddMetaData(void);
	};

// End of File

#endif

