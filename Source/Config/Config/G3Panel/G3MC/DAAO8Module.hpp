
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAAO8Module_HPP

#define INCLUDE_DAAO8Module_HPP

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDAAO8Output;
class CDAAO8OutputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 Module
//

class CDAAO8Module : public CManticoreGenericModule
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDAAO8Module(void);

	// UI Management
	CViewWnd * CreateMainView(void);

	// Comms Object Access
	UINT GetObjectCount(void);
	BOOL GetObjectData(UINT uIndex, CObjectData &Data);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	CDAAO8Output       * m_pOutput;
	CDAAO8OutputConfig * m_pOutputConfig;

protected:
	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
