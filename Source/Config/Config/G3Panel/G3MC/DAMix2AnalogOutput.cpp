
#include "intern.hpp"

#include "DAMix2AnalogOutput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daao8props.h"

#include "import/manticore/daao8dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2AnalogOutput, CCommsItem);

// Property List

CCommsList const CDAMix2AnalogOutput::m_CommsList[] = {

	{ 1, "Data1",		PROPID_DATA1,	usageWriteBoth,  IDS_NAME_DATA1	},
	{ 1, "Data2",		PROPID_DATA2,	usageWriteBoth,  IDS_NAME_DATA2	},

	{ 2, "Alarm1",		PROPID_ALARM1,	usageRead,	 IDS_NAME_A1	},
	{ 2, "Alarm2",		PROPID_ALARM2,	usageRead,	 IDS_NAME_A2	},

};

// Constructor

CDAMix2AnalogOutput::CDAMix2AnalogOutput(void)
{
	m_Data1 = 0;
	m_Data2 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAMix2AnalogOutput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Data");

		case 2:	return CString(L"Alarms");
	}

	return CCommsItem::GetGroupName(Group);
}

// Property Filter

BOOL CDAMix2AnalogOutput::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_DATA1:
			case PROPID_DATA2:

				return FALSE;
		}
	}

	return TRUE;
}

// Meta Data Creation

void CDAMix2AnalogOutput::AddMetaData(void)
{
	Meta_AddInteger(Data1);
	Meta_AddInteger(Data2);

	Meta_AddInteger(InitData);

	CCommsItem::AddMetaData();
}

// End of File
