
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2DigitalInput_HPP

#define INCLUDE_DAMix2DigitalInput_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Inputs
//

class CDAMix2DigitalInput : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2DigitalInput(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_Input1;
	UINT m_Input2;
	UINT m_Input3;
	UINT m_Input4;
	UINT m_Input5;
	UINT m_Input6;
	UINT m_Input7;
	UINT m_Input8;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
