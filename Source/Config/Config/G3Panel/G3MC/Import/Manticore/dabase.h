
//////////////////////////////////////////////////////////////////////////
//
// Manticore Module
//
// Copyright (c) 2001-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DABASE_H

#define	INCLUDE_DABASE_H

//////////////////////////////////////////////////////////////////////////
//
// Installation Data
//

typedef struct tagInstallDA
{
	} INSTALLDA;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigDA
{
	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIGDA;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusDA
{
	} STATUSDA;

// End of File

#endif
