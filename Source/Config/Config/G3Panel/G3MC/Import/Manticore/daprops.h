
//////////////////////////////////////////////////////////////////////////
//
// Manticore DADIDO Module
//
// Copyright (c) 2001-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAPROPS_H

#define	INCLUDE_DAPROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	MAKEPROP(type, id)	MAKEWORD(id, type)

//////////////////////////////////////////////////////////////////////////
//
// Object IDs
//

#define	OBJ_INPUT		0x00
#define OBJ_OUTPUT		0x01
#define OBJ_CONFIG_DI		0x02
#define OBJ_CONFIG_DO		0x03
#define OBJ_CONFIG_AI		0x04
#define OBJ_CONFIG_AO		0x05
#define OBJ_GLOBAL		0x06
#define OBJ_MODULE		0x07

//////////////////////////////////////////////////////////////////////////
//
// Property Types
//

#define	TYPE_BOOL		0x01	//  8-BIT
#define	TYPE_BYTE		0x02	//  8-BIT
#define	TYPE_WORD		0x03	// 16-BIT
#define	TYPE_INT		0x04	// 16-BIT
#define	TYPE_REAL		0x05	// 16-BIT
#define TYPE_INT32		0x06	// 32-BIT 
#define TYPE_UINT32		0x07	// 32-BIT

//////////////////////////////////////////////////////////////////////////
//
// Property Type Macros
//

#define TYPE_DI			TYPE_BOOL
#define	TYPE_DO			TYPE_BOOL
#define TYPE_AI			TYPE_INT32
#define TYPE_AO			TYPE_INT32
#define TYPE_LI			TYPE_UINT32
#define TYPE_LO			TYPE_UINT32
#define TYPE_FI			TYPE_REAL
#define TYPE_FO			TYPE_REAL

//////////////////////////////////////////////////////////////////////////
//
// Property Type Macros
//

#define	PROPID_DI(n)		MAKEPROP(TYPE_DI, n)
#define PROPID_DO(n)		MAKEPROP(TYPE_DO, n)
#define PROPID_AI(n)		MAKEPROP(TYPE_AI, n)
#define PROPID_AO(n)		MAKEPROP(TYPE_AO, n)
#define PROPID_LI(n)		MAKEPROP(TYPE_LI, n)
#define PROPID_LO(n)		MAKEPROP(TYPE_LO, n)
#define PROPID_FI(n)		MAKEPROP(TYPE_FI, n)
#define PROPID_FO(n)		MAKEPROP(TYPE_FO, n)

//////////////////////////////////////////////////////////////////////////
//
// Property Index
//

#define PROPIDGRP(n)		((n-1)*8)

#define PROPIDX(n,o)		(PROPIDGRP(n) + o)

// End of File

#endif
