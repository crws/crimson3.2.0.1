
#include "daprops.h"

//////////////////////////////////////////////////////////////////////////
//
// Manticore DAUIN6 Module
//
// Copyright (c) 2001-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMIXPROPS_H

#define	INCLUDE_DAMIXPROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Installation
//

// Object : DI

#define	PROPID_MODE1		MAKEPROP(TYPE_DI,     0x01)
#define	PROPID_MODE2		MAKEPROP(TYPE_DI,     0x02)
#define	PROPID_MODE3		MAKEPROP(TYPE_DI,     0x03)
#define	PROPID_MODE4		MAKEPROP(TYPE_DI,     0x04)
#define	PROPID_MODE5		MAKEPROP(TYPE_DI,     0x05)
#define	PROPID_MODE6		MAKEPROP(TYPE_DI,     0x06)
#define	PROPID_MODE7		MAKEPROP(TYPE_DI,     0x07)
#define	PROPID_MODE8		MAKEPROP(TYPE_DI,     0x08)

// End of File

#endif
