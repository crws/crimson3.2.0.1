
//////////////////////////////////////////////////////////////////////////
//
// R307 Tacoma - 4-Channel Analog Output Module
//
// Copyright (c) 2001-2006 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_OUT4DBASE_H

#define	INCLUDE_OUT4DBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Analog Output Types
//

#define	OUT_5V		1
#define	OUT_10V		2
#define	OUT_BIP10V	3
#define	OUT_020MA	4
#define	OUT_420MA	5

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalibOut4
{
	WORD	Out5V0[4];
	WORD	Out5V5[4];
	WORD	Out10V0[4];
	WORD	Out10V10[4];
	WORD	OutN10[4];
	WORD	OutP10[4];
	WORD	OutI0[4];
	WORD	OutI4[4];
	WORD	OutI20[4];

	} CALIBOUT4;

//////////////////////////////////////////////////////////////////////////
//
// Installation Data
//

typedef struct tagInstallOut4
{
	BYTE	OutType[4];

	} INSTALLOUT4;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigOut4
{
	INT	Data[4];
	INT	DataLo[4];
	INT	DataHi[4];
	INT	OutLo[4];
	INT	OutHi[4];
	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIGOUT4;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusOut4
{
	BYTE	Alarm1:1;
	BYTE	Alarm2:1;
	BYTE	Alarm3:1;
	BYTE	Alarm4:1;
	BYTE	SpareAlarm:4;
	//==
	BYTE	Running;

	} STATUSOUT4;

// End of File

#endif
