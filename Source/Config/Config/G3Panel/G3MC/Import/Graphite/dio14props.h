
//////////////////////////////////////////////////////////////////////////
//
// R307 Tacoma - 8-In 6-Out Digital Module
//
// Copyright (c) 2001-2006 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DIO14PROPS_H

#define	INCLUDE_DIO14PROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	MAKEPROP(type, id)	MAKEWORD(id, type)

//////////////////////////////////////////////////////////////////////////
//
// Object IDs
//

#define	OBJ_LOOP_1		0x01

//////////////////////////////////////////////////////////////////////////
//
// Property Types
//

#define	TYPE_BOOL		0x01
#define	TYPE_BYTE		0x02
#define	TYPE_WORD		0x03
#define	TYPE_LONG		0x04
#define	TYPE_REAL		0x05

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Installation
//

#define	PROPID_DIO_IN1		MAKEPROP(TYPE_BYTE, 0x01)
#define	PROPID_DIO_IN2		MAKEPROP(TYPE_BYTE, 0x02)
#define	PROPID_DIO_IN3		MAKEPROP(TYPE_BYTE, 0x03)
#define	PROPID_DIO_IN4		MAKEPROP(TYPE_BYTE, 0x04)
#define	PROPID_DIO_IN5		MAKEPROP(TYPE_BYTE, 0x05)
#define	PROPID_DIO_IN6		MAKEPROP(TYPE_BYTE, 0x06)
#define	PROPID_DIO_IN7		MAKEPROP(TYPE_BYTE, 0x07)
#define	PROPID_DIO_IN8		MAKEPROP(TYPE_BYTE, 0x08)

#define	PROPID_DIO_OUT1		MAKEPROP(TYPE_BYTE, 0x09)
#define	PROPID_DIO_OUT2		MAKEPROP(TYPE_BYTE, 0x0A)
#define	PROPID_DIO_OUT3		MAKEPROP(TYPE_BYTE, 0x0B)
#define	PROPID_DIO_OUT4		MAKEPROP(TYPE_BYTE, 0x0C)
#define	PROPID_DIO_OUT5		MAKEPROP(TYPE_BYTE, 0x0D)
#define	PROPID_DIO_OUT6		MAKEPROP(TYPE_BYTE, 0x0E)

#define	PROPID_DIO_TEST1	MAKEPROP(TYPE_BYTE, 0x10)
#define	PROPID_DIO_TEST2	MAKEPROP(TYPE_BYTE, 0x11)
#define	PROPID_DIO_TEST3	MAKEPROP(TYPE_BYTE, 0x12)
#define	PROPID_DIO_TEST4	MAKEPROP(TYPE_BYTE, 0x13)
#define	PROPID_DIO_TEST5	MAKEPROP(TYPE_BYTE, 0x14)
#define	PROPID_DIO_TEST6	MAKEPROP(TYPE_BYTE, 0x15)

#define	PROPID_DIO_HOLDER	MAKEPROP(TYPE_BYTE, 0x16)

#define	PROPID_INPUT_MODE1	MAKEPROP(TYPE_BYTE, 0x17)
#define	PROPID_INPUT_MODE2	MAKEPROP(TYPE_BYTE, 0x18)
#define	PROPID_INPUT_MODE3	MAKEPROP(TYPE_BYTE, 0x19)
#define	PROPID_INPUT_MODE4	MAKEPROP(TYPE_BYTE, 0x1A)
#define	PROPID_INPUT_MODE5	MAKEPROP(TYPE_BYTE, 0x1B)
#define	PROPID_INPUT_MODE6	MAKEPROP(TYPE_BYTE, 0x1C)
#define	PROPID_INPUT_MODE7	MAKEPROP(TYPE_BYTE, 0x1D)
#define	PROPID_INPUT_MODE8	MAKEPROP(TYPE_BYTE, 0x1E)

#define	PROPID_OUTPUT_MODE1	MAKEPROP(TYPE_BYTE, 0x1F)
#define	PROPID_OUTPUT_MODE2	MAKEPROP(TYPE_BYTE, 0x20)
#define	PROPID_OUTPUT_MODE3	MAKEPROP(TYPE_BYTE, 0x21)
#define	PROPID_OUTPUT_MODE4	MAKEPROP(TYPE_BYTE, 0x22)
#define	PROPID_OUTPUT_MODE5	MAKEPROP(TYPE_BYTE, 0x23)
#define	PROPID_OUTPUT_MODE6	MAKEPROP(TYPE_BYTE, 0x24)

// End of File

#endif
