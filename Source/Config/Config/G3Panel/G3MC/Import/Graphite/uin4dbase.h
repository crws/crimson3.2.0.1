
//////////////////////////////////////////////////////////////////////////
//
// R307 Tacoma - UIN4 Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UIN4DBASE_H

#define	INCLUDE_UIN4DBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Out of Range Value
//

#define	UPSCALE		((LONG) 0x7000000)

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Modes
//

#define	INPUT_010V	1
#define	INPUT_050MV	2
#define	INPUT_020MA	3
#define	INPUT_420MA	4
#define	INPUT_RTD	5
#define	INPUT_TC	6

//////////////////////////////////////////////////////////////////////////
//
// Input Fault Values
//

#define	IF_POS22MA	65336
#define	IF_POS2MA	15147
#define	IF_NEG2MA	4852

#define	IF_NEG3MV	7379
#define	IF_POS53MV	56312

#define	IF_POS10P5V	64378
#define	IF_NEG0P5V	7410

//////////////////////////////////////////////////////////////////////////
//
// Thermocouple  & RTD Types
// 
	
#define	TC_TYPEB	1
#define TC_TYPEC	2	
#define TC_TYPEE 	3
#define	TC_TYPEJ	4
#define TC_TYPEK	5	
#define TC_TYPEN	6
#define	TC_TYPER	7	
#define TC_TYPES	8
#define	TC_TYPET	9	
#define	TC_TYPE385	10
#define TC_TYPE392	11
#define	TC_TYPE672	12

//////////////////////////////////////////////////////////////////////////
//
// Temperature Scale
//

#define	DEGREES_K	1
#define	DEGREES_F	2
#define	DEGREES_C	3

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalib
{
	WORD	InputTN10[4];
	WORD	InputTP56[4];
	WORD	InputV0[4];
	WORD	InputV10[4];
	WORD	InputI0[4];
	WORD	InputI4[4];
	WORD	InputI20[4];
	WORD	CJTemp[4]; 
	WORD	CJVolt[4];
	WORD	CJSlope[4];
	INT	RTDSignal[4];
	INT	RTDExcite[4];
	WORD	RTDK[4];

	} CALIB;

//////////////////////////////////////////////////////////////////////////
//
// Installation Data
//

typedef struct tagInstall
{
	BYTE	InputType[4];
	BYTE	InputTC[4];
	BYTE	TempUnits[4];
	BYTE	ReqSqRoot[4];


	//t//////
	//BYTE	DigHeat:1;
	//BYTE	DigCool:1;
	//BYTE	HCMLatchLo:1;
	//BYTE	HCMLatchHi:1;
	//BYTE	DummyLatch:1;
	//BYTE	DummyRoot:1;
	//BYTE	SpareLatch:2;
	//t///////////


	} INSTALL;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfig
{
	WORD	InputFilter[4];
	INT	InputOffset[4];
	WORD	InputSlope[4];
	INT	ProcMin[4];
	INT	ProcMax[4];

	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIG;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatus
{
	LONG	Input[4];
	LONG	RangeLo[4];
	LONG	RangeHi[4];
	float	PV[4];
	WORD    ColdJunc[4];
	BYTE	InputAlarm[4];
	BYTE	Running;

	} STATUS;

// End of File

#endif
