
//////////////////////////////////////////////////////////////////////////
//
// R307 Tacoma - 8-In 6-Out Digital Module
//
// Copyright (c) 2001-2006 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DIO14DBASE_H

#define	INCLUDE_DIO14DBASE_H

//////////////////////////////////////////////////////////////////////////
//
// General defines
//

#define	ACTIVE_LO	0
#define	ACTIVE_HI	1

//////////////////////////////////////////////////////////////////////////
//
// Installation Data
//

typedef struct tagInstallDio14
{
	BYTE	Test[6];

	} INSTALLDIO14;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigDio14
{
	BYTE	Output[6];
	WORD	Holder;
	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIGDIO14;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusDio14
{
	BYTE	Input[8];
	BYTE	Running;

	} STATUSDIO14;

// End of File

#endif
