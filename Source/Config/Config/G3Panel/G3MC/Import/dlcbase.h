
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - PID Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DLCBASE_H

#define	INCLUDE_DLCBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Out of Range Value
//

#define	UPSCALE		((LONG) 0x7000000)

//////////////////////////////////////////////////////////////////////////
//
// Control Modes
//

#define	MODE_HEAT	1
#define	MODE_COOL	2
#define	MODE_BOTH	3

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Modes
//

#define	INPUT_010V	1
#define	INPUT_050MV	2
#define	INPUT_020MA	3
#define	INPUT_420MA	4
#define	INPUT_RTD	5
#define	INPUT_TC	6

//////////////////////////////////////////////////////////////////////////
//
// Input Fault Values
//

#define	IF_POS22MA	0xE900
#define	IF_NEG2MA	0x1900
#define	IF_NEG3MV	0x1F00
#define	IF_POS53MV	0xDFF0
#define	IF_POS10P3V	0xDCA0
#define	IF_NEG0P3V	0x23D0

//////////////////////////////////////////////////////////////////////////
//
// Thermocouple  & RTD Types
// 
	
#define	TC_TYPEB	1
#define TC_TYPEC	2	
#define TC_TYPEE 	3
#define	TC_TYPEJ	4
#define TC_TYPEK	5	
#define TC_TYPEN	6
#define	TC_TYPER	7	
#define TC_TYPES	8
#define	TC_TYPET	9	
#define	TC_TYPE385	10
#define TC_TYPE392	11
#define	TC_TYPE672	12

//////////////////////////////////////////////////////////////////////////
//
// Alarm Modes
//

#define	ALARM_NULL	0x00
#define	ALARM_ABS_LO	0x01
#define	ALARM_ABS_HI	0x02
#define	ALARM_DEV_LO	0x03
#define	ALARM_DEV_HI	0x04
#define	ALARM_BAND_IN	0x05
#define	ALARM_BAND_OUT	0x06

//////////////////////////////////////////////////////////////////////////
//
// Mapping Test Values
//

#define	MAP_MASK_CODE	0x7F
#define	MAP_MASK_TYPE	0x70
#define	MAP_MASK_NOTB	0x40
#define	MAP_MASK_BIT	0x07
#define	MAP_SHFT_BYTE	0x04
#define	MAP_TYPE_OUT	0x50
#define	MAP_TYPE_ANL	0x70

//////////////////////////////////////////////////////////////////////////
//
// Analog Data Items
//

#define	ANL_NULL	0x00		// Renumber once tested!  
#define	ANL_HEAT	0x70		// Change CSPID1 as well!
#define	ANL_COOL	0x71
#define	ANL_REQ_SP	0x72
#define	ANL_ACT_SP	0x73
#define	ANL_PV		0x74
#define	ANL_ERROR	0x75
#define	ANL_HEAT_2	0x76		
#define	ANL_COOL_2	0x77
#define	ANL_REQ_SP_2	0x7C
#define	ANL_ACT_SP_2	0x7D
#define	ANL_PV_2	0x7E
#define	ANL_ERROR_2	0x7F
#define ANL_REMOTE1	0x78
#define ANL_REMOTE2	0x79
#define ANL_REMOTE3	0x7A
#define ANL_REMOTE4	0x7B

//////////////////////////////////////////////////////////////////////////
//
// Digital Data Items
//

#define	DIG_NULL	0x00
#define	DIG_MANUAL	0x10
#define	DIG_TUNEBUSY	0x11
#define	DIG_TUNEDONE	0x12
#define	DIG_TUNEFAIL	0x13
#define	DIG_ALARM1	0x20
#define	DIG_ALARM2	0x21
#define	DIG_ALARM3	0x22
#define	DIG_ALARM4	0x23
#define	DIG_HCMLO	0x24
#define	DIG_HCMHI	0x25
#define	DIG_RANGE	0x26
#define	DIG_OUTLO	0x30
#define	DIG_OUTHI	0x31
#define	DIG_BADIN	0x32 
#define	DIG_MANUAL_2	0x14
#define	DIG_TUNEBUSY_2	0x15
#define	DIG_TUNEDONE_2	0x16
#define	DIG_TUNEFAIL_2	0x17
#define	DIG_ALARM1_2	0x27
#define	DIG_ALARM2_2	0x28
#define	DIG_ALARM3_2	0x29
#define	DIG_ALARM4_2	0x2A
#define	DIG_HCMLO_2	0x2B
#define	DIG_HCMHI_2	0x2C
#define	DIG_RANGE_2	0x2D
#define	DIG_OUTLO_2	0x37
#define	DIG_OUTHI_2	0x38
#define	DIG_BADIN_2	0x39 
#define DIG_REMOTE1	0x33
#define DIG_REMOTE2	0x34
#define DIG_REMOTE3	0x35
#define DIG_REMOTE4	0x36
#define	DIG_INVERT	0x80

//////////////////////////////////////////////////////////////////////////
//
// Combined Data Items
//

#define	ANY_ALARM	0x40	
#define	ANY_HCM		0x41
#define	ANY_EITHER	0x42	
#define	ANY_ALARM_1	0x43	
#define	ANY_HCM_1	0x44
#define	ANY_EITHER_1	0x45
#define	ANY_ALARM_2	0x46	
#define	ANY_HCM_2	0x47	
#define	ANY_EITHER_2	0x48	
#define	ANY_INVERT	0x80

//////////////////////////////////////////////////////////////////////////
//
// Additional LED Mappings
//

#define	LED_NULL	0x00
#define	LED_OUT1	0x50
#define	LED_OUT2	0x51
#define	LED_OUT3	0x52
#define	LED_OUT4	0x53
#define	LED_INVERT	0x80

//////////////////////////////////////////////////////////////////////////
//
// Derrived Data Items
//

#define	DIG_AUTO	(DIG_MANUAL | DIG_INVERT)
#define	DIG_OKAY	(ANY_EITHER | DIG_INVERT)

//////////////////////////////////////////////////////////////////////////
//
// Number of Control (PID) Loops 
//

#define LOOPS	2

//////////////////////////////////////////////////////////////////////////
//
// Profile Segment
//

typedef struct tagSegment_DLC
{
	WORD	Time;
	WORD	SP;
	BYTE	Mode;
	
	} SEGDLC;

//////////////////////////////////////////////////////////////////////////
//
// Profile Data
//

typedef struct tagProfile_DLC
{
	SEGDLC	Seg[30];

	} PROFDLC;

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalib_DLC
{
	WORD	HCMI0;
	WORD	HCMI100;
	WORD	InputTN10;
	WORD	InputTP56;
	WORD	InputV0;
	WORD	InputV10;
	WORD	InputI0;
	WORD	InputI4;
	WORD	InputI20;
	WORD	CJTemp; 
	WORD	CJVolt;
	WORD	CJSlope;
	INT	RTDSignal;
	INT	RTDExcite;
	WORD	RTDK;

	} CALIB_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Mapper Configuration
//

typedef	struct tagMapper_DLC
{
	BYTE    DigRemote1:1;		
	BYTE    DigRemote2:1;
	BYTE    DigRemote3:1;
	BYTE    DigRemote4:1;
	BYTE 	SpareRemote:4;	
	//==
	BYTE	DigOutMap[4];	 
	BYTE	LedOutMap[6];	 
	WORD	CycleTime[4];	 
	WORD 	AnlRemote1;		
	WORD 	AnlRemote2;
	WORD 	AnlRemote3;
	WORD 	AnlRemote4;	

	} MAPPER_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Installation Data
//

typedef struct tagInstall_DLC
{
	BYTE	DigHeat:1;
	BYTE	DigCool:1;
	BYTE	HCMLatchLo:1;
	BYTE	HCMLatchHi:1;
	BYTE	RangeLatch:1;
	BYTE	ReqSqRoot:1;
	BYTE	SpareLatch:2;
	//==
	BYTE	AlarmDelay1:1;			
	BYTE	AlarmDelay2:1;
	BYTE	AlarmDelay3:1;
	BYTE	AlarmDelay4:1;
	BYTE	AlarmLatch1:1;						
	BYTE	AlarmLatch2:1;
	BYTE	AlarmLatch3:1;
	BYTE	AlarmLatch4:1; 
	//== 
	BYTE	Mode;
	BYTE	TempUnits;
	BYTE	InputType;
	BYTE	InputTC;
	BYTE	HCMChannel;
	BYTE	AlarmMode[4];

	} INSTALL_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfig_DLC
{
	BYTE	ReqManual:1;
	BYTE	ReqTune:1;
	BYTE	ReqUserPID:1;
	BYTE	ReqProfile:1;
	BYTE	ReqHold:1;
	BYTE	ReqAltSP:1;
	BYTE	ReqAltPV:1;
	BYTE	ReqSpare:1;
	//==
	BYTE	AlarmAccept1:1;
	BYTE	AlarmAccept2:1;
	BYTE	AlarmAccept3:1;
	BYTE	AlarmAccept4:1;
	BYTE	HCMAcceptLo:1;
	BYTE	HCMAcceptHi:1;
	BYTE	RangeAccept:1;
	BYTE	SpareAccept:1;	
	//==
	BYTE	TuneCode;
	INT	Power;
	WORD	SP;
	WORD	AltSP;
	WORD	AltPV;
	WORD	SetHyst;
	INT	SetDead;
	WORD	SetRamp;
	BYTE	SetRampBase;
	//==
	WORD	InputFilter;
	INT	InputOffset;
	WORD	InputSlope;
	//==
	WORD	UserConstP;
	WORD	UserConstI;
	WORD	UserConstD;
	WORD	UserCLimit;
	WORD	UserHLimit;
	WORD	UserFilter;
	WORD	AutoConstP;
	WORD	AutoConstI;
	WORD	AutoConstD;
	WORD	AutoCLimit;
	WORD	AutoHLimit;
	WORD	AutoFilter;
	//==
	INT	PowerFault;
	INT	PowerOffset;
	INT	PowerDead;
	WORD	PowerHeatGain;
	WORD	PowerCoolGain;
	WORD	PowerHeatHyst;
	WORD	PowerCoolHyst;
	WORD	HeatLimitLo;
	WORD	HeatLimitHi;
	WORD	CoolLimitLo;
	WORD	CoolLimitHi;
	//==
	WORD	HCMLimitLo;
	WORD	HCMLimitHi;
	INT	AlarmData[4];
	INT	AlarmHyst[4];
	//==
	BYTE	ReqSegment;
	BYTE	EndSegment;
	INT	ProfError;
	INT	ProfHyst;
	PROFDLC	Profile;
	//==

	} CONFIG_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Module Data
//
typedef struct tagModule_DLC
{
	BYTE	GUID[16];
	BYTE	Valid;

	} MODULE_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatus_DLC
{
	BYTE	AckManual:1;		// pidmain
	BYTE	AckTune:1;		// pidmain
	BYTE	TuneDone:1;		// pidtune
	BYTE	TuneFail:1;		// pidtune
	BYTE	AckProfile:1;		// pidmain
	BYTE	AckHold:1;		// pidmain
	BYTE	AutoHold:1;		// pidmain
	BYTE	ProfDone:1;		// pidmain
	//==
	BYTE	Alarm1:1;		// alarm
	BYTE	Alarm2:1;		// alarm
	BYTE	Alarm3:1;		// alarm
	BYTE	Alarm4:1;		// alarm
	BYTE	HCMAlarmLo:1;		// hcm
	BYTE	HCMAlarmHi:1;		// hcm
	BYTE	RangeAlarm:1;		// pidmain
	BYTE	SpareAlarm:1;		// spare
	//==
	BYTE	OutLimitLo:1;		// pidout
	BYTE	OutLimitHi:1;		// pidout
	BYTE	InputRange:1;		// pidin
	BYTE    SpareLimits:4;		// spare
	BYTE	BangBang:1;		// pidmain
	//==
	WORD	ActConstP;		// pidmain
	WORD	ActConstI;		// pidmain
	WORD	ActConstD;		// pidmain
	WORD	ActCLimit;		// pidmain
	WORD	ActHLimit;		// pidmain
	WORD	ActFilter;		// pidmain
	//==
	LONG	Input;			// pidin
	LONG	RangeLo;		// pidin
	LONG	RangeHi;		// pidin
	float	SP;			// pidmain
	float	PV;			// pidin     
	float	DeltaPV;		// pidin
	float	DeltaT;			// pidin
	float	Error;			// pidmain
	float	Output;			// pidmain
	float	IntSum;			// pidmain
	//==
	WORD	HeatPower;		// pidout
	WORD	CoolPower;		// pidout
	WORD    ColdJunc;		// input
	WORD	HCMValue;		// input
	//==
	BYTE	ActSegment;		// pidmain
	WORD	ProfLast;		// pidmain
	LONG	ProfTimer;		// pidmain
	WORD	SegRemain;		// pidmain
	//==
	BYTE	Running;		// main
	
	} STATUS_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Input Data	- Used in Input.C
//

typedef struct tagInput_DLC
{
	// Conversion State
	BYTE	m_bInit;
	BYTE	m_bState;
	BYTE	m_InputType;
	BYTE	m_fReqHCM;		
	BYTE	m_fAckHCM;		
	BYTE	m_fReqCalib;	
	BYTE	m_fAckCalib;		

	// Calibration Data
	BYTE	m_bCalCode;
	BYTE	m_bCalCount;
	WORD	m_wCalData; 
	LONG	m_lCalTotal;

	// Conversion Data
	WORD	m_wZero;
	INT	m_ColdJunc;
	LONG 	m_RefVolt;
	LONG	m_Excite;
	LONG	m_Signal;
	LONG	m_Input;
	WORD	m_Resist;

	// Input Handshaking
	BYTE	m_fNewData;
	WORD	m_wThisTime;
	WORD	m_wLastTime;
	
	} INPUT_DLC;

//////////////////////////////////////////////////////////////////////////
//
// Integral Sum
//

typedef struct tagIntSum_DLC
{
	float	IntSum;

	} INTSUM_DLC;
	
//////////////////////////////////////////////////////////////////////////
//
// Profile Context
//

typedef struct tagProfCtx_DLC
{
	BYTE	AckProfile:1;
	BYTE	ProfDone:1;
	BYTE	AckSpare:6;
	//==
	BYTE	ActSegment;
	WORD	ProfLast;
	LONG	ProfTimer;

	} PROFCTX_DLC;
	
// End of File

#endif
