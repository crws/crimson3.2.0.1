
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - 8 Analog Input Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_8INDBASE_H

#define	INCLUDE_8INDBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Out of Range Value
//

#define	UPSCALE		((LONG) 0x7000000)
#define	FULLSCALE	((LONG) 300000)

//////////////////////////////////////////////////////////////////////////
//
// Process Current and Process Volt Input Ranges
//

#define	RANGE_020MA	1
#define	RANGE_420MA	2

#define	RANGE_010V	1
#define	RANGE_PM10V	2

//////////////////////////////////////////////////////////////////////////
//
// Model Numbers
//

#define	TC8		1
#define	INI8		2
#define	INV8		3

//////////////////////////////////////////////////////////////////////////
//
// Thermocouple Types
// 
	
#define	TC_TYPEB	1
#define TC_TYPEC	2	
#define TC_TYPEE 	3
#define	TC_TYPEJ	4
#define TC_TYPEK	5	
#define TC_TYPEN	6
#define	TC_TYPER	7	
#define TC_TYPES	8
#define	TC_TYPET	9	
#define TC_TYPELIN	10

//////////////////////////////////////////////////////////////////////////
//
// Module Hardware Constants
// 
	
#define	HDW_CHANNELS	8

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalibTC8
{
	WORD	InputTN10[HDW_CHANNELS];
	WORD	InputTP56[HDW_CHANNELS];
	WORD	InputVN10[HDW_CHANNELS];
	WORD	InputVP10[HDW_CHANNELS];
	WORD	InputI0[HDW_CHANNELS];
	WORD	InputI4[HDW_CHANNELS];
	WORD	InputI20[HDW_CHANNELS];
	WORD	CJTemp; 
	WORD	CJVolt;
	WORD	CJSlope;

	} CALIB8IN;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigTC8
{
	BYTE	ChanEnable[HDW_CHANNELS];
	WORD	InputFilter;
	BYTE	InputType[HDW_CHANNELS];
	WORD	InputSlope[HDW_CHANNELS];
	INT	InputOffset[HDW_CHANNELS];
	UINT	TempUnits;
	INT	DisplayLo[HDW_CHANNELS];
	INT	DisplayHi[HDW_CHANNELS];
	INT	SignalLo[HDW_CHANNELS];
	INT	SignalHi[HDW_CHANNELS];
	BYTE	PIRange;
	BYTE	PVRange;
	INT	ProcessMin[HDW_CHANNELS];
	INT	ProcessMax[HDW_CHANNELS];
	BYTE	SquareRt[HDW_CHANNELS];
	BYTE	ExtendPV[HDW_CHANNELS];
	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIG8IN;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusTC8
{
	BYTE	InputAlarm[HDW_CHANNELS];
	LONG	Input[HDW_CHANNELS];
	LONG	RangeLo[HDW_CHANNELS];
	LONG	RangeHi[HDW_CHANNELS];
	float	PV[HDW_CHANNELS];
	float	DeltaT[HDW_CHANNELS];
	WORD    ColdJunc;
	BYTE	Running;

	} STATUS8IN;
	
// End of File

#endif
