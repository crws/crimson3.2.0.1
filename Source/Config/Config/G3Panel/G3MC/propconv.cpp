
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Property Converter
//

// Constructor

CPropConverter::CPropConverter(void)
{
	m_Root.m_pLink = NULL;

	m_Root.m_fList = FALSE;
	}

// Destructor

CPropConverter::~CPropConverter(void)
{
	}

// Operations

BOOL CPropConverter::LoadFromItem(CMetaItem const *pItem)
{
	if( pItem ) {

		CPropValue *pRoot = &m_Root;

		CMetaList const *pList = pItem->FindMetaList();

		UINT c = pList->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CMetaData const *pMeta = pList->FindData(n);

			if( pMeta ) {

				if( LoadProp(pRoot, pMeta, PVOID(pItem)) ) {
				
					continue;
					}
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CPropConverter::LoadProp(CPropValue *pRoot, CMetaData const *pMeta, PVOID pItem)
{
	CString Name = pMeta->GetTag();

	if( pMeta->GetType() == metaObject ) {

		CPropValue *pValue = New CPropValue;

		pValue->m_pLink    = pRoot;

		pValue->m_fList    = FALSE;

		if( pRoot->m_Props.Insert(Name, pValue) ) {

			pRoot = pValue;

			return LoadObject(pRoot, (CMetaItem *) pMeta->GetObject(pItem));
			}		
		}

	if( pMeta->GetType() == metaCollect ) {

		return LoadCollect(pRoot, (CMetaItem *) pItem);
		}

	if( pMeta->GetType() == metaVirtual ) {
		
		return LoadVirtual(pRoot, (CMetaItem *) pItem);
		}

	if( pMeta->GetType() == metaInteger ) {

		CPropValue *pValue = New CPropValue;

		pValue->m_pLink    = pRoot;

		pValue->m_fList    = FALSE;

		pValue->m_Value    = ReadInteger(pMeta, pItem);

		return pRoot->m_Props.Insert(Name, pValue);
		}

	if( pMeta->GetType() == metaString ) {

		CPropValue *pValue = New CPropValue;

		pValue->m_pLink    = pRoot;

		pValue->m_fList    = FALSE;

		pValue->m_Value    = ReadString(pMeta, pItem);

		return pRoot->m_Props.Insert(Name, pValue);
		}

	if( pMeta->GetType() == metaGUID ) {
		
		return TRUE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CPropConverter::LoadObject(CPropValue *pRoot, CMetaItem const *pItem)
{
	CMetaList const *pList = pItem->FindMetaList();

	UINT c = pList->GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		if( pMeta ) {

			if( LoadProp(pRoot, pMeta, PVOID(pItem)) ) {
					
				continue;
				}

			return FALSE;
			}
		}
	
	return TRUE;
	}

CString CPropConverter::ReadString(CMetaData const *pMeta, PVOID pItem)
{
	AfxAssert(pMeta->GetType() == metaString);

	CString  Data;

	Data += 0xA7;
	
	Data += pMeta->ReadString(pItem);

	return CPrintf(L"%s", Data);
	}

CString CPropConverter::ReadInteger(CMetaData const *pMeta, PVOID pItem)
{
	AfxAssert(pMeta->GetType() == metaInteger);

	UINT   uData = pMeta->ReadInteger(pItem);	

	return CPrintf(L"%d", uData);
	}

BOOL CPropConverter::LoadCollect(CPropValue *pRoot, CMetaItem const *pItem)
{
	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CPropConverter::LoadVirtual(CPropValue *pRoot, CMetaItem const *pItem)
{
	AfxAssert(FALSE);

	return FALSE;
	}

// End of File

