
#include "intern.hpp"

#include "DAMix4DigitalInput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4DigitalInput, CCommsItem);

// Property List

CCommsList const CDAMix4DigitalInput::m_CommsList[] = {

	{ 1, "Input1",	PROPID_INPUT1,	usageRead,	IDS_NAME_I1 },
	{ 1, "Input2",	PROPID_INPUT2,	usageRead,	IDS_NAME_I2 },
	{ 1, "Input3",	PROPID_INPUT3,	usageRead,	IDS_NAME_I3 },

};

// Constructor

CDAMix4DigitalInput::CDAMix4DigitalInput(void)
{
	m_Input1 = 0;
	m_Input2 = 0;
	m_Input3 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAMix4DigitalInput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Digital Inputs");
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDAMix4DigitalInput::AddMetaData(void)
{
	Meta_AddInteger(Input1);
	Meta_AddInteger(Input2);
	Meta_AddInteger(Input3);	

	CCommsItem::AddMetaData();
}


// End of File
