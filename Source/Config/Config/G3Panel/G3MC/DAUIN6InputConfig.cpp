
#include "intern.hpp"

#include "DAUIN6InputConfig.hpp"

#include "DAUIN6InputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dauin6props.h"

#include "import/manticore/dauin6dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 AI Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAUIN6InputConfig, CCommsItem);

// Property List

CCommsList const CDAUIN6InputConfig::m_CommsList[] = {

	{ 0, "Range1",		PROPID_RANGE1,		usageWriteInit,  IDS_NAME_ANRNG1 },
	{ 0, "Range2",		PROPID_RANGE2,		usageWriteInit,  IDS_NAME_ANRNG2 },
	{ 0, "Range3",		PROPID_RANGE3,		usageWriteInit,  IDS_NAME_ANRNG3 },
	{ 0, "Range4",		PROPID_RANGE4,		usageWriteInit,  IDS_NAME_ANRNG4 },
	{ 0, "Range5",		PROPID_RANGE5,		usageWriteInit,  IDS_NAME_ANRNG5 },
	{ 0, "Range6",		PROPID_RANGE6,		usageWriteInit,  IDS_NAME_ANRNG6 },

	{ 0, "Sample1",		PROPID_SAMPLE1,		usageWriteInit,  IDS_NAME_ANSMP1 },
	{ 0, "Sample2",		PROPID_SAMPLE2,		usageWriteInit,  IDS_NAME_ANSMP2 },
	{ 0, "Sample3",		PROPID_SAMPLE3,		usageWriteInit,  IDS_NAME_ANSMP3 },
	{ 0, "Sample4",		PROPID_SAMPLE4,		usageWriteInit,  IDS_NAME_ANSMP4 },
	{ 0, "Sample5",		PROPID_SAMPLE5,		usageWriteInit,  IDS_NAME_ANSMP5 },
	{ 0, "Sample6",		PROPID_SAMPLE6,		usageWriteInit,  IDS_NAME_ANSMP6 },

	{ 0, "TempUnits1",	PROPID_UNITS1,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits2",	PROPID_UNITS2,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits3",	PROPID_UNITS3,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits4",	PROPID_UNITS4,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits5",	PROPID_UNITS5,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits6",	PROPID_UNITS6,		usageWriteInit,	IDS_MODULE_UNITS },

	{ 1, "Offset1",		PROPID_OFFSET1,		usageWriteBoth,	IDS_NAME_IO1	 },
	{ 1, "Offset2",		PROPID_OFFSET2,		usageWriteBoth,	IDS_NAME_IO2	 },
	{ 1, "Offset3",		PROPID_OFFSET3,		usageWriteBoth,	IDS_NAME_IO3	 },
	{ 1, "Offset4",		PROPID_OFFSET4,		usageWriteBoth,	IDS_NAME_IO4	 },
	{ 1, "Offset5",		PROPID_OFFSET5,		usageWriteBoth,	IDS_NAME_IO5	 },
	{ 1, "Offset6",		PROPID_OFFSET6,		usageWriteBoth,	IDS_NAME_IO6	 },

	{ 1, "Slope1",		PROPID_TEMP_SLOPE1,	usageWriteBoth,	IDS_NAME_IS1	 },
	{ 1, "Slope2",		PROPID_TEMP_SLOPE2,	usageWriteBoth,	IDS_NAME_IS2	 },
	{ 1, "Slope3",		PROPID_TEMP_SLOPE3,	usageWriteBoth,	IDS_NAME_IS3	 },
	{ 1, "Slope4",		PROPID_TEMP_SLOPE4,	usageWriteBoth,	IDS_NAME_IS4	 },
	{ 1, "Slope5",		PROPID_TEMP_SLOPE5,	usageWriteBoth,	IDS_NAME_IS5	 },
	{ 1, "Slope6",		PROPID_TEMP_SLOPE6,	usageWriteBoth,	IDS_NAME_IS6	 },

	{ 1, "Filter1",		PROPID_IN_FILTER1,	usageWriteBoth,	IDS_NAME_IF1	 },
	{ 1, "Filter2",		PROPID_IN_FILTER2,	usageWriteBoth,	IDS_NAME_IF2	 },
	{ 1, "Filter3",		PROPID_IN_FILTER3,      usageWriteBoth,	IDS_NAME_IF3	 },
	{ 1, "Filter4",		PROPID_IN_FILTER4,	usageWriteBoth,	IDS_NAME_IF4	 },
	{ 1, "Filter5",		PROPID_IN_FILTER5,	usageWriteBoth,	IDS_NAME_IF5	 },
	{ 1, "Filter6",		PROPID_IN_FILTER6,	usageWriteBoth,	IDS_NAME_IF6	 },

	{ 0, "ProcMin1",	PROPID_MINIMUM1,	usageWriteInit,	IDS_NAME_PMI1	 },
	{ 0, "ProcMin2",	PROPID_MINIMUM2,	usageWriteInit,	IDS_NAME_PMI2	 },
	{ 0, "ProcMin3",	PROPID_MINIMUM3,	usageWriteInit,	IDS_NAME_PMI3	 },
	{ 0, "ProcMin4",	PROPID_MINIMUM4,	usageWriteInit,	IDS_NAME_PMI4	 },
	{ 0, "ProcMin5",	PROPID_MINIMUM5,	usageWriteInit,	IDS_NAME_PMI5	 },
	{ 0, "ProcMin6",	PROPID_MINIMUM6,	usageWriteInit,	IDS_NAME_PMI6	 },

	{ 0, "ProcMax1",	PROPID_MAXIMUM1,	usageWriteInit,	IDS_NAME_PMA1	 },
	{ 0, "ProcMax2",	PROPID_MAXIMUM2,	usageWriteInit,	IDS_NAME_PMA2	 },
	{ 0, "ProcMax3",	PROPID_MAXIMUM3,	usageWriteInit,	IDS_NAME_PMA3	 },
	{ 0, "ProcMax4",	PROPID_MAXIMUM4,	usageWriteInit,	IDS_NAME_PMA4	 },
	{ 0, "ProcMax5",	PROPID_MAXIMUM5,	usageWriteInit,	IDS_NAME_PMA5	 },
	{ 0, "ProcMax6",	PROPID_MAXIMUM6,	usageWriteInit,	IDS_NAME_PMA6	 },

	{ 0, "Root1",		PROPID_ROOT1,		usageWriteInit,	IDS_NAME_SQRT1	 },
	{ 0, "Root2",		PROPID_ROOT2,		usageWriteInit,	IDS_NAME_SQRT2	 },
	{ 0, "Root3",		PROPID_ROOT3,		usageWriteInit,	IDS_NAME_SQRT3	 },
	{ 0, "Root4",		PROPID_ROOT4,		usageWriteInit,	IDS_NAME_SQRT4	 },
	{ 0, "Root5",		PROPID_ROOT5,		usageWriteInit,	IDS_NAME_SQRT5	 },
	{ 0, "Root6",		PROPID_ROOT6,		usageWriteInit,	IDS_NAME_SQRT6	 },

};

// Constructor

CDAUIN6InputConfig::CDAUIN6InputConfig(void)
{
	m_Range1	= 3;
	m_Range2	= 3;
	m_Range3	= 3;
	m_Range4	= 3;
	m_Range5	= 3;
	m_Range6	= 3;

	m_Sample1	= 4;
	m_Sample2	= 4;
	m_Sample3	= 4;
	m_Sample4	= 4;
	m_Sample5	= 4;
	m_Sample6	= 4;

	m_TempUnits1	= 2;
	m_TempUnits2	= 2;
	m_TempUnits3	= 2;
	m_TempUnits4	= 2;
	m_TempUnits5	= 2;
	m_TempUnits6	= 2;

	m_Offset1	= 0;
	m_Offset2	= 0;
	m_Offset3	= 0;
	m_Offset4	= 0;
	m_Offset5	= 0;
	m_Offset6	= 0;

	m_Slope1	= 1000;
	m_Slope2	= 1000;
	m_Slope3	= 1000;
	m_Slope4	= 1000;
	m_Slope5	= 1000;
	m_Slope6	= 1000;

	m_ProcMin1	= 0;
	m_ProcMin2	= 0;
	m_ProcMin3	= 0;
	m_ProcMin4	= 0;
	m_ProcMin5	= 0;
	m_ProcMin6	= 0;

	m_ProcMax1	= 10000;
	m_ProcMax2	= 10000;
	m_ProcMax3	= 10000;
	m_ProcMax4	= 10000;
	m_ProcMax5	= 10000;
	m_ProcMax6	= 10000;

	m_Root1   	= 0;
	m_Root2   	= 0;
	m_Root3   	= 0;
	m_Root4   	= 0;
	m_Root5   	= 0;
	m_Root6   	= 0;

	m_Filter1	= 20;
	m_Filter2	= 20;
	m_Filter3	= 20;
	m_Filter4	= 20;
	m_Filter5	= 20;
	m_Filter6	= 20;

	m_ProcDP1	= 2;
	m_ProcDP2	= 2;
	m_ProcDP3	= 2;
	m_ProcDP4	= 2;
	m_ProcDP5	= 2;
	m_ProcDP6	= 2;

	m_ProcUnits1	= "%";
	m_ProcUnits2	= "%";
	m_ProcUnits3	= "%";
	m_ProcUnits4	= "%";
	m_ProcUnits5	= "%";
	m_ProcUnits6	= "%";

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAUIN6InputConfig::GetPageCount(void)
{
	return 6;
}

CString CDAUIN6InputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			return CPrintf(IDS("Input %d"), n + 1);
	}

	return L"";
}

CViewWnd * CDAUIN6InputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			return New CDAUIN6InputConfigWnd(n);
	}

	return NULL;
}

// Group Names

CString CDAUIN6InputConfig::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1: return L"Control";
	}

	return CCommsItem::GetGroupName(Group);
}

// Conversion

BOOL CDAUIN6InputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 6; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Range%d", n+1), CPrintf(L"InputType%d", n+1)) ) {

				if( ImportNumber(pValue, CPrintf(L"TCType%d", n+1), CPrintf(L"InputTC%d", n+1)) ) {

					ConvertRange(n);
				}
			}

			if( ImportNumber(pValue, CPrintf(L"TempUnits%d", n+1)) ) {

				ConvertTempUnits(n);
			}

			ImportNumber(pValue, CPrintf(L"Offset%d", n+1), CPrintf(L"InputOffset%d", n+1));

			ImportNumber(pValue, CPrintf(L"Filter%d", n+1), CPrintf(L"InputFilter%d", n+1));

			ImportNumber(pValue, CPrintf(L"Slope%d", n+1), CPrintf(L"InputSlope%d", n+1));

			ImportNumber(pValue, CPrintf(L"Root%d", n+1), CPrintf(L"SquareRoot%d", n+1));

			ImportNumber(pValue, CPrintf(L"ProcMin%d", n+1));

			ImportNumber(pValue, CPrintf(L"ProcMax%d", n+1));

			ImportNumber(pValue, CPrintf(L"ProcDP%d", n+1));

			ImportString(pValue, CPrintf(L"ProcUnits%d", n+1));
		}

		return TRUE;
	}

	return FALSE;
}

// Persistence

void CDAUIN6InputConfig::Init(void)
{
	CCommsItem::Init();

	for( UINT n = 0; n < elements(m_TCType); n++ ) {

		m_TCType[n] = 0;
	}
}

// Property Filters

BOOL CDAUIN6InputConfig::SaveProp(CString const &Tag) const
{
	if( Tag.StartsWith(L"TCType") ) {

		return FALSE;
	}

	return TRUE;
}

// Meta Data Creation

void CDAUIN6InputConfig::AddMetaData(void)
{
	Meta_AddInteger(Range1);
	Meta_AddInteger(Range2);
	Meta_AddInteger(Range3);
	Meta_AddInteger(Range4);
	Meta_AddInteger(Range5);
	Meta_AddInteger(Range6);

	for( UINT n = 0; n < elements(m_TCType); n++ ) {

		UINT uOffset = (PBYTE(m_TCType + n) - PBYTE(this));

		AddMeta(CPrintf("TCType%u", 1 + n), metaInteger, uOffset);
	}

	Meta_AddInteger(Sample1);
	Meta_AddInteger(Sample2);
	Meta_AddInteger(Sample3);
	Meta_AddInteger(Sample4);
	Meta_AddInteger(Sample5);
	Meta_AddInteger(Sample6);

	Meta_AddInteger(TempUnits1);
	Meta_AddInteger(TempUnits2);
	Meta_AddInteger(TempUnits3);
	Meta_AddInteger(TempUnits4);
	Meta_AddInteger(TempUnits5);
	Meta_AddInteger(TempUnits6);

	Meta_AddInteger(Offset1);
	Meta_AddInteger(Offset2);
	Meta_AddInteger(Offset3);
	Meta_AddInteger(Offset4);
	Meta_AddInteger(Offset5);
	Meta_AddInteger(Offset6);

	Meta_AddInteger(Slope1);
	Meta_AddInteger(Slope2);
	Meta_AddInteger(Slope3);
	Meta_AddInteger(Slope4);
	Meta_AddInteger(Slope5);
	Meta_AddInteger(Slope6);

	Meta_AddInteger(ProcMin1);
	Meta_AddInteger(ProcMin2);
	Meta_AddInteger(ProcMin3);
	Meta_AddInteger(ProcMin4);
	Meta_AddInteger(ProcMin5);
	Meta_AddInteger(ProcMin6);

	Meta_AddInteger(ProcMax1);
	Meta_AddInteger(ProcMax2);
	Meta_AddInteger(ProcMax3);
	Meta_AddInteger(ProcMax4);
	Meta_AddInteger(ProcMax5);
	Meta_AddInteger(ProcMax6);

	Meta_AddInteger(Root1);
	Meta_AddInteger(Root2);
	Meta_AddInteger(Root3);
	Meta_AddInteger(Root4);
	Meta_AddInteger(Root5);
	Meta_AddInteger(Root6);

	Meta_AddInteger(Filter1);
	Meta_AddInteger(Filter2);
	Meta_AddInteger(Filter3);
	Meta_AddInteger(Filter4);
	Meta_AddInteger(Filter5);
	Meta_AddInteger(Filter6);

	Meta_AddInteger(ProcDP1);
	Meta_AddInteger(ProcDP2);
	Meta_AddInteger(ProcDP3);
	Meta_AddInteger(ProcDP4);
	Meta_AddInteger(ProcDP5);
	Meta_AddInteger(ProcDP6);

	Meta_AddString(ProcUnits1);
	Meta_AddString(ProcUnits2);
	Meta_AddString(ProcUnits3);
	Meta_AddString(ProcUnits4);
	Meta_AddString(ProcUnits5);
	Meta_AddString(ProcUnits6);

	CCommsItem::AddMetaData();
}

// Implementation

void CDAUIN6InputConfig::ConvertRange(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Range%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT   uPrev = pData->ReadInteger(this);

		UINT uData[] = { 0, 5, 10, 7, 9, 21, 11 };

		switch( uPrev ) {

			case 5:
				// RTD

				pData->WriteInteger(this, uData[uPrev] + m_TCType[uIndex] - 10);

				break;

			case 6:
				// Thermocouple

				pData->WriteInteger(this, uData[uPrev] + m_TCType[uIndex] - 1);

				break;

			default:

				pData->WriteInteger(this, uData[uPrev]);

				break;
		}

		return;
	}

	AfxAssert(FALSE);
}

void CDAUIN6InputConfig::ConvertTempUnits(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"TempUnits%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT   uPrev = pData->ReadInteger(this);

		UINT uData[] = { 0, 0, 2, 1 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
