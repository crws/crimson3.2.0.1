
#include "intern.hpp"

#include "legacy.h"

#include "slcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- PID Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUIPIDProcess, CUIEditBox)

// Linked List

CUIPIDProcess * CUIPIDProcess::m_pHead = NULL;

CUIPIDProcess * CUIPIDProcess::m_pTail = NULL;

// Constructor

CUIPIDProcess::CUIPIDProcess(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Destructor

CUIPIDProcess::~CUIPIDProcess(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Update Support

void CUIPIDProcess::CheckUpdate(CPIDLoop *pLoop, CString const &Tag)
{
	if( Tag.Left(4) == "Proc" || Tag.Left(4) == "Temp" || Tag == "InputType" ) {

		BOOL fKeep = (Tag != "TempUnits");

		CUIPIDProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextPIDProcess *pText = (CUITextPIDProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'L' ) {

					if( Tag == "InputType" ) {

						pScan->Update(FALSE);
						}

					if( Tag == "ProcDP" ) {

						pScan->Update(TRUE);
						}

					if( Tag == "ProcUnits" ) {

						pScan->Update(TRUE);
						}
					}
				else
					pScan->Update(fKeep);
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag.Left(9) == "AlarmMode" ) {

		CUIPIDProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextPIDProcess *pText = (CUITextPIDProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'A' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag == "LinOutMap" ) {

		CUIPIDProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextPIDProcess *pText = (CUITextPIDProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'M' ) {

					pScan->Update(TRUE);
					}

				if( pText->m_cType == 'N' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag == "SetRampBase" ) {

		CUIPIDProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextPIDProcess *pText = (CUITextPIDProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'R' ) {

					pScan->UpdateUnits();
					}
				}

			pScan = pScan->m_pNext;
			}
		}
	}

// Operations

void CUIPIDProcess::Update(BOOL fKeep)
{
	CUITextPIDProcess *pText = (CUITextPIDProcess *) m_pText;

	pText->GetConfig();

	if( fKeep ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
		}
	else {
		INT nData = m_pData->ReadInteger(m_pItem);

		INT nCopy = nData;

		pText->Check(CError(FALSE), nData);

		if( nData != nCopy ) {

			m_pData->WriteInteger(m_pItem, UINT(nData));

			m_pItem->SetDirty();
			}
		
		LoadUI();
		}

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

void CUIPIDProcess::UpdateUnits(void)
{
	CUITextPIDProcess *pText = (CUITextPIDProcess *) m_pText;

	pText->GetConfig();

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- PID Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextPIDProcess, CUITextInteger)

// Constructor

CUITextPIDProcess::CUITextPIDProcess(void)
{
	}

// Destructor

CUITextPIDProcess::~CUITextPIDProcess(void)
{
	}

// Core Overidables

void CUITextPIDProcess::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CPIDLoop)) ) {

		m_pLoop = (CPIDLoop *) m_pItem;
		}
	else {
		if( m_pItem->IsKindOf(AfxRuntimeClass(CSLCMapper)) ) {

			CSLCMapper *pMapper = (CSLCMapper *) m_pItem;

			CSLCModule *pModule = (CSLCModule *) pMapper->GetParent();

			m_pLoop = pModule->m_pLoop;
			}
		else
			AfxAssert(FALSE);
		}

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();	
	}

// Implementation

void CUITextPIDProcess::GetConfig(void)
{
	m_cType = char(m_UIData.m_Format[0]);

	if( m_cType == 'M' || m_cType == 'N' ) {

		if( IsOutputPercent() ) {

			m_uPlaces = 2;
			
			m_Units   = "%";

			m_nMin    = 0;
			
			m_nMax    = 20000;

			CheckFlags();

			return;
			}
		}

	if( m_pLoop->m_InputType < INPUT_RTD ) {

		m_uPlaces = m_pLoop->m_ProcDP;

		m_nMin    = Min(m_pLoop->m_ProcMin, m_pLoop->m_ProcMax);

		m_nMax    = Max(m_pLoop->m_ProcMin, m_pLoop->m_ProcMax);

		m_Units   = m_pLoop->m_ProcUnits;
		}
	else {
		switch( m_pLoop->m_TempUnits ) {

			case 0:
				m_Units = "K";
				m_t1    = 0;
				m_t2    = 10000;
				break;

			case 1:
				m_Units = CPrintf("%cF", 176);
				m_t1    = -4597;
				m_t2    = 13403;
				break;

			case 2:
				m_Units = CPrintf("%cC", 176);
				m_t1    = -2732;
				m_t2    =  7268;
				break;
			}

		m_nMin    = StoreToDisp(0);

		m_nMax    = StoreToDisp(600000);

		m_uPlaces = 1;
		}

	CheckType();

	CheckFlags();
	}

void CUITextPIDProcess::CheckType(void)
{
	BOOL fDelta = FALSE;

	INT  nRange = 0;

	INT  nLimit = 0;

	switch( m_cType ) {

		case 'L':

			m_nMin = -30000;

			m_nMax = +30000;

			break;

		case 'R':
		case 'H':

			if( m_cType == 'R' ) {

				CString Rate = GetRateUnits();

				m_Units += Rate;
				}

			nRange = abs(m_nMax - m_nMin);

			nLimit = StoreToDisp(300000);

			MakeMin(nRange, nLimit);

			m_nMin = 0;

			m_nMax = nRange;

			break;

		case 'D':
		case 'N':

			fDelta = TRUE;

			break;

		case 'O':

			m_nMin = -1000;

			m_nMax = +1000;

			fDelta = TRUE;

			break;

		case 'A':

			fDelta = !IsAlarmAbsolute();

			break;

		case 'M':

			fDelta = !IsOutputAbsolute();

			break;
		}

	if( fDelta ) {

		nRange = abs(m_nMax - m_nMin);

		nLimit = StoreToDisp(300000);

		MakeMin(nRange, nLimit);

		m_nMin = -nRange;

		m_nMax = +nRange;
		}
	}

void CUITextPIDProcess::CheckFlags(void)
{
	if( m_uPlaces ) {
		
		m_uFlags |= textPlaces;
		}
	else 
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
		}
	else 
		m_uFlags &= ~textSigned;
	}

// Scaling

INT CUITextPIDProcess::StoreToDisp(INT nData)
{
	if( m_cType != 'L' ) {

		double a, b, c;

		GetConstants(a, b, c);

		BOOL fDelta = FALSE;

		switch( m_cType ) {

			case 'R':
			case 'D':
			case 'H':
			case 'N':
			case 'O':
				fDelta = TRUE;
				break;

			case 'A':
				fDelta = !IsAlarmAbsolute();
				break;

			case 'M':
				fDelta = !IsOutputAbsolute();
				break;
			}

		if( fDelta ) {

			return Scale(nData, b - a, c, 0);
			}

		return Scale(nData, b - a, c, a);
		}

	return nData;
	}

INT CUITextPIDProcess::DispToStore(INT nData)
{
	if( m_cType != 'L' ) {

		double a, b, c;

		GetConstants(a, b, c);

		BOOL fDelta = FALSE;

		switch( m_cType ) {

			case 'R':
			case 'D':
			case 'H':
			case 'N':
			case 'O':
				fDelta = TRUE;
				break;

			case 'A':
				fDelta = !IsAlarmAbsolute();
				break;

			case 'M':
				fDelta = !IsOutputAbsolute();
				break;
			}

		if( fDelta ) {

			return Scale(nData, c, b - a, 0);
			}

		return Scale(nData - a, c, b - a, 0);
		}

	return nData;
	}

// Implementation

INT CUITextPIDProcess::Scale(double a, double b, double c, double d)
{
	double e = (a * b / c) + d;

	if( e < 0 ) e -= 0.5;

	if( e > 0 ) e += 0.5;

	return INT(e);
	}

void CUITextPIDProcess::GetConstants(double &a, double &b, double &c)
{
	if( m_cType == 'M' || m_cType == 'N' ) {

		if( IsOutputPercent() ) {

			a = 0;

			b = 10000;

			c = 100000;

			return;
			}
		}
			
	if( m_pLoop->m_InputType < INPUT_RTD ) {

		a = m_pLoop->m_ProcMin;

		b = m_pLoop->m_ProcMax;

		c = 300000;
		}
	else {
		a = m_t1;

		b = m_t2;

		c = 200000;
		}
	}

BOOL CUITextPIDProcess::IsAlarmAbsolute(void)
{
	UINT uAlarm = watoi(m_UIData.m_Tag.Right(1));

	UINT uMode  = 0;

	switch( uAlarm ) {

		case 1: uMode = m_pLoop->m_AlarmMode1; break;
		case 2: uMode = m_pLoop->m_AlarmMode2; break;
		case 3: uMode = m_pLoop->m_AlarmMode3; break;
		case 4: uMode = m_pLoop->m_AlarmMode4; break;

		}

	return uMode == ALARM_ABS_LO || uMode == ALARM_ABS_HI;
	}

BOOL CUITextPIDProcess::IsOutputAbsolute(void)
{
	CSLCMapper *pMapper = (CSLCMapper *) m_pItem;

	switch( pMapper->m_LinOutMap ) {
		
		case ANL_ERROR:

			return FALSE;
		}

	return TRUE;
	}

BOOL CUITextPIDProcess::IsOutputPercent(void)
{
	CSLCMapper *pMapper = (CSLCMapper *) m_pItem;

	switch( pMapper->m_LinOutMap ) {
		
		case ANL_NULL:
		case ANL_COOL:
		case ANL_HEAT:
		case ANL_REMOTE1:
		case ANL_REMOTE2:
		case ANL_REMOTE3:
		case ANL_REMOTE4:

			return TRUE;
		}

	return FALSE;
	}

CString CUITextPIDProcess::GetRateUnits(void)
{
	switch(  m_pLoop->m_SetRampBase ) {

		case 0:
			return CString(IDS_PER_UNIT);

		case 1:
			return CString(IDS_PER_SEC);

		case 2:
			return CString(IDS_PER_MIN);

		case 3:
			return CString(IDS_PER_HR);
		}

	return L"";
	}

// End of File
