
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GMUIN4_HPP

#define INCLUDE_GMUIN4_HPP

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration
//

class CGraphiteUIN4Config : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteUIN4Config(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Item Properties
		UINT m_InputType1;
		UINT m_InputType2;
		UINT m_InputType3;
		UINT m_InputType4;

		UINT m_InputTC1;
		UINT m_InputTC2;
		UINT m_InputTC3;
		UINT m_InputTC4;

		UINT m_TempUnits1;
		UINT m_TempUnits2;
		UINT m_TempUnits3;
		UINT m_TempUnits4;

		UINT m_InputOffset1;
		UINT m_InputOffset2;
		UINT m_InputOffset3;
		UINT m_InputOffset4;

		UINT m_InputSlope1;
		UINT m_InputSlope2;
		UINT m_InputSlope3;
		UINT m_InputSlope4;

		UINT m_InputFilter1;
		UINT m_InputFilter2;
		UINT m_InputFilter3;
		UINT m_InputFilter4;

		UINT m_SquareRoot1;
		UINT m_SquareRoot2;
		UINT m_SquareRoot3;
		UINT m_SquareRoot4;

		UINT m_PV1;
		UINT m_PV2;
		UINT m_PV3;
		UINT m_PV4;

		UINT m_InputAlarm1;
		UINT m_InputAlarm2;
		UINT m_InputAlarm3;
		UINT m_InputAlarm4;

		UINT m_ColdJunc1;
		UINT m_ColdJunc2;
		UINT m_ColdJunc3;
		UINT m_ColdJunc4;

		CString m_ProcUnits1;
		CString m_ProcUnits2;
		CString m_ProcUnits3;
		CString m_ProcUnits4;

		UINT m_ProcMin1;
		UINT m_ProcMin2;
		UINT m_ProcMin3;
		UINT m_ProcMin4;

		UINT m_ProcMax1;
		UINT m_ProcMax2;
		UINT m_ProcMax3;
		UINT m_ProcMax4;

		UINT m_ProcDP1;
		UINT m_ProcDP2;
		UINT m_ProcDP3;
		UINT m_ProcDP4;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module
//

class CGMUIN4Module : public CGraphiteGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMUIN4Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Item Properties
		CGraphiteUIN4Config * m_pConfig;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module Window
//

class CGraphiteUIN4MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteUIN4MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd   * m_pMult;
		CGMUIN4Module   * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddPages(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration View
//

class CGraphiteUIN4ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteUIN4ConfigWnd(UINT uPage);

	protected:
		// Data Members
		CGraphiteUIN4Config   * m_pItem;
		UINT			m_uPage;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// Implementation
		void AddOperation(void);
		void AddUnits(void);

		// Enabling
		void DoEnables(UINT uIndex);
		void EnableTC(UINT uIndex);

		//
		UINT GetInteger(CString Tag);
		void PutInteger(CString Tag, UINT Data);
	};

// End of File

#endif
