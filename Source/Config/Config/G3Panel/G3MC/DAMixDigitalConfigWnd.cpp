
#include "intern.hpp"

#include "DAMixDigitalConfigWnd.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mix Module Digital Configuration Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAMixDigitalConfigWnd, CUIViewWnd);

// Overibables

void CDAMixDigitalConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMixDigitalConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("damix_dig"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMixDigitalConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddConfig();

	EndPage(FALSE);
}

// Implementation

void CDAMixDigitalConfigWnd::AddConfig(void)
{
	StartTable(IDS("Options"), 1);

	AddColHead(IDS("Channel Mode"));

	for( UINT n = 0; n < m_pItem->m_uChans; n++ ) {

		AddRowHead(CPrintf(IDS("Channel %u"), n+1));

		AddUI(m_pItem, L"root", CPrintf("ChanMode%d", n+1));
	}

	EndTable();
}

// End of File
