
#include "intern.hpp"

#include "NewRackNavTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "NewCommsDeviceRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Backplane Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CNewRackNavTreeWnd, CNavTreeWnd);

// Constructor

CNewRackNavTreeWnd::CNewRackNavTreeWnd(void) : CNavTreeWnd(L"Modules", NULL, AfxRuntimeClass(CNewCommsDeviceRack))
{
}

// Overridables

void CNewRackNavTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	CCommsPortRack *pPort = (CCommsPortRack *) m_pItem;

	m_pSystem = (CCommsSystem  *) m_pItem->GetDatabase()->GetSystemItem();

	m_pList   = pPort->m_pDevices;
}

// Message Map

AfxMessageMap(CNewRackNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_ENDLABELEDIT, OnTreeEndEdit)

	AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)

	AfxMessageEnd(CNewRackNavTreeWnd)
};

// Message Handlers

void CNewRackNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"RackNavTreeTool"));
	}
}

// Notification Handlers

BOOL CNewRackNavTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
	}

	return FALSE;
}

// Command Handlers

BOOL CNewRackNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_ITEM_USAGE:

			Info.m_Image = MAKELONG(0x000C, 0x1000);

			return FALSE;
	}

	return FALSE;
}

BOOL CNewRackNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_ITEM_DELETE:

			Src.EnableItem(FALSE);

			return TRUE;

		case IDM_ITEM_USAGE:

			Src.EnableItem(!IsRootSelected());

			return TRUE;
	}

	return FALSE;
}

BOOL CNewRackNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_USAGE:

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

				m_pSystem->FindDeviceUsage((CCommsDevice *) m_pSelect);
			}

			return TRUE;
	}

	return FALSE;
}

BOOL CNewRackNavTreeWnd::OnEditControl(UINT uID, CCmdSource &Src) {

	switch( uID ) {

		case IDM_EDIT_DELETE:

			Src.EnableItem(FALSE);

			return TRUE;

		case IDM_EDIT_CUT:

			Src.EnableItem(FALSE);

			return TRUE;

		case IDM_EDIT_PASTE:

			Src.EnableItem(!IsReadOnly() && CanEditPaste() && m_pList->GetItemCount() <= 16);

			return TRUE;
	}

	return FALSE;
}

BOOL CNewRackNavTreeWnd::OnItemRename(CString Name)
{
	CString Prev = m_pNamed->GetName();

	if( wstrcmp(Name, Prev) ) {

		CItem *pItem = m_pSystem->m_pComms->FindDevice(Name);

		if( pItem ) {

			if( pItem != m_pSelect ) {

				CString Text = CFormat(CString(IDS_NAME_FMT_IS), Name);

				Error(Text);

				return FALSE;
			}
		}

		if( TRUE ) {

			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
			}
		}

		CCmd *pCmd = New CCmdRename(m_pNamed, Name);

		m_System.ExecCmd(pCmd);

		return TRUE;
	}

	return FALSE;
}

// Tree Loading

void CNewRackNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CommsTreeIcon16"), afxColor(MAGENTA));
}

// Item Hooks

BOOL CNewRackNavTreeWnd::IncludeItem(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CNewCommsDeviceRack)) ) {

		CNewCommsDeviceRack *pDev = (CNewCommsDeviceRack *) pItem;

		// TODO -- Should this be localized?!!!

		if( pDev->m_Name == L"Master" ) {

			return FALSE;
		}
	}

	return pItem->HasName() && !pItem->GetName().IsEmpty();
}

UINT CNewRackNavTreeWnd::GetRootImage(void)
{
	if( m_pSystem->GetModel().StartsWith(L"da") ) {

		return 0x4E;
		}
	else {
		return 0x45;
	}
}

UINT CNewRackNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	if( m_pSystem->GetModel().StartsWith(L"da") ) {

		return 0x4F;
	}
	else {
		return 0x46;
	}
}

BOOL CNewRackNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"RackNavTreeCtxMenu";

		return TRUE;
	}

	Name = L"RackNavTreeMissCtxMenu";

	return FALSE;
}

void CNewRackNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem ) {

		m_pSystem->Validate(TRUE);
	}

	CNavTreeWnd::OnItemRenamed(pItem);
}

void CNewRackNavTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
	if( pItem ) {

		if( pItem->IsKindOf(m_Class) ) {

			m_pSystem->Validate(TRUE);
		}
	}
}

void CNewRackNavTreeWnd::NewItemSelected(void)
{
	CNavTreeWnd::NewItemSelected();

	if( !m_fLocked ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CNewCommsDeviceRack)) ) {

			CNewCommsDeviceRack *pDev = (CNewCommsDeviceRack *) m_pSelect;

			afxThread->SetStatusText(pDev ? CPrintf(L"Device Number %d", pDev->m_Number) : L"");
		}
		else {
			afxThread->SetStatusText(L"");
		}
	}
}

// End of File
