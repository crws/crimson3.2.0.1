
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOInputConfig_HPP

#define INCLUDE_DADIDOInputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI Configuration
//

class CDADIDOInputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDADIDOInputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	UINT m_Type1;
	UINT m_Type2;
	UINT m_Type3;
	UINT m_Type4;
	UINT m_Type5;
	UINT m_Type6;
	UINT m_Type7;
	UINT m_Type8;

	UINT m_Mode1;
	UINT m_Mode2;
	UINT m_Mode3;
	UINT m_Mode4;
	UINT m_Mode5;
	UINT m_Mode6;
	UINT m_Mode7;
	UINT m_Mode8;

	UINT m_Filter1;
	UINT m_Filter2;
	UINT m_Filter3;
	UINT m_Filter4;
	UINT m_Filter5;
	UINT m_Filter6;
	UINT m_Filter7;
	UINT m_Filter8;

	UINT m_Timebase1;
	UINT m_Timebase2;
	UINT m_Timebase3;
	UINT m_Timebase4;
	UINT m_Timebase5;
	UINT m_Timebase6;
	UINT m_Timebase7;
	UINT m_Timebase8;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void ConvertMode(UINT uIndex);
};

// End of File

#endif
