
#include "intern.hpp"

#include "DAPID1ViewWnd.hpp"

#include "DAPID1Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAPID1ViewWnd, CUIViewWnd);

// Overibables

void CDAPID1ViewWnd::OnAttach(void)
{
	m_pItem   = (CDAPID1Module *) CViewWnd::m_pItem;

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAPID1ViewWnd::OnUICreate(void)
{
	StartPage(1);

	AddIdent();

	EndPage(FALSE);
}

// Implementation

void CDAPID1ViewWnd::AddIdent(void)
{
	StartGroup(IDS("Options"), 1, FALSE);

	CUIData Data;

	Data.m_Tag       = L"Ident";

	Data.m_Label     = IDS("Variant");

	Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

	Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");

	Data.m_Format    = IDS("39|Relay with Analog Output (RA)|41|Solid State with Analog Output (SA)");

	Data.m_Tip       = IDS("Select the Module Variant");

	AddUI(m_pItem, L"root", &Data);

	EndGroup(TRUE);
}

// End of File
