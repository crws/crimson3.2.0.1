
#include "intern.hpp"

#include "dadidooutput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOOutput, CCommsItem);

// Property List

CCommsList const CDADIDOOutput::m_CommsList[] = {

	{ 1, "Output1",  PROPID_OUTPUT1,  usageWriteUser,  IDS_NAME_O1 },
	{ 1, "Output2",  PROPID_OUTPUT2,  usageWriteUser,  IDS_NAME_O2 },
	{ 1, "Output3",  PROPID_OUTPUT3,  usageWriteUser,  IDS_NAME_O3 },
	{ 1, "Output4",  PROPID_OUTPUT4,  usageWriteUser,  IDS_NAME_O4 },
	{ 1, "Output5",  PROPID_OUTPUT5,  usageWriteUser,  IDS_NAME_O5 },
	{ 1, "Output6",  PROPID_OUTPUT6,  usageWriteUser,  IDS_NAME_O6 },
	{ 1, "Output7",  PROPID_OUTPUT7,  usageWriteUser,  IDS_NAME_O7 },
	{ 1, "Output8",  PROPID_OUTPUT8,  usageWriteUser,  IDS_NAME_O8 },

};

// Constructor

CDADIDOOutput::CDADIDOOutput(void)
{
	m_Output1 = 0;
	m_Output2 = 0;
	m_Output3 = 0;
	m_Output4 = 0;
	m_Output5 = 0;
	m_Output6 = 0;
	m_Output7 = 0;
	m_Output8 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDADIDOOutput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Outputs");
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDADIDOOutput::AddMetaData(void)
{
	Meta_AddInteger(Output1);
	Meta_AddInteger(Output2);
	Meta_AddInteger(Output3);
	Meta_AddInteger(Output4);
	Meta_AddInteger(Output5);
	Meta_AddInteger(Output6);
	Meta_AddInteger(Output7);
	Meta_AddInteger(Output8);

	CCommsItem::AddMetaData();
}

// End of File
