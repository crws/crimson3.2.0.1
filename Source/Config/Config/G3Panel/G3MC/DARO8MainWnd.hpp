
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DARO8MainWnd_HPP

#define INCLUDE_DARO8MainWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDARO8Module;

//////////////////////////////////////////////////////////////////////////
//
// DARO8 Module Window
//

class CDARO8MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDARO8MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd * m_pMult;
		CDARO8Module * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddDOPages(void);
	};

// End of File

#endif
