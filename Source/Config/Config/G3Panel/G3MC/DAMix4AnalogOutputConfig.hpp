
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4AnalogOutputConfig_HPP

#define INCLUDE_DAMix4AnalogOutputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output Configuration
//

class CDAMix4AnalogOutputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix4AnalogOutputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Group Names
	CString GetGroupName(WORD Group);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	UINT	m_Type1;
	UINT	m_Type2;

	UINT	m_DP1;
	UINT	m_DP2;

	INT	m_DataLo1;
	INT	m_DataLo2;

	INT	m_DataHi1;
	INT	m_DataHi2;

	INT	m_OutputLo1;
	INT	m_OutputLo2;

	INT	m_OutputHi1;
	INT	m_OutputHi2;

	INT	m_DataInit1;
	INT	m_DataInit2;

	UINT	m_InitData;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Data Members
	UINT m_uChans;

	// Property Filter
	BOOL IncludeProp(WORD PropID);

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void ConvertType(UINT uIndex);
};

// End of File

#endif
