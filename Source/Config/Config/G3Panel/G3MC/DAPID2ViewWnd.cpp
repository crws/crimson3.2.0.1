
#include "intern.hpp"

#include "DAPID2ViewWnd.hpp"

#include "DAPID2Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAPID2ViewWnd, CUIViewWnd);

// Overibables

void CDAPID2ViewWnd::OnAttach(void)
{
	m_pItem   = (CDAPID2Module *) CViewWnd::m_pItem;

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAPID2ViewWnd::OnUICreate(void)
{
	StartPage(1);

	AddIdent();

	EndPage(FALSE);
}

// Implementation

void CDAPID2ViewWnd::AddIdent(void)
{
	StartGroup(IDS("Options"), 1, FALSE);

	CUIData Data;

	Data.m_Tag       = L"Ident";

	Data.m_Label     = IDS("Variant");

	Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

	Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");

	Data.m_Format    = IDS("40|Solid State Output with Heater Current Monitor (SM)|42|Relay Output without Heater Current Monitor (RO)|43|Solid State Output without Heater Current Monitor (SO)");

	Data.m_Tip       = IDS("Select the Module Variant");

	AddUI(m_pItem, L"root", &Data);

	EndGroup(TRUE);
}

// End of File
