
#include "intern.hpp"

#include "legacy.h"

#include "pidloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Item
//

// Runtime Class

AfxImplementRuntimeClass(CPIDLoop, CCommsItem);

// Property List

CCommsList const CPIDLoop::m_CommsList[] = {

	{ 1, "PV",		PROPID_PV,			usageRead,	IDS_NAME_PV	},
	{ 1, "Output",		PROPID_OUTPUT,			usageRead,	IDS_NAME_OUT	},
	{ 1, "HeatPower",	PROPID_HEAT_POWER,		usageRead,	IDS_NAME_HPWR	},
	{ 1, "CoolPower",	PROPID_COOL_POWER,		usageRead,	IDS_NAME_CPWR	},
	{ 1, "ActSP",		PROPID_ACT_SP,			usageRead,	IDS_NAME_SP	},
	{ 1, "Error",		PROPID_ERROR,			usageRead,	IDS_NAME_ERR	},
	{ 1, "ColdJunc",	PROPID_COLD_JUNC,		usageRead,	IDS_NAME_CJ	},
	{ 1, "HCMValue",	PROPID_HCM_VALUE,		usageRead,	IDS_NAME_HCM	},
	{ 1, "AckManual",	PROPID_ACK_MANUAL,		usageRead,	IDS_NAME_AMAN	},
	{ 1, "AckTune",		PROPID_ACK_TUNE,		usageRead,	IDS_NAME_ATUNE	},
	{ 1, "TuneDone",	PROPID_TUNE_DONE,		usageRead,	IDS_NAME_TDONE	},
	{ 1, "TuneFail",	PROPID_TUNE_FAIL,		usageRead,	IDS_NAME_TFAIL  },
	{ 1, "Alarm1",		PROPID_ALARM_1,			usageRead,	IDS_NAME_A1	},
	{ 1, "Alarm2",		PROPID_ALARM_2,			usageRead,	IDS_NAME_A2	},
	{ 1, "Alarm3",		PROPID_ALARM_3,			usageRead,	IDS_NAME_A3	},
	{ 1, "Alarm4",		PROPID_ALARM_4,			usageRead,	IDS_NAME_A4	},
	{ 1, "HCMAlarmLo",	PROPID_HCM_ALARM_LO,		usageRead,	IDS_NAME_LHCM	},
	{ 1, "HCMAlarmHi",	PROPID_HCM_ALARM_HI,		usageRead,	IDS_NAME_HHCM	},
	{ 1, "InputAlarm",	PROPID_RANGE_ALARM,		usageRead,	IDS_NAME_IA	},
	{ 1, "AckProfile",	PROPID_ACK_PROFILE,		usageRead,	IDS_NAME_ACKPR	},
	{ 1, "AckHold",		PROPID_ACK_HOLD,		usageRead,	IDS_NAME_ACKHD	},
	{ 1, "AutoHold",	PROPID_AUTO_HOLD,		usageRead,	IDS_NAME_AUTOH	},
	{ 1, "ProfDone",	PROPID_PROF_DONE,		usageRead,	IDS_NAME_PROFD	},
	{ 1, "ActSegment",	PROPID_ACT_SEGMENT,		usageRead,	IDS_NAME_ASEG	},
	{ 1, "SegRemain",	PROPID_SEG_REMAIN,		usageRead,	IDS_NAME_SEGR	},

	{ 2, "ReqSP",		PROPID_REQ_SP,			usageWriteBoth,	IDS_NAME_RSP	},
	{ 2, "AltSP",		PROPID_ALT_SP,			usageWriteUser,	IDS_NAME_ASP	},
	{ 2, "AltPV",		PROPID_ALT_PV,			usageWriteUser,	IDS_NAME_APV	},
	{ 2, "Power",		PROPID_POWER,			usageWriteBoth,	IDS_NAME_PWR	},
	{ 2, "SetHyst",		PROPID_SET_HYST,		usageWriteBoth,	IDS_NAME_SHYST	},
	{ 2, "SetDead",		PROPID_SET_DEAD,		usageWriteBoth,	IDS_NAME_SDEAD	},
	{ 2, "SetRamp",		PROPID_SET_RAMP,		usageWriteBoth,	IDS_NAME_SRAMP	},
	{ 2, "SetRampBase",	PROPID_SET_RAMP_BASE,		usageWriteInit,	IDS_NAME_SRBASE	},
	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},
	{ 2, "InputOffset",	PROPID_INPUT_OFFSET,		usageWriteBoth,	IDS_NAME_IO	},
	{ 2, "InputSlope",	PROPID_INPUT_SLOPE,		usageWriteBoth,	IDS_NAME_IS	},
	{ 2, "ReqManual",	PROPID_REQ_MANUAL,		usageWriteBoth,	IDS_NAME_RMAN	},
	{ 2, "ReqTune",		PROPID_REQ_TUNE,		usageWriteBoth,	IDS_NAME_RTUNE	},
	{ 2, "ReqUserPID",	PROPID_REQ_USER_PID,		usageWriteBoth,	IDS_NAME_RUSER	},
	{ 2, "ReqProfile",	PROPID_REQ_PROFILE,		usageWriteUser,	IDS_NAME_REQPR	},
	{ 2, "ReqHold",		PROPID_REQ_HOLD,		usageWriteUser, IDS_NAME_REQHD  },
	{ 2, "ReqAltSP",	PROPID_REQ_ALTSP,		usageWriteUser, IDS_NAME_RASP	},
	{ 2, "ReqAltPV",	PROPID_REQ_ALTPV,		usageWriteUser, IDS_NAME_RAPV	},
	{ 2, "ReqSegment",	PROPID_REQ_SEGMENT,		usageWriteUser, IDS_NAME_RSEG	},
	{ 2, "EndSegment",	PROPID_END_SEGMENT,		usageWriteUser, IDS_NAME_ESEG	},
	{ 2, "ProfError",	PROPID_PROF_ERROR,		usageWriteUser, IDS_NAME_PERR	},
	{ 2, "ProfHyst",	PROPID_PROF_HYST,		usageWriteUser, IDS_NAME_PHYST	},

	{ 4, "AlarmData1",	PROPID_ALARM_DATA_1,		usageWriteBoth,	IDS_NAME_AD1	},
	{ 4, "AlarmData2",	PROPID_ALARM_DATA_2,		usageWriteBoth,	IDS_NAME_AD2	},
	{ 4, "AlarmData3",	PROPID_ALARM_DATA_3,		usageWriteBoth,	IDS_NAME_AD3	},
	{ 4, "AlarmData4",	PROPID_ALARM_DATA_4,		usageWriteBoth,	IDS_NAME_AD4	},
	{ 4, "AlarmHyst1",	PROPID_ALARM_HYST_1,		usageWriteBoth,	IDS_NAME_AH1	},
	{ 4, "AlarmHyst2",	PROPID_ALARM_HYST_2,		usageWriteBoth,	IDS_NAME_AH2	},
	{ 4, "AlarmHyst3",	PROPID_ALARM_HYST_3,		usageWriteBoth,	IDS_NAME_AH3	},
	{ 4, "AlarmHyst4",	PROPID_ALARM_HYST_4,		usageWriteBoth,	IDS_NAME_AH4	},
	{ 4, "AlarmAccept1",	PROPID_ALARM_ACCEPT_1,		usageWriteBoth,	IDS_NAME_AA1	},
	{ 4, "AlarmAccept2",	PROPID_ALARM_ACCEPT_2,		usageWriteBoth,	IDS_NAME_AA2	},
	{ 4, "AlarmAccept3",	PROPID_ALARM_ACCEPT_3,		usageWriteBoth,	IDS_NAME_AA3	},
	{ 4, "AlarmAccept4",	PROPID_ALARM_ACCEPT_4,		usageWriteBoth,	IDS_NAME_AA4	},
	{ 4, "HCMLimitLo",	PROPID_HCM_LIMIT_LO,		usageWriteBoth,	IDS_NAME_HLLO	},
	{ 4, "HCMLimitHi",	PROPID_HCM_LIMIT_HI,		usageWriteBoth,	IDS_NAME_HLHI	},
	{ 4, "HCMAcceptLo",	PROPID_HCM_ACCEPT_LO,		usageWriteBoth,	IDS_NAME_HALO	},
	{ 4, "HCMAcceptHi",	PROPID_HCM_ACCEPT_HI,		usageWriteBoth,	IDS_NAME_HAHI	},
	{ 4, "InputAccept",	PROPID_RANGE_ACCEPT,		usageWriteBoth,	IDS_NAME_IAC	},
	
	{ 5, "TuneCode",	PROPID_TUNE_CODE,		usageWriteBoth,	IDS_NAME_TCODE	},
	{ 5, "UserConstP",	PROPID_USER_CONST_P,		usageWriteBoth,	IDS_NAME_UCP	},
	{ 5, "UserConstI",	PROPID_USER_CONST_I,		usageWriteBoth,	IDS_NAME_UCI	},
	{ 5, "UserConstD",	PROPID_USER_CONST_D,		usageWriteBoth,	IDS_NAME_UCD	},
	{ 5, "UserCLimit",	PROPID_USER_CLIMIT,		usageWriteInit,	IDS_NAME_UCL	},
	{ 5, "UserHLimit",	PROPID_USER_HLIMIT,		usageWriteInit,	IDS_NAME_UHL	},
	{ 5, "UserFilter",	PROPID_USER_FILTER,		usageWriteBoth,	IDS_NAME_UF	},

	{ 5, "AutoConstP",	PROPID_AUTO_CONST_P,		usageRead,	IDS_NAME_ACP	},
	{ 5, "AutoConstI",	PROPID_AUTO_CONST_I,		usageRead,	IDS_NAME_ACI	},
	{ 5, "AutoConstD",	PROPID_AUTO_CONST_D,		usageRead,	IDS_NAME_ACD	},
	{ 5, "AutoFilter",	PROPID_AUTO_FILTER,		usageRead,	IDS_NAME_AF	},

	{ 5, "ActConstP",	PROPID_SEL_CONST_P,		usageRead,	IDS_NAME_CP	},
	{ 5, "ActConstI",	PROPID_SEL_CONST_I,		usageRead,	IDS_NAME_CI	},
	{ 5, "ActConstD",	PROPID_SEL_CONST_D,		usageRead,	IDS_NAME_CD	},
	{ 5, "ActFilter",	PROPID_SEL_FILTER,		usageRead,	IDS_NAME_F	},

	{ 3, "PowerFault",	PROPID_POWER_FAULT,		usageWriteBoth,	IDS_NAME_PF	},
	{ 3, "PowerOffset",	PROPID_POWER_OFFSET,		usageWriteBoth,	IDS_NAME_PO	},
	{ 3, "PowerDead",	PROPID_POWER_DEAD,		usageWriteBoth,	IDS_NAME_PD	},
	{ 3, "PowerHeatGain",	PROPID_POWER_HEAT_GAIN,		usageWriteBoth,	IDS_NAME_PHG	},
	{ 3, "PowerCoolGain",	PROPID_POWER_COOL_GAIN,		usageWriteBoth,	IDS_NAME_PCG	},
	{ 3, "PowerHeatHyst",	PROPID_POWER_HEAT_HYST,		usageWriteBoth,	IDS_NAME_PHH	},
	{ 3, "PowerCoolHyst",	PROPID_POWER_COOL_HYST,		usageWriteBoth,	IDS_NAME_PCH	},
	{ 3, "HeatLimitLo",	PROPID_HEAT_LIMIT_LO,		usageWriteBoth,	IDS_NAME_HLL	},
	{ 3, "HeatLimitHi",	PROPID_HEAT_LIMIT_HI,		usageWriteBoth,	IDS_NAME_HLH	},
	{ 3, "CoolLimitLo",	PROPID_COOL_LIMIT_LO,		usageWriteBoth,	IDS_NAME_CLL	},
	{ 3, "CoolLimitHi",	PROPID_COOL_LIMIT_HI,		usageWriteBoth,	IDS_NAME_CLH	},

	{ 6, "Seg00Time",	PROPID_P00_TIME,		usageWriteUser, IDS_P00_TIME	},
	{ 6, "Seg01Time",	PROPID_P01_TIME,		usageWriteUser, IDS_P01_TIME	},
	{ 6, "Seg02Time",	PROPID_P02_TIME,		usageWriteUser, IDS_P02_TIME	},
	{ 6, "Seg03Time",	PROPID_P03_TIME,		usageWriteUser, IDS_P03_TIME	},
	{ 6, "Seg04Time",	PROPID_P04_TIME,		usageWriteUser, IDS_P04_TIME	},
	{ 6, "Seg05Time",	PROPID_P05_TIME,		usageWriteUser, IDS_P05_TIME	},
	{ 6, "Seg06Time",	PROPID_P06_TIME,		usageWriteUser, IDS_P06_TIME	},
	{ 6, "Seg07Time",	PROPID_P07_TIME,		usageWriteUser, IDS_P07_TIME	},
	{ 6, "Seg08Time",	PROPID_P08_TIME,		usageWriteUser, IDS_P08_TIME	},
	{ 6, "Seg09Time",	PROPID_P09_TIME,		usageWriteUser, IDS_P09_TIME	},
	{ 6, "Seg10Time",	PROPID_P10_TIME,		usageWriteUser, IDS_P10_TIME	},
	{ 6, "Seg11Time",	PROPID_P11_TIME,		usageWriteUser, IDS_P11_TIME	},
	{ 6, "Seg12Time",	PROPID_P12_TIME,		usageWriteUser, IDS_P12_TIME	},
	{ 6, "Seg13Time",	PROPID_P13_TIME,		usageWriteUser, IDS_P13_TIME	},
	{ 6, "Seg14Time",	PROPID_P14_TIME,		usageWriteUser, IDS_P14_TIME	},
	{ 6, "Seg15Time",	PROPID_P15_TIME,		usageWriteUser, IDS_P15_TIME	},
	{ 6, "Seg16Time",	PROPID_P16_TIME,		usageWriteUser, IDS_P16_TIME	},
	{ 6, "Seg17Time",	PROPID_P17_TIME,		usageWriteUser, IDS_P17_TIME	},
	{ 6, "Seg18Time",	PROPID_P18_TIME,		usageWriteUser, IDS_P18_TIME	},
	{ 6, "Seg19Time",	PROPID_P19_TIME,		usageWriteUser, IDS_P19_TIME	},
	{ 6, "Seg20Time",	PROPID_P20_TIME,		usageWriteUser, IDS_P20_TIME	},
	{ 6, "Seg21Time",	PROPID_P21_TIME,		usageWriteUser, IDS_P21_TIME	},
	{ 6, "Seg22Time",	PROPID_P22_TIME,		usageWriteUser, IDS_P22_TIME	},
	{ 6, "Seg23Time",	PROPID_P23_TIME,		usageWriteUser, IDS_P23_TIME	},
	{ 6, "Seg24Time",	PROPID_P24_TIME,		usageWriteUser, IDS_P24_TIME	},
	{ 6, "Seg25Time",	PROPID_P25_TIME,		usageWriteUser, IDS_P25_TIME	},
	{ 6, "Seg26Time",	PROPID_P26_TIME,		usageWriteUser, IDS_P26_TIME	},
	{ 6, "Seg27Time",	PROPID_P27_TIME,		usageWriteUser, IDS_P27_TIME	},
	{ 6, "Seg28Time",	PROPID_P28_TIME,		usageWriteUser, IDS_P28_TIME	},
	{ 6, "Seg29Time",	PROPID_P29_TIME,		usageWriteUser, IDS_P29_TIME	},

	{ 6, "Seg00SP",		PROPID_P00_SP,			usageWriteUser, IDS_P00_SP	},
	{ 6, "Seg01SP",		PROPID_P01_SP,			usageWriteUser, IDS_P01_SP	},
	{ 6, "Seg02SP",		PROPID_P02_SP,			usageWriteUser, IDS_P02_SP	},
	{ 6, "Seg03SP",		PROPID_P03_SP,			usageWriteUser, IDS_P03_SP	},
	{ 6, "Seg04SP",		PROPID_P04_SP,			usageWriteUser, IDS_P04_SP	},
	{ 6, "Seg05SP",		PROPID_P05_SP,			usageWriteUser, IDS_P05_SP	},
	{ 6, "Seg06SP",		PROPID_P06_SP,			usageWriteUser, IDS_P06_SP	},
	{ 6, "Seg07SP",		PROPID_P07_SP,			usageWriteUser, IDS_P07_SP	},
	{ 6, "Seg08SP",		PROPID_P08_SP,			usageWriteUser, IDS_P08_SP	},
	{ 6, "Seg09SP",		PROPID_P09_SP,			usageWriteUser, IDS_P09_SP	},
	{ 6, "Seg10SP",		PROPID_P10_SP,			usageWriteUser, IDS_P10_SP	},
	{ 6, "Seg11SP",		PROPID_P11_SP,			usageWriteUser, IDS_P11_SP	},
	{ 6, "Seg12SP",		PROPID_P12_SP,			usageWriteUser, IDS_P12_SP	},
	{ 6, "Seg13SP",		PROPID_P13_SP,			usageWriteUser, IDS_P13_SP	},
	{ 6, "Seg14SP",		PROPID_P14_SP,			usageWriteUser, IDS_P14_SP	},
	{ 6, "Seg15SP",		PROPID_P15_SP,			usageWriteUser, IDS_P15_SP	},
	{ 6, "Seg16SP",		PROPID_P16_SP,			usageWriteUser, IDS_P16_SP	},
	{ 6, "Seg17SP",		PROPID_P17_SP,			usageWriteUser, IDS_P17_SP	},
	{ 6, "Seg18SP",		PROPID_P18_SP,			usageWriteUser, IDS_P18_SP	},
	{ 6, "Seg19SP",		PROPID_P19_SP,			usageWriteUser, IDS_P19_SP	},
	{ 6, "Seg20SP",		PROPID_P20_SP,			usageWriteUser, IDS_P20_SP	},
	{ 6, "Seg21SP",		PROPID_P21_SP,			usageWriteUser, IDS_P21_SP	},
	{ 6, "Seg22SP",		PROPID_P22_SP,			usageWriteUser, IDS_P22_SP	},
	{ 6, "Seg23SP",		PROPID_P23_SP,			usageWriteUser, IDS_P23_SP	},
	{ 6, "Seg24SP",		PROPID_P24_SP,			usageWriteUser, IDS_P24_SP	},
	{ 6, "Seg25SP",		PROPID_P25_SP,			usageWriteUser, IDS_P25_SP	},
	{ 6, "Seg26SP",		PROPID_P26_SP,			usageWriteUser, IDS_P26_SP	},
	{ 6, "Seg27SP",		PROPID_P27_SP,			usageWriteUser, IDS_P27_SP	},
	{ 6, "Seg28SP",		PROPID_P28_SP,			usageWriteUser, IDS_P28_SP	},
	{ 6, "Seg29SP",		PROPID_P29_SP,			usageWriteUser, IDS_P29_SP	},

	{ 6, "Seg00Mode",	PROPID_P00_MODE,		usageWriteUser, IDS_P00_MODE	},
	{ 6, "Seg01Mode",	PROPID_P01_MODE,		usageWriteUser, IDS_P01_MODE	},
	{ 6, "Seg02Mode",	PROPID_P02_MODE,		usageWriteUser, IDS_P02_MODE	},
	{ 6, "Seg03Mode",	PROPID_P03_MODE,		usageWriteUser, IDS_P03_MODE	},
	{ 6, "Seg04Mode",	PROPID_P04_MODE,		usageWriteUser, IDS_P04_MODE	},
	{ 6, "Seg05Mode",	PROPID_P05_MODE,		usageWriteUser, IDS_P05_MODE	},
	{ 6, "Seg06Mode",	PROPID_P06_MODE,		usageWriteUser, IDS_P06_MODE	},
	{ 6, "Seg07Mode",	PROPID_P07_MODE,		usageWriteUser, IDS_P07_MODE	},
	{ 6, "Seg08Mode",	PROPID_P08_MODE,		usageWriteUser, IDS_P08_MODE	},
	{ 6, "Seg09Mode",	PROPID_P09_MODE,		usageWriteUser, IDS_P09_MODE	},
	{ 6, "Seg10Mode",	PROPID_P10_MODE,		usageWriteUser, IDS_P10_MODE	},
	{ 6, "Seg11Mode",	PROPID_P11_MODE,		usageWriteUser, IDS_P11_MODE	},
	{ 6, "Seg12Mode",	PROPID_P12_MODE,		usageWriteUser, IDS_P12_MODE	},
	{ 6, "Seg13Mode",	PROPID_P13_MODE,		usageWriteUser, IDS_P13_MODE	},
	{ 6, "Seg14Mode",	PROPID_P14_MODE,		usageWriteUser, IDS_P14_MODE	},
	{ 6, "Seg15Mode",	PROPID_P15_MODE,		usageWriteUser, IDS_P15_MODE	},
	{ 6, "Seg16Mode",	PROPID_P16_MODE,		usageWriteUser, IDS_P16_MODE	},
	{ 6, "Seg17Mode",	PROPID_P17_MODE,		usageWriteUser, IDS_P17_MODE	},
	{ 6, "Seg18Mode",	PROPID_P18_MODE,		usageWriteUser, IDS_P18_MODE	},
	{ 6, "Seg19Mode",	PROPID_P19_MODE,		usageWriteUser, IDS_P19_MODE	},
	{ 6, "Seg20Mode",	PROPID_P20_MODE,		usageWriteUser, IDS_P20_MODE	},
	{ 6, "Seg21Mode",	PROPID_P21_MODE,		usageWriteUser, IDS_P21_MODE	},
	{ 6, "Seg22Mode",	PROPID_P22_MODE,		usageWriteUser, IDS_P22_MODE	},
	{ 6, "Seg23Mode",	PROPID_P23_MODE,		usageWriteUser, IDS_P23_MODE	},
	{ 6, "Seg24Mode",	PROPID_P24_MODE,		usageWriteUser, IDS_P24_MODE	},
	{ 6, "Seg25Mode",	PROPID_P25_MODE,		usageWriteUser, IDS_P25_MODE	},
	{ 6, "Seg26Mode",	PROPID_P26_MODE,		usageWriteUser, IDS_P26_MODE	},
	{ 6, "Seg27Mode",	PROPID_P27_MODE,		usageWriteUser, IDS_P27_MODE	},
	{ 6, "Seg28Mode",	PROPID_P28_MODE,		usageWriteUser, IDS_P28_MODE	},
	{ 6, "Seg29Mode",	PROPID_P29_MODE,		usageWriteUser, IDS_P29_MODE	},

	{ 0, "Mode",		PROPID_MODE,			usageWriteInit,	IDS_NAME_MODE	},
	{ 0, "TempUnits",	PROPID_TEMP_UNIT,		usageWriteInit,	IDS_NAME_TUNITS },
	{ 0, "DigHeat",		PROPID_DIG_HEAT,		usageWriteInit,	IDS_NAME_DHEAT	},
	{ 0, "DigCool",		PROPID_DIG_COOL,		usageWriteInit,	IDS_NAME_DCOOL	},
	{ 0, "InputType",	PROPID_INPUT_TYPE,		usageWriteInit,	IDS_NAME_IT	},
	{ 0, "InputTC",		PROPID_INPUT_TC,		usageWriteInit,	IDS_NAME_ITC	},
	{ 0, "HCMChannel",	PROPID_HCM_CHANNEL,		usageWriteInit,	IDS_NAME_HCHAN	},
	{ 0, "AlarmMode1",	PROPID_ALARM_MODE_1,		usageWriteInit,	IDS_NAME_AM1	},
	{ 0, "AlarmMode2",	PROPID_ALARM_MODE_2,		usageWriteInit,	IDS_NAME_AM2	},
	{ 0, "AlarmMode3",	PROPID_ALARM_MODE_3,		usageWriteInit,	IDS_NAME_AM3	},
	{ 0, "AlarmMode4",	PROPID_ALARM_MODE_4,		usageWriteInit,	IDS_NAME_AM4	},
	{ 0, "AlarmDelay1",	PROPID_ALARM_DELAY_1,		usageWriteInit,	IDS_NAME_ADE1	},
	{ 0, "AlarmDelay2",	PROPID_ALARM_DELAY_2,		usageWriteInit,	IDS_NAME_ADE2	},
	{ 0, "AlarmDelay3",	PROPID_ALARM_DELAY_3,		usageWriteInit,	IDS_NAME_ADE3	},
	{ 0, "AlarmDelay4",	PROPID_ALARM_DELAY_4,		usageWriteInit,	IDS_NAME_ADE4	},
	{ 0, "AlarmLatch1",	PROPID_ALARM_LATCH_1,		usageWriteInit,	IDS_NAME_ALA1	},
	{ 0, "AlarmLatch2",	PROPID_ALARM_LATCH_2,		usageWriteInit,	IDS_NAME_ALA2	},
	{ 0, "AlarmLatch3",	PROPID_ALARM_LATCH_3,		usageWriteInit,	IDS_NAME_ALA3	},
	{ 0, "AlarmLatch4",	PROPID_ALARM_LATCH_4,		usageWriteInit,	IDS_NAME_ALA4	},
	{ 0, "HCMLatchLo",	PROPID_HCM_LATCH_LO,		usageWriteInit,	IDS_NAME_HLALO	},
	{ 0, "HCMLatchHi",	PROPID_HCM_LATCH_HI,		usageWriteInit,	IDS_NAME_HLAHI	},
	{ 0, "RangeLatch",	PROPID_RANGE_LATCH,		usageWriteInit,	IDS_NAME_RL	},
	{ 0, "ReqSqRoot",	PROPID_SQUARE_ROOT,		usageWriteInit,	IDS_NAME_SQROOT	},

	};

// Constructor

CPIDLoop::CPIDLoop(UINT Model)
{
	m_TempUnits	= 1;
	m_ProcUnits	= "%";
	m_ProcMin	= 0;
	m_ProcMax	= 10000;
	m_ProcDP	= 2;
	m_Mode		= MODE_HEAT;
	m_DigHeat	= FALSE;
	m_DigCool	= FALSE;
	m_InputOffset	= 0;
	m_InputSlope	= 1000;
	m_ReqSqRoot	= FALSE;
	m_InputType	= INPUT_TC;
	m_InputTC	= TC_TYPEJ;
	m_HCMChannel	= 0;
	m_AlarmMode1	= ALARM_NULL;
	m_AlarmMode2	= ALARM_NULL;
	m_AlarmMode3	= ALARM_NULL;
	m_AlarmMode4	= ALARM_NULL;
	m_AlarmDelay1	= FALSE;
	m_AlarmDelay2	= FALSE;
	m_AlarmDelay3	= FALSE;
	m_AlarmDelay4	= FALSE;
	m_AlarmLatch1	= FALSE;
	m_AlarmLatch2	= FALSE;
	m_AlarmLatch3	= FALSE;
	m_AlarmLatch4	= FALSE;
	m_HCMLatchLo	= FALSE;
	m_HCMLatchHi	= FALSE;
	m_RangeLatch	= FALSE;
	m_TuneCode	= 2;
	m_InitData	= FALSE;
	m_ReqManual	= FALSE;
	m_ReqTune	= FALSE;
	m_ReqUserPID	= FALSE;
	m_AlarmAccept1	= FALSE;
	m_AlarmAccept2	= FALSE;
	m_AlarmAccept3	= FALSE;
	m_AlarmAccept4	= FALSE;
	m_HCMAcceptLo	= FALSE;
	m_HCMAcceptHi	= FALSE;
	m_InputAccept	= FALSE;
	m_Power		= 5000;
	m_ReqSP		= 58856;
	m_SetHyst	= 111;
	m_SetDead	= 0;
	m_SetRampBase	= 0;
	m_SetRamp	= 0;
	m_InputFilter	= 20;
	m_UserConstP	= 40;
	m_UserConstI	= 1200;
	m_UserConstD	= 300;
	m_UserCLimit	= 10000;
	m_UserHLimit	= 10000;
	m_UserFilter	= 10;
	m_PowerFault	= 0;
	m_PowerOffset	= 0;
	m_PowerDead	= 0;
	m_PowerHeatGain	= 10000;
	m_PowerCoolGain	= 10000;
	m_PowerHeatHyst	= 2000;
	m_PowerCoolHyst	= 2000;
	m_HeatLimitLo	= 0;
	m_HeatLimitHi	= 10000;
	m_CoolLimitLo	= 0;;
	m_CoolLimitHi	= 10000;
	m_HCMLimitLo	= 1000;
	m_HCMLimitHi	= 5000;
	m_AlarmData1	= 0;
	m_AlarmData2	= 0;
	m_AlarmData3	= 0;
	m_AlarmData4	= 0;
	m_AlarmHyst1	= 0;
	m_AlarmHyst2	= 0;
	m_AlarmHyst3	= 0;
	m_AlarmHyst4	= 0;
	m_Model		= Model;

	memset(m_SegTime, 0, sizeof(m_SegTime));

	memset(m_SegSP,   0, sizeof(m_SegSP  ));
	
	memset(m_SegMode, 0, sizeof(m_SegMode));

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CPIDLoop::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_STATUS);

		case 2:	return CString(IDS_MODULE_CTRL);

		case 4:	return CString(IDS_MODULE_ALARMS);

		case 3:	return CString(IDS_MODULE_POWER);

		case 5:	return CString(IDS_MODULE_PID);

		case 6: return CString(IDS_PROFILE);

		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CPIDLoop::GetPageCount(void)
{
	return 4;
	}

CString CPIDLoop::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_GEN);

		case 1: return CString(IDS_MODULE_CTRL);
		
		case 2: return CString(IDS_MODULE_POWER);
		
		case 3: return CString(IDS_MODULE_ALARMS);

		}

	return L"";
	}

CViewWnd * CPIDLoop::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CPIDLoopGeneralWnd;
		
		case 1: return New CPIDLoopControlWnd;
		
		case 2: return New CPIDLoopPowerWnd;
		
		case 3: return New CPIDLoopAlarmWnd;

		}

	return NULL;
	}

// Conversion

BOOL CPIDLoop::Convert(CPropValue *pValue)
{
	if( CCommsItem::Convert(pValue) ) {

		ImportString(pValue, L"ProcUnits");

		ImportNumber(pValue, L"ProcMin");
		ImportNumber(pValue, L"ProcMax");
		ImportNumber(pValue, L"ProcDP");

		return TRUE;
		}
	
	return FALSE;
	}

// Property Filter

BOOL CPIDLoop::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_USER_CONST_P:
			case PROPID_USER_CONST_I:
			case PROPID_USER_CONST_D:
			case PROPID_USER_HLIMIT:
			case PROPID_USER_CLIMIT:
			case PROPID_USER_FILTER:
			case PROPID_REQ_MANUAL:
			case PROPID_REQ_SP:
			case PROPID_POWER:
				
				return FALSE;
			}
		}

	return TRUE;
	}

// Data Scaling

DWORD CPIDLoop::GetIntProp(PCTXT pTag)
{
	if( IsTenTimes(pTag) ) {

		CMetaData const *pMeta = FindMetaData(pTag);

		if( pTag ) {

			INT nData = pMeta->ReadInteger(this);

			if( nData > 0 ) {

				nData = (nData + 5) / 10;
				}

			if( nData < 0 ) {

				nData = (nData - 5) / 10;
				}

			return DWORD(nData);
			}

		return 0;
		}

	return CCommsItem::GetIntProp(pTag);
	}

BOOL CPIDLoop::IsTenTimes(CString Tag)
{
	if( Tag == "ReqSP" ) {

		return TRUE;
		}

	if( Tag == "InputOffset" ) {

		return TRUE;
		}
	
	if( Tag == "SetHyst" ) {

		return TRUE;
		}

	if( Tag == "SetDead" ) {

		return TRUE;
		}

	if( Tag == "SetRamp" ) {

		return TRUE;
		}

	if( Tag.Left(9) == "AlarmData" ) {

		return TRUE;
		}

	if( Tag.Left(9) == "AlarmHyst" ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CPIDLoop::AddMetaData(void)
{
	Meta_AddInteger(TempUnits);
	Meta_AddString (ProcUnits);
	Meta_AddInteger(ProcMin);
	Meta_AddInteger(ProcMax);
	Meta_AddInteger(ProcDP);
	Meta_AddInteger(Mode);
	Meta_AddInteger(DigHeat);
	Meta_AddInteger(DigCool);
	Meta_AddInteger(InputType);
	Meta_AddInteger(InputTC);
	Meta_AddInteger(HCMChannel);
	Meta_AddInteger(AlarmMode1);
	Meta_AddInteger(AlarmMode2);
	Meta_AddInteger(AlarmMode3);
	Meta_AddInteger(AlarmMode4);
	Meta_AddInteger(AlarmDelay1);
	Meta_AddInteger(AlarmDelay2);
	Meta_AddInteger(AlarmDelay3);
	Meta_AddInteger(AlarmDelay4);
	Meta_AddInteger(AlarmLatch1);
	Meta_AddInteger(AlarmLatch2);
	Meta_AddInteger(AlarmLatch3);
	Meta_AddInteger(AlarmLatch4);
	Meta_AddInteger(HCMLatchLo);
	Meta_AddInteger(HCMLatchHi);
	Meta_AddInteger(RangeLatch);
	Meta_AddInteger(TuneCode);
	Meta_AddInteger(InitData);
	Meta_AddInteger(ReqManual);
	Meta_AddInteger(ReqTune);
	Meta_AddInteger(ReqUserPID);
	Meta_AddInteger(AlarmAccept1);
	Meta_AddInteger(AlarmAccept2);
	Meta_AddInteger(AlarmAccept3);
	Meta_AddInteger(AlarmAccept4);
	Meta_AddInteger(HCMAcceptLo);
	Meta_AddInteger(HCMAcceptHi);
	Meta_AddInteger(InputAccept);
	Meta_AddInteger(Power);
	Meta_AddInteger(ReqSP);
	Meta_AddInteger(SetHyst);
	Meta_AddInteger(SetDead);
	Meta_AddInteger(SetRampBase);
	Meta_AddInteger(SetRamp);
	Meta_AddInteger(InputFilter);
	Meta_AddInteger(InputOffset);
	Meta_AddInteger(InputSlope);
	Meta_AddInteger(ReqSqRoot);
	Meta_AddInteger(UserConstP);
	Meta_AddInteger(UserConstI);
	Meta_AddInteger(UserConstD);
	Meta_AddInteger(UserCLimit);
	Meta_AddInteger(UserHLimit);
	Meta_AddInteger(UserFilter);
	Meta_AddInteger(PowerFault);
	Meta_AddInteger(PowerOffset);
	Meta_AddInteger(PowerDead);
	Meta_AddInteger(PowerHeatGain);
	Meta_AddInteger(PowerCoolGain);
	Meta_AddInteger(PowerHeatHyst);
	Meta_AddInteger(PowerCoolHyst);
	Meta_AddInteger(HeatLimitLo);
	Meta_AddInteger(HeatLimitHi);
	Meta_AddInteger(CoolLimitLo);
	Meta_AddInteger(CoolLimitHi);
	Meta_AddInteger(HCMLimitLo);
	Meta_AddInteger(HCMLimitHi);
	Meta_AddInteger(AlarmData1);
	Meta_AddInteger(AlarmData2);
	Meta_AddInteger(AlarmData3);
	Meta_AddInteger(AlarmData4);
	Meta_AddInteger(AlarmHyst1);
	Meta_AddInteger(AlarmHyst2);
	Meta_AddInteger(AlarmHyst3);
	Meta_AddInteger(AlarmHyst4);

	for( UINT n = 0; n < 32; n++ ) {

		AddMeta( CPrintf("Seg%2.2uTime", n),
			 metaInteger,
			 offset(CPIDLoop, m_SegTime[n])
			 );
		}

	CMetaItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// PID Loop General View
//

// Runtime Class

AfxImplementRuntimeClass(CPIDLoopGeneralWnd, CUIViewWnd);

// Overibables

void CPIDLoopGeneralWnd::OnAttach(void)
{
	m_pItem   = (CPIDLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	if( m_pItem->m_Model ) {

		m_pSchema->LoadFromFile(TEXT("dlcloop"));
		}
	else
		m_pSchema->LoadFromFile(TEXT("pidloop"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CPIDLoopGeneralWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddUnits();

	AddInitial();

	AddSmart();

	EndPage(FALSE);
	}

void CPIDLoopGeneralWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	if( Tag == "InputType" ) {

		if( m_pItem->m_InputType == INPUT_RTD ) {

			if( m_pItem->m_InputTC < 10 ) {

				m_pItem->m_InputTC = 10;

				UpdateUI("InputTC");
				}
			}
		
		if( m_pItem->m_InputType == INPUT_TC ) {

			if( m_pItem->m_InputTC > 9 ) {

				m_pItem->m_InputTC = 1;

				UpdateUI("InputTC");
				}
			}
		
		EnableTC();
		}

	if( Tag == "InputTC" ) {

		if( m_pItem->m_InputTC < 10 ) {

			if( m_pItem->m_InputType == INPUT_RTD ) {

				m_pItem->m_InputType = INPUT_TC;

				UpdateUI("InputType");
				}
			}

		if( m_pItem->m_InputTC > 9 ) {

			if( m_pItem->m_InputType == INPUT_TC ) {

				m_pItem->m_InputType = INPUT_RTD;

				UpdateUI("InputType");
				}
			}
		}

	
	if( Tag == "InitData" ) {
		
		EnableInit();
		}

	if( Tag == "DigHeat" ) {
		
		EnableHeat();
		}

	if( Tag == "DigCool" ) {
		
		EnableCool();
		}

	CUIPIDProcess::CheckUpdate(m_pItem, Tag);
	}

// UI Creation

void CPIDLoopGeneralWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_OP), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("Mode"       ));
	AddUI(m_pItem, TEXT("root"), TEXT("InputType"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("InputTC"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));
	AddUI(m_pItem, TEXT("root"), TEXT("InputOffset"));
	AddUI(m_pItem, TEXT("root"), TEXT("InputSlope" ));

	EndGroup(TRUE);
	}

void CPIDLoopGeneralWnd::AddUnits(void)
{
	StartGroup(CString(IDS_MODULE_UNITS), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("TempUnits"));
	AddUI(m_pItem, TEXT("root"), TEXT("ProcUnits"));
	AddUI(m_pItem, TEXT("root"), TEXT("ProcDP"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("ProcMin"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("ProcMax"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqSqRoot"));

	EndGroup(TRUE);
	}

void CPIDLoopGeneralWnd::AddInitial(void)
{
	StartGroup(CString(IDS_MODULE_INIT), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("InitData"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqUserPID"));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqManual" ));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqSP"     ));
	AddUI(m_pItem, TEXT("root"), TEXT("Power"     ));

	EndGroup(TRUE);
	}

void CPIDLoopGeneralWnd::AddSmart(void)
{
	StartGroup(CPrintf(IDS_MODULE_SMART, 174), 2, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("DigHeat"      ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerHeatHyst"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigCool"      ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerCoolHyst"));

	EndGroup(TRUE);
	}

// Enabling

void CPIDLoopGeneralWnd::DoEnables(void)
{
	EnableTC();

	EnableHeat();

	EnableCool();

	EnableInit();
	}

void CPIDLoopGeneralWnd::EnableTC(void)
{
	BOOL fEnable = (m_pItem->m_InputType >= INPUT_RTD);

	BOOL fMilli  = (m_pItem->m_InputType == INPUT_050MV);

	EnableUI("InputTC",     fEnable);
	EnableUI("TempUnits",   fEnable);
	EnableUI("InputOffset", fEnable);
	EnableUI("InputSlope",  fEnable);
	EnableUI("ProcUnits",  !fEnable);
	EnableUI("ProcDP",     !fEnable);
	EnableUI("ProcMin",    !fEnable);
	EnableUI("ProcMax",    !fEnable);
	EnableUI("ReqSqRoot",  !fEnable && !fMilli);
	}

void CPIDLoopGeneralWnd::EnableHeat(void)
{
	BOOL fEnable = m_pItem->m_DigHeat;

	EnableUI("PowerHeatHyst", fEnable);
	}

void CPIDLoopGeneralWnd::EnableCool(void)
{
	BOOL fEnable = m_pItem->m_DigCool;

	EnableUI("PowerCoolHyst", fEnable);
	}

void CPIDLoopGeneralWnd::EnableInit(void)
{
	BOOL fEnable = m_pItem->m_InitData;

	EnableUI("ReqManual",  fEnable);
	EnableUI("ReqUserPID", fEnable);
	EnableUI("ReqSP",      fEnable);
	EnableUI("Power",      fEnable);
	}

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Power View
//

// Runtime Class

AfxImplementRuntimeClass(CPIDLoopPowerWnd, CUIViewWnd);

// Overibables

void CPIDLoopPowerWnd::OnAttach(void)
{
	m_fScroll = FALSE;

	m_pItem   = (CPIDLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	if( m_pItem->m_Model ) {

		m_pSchema->LoadFromFile(TEXT("dlcloop"));
		}
	else
		m_pSchema->LoadFromFile(TEXT("pidloop"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CPIDLoopPowerWnd::OnUICreate(void)
{
	StartPage(1);

	AddPower();

	AddGraph();

	EndPage(FALSE);
	}

void CPIDLoopPowerWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	m_pPower->Update();

	CUIPIDProcess::CheckUpdate(m_pItem, Tag);
	}

// Message Map

AfxMessageMap(CPIDLoopPowerWnd, CUIViewWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxMessageEnd(CPIDLoopPowerWnd)
	};

BOOL CPIDLoopPowerWnd::OnEraseBkGnd(CDC &DC)
{
	DC.Save();

	m_pPower->Exclude(ThisObject, DC);

	CRect Rect = GetClientRect();

	DC.FillRect(Rect, afxBrush(TabFace));

	DC.Restore();

	return TRUE;
	}

// UI Creation

void CPIDLoopPowerWnd::AddPower(void)
{
	StartGroup(CString(IDS_MODULE_PWR_TRANS), 2, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("PowerOffset"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerDead"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerHeatGain"));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerCoolGain"));
	AddUI(m_pItem, TEXT("root"), TEXT("HeatLimitLo"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("CoolLimitLo"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("HeatLimitHi"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("CoolLimitHi"  ));

	EndGroup(TRUE);
	}

void CPIDLoopPowerWnd::AddGraph(void)
{
	StartGroup(CString(IDS_MODULE_PWR_GRAPH), 1, FALSE);

	m_pPower = New CUIPIDPower(m_pItem);

	AddElement(m_pPower);

	EndGroup(TRUE);
	}

// Enabling

void CPIDLoopPowerWnd::DoEnables(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Control View
//

// Runtime Class

AfxImplementRuntimeClass(CPIDLoopControlWnd, CUIViewWnd);

// Overibables

void CPIDLoopControlWnd::OnAttach(void)
{
	m_pItem   = (CPIDLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	if( m_pItem->m_Model ) {

		m_pSchema->LoadFromFile(TEXT("dlcloop"));
		}
	else
		m_pSchema->LoadFromFile(TEXT("pidloop"));

	CUIViewWnd::OnAttach();
 	}

// UI Update

void CPIDLoopControlWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddAuto();

	AddPID();

	EndPage(FALSE);
	}

void CPIDLoopControlWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	if( Tag == "SetRampBase" ) {

		DoEnables();
		}

	CUIPIDProcess::CheckUpdate(m_pItem, Tag);
	}

// UI Creation

void CPIDLoopControlWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_SP), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("SetRampBase"));
	AddUI(m_pItem, TEXT("root"), TEXT("SetRamp"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("SetHyst"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("SetDead"    ));

	EndGroup(TRUE);
	}

void CPIDLoopControlWnd::AddAuto(void)
{
	StartGroup(CString(IDS_MODULE_AUTO_SET), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("TuneCode"));

	EndGroup(TRUE);
	}

void CPIDLoopControlWnd::AddPID(void)
{
	StartGroup(CString(IDS_MODULE_PID_USET), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("UserConstP"));
	AddUI(m_pItem, TEXT("root"), TEXT("UserConstI"));
	AddUI(m_pItem, TEXT("root"), TEXT("UserConstD"));
	AddUI(m_pItem, TEXT("root"), TEXT("UserFilter"));

	EndGroup(TRUE);
	}

// Enabling

void CPIDLoopControlWnd::DoEnables(void)
{
	BOOL fRamp = (m_pItem->m_SetRampBase > 0);

	EnableUI("SetRamp", fRamp);
	}

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Alarm View
//

// Runtime Class

AfxImplementRuntimeClass(CPIDLoopAlarmWnd, CUIViewWnd);

// Overibables

void CPIDLoopAlarmWnd::OnAttach(void)
{
	m_pItem   = (CPIDLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	if( m_pItem->m_Model ) {

		m_pSchema->LoadFromFile(TEXT("dlcloop"));
		}
	else
		m_pSchema->LoadFromFile(TEXT("pidloop"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CPIDLoopAlarmWnd::OnUICreate(void)
{
	StartPage(1);

	AddAlarms();

	AddHCM();

	AddInput();

	EndPage(FALSE);
	}

void CPIDLoopAlarmWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	for( UINT n = 1; n <= 4; n++ ) {

		if( Tag == CPrintf("AlarmMode%u", n) ) {

			EnableAlarm(n);
			}
		}

	if( Tag == "HCMChannel" ) {
		
		EnableHCM();
		}

	CUIPIDProcess::CheckUpdate(m_pItem, Tag);
	}

// UI Creation

void CPIDLoopAlarmWnd::AddAlarms(void)
{
	for( UINT n = 1; n <= 4; n++ ) {

		AddAlarm(n);
		}
	}

void CPIDLoopAlarmWnd::AddAlarm(UINT n)
{
	StartGroup(CPrintf(IDS_MODULE_ALARM, n), 2, FALSE);

	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmMode%u", n));

	AddUI(m_pItem, TEXT("root"), TEXT(""));
	
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmData%u",  n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmLatch%u", n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmHyst%u",  n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmDelay%u", n));

	EndGroup(TRUE);
	}

void CPIDLoopAlarmWnd::AddHCM(void)
{
	StartGroup(CString(IDS_MODULE_HC), 2, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("HCMChannel"));

	AddUI(m_pItem, TEXT("root"), TEXT(""));

	AddUI(m_pItem, TEXT("root"), TEXT("HCMLimitLo"));
	AddUI(m_pItem, TEXT("root"), TEXT("HCMLatchLo"));
	AddUI(m_pItem, TEXT("root"), TEXT("HCMLimitHi"));
	AddUI(m_pItem, TEXT("root"), TEXT("HCMLatchHi"));

	EndGroup(TRUE);
	}

void CPIDLoopAlarmWnd::AddInput(void)
{
	StartGroup(CString(IDS_MODULE_INP_FAULT), 2, FALSE);

	AddUI(m_pItem, TEXT("root"), TEXT("PowerFault"));
	AddUI(m_pItem, TEXT("root"), TEXT("RangeLatch"));

	EndGroup(TRUE);
	}

// Enabling

void CPIDLoopAlarmWnd::DoEnables(void)
{
	for( UINT n = 1; n <= 4; n++ ) {

		EnableAlarm(n);
		}

	EnableHCM();
	}

void CPIDLoopAlarmWnd::EnableAlarm(UINT n)
{
	BOOL fEnable = FALSE;

	switch( n ) {
		
		case 1: fEnable = m_pItem->m_AlarmMode1; break;
		case 2: fEnable = m_pItem->m_AlarmMode2; break;
		case 3: fEnable = m_pItem->m_AlarmMode3; break;
		case 4: fEnable = m_pItem->m_AlarmMode4; break;

		}

	EnableUI(CPrintf("AlarmData%u",  n), fEnable);
	EnableUI(CPrintf("AlarmLatch%u", n), fEnable);
	EnableUI(CPrintf("AlarmHyst%u",  n), fEnable);
	EnableUI(CPrintf("AlarmDelay%u", n), fEnable);
	}

void CPIDLoopAlarmWnd::EnableHCM(void)
{
	BOOL fEnable = m_pItem->m_HCMChannel ? TRUE : FALSE;

	EnableUI("HCMLimitLo", fEnable);
	EnableUI("HCMLimitHi", fEnable);
	EnableUI("HCMLatchLo", fEnable);
	EnableUI("HCMLatchHi", fEnable);
	}

// End of File
