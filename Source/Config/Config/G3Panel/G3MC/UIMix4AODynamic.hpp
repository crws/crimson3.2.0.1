
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UIMix4AODynamic_HPP

#define INCLUDE_UIMix4AODynamic_HPP

#include "DAMix4AnalogOutputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

class CUIMix4AODynamic : public CUIEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUIMix4AODynamic(void);

	// Destructor
	~CUIMix4AODynamic(void);

	// Update Support
	static void CheckUpdate(CDAMix4AnalogOutputConfig *pConfig, CString const &Tag);

	// Operations
	void Update(BOOL fKeep);
	void UpdateUnits(void);

protected:
	// Linked List
	static CUIMix4AODynamic * m_pHead;
	static CUIMix4AODynamic * m_pTail;

	// Data Members
	CUIMix4AODynamic * m_pNext;
	CUIMix4AODynamic * m_pPrev;
};

// End of File

#endif
