
#include "intern.hpp"

#include "gmrc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/rcprops.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rate Count Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMRCModule, CGraphiteGenericModule);

// Constructor

CGMRCModule::CGMRCModule(void)
{
	m_pConfig = New CGraphiteRCConfig;

	m_Ident   = LOBYTE(ID_GMRC);

	m_FirmID  = FIRM_GMRC;

	m_Model   = "GMRC";

	m_Power   = 41; //??

	m_Conv.Insert(L"Legacy", L"RCModule");
	}

// UI Management

CViewWnd * CGMRCModule::CreateMainView(void)
{
	return New CGraphiteRCMainWnd;
	}

// Comms Object Access

UINT CGMRCModule::GetObjectCount(void)
{
	return 1;
	}

BOOL CGMRCModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(L"TestObject");
			
			Data.pItem = m_pConfig;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CGMRCModule::Convert(CPropValue *pValue)
{
	return FALSE;
	}

// Implementation

void CGMRCModule::AddMetaData(void)
{
	Meta_AddObject(Config);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Rate Count Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteRCMainWnd, CProxyViewWnd);

// Constructor

CGraphiteRCMainWnd::CGraphiteRCMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CGraphiteRCMainWnd::OnAttach(void)
{
	m_pItem = (CGMRCModule *) CProxyViewWnd::m_pItem;

	AddPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CGraphiteRCMainWnd::AddPages(void)
{
	CGraphiteRCConfig *pConfig = m_pItem->m_pConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(pConfig->GetPageName(n), pPage);

		pPage->Attach(pConfig);
		}
	}

// End of File
