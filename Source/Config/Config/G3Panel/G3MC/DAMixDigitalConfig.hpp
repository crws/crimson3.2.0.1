
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMixDigitalConfig_HPP

#define INCLUDE_DAMixDigitalConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// Mix Module Digital Configuration
//

class CDAMixDigitalConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMixDigitalConfig(UINT uChans);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Item Properties
	UINT m_ChanMode1;
	UINT m_ChanMode2;
	UINT m_ChanMode3;
	UINT m_ChanMode4;
	UINT m_ChanMode5;
	UINT m_ChanMode6;
	UINT m_ChanMode7;
	UINT m_ChanMode8;

	// Public Data
	UINT	m_uChans;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Meta Data Creation
	void AddMetaData(void);
};

// End of File

#endif
