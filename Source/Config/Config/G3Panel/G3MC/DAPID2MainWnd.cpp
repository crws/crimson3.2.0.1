
#include "intern.hpp"

#include "DAPID2MainWnd.hpp"

#include "DAPID2ViewWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Digitail Input Output Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAPID2MainWnd, CDLCMainWnd);

// Constructor

CDAPID2MainWnd::CDAPID2MainWnd(void)
{
}

// Overribables

void CDAPID2MainWnd::OnAttach(void)
{
	m_pItem = (CDLCModule *) CProxyViewWnd::m_pItem;

	AddPIDPages(m_pItem->m_pLoop1, L"L1");

	AddPIDPages(m_pItem->m_pLoop2, L"L2");

	AddMapPages();

	AddIdentPage();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDAPID2MainWnd::AddIdentPage(void)
{
	CString   Name  = IDS("Hardware");

	CViewWnd *pPage = New CDAPID2ViewWnd;

	m_pMult->AddView(Name, pPage);

	pPage->Attach(m_pItem);
}

// End of File
