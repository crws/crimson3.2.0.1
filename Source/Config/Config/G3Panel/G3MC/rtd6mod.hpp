
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RTD6MOD_HPP

#define INCLUDE_RTD6MOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rtd6inp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRTD6Module;
class CRTD6MainWnd;

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module
//

class CRTD6Module : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRTD6Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Data Members
		CRTD6Input * m_pInput;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module Window
//

class CRTD6MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRTD6MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd   * m_pMult;
		CRTD6Module     * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddRTD6Pages(void);
	};

// End of File

#endif
