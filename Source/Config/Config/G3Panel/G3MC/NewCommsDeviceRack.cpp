
#include "intern.hpp"

#include "NewCommsDeviceRack.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ManticoreGenericModule.hpp"

#include "NewCommsDeviceRackWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device
//

// Dynamic Class

AfxImplementDynamicClass(CNewCommsDeviceRack, CCommsDevice);

// Constructor

CNewCommsDeviceRack::CNewCommsDeviceRack(void)
{
	m_pModule = NULL;
}

// UI Creation

CViewWnd * CNewCommsDeviceRack::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return New CNewCommsDeviceRackWnd;
	}

	return NULL;
}

// Attributes

UINT CNewCommsDeviceRack::GetJsonType(void) const
{
	if( m_pModule ) {

		CString Name = m_pModule->GetClassName();

		if( Name.StartsWith(L"CGM") ) {

			switch( m_pModule->m_Ident ) {

				case LOBYTE(ID_GMPID1):		return 204;
				case LOBYTE(ID_GMPID2):		return 205;
				case LOBYTE(ID_GMUIN4):		return 208;
				case LOBYTE(ID_GMOUT4):		return 203;
				case LOBYTE(ID_GMDIO14):	return 200;
				case LOBYTE(ID_GMTC8):		return 207;
				case LOBYTE(ID_GMINI8):		return 201;
				case LOBYTE(ID_GMINV8):		return 202;
				case LOBYTE(ID_GMRTD6):		return 206;
				case LOBYTE(ID_GMSG1):		return 209;
			}

			return 0;
		}

		if( Name.StartsWith(L"CDA") ) {

			switch( m_pModule->m_Ident ) {

				case LOBYTE(ID_DADIDO):		return 210;
				case LOBYTE(ID_DADIRO):		return 210;
				case LOBYTE(ID_DAUIN6):		return 211;
				case LOBYTE(ID_DAAO8):		return 212;
				case LOBYTE(ID_DAPID1_RA):	return 213;
				case LOBYTE(ID_DAPID1_SA):	return 213;
				case LOBYTE(ID_DAPID2_SM):	return 214;
				case LOBYTE(ID_DAPID2_RO):	return 214;
				case LOBYTE(ID_DAPID2_SO):	return 214;
			}

			return 0;
		}

		return m_pModule->m_FirmID ? 300+m_pModule->m_Ident : 0;
	}

	return 0;
}

// Operations

void CNewCommsDeviceRack::SetClass(CLASS Class)
{
	if( m_pModule ) {

		if( AfxPointerClass(m_pModule) == Class ) {

			return;
		}

		m_pModule->Kill();

		delete m_pModule;
	}

	m_pModule = AfxNewObject(CGenericModule, Class);

	m_pModule->SetParent(this);

	m_pModule->Init();
}

// Naming

CString CNewCommsDeviceRack::GetHumanName(void) const
{
	CString Name = m_Name;

	Name += L" - ";

	if( m_pModule ) {

		CString Model = m_pModule->m_Model;

		C3OemStrings(Model);

		Name += Model;
	}

	return Name;
}

CString CNewCommsDeviceRack::GetItemOrdinal(void) const
{
	return m_Where;
}

// Persistance

void CNewCommsDeviceRack::PostPaste(void)
{
	CCodedHost::PostPaste();

	UpdateDriver();

	CCommsManager *pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

	m_Number = pManager->AllocDeviceNumber();
}

// Conversion

BOOL CNewCommsDeviceRack::PostConvert(CString const &Conv)
{
	if( Conv != L"KEEP" ) {

		CLASS Class = AfxNamedClass(Conv);

		if( AfxPointerClass(m_pModule) != Class ) {

			CPropConverter Converter;

			Converter.LoadFromItem(m_pModule);

			SetClass(Class);

			CPropValue *pRoot = &Converter.m_Root;

			m_pModule->Convert(pRoot);

			return TRUE;
		}
	}

	return FALSE;
}

// Meta Data Creation

void CNewCommsDeviceRack::AddMetaData(void)
{
	CCommsDevice::AddMetaData();

	Meta_AddVirtual(Module);
	Meta_AddString(SlotTag);

	Meta_SetName((IDS_MODULE_MODULE));
}

// End of File
