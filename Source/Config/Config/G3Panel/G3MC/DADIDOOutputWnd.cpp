
#include "intern.hpp"

#include "dadidooutputwnd.hpp"

#include "dadidooutputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOOutputWnd, CUIViewWnd);

// Overibables

void CDADIDOOutputWnd::OnAttach(void)
{
	m_pItem   = (CDADIDOOutputConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dadio_out"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDADIDOOutputWnd::OnUICreate(void)
{
	StartPage(1);

	AddOutputs();

	EndPage(FALSE);
}

// Implementation

void CDADIDOOutputWnd::AddOutputs(void)
{
	StartTable(IDS("Control"), 1);

	AddColHead(L"Power On State");

	for( UINT n = 0; n < 8; n++ ) {

		AddRowHead(CPrintf(IDS("Output %u"), n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));
	}

	EndTable();
}

// End of File
