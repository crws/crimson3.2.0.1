
#include "intern.hpp"

#include "UIAO8Dynamic.hpp"

#include "UITextAO8Dynamic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

// Dynamic Class

AfxImplementDynamicClass(CUIAO8Dynamic, CUIEditBox)

// Linked List

CUIAO8Dynamic * CUIAO8Dynamic::m_pHead = NULL;

CUIAO8Dynamic * CUIAO8Dynamic::m_pTail = NULL;

// Constructor

CUIAO8Dynamic::CUIAO8Dynamic(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
}

// Destructor

CUIAO8Dynamic::~CUIAO8Dynamic(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
}

// Update Support

void CUIAO8Dynamic::CheckUpdate(CDAAO8OutputConfig *pConfig, CString const &Tag)
{
	if( Tag.Left(4) == "Type" ) {

		CUIAO8Dynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextAO8Dynamic *pText = (CUITextAO8Dynamic *) pScan->m_pText;

			if( pText->m_pConfig == pConfig ) {

				if( pText->m_Type[0] == 'A' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'B' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'C' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'D' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'E' ) {

					pScan->Update(TRUE);
				}
			}

			pScan = pScan->m_pNext;
		}
	}

	if( Tag.Left(2) == "DP" ) {

		CUIAO8Dynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextAO8Dynamic *pText = (CUITextAO8Dynamic *) pScan->m_pText;

			if( pText->m_pConfig == pConfig ) {

				if( pText->m_Type[0] == 'A' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'B' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'E' ) {

					pScan->Update(TRUE);
				}
			}

			pScan = pScan->m_pNext;
		}
	}

	if( Tag.Left(4) == "Data" ) {

		CUIAO8Dynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextAO8Dynamic *pText = (CUITextAO8Dynamic *) pScan->m_pText;

			if( pText->m_pConfig == pConfig ) {

				if( pText->m_Type[0] == 'E' ) {

					pScan->Update(TRUE);
				}
			}

			pScan = pScan->m_pNext;
		}
	}
}

// Operations

void CUIAO8Dynamic::Update(BOOL fKeep)
{
	CUITextAO8Dynamic *pText = (CUITextAO8Dynamic *) m_pText;

	pText->GetConfig();

	if( fKeep ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
	}
	else {
		INT nData = m_pData->ReadInteger(m_pItem);

		INT nCopy = nData;

		pText->Check(CError(FALSE), nData);

		if( nData != nCopy ) {

			m_pData->WriteInteger(m_pItem, UINT(nData));

			m_pItem->SetDirty();
		}

		LoadUI();
	}

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
}

void CUIAO8Dynamic::UpdateUnits(void)
{
	CUITextAO8Dynamic *pText = (CUITextAO8Dynamic *) m_pText;

	pText->GetConfig();

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
}

// End of File
