
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PIDLOOP_HPP

#define INCLUDE_PIDLOOP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\dbase.h"

#include "import\dlcbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPIDLoop;
class CPIDLoopGeneralWnd;
class CPIDLoopControlWnd;
class CPIDLoopPowerWnd;
class CPIDLoopAlarmWnd;
class CUITextPIDProcess;
class CUIPIDPower;
class CPIDPowerWnd;

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Item
//

class CPIDLoop : public CCommsItem
{

	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPIDLoop(UINT Model);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		UINT	m_TempUnits;
		CString	m_ProcUnits;
		INT	m_ProcMin;
		INT	m_ProcMax;
		UINT	m_ProcDP;
		UINT	m_Mode;
		UINT	m_DigHeat;
		UINT	m_DigCool;
		UINT    m_ReqSqRoot;
		UINT	m_InputSlope;
		INT	m_InputOffset;
		UINT	m_InputType;
		UINT	m_InputTC;
		UINT	m_HCMChannel;
		UINT	m_AlarmMode1;
		UINT	m_AlarmMode2;
		UINT	m_AlarmMode3;
		UINT	m_AlarmMode4;
		UINT	m_AlarmDelay1;
		UINT	m_AlarmDelay2;
		UINT	m_AlarmDelay3;
		UINT	m_AlarmDelay4;
		UINT	m_AlarmLatch1;
		UINT	m_AlarmLatch2;
		UINT	m_AlarmLatch3;
		UINT	m_AlarmLatch4;
		UINT	m_HCMLatchLo;
		UINT	m_HCMLatchHi;
		UINT	m_RangeLatch;
		UINT	m_TuneCode;
		UINT	m_InitData;
		UINT	m_ReqManual;
		UINT	m_ReqTune;
		UINT	m_ReqUserPID;
		UINT	m_AlarmAccept1;
		UINT	m_AlarmAccept2;
		UINT	m_AlarmAccept3;
		UINT	m_AlarmAccept4;
		UINT	m_HCMAcceptLo;
		UINT	m_HCMAcceptHi;
		UINT	m_InputAccept;
		UINT	m_Power;
		UINT	m_ReqSP;
		UINT	m_SetHyst;
		INT	m_SetDead;
		UINT	m_SetRampBase;
		UINT	m_SetRamp;
		UINT	m_InputFilter;
		UINT	m_UserConstP;
		UINT	m_UserConstI;
		UINT	m_UserConstD;
		UINT	m_UserCLimit;
		UINT	m_UserHLimit;
		UINT	m_UserFilter;
		INT	m_PowerFault;
		INT	m_PowerOffset;
		INT	m_PowerDead;
		UINT	m_PowerHeatGain;
		UINT	m_PowerCoolGain;
		UINT	m_PowerHeatHyst;
		UINT	m_PowerCoolHyst;
		UINT	m_HeatLimitLo;
		UINT	m_HeatLimitHi;
		UINT	m_CoolLimitLo;
		UINT	m_CoolLimitHi;
		UINT	m_HCMLimitLo;
		UINT	m_HCMLimitHi;
		INT	m_AlarmData1;
		INT	m_AlarmData2;
		INT	m_AlarmData3;
		INT	m_AlarmData4;
		INT	m_AlarmHyst1;
		INT	m_AlarmHyst2;
		INT	m_AlarmHyst3;
		INT	m_AlarmHyst4;
		UINT	m_Model;
		UINT	m_SegTime[32];
		INT	m_SegSP  [32];
		UINT	m_SegMode[32];

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Data Scaling
		DWORD GetIntProp(PCTXT pTag);
		BOOL  IsTenTimes(CString Tag);

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Loop General View
//

class CPIDLoopGeneralWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CPIDLoop * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddGeneral(void);
		void AddUnits(void);
		void AddSmart(void);
		void AddInitial(void);

		// Enabling
		void DoEnables(void);
		void EnableTC(void);
		void EnableHeat(void);
		void EnableCool(void);
		void EnableInit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Power View
//

class CPIDLoopPowerWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CPIDLoop    * m_pItem;
		CUIPIDPower * m_pPower;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);

		// UI Creation
		void AddPower(void);
		void AddGraph(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Control View
//

class CPIDLoopControlWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CPIDLoop * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddGeneral(void);
		void AddAuto(void);
		void AddPID(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Loop Alarm View
//

class CPIDLoopAlarmWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CPIDLoop * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddAlarms(void);
		void AddAlarm(UINT n);
		void AddHCM(void);
		void AddInput(void);

		// Enabling
		void DoEnables(void);
		void EnableAlarm(UINT n);
		void EnableHCM(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- PID Process Value
//

class CUIPIDProcess : public CUIEditBox
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIPIDProcess(void);

		// Destructor
		~CUIPIDProcess(void);

		// Update Support
		static void CheckUpdate(CPIDLoop *pLoop, CString const &Tag);

		// Operations
		void Update(BOOL fKeep);
		void UpdateUnits(void);

	protected:
		// Linked List
		static CUIPIDProcess * m_pHead;
		static CUIPIDProcess * m_pTail;

		// Data Members
		CUIPIDProcess * m_pNext;
		CUIPIDProcess * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- PID Process Value
//

class CUITextPIDProcess : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextPIDProcess(void);

		// Destructor
		~CUITextPIDProcess(void);

	protected:
		// Data Members
		CPIDLoop * m_pLoop;
		char	   m_cType;
		double	   m_t1;
		double	   m_t2;

		// Core Overidables
		void OnBind(void);

		// Implementation
		void GetConfig(void);
		void CheckType(void);
		void CheckFlags(void);

		// Scaling
		INT  StoreToDisp(INT nData);
		INT  DispToStore(INT nData);

		// Implementation
		INT     Scale(double a, double b, double c, double d);
		void    GetConstants(double &a, double &b, double &c);
		BOOL    IsAlarmAbsolute(void);
		BOOL	IsOutputAbsolute(void);
		BOOL    IsOutputPercent(void);
		CString GetRateUnits(void);

		// Friends
		friend class CUIPIDProcess;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- PID Power Curve
//

class CUIPIDPower : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIPIDPower(CPIDLoop *pLoop);

		// Destructor
		~CUIPIDPower(void);

		// Operations
		void Update(void);
		void Exclude(CWnd &Wnd, CDC &DC);

	protected:
		// Data Members
		CPIDLoop     * m_pLoop;
		CLayFormPad  * m_pLayout;
		CPIDPowerWnd * m_pCtrl;

		// Core Overidables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnMove(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Power Curve Window
//

class CPIDPowerWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPIDPowerWnd(CPIDLoop *pLoop);

		// Destructor
		~CPIDPowerWnd(void);

		// Operations
		void Update(void);
		void Exclude(CWnd &Wnd, CDC &DC);

	protected:
		// Data Members
		CPIDLoop * m_pLoop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);

		// Implementation
		void Draw(CDC &DC, CRect Rect);
	};

// End of File

#endif
