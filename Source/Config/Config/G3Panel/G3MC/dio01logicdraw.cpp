
#include "intern.hpp"

#include "legacy.h"

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Digital Module Logic Drawing Functions
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Logic window drawing ops.
// 

void CDIO14LogicWnd::DrawReferenceSyms(CDC &DC)
{
	DrawAND(AND_PROTO, DC);
	
	DrawOR(OR_PROTO, DC);
	
	DrawXOR(XOR_PROTO, DC);

	DrawNOT(NOT_PROTO, DC);

	DrawTIMERUP(TIMERUP_PROTO, DC);
	
	DrawTIMERDN(TIMERDN_PROTO, DC);

	DrawCOUNTUP(COUNTUP_PROTO, DC);
	
	DrawCOUNTDN(COUNTDN_PROTO, DC);

	DrawLATCH(LATCH_PROTO, DC);

	DrawINCOIL(INCOIL_PROTO, DC);
	
	DrawOUTCOIL(OUTCOIL_PROTO, DC);
	}

// Symbol drawing ops

void CDIO14LogicWnd::DrawJCT(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
		
	Bitmap.Create(L"JCT", CSize(5, 5));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawAND(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
		
	Bitmap.Create(L"AND_Gate", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawOR(UINT Index, CDC &DC)
{	
	CBitmap Bitmap;
		
	Bitmap.Create(L"OR_Gate", CSize(19, 19));
		
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawXOR(UINT Index, CDC &DC)
{
	CBitmap Bitmap;

	Bitmap.Create(L"XOR_Gate", CSize(19, 19));
		
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawNOT(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"NOT_Gate", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawTIMERUP(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Timerup", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawTIMERDN(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Timerdn", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawCOUNTUP(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Countup", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawCOUNTDN(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Countdn", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawLATCH(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Latch", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawINCOIL(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Incoil", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawOUTCOIL(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"Outcoil", CSize(19, 19));
	
	DC.BitBlt(m_pItem->m_Sym[Index].Left,m_pItem->m_Sym[Index].Top,m_pItem->m_Sym[Index].Width-11,m_pItem->m_Sym[Index].Height, Bitmap,m_pItem->m_Sym[Index].BMPOffsetX,0, SRCCOPY);
	}

void CDIO14LogicWnd::DrawB_UNDO(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"B_Undo",CSize(19, 19));
	
	if(m_UlistLimit > 0) m_Button[Index].Enabled = TRUE;

	else m_Button[Index].Enabled = FALSE;

	short Xoff;
	
	if(m_Button[Index].Enabled == TRUE) Xoff = 0;

	else Xoff = 23;
		
	RECT Rect;

	Rect.left   = m_Button[Index].Left;
	Rect.top    = m_Button[Index].Top;
	Rect.right  = m_Button[Index].Left + m_Button[Index].Width;
	Rect.bottom = m_Button[Index].Top + m_Button[Index].Height;

	if(m_Button[Index].Hover == TRUE && m_Button[Index].Enabled == TRUE) {

		if(m_Button[Index].State == TRUE) {

			DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);
			}
		else DC.DrawEdge(Rect, EDGE_RAISED, BF_RECT);
		}
	else {
		DC.FrameRect(Rect, afxBrush(3dFace));

		Rect.left   += 1;
		Rect.top    += 1;
		Rect.right  -= 1;
		Rect.bottom -= 1;
		
		DC.FrameRect(Rect, afxBrush(3dFace));
		}

	DrawTransparentBitmap(DC,Bitmap,(short)m_Button[Index].Left,
		              (short)m_Button[Index].Top,
			      (short)m_Button[Index].Width,
			      (short)m_Button[Index].Height,
			      Xoff,
			      RGB(128,128,0));
	}			

void CDIO14LogicWnd::DrawB_REDO(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"B_Redo",CSize(19, 19));
	
	if(m_RlistLimit > 0) m_Button[Index].Enabled = TRUE;

	else m_Button[Index].Enabled = FALSE;

	short Xoff;
	
	if(m_Button[Index].Enabled == TRUE) Xoff = 0;

	else Xoff = 23;
		
	RECT Rect;

	Rect.left   = m_Button[Index].Left;
	Rect.top    = m_Button[Index].Top;
	Rect.right  = m_Button[Index].Left + m_Button[Index].Width;
	Rect.bottom = m_Button[Index].Top + m_Button[Index].Height;

	if(m_Button[Index].Hover == TRUE && m_Button[Index].Enabled == TRUE) {

		if(m_Button[Index].State == TRUE) {

			DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);
			}
		else DC.DrawEdge(Rect, EDGE_RAISED, BF_RECT);
		}
	else {
		DC.FrameRect(Rect, afxBrush(3dFace));

		Rect.left   += 1;
		Rect.top    += 1;
		Rect.right  -= 1;
		Rect.bottom -= 1;
		
		DC.FrameRect(Rect, afxBrush(3dFace));
		}

	
	
	DrawTransparentBitmap(DC,Bitmap,(short)m_Button[Index].Left,
		              (short)m_Button[Index].Top,
			      (short)m_Button[Index].Width,
			      (short)m_Button[Index].Height,
			      Xoff,
			      RGB(128,128,0));
	}

void CDIO14LogicWnd::DrawB_REFRESH(UINT Index, CDC &DC)
{
	CBitmap Bitmap;
	
	Bitmap.Create(L"B_Refresh",CSize(19, 19));
	
	m_Button[Index].Enabled = TRUE;

	short Xoff;
	
	if(m_Button[Index].Hover == TRUE) Xoff = 0;

	else Xoff = 23;
		
	RECT Rect;

	Rect.left   = m_Button[Index].Left;
	Rect.top    = m_Button[Index].Top;
	Rect.right  = m_Button[Index].Left + m_Button[Index].Width;
	Rect.bottom = m_Button[Index].Top + m_Button[Index].Height;
	
	if(m_Button[Index].Hover == TRUE && m_Button[Index].Enabled == TRUE) {

		if(m_Button[Index].State == TRUE) {

			DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);
			}
		else DC.DrawEdge(Rect, EDGE_RAISED, BF_RECT);
		}
	else {
		DC.FrameRect(Rect, afxBrush(3dFace));

		Rect.left   += 1;
		Rect.top    += 1;
		Rect.right  -= 1;
		Rect.bottom -= 1;
		
		DC.FrameRect(Rect, afxBrush(3dFace));
		}

	DrawTransparentBitmap(DC,Bitmap,(short)m_Button[Index].Left,
		              (short)m_Button[Index].Top,
			      (short)m_Button[Index].Width,
			      (short)m_Button[Index].Height,
			      Xoff,
			      RGB(128,128,0));
	}

void CDIO14LogicWnd::DrawInputBoxes(CDC &DC)	
{
	TCHAR str[80];
	
	DC.Save();	

	CRect Rect;
		
	DC.SetBkMode(TRANSPARENT);

	CPen Pen1(CColor(0x88, 0x88, 0x88));
		
	DC.SetTextColor(0x00C0C0C0);
	
	for(UINT inst = INP0; inst <= INP7; inst++) {
						
		Rect.left   = m_pItem->m_Sym[inst].Left;
		Rect.top    = m_pItem->m_Sym[inst].Top;
		Rect.right  = m_pItem->m_Sym[inst].Left + m_pItem->m_Sym[inst].Width-10;
		Rect.bottom = m_pItem->m_Sym[inst].Top  + m_pItem->m_Sym[inst].Height-2;
		
		DC.FillRect(Rect, afxBrush(3dFace));
		
		DC.Select(Pen1);
		
		DC.MoveTo(Rect.right,Rect.top+5);
		
		DC.LineTo(Rect.right+11,Rect.top+5);

		DC.Deselect();
		
		Rect.left   += 1;
		Rect.top    += 1;
		Rect.right  -= 1;
		Rect.bottom -= 1;

		CColor Color;
		
		if(m_pItem->m_InputState[inst-INP0] == 0) {

			Color = INPUT_RED;

			CBrush Brush(Color);
			}
		else {
			Color = INPUT_GREEN;

			CBrush Brush(Color);
			}
			
		DC.FillRect(Rect, Color);
		
		witoa(inst-INP0+1,str,10);
		
		DC.Select(afxFont(Dialog));
		
		DC.TextOut(4,Rect.top-3, str);
		
		DC.Deselect();
		}

	DC.Restore();
	}
	
void CDIO14LogicWnd::DrawOutputBoxes(BOOL mode,CDC &DC)	
{
	TCHAR str[80];

	DC.Save();	

	CRect Rect;
		
	DC.SetBkMode(TRANSPARENT);

	CPen Pen1(CColor(0x88, 0x88, 0x88));
		
	DC.SetTextColor(0x00C0C0C0);
	
	BYTE OutputState = 0;

	BYTE Index = 0x01;

	for(UINT inst = OUT0; inst <= OUT5; inst++) {

		CColor Color;
				
		if(m_pItem->m_EResult[inst] == 0) {

			Color = INPUT_RED;
			
			CBrush Brush(Color);
			
			OutputState |= Index;
			
			}
		else {
			Color = INPUT_GREEN;
				
			CBrush Brush(Color);
			}			
		
		//Was there a change?
		if(mode == FALSE) {

			if((m_OutputStateOld & Index) == (OutputState & Index)) {
				
				Index = (BYTE)(Index << 1);

				continue;
				}
			}
		
		DrawUserSymbols(DC);
		
		Index = (BYTE)(Index << 1);
				
		Rect.left   = m_pItem->m_Sym[inst].Left+10;
		Rect.top    = m_pItem->m_Sym[inst].Top;
		Rect.right  = m_pItem->m_Sym[inst].Left + m_pItem->m_Sym[inst].Width;
		Rect.bottom = m_pItem->m_Sym[inst].Top  + m_pItem->m_Sym[inst].Height;
		
		DC.FillRect(Rect, afxBrush(3dFace));
		
		DC.Select(Pen1);
		
		DC.MoveTo(Rect.left,Rect.top+5);
		
		DC.LineTo(Rect.left-11,Rect.top+5);

		DC.Deselect();
		
		Rect.left   += 1;
		Rect.top    += 1;
		Rect.right  -= 1;
		Rect.bottom -= 1;
				
		DC.FillRect(Rect, Color);
		
		witoa(inst-OUT0+1,str,10);
		
		DC.Select(afxFont(Dialog));
		
		DC.TextOut(553,Rect.top-3, str);
		
		DC.Deselect();
		}

	m_OutputStateOld = OutputState;

	DC.Restore();
	}

void CDIO14LogicWnd::DrawGraphicsAreaText(CDC &DC)
{
	TCHAR str[80];
	
	DC.Save();

	CRect Rect = GetClientRect();	
			
	DC.SetBkMode(TRANSPARENT);
	
	DC.Select(afxFont(Dialog));
	
	DC.SetTextColor(0x00E0E0E0);

	wstrcpy(str, CString(IDS_MODULE_INPUTS));
	
	DC.TextOut(4,90, str);

	wstrcpy(str, CString(IDS_MODULE_OUTPUTS));
	
	DC.TextOut(518,90, str);

	DC.SetTextColor(0x00000000);

	wstrcpy(str, CString(IDS_DIO_SYMBOL_SEL));
	
	DC.TextOut(2,Rect.top+4, str);
	
	DC.Deselect();

	DC.Restore();
	}

void CDIO14LogicWnd::DrawAllWires(CDC &DC)
{
	for(UINT i = 1; i < m_pItem->m_NumWires; i++) {

		DrawWire(i,0,0,0,0,DDRAW,DC);
		}
	}

void CDIO14LogicWnd::DrawUserSymbols(CDC &DC)
{
	CBitmap Bitmap;

	CPoint pt;
	
	for(UINT i = START_USER_SYM; i < m_pItem->m_NSI; i++) {
				
		pt.x = m_pItem->m_Sym[i].Left;
		
		pt.y = m_pItem->m_Sym[i].Top;

		m_Oldx = pt.x;

		m_Oldy = pt.y;
		
		m_pItem->m_Sym[i].BMPOffsetX = 0;
		
		DrawSymbol(i,pt,SPLACE,DC);		
		}
	}

void CDIO14LogicWnd::DrawSymbolText(UINT type,UINT inst,UINT x,UINT y,BOOL mode,CDC &DC)
{
	DC.Save();
	
	DC.SetBkMode(TRANSPARENT);
	
	DC.Select(afxFont(Dialog));
	
	DC.SetTextColor(0x00C0C0C0);
	
	TCHAR str[20];
	
	TCHAR str1[20];

	x += 2;

	y += 1;
		
	UINT theight = 0;
		
	if(type == TIMER_START) {
		
		UINT val = m_pItem->m_Sym[inst].Prop[1]*256 + m_pItem->m_Sym[inst].Prop[2];
		
		witoa(val,str,10);
		
		UINT len = wstrlen(str);
		
		str[len] = str[len-1];

		str[len-1] = '.';

		str[len+1] = '\0';
	
		if(val < 10 && mode == TRUE) {

			TCHAR strx[80];

			wstrcpy(strx,str);

			wstrcpy(str,L"0");

			wstrcat(str,strx);
			}
		
		if(mode == TRUE) DC.TextOut(x,y-14,str);
		
		theight = 11;
		}

	if(type == COUNTER_START) {
	
		UINT val = m_pItem->m_Sym[inst].Prop[1]*256 + m_pItem->m_Sym[inst].Prop[2];
		
		witoa(val,str,10);

		val = m_pItem->m_Sym[inst].Prop[3]*256 + m_pItem->m_Sym[inst].Prop[4];
		
		witoa(val,str1,10);
		
		if(mode == TRUE) {
						
			DC.TextOut(x,y-14,str);
			
			DC.TextOut(x,y+m_pItem->m_Sym[inst].Height,str1);
			}

		theight = 12;
		}
	
	if(mode == FALSE) {

		CRect Rect;

		Rect.left   = x;
		Rect.top    = y-15;
		Rect.right  = x+40;
		Rect.bottom = y-1;
		
		DC.FillRect(Rect, afxBrush(BLACK));

		switch(type) {

			case COUNTER_START:
			
			Rect.left   = x;
			Rect.top    = y+m_pItem->m_Sym[inst].Height;
			Rect.right  = x+40;
			Rect.bottom = y+m_pItem->m_Sym[inst].Height+theight;
			
			DC.FillRect(Rect, afxBrush(BLACK));
			}
		}
	
	DC.Deselect();

	DC.Restore();
	}

void CDIO14LogicWnd::EraseLogicScreen(CDC &DC)
{
	RECT Rect;
	
	Rect.left   = 0;
	Rect.top    = LOGIC_WIN_TOP;
	Rect.right  = LOGIC_WIN_RIGHT;
	Rect.bottom = LOGIC_WIN_BOTTOM;
	
	DC.FillRect(Rect, afxBrush(BLACK));
	}

void CDIO14LogicWnd::RedrawLogicScreen(CDC &DC)
{
	DrawInputBoxes(DC);
	
	DrawOutputBoxes(TRUE,DC);

	DrawGraphicsAreaText(DC);
	
	DrawUserSymbols(DC);

	DrawAllWires(DC);
	}
	
void CDIO14LogicWnd::RedrawButtons(CDC &DC)
{
	DrawB_UNDO(B_UNDO,DC);

	DrawB_REDO(B_REDO,DC);

	DrawB_REFRESH(B_REFRESH,DC);

	RECT Rect;

	Rect.left   = m_Button[B_REDO].Left + m_Button[B_REDO].Width;
	Rect.top    = m_Button[B_REDO].Top;
	Rect.right  = Rect.left;
	Rect.bottom = Rect.top + 30;
		
	DC.Save();
				
	CColor Color = CColor(255, 255, 255);

	CPen Pen1(Color);

	DC.Select(Pen1);

	DC.MoveTo(m_Button[B_REDO].Left + m_Button[B_REDO].Width+8,30);
	DC.LineTo(m_Button[B_REDO].Left + m_Button[B_REDO].Width+8,55);
	
	DC.Deselect();		

	Color = CColor(130, 130, 130);

	CPen Pen2(Color);

	DC.Select(Pen2);

	DC.MoveTo(m_Button[B_REDO].Left + m_Button[B_REDO].Width+9,30);
	DC.LineTo(m_Button[B_REDO].Left + m_Button[B_REDO].Width+9,55);
	
	DC.Deselect();		

	DC.Restore();	
	}
	
void CDIO14LogicWnd::DrawSymbol(UINT inst, CPoint Pos, BYTE mode,CDC &DC)
{
	/*------------------------------------------------
	Draw the current symbol and update its position.
	------------------------------------------------*/

	CBitmap Bitmap;	
	
	DWORD BMode = 0;
	
	int LMode = 0;
	
	UINT x1 = 0,y1 = 0;

	BOOL tmode = FALSE;

	DC.Save();
				
	CColor Color = CColor(0, 128, 128);

	CPen Pen1(Color);

	DC.Select(Pen1);

	if(m_pItem->m_Sym[inst].Type != JCT) m_pItem->m_Sym[inst].Height = GetHeight(inst);
		
	if(m_pItem->m_Sym[inst].Type == JCT)      Bitmap.Create(L"JCT", CSize(5, 5));
	if(m_pItem->m_Sym[inst].Type == AND)      Bitmap.Create(L"AND_Gate", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == OR)       Bitmap.Create(L"OR_Gate", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == XOR)      Bitmap.Create(L"XOR_Gate", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == NOT)      Bitmap.Create(L"NOT_Gate", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == TIMERUP)  Bitmap.Create(L"Timerup", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == TIMERDN)  Bitmap.Create(L"Timerdn", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == COUNTUP)  Bitmap.Create(L"Countup", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == COUNTDN)  Bitmap.Create(L"Countdn", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == LAT)      Bitmap.Create(L"Latch", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == INC)      Bitmap.Create(L"Incoil", CSize(19, 19));
	if(m_pItem->m_Sym[inst].Type == OUTC)     Bitmap.Create(L"Outcoil", CSize(19, 19));

	for(BYTE i = 0; i < 2; i++) {

		if(i == 0) {
						
			x1 = m_Oldx;
			     
			y1 = m_Oldy;

			if(mode == SPREMOVE) {

				BMode = BLACKNESS;

				LMode = R2_BLACK;
				}
			
			if(mode == SMOVE) {

				BMode = SRCINVERT;

				LMode = R2_NOTXORPEN;
				}

			if(mode == SXOR) {

				BMode = SRCINVERT;

				LMode = R2_NOTXORPEN;
				
				i = 3;
				}

			if(mode == SPLACE) {

				BMode = SRCCOPY;

				LMode = R2_COPYPEN;
			
				tmode = TRUE;

				i = 3;
				}
			
			if(mode == SERASE) {

				BMode = BLACKNESS;

				LMode = R2_BLACK;
				
				i = 3;
				}
			}
		
		if(i == 1) {

			BMode = SRCINVERT;
			
			LMode = R2_NOTXORPEN;
			
			m_pItem->m_Sym[inst].Left = Pos.x;
			
			m_pItem->m_Sym[inst].Top = Pos.y;
			
			x1 = m_pItem->m_Sym[inst].Left;

			y1 = m_pItem->m_Sym[inst].Top;
			}

		DC.SetROP2(LMode);		
		
		if(m_pItem->m_Sym[inst].Type == JCT) {

			//Symbol body
			DC.BitBlt(x1,y1,m_pItem->m_Sym[inst].Width,m_pItem->m_Sym[inst].Height,
				  Bitmap,m_pItem->m_Sym[inst].BMPOffsetX,0, BMode);

			}
		else {	
			if(TestSymType(inst,TIMER_START,TIMER_STOP) && mode != SMOVE) {
			
				DrawSymbolText(TIMER_START,inst,x1,y1,FALSE,DC);
				
				DrawSymbolText(TIMER_START,inst,x1,y1,tmode,DC);
				}
			
			if(TestSymType(inst,COUNTER_START,COUNTER_STOP) && mode != SMOVE) {

				DrawSymbolText(COUNTER_START,inst,x1,y1,FALSE,DC);

				DrawSymbolText(COUNTER_START,inst,x1,y1,tmode,DC);
				}

			//Symbol bitmap body
			DC.BitBlt(x1+(m_pItem->m_Sym[inst].Width-15)/2 - 1,
				  y1+(m_pItem->m_Sym[inst].Height-15)/2 - 5,
				  19,27, Bitmap, m_pItem->m_Sym[inst].BMPOffsetX,0, BMode);

			//Rectangle
			DC.MoveTo(x1,y1);
			DC.LineTo(x1+m_pItem->m_Sym[inst].Width,y1);
			DC.LineTo(x1+m_pItem->m_Sym[inst].Width,y1+m_pItem->m_Sym[inst].Height);
			DC.LineTo(x1,y1+m_pItem->m_Sym[inst].Height);
			DC.LineTo(x1,y1);
						
			//Output pin
			if(m_pItem->m_Sym[inst].NumOutputs > 0) {

				DC.MoveTo(x1+m_pItem->m_Sym[inst].Width,
					  y1+(m_pItem->m_Sym[inst].Height)/2 - 1);
				
				DC.LineTo(x1+m_pItem->m_Sym[inst].Width+10,
					  y1+(m_pItem->m_Sym[inst].Height)/2 - 1);
				}
			
			//Input pins
			for(UINT j = 0; j < m_pItem->m_Sym[inst].NumInputs; j++) { 

				if(m_pItem->m_Sym[inst].NumInputs == 1) {

					DC.MoveTo(x1,y1+(m_pItem->m_Sym[inst].Height)/2 - 1);
				
					DC.LineTo(x1-10,y1+(m_pItem->m_Sym[inst].Height)/2 - 1);  
					}
				else {

					DC.MoveTo(x1,y1+(m_pItem->m_Sym[inst].PinOffset+j*m_pItem->m_Sym[inst].PinSpace)-1);
					
					DC.LineTo(x1-10,y1+(m_pItem->m_Sym[inst].PinOffset+j*m_pItem->m_Sym[inst].PinSpace)-1);  
					}
				}
			}
		}

	m_Oldx = Pos.x;

	m_Oldy = Pos.y;

	DC.Deselect();		

	DC.Restore();	
	}

void CDIO14LogicWnd::DrawWire(UINT inst,UINT x1,UINT y1,UINT x2,UINT y2,BYTE draw,CDC &DC)
{
	INT dist;

	INT Ext1 = 0;
	
	INT Ext2 = 0;

	UINT Yoff = 0;

	UINT DrawMode = WM_Z;

	if(m_pItem->m_Wire[inst].DrawMode == WM_PLACE) DrawMode = WM_PLACE;
	
	DC.Save();
	
	CColor Color;

	Color = CColor(0x88, 0x88, 0x88);
	
	if(draw == DERASE) {
		
		Color = CColor(0x00, 0x00, 0x00);
		}
	
	if(draw == DSELECT1) {
		
		Color = CColor(0x00, 0x88, 0x00);
		}
		
	if(draw == DSELECT2) {
		
		Color = CColor(0x50, 0x50, 0xFF);
		}

	CPen Pen1(Color);

	DC.Select(Pen1);
		
	if(draw == DNORM || draw == DXOR) DC.SetROP2(R2_NOTXORPEN);
	
	else DC.SetROP2(R2_COPYPEN);
	
	if(m_pItem->m_Wire[inst].Pin1 == 0 && DrawMode != WM_PLACE) {

		if(m_pItem->m_Wire[inst].x1 > m_pItem->m_Wire[inst].x2) {
			
			DrawMode = WM_U;

			Ext1 = UEXT;
			Ext2 = UEXT;
			}
		}
		
	if(m_pItem->m_Wire[inst].Pin2 == 0 && DrawMode != WM_PLACE) {

		if(m_pItem->m_Wire[inst].x2 > m_pItem->m_Wire[inst].x1) {
			
			DrawMode = WM_U;

			Ext1 = -UEXT;
			Ext2 = -UEXT;
			}
		}
	
	if(DrawMode == WM_U) {

		UINT sym1 = m_pItem->m_Wire[inst].Sym1;
		
		UINT sym2 = m_pItem->m_Wire[inst].Sym2;
		
		if(m_pItem->m_Sym[sym1].Type == JCT) Ext1 = 0;
		
		if(m_pItem->m_Sym[sym2].Type == JCT) Ext2 = 0;

		UINT H1 = GetHeight(sym1);

		UINT H2 = GetHeight(sym2);
				
		if(m_pItem->m_Sym[sym1].Top+20+H1 > m_pItem->m_Sym[sym2].Top+20+H2) {
			
			Yoff = m_pItem->m_Sym[sym1].Top + 20 + H1;
			}
		else {
			Yoff = m_pItem->m_Sym[sym2].Top + 20 + H2;
			}
		}
	
	dist = CalcVertSegPos(inst,m_pItem->m_Wire[inst].Frac);
		
	if(DrawMode != WM_PLACE) m_pItem->m_Wire[inst].DrawMode = DrawMode;
	
	if(draw == DSELECT2) {
				
		DC.MoveTo(m_pItem->m_Wire[inst].x1+dist,m_pItem->m_Wire[inst].y1);
		DC.LineTo(m_pItem->m_Wire[inst].x1+dist,m_pItem->m_Wire[inst].y2);
		}
	else {

		if(DrawMode == WM_Z) {

			//Draw in original position
			DC.MoveTo(m_pItem->m_Wire[inst].x1,m_pItem->m_Wire[inst].y1);
			DC.LineTo(m_pItem->m_Wire[inst].x1+dist,m_pItem->m_Wire[inst].y1);
			DC.LineTo(m_pItem->m_Wire[inst].x1+dist,m_pItem->m_Wire[inst].y2);
			DC.LineTo(m_pItem->m_Wire[inst].x2,m_pItem->m_Wire[inst].y2);
			}

		if(DrawMode == WM_U) {

			DC.MoveTo(m_pItem->m_Wire[inst].x1,m_pItem->m_Wire[inst].y1);
			DC.LineTo(m_pItem->m_Wire[inst].x1+Ext1,m_pItem->m_Wire[inst].y1);
			DC.LineTo(m_pItem->m_Wire[inst].x1+Ext1,Yoff);
			DC.LineTo(m_pItem->m_Wire[inst].x2-Ext2,Yoff);
			DC.LineTo(m_pItem->m_Wire[inst].x2-Ext2,m_pItem->m_Wire[inst].y2);
			DC.LineTo(m_pItem->m_Wire[inst].x2,m_pItem->m_Wire[inst].y2);
			}

		if(DrawMode == WM_PLACE) {

			DC.MoveTo(m_pItem->m_Wire[inst].x1,m_pItem->m_Wire[inst].y1);
			DC.LineTo(m_pItem->m_Wire[inst].x2,m_pItem->m_Wire[inst].y2);
			}
		}
	
	if(draw == DNORM) {

		//First endpoint
		m_pItem->m_Wire[inst].x1 = x1;
		m_pItem->m_Wire[inst].y1 = y1;
					
		//Second endpoint
		m_pItem->m_Wire[inst].x2 = x2;
		m_pItem->m_Wire[inst].y2 = y2;
			
		m_pItem->m_Wire[inst].Frac = m_pItem->m_Wire[inst].Nfrac;

		dist = CalcVertSegPos(inst,m_pItem->m_Wire[inst].Nfrac);
		
		if(DrawMode == WM_Z) {

			DC.MoveTo(x1,y1);
			DC.LineTo(x1+dist,y1);
			DC.LineTo(x1+dist,y2);
			DC.LineTo(x2,y2);
			}

		if(DrawMode == WM_U) {

			DC.MoveTo(x1,y1);
			DC.LineTo(x1+Ext1,y1);
			DC.LineTo(x1+Ext1,Yoff);
			DC.LineTo(x2-Ext2,Yoff);
			DC.LineTo(x2-Ext2,y2);
			DC.LineTo(x2,y2);
			}

		if(DrawMode == WM_PLACE) {

			DC.MoveTo(x1,y1);
			DC.LineTo(x2,y2);
			}
		}
		
	DC.Deselect();

	DC.Restore();
	}

INT CDIO14LogicWnd::CalcVertSegPos(UINT inst,double frac)
{
	INT dist1;

	INT sign = 1;
	
	double dist,/*r,*/n;
		
	if(m_pItem->m_Wire[inst].x2 >= m_pItem->m_Wire[inst].x1) {
	
		dist = (double)(m_pItem->m_Wire[inst].x2-m_pItem->m_Wire[inst].x1);
		
		sign = 1;
		}
	else {
		dist = (double)(m_pItem->m_Wire[inst].x1-m_pItem->m_Wire[inst].x2);

		sign = -1;
		}
		
	dist = dist*frac;

	/*r =*/ modf(dist / GRID_SIZE,&n);
	
	dist1 = sign*(WORD)(n*GRID_SIZE);
		
	if(frac == 0.0) dist1 = 0;

	if(frac == 1.0) dist1 = m_pItem->m_Wire[inst].x2-m_pItem->m_Wire[inst].x1;

	
	return dist1;
	}

void CDIO14LogicWnd::DrawPinSelect(UINT x,UINT y,BOOL mode,CDC &DC)
{
	/*----------------------------------
	Draw the pin selection target.
	----------------------------------*/

	CColor Color;
	
	if(mode == TRUE && m_PinSelActive == FALSE) {
		
		m_PinSelActive = TRUE;
		
		Color = CColor(0x88, 0x88, 0x88);
		}
	else {
		if(mode == FALSE && m_PinSelActive == TRUE) {
				
			m_PinSelActive = FALSE;
			
			Color = CColor(0x00, 0x00, 0x00);
			}
		else return;
		}
		
	CPen Pen1(Color);

	DC.Select(Pen1);
		
	//Select Box
	DC.MoveTo(x-3,y-3);		
	DC.LineTo(x+3,y-3);
	DC.LineTo(x+3,y+3);
	DC.LineTo(x-3,y+3);
	DC.LineTo(x-3,y-3);

	DC.Deselect();
	}

void CDIO14LogicWnd::DrawWireSelect(UINT inst,BOOL mode,BYTE color,CDC &DC)
{
	if(mode == TRUE) {
		
		if(color == 0) DrawWire(inst,0,0,0,0,DSELECT1,DC);
			
		else DrawWire(inst,0,0,0,0,DSELECT2,DC);
		}
	else {
		DrawWire(inst,0,0,0,0,DDRAW,DC);
		}
	}

void CDIO14LogicWnd::DrawTransparentBitmap(HDC hdc, HBITMAP hBitmap, short xStart,
                           short yStart,short Width,short Height,
			   short Xoffset,COLORREF cTransparentColor)
{
	/*-------------------------------------------------
	Lifted from a Studio help file :) Modified to take
	an indexed X offset - JAV.
	-------------------------------------------------*/
	
	BITMAP     bm;
	COLORREF   cColor;
	HBITMAP    bmAndBack, bmAndObject, bmAndMem, bmSave;
	HBITMAP    bmBackOld, bmObjectOld, bmMemOld, bmSaveOld;
	HDC        hdcMem, hdcBack, hdcObject, hdcTemp, hdcSave;
	POINT      ptSize;

	hdcTemp = CreateCompatibleDC(hdc);
	
	// Select the bitmap
	SelectObject(hdcTemp, hBitmap);

	::GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);
	
        // Set width of bitmap
	ptSize.x = Width;
	
        // Set height of bitmap
	ptSize.y = Height;
	
	// Convert from device to logical points
	DPtoLP(hdcTemp, &ptSize, 1); 

	// Create some DCs to hold temporary data.
	hdcBack   = CreateCompatibleDC(hdc);
	hdcObject = CreateCompatibleDC(hdc);
	hdcMem    = CreateCompatibleDC(hdc);
	hdcSave   = CreateCompatibleDC(hdc);

	// Create a bitmap for each DC. DCs are required for a number of
	// GDI functions.

	// Monochrome DC
	bmAndBack   = CreateBitmap(ptSize.x, ptSize.y, 1, 1, NULL);

	// Monochrome DC
	bmAndObject = CreateBitmap(ptSize.x, ptSize.y, 1, 1, NULL);

	bmAndMem    = CreateCompatibleBitmap(hdc, ptSize.x, ptSize.y);
	bmSave      = CreateCompatibleBitmap(hdc, ptSize.x, ptSize.y);

	// Each DC must select a bitmap object to store pixel data.
	bmBackOld   = SelectObject(hdcBack, bmAndBack);
	bmObjectOld = SelectObject(hdcObject, bmAndObject);
	bmMemOld    = SelectObject(hdcMem, bmAndMem);
	bmSaveOld   = SelectObject(hdcSave, bmSave);

	// Set proper mapping mode.
	SetMapMode(hdcTemp, GetMapMode(hdc));

	// Get the required x-offset bitmap section.
	BitBlt(hdcTemp, 0, 0, ptSize.x, ptSize.y, hdcTemp, Xoffset, 0, SRCCOPY);

	// Save the bitmap sent here, because it will be overwritten.
	BitBlt(hdcSave, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);
		
	// Set the background color of the source DC to the color.
	// contained in the parts of the bitmap that should be transparent
	cColor = SetBkColor(hdcTemp, cTransparentColor);

	// Create the object mask for the bitmap by performing a BitBlt
	// from the source bitmap to a monochrome bitmap.
	BitBlt(hdcObject, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);

	// Set the background color of the source DC back to the original
	// color.
	SetBkColor(hdcTemp, cColor);

	// Create the inverse of the object mask.
	BitBlt(hdcBack, 0, 0, ptSize.x, ptSize.y, hdcObject, 0, 0,
	  NOTSRCCOPY);

	// Copy the background of the main DC to the destination.
	BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdc, xStart, yStart,
	  SRCCOPY);

	// Mask out the places where the bitmap will be placed.
	BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcObject, 0, 0, SRCAND);

	// Mask out the transparent colored pixels on the bitmap.
	BitBlt(hdcTemp, 0, 0, ptSize.x, ptSize.y, hdcBack, 0, 0, SRCAND);

	// XOR the bitmap with the background on the destination DC.
	BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCPAINT);

	// Copy the destination to the screen.
	BitBlt(hdc, xStart, yStart, ptSize.x, ptSize.y, hdcMem, 0, 0,SRCCOPY);
	
	// Place the original bitmap back into the bitmap sent here.
	BitBlt(hdcTemp, Xoffset, 0, ptSize.x, ptSize.y, hdcSave, 0, 0, SRCCOPY);
	
	// Delete the memory bitmaps.
	DeleteObject(SelectObject(hdcBack, bmBackOld));
	DeleteObject(SelectObject(hdcObject, bmObjectOld));
	DeleteObject(SelectObject(hdcMem, bmMemOld));
	DeleteObject(SelectObject(hdcSave, bmSaveOld));

	// Delete the memory DCs.
	DeleteDC(hdcMem);
	DeleteDC(hdcBack);
	DeleteDC(hdcObject);
	DeleteDC(hdcSave);
	DeleteDC(hdcTemp);
	} 
//End of File