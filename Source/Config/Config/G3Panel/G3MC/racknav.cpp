
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRackNavTreeWnd;
class CNewModuleDialog;

//////////////////////////////////////////////////////////////////////////
//
// Backplane Navigation Window
//

class CRackNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CRackNavTreeWnd(void);

		// Overridables
		void OnAttach(void);

	protected:
		// Data
		CCommsSystem * m_pSystem;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Notification Handlers
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);
		
		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		void OnItemNew(void);
		BOOL OnItemRename(CString Name);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		BOOL IncludeItem(CMetaItem *pItem);
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void OnItemRenamed(CItem *pItem);
		void OnItemDeleted(CItem *pItem, BOOL fExec);
		void NewItemSelected(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Fixed Backplane Navigation Window
//

class CFixedRackNavTreeWnd : public CRackNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CFixedRackNavTreeWnd(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Overridables
		void OnAttach(void);

	protected:
		// Data
		CExpansionItem   * m_pExpansion;
		CCommsDeviceList * m_pDevices;
		HTREEITEM          m_hDropMove;

		// Overridables
		void OnExec(CCmd *pCmd);
		void OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);

		// Notification Handlers
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeItemRename(CString Name);

		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Item Menu : Create
		void OnItemCreate(void);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);

		// Item Menu : Delete 
		void OnItemDelete(UINT uVerb, BOOL fMove);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);
		void PerformDelete(CCommsPortSlot *pSlot);

		// Edit Menu : Cut
		void OnEditCut(void);

		// Edit Menu : Paste
		void OnExecPaste(CCmdPaste *pCmd);
		void OnUndoPaste(CCmdPaste *pCmd);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
		void AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem);

		// Data Object Acceptance
		BOOL AcceptStream(ITextStream &Stream, CAcceptCtx const &Ctx, BOOL fLocal);

		// Drag Hooks
		DWORD FindDragAllowed(void);
		
		// Drag Support
		BOOL DropDone(IDataObject *pData);
		BOOL IsValidDrop(void);
		
		// Tree Loading
		void ListUpdate(CCommsPortSlot *pSlot);		
		void LoadTree(void);

		// Item Hooks
		UINT    GetRootImage(void);
		UINT    GetItemImage(CMetaItem *pItem);
		CString GetItemText(CMetaItem *pItem);
		void    OnItemRenamed(CItem *pItem);
		void    NewItemSelected(void);

		// Implementation
		BOOL IsFitted(void);
		BOOL IsEmpty(void);
		BOOL IsRack(void);
		BOOL CanAddRack(void);
		BOOL CanDelRack(void);
		void OnListUpdated(void);
		void UpdateStatus(void);
		void MakeUnique(CString &Name);
		void RefreshTree(void);
		void RefreshSlots(void);
		void ExpandAll(void);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// New Module Dialog
//

class CNewModuleDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CNewModuleDialog(BOOL fGraphite);
		
		// Attributes
		CLASS GetClass(void) const;

	protected:
		// Model Definition
		struct CModel
		{
			PCTXT m_pName;
			PCTXT m_pClass;
			UINT  m_uDesc;
			};

		// Static Data Members
		static CLASS m_Class;

		// Data Members
		BOOL m_fGraphite;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		void OnDblClk(UINT uID, CWnd &Wnd);

		// Implementation
		void LoadModels(CListBox &List);
		void LoadModels(CListBox &List, CModel *pType, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Navigation Window
//
	
// Dynamic Class

AfxImplementDynamicClass(CRackNavTreeWnd, CNavTreeWnd);
		
// Constructor

CRackNavTreeWnd::CRackNavTreeWnd(void) : CNavTreeWnd( L"Modules",
						      NULL,
						      AfxRuntimeClass(CCommsDeviceRack)
						      )
{
	}

// Overridables

void CRackNavTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	CCommsPortRack *pPort = (CCommsPortRack *) m_pItem;

	m_pSystem = (CCommsSystem  *) m_pItem->GetDatabase()->GetSystemItem();

	m_pList   = pPort->m_pDevices;
	}

// Message Map

AfxMessageMap(CRackNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_ENDLABELEDIT, OnTreeEndEdit)

	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)
	AfxDispatchControlType(IDM_EDIT,  OnEditControl)

	AfxMessageEnd(CRackNavTreeWnd)
	};

// Message Handlers

void CRackNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"RackNavTreeTool"));
		}
	}

// Notification Handlers

BOOL CRackNavTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{
	if( Info.item.pszText && *Info.item.pszText ) {

		OnItemRename(Info.item.pszText);
		}
	
	return FALSE;
	}

// Command Handlers

BOOL CRackNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Info.m_Image   = MAKELONG(0x0012, 0x4000);

			Info.m_ToolTip = CString(IDS_NEW_MODULE);

			Info.m_Prompt  = CString(IDS_ADD_NEW_MODULE_TO);

			return TRUE;

		case IDM_ITEM_USAGE:

			Info.m_Image = MAKELONG(0x000C, 0x1000);

			return FALSE;
		}

	return FALSE;
	}

BOOL CRackNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Src.EnableItem(!IsReadOnly() && m_pList->GetItemCount() <= 16);

			return TRUE;

		case IDM_ITEM_USAGE:

			Src.EnableItem(!IsRootSelected());

			return TRUE;
		}

	return FALSE;
	}

BOOL CRackNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			OnItemNew();

			return TRUE;

		case IDM_ITEM_USAGE:

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

				m_pSystem->FindDeviceUsage((CCommsDevice *) m_pSelect);
				}

			return TRUE;
		}

	return FALSE;
	}

BOOL CRackNavTreeWnd::OnEditControl(UINT uID, CCmdSource &Src) {

	switch( uID ) {

		case IDM_EDIT_PASTE:

			Src.EnableItem(!IsReadOnly() && CanEditPaste() && m_pList->GetItemCount() <= 16);

			return TRUE;
		}

	return FALSE;
	}

void CRackNavTreeWnd::OnItemNew(void)
{
	for( UINT n = 1;; n++ ) {

		CPrintf Name(CString(IDS_SEND_MODULE), n);

		if( !m_MapNames[Name] ) {

			CNewModuleDialog Dlg(FALSE);

			if( Dlg.Execute() ) {

				CCommsDeviceRack *pDev = New CCommsDeviceRack;

				AddToList(m_hRoot, m_hSelect, pDev);

				pDev->SetClass(Dlg.GetClass());

				pDev->SetName (Name);

				m_System.SaveCmd(New CCmdCreate(m_pSelect, pDev));

				HTREEITEM hItem = AddToTree(m_hRoot, m_hSelect, pDev);

				ExpandItem(m_hSelect);

				m_pTree->SelectItem(hItem);

				ListUpdated();
				}

			break;
			}
		}
	}

BOOL CRackNavTreeWnd::OnItemRename(CString Name)
{
	CString Prev = m_pNamed->GetName();

	if( wstrcmp(Name, Prev) ) {

		CItem *pItem = m_pSystem->m_pComms->FindDevice(Name);

		if( pItem ) {

			if( pItem != m_pSelect ) {

				CString Text = CFormat(CString(IDS_NAME_FMT_IS), Name);
					
				Error(Text);

				return FALSE;
				}
			}

		if( TRUE ) {
		
			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
				}
			}

		CCmd *pCmd = New CCmdRename(m_pNamed, Name);

		m_System.ExecCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

// Tree Loading

void CRackNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CommsTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

BOOL CRackNavTreeWnd::IncludeItem(CMetaItem *pItem)
{
	if( pItem->IsKindOf(AfxRuntimeClass(CCommsDeviceRack)) ) {

		CCommsDeviceRack *pDev = (CCommsDeviceRack *) pItem;

		if( pDev->m_Name == L"Master" ) {

			return FALSE;
			}
		}

	return pItem->HasName() && !pItem->GetName().IsEmpty();
	}

UINT CRackNavTreeWnd::GetRootImage(void)
{
	return 0x40;
	}

UINT CRackNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return 0x41;
	}

BOOL CRackNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"RackNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"RackNavTreeMissCtxMenu";

	return FALSE;
	}

void CRackNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem ) {

		m_pSystem->Validate(TRUE);
		}

	CNavTreeWnd::OnItemRenamed(pItem);
	}

void CRackNavTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
	if( pItem ) {

		if( pItem->IsKindOf(m_Class) ) {
		
			m_pSystem->Validate(TRUE);
			}
		}
	}

void CRackNavTreeWnd::NewItemSelected(void)
{
	CNavTreeWnd::NewItemSelected();

	if( !m_fLocked ) {

		if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsDeviceRack)) ) {

			CCommsDeviceRack *pDev = (CCommsDeviceRack *) m_pSelect;

			afxThread->SetStatusText(pDev ? CPrintf(L"Device Number %d", pDev->m_Number) : L"");
			}
		else {
			afxThread->SetStatusText(L"");
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Fixed Backplane Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CFixedRackNavTreeWnd, CRackNavTreeWnd);

// Constructor

CFixedRackNavTreeWnd::CFixedRackNavTreeWnd(void)
{
	}

// IUpdate

HRESULT CFixedRackNavTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

			RefreshTree();

			ExpandAll();
			
			return S_OK;
			}
		}

	if( uType == updateRename || uType == updateChildren ) {

		if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

			COptionCardItem *pOpt  = (COptionCardItem *) pItem;

			CCommsPortSlot  *pSlot = ((CCommsSlotList *) m_pList)->FindSlot(pOpt->m_Number);

			if( pSlot ) {

				HTREEITEM hItem = m_MapNames[pSlot->GetName()];

				if( hItem ) {

					m_pTree->SetItemText(hItem, pSlot->GetTreeLabel());
					}
				}

			return S_OK;
			}
		}

	return CRackNavTreeWnd::ItemUpdated(pItem, uType);
	}

// IDropTarget

HRESULT CFixedRackNavTreeWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	CanAcceptDataObject(pData, m_fDropLocal);

	*pEffect    = m_fDropLocal ? DROPEFFECT_LINK : DROPEFFECT_MOVE;

	m_uDrop     = dropOther;

	m_hDropRoot = NULL;

	m_hDropPrev = NULL;

	m_fDropMove = FALSE;

	m_hDropMove = m_hSelect;

	DropTrack(CPoint(pt.x, pt.y), TRUE);

	return S_OK;
	}

HRESULT CFixedRackNavTreeWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		DropTrack(CPoint(pt.x, pt.y), TRUE);

		if( IsValidDrop() ) {

			*pEffect = m_fDropLocal ? DROPEFFECT_LINK : DROPEFFECT_MOVE;
					
			return S_OK;
			}
		}
	
	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CFixedRackNavTreeWnd::DragLeave(void)
{
	return CStdTreeWnd::DragLeave();
	}

HRESULT CFixedRackNavTreeWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop ) {

		ShowDrop(FALSE);

		if( m_uDrop == dropOther ) {

			if( IsValidDrop() ) {

				m_fDropMove = (m_fDropLocal && !(dwKeys & MK_CONTROL)); 

				DropDone(pData);
			
				SetFocus();
				}
			}

		m_uDrop = dropNone;
	
		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Overridables

void CFixedRackNavTreeWnd::OnAttach(void)
{
	CRackNavTreeWnd::OnAttach();

	CCommsPortRack *pPort = (CCommsPortRack *) m_pItem;

	m_pExpansion = m_pSystem->m_pComms->m_pExpansion;
	
	m_pList      = pPort->m_pSlots;

	m_pDevices   = pPort->m_pDevices;

	pPort->RemapSlots(TRUE);
	}

void CFixedRackNavTreeWnd::OnExec(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnExecCreate((CCmdCreate *) pCmd);
		
		return;
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnExecDelete((CCmdDelete *) pCmd);
		
		return;
		}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnExecPaste((CCmdPaste *) pCmd);
		
		return;
		}

	CRackNavTreeWnd::OnExec(pCmd);
	}

void CFixedRackNavTreeWnd::OnUndo(CCmd *pCmd)
{
	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CCmdCreate) ) {

		OnUndoCreate((CCmdCreate *) pCmd);
		
		return;
		}

	if( Class == AfxRuntimeClass(CCmdDelete) ) {

		OnUndoDelete((CCmdDelete *) pCmd);
		
		return;
		}

	if( Class == AfxRuntimeClass(CCmdPaste) ) {

		OnUndoPaste((CCmdPaste *) pCmd);
		
		return;
		}

	CRackNavTreeWnd::OnUndo(pCmd);
	}

// Message Map

AfxMessageMap(CFixedRackNavTreeWnd, CRackNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SHOWWINDOW)

	AfxDispatchNotify(100, NM_CUSTOMDRAW,      OnTreeCustomDraw)
	AfxDispatchNotify(100, TVN_BEGINLABELEDIT, OnTreeBeginEdit )
	AfxDispatchNotify(100, TVN_ENDLABELEDIT,   OnTreeEndEdit   )

	AfxDispatchGetInfoType(IDM_ITEM, OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM, OnItemControl)
	AfxDispatchCommandType(IDM_ITEM, OnItemCommand)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CFixedRackNavTreeWnd)
	};

// Message Handlers

void CFixedRackNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"RackNavTreeToolGraphite"));
		}
	}

void CFixedRackNavTreeWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	CNavTreeWnd::OnShowWindow(fShow, uStatus);

	if( fShow ) {

		UpdateStatus();
		
		return;
		}

	afxThread->SetStatusText(L"");
	}

// Notification Handlers

UINT CFixedRackNavTreeWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		HTREEITEM hItem = HTREEITEM(Info.dwItemSpec);

		if( hItem ) {

			CItem *pItem = (CItem *) Info.lItemlParam;

			if( pItem ) {

				if( pItem->IsKindOf(AfxRuntimeClass(CCommsPortSlot)) ) {

					CCommsPortSlot *pSlot = (CCommsPortSlot *) pItem;

					if( pSlot->GetType() != typeNone && pSlot->GetType() != typeModule && pSlot->GetType() != typeRack ) {

						NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;
						
						Extra.clrText = afxColor(Disabled);
						}
					}
				}
			}
		}
	
	return CRackNavTreeWnd::OnTreeCustomDraw(uID, Info);
	}

BOOL CFixedRackNavTreeWnd::OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info)
{	
	if( !CRackNavTreeWnd::OnTreeBeginEdit(uID, Info) ) {

		if( IsFitted() ) {

			CString Text = Info.item.pszText;

			UINT   uFind = Text.Find('-');

			if( uFind != NOTHING ) {

				Text = Text.Mid(uFind + 2);

				m_pTree->GetEditControl().SetWindowTextW(Text);
				}

			return FALSE;
			}
		}
	
	return TRUE;
	}

BOOL CFixedRackNavTreeWnd::OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info)
{	
	if( Info.item.pszText && *Info.item.pszText ) {

		if( OnTreeItemRename(Info.item.pszText) ) {

			CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

			pSlot->UpdateName();

			CString Text = pSlot->GetTreeLabel();

			Text.Delete(Info.item.cchTextMax, NOTHING);
			
			wstrcpy(Info.item.pszText, Text);

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CFixedRackNavTreeWnd::OnTreeItemRename(CString Name)
{
	CString Prev = m_pNamed->GetName();
	
	if( wstrcmp(Name, Prev) ) {

		CItem *pItem  = m_pSystem->m_pComms->FindDevice(Name);

		CItem *pCheck = m_pSelect; 
		
		if( pItem ) {

			if( m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortSlot)) ) {

				pCheck = ((CCommsPortSlot *) m_pSelect)->GetDevice();
				}
			
			if( pItem != pCheck ) {

				CString Text = CFormat(CString(IDS_NAME_FMT_IS), Name);
					
				Error(Text);

				return FALSE;
				}
			}

		if( TRUE ) {
		
			CError Error(TRUE);

			if( !C3ValidateName(Error, Name) ) {

				Error.Show(ThisObject);

				return FALSE;
				}
			}

		CCmd *pCmd = New CCmdRename(m_pNamed, Name);

		m_System.ExecCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

// Command Handlers

BOOL CFixedRackNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Info.m_Image   = MAKELONG(0x0015, 0x4000);

			Info.m_ToolTip = CString(IDS_NEW_MODULE);

			Info.m_Prompt  = CString(IDS_ADD_NEW_MODULE_TO);

			return TRUE;

		case IDM_ITEM_NEW_RACK:

			Info.m_Image   = MAKELONG(0x0015, 0x4000);

			Info.m_ToolTip = CString(IDS_NEW_RACK);

			Info.m_Prompt  = CString(IDS_ADD_NEW_RACK_TO);

			return TRUE;
		}

	return CRackNavTreeWnd::OnItemGetInfo(uID, Info);
	}

BOOL CFixedRackNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:

			Src.EnableItem(!IsReadOnly() && !IsRootSelected() && IsEmpty());

			return TRUE;

		case IDM_ITEM_DELETE:

			Src.EnableItem(!IsSelLocked() && !IsRootSelected() && (IsFitted() || CanDelRack()) );

			return TRUE;

		case IDM_ITEM_NEW_RACK:

			Src.EnableItem(!IsSelLocked() && !IsRootSelected() && CanAddRack());

			return TRUE;

		case IDM_ITEM_RENAME:

			Src.EnableItem(!IsSelLocked() && !IsRootSelected() && IsFitted() && !IsRack());

			return TRUE;

		case IDM_ITEM_USAGE:

			Src.EnableItem(!IsSelLocked() && !IsRootSelected() && IsFitted() && !IsRack());

			return TRUE;
		}

	return CRackNavTreeWnd::OnItemControl(uID, Src);
	}

BOOL CFixedRackNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {
		
		case IDM_ITEM_NEW:
		case IDM_ITEM_NEW_RACK:

			OnItemCreate();

			return TRUE;

		case IDM_ITEM_DELETE:

			OnItemDelete(IDS_DELETE_1, FALSE);

			return TRUE;
		}

	return CRackNavTreeWnd::OnItemCommand(uID);
	}

BOOL CFixedRackNavTreeWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			Src.EnableItem(!IsSelLocked() && !IsItemLocked(m_hSelect) && (IsFitted() || CanDelRack()));

			return TRUE;

		case IDM_EDIT_CUT:

			Src.EnableItem(!m_fMulti && !IsSelLocked() && !IsItemLocked(m_hSelect) && IsFitted());

			return TRUE;

		case IDM_EDIT_COPY:

			Src.EnableItem(!m_fMulti && IsFitted());

			return TRUE;

		case IDM_EDIT_PASTE:

			Src.EnableItem(CanEditPaste() && IsEmpty());

			return TRUE;
		}

	return FALSE;
	}

BOOL CFixedRackNavTreeWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_DELETE:

			OnItemDelete(IDS_DELETE_1, FALSE);

			return TRUE;

		case IDM_EDIT_CUT:

			OnEditCut();

			return TRUE;
		}

	return CRackNavTreeWnd::OnEditCommand(uID);
	}

// Item Menu : Create

void CFixedRackNavTreeWnd::OnItemCreate(void)
{
	CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

	if( pSlot->GetType() == typeRack ) {

		COptionCardRackItem *pRack = pSlot->GetRack();

		if( pRack->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

			CCascadedRackItem *pRoot = (CCascadedRackItem *) pRack;

			CString Menu = CPrintf(IDS_ADD_F, m_pNamed->GetName());
				
			m_System.ExecCmd(New CCmdMulti(m_pSelect->GetFixedPath(), Menu, navLock));
			
			HGLOBAL hPrev = pRoot->TakeSnapshot();

			pRoot->AddRack();
	
			m_System.SaveCmd(New CCmdItem(Menu, pRoot, hPrev));

			RefreshTree();
			
			m_System.ExecCmd(New CCmdMulti(m_pSelect->GetFixedPath(), Menu, navLock));
			
			ExpandAll();
			}
		
		return;
		}
	
	for( UINT n = 1;; n++ ) {

		CPrintf Name(CString(IDS_SEND_MODULE), n);

		if( !m_pSystem->m_pComms->FindDevice(Name) ) {

			CNewModuleDialog Dlg(TRUE);

			if( Dlg.Execute() ) {

				CCommsDeviceRack *pDev = New CCommsDeviceRack;

				m_pDevices->AppendItem(pDev);

				pDev->SetClass(Dlg.GetClass());

				pDev->SetName(Name);

				pSlot->SetDevice(pDev);

				m_System.SaveCmd(New CCmdCreate(pSlot, pDev));

				ListUpdate(pSlot);

				CString Warning;

				if( pSlot->GetPower() && !m_pExpansion->CheckPower(Warning) ) {  
				
					Information(Warning);
					}
				}

			break;
			}
		}
	}

void CFixedRackNavTreeWnd::OnExecCreate(CCmdCreate *pCmd)
{
	CCommsDeviceRack *pDev  = New CCommsDeviceRack;

	CCommsPortSlot   *pSlot = (CCommsPortSlot *) m_pSelect;
	
	m_pDevices->AppendItem(pDev);

	pDev->LoadSnapshot(pCmd->m_hData);

	pDev->SetDirty();
	
	pSlot->SetDevice(pDev);

	ListUpdate(pSlot);
	}

void CFixedRackNavTreeWnd::OnUndoCreate(CCmdCreate *pCmd)
{
	HTREEITEM  hItem = m_MapFixed[pCmd->m_Item];

	CMetaItem *pItem = GetItemPtr(hItem);

	PerformDelete((CCommsPortSlot *) pItem);
	}

// Item Menu : Delete 

void CFixedRackNavTreeWnd::OnItemDelete(UINT uVerb, BOOL fMove)
{
	CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

	if( pSlot->GetType() == typeRack ) {

		COptionCardRackItem *pRack = pSlot->GetRack();

		if( pRack->HasType(classIO) && !pRack->HasType(classComms) ) {

			// TODO -- implement
			}

		if( pRack->HasType(classIO) || pRack->HasType(classComms) ) {

			CString Text = IDS_YOU_MUST_DELETE;

			afxMainWnd->Information(Text);

			return;
			}

		CCascadedRackItem   *pRoot = (CCascadedRackItem *) pRack->GetParent(2);

		CString              Menu  = CFormat(IDS_FORMAT, CString(uVerb), pRack->GetTreeLabel());
				
		m_System.ExecCmd(New CCmdMulti(pSlot->GetFixedPath(), Menu, navLock));

		HGLOBAL hPrev = pRoot->TakeSnapshot();

		pRoot->DelRack(pRack);
	
		m_System.SaveCmd(New CCmdItem(Menu, pRoot, hPrev));

		m_System.ExecCmd(New CCmdMulti(pSlot->GetFixedPath(), Menu, navLock));
						
		StepAway();

		RefreshTree();
	
		return;
		}

	IDataObject *pData = NULL;

	if( MakeDataObject(pData, FALSE) ) {

		CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

		CString         Item  = m_pSelect->GetFixedPath();

		CString         Menu  = CFormat(IDS_FORMAT, CString(uVerb), pSlot->GetDevice()->GetName());

		HTREEITEM       hRoot = m_pTree->GetParent (m_hSelect);

		CItem          *pRoot = GetItemPtr(hRoot);

		PerformDelete(pSlot);
		
		m_System.SaveCmd(New CCmdDelete(Menu, Item, pRoot, NULL, pData, fMove));
		}
	}

void CFixedRackNavTreeWnd::OnExecDelete(CCmdDelete *pCmd)
{
	PerformDelete((CCommsPortSlot *) m_pSelect);
	}

void CFixedRackNavTreeWnd::OnUndoDelete(CCmdDelete *pCmd)
{
	CRackNavTreeWnd::OnUndoDelete(pCmd);

	m_pSystem->Validate(FALSE);
	
	ListUpdate((CCommsPortSlot *) m_pSelect);
	}

void CFixedRackNavTreeWnd::PerformDelete(CCommsPortSlot *pSlot)
{
	m_pTree->EndEditLabelNow(TRUE);

	m_pDevices->DeleteItem(pSlot->GetDevice());

	m_pKilled = pSlot->GetDevice();

	pSlot->SetDevice(NULL);

	m_pSystem->Validate(FALSE);

	ListUpdate(pSlot);
	}

void CFixedRackNavTreeWnd::OnEditCut(void)
{
	OnEditCopy();
	
	OnItemDelete(IDS_CUT_1, FALSE);
	}

void CFixedRackNavTreeWnd::OnExecPaste(CCmdPaste *pCmd)
{
	CRackNavTreeWnd::OnExecPaste(pCmd);
	}

void CFixedRackNavTreeWnd::OnUndoPaste(CCmdPaste *pCmd)
{
	UINT n = pCmd->m_Names.GetCount();

	while( n-- ) {

		CString    Base  = m_pList->GetFixedPath();

		CString    Name  = pCmd->m_Names[n];

		CString    Fixed = Base + L'/' + Name;

		HTREEITEM  hItem = m_MapFixed[Fixed];

		CMetaItem *pItem = GetItemPtr(hItem);

		PerformDelete((CCommsPortSlot *) pItem);
		}
	}

// Data Object Construction

BOOL CFixedRackNavTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	CDataObject *pMake = New CDataObject;

	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		if( AddItemToStream(Stream, m_hSelect) ) {
		
			pMake->AddStream(m_cfData, Stream);
			}
		}

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

void CFixedRackNavTreeWnd::AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem)
{
	CItem *pItem = GetItemPtr(hItem);

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsPortSlot)) ) {

		pItem = ((CCommsPortSlot *) pItem)->GetDevice();
		
		if( pItem ) {

			CString Class = pItem->GetClassName();

			Tree.PutObject(Class.Mid(1));

			Tree.PutValue (L"NDX", pItem->GetIndex());

			pItem->PreCopy();

			pItem->Save(Tree);

			Tree.EndObject();
			}
		}
	}

// Data Object Acceptance

BOOL CFixedRackNavTreeWnd::AcceptStream(ITextStream &Stream, CAcceptCtx const &Ctx, BOOL fLocal)
{
	CTreeFile Tree;

	if( Tree.OpenLoad(Stream) ) {

		OnAcceptInit(Tree);

		CCommsPortSlot * pSlot = (CCommsPortSlot *) m_pSelect;
		
		while( !Tree.IsEndOfData() ) {

			CString Name = Tree.GetName();

			if( !Name.IsEmpty() ) {

				if( OnAcceptName(Tree, fLocal, Name) ) {

					CLASS Class = AfxNamedClass(PCTXT(L"C" + Name));

					UINT  uSize = Class->GetObjectSize();

					PVOID pData = AfxMalloc(uSize, afxFile, afxLine, TRUE);

					if( pData == m_pKilled ) {

						// URGH -- Various things get confused during a move
						// if the new item has the same address as the one
						// we have just deleted, so this horrible hack avoids
						// the problem until I can fix it properly. Note we
						// do this with a block of memory rather than the
						// object itself as that is painful to construct and
						// tear-down without breaking other things.

						PVOID   pMore = AfxMalloc(uSize, afxFile, afxLine, TRUE);

						AfxFree(pData, TRUE);

						pData = pMore;
						}

					CMetaItem *pItem = AfxNewObjectAt( CMetaItem,
									   Class,
									   pData
									   );
					Tree.GetObject();

					Tree.GetName();

					UINT uIndex = Tree.GetValueAsInteger();

					pItem->SetIndex(uIndex);

					pItem->SetParent(m_pDevices);

					pItem->Load(Tree);

					Tree.EndObject();

					Name = pItem->GetName();

					if( Ctx.m_fCheck ) {

						MakeUnique(Name);
						}

					pItem->SetName(Name);

					m_pDevices->AppendItem(pItem);

					if( !Ctx.m_fPaste ) {

						pItem->PostLoad();
						}
					else {
						pItem->PostPaste();

						pItem->PostLoad();
						}

					if( Ctx.m_pNames ) {

						CString Fixed = pSlot->GetFixedName();

						Ctx.m_pNames->Append(Fixed);
						}

					pSlot->SetDevice((CCommsDeviceRack *) pItem);

					ListUpdate(pSlot);
					}
				}
			}

		return TRUE;
		}

	m_pTree->EnsureVisible(m_hSelect);		       

	return FALSE;
	}

// Drag Hooks

DWORD CFixedRackNavTreeWnd::FindDragAllowed(void)
{
	if( IsFitted() ) {

		return 	DROPEFFECT_LINK;
		}

	return DROPEFFECT_NONE;
	}

// Drag Support

BOOL CFixedRackNavTreeWnd::DropDone(IDataObject *pData)
{
	if( m_fDropLocal ) {
		
		if( IsValidDrop() ) {
		
			if( m_fDropMove ) {
			
				HTREEITEM  hSave = m_hSelect;

				CMetaItem *pSave = m_pSelect;

				m_hSelect = m_hDropMove;

				m_pSelect = GetItemPtr(m_hDropMove);

				m_pNamed  = m_pSelect;

				CString Menu;
				
				Menu.Format(IDS_MOVE_2, m_pNamed->GetName());

				m_System.ExecCmd(New CCmdMulti(m_pSelect->GetFixedPath(), Menu, navAll));

				OnItemDelete(IDS_MOVE_1, TRUE);

				m_hSelect = hSave;

				m_pSelect = pSave;

				m_pNamed  = m_pSelect;

				CItem     * pRoot = GetItemPtr(m_hDropRoot);

				CCmdPaste * pCmd  = New CCmdPaste(m_pSelect, pRoot, NULL, pData, TRUE);

				CAcceptCtx  Ctx;

				Ctx.m_Root   = pCmd->m_Root;

				Ctx.m_fPaste = FALSE;

				Ctx.m_fCheck = TRUE;

				Ctx.m_fUndel = FALSE;

				Ctx.m_pNames = &pCmd->m_Names;

				AcceptDataObject(pCmd->m_pData, Ctx);

				pCmd->m_fInit = FALSE;

				m_System.SaveCmd(pCmd);

				m_System.ExecCmd(New CCmdMulti(m_pSelect->GetFixedPath(), Menu, navAll));

				m_pSystem->Validate(FALSE);

				return TRUE;
				}

			return CRackNavTreeWnd::DropDone(pData);
			}

		return FALSE;
		}

	return CRackNavTreeWnd::DropDone(pData);
	}

BOOL CFixedRackNavTreeWnd::IsValidDrop(void)
{
	if( m_uDrop == dropOther || m_uDrop == dropAccept ) {

		if( m_hDropRoot ) {

			CMetaItem *pItem = GetItemPtr(m_hDropRoot);

			if( pItem->IsKindOf(AfxRuntimeClass(CCommsPortSlot)) ) {

				return ((CCommsPortSlot *) pItem)->GetType() == typeNone;
				}
			}
		}

	return FALSE;
	}

// Tree Loading

void CFixedRackNavTreeWnd::ListUpdate(CCommsPortSlot *pSlot)
{
	m_System.SetViewedItem(NULL);

	m_pTree->SetItemText(m_hSelect, pSlot->GetTreeLabel());
	
	m_System.SetViewedItem(pSlot);

	KillItem(m_hSelect, TRUE);

	AddToMaps(pSlot, m_hSelect);

	ListUpdated();

	UpdateStatus();
	}

void CFixedRackNavTreeWnd::LoadTree(void)
{
	CRackNavTreeWnd::LoadTree();

	ExpandAll();
	}

// Item Hooks

UINT CFixedRackNavTreeWnd::GetRootImage(void)
{
	return 0x45;
	}

UINT CFixedRackNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	CCommsPortSlot *pSlot = (CCommsPortSlot *) pItem;	

	return pSlot->GetTreeImage();
	}

CString CFixedRackNavTreeWnd::GetItemText(CMetaItem *pItem)
{
	CCommsPortSlot *pSlot = (CCommsPortSlot *) pItem;	
	
	return pSlot->GetTreeLabel();
	}

void CFixedRackNavTreeWnd::OnItemRenamed(CItem *pItem)
{
	CCommsPortSlot *pSlot = (CCommsPortSlot *) pItem;

	pSlot->UpdateName();

	m_pTree->SetItemText(m_hSelect, ((CCommsPortSlot *) pItem)->GetTreeLabel());

	CRackNavTreeWnd::OnItemRenamed(pItem);

	m_System.ItemUpdated(pSlot->GetDevice(), updateRename);

	m_System.ItemUpdated(pSlot->GetOption(), updateRename);
	}

void CFixedRackNavTreeWnd::NewItemSelected(void)
{
	CRackNavTreeWnd::NewItemSelected();

	if( !m_fLocked ) {

		UpdateStatus();
		}
	}

// Implementation

BOOL CFixedRackNavTreeWnd::IsFitted(void)
{
	if( m_pSelect->IsKindOf(AfxNamedClass(L"CCommsPortSlot")) ) {

		return ((CCommsPortSlot *) m_pSelect)->GetType() == typeModule;
		}

	return false;
	}

BOOL CFixedRackNavTreeWnd::IsEmpty(void)
{
	if( m_pSelect->IsKindOf(AfxNamedClass(L"CCommsPortSlot")) ) {

		return ((CCommsPortSlot *) m_pSelect)->GetType() == typeNone;
		}

	return false;
	}

BOOL CFixedRackNavTreeWnd::IsRack(void)
{
	if( m_pSelect->IsKindOf(AfxNamedClass(L"CCommsPortSlot")) ) {

		return ((CCommsPortSlot *) m_pSelect)->GetType() == typeRack;
		}

	return false;
	}

BOOL CFixedRackNavTreeWnd::CanAddRack(void)
{
	if( m_pSelect->IsKindOf(AfxNamedClass(L"CCommsPortSlot")) ) {

		CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

		if( pSlot->GetType() == typeRack ) {

			COptionCardRackItem *pRack = pSlot->GetRack();

			if( pRack->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

				return ((CCascadedRackItem *) pRack)->HasRoom();
				}
			}
		}
	
	return false;
	}

BOOL CFixedRackNavTreeWnd::CanDelRack(void)
{
	if( m_pSelect->IsKindOf(AfxNamedClass(L"CCommsPortSlot")) ) {

		CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

		if( pSlot->GetType() == typeRack ) {

			COptionCardRackItem *pRack = pSlot->GetRack();

			if( !pRack->IsKindOf(AfxRuntimeClass(CCascadedRackItem)) ) {

				if( pRack->HasType(classComms) ) {

					return TRUE;
					}
				
				if( pRack->HasType(classIO) ) {

					return TRUE;
					}

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

void CFixedRackNavTreeWnd::OnListUpdated(void)
{
	m_System.ItemUpdated(m_pDevices,   updateChildren);

	m_System.ItemUpdated(m_pExpansion, updateChildren);
	}

void CFixedRackNavTreeWnd::UpdateStatus(void)
{
	if( !m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortRack)) && m_pSelect->IsKindOf(AfxRuntimeClass(CCommsPortSlot)) ) {

		CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pSelect;

		if( pSlot->GetType() != typeRack ) {

			CCommsDeviceRack *pDev = pSlot->GetDevice();

			if( pDev ) {
				
				afxThread->SetStatusText(CPrintf(L"Device Number %d", pDev->m_Number));
				}
			else {
				afxThread->SetStatusText(CPrintf(L"Slot Number %d", pSlot->m_SlotNum));
				}
			}
		}
	else {
		afxThread->SetStatusText(L"");
		}
	}

void CFixedRackNavTreeWnd::MakeUnique(CString &Name)
{
	if( m_pSystem->m_pComms->FindDevice(Name) ) {

		UINT c = Name.GetLength();

		UINT d = CNamedList::m_Disambig.GetLength();

		UINT n = 0;

		while( isdigit(Name[c - 1 - n]) ) {

			n++;
			}

		if( Name.Mid(c - n - d, d) == CNamedList::m_Disambig ) {
			
			Name = Name.Left(c - n - d);
			}

		CString r = Name + CNamedList::m_Disambig;

		UINT    v = 0;

		for(;;) { 

			Name = r + CPrintf(L"%u", ++v);
			
			if( !m_pSystem->m_pComms->FindDevice(Name) ) {

				break;
				}
			}
		}
	}

void CFixedRackNavTreeWnd::RefreshTree(void)
{
	RefreshSlots();

	CRackNavTreeWnd::RefreshTree();
	}

void CFixedRackNavTreeWnd::RefreshSlots(void)
{
	CCommsPortRack *pPort = (CCommsPortRack *) m_pItem;

	pPort->RemapSlots(FALSE);
	}

void CFixedRackNavTreeWnd::ExpandAll(void)
{
	for( INDEX i = m_MapNames.GetHead(); !m_MapNames.Failed(i); m_MapNames.GetNext(i) ) {

		HTREEITEM hItem = m_MapNames.GetData(i);

		if( IsParent(hItem) ) {

			ExpandItem(hItem);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// New Module Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CNewModuleDialog, CStdDialog);

// Static Data

CLASS CNewModuleDialog::m_Class = NULL;
		
// Constructors

CNewModuleDialog::CNewModuleDialog(BOOL fGraphite)
{
	SetName(L"NewModuleDialog");

	m_fGraphite = fGraphite;
	}

// Attributes

CLASS CNewModuleDialog::GetClass(void) const
{
	return m_Class;
	}

// Message Map

AfxMessageMap(CNewModuleDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchCommand(IDOK, OnCommandOK)

	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchNotify(100, LBN_DBLCLK, OnDblClk)

	AfxMessageEnd(CNewModuleDialog)
	};

// Message Handlers

BOOL CNewModuleDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &List = (CListBox  &) GetDlgItem(100);

	LoadModels(List);
	
	return TRUE;
	}
	
// Command Handlers

BOOL CNewModuleDialog::OnCommandOK(UINT uID)
{
	CListBox &List = (CListBox &) GetDlgItem(100);

	m_Class        = CLASS(List.GetCurSelData());

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CNewModuleDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

void CNewModuleDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	OnCommandOK(uID);
	}

// Implementation

void CNewModuleDialog::LoadModels(CListBox &List, CModel *pType, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {		

		CString Class;

		CString Name  = pType[n].m_pName;

		if( C3OemFeature(Name, TRUE) ) {

			C3OemStrings(Name);

			Name  += L'\t';

			Name  += CString(pType[n].m_uDesc);

			Class += L'C';

			Class += pType[n].m_pClass;

			Class += L"Module";

			List.AddString(Name, DWORD(AfxNamedClass(Class)));
			}
		}
	}

void CNewModuleDialog::LoadModels(CListBox &List)
{
	int nTab[] = { 40, 0 };

	List.SetTabStops(elements(nTab), nTab);

	if( m_fGraphite ) {

		static CModel Type[] = {

		{	L"GMDIO14",	L"GMDIO14",	IDS_IN_OUT_DIGITAL	},
		{	L"GMINI8",	L"GMINI8",	IDS_CHANNEL_CURRENT	},
		{	L"GMINV8",	L"GMINV8",	IDS_CHANNEL_VOLTAGE	},
		{	L"GMOUT4",	L"GMOUT4",	IDS_CHANNEL_ANALOG	},
		{	L"GMPID1",	L"GMPID1",	IDS_SINGLE_LOOP		},
		{	L"GMPID2",	L"GMPID2",	IDS_DUAL_LOOP		},
		{	L"GMRTD6",	L"GMRTD6",	IDS_CHANNEL_RTD_INPUT	},
		{	L"GMTC8",	L"GMTC8",	IDS_CHANNEL		},
		{	L"GMUIN4",	L"GMUIN4",	IDS_IN_ANALOG		},
		{	L"GMSG1",	L"GMSG",	IDS_SINGLE_LOOP_STRAIN	},

		};

		LoadModels(List, Type, elements(Type));
		}
	else {
		static CModel Type[] = {

		{	L"CSPID1",	L"SLC",		IDS_SINGLE_LOOP		},
		{	L"CSPID2",	L"DLC",		IDS_DUAL_LOOP		},
		{	L"CSSG1",	L"SG",		IDS_SINGLE_LOOP_STRAIN	},
		{	L"CSTC8",	L"TC8",		IDS_CHANNEL		},
		{	L"CSRTD6",	L"RTD6",	IDS_CHANNEL_RTD_INPUT	},
		{	L"CSINI8",	L"II8",		IDS_CHANNEL_CURRENT	},
		{	L"CSINI8L",	L"II8L",	IDS_CHANNEL_CURRENT_2	},
		{	L"CSINV8",	L"IV8",		IDS_CHANNEL_VOLTAGE	},
		{	L"CSINV8L",	L"IV8L",	IDS_CHANNEL_VOLTAGE_2	},
		{	L"CSOUT4",	L"OUT4",	IDS_CHANNEL_ANALOG	},
		{	L"CSDIO14",	L"DIO14",	IDS_IN_OUT_DIGITAL	},
		{	L"CSTC8ISO",	L"TC8ISO",	IDS_CHANNEL_ISO		}

		};

		LoadModels(List, Type, elements(Type));
		}

	if( !List.SelectData(DWORD(m_Class)) ) {

		List.SetCurSel(0);
		}
	}

// End of File
