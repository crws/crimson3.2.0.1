
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAUIN6MainWnd_HPP

#define INCLUDE_DAUIN6MainWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAUIN6Module;

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 Module Window
//

class CDAUIN6MainWnd : public CProxyViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAUIN6MainWnd(void);

protected:
	// Data Members
	CMultiViewWnd * m_pMult;
	CDAUIN6Module * m_pItem;

	// Overridables
	void OnAttach(void);

	// Implementation
	void AddInputPages(void);
};

// End of File

#endif
