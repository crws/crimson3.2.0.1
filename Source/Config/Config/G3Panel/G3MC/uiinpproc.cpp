
#include "intern.hpp"

#include "uiinpproc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/uin4dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Input Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUIInputProcess, CUIEditBox)

// Linked List

CUIInputProcess * CUIInputProcess::m_pHead = NULL;

CUIInputProcess * CUIInputProcess::m_pTail = NULL;

// Constructor

CUIInputProcess::CUIInputProcess(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Core Overridables

void CUIInputProcess::OnBind(void)
{
	CUIEditBox::OnBind();
	}

// Destructor

CUIInputProcess::~CUIInputProcess(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Update Support

void CUIInputProcess::CheckUpdate(CCommsItem *pComms, CString const &Tag)
{
	if( Tag.StartsWith(L"TempUnits") || Tag.StartsWith(L"ProcUnits") || Tag.StartsWith(L"ProcDP") ) {
		
		CUIInputProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextInputProcess *pText = (CUITextInputProcess *) pScan->m_pText;

			if( pText->m_pComms == pComms ) {

				if( pText->m_cType == 'O' ) {
				
					pScan->Update(FALSE);
					}

				if( pText->m_cType == 'L' || pText->m_cType == 'N' ) {
				
					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}

		return;
		}
	}

void CUIInputProcess::Update(BOOL fFlag)
{
	CUITextInputProcess *pText = (CUITextInputProcess *) m_pText;

	pText->GetConfig();

	if( fFlag ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
		}

	m_Units = pText->m_Units;

	m_pUnitCtrl->SetWindowText(m_Units);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- PID Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextInputProcess, CUITextInteger)

// Constructor

CUITextInputProcess::CUITextInputProcess(void)
{
	}

// Destructor

CUITextInputProcess::~CUITextInputProcess(void)
{
	}

// Core Overidables

void CUITextInputProcess::OnBind(void)
{
	CUITextInteger::OnBind();

	m_pComms = (CCommsItem *) m_pItem;

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
	}

// Implementation

void CUITextInputProcess::GetConfig(void)
{
	m_cType     = char(m_UIData.m_Format[0]);

	UINT uIndex = m_UIData.m_Format[1] - '0';

	if( m_cType == 'N' ) {
		
		m_uPlaces = GetInteger(CPrintf("ProcDP%d", uIndex));

		m_Units   = "";

		m_nMin    = -30000;

		m_nMax    = +30000;

		CheckFlags();

		return;
		}

	if( m_cType == 'L' ) {
		
		m_uPlaces = GetInteger(CPrintf("ProcDP%d", uIndex));

		m_Units   = GetString (CPrintf("ProcUnits%d", uIndex));

		m_nMin    = -30000;

		m_nMax    = +30000;

		CheckFlags();

		return;
		}

	if( m_cType == 'O' ) {
		
		m_uPlaces = 2;

		switch( GetInteger(CPrintf("TempUnits%d", uIndex)) ) {

			case DEGREES_K:	
				m_Units   = "K";
				break;

			case DEGREES_F:	
				m_Units   = CPrintf("%cF", 176);
				break;

			case DEGREES_C:	
				m_Units   = CPrintf("%cC", 176);
				break;
			}

		m_nMin    = -30000;

		m_nMax    = +30000;

		CheckFlags();

		return;
		}

	if( m_cType == 'M' ) {

		m_uPlaces = 2;

		switch( GetInteger(CPrintf("TempUnits%d", uIndex)) ) {

			case 0:
				m_Units   = "K";
				break;

			case 1:
				m_Units   = CPrintf("%cC", 176);
				break;

			case 2:
				m_Units   = CPrintf("%cF", 176);
				break;
			}

		m_nMin    = -30000;

		m_nMax    = +30000;

		CheckFlags();

		return;
		}
	}

void CUITextInputProcess::CheckFlags(void)
{
	if( m_uPlaces ) {
		
		m_uFlags |= textPlaces;
		}
	else 
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
		}
	else 
		m_uFlags &= ~textSigned;
	}

UINT CUITextInputProcess::GetInteger(CString Tag)
{
	CMetaData const *pData = m_pComms->FindMetaData(Tag);

	AfxAssert(pData);
	
	return pData->ReadInteger(m_pComms);
	}

CString CUITextInputProcess::GetString(CString Tag)
{
	CMetaData const *pData = m_pComms->FindMetaData(Tag);

	AfxAssert(pData);
	
	return pData->ReadString(m_pComms);
	}

// End of File
