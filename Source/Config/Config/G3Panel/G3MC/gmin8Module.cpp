
#include "intern.hpp"

#include "gmin8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/in8dbase.h"

#include "import/graphite/in8props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMIN8Module, CGraphiteGenericModule);

// UI Management

CViewWnd * CGMIN8Module::CreateMainView(void)
{
	return New CGraphiteIN8MainWnd;
	}

// Comms Object Access

UINT CGMIN8Module::GetObjectCount(void)
{
	return 1;
	}

BOOL CGMIN8Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_INP);
			
			Data.pItem = m_pConfig;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CGMIN8Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pConfig->  Convert(pValue->GetChild(L"Input"));

		return TRUE;
		}
	
	return FALSE;
	}

// Meta Data Creation

void CGMIN8Module::AddMetaData(void)
{
	Meta_AddObject(Config);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMINI8Module, CGMIN8Module);

// Constructor

CGMINI8Module::CGMINI8Module(void)
{
	m_pConfig = New CGraphiteINI8Config;

	m_Ident   = LOBYTE(ID_GMINI8);

	m_FirmID  = FIRM_GMIN8;

	m_Model   = "GMINI8";

	m_Power   = 14;

	m_Conv.Insert(L"Legacy", L"CII8Module");
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Voltage Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMINV8Module, CGMIN8Module);

// Constructor

CGMINV8Module::CGMINV8Module(void)
{
	m_pConfig = New CGraphiteINV8Config;

	m_Ident   = LOBYTE(ID_GMINV8);

	m_FirmID  = FIRM_GMIN8;

	m_Model   = "GMINV8";

	m_Power   = 14;

	m_Conv.Insert(L"Legacy", L"CIV8Module");
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Modules Window
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteIN8MainWnd, CProxyViewWnd);

// Constructor

CGraphiteIN8MainWnd::CGraphiteIN8MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CGraphiteIN8MainWnd::OnAttach(void)
{
	m_pItem = (CGMIN8Module *) CProxyViewWnd::m_pItem;

	AddPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CGraphiteIN8MainWnd::AddPages(void)
{
	CGraphiteIN8Config *pConfig = m_pItem->m_pConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(pConfig->GetPageName(n), pPage);

		pPage->Attach(pConfig);
		}
	}

// End of File

