
//////////////////////////////////////////////////////////////////////////
//
// CSDIO14 Configuration
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MASTER_HPP

#define INCLUDE_MASTER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMasterModule;
class CMasterData;

//////////////////////////////////////////////////////////////////////////
//
// Object IDs
//

#define	OBJ_COMMS		0x00

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Comms Data
//

#define	PROPID_ERROR_01		MAKEPROP(TYPE_BOOL, 0x10)
#define	PROPID_ERROR_02		MAKEPROP(TYPE_BOOL, 0x11)
#define	PROPID_ERROR_03		MAKEPROP(TYPE_BOOL, 0x12)
#define	PROPID_ERROR_04		MAKEPROP(TYPE_BOOL, 0x13)
#define	PROPID_ERROR_05		MAKEPROP(TYPE_BOOL, 0x14)
#define	PROPID_ERROR_06		MAKEPROP(TYPE_BOOL, 0x15)
#define	PROPID_ERROR_07		MAKEPROP(TYPE_BOOL, 0x16)
#define	PROPID_ERROR_08		MAKEPROP(TYPE_BOOL, 0x17)
#define	PROPID_ERROR_09		MAKEPROP(TYPE_BOOL, 0x18)
#define	PROPID_ERROR_10		MAKEPROP(TYPE_BOOL, 0x19)
#define	PROPID_ERROR_11		MAKEPROP(TYPE_BOOL, 0x1A)
#define	PROPID_ERROR_12		MAKEPROP(TYPE_BOOL, 0x1B)
#define	PROPID_ERROR_13		MAKEPROP(TYPE_BOOL, 0x1C)
#define	PROPID_ERROR_14		MAKEPROP(TYPE_BOOL, 0x1D)
#define	PROPID_ERROR_15		MAKEPROP(TYPE_BOOL, 0x1E)
#define	PROPID_ERROR_16		MAKEPROP(TYPE_BOOL, 0x1F)

#define PROPID_TOOLS_WATCHDOG	MAKEPROP(TYPE_WORD, 0x30)

//////////////////////////////////////////////////////////////////////////
//
// Master Module
//

class CMasterModule : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMasterModule(void);

		// Destructor
		~CMasterModule(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Downoad Support
		BOOL MakeInitData(CInitData &Init);
		
	protected:
		// Data Members
		CMasterData   * m_pData;
		CFirmwareList * m_pFirm;

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Master Module Data
//

class CMasterData : public CCommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMasterData(void);

		// Group Names
		CString GetGroupName(WORD Group);

	protected:
		// Static Data
		static CCommsList m_CommsList[];

		// Implementation
		void AddMetaData(void);
	};

// End of File

#endif
