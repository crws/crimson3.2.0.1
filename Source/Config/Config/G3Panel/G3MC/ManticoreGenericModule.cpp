
#include "intern.hpp"

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Manticore Generic Module
//

// Runtime Class

AfxImplementRuntimeClass(CManticoreGenericModule, CGenericModule);

// Constructor

CManticoreGenericModule::CManticoreGenericModule(void)
{
}

// Download Data

void CManticoreGenericModule::MakeConfigData(CInitData &Init)
{
	Init.AddByte(m_FirmID);
}

// End of File
