
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UIMix2AODynamic_HPP

#define INCLUDE_UIMix2AODynamic_HPP

#include "DAMix2AnalogOutputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

class CUIMix2AODynamic : public CUIEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUIMix2AODynamic(void);

	// Destructor
	~CUIMix2AODynamic(void);

	// Update Support
	static void CheckUpdate(CDAMix2AnalogOutputConfig *pConfig, CString const &Tag);

	// Operations
	void Update(BOOL fKeep);
	void UpdateUnits(void);

protected:
	// Linked List
	static CUIMix2AODynamic * m_pHead;
	static CUIMix2AODynamic * m_pTail;

	// Data Members
	CUIMix2AODynamic * m_pNext;
	CUIMix2AODynamic * m_pPrev;
};

// End of File

#endif
