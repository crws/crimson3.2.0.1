
#include "intern.hpp"

#include "legacy.h"

#include "out4mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

// Dynamic Class

AfxImplementDynamicClass(CUIOut4Dynamic, CUIEditBox)

// Linked List

CUIOut4Dynamic * CUIOut4Dynamic::m_pHead = NULL;

CUIOut4Dynamic * CUIOut4Dynamic::m_pTail = NULL;

// Constructor

CUIOut4Dynamic::CUIOut4Dynamic(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Destructor

CUIOut4Dynamic::~CUIOut4Dynamic(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Update Support

void CUIOut4Dynamic::CheckUpdate(COut4Conf *pConf, CString const &Tag)
{
	if( Tag.Left(7) == "OutType" ) {

		CUIOut4Dynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextOut4Dynamic *pText = (CUITextOut4Dynamic *) pScan->m_pText;

			if( pText->m_pConf == pConf ) {

				if( pText->m_cType >= 'J' && pText->m_cType <= 'Q' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag.Left(2) == "DP" ){

		CUIOut4Dynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextOut4Dynamic *pText = (CUITextOut4Dynamic *) pScan->m_pText;

			if( pText->m_pConf == pConf ) {

				if( pText->m_cType >= 'A' && pText->m_cType <= 'H' ) {

					pScan->Update(TRUE);
					}

				if( pText->m_cType >= 'R' && pText->m_cType <= 'U' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag.Left(4) == "Data" ){

		CUIOut4Dynamic *pScan = m_pHead;

		while( pScan ){

			CUITextOut4Dynamic *pText = (CUITextOut4Dynamic *) pScan->m_pText;

			if( pText->m_pConf == pConf ) {

				if( pText->m_cType >= 'R' && pText->m_cType <= 'U' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}
	}

// Operations

void CUIOut4Dynamic::Update(BOOL fKeep)
{
	CUITextOut4Dynamic *pText = (CUITextOut4Dynamic *) m_pText;

	pText->GetConfig();

	if( fKeep ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
		}
	else {
		INT nData = m_pData->ReadInteger(m_pItem);

		INT nCopy = nData;

		pText->Check(CError(FALSE), nData);

		if( nData != nCopy ) {

			m_pData->WriteInteger(m_pItem, UINT(nData));

			m_pItem->SetDirty();
			}

		LoadUI();
		}

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

void CUIOut4Dynamic::UpdateUnits(void)
{
	CUITextOut4Dynamic *pText = (CUITextOut4Dynamic *) m_pText;

	pText->GetConfig();

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Analog Output Module Dynamic Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextOut4Dynamic, CUITextInteger)

// Constructor

CUITextOut4Dynamic::CUITextOut4Dynamic(void)
{
	}

// Destructor

CUITextOut4Dynamic::~CUITextOut4Dynamic(void)
{
	}

// Core Overidables

void CUITextOut4Dynamic::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(COut4Conf)) ) {

		m_pConf = (COut4Conf *) m_pItem;
		}
	else
		AfxAssert(FALSE);

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
	}

// Implementation

void CUITextOut4Dynamic::GetConfig(void)
{
	m_cType = char(m_UIData.m_Format[0]);

	FindPlaces();

	FindUnits();

	FindRanges();

	CheckFlags();
	}

void CUITextOut4Dynamic::FindPlaces(void)
{
	switch( m_cType ){

		case 'A':
		case 'E':
		case 'R':
			m_uPlaces = m_pConf->m_DP1;
			break;

		case 'B':
		case 'F':
		case 'S':
			m_uPlaces = m_pConf->m_DP2;
			break;

		case 'C':
		case 'G':
		case 'T':
			m_uPlaces = m_pConf->m_DP3;
			break;

		case 'D':
		case 'H':
		case 'U':
			m_uPlaces = m_pConf->m_DP4;
			break;

		default:
			m_uPlaces = 3;
			break;
		}
	}

void CUITextOut4Dynamic::FindUnits(void)
{
	switch( m_cType ){

		case 'J':
		case 'N':
			m_Units = (m_pConf->m_OutType1 >= OUT_020MA) ? "mA" : "V";
			break;

		case 'K':
		case 'O':
			m_Units = (m_pConf->m_OutType2 >= OUT_020MA) ? "mA" : "V";
			break;

		case 'L':
		case 'P':
			m_Units = (m_pConf->m_OutType3 >= OUT_020MA) ? "mA" : "V";
			break;

		case 'M':
		case 'Q':
			m_Units = (m_pConf->m_OutType4 >= OUT_020MA) ? "mA" : "V";
			break;

		default:
			m_Units = "";
			break;
		}
	}

void CUITextOut4Dynamic::FindRanges(void)
{
	switch( m_cType ){

		case 'J':
		case 'N':
			SetMinMax(m_pConf->m_OutType1);
			break;

		case 'K':
		case 'O':
			SetMinMax(m_pConf->m_OutType2);
			break;

		case 'L':
		case 'P':
			SetMinMax(m_pConf->m_OutType3);
			break;

		case 'M':
		case 'Q':
			SetMinMax(m_pConf->m_OutType4);
			break;

		case 'R':
			m_nMin = m_pConf->m_DataLo1;
			m_nMax = m_pConf->m_DataHi1;
			break;

		case 'S':
			m_nMin = m_pConf->m_DataLo2;
			m_nMax = m_pConf->m_DataHi2;
			break;

		case 'T':
			m_nMin = m_pConf->m_DataLo3;
			m_nMax = m_pConf->m_DataHi3;
			break;

		case 'U':
			m_nMin = m_pConf->m_DataLo4;
			m_nMax = m_pConf->m_DataHi4;
			break;

		default:
			SetMinMax(0);
			break;
		}
	}

void CUITextOut4Dynamic::SetMinMax(UINT OutType)
{
	switch( OutType ){

		case OUT_5V:
			m_nMin =      0;
			m_nMax =   5000;
			break;

		case OUT_10V:
			m_nMin =      0;
			m_nMax =  10000;
			break;

		case OUT_BIP10V:
			m_nMin = -10000;
			m_nMax =  10000;
			break;

		case OUT_020MA:
			m_nMin =      0;
			m_nMax =  20000;
			break;

		case OUT_420MA:
			m_nMin =   4000;
			m_nMax =  20000;
			break;

		default:
			m_nMin = -30000;
			m_nMax =  30000;
			break;
		}
	}

void CUITextOut4Dynamic::CheckFlags(void)
{
	if( m_uPlaces ) {
		
		m_uFlags |= textPlaces;
		}
	else 
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
		}
	else 
		m_uFlags &= ~textSigned;
	}

// End of File
