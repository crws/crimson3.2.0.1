
#include "intern.hpp"

#include "DAMix2AnalogInputConfigWnd.hpp"

#include "DAMix2AnalogInputConfig.hpp"

#include "DAMix2Module.hpp"

#include "uiinpproc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Input Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2AnalogInputConfigWnd, CUIViewWnd);

// Constructor

CDAMix2AnalogInputConfigWnd::CDAMix2AnalogInputConfigWnd(UINT uPage)
{
	m_uPage = uPage;
}

// Overibables

void CDAMix2AnalogInputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMix2AnalogInputConfig *) CViewWnd::m_pItem;

	m_pModule = (CDAMix2Module *) m_pItem->GetParent(AfxRuntimeClass(CDAMix2Module));

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dauin6_cfg"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix2AnalogInputConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddOperation();

	AddUnits();

	EndPage(FALSE);
}

void CDAMix2AnalogInputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag.StartsWith(L"Range") ) {

		DoEnables(1);
		DoEnables(2);

		return;
	}

	CUIInputProcess::CheckUpdate(m_pItem, Tag);
}

// Implementation

void CDAMix2AnalogInputConfigWnd::AddOperation(void)
{
	StartGroup(IDS("Operation"), 1);

	AddUI(m_pItem, L"root", CPrintf("Range%u",  m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("Filter%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("Offset%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("Slope%u",  m_uPage + 1));

	EndGroup(TRUE);
}

void CDAMix2AnalogInputConfigWnd::AddUnits(void)
{
	StartGroup(IDS("Units"), 1);

	AddUI(m_pItem, L"root", CPrintf("TempUnits%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("ProcUnits%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("ProcDP%u",    m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("ProcMin%u",   m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("ProcMax%u",   m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("Root%u",      m_uPage + 1));

	EndGroup(TRUE);
}

// Data Access

UINT CDAMix2AnalogInputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

void CDAMix2AnalogInputConfigWnd::PutInteger(CMetaItem *pItem, CString Tag, UINT Data)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	pData->WriteInteger(pItem, Data);
}

// Enabling

void CDAMix2AnalogInputConfigWnd::DoEnables(UINT uIndex)
{
	CDAMix2AnalogInputConfig *pInputConfig = m_pModule->m_pAnalogInputConfig;

	UINT uRange = GetInteger(pInputConfig, CPrintf("Range%u", uIndex));

	BOOL fOff   = uRange == 0;

	BOOL fRawV  = uRange == 1;

	BOOL fRawA  = uRange == 2;

	BOOL fOhms  = uRange == 3;

	BOOL fVolt  = uRange >=  3 && uRange <= 6;

	BOOL fmAmp  = uRange >=  7 && uRange <= 9  || fRawA || fOhms;

	BOOL fmVolt = uRange == 10 || uRange == 25 || fRawV || fOhms;

	BOOL fTC    = uRange >= 11 && uRange <= 19;

	BOOL fRTD   = uRange >= 21 && uRange <= 24;

	EnableUI(CPrintf("Filter%u", uIndex), !fOff && (fVolt || fmVolt || fmAmp || fRTD || fTC));
	EnableUI(CPrintf("Offset%u", uIndex), !fOff && (fRTD || fTC));
	EnableUI(CPrintf("Slope%u", uIndex), !fOff && (fRTD || fTC));

	EnableUI(CPrintf("TempUnits%u", uIndex), !fOff && (fRTD || fTC));
	EnableUI(CPrintf("ProcUnits%u", uIndex), !fOff && (fVolt || fmVolt || fmAmp));
	EnableUI(CPrintf("ProcDP%u", uIndex), !fOff && (fVolt || fmVolt || fmAmp));
	EnableUI(CPrintf("ProcMin%u", uIndex), !fOff && (fVolt || fmVolt || fmAmp));
	EnableUI(CPrintf("ProcMax%u", uIndex), !fOff && (fVolt || fmVolt || fmAmp));
	EnableUI(CPrintf("Root%u", uIndex), !fOff && (fVolt || fmAmp));

	EnableUI(CPrintf("Sample%u", uIndex), !fOff);
}

// End of File
