
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2DigitalInputConfig_HPP

#define INCLUDE_DAMix2DigitalInputInputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input Configuration
//

class CDAMix2DigitalInputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2DigitalInputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	UINT m_Mode1;
	UINT m_Mode2;
	UINT m_Mode3;
	UINT m_Mode4;
	UINT m_Mode5;
	UINT m_Mode6;
	UINT m_Mode7;
	UINT m_Mode8;
	UINT m_Pull1;
	UINT m_Pull2;
	UINT m_Pull3;
	UINT m_Pull4;
	UINT m_Pull5;
	UINT m_Pull6;
	UINT m_Pull7;
	UINT m_Pull8;


protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void ConvertMode(UINT uIndex);
};

// End of File

#endif
