
#include "intern.hpp"

#include "tc8mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMTC8Module, CTC8Module);

// Constructor

CGMTC8Module::CGMTC8Module(void)
{
	delete m_pInput;

	m_pInput = New CTC8Input(TRUE);

	m_Ident  = LOBYTE(ID_GMTC8);

	m_FirmID = FIRM_GMIN8;

	m_Model  = "GMTC8";

	m_Power  = 14;

	m_Conv.Insert(L"Legacy", L"CTC8Module");
	}

// Download Support

void CGMTC8Module::MakeConfigData(CInitData &Init)
{
	Init.AddByte(BYTE(m_FirmID));

	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);
	}

// End of File
