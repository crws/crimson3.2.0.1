
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2Module_HPP

#define INCLUDE_DAMix2Module_HPP

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDAMix2AnalogInput;
class CDAMix2AnalogInputConfig;

class CDAMix2AnalogOutput;
class CDAMix2AnalogOutputConfig;

class CDAMix2DigitalInput;
class CDAMix2DigitalInputConfig;

class CDAMix2DigitalOutput;
class CDAMix2DigitalOutputConfig;

class CDAMixDigitalConfig;

//////////////////////////////////////////////////////////////////////////
//
// 2-Channel Mix Module
//

class CDAMix2Module : public CManticoreGenericModule
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDAMix2Module(void);

	// UI Management
	CViewWnd * CreateMainView(void);

	// Comms Object Access
	UINT GetObjectCount(void);
	BOOL GetObjectData(UINT uIndex, CObjectData &Data);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	CDAMix2AnalogInput         * m_pAnalogInput;
	CDAMix2AnalogInputConfig   * m_pAnalogInputConfig;
	CDAMix2AnalogOutput        * m_pAnalogOutput;
	CDAMix2AnalogOutputConfig  * m_pAnalogOutputConfig;
	CDAMix2DigitalInput        * m_pDigitalInput;
	CDAMix2DigitalInputConfig  * m_pDigitalInputConfig;
	CDAMix2DigitalOutput       * m_pDigitalOutput;
	CDAMix2DigitalOutputConfig * m_pDigitalOutputConfig;
	CDAMixDigitalConfig        * m_pDigitalConfig;

protected:
	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
