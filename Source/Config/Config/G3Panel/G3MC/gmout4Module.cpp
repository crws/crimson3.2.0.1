
#include "intern.hpp"

#include "out4mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Analog Output Module
//

class CGMOut4Module : public COut4Module
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMOut4Module(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Analog Output Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMOut4Module, COut4Module);

// Constructor

CGMOut4Module::CGMOut4Module(void)
{
	m_Ident  = LOBYTE(ID_GMOUT4);

	m_FirmID = FIRM_GMOUT4;

	m_Model  = "GMOUT4";

	m_Power  = 43;

	m_Conv.Insert(L"Legacy", L"COut4Module");

	m_Conv.Insert(L"Manticore", L"CDAAO8Module");
	}

// Download Support

void CGMOut4Module::MakeConfigData(CInitData &Init)
{
	Init.AddByte(BYTE(m_FirmID));

	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);
	}

// End of File
