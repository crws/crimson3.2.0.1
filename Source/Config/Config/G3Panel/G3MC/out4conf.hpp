
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Out4CONF_HPP

#define INCLUDE_Out4CONF_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\out4dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class COut4Conf;
class COut4ConfigWnd;
class COut4InitOutWnd;
class CUIOut4Dynamic;

//////////////////////////////////////////////////////////////////////////
//
// Out4 Configuration Item
//

class COut4Conf : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		COut4Conf(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Data Members
		UINT	m_OutType1;
		UINT	m_OutType2;
		UINT	m_OutType3;
		UINT	m_OutType4;
		UINT	m_DP1;
		UINT	m_DP2;
		UINT	m_DP3;
		UINT	m_DP4;
		INT	m_Data1;
		INT	m_Data2;
		INT	m_Data3;
		INT	m_Data4;
		INT	m_DataLo1;
		INT	m_DataLo2;
		INT	m_DataLo3;
		INT	m_DataLo4;
		INT	m_DataHi1;
		INT	m_DataHi2;
		INT	m_DataHi3;
		INT	m_DataHi4;
		INT	m_OutputLo1;
		INT	m_OutputLo2;
		INT	m_OutputLo3;
		INT	m_OutputLo4;
		INT	m_OutputHi1;
		INT	m_OutputHi2;
		INT	m_OutputHi3;
		INT	m_OutputHi4;
		UINT	m_InitData;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Out4 Configuration View
//

class COut4ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		COut4Conf * m_pItem;

		// Overidables
		void OnAttach(void);

		// UI Update
		void OnUIChange(CItem *pItem, CString Tag);	
		void OnUICreate(void);

		// UI Creation
		void AddOutput(UINT Channel);
	};

//////////////////////////////////////////////////////////////////////////
//
// Out4 Initial Output View
//

class COut4InitOutWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		COut4Conf * m_pItem;

		// Overidables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// UI Creation
		void AddInit(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

class CUIOut4Dynamic : public CUIEditBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIOut4Dynamic(void);

		// Destructor
		~CUIOut4Dynamic(void);

		// Update Support
		static void CheckUpdate(COut4Conf *pConf, CString const &Tag);

		// Operations
		void Update(BOOL fKeep);
		void UpdateUnits(void);

	protected:
		// Linked List
		static CUIOut4Dynamic * m_pHead;
		static CUIOut4Dynamic * m_pTail;

		// Data Members
		CUIOut4Dynamic * m_pNext;
		CUIOut4Dynamic * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Analog Output Module Dynamic Value
//

class CUITextOut4Dynamic : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextOut4Dynamic(void);

		// Destructor
		~CUITextOut4Dynamic(void);

	protected:
		// Data Members
		COut4Conf * m_pConf;
		char	    m_cType;

		// Core Overidables
		void OnBind(void);

		// Implementation
		void GetConfig(void);
		void FindPlaces(void);
		void FindUnits(void);
		void FindRanges(void);
		void SetMinMax(UINT OutType);
		void CheckFlags(void);

		// Friends
		friend class CUIOut4Dynamic;
	};

// End of File

#endif
