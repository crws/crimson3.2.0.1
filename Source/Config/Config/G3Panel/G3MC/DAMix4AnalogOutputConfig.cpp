
#include "intern.hpp"

#include "DAMix4AnalogOutputConfig.hpp"

#include "DAMix4AnalogOutputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daao8props.h"

#include "import/manticore/daao8dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4AnalogOutputConfig, CCommsItem);

// Property List

CCommsList const CDAMix4AnalogOutputConfig::m_CommsList[] = {

	{ 0, "Type1",		PROPID_TYPE1,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type2",		PROPID_TYPE2,		usageWriteInit,	IDS_NAME_LOT     },

	{ 1, "DataLo1",		PROPID_DATA_LO1,	usageWriteBoth,	IDS_NAME_DATALO1 },
	{ 1, "DataLo2",		PROPID_DATA_LO2,	usageWriteBoth,	IDS_NAME_DATALO2 },

	{ 1, "DataHi1",		PROPID_DATA_HI1,	usageWriteBoth,	IDS_NAME_DATAHI1 },
	{ 1, "DataHi2",		PROPID_DATA_HI2,	usageWriteBoth,	IDS_NAME_DATAHI2 },

	{ 1, "OutputLo1",	PROPID_OUT_LO1,		usageWriteBoth,	IDS_NAME_OUTLO1 },
	{ 1, "OutputLo2",	PROPID_OUT_LO2,		usageWriteBoth,	IDS_NAME_OUTLO2 },

	{ 1, "OutputHi1",	PROPID_OUT_HI1,		usageWriteBoth,	IDS_NAME_OUTHI1 },
	{ 1, "OutputHi2",	PROPID_OUT_HI2,		usageWriteBoth,	IDS_NAME_OUTHI2 },

	{ 1, "DataInit1",	PROPID_DATA_INIT1,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit2",	PROPID_DATA_INIT2,	usageWriteInit,	IDS_NAME_OUTHI1 },
};

// Constructor

CDAMix4AnalogOutputConfig::CDAMix4AnalogOutputConfig(void)
{
	m_Type1		= 4;
	m_Type2		= 4;

	m_DP1		= 3;
	m_DP2		= 3;

	m_DataLo1	= 0;
	m_DataLo2	= 0;

	m_DataHi1	= 10000;
	m_DataHi2	= 10000;

	m_OutputLo1	= 0;
	m_OutputLo2	= 0;

	m_OutputHi1	= 10000;
	m_OutputHi2	= 10000;

	m_DataInit1	= 0;
	m_DataInit2	= 0;

	m_InitData	= 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();

	m_uChans      = 2;
}

// View Pages

UINT CDAMix4AnalogOutputConfig::GetPageCount(void)
{
	return m_uChans + 1;
}

CString CDAMix4AnalogOutputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
			return CPrintf(IDS("AO %d"), n + 1);

		case 2:
			return IDS("AO Initial Value");
	}

	return L"";
}

CViewWnd * CDAMix4AnalogOutputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
			return New CDAMix4AnalogOutputConfigWnd(n);

		case 2:
			return New CDAMix4AnalogOutputConfigWnd(n);
	}

	return NULL;
}

// Group Names

CString CDAMix4AnalogOutputConfig::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1: return L"Scaling";
	}

	return CCommsItem::GetGroupName(Group);
}

// Conversion

BOOL CDAMix4AnalogOutputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 2; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Type%d", n+1), CPrintf(L"OutType%d", n+1)) ) {

				// Legacy +  Graphite

				ConvertType(n);
			}

			ImportNumber(pValue, CPrintf(L"DP%d", n+1));

			ImportNumber(pValue, CPrintf(L"DataLo%d", n+1));

			ImportNumber(pValue, CPrintf(L"DataHi%d", n+1));

			ImportNumber(pValue, CPrintf(L"OutputLo%d", n+1));

			ImportNumber(pValue, CPrintf(L"OutputHi%d", n+1));

			ImportNumber(pValue, CPrintf(L"DataInit%d", n+1), CPrintf(L"Data%d", n+1));
		}

		ImportNumber(pValue, L"InitData");

		return TRUE;
	}

	return FALSE;
}

// Property Filter

BOOL CDAMix4AnalogOutputConfig::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_DATA1:
			case PROPID_DATA2:
			case PROPID_DATA3:
			case PROPID_DATA4:
			case PROPID_DATA5:
			case PROPID_DATA6:
			case PROPID_DATA7:
			case PROPID_DATA8:

				return FALSE;
		}
	}

	return TRUE;
}

// Meta Data Creation

void CDAMix4AnalogOutputConfig::AddMetaData(void)
{
	Meta_AddInteger(Type1);
	Meta_AddInteger(Type2);

	Meta_AddInteger(DP1);
	Meta_AddInteger(DP2);

	Meta_AddInteger(DataLo1);
	Meta_AddInteger(DataLo2);

	Meta_AddInteger(DataHi1);
	Meta_AddInteger(DataHi2);

	Meta_AddInteger(OutputLo1);
	Meta_AddInteger(OutputLo2);

	Meta_AddInteger(OutputHi1);
	Meta_AddInteger(OutputHi2);

	Meta_AddInteger(DataInit1);
	Meta_AddInteger(DataInit2);

	Meta_AddInteger(InitData);

	CCommsItem::AddMetaData();
}

// Implementation

void CDAMix4AnalogOutputConfig::ConvertType(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Type%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT   uPrev = pData->ReadInteger(this);

		UINT uData[] = { 0, 1, 4, 5, 7, 9 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
