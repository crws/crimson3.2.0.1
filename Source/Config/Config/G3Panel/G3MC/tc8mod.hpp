
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TC8MOD_HPP

#define INCLUDE_TC8MOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "tc8inp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTC8BaseModule;
class CTC8Module;
class CTC8MainWnd;

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Module
//

class CTC8Module : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTC8Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Data Members
		CTC8Input * m_pInput;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Module
//

class CGMTC8Module : public CTC8Module
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMTC8Module(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Module Window
//

class CTC8MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTC8MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd   * m_pMult;
		CTC8Module      * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddTC8Pages(void);
	};

// End of File

#endif
