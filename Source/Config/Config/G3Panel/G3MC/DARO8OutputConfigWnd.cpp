
#include "intern.hpp"

#include "daro8outputconfigwnd.hpp"

#include "daro8outputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DARO8 DO Configuration Window
//

// Runtime Class

AfxImplementRuntimeClass(CDARO8OutputConfigWnd, CUIViewWnd);

// Overibables

void CDARO8OutputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDARO8OutputConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("daro8_cfg"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CDARO8OutputConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddOutputs();

	EndPage(FALSE);
	}

// Implementation

void CDARO8OutputConfigWnd::AddOutputs(void)
{
	StartTable(IDS("Control"), 1);

	AddColHead(IDS("Power On State"));

	for( UINT n = 0; n < 8; n ++ ) {

		AddRowHead(CPrintf(IDS("Output %u"), n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));
		}

	EndTable();
	}
// End of File
