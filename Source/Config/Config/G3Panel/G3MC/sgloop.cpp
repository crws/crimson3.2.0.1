
#include "intern.hpp"

#include "legacy.h"

#include "sgloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Item
//

// Runtime Class

AfxImplementRuntimeClass(CSGLoop, CCommsItem);

// Property List

CCommsList const CSGLoop::m_CommsList[] = {

	{ 1, "PV",		PROPID_PV,			usageRead,	IDS_NAME_PV	},
	{ 1, "Input1",		PROPID_INPUT1,			usageRead,	IDS_NAME_I1	},
	{ 1, "Input2",		PROPID_INPUT2,			usageRead,	IDS_NAME_I2	},
	{ 1, "Output",		PROPID_OUTPUT,			usageRead,	IDS_NAME_OUT	},
	{ 1, "RevPower",	PROPID_HEAT_POWER,		usageRead,	IDS_NAME_RPWR	},
	{ 1, "DirPower",	PROPID_COOL_POWER,		usageRead,	IDS_NAME_DPWR	},
	{ 1, "ActSP",		PROPID_ACT_SP,			usageRead,	IDS_NAME_SP	},
	{ 1, "Error",		PROPID_ERROR,			usageRead,	IDS_NAME_ERR	},
	{ 1, "AckManual",	PROPID_ACK_MANUAL,		usageRead,	IDS_NAME_AMAN	},
	{ 1, "AckTune",		PROPID_ACK_TUNE,		usageRead,	IDS_NAME_ATUNE	},
	{ 1, "TuneDone",	PROPID_TUNE_DONE,		usageRead,	IDS_NAME_TDONE	},
	{ 1, "TuneFail",	PROPID_TUNE_FAIL,		usageRead,	IDS_NAME_TFAIL  },
	{ 1, "Alarm1",		PROPID_ALARM_1,			usageRead,	IDS_NAME_A1	},
	{ 1, "Alarm2",		PROPID_ALARM_2,			usageRead,	IDS_NAME_A2	},
	{ 1, "Alarm3",		PROPID_ALARM_3,			usageRead,	IDS_NAME_A3	},
	{ 1, "Alarm4",		PROPID_ALARM_4,			usageRead,	IDS_NAME_A4	},
	{ 1, "PVAlarm",		PROPID_RANGE_ALARM,		usageRead,	IDS_NAME_PVA	},
	{ 1, "Inp1Alarm",	PROPID_IN1_ALARM,		usageRead,	IDS_NAME_I1A	},
	{ 1, "Inp2Alarm",	PROPID_IN2_ALARM,		usageRead,	IDS_NAME_I2A	},

	{ 2, "ReqSP",		PROPID_REQ_SP,			usageWriteBoth,	IDS_NAME_RSP	},
	{ 2, "Power",		PROPID_POWER,			usageWriteBoth,	IDS_NAME_PWR	},
	{ 2, "SetHyst",		PROPID_SET_HYST,		usageWriteBoth,	IDS_NAME_SHYST	},
	{ 2, "SetDead",		PROPID_SET_DEAD,		usageWriteBoth,	IDS_NAME_SDEAD	},
	{ 2, "SetRamp",		PROPID_SET_RAMP,		usageWriteBoth,	IDS_NAME_SRAMP	},
	{ 2, "SetRampBase",	PROPID_SET_RAMP_BASE,		usageWriteInit,	IDS_NAME_SRBASE	},
	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},
	{ 2, "ReqManual",	PROPID_REQ_MANUAL,		usageWriteBoth,	IDS_NAME_RMAN	},
	{ 2, "ReqTune",		PROPID_REQ_TUNE,		usageWriteBoth,	IDS_NAME_RTUNE	},
	{ 2, "ReqUserPID",	PROPID_REQ_USER_PID,		usageWriteBoth,	IDS_NAME_RUSER	},

	{ 3, "AlarmData1",	PROPID_ALARM_DATA_1,		usageWriteBoth,	IDS_NAME_AD1	},
	{ 3, "AlarmData2",	PROPID_ALARM_DATA_2,		usageWriteBoth,	IDS_NAME_AD2	},
	{ 3, "AlarmData3",	PROPID_ALARM_DATA_3,		usageWriteBoth,	IDS_NAME_AD3	},
	{ 3, "AlarmData4",	PROPID_ALARM_DATA_4,		usageWriteBoth,	IDS_NAME_AD4	},
	{ 3, "AlarmHyst1",	PROPID_ALARM_HYST_1,		usageWriteBoth,	IDS_NAME_AH1	},
	{ 3, "AlarmHyst2",	PROPID_ALARM_HYST_2,		usageWriteBoth,	IDS_NAME_AH2	},
	{ 3, "AlarmHyst3",	PROPID_ALARM_HYST_3,		usageWriteBoth,	IDS_NAME_AH3	},
	{ 3, "AlarmHyst4",	PROPID_ALARM_HYST_4,		usageWriteBoth,	IDS_NAME_AH4	},
	{ 3, "AlarmAccept1",	PROPID_ALARM_ACCEPT_1,		usageWriteBoth,	IDS_NAME_AA1	},
	{ 3, "AlarmAccept2",	PROPID_ALARM_ACCEPT_2,		usageWriteBoth,	IDS_NAME_AA2	},
	{ 3, "AlarmAccept3",	PROPID_ALARM_ACCEPT_3,		usageWriteBoth,	IDS_NAME_AA3	},
	{ 3, "AlarmAccept4",	PROPID_ALARM_ACCEPT_4,		usageWriteBoth,	IDS_NAME_AA4	},
	{ 3, "InputAccept",	PROPID_RANGE_ACCEPT,		usageWriteBoth,	IDS_NAME_IAC	},
	
	{ 4, "TuneCode",	PROPID_TUNE_CODE,		usageWriteBoth,	IDS_NAME_TCODE	},
	{ 4, "UserConstP",	PROPID_USER_CONST_P,		usageWriteBoth,	IDS_NAME_UCP	},
	{ 4, "UserConstI",	PROPID_USER_CONST_I,		usageWriteBoth,	IDS_NAME_UCI	},
	{ 4, "UserConstD",	PROPID_USER_CONST_D,		usageWriteBoth,	IDS_NAME_UCD	},
	{ 4, "UserCLimit",	PROPID_USER_CLIMIT,		usageWriteInit,	IDS_NAME_UCL	},
	{ 4, "UserHLimit",	PROPID_USER_HLIMIT,		usageWriteInit,	IDS_NAME_UHL	},
	{ 4, "UserFilter",	PROPID_USER_FILTER,		usageWriteBoth,	IDS_NAME_UF	},
	{ 4, "AutoConstP",	PROPID_AUTO_CONST_P,		usageRead,	IDS_NAME_ACP	},
	{ 4, "AutoConstI",	PROPID_AUTO_CONST_I,		usageRead,	IDS_NAME_ACI	},
	{ 4, "AutoConstD",	PROPID_AUTO_CONST_D,		usageRead,	IDS_NAME_ACD	},
	{ 4, "AutoFilter",	PROPID_AUTO_FILTER,		usageRead,	IDS_NAME_AF	},
	{ 4, "ActConstP",	PROPID_SEL_CONST_P,		usageRead,	IDS_NAME_CP	},
	{ 4, "ActConstI",	PROPID_SEL_CONST_I,		usageRead,	IDS_NAME_CI	},
	{ 4, "ActConstD",	PROPID_SEL_CONST_D,		usageRead,	IDS_NAME_CD	},
	{ 4, "ActFilter",	PROPID_SEL_FILTER,		usageRead,	IDS_NAME_F	},

	{ 5, "PowerFault",	PROPID_POWER_FAULT,		usageWriteBoth,	IDS_NAME_PF	},
	{ 5, "PowerOffset",	PROPID_POWER_OFFSET,		usageWriteBoth,	IDS_NAME_PO	},
	{ 5, "PowerDead",	PROPID_POWER_DEAD,		usageWriteBoth,	IDS_NAME_PD	},
	{ 5, "PowerRevGain",	PROPID_POWER_HEAT_GAIN,		usageWriteBoth,	IDS_NAME_PRG	},
	{ 5, "PowerDirGain",	PROPID_POWER_COOL_GAIN,		usageWriteBoth,	IDS_NAME_PDG	},
	{ 5, "PowerRevHyst",	PROPID_POWER_HEAT_HYST,		usageWriteBoth,	IDS_NAME_PRH	},
	{ 5, "PowerDirHyst",	PROPID_POWER_COOL_HYST,		usageWriteBoth,	IDS_NAME_PDH	},
	{ 5, "RevLimitLo",	PROPID_HEAT_LIMIT_LO,		usageWriteBoth,	IDS_NAME_RLL	},
	{ 5, "RevLimitHi",	PROPID_HEAT_LIMIT_HI,		usageWriteBoth,	IDS_NAME_RLH	},
	{ 5, "DirLimitLo",	PROPID_COOL_LIMIT_LO,		usageWriteBoth,	IDS_NAME_DLL	},
	{ 5, "DirLimitHi",	PROPID_COOL_LIMIT_HI,		usageWriteBoth,	IDS_NAME_DLH	},

	{ 6, "DispLo1",		PROPID_DISPLO_1,		usageWriteBoth,	IDS_NAME_DL1	},
	{ 6, "DispHi1",		PROPID_DISPHI_1,		usageWriteBoth,	IDS_NAME_DH1	},
	{ 6, "SigLoKey1",	PROPID_SIGLOKEY_1,		usageWriteBoth,	IDS_NAME_SLK1	},
	{ 6, "SigHiKey1",	PROPID_SIGHIKEY_1,		usageWriteBoth,	IDS_NAME_SHK1	},
	{ 6, "SigLoApp1",	PROPID_SIGLOAPP_1,		usageRead,	IDS_NAME_SLA1	},
	{ 6, "SigHiApp1",	PROPID_SIGHIAPP_1,		usageRead,	IDS_NAME_SHA1	},
	{ 6, "StoreSigLo1",	PROPID_APPLY_SIGLO_1,		usageWriteUser,	IDS_NAME_ASL1	},
	{ 6, "StoreSigHi1",	PROPID_APPLY_SIGHI_1,		usageWriteUser,	IDS_NAME_ASH1	},
	{ 6, "SelectScaling1",	PROPID_SEL_SIG_1,		usageWriteBoth,	IDS_NAME_SELSIG1 },

	{ 7, "DispLo2",		PROPID_DISPLO_2,		usageWriteBoth,	IDS_NAME_DL2	},
	{ 7, "DispHi2",		PROPID_DISPHI_2,		usageWriteBoth,	IDS_NAME_DH2	},
	{ 7, "SigLoKey2",	PROPID_SIGLOKEY_2,		usageWriteBoth,	IDS_NAME_SLK2	},
	{ 7, "SigHiKey2",	PROPID_SIGHIKEY_2,		usageWriteBoth,	IDS_NAME_SHK2	},
	{ 7, "SigLoApp2",	PROPID_SIGLOAPP_2,		usageRead,	IDS_NAME_SLA2	},
	{ 7, "SigHiApp2",	PROPID_SIGHIAPP_2,		usageRead,	IDS_NAME_SHA2	},
	{ 7, "StoreSigLo2",	PROPID_APPLY_SIGLO_2,		usageWriteUser,	IDS_NAME_ASL2	},
	{ 7, "StoreSigHi2",	PROPID_APPLY_SIGHI_2,		usageWriteUser,	IDS_NAME_ASH2	},
	{ 7, "SelectScaling2",	PROPID_SEL_SIG_2,		usageWriteBoth,	IDS_NAME_SELSIG2 },

	{ 8, "PVPeak",		PROPID_PV_PEAK,			usageRead,	IDS_NAME_PVPEAK	},
	{ 8, "PVVall",		PROPID_PV_VALLEY,		usageRead,	IDS_NAME_PVVALL	},
	{ 8, "ResetPkVall",	PROPID_RST_PKVALL,		usageWriteUser,	IDS_NAME_RST_PKVALL },
	{ 8, "ResetPeak",	PROPID_RST_PEAK,		usageWriteUser,	IDS_NAME_RST_PEAK },
	{ 8, "ResetVall",	PROPID_RST_VALLEY,		usageWriteUser,	IDS_NAME_RST_VALLEY },

	{ 9, "PVGross",		PROPID_PV_GROSS,		usageRead,	IDS_NAME_PVGROSS },
	{ 9, "Inp1Gross",	PROPID_IN1_GROSS,		usageRead,	IDS_NAME_IN1GROSS },
	{ 9, "Inp2Gross",	PROPID_IN2_GROSS,		usageRead,	IDS_NAME_IN2GROSS },
	{ 9, "PVTareTot",	PROPID_PV_TARE_TOT,		usageRead,	IDS_NAME_PVTT   },
	{ 9, "Inp1TareTot",	PROPID_IN1_TARE_TOT,		usageRead,	IDS_NAME_IN1TT	},
	{ 9, "Inp2TareTot",	PROPID_IN2_TARE_TOT,		usageRead,	IDS_NAME_IN2TT	},
	{ 9, "TarePV",		PROPID_TARE_PV,			usageWriteUser,	IDS_NAME_TAREPV	},
	{ 9, "TareInp1",	PROPID_TARE_IN1,		usageWriteUser,	IDS_NAME_TAREIN1 },
	{ 9, "TareInp2",	PROPID_TARE_IN2,		usageWriteUser,	IDS_NAME_TAREIN2 },
	{ 9, "RstPVTareTot",	PROPID_RST_PVTT,		usageWriteUser,	IDS_NAME_RST_PVTT },
	{ 9, "RstIn1TareTot",	PROPID_RST_IN1TT,		usageWriteUser,	IDS_NAME_RST_IN1TT },
	{ 9, "RstIn2TareTot",	PROPID_RST_IN2TT,		usageWriteUser,	IDS_NAME_RST_IN2TT },

	{ 0, "Mode",		PROPID_MODE,			usageWriteInit,	IDS_NAME_MODE	},
	{ 0, "DigHeat",		PROPID_DIG_HEAT,		usageWriteInit,	IDS_NAME_DHEAT	},
	{ 0, "DigCool",		PROPID_DIG_COOL,		usageWriteInit,	IDS_NAME_DCOOL	},
	{ 0, "InputRange1",	PROPID_SG_IN_RANGE1,		usageWriteInit,	IDS_NAME_IT	},
	{ 0, "InputRange2",	PROPID_SG_IN_RANGE2,		usageWriteInit,	IDS_NAME_IT	},
	{ 0, "ExcitOut1",	PROPID_SG_EXCIT1,		usageWriteInit,	IDS_NAME_EXCIT	},
	{ 0, "ExcitOut2",	PROPID_SG_EXCIT2,		usageWriteInit,	IDS_NAME_EXCIT	},
	{ 0, "PVAssign",	PROPID_SG_PV_ASSIGN,		usageWriteInit,	IDS_NAME_PVASSIGN },
	{ 0, "PVLimitLo",	PROPID_PV_LIMIT_LO,		usageWriteInit,	IDS_NAME_PVLL	},
	{ 0, "PVLimitHi",	PROPID_PV_LIMIT_HI,		usageWriteInit,	IDS_NAME_PVLH	},
	{ 0, "AlarmAssign1",	PROPID_ALARM_ASSIGN_1,		usageWriteInit,	IDS_NAME_ALASS1	},
	{ 0, "AlarmAssign2",	PROPID_ALARM_ASSIGN_2,		usageWriteInit,	IDS_NAME_ALASS2	},
	{ 0, "AlarmAssign3",	PROPID_ALARM_ASSIGN_3,		usageWriteInit,	IDS_NAME_ALASS3	},
	{ 0, "AlarmAssign4",	PROPID_ALARM_ASSIGN_4,		usageWriteInit,	IDS_NAME_ALASS4	},
	{ 0, "AlarmMode1",	PROPID_ALARM_MODE_1,		usageWriteInit,	IDS_NAME_AM1	},
	{ 0, "AlarmMode2",	PROPID_ALARM_MODE_2,		usageWriteInit,	IDS_NAME_AM2	},
	{ 0, "AlarmMode3",	PROPID_ALARM_MODE_3,		usageWriteInit,	IDS_NAME_AM3	},
	{ 0, "AlarmMode4",	PROPID_ALARM_MODE_4,		usageWriteInit,	IDS_NAME_AM4	},
	{ 0, "AlarmDelay1",	PROPID_ALARM_DELAY_1,		usageWriteInit,	IDS_NAME_ADE1	},
	{ 0, "AlarmDelay2",	PROPID_ALARM_DELAY_2,		usageWriteInit,	IDS_NAME_ADE2	},
	{ 0, "AlarmDelay3",	PROPID_ALARM_DELAY_3,		usageWriteInit,	IDS_NAME_ADE3	},
	{ 0, "AlarmDelay4",	PROPID_ALARM_DELAY_4,		usageWriteInit,	IDS_NAME_ADE4	},
	{ 0, "AlarmLatch1",	PROPID_ALARM_LATCH_1,		usageWriteInit,	IDS_NAME_ALA1	},
	{ 0, "AlarmLatch2",	PROPID_ALARM_LATCH_2,		usageWriteInit,	IDS_NAME_ALA2	},
	{ 0, "AlarmLatch3",	PROPID_ALARM_LATCH_3,		usageWriteInit,	IDS_NAME_ALA3	},
	{ 0, "AlarmLatch4",	PROPID_ALARM_LATCH_4,		usageWriteInit,	IDS_NAME_ALA4	},
	{ 0, "RangeLatch",	PROPID_RANGE_LATCH,		usageWriteInit,	IDS_NAME_RL	},
	{ 0, "FaultAssign",	PROPID_FAULT_ASSIGN,		usageWriteInit,	IDS_NAME_FAULT	},

	};

// Constructor

CSGLoop::CSGLoop(void)
{
	m_ProcUnits	= "%";
	m_DispLo1	= -10000;
	m_DispHi1	=  10000;
	m_SigLoKey1	=      0;
	m_SigHiKey1	=  60000;
	m_DispLo2	= -10000;
	m_DispHi2	=  10000;
	m_SigLoKey2	=      0;
	m_SigHiKey2	=  60000;
	m_ProcDP	= 2;
	m_Mode		= MODE_HEAT;
	m_DigHeat	= FALSE;
	m_DigCool	= FALSE;
	m_InputRange1	= SG_INPUT_20MV;
	m_InputRange2	= SG_INPUT_20MV;
	m_ExcitOut1	= SG_EXCIT_5V;
	m_ExcitOut2	= SG_EXCIT_5V;
	m_PVAssign	= PVASS_IN1;
	m_PVLimitLo	= -10000;
	m_PVLimitHi	=  10000;
	m_AlarmAssign1	= ALM_ASS_IN1;
	m_AlarmAssign2	= ALM_ASS_IN1;
	m_AlarmAssign3	= ALM_ASS_IN1;
	m_AlarmAssign4	= ALM_ASS_IN1;
	m_AlarmMode1	= ALARM_NULL;
	m_AlarmMode2	= ALARM_NULL;
	m_AlarmMode3	= ALARM_NULL;
	m_AlarmMode4	= ALARM_NULL;
	m_AlarmDelay1	= FALSE;
	m_AlarmDelay2	= FALSE;
	m_AlarmDelay3	= FALSE;
	m_AlarmDelay4	= FALSE;
	m_AlarmLatch1	= FALSE;
	m_AlarmLatch2	= FALSE;
	m_AlarmLatch3	= FALSE;
	m_AlarmLatch4	= FALSE;
	m_RangeLatch	= FALSE;
	m_TuneCode	= 2;
	m_InitData	= FALSE;
	m_DnLoadScaling	= 1;
	m_ReqManual	= FALSE;
	m_ReqTune	= FALSE;
	m_ReqUserPID	= FALSE;
	m_SelectScaling1 = FALSE;
	m_SelectScaling2 = FALSE;
	m_AlarmAccept1	= FALSE;
	m_AlarmAccept2	= FALSE;
	m_AlarmAccept3	= FALSE;
	m_AlarmAccept4	= FALSE;
	m_InputAccept	= FALSE;
	m_Power		= 5000;
	m_ReqSP		= 150000;
	m_SetHyst	= 1500;
	m_SetDead	= 0;
	m_SetRampBase	= 0;
	m_SetRamp	= 0;
	m_InputFilter	= 10;
	m_UserConstP	= 40;
	m_UserConstI	= 1200;
	m_UserConstD	= 300;
	m_UserCLimit	= 10000;
	m_UserHLimit	= 10000;
	m_UserFilter	= 10;
	m_PowerFault	= 0;
	m_FaultAssign	= INPUT_FAULT_IN1;
	m_PowerOffset	= 0;
	m_PowerDead	= 0;
	m_PowerRevGain	= 10000;
	m_PowerDirGain	= 10000;
	m_PowerRevHyst	= 2000;
	m_PowerDirHyst	= 2000;
	m_RevLimitLo	= 0;
	m_RevLimitHi	= 10000;
	m_DirLimitLo	= 0;;
	m_DirLimitHi	= 10000;
	m_AlarmData1	= 0;
	m_AlarmData2	= 0;
	m_AlarmData3	= 0;
	m_AlarmData4	= 0;
	m_AlarmHyst1	= 0;
	m_AlarmHyst2	= 0;
	m_AlarmHyst3	= 0;
	m_AlarmHyst4	= 0;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CSGLoop::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_STATUS);

		case 2:	return CString(IDS_MODULE_CTRL);

		case 3:	return CString(IDS_MODULE_ALARMS);

		case 4:	return CString(IDS_MODULE_PID);

		case 5:	return CString(IDS_MODULE_POWER);

		case 6: return CString(IDS_MODULE_SCALE1);

		case 7: return CString(IDS_MODULE_SCALE2);

		case 8:	return CString(IDS_MODULE_PKVALL);

		case 9:	return CString(IDS_MODULE_TARE);

		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CSGLoop::GetPageCount(void)
{
	return 4;
	}

CString CSGLoop::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_GEN);

		case 1: return CString(IDS_MODULE_CTRL);
		
		case 2: return CString(IDS_MODULE_POWER);
		
		case 3: return CString(IDS_MODULE_ALARMS);

		}

	return L"";
	}

CViewWnd * CSGLoop::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CSGLoopGeneralWnd;
		
		case 1: return New CSGLoopControlWnd;
		
		case 2: return New CSGLoopPowerWnd;
		
		case 3: return New CSGLoopAlarmWnd;

		}

	return NULL;
	}

// Property Filter

BOOL CSGLoop::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_USER_CONST_P:
			case PROPID_USER_CONST_I:
			case PROPID_USER_CONST_D:
			case PROPID_USER_HLIMIT:
			case PROPID_USER_CLIMIT:
			case PROPID_USER_FILTER:
			case PROPID_REQ_MANUAL:
			case PROPID_REQ_SP:
			case PROPID_POWER:
				
				return FALSE;
			}
		}

	if( !(m_DnLoadScaling & 1) ) {

		switch( PropID ) {

			case PROPID_SG_IN_RANGE1:
			case PROPID_SG_EXCIT1:
			case PROPID_DISPLO_1:
			case PROPID_DISPHI_1:
			case PROPID_SIGLOKEY_1:
			case PROPID_SIGHIKEY_1:
			case PROPID_SEL_SIG_1:

				return FALSE;
			}
		}

	if( !(m_DnLoadScaling & 2) ) {

		switch( PropID ) {

			case PROPID_SG_IN_RANGE2:
			case PROPID_SG_EXCIT2:
			case PROPID_DISPLO_2:
			case PROPID_DISPHI_2:
			case PROPID_SIGLOKEY_2:
			case PROPID_SIGHIKEY_2:
			case PROPID_SEL_SIG_2:

				return FALSE;
			}
		}

	return TRUE;
	}

// Data Scaling

DWORD CSGLoop::GetIntProp(PCTXT pTag)
{
	if( IsTenTimes(pTag) ) {

		CMetaData const *pMeta = FindMetaData(pTag);

		if( pTag ) {

			INT nData = pMeta->ReadInteger(this);

			if( nData > 0 ) {

				nData = (nData + 5) / 10;
				}

			if( nData < 0 ) {

				nData = (nData - 5) / 10;
				}

			return DWORD(nData);
			}

		return 0;
		}

	return CCommsItem::GetIntProp(pTag);
	}

BOOL CSGLoop::IsTenTimes(CString Tag)
{
	if( Tag == "ReqSP" ) {
		
		return TRUE;
		}
	
	if( Tag == "SetHyst" ) {
		
		return TRUE;
		}

	if( Tag == "SetDead" ) {
		
		return TRUE;
		}

	if( Tag == "SetRamp" ) {
		
		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CSGLoop::AddMetaData(void)
{
	Meta_AddString (ProcUnits);
	Meta_AddInteger(DispLo1);
	Meta_AddInteger(DispHi1);
	Meta_AddInteger(SigLoKey1);
	Meta_AddInteger(SigHiKey1);
	Meta_AddInteger(DispLo2);
	Meta_AddInteger(DispHi2);
	Meta_AddInteger(SigLoKey2);
	Meta_AddInteger(SigHiKey2);
	Meta_AddInteger(ProcDP);
	Meta_AddInteger(Mode);
	Meta_AddInteger(DigHeat);
	Meta_AddInteger(DigCool);
	Meta_AddInteger(InputRange1);
	Meta_AddInteger(InputRange2);
	Meta_AddInteger(ExcitOut1);
	Meta_AddInteger(ExcitOut2);
	Meta_AddInteger(PVAssign);
	Meta_AddInteger(PVLimitLo);
	Meta_AddInteger(PVLimitHi);
	Meta_AddInteger(AlarmAssign1);
	Meta_AddInteger(AlarmAssign2);
	Meta_AddInteger(AlarmAssign3);
	Meta_AddInteger(AlarmAssign4);
	Meta_AddInteger(AlarmMode1);
	Meta_AddInteger(AlarmMode2);
	Meta_AddInteger(AlarmMode3);
	Meta_AddInteger(AlarmMode4);
	Meta_AddInteger(AlarmDelay1);
	Meta_AddInteger(AlarmDelay2);
	Meta_AddInteger(AlarmDelay3);
	Meta_AddInteger(AlarmDelay4);
	Meta_AddInteger(AlarmLatch1);
	Meta_AddInteger(AlarmLatch2);
	Meta_AddInteger(AlarmLatch3);
	Meta_AddInteger(AlarmLatch4);
	Meta_AddInteger(RangeLatch);
	Meta_AddInteger(TuneCode);
	Meta_AddInteger(InitData);
	Meta_AddInteger(DnLoadScaling);
	Meta_AddInteger(ReqManual);
	Meta_AddInteger(ReqTune);
	Meta_AddInteger(ReqUserPID);
	Meta_AddInteger(SelectScaling1);
	Meta_AddInteger(SelectScaling2);
	Meta_AddInteger(AlarmAccept1);
	Meta_AddInteger(AlarmAccept2);
	Meta_AddInteger(AlarmAccept3);
	Meta_AddInteger(AlarmAccept4);
	Meta_AddInteger(InputAccept);
	Meta_AddInteger(Power);
	Meta_AddInteger(ReqSP);
	Meta_AddInteger(SetHyst);
	Meta_AddInteger(SetDead);
	Meta_AddInteger(SetRampBase);
	Meta_AddInteger(SetRamp);
	Meta_AddInteger(InputFilter);
	Meta_AddInteger(UserConstP);
	Meta_AddInteger(UserConstI);
	Meta_AddInteger(UserConstD);
	Meta_AddInteger(UserCLimit);
	Meta_AddInteger(UserHLimit);
	Meta_AddInteger(UserFilter);
	Meta_AddInteger(PowerFault);
	Meta_AddInteger(FaultAssign);
	Meta_AddInteger(PowerOffset);
	Meta_AddInteger(PowerDead);
	Meta_AddInteger(PowerRevGain);
	Meta_AddInteger(PowerDirGain);
	Meta_AddInteger(PowerRevHyst);
	Meta_AddInteger(PowerDirHyst);
	Meta_AddInteger(RevLimitLo);
	Meta_AddInteger(RevLimitHi);
	Meta_AddInteger(DirLimitLo);
	Meta_AddInteger(DirLimitHi);
	Meta_AddInteger(AlarmData1);
	Meta_AddInteger(AlarmData2);
	Meta_AddInteger(AlarmData3);
	Meta_AddInteger(AlarmData4);
	Meta_AddInteger(AlarmHyst1);
	Meta_AddInteger(AlarmHyst2);
	Meta_AddInteger(AlarmHyst3);
	Meta_AddInteger(AlarmHyst4);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop General View
//

// Runtime Class

AfxImplementRuntimeClass(CSGLoopGeneralWnd, CUIViewWnd);

// Overidables

void CSGLoopGeneralWnd::OnAttach(void)
{
	m_pItem   = (CSGLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("sgloop"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CSGLoopGeneralWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	if( Tag == "DnLoadScaling" ) {

		EnableScaling();
		}

	if( Tag == "InitData" ) {

		EnableInit();
		}

	if( Tag == "DigHeat" ) {

		EnableHeat();
		}

	if( Tag == "DigCool" ) {

		EnableCool();
		}

	CUISGProcess::CheckUpdate(m_pItem, Tag);
	}

void CSGLoopGeneralWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	AddOperation();

	AddInitial();

	AddSmart();

	EndPage(TRUE);
	}

// UI Creation

void CSGLoopGeneralWnd::AddOperation(void)
{
	StartGroup(CString(IDS_MODULE_OP), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("Mode"       ));
	AddUI(m_pItem, TEXT("root"), TEXT("PVAssign"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("PVLimitLo"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("PVLimitHi"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));
	AddUI(m_pItem, TEXT("root"), TEXT("ProcUnits"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("ProcDP"     ));

	EndGroup(TRUE);
	}

void CSGLoopGeneralWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 2);

	AddColHead(CPrintf(CString(IDS_MODULE_INPUT), 1));
	AddColHead(CPrintf(CString(IDS_MODULE_INPUT), 2));

	AddRowHead(CString(IDS_NAME_RANGE_MV));

	AddUI(m_pItem, TEXT("root"), TEXT("InputRange1"));
	AddUI(m_pItem, TEXT("root"), TEXT("InputRange2"));

	AddRowHead(CString(IDS_NAME_EXCIT));

	AddUI(m_pItem, TEXT("root"), TEXT("ExcitOut1"));
	AddUI(m_pItem, TEXT("root"), TEXT("ExcitOut2"));

	AddRowHead(CString(IDS_NAME_DL));

	AddUI(m_pItem, TEXT("root"), TEXT("DispLo1"));
	AddUI(m_pItem, TEXT("root"), TEXT("DispLo2"));

	AddRowHead(CString(IDS_NAME_DH));
	
	AddUI(m_pItem, TEXT("root"), TEXT("DispHi1"));
	AddUI(m_pItem, TEXT("root"), TEXT("DispHi2"));

	AddRowHead(CString(IDS_NAME_SLK));
	
	AddUI(m_pItem, TEXT("root"), TEXT("SigLoKey1"));
	AddUI(m_pItem, TEXT("root"), TEXT("SigLoKey2"));

	AddRowHead(CString(IDS_NAME_SHK));

	AddUI(m_pItem, TEXT("root"), TEXT("SigHiKey1"));
	AddUI(m_pItem, TEXT("root"), TEXT("SigHiKey2"));

	EndTable();
	}

void CSGLoopGeneralWnd::AddInitial(void)
{
	StartGroup(CString(IDS_MODULE_INIT), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("InitData"     ));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqUserPID"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqManual"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("ReqSP"	 ));
	AddUI(m_pItem, TEXT("root"), TEXT("Power"	 ));
	AddUI(m_pItem, TEXT("root"), TEXT("DnLoadScaling"));

	EndGroup(TRUE);
	}

void CSGLoopGeneralWnd::AddSmart(void)
{
	StartGroup(CPrintf(CString(IDS_MODULE_SMART), 174), 2);

	AddUI(m_pItem, TEXT("root"), TEXT("DigHeat"     ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerRevHyst"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigCool"     ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerDirHyst"));

	EndGroup(TRUE);
	}

// Enabling

void CSGLoopGeneralWnd::DoEnables(void)
{
	EnableHeat();

	EnableCool();

	EnableInit();

	EnableScaling();
	}

void CSGLoopGeneralWnd::EnableHeat(void)
{
	BOOL fEnable = m_pItem->m_DigHeat;

	EnableUI("PowerRevHyst", fEnable);
	}

void CSGLoopGeneralWnd::EnableCool(void)
{
	BOOL fEnable = m_pItem->m_DigCool;

	EnableUI("PowerDirHyst", fEnable);
	}

void CSGLoopGeneralWnd::EnableInit(void)
{
	BOOL fEnable = m_pItem->m_InitData;

	EnableUI("ReqManual",  fEnable);
	EnableUI("ReqUserPID", fEnable);
	EnableUI("ReqSP",      fEnable);
	EnableUI("Power",      fEnable);
	}

void CSGLoopGeneralWnd::EnableScaling(void)
{
	UINT DownLoad = m_pItem->m_DnLoadScaling;

	BOOL fEnable1 = (DownLoad & 1) ? TRUE : FALSE;

	BOOL fEnable2 = (DownLoad & 2) ? TRUE : FALSE;

	EnableUI("InputRange1", fEnable1);
	EnableUI("ExcitOut1",   fEnable1);
	EnableUI("DispLo1",     fEnable1);
	EnableUI("DispHi1",     fEnable1);
	EnableUI("SigLoKey1",   fEnable1);
	EnableUI("SigHiKey1",   fEnable1);

	EnableUI("InputRange2", fEnable2);
	EnableUI("ExcitOut2",   fEnable2);
	EnableUI("DispLo2",     fEnable2);
	EnableUI("DispHi2",     fEnable2);
	EnableUI("SigLoKey2",   fEnable2);
	EnableUI("SigHiKey2",   fEnable2);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Power View
//

// Runtime Class

AfxImplementRuntimeClass(CSGLoopPowerWnd, CUIViewWnd);

// Overidables

void CSGLoopPowerWnd::OnAttach(void)
{
	m_fScroll = FALSE;

	m_pItem   = (CSGLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("sgloop"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CSGLoopPowerWnd::OnUIChange(CItem *pItem, CString Tag)
{
	m_pPower->Update();

	CUISGProcess::CheckUpdate(m_pItem, Tag);
	}

void CSGLoopPowerWnd::OnUICreate(void)
{
	StartPage(1);

	AddPower();

	AddGraph();

	EndPage(TRUE);
	}

// Message Map

AfxMessageMap(CSGLoopPowerWnd, CUIViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxDispatchMessage(WM_ERASEBKGND)

	AfxMessageEnd(CSGLoopPowerWnd)
	};

// Message Handlers

BOOL CSGLoopPowerWnd::OnEraseBkGnd(CDC &DC)
{
	DC.Save();

	m_pPower->Exclude(ThisObject, DC);

	CRect Rect = GetClientRect();

	DC.FillRect(Rect, afxBrush(TabFace));

	DC.Restore();

	return TRUE;
	}

// UI Creation

void CSGLoopPowerWnd::AddPower(void)
{
	StartGroup(CString(IDS_MODULE_PWR_TRANS), 2);

	AddUI(m_pItem, TEXT("root"), TEXT("PowerOffset" ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerDead"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerRevGain"));
	AddUI(m_pItem, TEXT("root"), TEXT("PowerDirGain"));
	AddUI(m_pItem, TEXT("root"), TEXT("RevLimitLo"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("DirLimitLo"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("RevLimitHi"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("DirLimitHi"  ));

	EndGroup(TRUE);
	}

void CSGLoopPowerWnd::AddGraph(void)
{
	StartGroup(CString(IDS_MODULE_PWR_GRAPH), 1, FALSE);

	m_pPower = New CUISGPower(m_pItem);

	AddElement(m_pPower);

	EndGroup(TRUE);
	}

// Enabling

void CSGLoopPowerWnd::DoEnables(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Control View
//

// Runtime Class

AfxImplementRuntimeClass(CSGLoopControlWnd, CUIViewWnd);

// Overidables

void CSGLoopControlWnd::OnAttach(void)
{
	m_pItem   = (CSGLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("sgloop"));

	CUIViewWnd::OnAttach();
 	}

// UI Update

void CSGLoopControlWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "SetRampBase" ) {

		DoEnables();
		}

	CUISGProcess::CheckUpdate(m_pItem, Tag);
	}

void CSGLoopControlWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddAuto();

	AddPID();

	EndPage(TRUE);
	}

// UI Creation

void CSGLoopControlWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_SP), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("SetRampBase"));
	AddUI(m_pItem, TEXT("root"), TEXT("SetRamp"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("SetHyst"    ));
	AddUI(m_pItem, TEXT("root"), TEXT("SetDead"    ));

	EndGroup(TRUE);
	}

void CSGLoopControlWnd::AddAuto(void)
{
	StartGroup(CString(IDS_MODULE_AUTO_SET), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("TuneCode"));

	EndGroup(TRUE);
	}

void CSGLoopControlWnd::AddPID(void)
{
	StartGroup(CString(IDS_MODULE_PID_USET), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("UserConstP"));
	AddUI(m_pItem, TEXT("root"), TEXT("UserConstI"));
	AddUI(m_pItem, TEXT("root"), TEXT("UserConstD"));
	AddUI(m_pItem, TEXT("root"), TEXT("UserFilter"));

	EndGroup(TRUE);
	}

// Enabling

void CSGLoopControlWnd::DoEnables(void)
{
	BOOL fRamp = (m_pItem->m_SetRampBase > 0);

	EnableUI("SetRamp", fRamp);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Alarm View
//

// Runtime Class

AfxImplementRuntimeClass(CSGLoopAlarmWnd, CUIViewWnd);

// Overidables

void CSGLoopAlarmWnd::OnAttach(void)
{
	m_pItem   = (CSGLoop *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("sgloop"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CSGLoopAlarmWnd::OnUIChange(CItem *pItem, CString Tag)
{
	for( UINT n = 1; n <= 4; n++ ) {

		if( Tag.IsEmpty() || Tag == CPrintf("AlarmMode%u", n) ) {

			EnableAlarm(n);
			}
		}

	CUISGProcess::CheckUpdate(m_pItem, Tag);
	}

void CSGLoopAlarmWnd::OnUICreate(void)
{
	StartPage(1);

	AddAlarms();

	AddInput();

	EndPage(TRUE);
	}

// UI Creation

void CSGLoopAlarmWnd::AddAlarms(void)
{
	for( UINT n = 1; n <= 4; n++ ) {

		AddAlarm(n);
		}
	}

void CSGLoopAlarmWnd::AddAlarm(UINT n)
{
	StartGroup(CPrintf(CString(IDS_MODULE_ALARM), n), 3);

	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmMode%u",   n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmData%u",   n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmLatch%u",  n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmAssign%u", n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmHyst%u",   n));
	AddUI(m_pItem, TEXT("root"), CPrintf("AlarmDelay%u",  n));

	EndGroup(TRUE);
	}

void CSGLoopAlarmWnd::AddInput(void)
{
	StartGroup(CString(IDS_MODULE_INP_FAULT), 3);

	AddUI(m_pItem, TEXT("root"), TEXT("PowerFault" ));
	AddUI(m_pItem, TEXT("root"), TEXT("FaultAssign"));
	AddUI(m_pItem, TEXT("root"), TEXT("RangeLatch" ));

	EndGroup(TRUE);
	}

// Enabling

void CSGLoopAlarmWnd::DoEnables(void)
{
	for( UINT n = 1; n <= 4; n++ ) {

		EnableAlarm(n);
		}
	}

void CSGLoopAlarmWnd::EnableAlarm(UINT n)
{
	BOOL fEnable = FALSE;

	switch( n ) {
		
		case 1: fEnable = m_pItem->m_AlarmMode1; break;
		case 2: fEnable = m_pItem->m_AlarmMode2; break;
		case 3: fEnable = m_pItem->m_AlarmMode3; break;
		case 4: fEnable = m_pItem->m_AlarmMode4; break;

		}

	EnableUI(CPrintf("AlarmData%u",   n), fEnable);
	EnableUI(CPrintf("AlarmLatch%u",  n), fEnable);
	EnableUI(CPrintf("AlarmAssign%u", n), fEnable);
	EnableUI(CPrintf("AlarmHyst%u",   n), fEnable);
	EnableUI(CPrintf("AlarmDelay%u",  n), fEnable);
	}

// End of File
