
#include "intern.hpp"

#include "DAMix4DigitalOutputConfigWnd.hpp"

#include "DAMix4DigitalOutputConfig.hpp"

#include "DAMix4Module.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4DigitalOutputConfigWnd, CUIViewWnd);

// Overibables

void CDAMix4DigitalOutputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMix4DigitalOutputConfig *) CViewWnd::m_pItem;

	m_pModule = (CDAMix4Module *) m_pItem->GetParent(AfxRuntimeClass(CDAMix4Module));

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dadio_out"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix4DigitalOutputConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddOutputs();

	EndPage(FALSE);
}

void CDAMix4DigitalOutputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(1);
		DoEnables(2);
		DoEnables(3);
	}
}

// Implementation

void CDAMix4DigitalOutputConfigWnd::AddOutputs(void)
{
	StartTable(IDS("Control"), 1);

	AddColHead(IDS("Power On State"));

	for( UINT n = 0; n < 3; n++ ) {

		AddRowHead(CPrintf(IDS("Output %u"), n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));
	}

	EndTable();
}

void CDAMix4DigitalOutputConfigWnd::DoEnables(UINT uIndex)
{
	BOOL fMode = GetInteger(m_pModule->m_pDigitalConfig, CPrintf(L"ChanMode%u", uIndex));

	EnableUI(CPrintf(L"Mode%u", uIndex), fMode == 0);
}

// Data Access

UINT CDAMix4DigitalOutputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

// End of File
