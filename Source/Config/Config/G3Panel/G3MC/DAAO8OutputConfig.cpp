
#include "intern.hpp"

#include "DAAO8OutputConfig.hpp"

#include "DAAO8OutputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daao8props.h"

#include "import/manticore/daao8dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 AI Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAAO8OutputConfig, CCommsItem);

// Property List

CCommsList const CDAAO8OutputConfig::m_CommsList[] = {

	{ 0, "Type1",		PROPID_TYPE1,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type2",		PROPID_TYPE2,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type3",		PROPID_TYPE3,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type4",		PROPID_TYPE4,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type5",		PROPID_TYPE5,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type6",		PROPID_TYPE6,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type7",		PROPID_TYPE7,		usageWriteInit,	IDS_NAME_LOT     },
	{ 0, "Type8",		PROPID_TYPE8,		usageWriteInit,	IDS_NAME_LOT     },

	{ 1, "DataLo1",		PROPID_DATA_LO1,	usageWriteBoth,	IDS_NAME_DATALO1 },
	{ 1, "DataLo2",		PROPID_DATA_LO2,	usageWriteBoth,	IDS_NAME_DATALO2 },
	{ 1, "DataLo3",		PROPID_DATA_LO3,	usageWriteBoth,	IDS_NAME_DATALO3 },
	{ 1, "DataLo4",		PROPID_DATA_LO4,	usageWriteBoth,	IDS_NAME_DATALO4 },
	{ 1, "DataLo5",		PROPID_DATA_LO5,	usageWriteBoth,	IDS_NAME_DATALO5 },
	{ 1, "DataLo6",		PROPID_DATA_LO6,	usageWriteBoth,	IDS_NAME_DATALO6 },
	{ 1, "DataLo7",		PROPID_DATA_LO7,	usageWriteBoth,	IDS_NAME_DATALO7 },
	{ 1, "DataLo8",		PROPID_DATA_LO8,	usageWriteBoth,	IDS_NAME_DATALO8 },

	{ 1, "DataHi1",		PROPID_DATA_HI1,	usageWriteBoth,	IDS_NAME_DATAHI1 },
	{ 1, "DataHi2",		PROPID_DATA_HI2,	usageWriteBoth,	IDS_NAME_DATAHI2 },
	{ 1, "DataHi3",		PROPID_DATA_HI3,	usageWriteBoth,	IDS_NAME_DATAHI3 },
	{ 1, "DataHi4",		PROPID_DATA_HI4,	usageWriteBoth,	IDS_NAME_DATAHI4 },
	{ 1, "DataHi5",		PROPID_DATA_HI5,	usageWriteBoth,	IDS_NAME_DATAHI5 },
	{ 1, "DataHi6",		PROPID_DATA_HI6,	usageWriteBoth,	IDS_NAME_DATAHI6 },
	{ 1, "DataHi7",		PROPID_DATA_HI7,	usageWriteBoth,	IDS_NAME_DATAHI7 },
	{ 1, "DataHi8",		PROPID_DATA_HI8,	usageWriteBoth,	IDS_NAME_DATAHI8 },

	{ 1, "OutputLo1",	PROPID_OUT_LO1,		usageWriteBoth,	IDS_NAME_OUTLO1 },
	{ 1, "OutputLo2",	PROPID_OUT_LO2,		usageWriteBoth,	IDS_NAME_OUTLO2 },
	{ 1, "OutputLo3",	PROPID_OUT_LO3,		usageWriteBoth,	IDS_NAME_OUTLO3 },
	{ 1, "OutputLo4",	PROPID_OUT_LO4,		usageWriteBoth,	IDS_NAME_OUTLO4 },
	{ 1, "OutputLo5",	PROPID_OUT_LO5,		usageWriteBoth,	IDS_NAME_OUTLO5 },
	{ 1, "OutputLo6",	PROPID_OUT_LO6,		usageWriteBoth,	IDS_NAME_OUTLO6 },
	{ 1, "OutputLo7",	PROPID_OUT_LO7,		usageWriteBoth,	IDS_NAME_OUTLO7 },
	{ 1, "OutputLo8",	PROPID_OUT_LO8,		usageWriteBoth,	IDS_NAME_OUTLO8 },

	{ 1, "OutputHi1",	PROPID_OUT_HI1,		usageWriteBoth,	IDS_NAME_OUTHI1 },
	{ 1, "OutputHi2",	PROPID_OUT_HI2,		usageWriteBoth,	IDS_NAME_OUTHI2 },
	{ 1, "OutputHi3",	PROPID_OUT_HI3,		usageWriteBoth,	IDS_NAME_OUTHI3 },
	{ 1, "OutputHi4",	PROPID_OUT_HI4,		usageWriteBoth,	IDS_NAME_OUTHI4 },
	{ 1, "OutputHi5",	PROPID_OUT_HI5,		usageWriteBoth,	IDS_NAME_OUTHI5 },
	{ 1, "OutputHi6",	PROPID_OUT_HI6,		usageWriteBoth,	IDS_NAME_OUTHI6 },
	{ 1, "OutputHi7",	PROPID_OUT_HI7,		usageWriteBoth,	IDS_NAME_OUTHI7 },
	{ 1, "OutputHi8",	PROPID_OUT_HI8,		usageWriteBoth,	IDS_NAME_OUTHI8 },

	{ 1, "DataInit1",	PROPID_DATA_INIT1,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit2",	PROPID_DATA_INIT2,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit3",	PROPID_DATA_INIT3,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit4",	PROPID_DATA_INIT4,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit5",	PROPID_DATA_INIT5,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit6",	PROPID_DATA_INIT6,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit7",	PROPID_DATA_INIT7,	usageWriteInit,	IDS_NAME_OUTHI1 },
	{ 1, "DataInit8",	PROPID_DATA_INIT8,	usageWriteInit,	IDS_NAME_OUTHI1 },

};

// Constructor

CDAAO8OutputConfig::CDAAO8OutputConfig(void)
{
	m_Type1		= 4;
	m_Type2		= 4;
	m_Type3		= 4;
	m_Type4		= 4;
	m_Type5		= 4;
	m_Type6		= 4;
	m_Type7		= 4;
	m_Type8		= 4;

	m_DP1		= 3;
	m_DP2		= 3;
	m_DP3		= 3;
	m_DP4		= 3;
	m_DP5		= 3;
	m_DP6		= 3;
	m_DP7		= 3;
	m_DP8		= 3;

	m_DataLo1	= 0;
	m_DataLo2	= 0;
	m_DataLo3	= 0;
	m_DataLo4	= 0;
	m_DataLo5	= 0;
	m_DataLo6	= 0;
	m_DataLo7	= 0;
	m_DataLo8	= 0;

	m_DataHi1	= 10000;
	m_DataHi2	= 10000;
	m_DataHi3	= 10000;
	m_DataHi4	= 10000;
	m_DataHi5	= 10000;
	m_DataHi6	= 10000;
	m_DataHi7	= 10000;
	m_DataHi8	= 10000;

	m_OutputLo1	= 0;
	m_OutputLo2	= 0;
	m_OutputLo3	= 0;
	m_OutputLo4	= 0;
	m_OutputLo5	= 0;
	m_OutputLo6	= 0;
	m_OutputLo7	= 0;
	m_OutputLo8	= 0;

	m_OutputHi1	= 10000;
	m_OutputHi2	= 10000;
	m_OutputHi3	= 10000;
	m_OutputHi4	= 10000;
	m_OutputHi5	= 10000;
	m_OutputHi6	= 10000;
	m_OutputHi7	= 10000;
	m_OutputHi8	= 10000;

	m_DataInit1	= 0;
	m_DataInit2	= 0;
	m_DataInit3	= 0;
	m_DataInit4	= 0;
	m_DataInit5	= 0;
	m_DataInit6	= 0;
	m_DataInit7	= 0;
	m_DataInit8	= 0;

	m_InitData	= 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAAO8OutputConfig::GetPageCount(void)
{
	return 8 + 1;
}

CString CDAAO8OutputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			return CPrintf(IDS("Output %d"), n + 1);

		case 8:
			return IDS("Initial Output");
	}

	return L"";
}

CViewWnd * CDAAO8OutputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			return New CDAAO8OutputConfigWnd(n);

		case 8:
			return New CDAAO8OutputConfigWnd(n);
	}

	return NULL;
}

// Group Names

CString CDAAO8OutputConfig::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1: return L"Scaling";
	}

	return CCommsItem::GetGroupName(Group);
}

// Conversion

BOOL CDAAO8OutputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 8; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Type%d", n+1), CPrintf(L"OutType%d", n+1)) ) {

				// Legacy +  Graphite

				ConvertType(n);
			}

			ImportNumber(pValue, CPrintf(L"DP%d", n+1));

			ImportNumber(pValue, CPrintf(L"DataLo%d", n+1));

			ImportNumber(pValue, CPrintf(L"DataHi%d", n+1));

			ImportNumber(pValue, CPrintf(L"OutputLo%d", n+1));

			ImportNumber(pValue, CPrintf(L"OutputHi%d", n+1));

			ImportNumber(pValue, CPrintf(L"DataInit%d", n+1), CPrintf(L"Data%d", n+1));
		}

		ImportNumber(pValue, L"InitData");

		return TRUE;
	}

	return FALSE;
}

// Property Filter

BOOL CDAAO8OutputConfig::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_DATA1:
			case PROPID_DATA2:
			case PROPID_DATA3:
			case PROPID_DATA4:
			case PROPID_DATA5:
			case PROPID_DATA6:
			case PROPID_DATA7:
			case PROPID_DATA8:

				return FALSE;
		}
	}

	return TRUE;
}

// Meta Data Creation

void CDAAO8OutputConfig::AddMetaData(void)
{
	Meta_AddInteger(Type1);
	Meta_AddInteger(Type2);
	Meta_AddInteger(Type3);
	Meta_AddInteger(Type4);
	Meta_AddInteger(Type5);
	Meta_AddInteger(Type6);
	Meta_AddInteger(Type7);
	Meta_AddInteger(Type8);

	Meta_AddInteger(DP1);
	Meta_AddInteger(DP2);
	Meta_AddInteger(DP3);
	Meta_AddInteger(DP4);
	Meta_AddInteger(DP5);
	Meta_AddInteger(DP6);
	Meta_AddInteger(DP7);
	Meta_AddInteger(DP8);

	Meta_AddInteger(DataLo1);
	Meta_AddInteger(DataLo2);
	Meta_AddInteger(DataLo3);
	Meta_AddInteger(DataLo4);
	Meta_AddInteger(DataLo5);
	Meta_AddInteger(DataLo6);
	Meta_AddInteger(DataLo7);
	Meta_AddInteger(DataLo8);

	Meta_AddInteger(DataHi1);
	Meta_AddInteger(DataHi2);
	Meta_AddInteger(DataHi3);
	Meta_AddInteger(DataHi4);
	Meta_AddInteger(DataHi5);
	Meta_AddInteger(DataHi6);
	Meta_AddInteger(DataHi7);
	Meta_AddInteger(DataHi8);

	Meta_AddInteger(OutputLo1);
	Meta_AddInteger(OutputLo2);
	Meta_AddInteger(OutputLo3);
	Meta_AddInteger(OutputLo4);
	Meta_AddInteger(OutputLo5);
	Meta_AddInteger(OutputLo6);
	Meta_AddInteger(OutputLo7);
	Meta_AddInteger(OutputLo8);

	Meta_AddInteger(OutputHi1);
	Meta_AddInteger(OutputHi2);
	Meta_AddInteger(OutputHi3);
	Meta_AddInteger(OutputHi4);
	Meta_AddInteger(OutputHi5);
	Meta_AddInteger(OutputHi6);
	Meta_AddInteger(OutputHi7);
	Meta_AddInteger(OutputHi8);

	Meta_AddInteger(DataInit1);
	Meta_AddInteger(DataInit2);
	Meta_AddInteger(DataInit3);
	Meta_AddInteger(DataInit4);
	Meta_AddInteger(DataInit5);
	Meta_AddInteger(DataInit6);
	Meta_AddInteger(DataInit7);
	Meta_AddInteger(DataInit8);

	Meta_AddInteger(InitData);

	CCommsItem::AddMetaData();
}

// Implementation

void CDAAO8OutputConfig::ConvertType(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Type%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT   uPrev = pData->ReadInteger(this);

		UINT uData[] = { 0, 1, 4, 5, 7, 9 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
