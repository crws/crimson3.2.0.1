
#include "intern.hpp"

#include "sgmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module
//

class CGMSGModule : public CSGModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMSGModule(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMSGModule, CSGModule);

// Constructor

CGMSGModule::CGMSGModule(void)
{
	m_Ident  = LOBYTE(ID_GMSG1);

	m_FirmID = FIRM_GMSG;

	m_Model  = "GMSG1";

	m_Power  = 56;

	m_Conv.Insert(L"Legacy", L"CSGModule");

	m_Conv.Insert(L"Manticore", L"CDASG1Module");
	}

// Download Support

void CGMSGModule::MakeConfigData(CInitData &Init)
{
	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);
	
	CSGModule::MakeConfigData(Init);
	}

// End of File
