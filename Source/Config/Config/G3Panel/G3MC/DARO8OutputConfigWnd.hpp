
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DARO8OutputConfigWnd_HPP

#define INCLUDE_DARO8OutputConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDARO8OutputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DARO8 DO Configuration Window
//

class CDARO8OutputConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CDARO8OutputConfig * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);

		// Implementation
		void AddOutputs(void);
	};

// End of File

#endif
