
#include "intern.hpp"

#include "legacy.h"

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tool tips an rules.
// 

void CDIO14LogicWnd::InfoBaloon(UINT type,UINT inst,CPoint pt,BOOL mode)
{
	CClientDC DC(ThisObject);
		
	CString str = L"NULL";

	m_InfoBal.Width = 10;
	
	UINT ib_height = 17;
	
	RECT Rect;
	
	CColor Color;
	
	if((m_EditMode || m_ConnectMode) && m_InfoBal.Disp != IB_DRAWN) {
		
		if(m_InfoBal.Disp == IB_PEND) {

			m_InfoBal.Disp = IB_OFF;

			KillTimer(m_InfoBal.Inst+m_pItem->m_NSI+1);
			}

		return;
		}
	
	if(m_InfoBal.Disp == IB_ON) {
				
		m_InfoBal.Disp = IB_DRAWN;
		
		str = GetTipText(m_InfoBal.Type,m_InfoBal.Inst,DC);
		
		Rect.left   = m_InfoBal.Pos.x;
		Rect.top    = m_InfoBal.Pos.y;
		Rect.right  = m_InfoBal.Pos.x+m_InfoBal.Width;
		Rect.bottom = m_InfoBal.Pos.y+ib_height;
		
		if(Rect.right > LOGIC_WIN_RIGHT-10) {
			
			Rect.left -= (m_InfoBal.Width);
			
			m_InfoBal.Pos.x = Rect.left;
			
			Rect.right -= (m_InfoBal.Width);
			}
		
		if(Rect.bottom > LOGIC_WIN_BOTTOM-10) {
			
			Rect.top = m_pItem->m_Sym[m_InfoBal.Inst].Top-40;
			
			m_InfoBal.Pos.y = Rect.top;
			
			Rect.bottom = Rect.top+ib_height;
			}

		CBrush Brush(CColor(100, 100, 100));
				
		DC.FillRect(Rect, afxBrush(InfoBack));
		
		DC.FrameRect(Rect,Brush);

		DC.Save();

		DC.SetBkMode(TRANSPARENT);
		
		DC.Select(afxFont(Dialog));
		
		DC.SetTextColor(0x00000000);
	
		DC.TextOut(Rect.left+3,Rect.top+1,str);

		DC.Deselect();

		DC.Restore();

		return;
		}

	if(mode == IB_START) {
					
		if(m_InfoBal.Disp != IB_OFF) return;
		
		m_InfoBal.Disp = IB_PEND;
		
		m_InfoBal.Type = type;
		
		if(type == ISYMBOL) {

			m_InfoBal.Pos.x = m_pItem->m_Sym[inst].Left;

			m_InfoBal.Pos.y = m_pItem->m_Sym[inst].Top + m_pItem->m_Sym[inst].Height + 25;
			}
		
		else {
			m_InfoBal.Pos.x = pt.x;

			m_InfoBal.Pos.y = pt.y+25;
			}
				
		m_InfoBal.Inst = inst;

		SetTimer(m_InfoBal.Inst+m_pItem->m_NSI+1,1000);

		return;
		}

	if(mode == IB_STOP) {
				
		if(m_InfoBal.Disp == IB_DRAWN) {
		
			str = GetTipText(m_InfoBal.Type,m_InfoBal.Inst,DC);

			KillTimer(m_InfoBal.Inst+m_pItem->m_NSI+1);

			Rect.left   = m_InfoBal.Pos.x;
			Rect.top    = m_InfoBal.Pos.y;
			Rect.right  = m_InfoBal.Pos.x+m_InfoBal.Width;
			Rect.bottom = m_InfoBal.Pos.y+ib_height;

			DC.FillRect(Rect, afxBrush(BLACK));

			DrawInputBoxes(DC);
				
			DrawOutputBoxes(TRUE,DC);
			
			DrawUserSymbols(DC);
			
			DrawAllWires(DC);

			DrawGraphicsAreaText(DC);
			}
		else {
			KillTimer(m_InfoBal.Inst+m_pItem->m_NSI+1);
			}

		m_InfoBal.Disp = IB_OFF;
		}
	}

CString CDIO14LogicWnd::GetTipText(UINT type,UINT inst,CDC &DC)
{
	CString str = L"NULL";
	
	if(type == ISYMBOL) {

		if(inst > START_PROTO) {

			switch(inst) {

				case AND_PROTO:
					str = CString(IDS_DIO_AND);
					break;

				case OR_PROTO:
					str = CString(IDS_DIO_OR);
					break;
				
				case XOR_PROTO:
					str = CString(IDS_DIO_XOR);
					break;
				
				case NOT_PROTO:
					str = CString(IDS_DIO_NOT);
					break;
				
				case TIMERUP_PROTO:
					str = CString(IDS_DIO_UPTMR);
					break;
				
				case TIMERDN_PROTO:
					str = CString(IDS_DIO_DNTMR);
					break;

				case COUNTUP_PROTO:
					str = CString(IDS_DIO_UPCTR);
					break;
				
				case COUNTDN_PROTO:
					str = CString(IDS_DIO_DNCTR);
					break;

				case LATCH_PROTO:
					str = CString(IDS_DIO_LATCH);
					break;

				case INCOIL_PROTO:
					str = CString(IDS_DIO_INCOIL);
					break;
				
				case OUTCOIL_PROTO:
					str = CString(IDS_DIO_OUT_COIL);
					break;

				default:
					str ="";
					break;
				}
			}
		else {
			switch(m_pItem->m_Sym[inst].Type) {

				case MINP:
					str = CString(IDS_DIO_IN_STATE);
					break;

				case MOUT:
					str = CString(IDS_DIO_NOOPS);
					break;

#ifdef DEBUG_INST				
				
				default:
					
					char str1[80];
					itoa(inst,str1,10);
					str = str1;
					m_InfoBal.Width = 198;
					return str;
#else					
					
				default:
					str = CString(IDS_DIO_CLICK_DEF);
					break;
#endif
				
				}	
			}
		}
	
	if(type == IWIRE) {
			
		str = CString(IDS_DIO_CLICK_R);
		}

	if(type == ISEG) {
			
		str = CString(IDS_DIO_CLIC_LR);		
		}

	if(type == IBUTTON) {
			
		switch(inst) {

			case B_UNDO:
				str = CString(IDS_DIO_UNDO);
				break;

			case B_REDO:
				str = CString(IDS_DIO_REDO);
				break;

			case B_REFRESH:
				str = CString(IDS_DIO_REFRESH);
				break;

			}
		}

	m_InfoBal.Width = GetTipTextWidth(str,DC);

	return str;
	}	

UINT CDIO14LogicWnd::GetTipTextWidth(CString str,CDC &DC)
{
	/*-----------------------------------------------
	Pre-write the string at the unused bottom of the
	black workspace in a color that it 1 count above
	black. You can't see this happen.  Scan the pixels
	for the width of the displayed string, then erase
	the string.
	-----------------------------------------------*/
	
	CRect Rect;	
	
	COLORREF Color;

	COLORREF TC;

	Rect.left   = 1;
	Rect.top    = LOGIC_WIN_TOP;
	Rect.right  = LOGIC_WIN_RIGHT-10;
	Rect.bottom = LOGIC_WIN_TOP+13;
	
	DC.Save();

	DC.SetBkMode(TRANSPARENT);
	
	DC.Select(afxFont(Dialog));
	
	TC = 0x00000010;
	
	DC.SetTextColor(TC);

	DC.TextOut(Rect.left+3,Rect.top+1,str);
	
	UINT LastPixel = 0;

	BOOL Pixel = FALSE;

	for(UINT i = Rect.left+1; i < (UINT)(Rect.right-1); i++) {
				
		Pixel = FALSE;
		
		for(UINT j = Rect.top+1; j < (UINT)(Rect.bottom-1); j++) {
		
			Color = GetPixel(DC,i,j);
			
			if(Color == TC) {
				
				Pixel = TRUE;
				
				LastPixel = i;
				
				break;
				}
			}
		}

	DC.SetTextColor(0x00000000);

	DC.TextOut(Rect.left+3,Rect.top+1,str);

	DC.Deselect();

	DC.Restore();

	return (LastPixel + 3);
	}

//End of File
