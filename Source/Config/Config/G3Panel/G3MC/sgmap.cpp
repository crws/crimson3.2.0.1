
#include "intern.hpp"

#include "legacy.h"

#include "sgmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Mapper
//

// Runtime Class

AfxImplementRuntimeClass(CSGMapper, CCommsItem);

// Property List

CCommsList const CSGMapper::m_CommsList[] = {

	{ 0, "LinOutType",		PROPID_LIN_OUT_TYPE,		usageWriteInit,	IDS_NAME_LOT	},
	{ 0, "LinOutMin",		PROPID_LIN_OUT_MIN,		usageWriteInit,	IDS_NAME_LOMIN	},
	{ 0, "LinOutMax",		PROPID_LIN_OUT_MAX,		usageWriteInit, IDS_NAME_LOMAX	},
	{ 0, "LinOutFilter",		PROPID_LIN_OUT_FILTER,		usageWriteInit, IDS_NAME_LOF	},
	{ 0, "LinOutDead",		PROPID_LIN_OUT_DEAD,		usageWriteInit,	IDS_NAME_LOD	},
	{ 0, "LinOutUpdate",		PROPID_LIN_OUT_UPDATE,		usageWriteInit,	IDS_NAME_LOU	},
	{ 0, "LinOutMap",		PROPID_LIN_OUT_MAP,		usageWriteInit,	IDS_NAME_LOM	},
	{ 0, "DigOutMap1",		PROPID_DIG_OUT_MAP_1,		usageWriteInit,	IDS_NAME_DOM1	},
	{ 0, "DigOutMap2",		PROPID_DIG_OUT_MAP_2,		usageWriteInit,	IDS_NAME_DOM2	},
	{ 0, "DigOutMap3",		PROPID_DIG_OUT_MAP_3,		usageWriteInit,	IDS_NAME_DOM3	},
	{ 0, "LedOutMap1",		PROPID_LED_OUT_MAP_1,		usageWriteInit,	IDS_NAME_LOM1	},
	{ 0, "LedOutMap2",		PROPID_LED_OUT_MAP_2,		usageWriteInit,	IDS_NAME_LOM2	},
	{ 0, "LedOutMap3",		PROPID_LED_OUT_MAP_3,		usageWriteInit,	IDS_NAME_LOM3	},
	{ 0, "LedOutMap4",		PROPID_LED_OUT_MAP_4,		usageWriteInit,	IDS_NAME_LOM4	},

	{ 1, "CycleTime1",		PROPID_CYCLE_TIME_1,		usageWriteBoth,	IDS_NAME_CT1	},
	{ 1, "CycleTime2",		PROPID_CYCLE_TIME_2,		usageWriteBoth,	IDS_NAME_CT2	},
	{ 1, "CycleTime3",		PROPID_CYCLE_TIME_3,		usageWriteBoth,	IDS_NAME_CT3	},
	
	{ 2, "DigRemote1",		PROPID_DIG_REMOTE_1,		usageWriteUser,	IDS_NAME_DR1	},
	{ 2, "DigRemote2",		PROPID_DIG_REMOTE_2,		usageWriteUser,	IDS_NAME_DR2	},
	{ 2, "DigRemote3",		PROPID_DIG_REMOTE_3,		usageWriteUser,	IDS_NAME_DR3	},
	{ 2, "DigRemote4",		PROPID_DIG_REMOTE_4,		usageWriteUser,	IDS_NAME_DR4	},
	{ 2, "AnlRemote1",		PROPID_ANL_REMOTE_1,		usageWriteUser,	IDS_NAME_AR1	},
	{ 2, "AnlRemote2",		PROPID_ANL_REMOTE_2,		usageWriteUser,	IDS_NAME_AR2	},
	{ 2, "AnlRemote3",		PROPID_ANL_REMOTE_3,		usageWriteUser,	IDS_NAME_AR3	},
	{ 2, "AnlRemote4",		PROPID_ANL_REMOTE_4,		usageWriteUser,	IDS_NAME_AR4	},

	{ 3, "OP1State",		PROPID_OP1_STATE,		usageRead,	IDS_OP1_STATE	},
	{ 3, "OP2State",		PROPID_OP2_STATE,		usageRead,	IDS_OP2_STATE	},
	{ 3, "OP3State",		PROPID_OP3_STATE,		usageRead,	IDS_OP3_STATE	},

	};

// Constructor

CSGMapper::CSGMapper(void)
{
	m_LinOutType	= LINOUT_420MA;
	m_LinOutMin	= 0;
	m_LinOutMax	= 100000;
	m_LinOutFilter	= 0;
	m_LinOutDead	= 0;
	m_LinOutUpdate	= 0;
	m_LinOutMap	= ANL_HEAT;
	m_DigOutMap1	= ANL_HEAT;
	m_DigOutMap2	= 0;
	m_DigOutMap3	= 0;
	m_LedOutMap1	= LED_OUT1;
	m_LedOutMap2	= LED_OUT2;
	m_LedOutMap3	= LED_OUT3;
	m_LedOutMap4	= ANY_EITHER;
	m_CycleTime1	= 20;
	m_CycleTime2	= 20;
	m_CycleTime3	= 20;
	m_DigRemote1	= 0;
	m_DigRemote2	= 0;
	m_DigRemote3	= 0;
	m_DigRemote4	= 0;
	m_AnlRemote1	= 0;
	m_AnlRemote2	= 0;
	m_AnlRemote3	= 0;
	m_AnlRemote4	= 0;
	m_OP1State	= 0;
	m_OP2State	= 0;
	m_OP3State	= 0;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CSGMapper::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_CYCLE_TIME);

		case 2:	return CString(IDS_MODULE_REMOTE);

		case 3:	return CString(IDS_MODULE_INFO);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CSGMapper::GetPageCount(void)
{
	return 2;
	}

CString CSGMapper::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_OUTPUTS);
		
		case 1: return CString(IDS_MODULE_LEDS);

		}

	return L"";
	}

CViewWnd * CSGMapper::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CSGMapMainWnd;

		case 1: return New CSGMapLEDsWnd;

		}

	return NULL;
	}

// Data Scaling

DWORD CSGMapper::GetIntProp(PCTXT pTag)
{
	if( IsTenTimes(pTag) ) {

		CMetaData const *pMeta = FindMetaData(pTag);

		if( pTag ) {

			INT nData = pMeta->ReadInteger(this);

			if( nData > 0 ) {

				nData = (nData + 5) / 10;
				}

			if( nData < 0 ) {

				nData = (nData - 5) / 10;
				}

			return DWORD(nData);
			}

		return 0;
		}

	return CCommsItem::GetIntProp(pTag);
	}

BOOL CSGMapper::IsTenTimes(CString Tag)
{
	if( Tag == "LinOutMin" ) {
		
		return TRUE;
		}

	if( Tag == "LinOutMax" ) {
		
		return TRUE;
		}

	if( Tag == "LinOutDead" ) {
		
		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CSGMapper::AddMetaData(void)
{
	Meta_AddInteger(LinOutType);
	Meta_AddInteger(LinOutMin);
	Meta_AddInteger(LinOutMax);
	Meta_AddInteger(LinOutFilter);
	Meta_AddInteger(LinOutDead);
	Meta_AddInteger(LinOutUpdate);
	Meta_AddInteger(LinOutMap);
	Meta_AddInteger(DigOutMap1);
	Meta_AddInteger(DigOutMap2);
	Meta_AddInteger(DigOutMap3);
	Meta_AddInteger(LedOutMap1);
	Meta_AddInteger(LedOutMap2);
	Meta_AddInteger(LedOutMap3);
	Meta_AddInteger(LedOutMap4);
	Meta_AddInteger(CycleTime1);
	Meta_AddInteger(CycleTime2);
	Meta_AddInteger(CycleTime3);
	Meta_AddInteger(DigRemote1);
	Meta_AddInteger(DigRemote2);
	Meta_AddInteger(DigRemote3);
	Meta_AddInteger(DigRemote4);
	Meta_AddInteger(AnlRemote1);
	Meta_AddInteger(AnlRemote2);
	Meta_AddInteger(AnlRemote3);
	Meta_AddInteger(AnlRemote4);
	Meta_AddInteger(OP1State);
	Meta_AddInteger(OP2State);
	Meta_AddInteger(OP3State);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Mapper Main View
//

// Runtime Class

AfxImplementRuntimeClass(CSGMapMainWnd, CUIViewWnd);

// Overidables

void CSGMapMainWnd::OnAttach(void)
{
	m_pItem   = (CSGMapper *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("sgmap"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CSGMapMainWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	if( Tag == "LinOutMap" ) {

		EnableLinear();
		}

	if( Tag == "DigOutMap1" ) {

		EnableOutput(1);
		}

	if( Tag == "DigOutMap2" ) {

		EnableOutput(2);
		}

	if( Tag == "DigOutMap3" ) {

		EnableOutput(3);
		}

	CSGModule *pModule = (CSGModule *) m_pItem->GetParent();

	CSGLoop   *pLoop   = pModule->m_pLoop;

	CUISGProcess::CheckUpdate(pLoop, Tag);
	}

void CSGMapMainWnd::OnUICreate(void)
{
	StartPage(1);

	AddDigital();

	AddLinear();

	EndPage(TRUE);
	}

// UI Creation

void CSGMapMainWnd::AddLinear(void)
{
	StartGroup(CString(IDS_MODULE_LIN_OUT), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("LinOutType"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOutMap"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOutMin"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOutMax"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOutFilter"));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOutDead"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOutUpdate"));

	EndGroup(TRUE);
	}

void CSGMapMainWnd::AddDigital(void)
{
	StartGroup(CString(IDS_MODULE_DIG_OUTS), 2);

	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap1"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime1"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap2"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime2"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap3"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime3"));

	EndGroup(TRUE);
	}

// Enabling

void CSGMapMainWnd::DoEnables(void)
{
	EnableLinear();

	EnableOutput(1);

	EnableOutput(2);
	
	EnableOutput(3);
	}

void CSGMapMainWnd::EnableLinear(void)
{
	BOOL fEnable = m_pItem->m_LinOutMap ? TRUE : FALSE;

	EnableUI("LinOutMin",    fEnable);
	EnableUI("LinOutMax",    fEnable);
	EnableUI("LinOutFilter", fEnable);
	EnableUI("LinOutDead",   fEnable);
	EnableUI("LinOutUpdate", fEnable);
	}

void CSGMapMainWnd::EnableOutput(UINT n)
{
	BYTE bCode = 0x00;

	switch( n ) {

		case 1: bCode = BYTE(m_pItem->m_DigOutMap1); break;
		case 2: bCode = BYTE(m_pItem->m_DigOutMap2); break;
		case 3: bCode = BYTE(m_pItem->m_DigOutMap3); break;

		}

	BOOL fEnable = ((bCode & 0xF0) == 0x70);

	EnableUI(CPrintf("CycleTime%u", n), fEnable);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Mapper LEDs View
//

// Runtime Class

AfxImplementRuntimeClass(CSGMapLEDsWnd, CUIViewWnd);

// Overidables

void CSGMapLEDsWnd::OnAttach(void)
{
	m_pItem   = (CSGMapper *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("sgmap"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CSGMapLEDsWnd::OnUIChange(CItem *pItem, CString Tag)
{
	}

void CSGMapLEDsWnd::OnUICreate(void)
{
	StartPage(1);

	AddLEDs();

	EndPage(TRUE);
	}

// UI Creation

void CSGMapLEDsWnd::AddLEDs(void)
{
	StartGroup(CString(IDS_MODULE_LED_FUNC), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap1"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap2"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap3"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap4"));

	EndGroup(TRUE);
	}

// Enabling

void CSGMapLEDsWnd::DoEnables(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Strain Gage Mapping
//

// Dynamic Class

AfxImplementDynamicClass(CUISGMap, CUITextEnum);

// Constructor

CUISGMap::CUISGMap(void)
{
	}

// Core Overidables

void CUISGMap::OnBind(void)
{
	char cKey = char(m_UIData.m_Format[0]);

	AddData(0x00, CString(IDS_MODULE_UNASSIGNED));

	switch( cKey ) {

		case 'A':
			AddCoreAnalog();
			AddMiscAnalog();
			AddCommAnalog();
			break;

		case 'D':
			AddCoreAnalog();
			AddDigital();
			AddCommAnalog();
			break;

		case 'L':
			AddOutputs();
			AddDigital();
			break;

		default:
			AfxAssert(FALSE);
			break;
		}
	}

// Implementation

void CUISGMap::AddCoreAnalog(void)
{
	AddData(0x70, CString(IDS_MODULE_PWR_REV));
	AddData(0x71, CString(IDS_MODULE_PWR_DIR));
	}

void CUISGMap::AddMiscAnalog(void)
{
	AddData(0x72, CString(IDS_MODULE_SP_REQ));
	AddData(0x73, CString(IDS_MODULE_SP_AC));
	AddData(0x74, CString(IDS_MODULE_PROC_VALUE));
	AddData(0x7C, CPrintf(CString(IDS_MODULE_INP) + L" 1"));
	AddData(0x7D, CPrintf(CString(IDS_MODULE_INP) + L" 2"));
	AddData(0x75, CString(IDS_MODULE_PROC_ERROR));
	}

void CUISGMap::AddDigital(void)
{
	AddData(0x42, CPrintf(CString(IDS_MODULE_ALRM_ANY), L""));
	AddData(0x40, CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_PROC)));
	AddData(0x20, CPrintf(CString(IDS_MODULE_ALRM_STAT), 1));
	AddData(0x21, CPrintf(CString(IDS_MODULE_ALRM_STAT), 2));
	AddData(0x22, CPrintf(CString(IDS_MODULE_ALRM_STAT), 3));
	AddData(0x23, CPrintf(CString(IDS_MODULE_ALRM_STAT), 4));
	AddData(0x26, CString(IDS_MODULE_INP_FAULT));
	AddData(0x10, CString(IDS_MODULE_MODE_MAN));
	AddData(0x30, CString(IDS_MODULE_OUT_PEGLO));
	AddData(0x31, CString(IDS_MODULE_OUT_PEGHI));
	AddData(0x11, CString(IDS_MODULE_AUTO_BUSY));
	AddData(0x12, CString(IDS_MODULE_AUTO_DONE));
	AddData(0x13, CString(IDS_MODULE_AUTO_FAIL));
	AddData(0x33, CPrintf(CString(IDS_MODULE_REM_DIG), 1));
	AddData(0x34, CPrintf(CString(IDS_MODULE_REM_DIG), 2));
	AddData(0x35, CPrintf(CString(IDS_MODULE_REM_DIG), 3));
	AddData(0x36, CPrintf(CString(IDS_MODULE_REM_DIG), 4));
	}

void CUISGMap::AddOutputs(void)
{
	AddData(0x50, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 1));
	AddData(0x51, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 2));
	AddData(0x52, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 3));
     	}

void CUISGMap::AddCommAnalog(void)
{
	AddData(0x78, CPrintf(CString(IDS_MODULE_REM_ANA), 1));
	AddData(0x79, CPrintf(CString(IDS_MODULE_REM_ANA), 2));
	AddData(0x7A, CPrintf(CString(IDS_MODULE_REM_ANA), 3));
	AddData(0x7B, CPrintf(CString(IDS_MODULE_REM_ANA), 4));
	}

// End of File
