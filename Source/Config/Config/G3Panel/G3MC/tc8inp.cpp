
#include "intern.hpp"

#include "legacy.h"

#include "tc8inp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Input Item
//

// Runtime Class

AfxImplementRuntimeClass(CTC8Input, CCommsItem);

// Property List

CCommsList const CTC8Input::m_CommsList[] = {

	{ 1, "PV1",		PROPID_PV1,			usageRead,	IDS_NAME_PV1	},
	{ 1, "PV2",		PROPID_PV2,			usageRead,	IDS_NAME_PV2	},
	{ 1, "PV3",		PROPID_PV3,			usageRead,	IDS_NAME_PV3	},
	{ 1, "PV4",		PROPID_PV4,			usageRead,	IDS_NAME_PV4	},
	{ 1, "PV5",		PROPID_PV5,			usageRead,	IDS_NAME_PV5	},
	{ 1, "PV6",		PROPID_PV6,			usageRead,	IDS_NAME_PV6	},
	{ 1, "PV7",		PROPID_PV7,			usageRead,	IDS_NAME_PV7	},
	{ 1, "PV8",		PROPID_PV8,			usageRead,	IDS_NAME_PV8	},

	{ 1, "ColdJunc",	PROPID_COLD_JUNC,		usageRead,	IDS_NAME_CJ	},

	{ 1, "InputAlarm1",	PROPID_INPUT_ALARM1,		usageRead,	IDS_NAME_IA1	},
	{ 1, "InputAlarm2",	PROPID_INPUT_ALARM2,		usageRead,	IDS_NAME_IA2	},
	{ 1, "InputAlarm3",	PROPID_INPUT_ALARM3,		usageRead,	IDS_NAME_IA3	},
	{ 1, "InputAlarm4",	PROPID_INPUT_ALARM4,		usageRead,	IDS_NAME_IA4	},
	{ 1, "InputAlarm5",	PROPID_INPUT_ALARM5,		usageRead,	IDS_NAME_IA5	},
	{ 1, "InputAlarm6",	PROPID_INPUT_ALARM6,		usageRead,	IDS_NAME_IA6	},
	{ 1, "InputAlarm7",	PROPID_INPUT_ALARM7,		usageRead,	IDS_NAME_IA7	},
	{ 1, "InputAlarm8",	PROPID_INPUT_ALARM8,		usageRead,	IDS_NAME_IA8	},

	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},

	{ 2, "InputOffset1",	PROPID_INPUT_OFFSET1,		usageWriteBoth,	IDS_NAME_IO1	},
	{ 2, "InputOffset2",	PROPID_INPUT_OFFSET2,		usageWriteBoth,	IDS_NAME_IO2	},
	{ 2, "InputOffset3",	PROPID_INPUT_OFFSET3,		usageWriteBoth,	IDS_NAME_IO3	},
	{ 2, "InputOffset4",	PROPID_INPUT_OFFSET4,		usageWriteBoth,	IDS_NAME_IO4	},
	{ 2, "InputOffset5",	PROPID_INPUT_OFFSET5,		usageWriteBoth,	IDS_NAME_IO5	},
	{ 2, "InputOffset6",	PROPID_INPUT_OFFSET6,		usageWriteBoth,	IDS_NAME_IO6	},
	{ 2, "InputOffset7",	PROPID_INPUT_OFFSET7,		usageWriteBoth,	IDS_NAME_IO7	},
	{ 2, "InputOffset8",	PROPID_INPUT_OFFSET8,		usageWriteBoth,	IDS_NAME_IO8	},

	{ 2, "InputSlope1",	PROPID_INPUT_SLOPE1,		usageWriteBoth,	IDS_NAME_IS1	},
	{ 2, "InputSlope2",	PROPID_INPUT_SLOPE2,		usageWriteBoth,	IDS_NAME_IS2	},
	{ 2, "InputSlope3",	PROPID_INPUT_SLOPE3,		usageWriteBoth,	IDS_NAME_IS3	},
	{ 2, "InputSlope4",	PROPID_INPUT_SLOPE4,		usageWriteBoth,	IDS_NAME_IS4	},
	{ 2, "InputSlope5",	PROPID_INPUT_SLOPE5,		usageWriteBoth,	IDS_NAME_IS5	},
	{ 2, "InputSlope6",	PROPID_INPUT_SLOPE6,		usageWriteBoth,	IDS_NAME_IS6	},
	{ 2, "InputSlope7",	PROPID_INPUT_SLOPE7,		usageWriteBoth,	IDS_NAME_IS7	},
	{ 2, "InputSlope8",	PROPID_INPUT_SLOPE8,		usageWriteBoth,	IDS_NAME_IS8	},

	{ 3, "DisplayLo1",	PROPID_TCLIN_DISPLO1,		usageWriteBoth,	IDS_NAME_DL1	},
	{ 3, "DisplayLo2",	PROPID_TCLIN_DISPLO2,		usageWriteBoth,	IDS_NAME_DL2	},
	{ 3, "DisplayLo3",	PROPID_TCLIN_DISPLO3,		usageWriteBoth,	IDS_NAME_DL3	},
	{ 3, "DisplayLo4",	PROPID_TCLIN_DISPLO4,		usageWriteBoth,	IDS_NAME_DL4	},
	{ 3, "DisplayLo5",	PROPID_TCLIN_DISPLO5,		usageWriteBoth,	IDS_NAME_DL5	},
	{ 3, "DisplayLo6",	PROPID_TCLIN_DISPLO6,		usageWriteBoth,	IDS_NAME_DL6	},
	{ 3, "DisplayLo7",	PROPID_TCLIN_DISPLO7,		usageWriteBoth,	IDS_NAME_DL7	},
	{ 3, "DisplayLo8",	PROPID_TCLIN_DISPLO8,		usageWriteBoth,	IDS_NAME_DL8	},

	{ 3, "DisplayHi1",	PROPID_TCLIN_DISPHI1,		usageWriteBoth,	IDS_NAME_DH1	},
	{ 3, "DisplayHi2",	PROPID_TCLIN_DISPHI2,		usageWriteBoth,	IDS_NAME_DH2	},
	{ 3, "DisplayHi3",	PROPID_TCLIN_DISPHI3,		usageWriteBoth,	IDS_NAME_DH3	},
	{ 3, "DisplayHi4",	PROPID_TCLIN_DISPHI4,		usageWriteBoth,	IDS_NAME_DH4	},
	{ 3, "DisplayHi5",	PROPID_TCLIN_DISPHI5,		usageWriteBoth,	IDS_NAME_DH5	},
	{ 3, "DisplayHi6",	PROPID_TCLIN_DISPHI6,		usageWriteBoth,	IDS_NAME_DH6	},
	{ 3, "DisplayHi7",	PROPID_TCLIN_DISPHI7,		usageWriteBoth,	IDS_NAME_DH7	},
	{ 3, "DisplayHi8",	PROPID_TCLIN_DISPHI8,		usageWriteBoth,	IDS_NAME_DH8	},

	{ 3, "SignalLo1",	PROPID_TCLIN_SIGLO1,		usageWriteBoth,	IDS_NAME_SL1	},
	{ 3, "SignalLo2",	PROPID_TCLIN_SIGLO2,		usageWriteBoth,	IDS_NAME_SL2	},
	{ 3, "SignalLo3",	PROPID_TCLIN_SIGLO3,		usageWriteBoth,	IDS_NAME_SL3	},
	{ 3, "SignalLo4",	PROPID_TCLIN_SIGLO4,		usageWriteBoth,	IDS_NAME_SL4	},
	{ 3, "SignalLo5",	PROPID_TCLIN_SIGLO5,		usageWriteBoth,	IDS_NAME_SL5	},
	{ 3, "SignalLo6",	PROPID_TCLIN_SIGLO6,		usageWriteBoth,	IDS_NAME_SL6	},
	{ 3, "SignalLo7",	PROPID_TCLIN_SIGLO7,		usageWriteBoth,	IDS_NAME_SL7	},
	{ 3, "SignalLo8",	PROPID_TCLIN_SIGLO8,		usageWriteBoth,	IDS_NAME_SL8	},

	{ 3, "SignalHi1",	PROPID_TCLIN_SIGHI1,		usageWriteBoth,	IDS_NAME_SH1	},
	{ 3, "SignalHi2",	PROPID_TCLIN_SIGHI2,		usageWriteBoth,	IDS_NAME_SH2	},
	{ 3, "SignalHi3",	PROPID_TCLIN_SIGHI3,		usageWriteBoth,	IDS_NAME_SH3	},
	{ 3, "SignalHi4",	PROPID_TCLIN_SIGHI4,		usageWriteBoth,	IDS_NAME_SH4	},
	{ 3, "SignalHi5",	PROPID_TCLIN_SIGHI5,		usageWriteBoth,	IDS_NAME_SH5	},
	{ 3, "SignalHi6",	PROPID_TCLIN_SIGHI6,		usageWriteBoth,	IDS_NAME_SH6	},
	{ 3, "SignalHi7",	PROPID_TCLIN_SIGHI7,		usageWriteBoth,	IDS_NAME_SH7	},
	{ 3, "SignalHi8",	PROPID_TCLIN_SIGHI8,		usageWriteBoth,	IDS_NAME_SH8	},

	{ 0, "InputType1",	PROPID_INPUT_TYPE1,		usageWriteInit,	IDS_NAME_IT1	},
	{ 0, "InputType2",	PROPID_INPUT_TYPE2,		usageWriteInit,	IDS_NAME_IT2	},
	{ 0, "InputType3",	PROPID_INPUT_TYPE3,		usageWriteInit,	IDS_NAME_IT3	},
	{ 0, "InputType4",	PROPID_INPUT_TYPE4,		usageWriteInit,	IDS_NAME_IT4	},
	{ 0, "InputType5",	PROPID_INPUT_TYPE5,		usageWriteInit,	IDS_NAME_IT5	},
	{ 0, "InputType6",	PROPID_INPUT_TYPE6,		usageWriteInit,	IDS_NAME_IT6	},
	{ 0, "InputType7",	PROPID_INPUT_TYPE7,		usageWriteInit,	IDS_NAME_IT7	},
	{ 0, "InputType8",	PROPID_INPUT_TYPE8,		usageWriteInit,	IDS_NAME_IT8	},

	{ 0, "ChanEnable1",	PROPID_CHAN_ENABLE1,		usageWriteInit,	IDS_NAME_CE1	},
	{ 0, "ChanEnable2",	PROPID_CHAN_ENABLE2,		usageWriteInit,	IDS_NAME_CE2	},
	{ 0, "ChanEnable3",	PROPID_CHAN_ENABLE3,		usageWriteInit,	IDS_NAME_CE3	},
	{ 0, "ChanEnable4",	PROPID_CHAN_ENABLE4,		usageWriteInit,	IDS_NAME_CE4	},
	{ 0, "ChanEnable5",	PROPID_CHAN_ENABLE5,		usageWriteInit,	IDS_NAME_CE5	},
	{ 0, "ChanEnable6",	PROPID_CHAN_ENABLE6,		usageWriteInit,	IDS_NAME_CE6	},
	{ 0, "ChanEnable7",	PROPID_CHAN_ENABLE7,		usageWriteInit,	IDS_NAME_CE7	},
	{ 0, "ChanEnable8",	PROPID_CHAN_ENABLE8,		usageWriteInit,	IDS_NAME_CE8	},

	{ 0, "TempUnits",	PROPID_TEMP_UNITS,		usageWriteInit,	IDS_TEMP_UNITS	},

	{ 0, "ExtendPV1",	PROPID_EXTEND_PV1,		usageWriteInit,	IDS_NAME_EXPV1	},
	{ 0, "ExtendPV2",	PROPID_EXTEND_PV2,		usageWriteInit,	IDS_NAME_EXPV2	},
	{ 0, "ExtendPV3",	PROPID_EXTEND_PV3,		usageWriteInit,	IDS_NAME_EXPV3	},
	{ 0, "ExtendPV4",	PROPID_EXTEND_PV4,		usageWriteInit,	IDS_NAME_EXPV4	},
	{ 0, "ExtendPV5",	PROPID_EXTEND_PV5,		usageWriteInit,	IDS_NAME_EXPV5	},
	{ 0, "ExtendPV6",	PROPID_EXTEND_PV6,		usageWriteInit,	IDS_NAME_EXPV6	},
	{ 0, "ExtendPV7",	PROPID_EXTEND_PV7,		usageWriteInit,	IDS_NAME_EXPV7	},
	{ 0, "ExtendPV8",	PROPID_EXTEND_PV8,		usageWriteInit,	IDS_NAME_EXPV8	},

	};

// Constructor

CTC8Input::CTC8Input(BOOL fGraphite)
{
	m_fGraphite     = fGraphite;

	m_TempUnits	= 1;

	m_InputType1	= TC_TYPEJ;
	m_InputType2	= TC_TYPEJ;
	m_InputType3	= TC_TYPEJ;
	m_InputType4	= TC_TYPEJ;
	m_InputType5	= TC_TYPEJ;
	m_InputType6	= TC_TYPEJ;
	m_InputType7	= TC_TYPEJ;
	m_InputType8	= TC_TYPEJ;

	m_InputFilter	= 20;

	m_InputOffset1	= 0;
	m_InputOffset2	= 0;
	m_InputOffset3	= 0;
	m_InputOffset4	= 0;
	m_InputOffset5	= 0;
	m_InputOffset6	= 0;
	m_InputOffset7	= 0;
	m_InputOffset8	= 0;

	m_InputSlope1	= 1000;
	m_InputSlope2	= 1000;
	m_InputSlope3	= 1000;
	m_InputSlope4	= 1000;
	m_InputSlope5	= 1000;
	m_InputSlope6	= 1000;
	m_InputSlope7	= 1000;
	m_InputSlope8	= 1000;

	m_ProcDP	= 2;

	m_DisplayLo1	= -1000;
	m_DisplayLo2	= -1000;
	m_DisplayLo3	= -1000;
	m_DisplayLo4	= -1000;
	m_DisplayLo5	= -1000;
	m_DisplayLo6	= -1000;
	m_DisplayLo7	= -1000;
	m_DisplayLo8	= -1000;

	m_DisplayHi1	=  5600;
	m_DisplayHi2	=  5600;
	m_DisplayHi3	=  5600;
	m_DisplayHi4	=  5600;
	m_DisplayHi5	=  5600;
	m_DisplayHi6	=  5600;
	m_DisplayHi7	=  5600;
	m_DisplayHi8	=  5600;

	m_SignalLo1	= -1000;
	m_SignalLo2	= -1000;
	m_SignalLo3	= -1000;
	m_SignalLo4	= -1000;
	m_SignalLo5	= -1000;
	m_SignalLo6	= -1000;
	m_SignalLo7	= -1000;
	m_SignalLo8	= -1000;

	m_SignalHi1	=  5600;
	m_SignalHi2	=  5600;
	m_SignalHi3	=  5600;
	m_SignalHi4	=  5600;
	m_SignalHi5	=  5600;
	m_SignalHi6	=  5600;
	m_SignalHi7	=  5600;
	m_SignalHi8	=  5600;

	m_ChanEnable1	= TRUE;
	m_ChanEnable2	= TRUE;
	m_ChanEnable3	= TRUE;
	m_ChanEnable4	= TRUE;
	m_ChanEnable5	= TRUE;
	m_ChanEnable6	= TRUE;
	m_ChanEnable7	= TRUE;
	m_ChanEnable8	= TRUE;

	m_ExtendPV1	= FALSE;
	m_ExtendPV2	= FALSE;
	m_ExtendPV3	= FALSE;
	m_ExtendPV4	= FALSE;
	m_ExtendPV5	= FALSE;
	m_ExtendPV6	= FALSE;
	m_ExtendPV7	= FALSE;
	m_ExtendPV8	= FALSE;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CTC8Input::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_STATUS);

		case 2:	return CString(IDS_MODULE_CTRL);

		case 3: return CString(IDS_MODULE_SCALEMV);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CTC8Input::GetPageCount(void)
{
	return 2;
	}

CString CTC8Input::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_CONFIG);

		case 1: return CString(IDS_MODULE_MV);
		}

	return L"";
	}

CViewWnd * CTC8Input::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CTC8ConfigWnd;

		case 1: return New CTC8ScaleWnd;
		}

	return NULL;
	}

// Implementation

void CTC8Input::AddMetaData(void)
{
	Meta_AddInteger(TempUnits);

	Meta_AddInteger(InputType1);
	Meta_AddInteger(InputType2);
	Meta_AddInteger(InputType3);
	Meta_AddInteger(InputType4);
	Meta_AddInteger(InputType5);
	Meta_AddInteger(InputType6);
	Meta_AddInteger(InputType7);
	Meta_AddInteger(InputType8);

	Meta_AddInteger(InputFilter);

	Meta_AddInteger(InputOffset1);
	Meta_AddInteger(InputOffset2);
	Meta_AddInteger(InputOffset3);
	Meta_AddInteger(InputOffset4);
	Meta_AddInteger(InputOffset5);
	Meta_AddInteger(InputOffset6);
	Meta_AddInteger(InputOffset7);
	Meta_AddInteger(InputOffset8);

	Meta_AddInteger(InputSlope1);
	Meta_AddInteger(InputSlope2);
	Meta_AddInteger(InputSlope3);
	Meta_AddInteger(InputSlope4);
	Meta_AddInteger(InputSlope5);
	Meta_AddInteger(InputSlope6);
	Meta_AddInteger(InputSlope7);
	Meta_AddInteger(InputSlope8);

	Meta_AddInteger(ProcDP);

	Meta_AddInteger(DisplayLo1);
	Meta_AddInteger(DisplayLo2);
	Meta_AddInteger(DisplayLo3);
	Meta_AddInteger(DisplayLo4);
	Meta_AddInteger(DisplayLo5);
	Meta_AddInteger(DisplayLo6);
	Meta_AddInteger(DisplayLo7);
	Meta_AddInteger(DisplayLo8);

	Meta_AddInteger(DisplayHi1);
	Meta_AddInteger(DisplayHi2);
	Meta_AddInteger(DisplayHi3);
	Meta_AddInteger(DisplayHi4);
	Meta_AddInteger(DisplayHi5);
	Meta_AddInteger(DisplayHi6);
	Meta_AddInteger(DisplayHi7);
	Meta_AddInteger(DisplayHi8);

	Meta_AddInteger(SignalLo1);
	Meta_AddInteger(SignalLo2);
	Meta_AddInteger(SignalLo3);
	Meta_AddInteger(SignalLo4);
	Meta_AddInteger(SignalLo5);
	Meta_AddInteger(SignalLo6);
	Meta_AddInteger(SignalLo7);
	Meta_AddInteger(SignalLo8);

	Meta_AddInteger(SignalHi1);
	Meta_AddInteger(SignalHi2);
	Meta_AddInteger(SignalHi3);
	Meta_AddInteger(SignalHi4);
	Meta_AddInteger(SignalHi5);
	Meta_AddInteger(SignalHi6);
	Meta_AddInteger(SignalHi7);
	Meta_AddInteger(SignalHi8);

	Meta_AddInteger(ChanEnable1);
	Meta_AddInteger(ChanEnable2);
	Meta_AddInteger(ChanEnable3);
	Meta_AddInteger(ChanEnable4);
	Meta_AddInteger(ChanEnable5);
	Meta_AddInteger(ChanEnable6);
	Meta_AddInteger(ChanEnable7);
	Meta_AddInteger(ChanEnable8);

	Meta_AddInteger(ExtendPV1);
	Meta_AddInteger(ExtendPV2);
	Meta_AddInteger(ExtendPV3);
	Meta_AddInteger(ExtendPV4);
	Meta_AddInteger(ExtendPV5);
	Meta_AddInteger(ExtendPV6);
	Meta_AddInteger(ExtendPV7);
	Meta_AddInteger(ExtendPV8);

	CCommsItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CTC8ConfigWnd, CUIViewWnd);

// Overidables

void CTC8ConfigWnd::OnAttach(void)
{
	m_pItem   = (CTC8Input *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("tc8inp"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CTC8ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddInputs();

	EndPage(TRUE);
	}

void CTC8ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag.Left(10) == "ChanEnable" || Tag.Left(9) == "InputType" ) {

		DoEnables();
		}

	CUITC8Process::CheckUpdate(m_pItem, Tag);
	}

// UI Creation

void CTC8ConfigWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_GEN), 2);

	AddUI(m_pItem, TEXT("root"), TEXT("TempUnits"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));

	EndGroup(TRUE);
	}

void CTC8ConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 4);

	AddColHead(CString(IDS_MODULE_ENABLED));
	AddColHead(CString(IDS_MODULE_TC));
	AddColHead(CString(IDS_MODULE_SLOPE));
	AddColHead(CString(IDS_MODULE_OFFSET));

	for( UINT n = 1; n <= 8; n++ ) {

		AddRowHead(CPrintf(CString(IDS_MODULE_CHANNEL), n));

		AddUI(m_pItem, TEXT("root"), CPrintf("ChanEnable%u",  n));
		AddUI(m_pItem, TEXT("root"), CPrintf("InputType%u",   n));
		AddUI(m_pItem, TEXT("root"), CPrintf("InputSlope%u",  n));
		AddUI(m_pItem, TEXT("root"), CPrintf("InputOffset%u", n));
		}

	EndTable();
	}

// Enabling

void CTC8ConfigWnd::DoEnables(void)
{
	for( UINT n = 0; n < 8; n++ ) {

		EnableChannel(n);
		}
	}

void CTC8ConfigWnd::EnableChannel(UINT n)
{
	BOOL fEnable = FALSE;

	BOOL fTypeTC = FALSE;

	switch( ++n ) {
		
		case 1:
			fEnable =  m_pItem->m_ChanEnable1;
			
			fTypeTC = (m_pItem->m_InputType1 != TC_TYPELIN);
			
			break;

		case 2:
			fEnable =  m_pItem->m_ChanEnable2;
			
			fTypeTC = (m_pItem->m_InputType2 != TC_TYPELIN);
			
			break;

		case 3:
			fEnable =  m_pItem->m_ChanEnable3;
			
			fTypeTC = (m_pItem->m_InputType3 != TC_TYPELIN);
			
			break;

		case 4:
			fEnable =  m_pItem->m_ChanEnable4;
			
			fTypeTC = (m_pItem->m_InputType4 != TC_TYPELIN);
			
			break;

		case 5:
			fEnable =  m_pItem->m_ChanEnable5;
			
			fTypeTC = (m_pItem->m_InputType5 != TC_TYPELIN);
			
			break;

		case 6:
			fEnable =  m_pItem->m_ChanEnable6;
			
			fTypeTC = (m_pItem->m_InputType6 != TC_TYPELIN);
			
			break;

		case 7:
			fEnable =  m_pItem->m_ChanEnable7;
			
			fTypeTC = (m_pItem->m_InputType7 != TC_TYPELIN);
			
			break;

		case 8:
			fEnable =  m_pItem->m_ChanEnable8;
			
			fTypeTC = (m_pItem->m_InputType8 != TC_TYPELIN);
			
			break;
		}

	EnableUI(CPrintf("InputType%u",   n), fEnable);
	EnableUI(CPrintf("InputSlope%u",  n), fEnable && fTypeTC);
	EnableUI(CPrintf("InputOffset%u", n), fEnable && fTypeTC);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Millivolt Scaling View
// 

// Runtime Class

AfxImplementRuntimeClass(CTC8ScaleWnd, CUIViewWnd);

// Overidables

void CTC8ScaleWnd::OnAttach(void)
{
	m_pItem   = (CTC8Input *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("tc8inp"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CTC8ScaleWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddInputs();

	EndPage(TRUE);
	}

void CTC8ScaleWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	CUITC8Process::CheckUpdate(m_pItem, Tag);
	}

// UI Creation

void CTC8ScaleWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_GEN), 2);

	AddUI(m_pItem, TEXT("root"), TEXT("ProcDP"));

	EndGroup(TRUE);
	}

void CTC8ScaleWnd::AddInputs(void)
{
	if( !m_pItem->m_fGraphite ) {

		StartTable(CString(IDS_MODULE_MV) + L" " + CString(IDS_MODULE_SCALING), 5);

		AddColHead(L"");
		AddColHead(L"");
		AddColHead(L"");
		AddColHead(L"");

		AddColHead(CString(IDS_MODULE_SBPV1));

		AddColHeadExtraRow();

		AddColHead(CString(IDS_NAME_DL));
		AddColHead(CString(IDS_NAME_DH));
		AddColHead(CString(IDS_NAME_SLK));
		AddColHead(CString(IDS_NAME_SHK));

		AddColHead(CString(IDS_NAME_DV));

		for( UINT n = 0; n < 8; n++ ) {

			AddRowHead(CPrintf(CString(IDS_MODULE_CHANNEL), n+1));

			AddUI(m_pItem, TEXT("root"), CPrintf("DisplayLo%u", n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("DisplayHi%u", n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("SignalLo%u",  n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("SignalHi%u",  n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("ExtendPV%u",  n+1));
			}

		EndTable();
		}
	else {
		StartTable(CString(IDS_MODULE_MV) + L" " + CString(IDS_MODULE_SCALING), 4);

		AddColHead(L"");
		AddColHead(L"");
		AddColHead(L"");
		AddColHead(L"");

		AddColHeadExtraRow();

		AddColHead(CString(IDS_NAME_DL));
		AddColHead(CString(IDS_NAME_DH));
		AddColHead(CString(IDS_NAME_SLK));
		AddColHead(CString(IDS_NAME_SHK));

		for( UINT n = 0; n < 8; n++ ) {

			AddRowHead(CPrintf(CString(IDS_MODULE_CHANNEL), n+1));

			AddUI(m_pItem, TEXT("root"), CPrintf("DisplayLo%u", n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("DisplayHi%u", n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("SignalLo%u",  n+1));
			AddUI(m_pItem, TEXT("root"), CPrintf("SignalHi%u",  n+1));
			}

		EndTable();
		}
	}

// Enabling

void CTC8ScaleWnd::DoEnables(void)
{
	for( UINT n = 0; n < 8; n++ ) {

		EnableChannel(n);
		}
	}

void CTC8ScaleWnd::EnableChannel(UINT n)
{
	BOOL fEnable = FALSE;

	BOOL fTypeTC = FALSE;

	switch( ++n ) {
		
		case 1:
			fEnable =  m_pItem->m_ChanEnable1;
			
			fTypeTC = (m_pItem->m_InputType1 != TC_TYPELIN);
			
			break;

		case 2:
			fEnable =  m_pItem->m_ChanEnable2;
			
			fTypeTC = (m_pItem->m_InputType2 != TC_TYPELIN);
			
			break;

		case 3:
			fEnable =  m_pItem->m_ChanEnable3;
			
			fTypeTC = (m_pItem->m_InputType3 != TC_TYPELIN);
			
			break;

		case 4:
			fEnable =  m_pItem->m_ChanEnable4;
			
			fTypeTC = (m_pItem->m_InputType4 != TC_TYPELIN);
			
			break;

		case 5:
			fEnable =  m_pItem->m_ChanEnable5;
			
			fTypeTC = (m_pItem->m_InputType5 != TC_TYPELIN);
			
			break;

		case 6:
			fEnable =  m_pItem->m_ChanEnable6;
			
			fTypeTC = (m_pItem->m_InputType6 != TC_TYPELIN);
			
			break;

		case 7:
			fEnable =  m_pItem->m_ChanEnable7;
			
			fTypeTC = (m_pItem->m_InputType7 != TC_TYPELIN);
			
			break;

		case 8:
			fEnable =  m_pItem->m_ChanEnable8;
			
			fTypeTC = (m_pItem->m_InputType8 != TC_TYPELIN);
			
			break;
		}

	EnableUI(CPrintf("DisplayLo%u", n), fEnable && !fTypeTC);
	EnableUI(CPrintf("DisplayHi%u", n), fEnable && !fTypeTC);
	EnableUI(CPrintf("SignalLo%u",  n), fEnable && !fTypeTC);
	EnableUI(CPrintf("SignalHi%u",  n), fEnable && !fTypeTC);
	EnableUI(CPrintf("ExtendPV%u",  n), fEnable && !fTypeTC);
	}

// End of File
