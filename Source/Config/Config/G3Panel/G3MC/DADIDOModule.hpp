
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOModule_HPP

#define INCLUDE_DADIDOModule_HPP

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDADIDOInput;
class CDADIDOOutput;
class CDADIDOInputConfig;
class CDADIDOOutputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO Module
//

class CDADIDOModule : public CManticoreGenericModule
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDADIDOModule(void);

	// Destructor
	~CDADIDOModule(void);

	// UI Management
	CViewWnd * CreateMainView(void);

	// Comms Object Access
	UINT GetObjectCount(void);
	BOOL GetObjectData(UINT uIndex, CObjectData &Data);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	CDADIDOInput        * m_pInput;
	CDADIDOOutput       * m_pOutput;
	CDADIDOInputConfig  * m_pInputConfig;
	CDADIDOOutputConfig * m_pOutputConfig;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Download Data
	void MakeConfigData(CInitData &Init);

	// Firmware
	void FindFirmware(void);
};

// End of File

#endif
