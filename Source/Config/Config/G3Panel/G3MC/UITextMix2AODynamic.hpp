
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UITextMix2AODynamic_HPP

#define INCLUDE_UITextMix2AODynamic_HPP

#include "DAMix2AnalogOutputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Analog Output Module Dynamic Value
//

class CUITextMix2AODynamic : public CUITextInteger
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUITextMix2AODynamic(void);

	// Destructor
	~CUITextMix2AODynamic(void);

protected:
	// Data Members
	CDAMix2AnalogOutputConfig * m_pConfig;
	CString	                    m_Type;

	// Core Overidables
	void OnBind(void);

	// Implementation
	void GetConfig(void);
	void FindPlaces(void);
	void FindUnits(void);
	void FindRanges(void);
	void SetMinMax(UINT OutType);
	void CheckFlags(void);

	// Friends
	friend class CUIMix2AODynamic;
};

// End of File

#endif
