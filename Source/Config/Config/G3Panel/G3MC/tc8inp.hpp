
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TC8INP_HPP

#define INCLUDE_TC8INP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\8indbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTC8Input;
class CTC8ConfigWnd;
class CTC8ScaleWnd;

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Input Item
//

class CTC8Input : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTC8Input(BOOL fGraphite = FALSE);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Data Members
		UINT	m_TempUnits;
		UINT	m_InputType1;
		UINT	m_InputType2;
		UINT	m_InputType3;
		UINT	m_InputType4;
		UINT	m_InputType5;
		UINT	m_InputType6;
		UINT	m_InputType7;
		UINT	m_InputType8;
		UINT	m_InputFilter;
		INT	m_InputOffset1;
		INT	m_InputOffset2;
		INT	m_InputOffset3;
		INT	m_InputOffset4;
		INT	m_InputOffset5;
		INT	m_InputOffset6;
		INT	m_InputOffset7;
		INT	m_InputOffset8;
		INT	m_InputSlope1;
		INT	m_InputSlope2;
		INT	m_InputSlope3;
		INT	m_InputSlope4;
		INT	m_InputSlope5;
		INT	m_InputSlope6;
		INT	m_InputSlope7;
		INT	m_InputSlope8;
		UINT	m_ProcDP;
		INT	m_DisplayLo1;
		INT	m_DisplayLo2;
		INT	m_DisplayLo3;
		INT	m_DisplayLo4;
		INT	m_DisplayLo5;
		INT	m_DisplayLo6;
		INT	m_DisplayLo7;
		INT	m_DisplayLo8;
		INT	m_DisplayHi1;
		INT	m_DisplayHi2;
		INT	m_DisplayHi3;
		INT	m_DisplayHi4;
		INT	m_DisplayHi5;
		INT	m_DisplayHi6;
		INT	m_DisplayHi7;
		INT	m_DisplayHi8;
		INT	m_SignalLo1;
		INT	m_SignalLo2;
		INT	m_SignalLo3;
		INT	m_SignalLo4;
		INT	m_SignalLo5;
		INT	m_SignalLo6;
		INT	m_SignalLo7;
		INT	m_SignalLo8;
		INT	m_SignalHi1;
		INT	m_SignalHi2;
		INT	m_SignalHi3;
		INT	m_SignalHi4;
		INT	m_SignalHi5;
		INT	m_SignalHi6;
		INT	m_SignalHi7;
		INT	m_SignalHi8;
		UINT	m_ChanEnable1;
		UINT	m_ChanEnable2;
		UINT	m_ChanEnable3;
		UINT	m_ChanEnable4;
		UINT	m_ChanEnable5;
		UINT	m_ChanEnable6;
		UINT	m_ChanEnable7;
		UINT	m_ChanEnable8;
		UINT	m_ExtendPV1;
		UINT	m_ExtendPV2;
		UINT	m_ExtendPV3;
		UINT	m_ExtendPV4;
		UINT	m_ExtendPV5;
		UINT	m_ExtendPV6;
		UINT	m_ExtendPV7;
		UINT	m_ExtendPV8;

		// Data
		BOOL m_fGraphite;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Configuration View
//

class CTC8ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CTC8Input * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// UI Creation
		void AddGeneral(void);
		void AddInputs(void);

		// Enabling
		void DoEnables(void);
		void EnableChannel(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Millivolt Scaling View
//

class CTC8ScaleWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CTC8Input * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// UI Creation
		void AddGeneral(void);
		void AddInputs(void);

		// Enabling
		void DoEnables(void);
		void EnableChannel(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- TC8 Linear Millivolt Process Value
//

class CUITC8Process : public CUIEditBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITC8Process(void);

		// Destructor
		~CUITC8Process(void);

		// Update Support
		static void CheckUpdate(CTC8Input *pLoop, CString const &Tag);

		// Operations
		void Update(BOOL fKeep);
		void UpdateUnits(void);

	protected:
		// Linked List
		static CUITC8Process * m_pHead;
		static CUITC8Process * m_pTail;

		// Data Members
		CUITC8Process * m_pNext;
		CUITC8Process * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- TC8 Linear Millivolt Process Value
//

class CUITextTC8Process : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextTC8Process(void);

		// Destructor
		~CUITextTC8Process(void);

	protected:
		// Data Members
		CTC8Input * m_pLoop;
		char	    m_cType;
		double	    m_t1;
		double	    m_t2;

		// Core Overidables
		void OnBind(void);

		// Implementation
		void GetConfig(void);
		void CheckFlags(void);

		// Scaling
		INT  StoreToDisp(INT nData);
		INT  DispToStore(INT nData);

		// Friends
		friend class CUITC8Process;
	};

// End of File

#endif
