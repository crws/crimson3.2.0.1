
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GMDIO14_HPP

#define INCLUDE_GMDIO14_HPP

//////////////////////////////////////////////////////////////////////////
//
// UIN4 Configuration
//

class CGraphiteDIO14Config : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteDIO14Config(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Persistance
		void Load(CTreeFile &File);

		// Item Properties
		UINT m_InpMode1;
		UINT m_InpMode2;
		UINT m_InpMode3;
		UINT m_InpMode4;
		UINT m_InpMode5;
		UINT m_InpMode6;
		UINT m_InpMode7;
		UINT m_InpMode8;
		UINT m_Output1;
		UINT m_Output2;
		UINT m_Output3;
		UINT m_Output4;
		UINT m_Output5;
		UINT m_Output6;
		UINT m_Input1;
		UINT m_Input2;
		UINT m_Input3;
		UINT m_Input4;
		UINT m_Input5;
		UINT m_Input6;
		UINT m_Input7;
		UINT m_Input8;
		UINT m_Test1;
		UINT m_Test2;
		UINT m_Test3;
		UINT m_Test4;
		UINT m_Test5;
		UINT m_Test6;
		UINT m_OutMode1;
		UINT m_OutMode2;
		UINT m_OutMode3;
		UINT m_OutMode4;
		UINT m_OutMode5;
		UINT m_OutMode6;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module
//

class CGMDIO14Module : public CGraphiteGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMDIO14Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Item Properties
		CGraphiteDIO14Config *m_pConfig;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module Window
//

class CGraphiteDIO14MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteDIO14MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd  * m_pMult;
		CGMDIO14Module * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddPages(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration View
//

class CGraphiteDIO14ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		CGraphiteDIO14ConfigWnd(void);

	protected:
		// Data Members
		CGraphiteDIO14Config  * m_pItem;
		UINT			m_uPage;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// Implementation
		void AddInputs(void);
		void AddOutputs(void);
	};

// End of File

#endif
