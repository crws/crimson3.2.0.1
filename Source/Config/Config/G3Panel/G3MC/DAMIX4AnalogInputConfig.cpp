
#include "intern.hpp"

#include "DAMix4AnalogInputConfig.hpp"

#include "DAMix4AnalogInputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dauin6props.h"

#include "import/manticore/dauin6dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Input Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4AnalogInputConfig, CCommsItem);

// Property List

CCommsList const CDAMix4AnalogInputConfig::m_CommsList[] = {

	{ 0, "Range1",		PROPID_RANGE1,		usageWriteInit,  IDS_NAME_ANRNG1 },
	{ 0, "Range2",		PROPID_RANGE2,		usageWriteInit,  IDS_NAME_ANRNG2 },
	{ 0, "Range3",		PROPID_RANGE3,		usageWriteInit,  IDS_NAME_ANRNG3 },
	{ 0, "Range4",		PROPID_RANGE4,		usageWriteInit,  IDS_NAME_ANRNG4 },

	{ 0, "Sample1",		PROPID_SAMPLE1,		usageWriteInit,  IDS_NAME_ANSMP1 },
	{ 0, "Sample2",		PROPID_SAMPLE2,		usageWriteInit,  IDS_NAME_ANSMP2 },
	{ 0, "Sample3",		PROPID_SAMPLE3,		usageWriteInit,  IDS_NAME_ANSMP3 },
	{ 0, "Sample4",		PROPID_SAMPLE4,		usageWriteInit,  IDS_NAME_ANSMP4 },

	{ 0, "TempUnits1",	PROPID_UNITS1,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits2",	PROPID_UNITS2,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits3",	PROPID_UNITS3,		usageWriteInit,	IDS_MODULE_UNITS },
	{ 0, "TempUnits4",	PROPID_UNITS4,		usageWriteInit,	IDS_MODULE_UNITS },

	{ 1, "Offset1",		PROPID_OFFSET1,		usageWriteBoth,	IDS_NAME_IO1	 },
	{ 1, "Offset2",		PROPID_OFFSET2,		usageWriteBoth,	IDS_NAME_IO2	 },
	{ 1, "Offset3",		PROPID_OFFSET3,		usageWriteBoth,	IDS_NAME_IO3	 },
	{ 1, "Offset4",		PROPID_OFFSET4,		usageWriteBoth,	IDS_NAME_IO4	 },

	{ 1, "Slope1",		PROPID_TEMP_SLOPE1,	usageWriteBoth,	IDS_NAME_IS1	 },
	{ 1, "Slope2",		PROPID_TEMP_SLOPE2,	usageWriteBoth,	IDS_NAME_IS2	 },
	{ 1, "Slope3",		PROPID_TEMP_SLOPE3,	usageWriteBoth,	IDS_NAME_IS3	 },
	{ 1, "Slope4",		PROPID_TEMP_SLOPE4,	usageWriteBoth,	IDS_NAME_IS4	 },

	{ 1, "Filter1",		PROPID_IN_FILTER1,	usageWriteBoth,	IDS_NAME_IF1	 },
	{ 1, "Filter2",		PROPID_IN_FILTER2,	usageWriteBoth,	IDS_NAME_IF2	 },
	{ 1, "Filter3",		PROPID_IN_FILTER3,      usageWriteBoth,	IDS_NAME_IF3	 },
	{ 1, "Filter4",		PROPID_IN_FILTER4,	usageWriteBoth,	IDS_NAME_IF4	 },

	{ 0, "ProcMin1",	PROPID_MINIMUM1,	usageWriteInit,	IDS_NAME_PMI1	 },
	{ 0, "ProcMin2",	PROPID_MINIMUM2,	usageWriteInit,	IDS_NAME_PMI2	 },
	{ 0, "ProcMin3",	PROPID_MINIMUM3,	usageWriteInit,	IDS_NAME_PMI3	 },
	{ 0, "ProcMin4",	PROPID_MINIMUM4,	usageWriteInit,	IDS_NAME_PMI4	 },

	{ 0, "ProcMax1",	PROPID_MAXIMUM1,	usageWriteInit,	IDS_NAME_PMA1	 },
	{ 0, "ProcMax2",	PROPID_MAXIMUM2,	usageWriteInit,	IDS_NAME_PMA2	 },
	{ 0, "ProcMax3",	PROPID_MAXIMUM3,	usageWriteInit,	IDS_NAME_PMA3	 },
	{ 0, "ProcMax4",	PROPID_MAXIMUM4,	usageWriteInit,	IDS_NAME_PMA4	 },

	{ 0, "Root1",		PROPID_ROOT1,		usageWriteInit,	IDS_NAME_SQRT1	 },
	{ 0, "Root2",		PROPID_ROOT2,		usageWriteInit,	IDS_NAME_SQRT2	 },
	{ 0, "Root3",		PROPID_ROOT3,		usageWriteInit,	IDS_NAME_SQRT3	 },
	{ 0, "Root4",		PROPID_ROOT4,		usageWriteInit,	IDS_NAME_SQRT4	 },

};

// Constructor

CDAMix4AnalogInputConfig::CDAMix4AnalogInputConfig(void)
{
	m_Range1	= 3;
	m_Range2	= 3;
	m_Range3	= 3;
	m_Range4	= 3;

	m_Sample1	= 4;
	m_Sample2	= 4;
	m_Sample3	= 4;
	m_Sample4	= 4;

	m_TempUnits1	= 2;
	m_TempUnits2	= 2;
	m_TempUnits3	= 2;
	m_TempUnits4	= 2;

	m_Offset1	= 0;
	m_Offset2	= 0;
	m_Offset3	= 0;
	m_Offset4	= 0;

	m_Slope1	= 1000;
	m_Slope2	= 1000;
	m_Slope3	= 1000;
	m_Slope4	= 1000;

	m_ProcMin1	= 0;
	m_ProcMin2	= 0;
	m_ProcMin3	= 0;
	m_ProcMin4	= 0;

	m_ProcMax1	= 10000;
	m_ProcMax2	= 10000;
	m_ProcMax3	= 10000;
	m_ProcMax4	= 10000;

	m_Root1   	= 0;
	m_Root2   	= 0;
	m_Root3   	= 0;
	m_Root4   	= 0;

	m_Filter1	= 20;
	m_Filter2	= 20;
	m_Filter3	= 20;
	m_Filter4	= 20;

	m_ProcDP1	= 2;
	m_ProcDP2	= 2;
	m_ProcDP3	= 2;
	m_ProcDP4	= 2;

	m_ProcUnits1	= "%";
	m_ProcUnits2	= "%";
	m_ProcUnits3	= "%";
	m_ProcUnits4	= "%";

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAMix4AnalogInputConfig::GetPageCount(void)
{
	return 4;
}

CString CDAMix4AnalogInputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
		case 2:
		case 3:
			return CPrintf(IDS("AI %d"), n + 1);
	}

	return L"";
}

CViewWnd * CDAMix4AnalogInputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0:
		case 1:
		case 2:
		case 3:
			return New CDAMix4AnalogInputConfigWnd(n);
	}

	return NULL;
}

// Group Names

CString CDAMix4AnalogInputConfig::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1: return L"Control";
	}

	return CCommsItem::GetGroupName(Group);
}

// Conversion

BOOL CDAMix4AnalogInputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 4; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Range%d", n+1), CPrintf(L"InputType%d", n+1)) ) {

				if( ImportNumber(pValue, CPrintf(L"TCType%d", n+1), CPrintf(L"InputTC%d", n+1)) ) {

					ConvertRange(n);
				}
			}

			if( ImportNumber(pValue, CPrintf(L"TempUnits%d", n+1)) ) {

				ConvertTempUnits(n);
			}

			ImportNumber(pValue, CPrintf(L"Offset%d", n+1), CPrintf(L"InputOffset%d", n+1));

			ImportNumber(pValue, CPrintf(L"Filter%d", n+1), CPrintf(L"InputFilter%d", n+1));

			ImportNumber(pValue, CPrintf(L"Slope%d", n+1), CPrintf(L"InputSlope%d", n+1));

			ImportNumber(pValue, CPrintf(L"Root%d", n+1), CPrintf(L"SquareRoot%d", n+1));

			ImportNumber(pValue, CPrintf(L"ProcMin%d", n+1));

			ImportNumber(pValue, CPrintf(L"ProcMax%d", n+1));

			ImportNumber(pValue, CPrintf(L"ProcDP%d", n+1));

			ImportString(pValue, CPrintf(L"ProcUnits%d", n+1));
		}

		return TRUE;
	}

	return FALSE;
}

// Persistence

void CDAMix4AnalogInputConfig::Init(void)
{
	CCommsItem::Init();

	for( UINT n = 0; n < elements(m_TCType); n++ ) {

		m_TCType[n] = 0;
	}
}

// Property Filters

BOOL CDAMix4AnalogInputConfig::SaveProp(CString const &Tag) const
{
	if( Tag.StartsWith(L"TCType") ) {

		return FALSE;
	}

	return TRUE;
}

// Meta Data Creation

void CDAMix4AnalogInputConfig::AddMetaData(void)
{
	Meta_AddInteger(Range1);
	Meta_AddInteger(Range2);
	Meta_AddInteger(Range3);
	Meta_AddInteger(Range4);

	for( UINT n = 0; n < elements(m_TCType); n++ ) {

		UINT uOffset = (PBYTE(m_TCType + n) - PBYTE(this));

		AddMeta(CPrintf("TCType%u", 1 + n), metaInteger, uOffset);
	}

	Meta_AddInteger(Sample1);
	Meta_AddInteger(Sample2);
	Meta_AddInteger(Sample3);
	Meta_AddInteger(Sample4);

	Meta_AddInteger(TempUnits1);
	Meta_AddInteger(TempUnits2);
	Meta_AddInteger(TempUnits3);
	Meta_AddInteger(TempUnits4);

	Meta_AddInteger(Offset1);
	Meta_AddInteger(Offset2);
	Meta_AddInteger(Offset3);
	Meta_AddInteger(Offset4);

	Meta_AddInteger(Slope1);
	Meta_AddInteger(Slope2);
	Meta_AddInteger(Slope3);
	Meta_AddInteger(Slope4);

	Meta_AddInteger(ProcMin1);
	Meta_AddInteger(ProcMin2);
	Meta_AddInteger(ProcMin3);
	Meta_AddInteger(ProcMin4);

	Meta_AddInteger(ProcMax1);
	Meta_AddInteger(ProcMax2);
	Meta_AddInteger(ProcMax3);
	Meta_AddInteger(ProcMax4);

	Meta_AddInteger(Root1);
	Meta_AddInteger(Root2);
	Meta_AddInteger(Root3);
	Meta_AddInteger(Root4);

	Meta_AddInteger(Filter1);
	Meta_AddInteger(Filter2);
	Meta_AddInteger(Filter3);
	Meta_AddInteger(Filter4);

	Meta_AddInteger(ProcDP1);
	Meta_AddInteger(ProcDP2);
	Meta_AddInteger(ProcDP3);
	Meta_AddInteger(ProcDP4);

	Meta_AddString(ProcUnits1);
	Meta_AddString(ProcUnits2);
	Meta_AddString(ProcUnits3);
	Meta_AddString(ProcUnits4);

	CCommsItem::AddMetaData();
}

// Implementation

void CDAMix4AnalogInputConfig::ConvertRange(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Range%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT   uPrev = pData->ReadInteger(this);

		UINT uData[] = { 0, 5, 10, 7, 9, 21, 11 };

		switch( uPrev ) {

			case 5:
				// RTD

				pData->WriteInteger(this, uData[uPrev] + m_TCType[uIndex] - 10);

				break;

			case 6:
				// Thermocouple

				pData->WriteInteger(this, uData[uPrev] + m_TCType[uIndex] - 1);

				break;

			default:

				pData->WriteInteger(this, uData[uPrev]);

				break;
		}

		return;
	}

	AfxAssert(FALSE);
}

void CDAMix4AnalogInputConfig::ConvertTempUnits(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"TempUnits%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT   uPrev = pData->ReadInteger(this);

		UINT uData[] = { 0, 0, 2, 1 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
