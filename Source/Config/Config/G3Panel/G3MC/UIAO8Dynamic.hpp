
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UIAO8Dynamic_HPP

#define INCLUDE_UIAO8Dynamic_HPP

#include "DAAO8OutputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

class CUIAO8Dynamic : public CUIEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUIAO8Dynamic(void);

	// Destructor
	~CUIAO8Dynamic(void);

	// Update Support
	static void CheckUpdate(CDAAO8OutputConfig *pConfig, CString const &Tag);

	// Operations
	void Update(BOOL fKeep);
	void UpdateUnits(void);

protected:
	// Linked List
	static CUIAO8Dynamic * m_pHead;
	static CUIAO8Dynamic * m_pTail;

	// Data Members
	CUIAO8Dynamic * m_pNext;
	CUIAO8Dynamic * m_pPrev;
};

// End of File

#endif
