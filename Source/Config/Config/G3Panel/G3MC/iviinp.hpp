
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IVIINP_HPP

#define INCLUDE_IVIINP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\8indbase.h"

#include "import\8inldbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CIVIInput;
class CIVIConfigWnd;
class CIVILinearWnd;
class CUIIVIProcess;
class CUITextIVIProcess;
class CUILinCurve;
class CIVICurveWnd;
class CIVILinData;

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

class CIVIInput : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIVIInput(void);
		CIVIInput(BYTE IVIModel);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Data Support
		INT     GetScaledPV(UINT Channel, UINT SegPoint);
		BOOL	GetLinEnable(void);
		UINT    GetLinDP(void);
		void	SetLinDP(void);
		INT	GetLinSlope(void);
		void	SetLinSlope(void);
		INT	GetLinOffset(void);
		void	SetLinOffset(void);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		UINT	m_PIRange;
		UINT	m_PVRange;
		BYTE	m_IVIModel;
		UINT	m_ProcessDP1;
		UINT	m_ProcessDP2;
		UINT	m_ProcessDP3;
		UINT	m_ProcessDP4;
		UINT	m_ProcessDP5;
		UINT	m_ProcessDP6;
		UINT	m_ProcessDP7;
		UINT	m_ProcessDP8;
		UINT	m_InputFilter;
		INT	m_ProcessMin1;
		INT	m_ProcessMin2;
		INT	m_ProcessMin3;
		INT	m_ProcessMin4;
		INT	m_ProcessMin5;
		INT	m_ProcessMin6;
		INT	m_ProcessMin7;
		INT	m_ProcessMin8;
		INT	m_ProcessMax1;
		INT	m_ProcessMax2;
		INT	m_ProcessMax3;
		INT	m_ProcessMax4;
		INT	m_ProcessMax5;
		INT	m_ProcessMax6;
		INT	m_ProcessMax7;
		INT	m_ProcessMax8;
		UINT	m_SquareRt1;
		UINT	m_SquareRt2;
		UINT	m_SquareRt3;
		UINT	m_SquareRt4;
		UINT	m_SquareRt5;
		UINT	m_SquareRt6;
		UINT	m_SquareRt7;
		UINT	m_SquareRt8;
		UINT	m_ChanEnable1;
		UINT	m_ChanEnable2;
		UINT	m_ChanEnable3;
		UINT	m_ChanEnable4;
		UINT	m_ChanEnable5;
		UINT	m_ChanEnable6;
		UINT	m_ChanEnable7;
		UINT	m_ChanEnable8;
		UINT	m_ExtendPV1;
		UINT	m_ExtendPV2;
		UINT	m_ExtendPV3;
		UINT	m_ExtendPV4;
		UINT	m_ExtendPV5;
		UINT	m_ExtendPV6;
		UINT	m_ExtendPV7;
		UINT	m_ExtendPV8;
		UINT	m_Linearize  [HDW_CHANNELS];
		UINT	m_LinSegments[HDW_CHANNELS];
		INT	m_LinInput   [HDW_CHANNELS][LIN_POINTS];
		INT	m_LinPV      [HDW_CHANNELS][LIN_POINTS];
		INT	m_LinSlope   [HDW_CHANNELS];
		INT	m_LinOffset  [HDW_CHANNELS];
		INT	m_LinSlopeUI;
		INT	m_LinOffsetUI;
		UINT	m_LinChannel;
		UINT	m_LinDP;

	protected:
		// Static Data
		static CCommsList const m_CommsListStd[];
		static CCommsList const m_CommsListLin[];

		// Download Filter
		BOOL SaveProp(CString Tag) const;

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
//

class CIVIConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CIVIInput *m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddGeneral(void);
		void AddInputs(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Configuration View
//

class CIVILinearWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CIVIInput   *m_pItem;
		CUILinCurve *m_pCurve;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddLinear(void);
		void AddCurve(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphic UI Element -- 8-Channel PI and PV Process Value
//

class CUIIVIProcess : public CUIEditBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIIVIProcess(void);

		// Destructor
		~CUIIVIProcess(void);

		// Update Support
		static void CheckUpdate(CIVIInput *pLoop, CString const &Tag);

		// Operations
		void Update(void);

	protected:
		// Linked List
		static CUIIVIProcess * m_pHead;
		static CUIIVIProcess * m_pTail;

		// Data Members
		CUIIVIProcess * m_pNext;
		CUIIVIProcess * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- 8-Channel PI and PV Process Value
//

class CUITextIVIProcess : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextIVIProcess(void);

		// Destructor
		~CUITextIVIProcess(void);

	protected:
		// Data Members
		CIVIInput     * m_pLoop;
		char		m_cType;
		double		m_t1;
		double		m_t2;

		// Core Overidables
		void OnBind(void);

		// Implementation
		void GetConfig(void);
		void CheckFlags(void);

		// Friends
		friend class CUIIVIProcess;
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Graph
//

class CUILinCurve : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUILinCurve(CIVIInput *pInput);

		// Destructor
		~CUILinCurve(void);

		// Operations
		void Update(void);
		void Exclude(CWnd &Wnd, CDC &DC);

	protected:
		// Data Members
		CLayFormPad  *m_pLayout;
		CIVICurveWnd *m_pCurveWnd;

		// Core Overidables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnMove(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Graph Constants
//

#define IVI_XMAX	1230
#define IVI_XMIN	   0

#define IVI_YMAX	1100
#define IVI_YMIN	   0

#define IVI_XGRIDMAX	(IVI_XMAX - 70)
#define IVI_XGRIDMIN	(IVI_XGRIDMAX - 1000)

#define IVI_YGRIDMAX	(IVI_YMAX - 30)
#define IVI_YGRIDMIN	(IVI_YGRIDMAX - 1000)

#define IVI_XTEXT	 250
#define IVI_YTEXT	  30

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Graph Window
//

class CIVICurveWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIVICurveWnd(CIVIInput *pInput);

		// Destructor
		~CIVICurveWnd(void);

		// Operations
		void Update(void);
		void Exclude(CWnd &Wnd, CDC &DC);

	protected:
		// Data Members
		CIVIInput *m_pInput;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnLButtonUp(UINT uFlags, CPoint Pos);

		// Implementation
		void    Draw(CDC &DC, CRect Rect);
		CPoint  GetPointLocation(UINT SegPoint);
		CRect   DrawPoint(UINT SegPoint);
		CString Format(INT nData, UINT DP);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Data Entry Dialog
//

class CIVILinData : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIVILinData(CIVIInput *pInput);

	protected:	

		// Data Members
		CIVIInput *m_pInput;
		CString    m_LinData;
		UINT	   m_Channel;
		UINT	   m_Segments;
		UINT	   m_DP;
		INT	   m_Input[LIN_POINTS];
		INT	   m_PV[LIN_POINTS];

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnKillFocus(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);

		// Implementation
		void    OnPaint(void);
		CString Format(INT nData, UINT DP, BOOL PlusSign);
		void    FormatLinData(void);
		INT     Parse(PCTXT pText, UINT DP);
		void    ParseLinData(void);
		INT     CheckInputRange(INT nData);
		INT     CheckPVRange(INT nData);
	};

// End of File

#endif
