
//////////////////////////////////////////////////////////////////////////
//
// CSDIO14 Configuration
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CDIO14_HPP

#define INCLUDE_CDIO14_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\dio01dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	witoa(v,s,b) swprintf((s),L"%u",(v))

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDIO14ModCfg;
class CDIO14ConfigWnd;
class CDIO14LogicWnd;
class CDIO14LogicDialog;
class CDIO14CfgSym;

//////////////////////////////////////////////////////////////////////////
//
// DIO Basic Configuration
//

class CDIO14ModCfg : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDIO14ModCfg(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		void RunLogic(void);
		void DefRefSymbols(void);
		void InitLogic(void);
		void LogicEngine(void);
		void MakeASCIIEquations(void);
		void MakeBinaryEquations(void);

	public:
		// Config Data

		BOOL	m_Input1;
		BOOL	m_Input2;
		BOOL	m_Input3;
		BOOL	m_Input4;
		BOOL	m_Input5;
		BOOL	m_Input6;
		BOOL	m_Input7;
		BOOL	m_Input8;

		BOOL	m_Output1;
		BOOL	m_Output2;
		BOOL	m_Output3;
		BOOL	m_Output4;
		BOOL	m_Output5;
		BOOL	m_Output6;
			
		UINT	m_InputMode1;
		UINT	m_InputMode2;
		UINT	m_InputMode3;
		UINT	m_InputMode4;
		UINT	m_InputMode5;
		UINT	m_InputMode6;
		UINT	m_InputMode7;
		UINT	m_InputMode8;

		UINT	m_InputCoil1;
		UINT	m_InputCoil2;
		UINT	m_InputCoil3;
		UINT	m_InputCoil4;
		UINT	m_InputCoil5;
		UINT	m_InputCoil6;
		UINT	m_InputCoil7;
		UINT	m_InputCoil8;

		UINT	m_OutputCoil1;
		UINT	m_OutputCoil2;
		UINT	m_OutputCoil3;
		UINT	m_OutputCoil4;
		UINT	m_OutputCoil5;
		UINT	m_OutputCoil6;
		UINT	m_OutputCoil7;
		UINT	m_OutputCoil8;

		UINT	m_CounterPreset1;
		UINT	m_CounterPreset2;
		UINT	m_CounterPreset3;
		UINT	m_CounterPreset4;
		UINT	m_CounterPreset5;
		UINT	m_CounterPreset6;
		UINT	m_CounterPreset7;
		UINT	m_CounterPreset8;

		UINT	m_CounterValue1;
		UINT	m_CounterValue2;
		UINT	m_CounterValue3;
		UINT	m_CounterValue4;
		UINT	m_CounterValue5;
		UINT	m_CounterValue6;
		UINT	m_CounterValue7;
		UINT	m_CounterValue8;

		UINT	m_TimerPreset1;
		UINT	m_TimerPreset2;
		UINT	m_TimerPreset3;
		UINT	m_TimerPreset4;
		UINT	m_TimerPreset5;
		UINT	m_TimerPreset6;
		UINT	m_TimerPreset7;
		UINT	m_TimerPreset8;

		UINT	m_TimerValue1;
		UINT	m_TimerValue2;
		UINT	m_TimerValue3;
		UINT	m_TimerValue4;
		UINT	m_TimerValue5;
		UINT	m_TimerValue6;
		UINT	m_TimerValue7;
		UINT	m_TimerValue8;
				
		UINT	m_LogicDisable;
		UINT	m_IncludeDownload;
				
		//Logic config data -------------------
		
		//Symbol object
		struct SYMBOL
		{
			UINT	Left;
			UINT	Top;
			UINT	Width;
			UINT	Height;
			UINT	Type;
			UINT	NumInputs;
			UINT	NumOutputs;
			UINT	PinSpace;
			UINT	PinOffset;
			UINT	BMPOffsetX;
			BYTE	Prop[NUM_PROPS];
			UINT	PinWire[NUM_PINS];
			UINT	WireEnd[NUM_PINS];
		
			} m_Sym[NUM_SYMS];

		//Wire object
		struct WIRE
		{
			UINT	Sym1;
			UINT	Pin1;
			UINT	Sym2;
			UINT	Pin2;
			UINT	x1;
			UINT	y1;
			UINT	x2;
			UINT	y2;
			double	Frac;
			double	Nfrac;
			UINT	DrawMode;
		
			} m_Wire[NUM_WIRES];

		UINT	m_NSI;
		UINT	m_NumWires;
		
		//-------------------------------------
		
		BOOL	m_InputFlag[NUM_SYMS];
		
		UINT	m_NumEqnEle;
		UINT	m_EqnElement;
		RECT	m_RInput[8];
		RECT	m_ROutput[6];

		BOOL	m_InputState[8];
		BOOL	m_NewLine;
		
		BOOL	m_OutputIsDef[NUM_SYMS];
		BOOL	m_EResult[NUM_SYMS];
		BOOL	m_OldResult[NUM_SYMS];
		BOOL	m_LogicStateChange;
		
		char	m_Equation[NUM_EQUATIONS][NUM_EQ_ELEMENTS];
		char	m_OprStr[80];

		UINT	m_NumEquations;

		BYTE	m_BinEq[100*20];
		UINT	m_NumBinElements;

		BOOL	m_LogicTimerRunning;

		UINT	m_LogicTimer[NUM_SYMS];

	protected:
		// Static Data
		static CCommsList m_CommsList[];

		BOOL IncludeProp(WORD PropID);
		
		// Implementation
		void AddMetaData(void);

		UINT GetEquationOutput(UINT eqn);
		void GetEquationOperator(UINT eqn);
		BOOL GetInputStatus(UINT eqn,UINT inp);
		UINT GetInputElement(UINT eqn,UINT inp);

		void SetInputStates(void);

		BOOL Do_AND_Function(UINT inst,UINT eqn);
		BOOL Do_OR_Function(UINT inst,UINT eqn);
		BOOL Do_XOR_Function(UINT inst,UINT eqn);
		BOOL Do_NOT_Function(UINT inst,UINT eqn);
		BOOL Do_TIMERUP_Function(UINT inst,UINT eqn);
		BOOL Do_TIMERDN_Function(UINT inst,UINT eqn);
		BOOL Do_COUNTUP_Function(UINT inst,UINT eqn);
		BOOL Do_COUNTDN_Function(UINT inst,UINT eqn);
		BOOL Do_LATCH_Function(UINT inst,UINT eqn);
		BOOL Do_INCOIL_Function(UINT inst,UINT eqn);
		BOOL Do_OUTCOIL_Function(UINT inst,UINT eqn);
		BOOL Do_JCT_Function(UINT inst,UINT eqn);
		BOOL Do_MOUT_Function(UINT inst,UINT eqn);

		void Init(void);
		void Kill(void);
		void Load(CTreeFile &File);
		void LoadLogic(CTreeFile &File);
		void LoadLogicSymbol(CTreeFile &File, SYMBOL &Sym);
		void LoadWire(CTreeFile &File, WIRE &Wire);
		void Save(CTreeFile &File);
		void SaveLogic(CTreeFile &File);
		void SaveLogicSymbol(CTreeFile &File, SYMBOL &Sym);
		void SaveWire(CTreeFile &File, WIRE &Wire);
		void ArrayErr(UINT err);
	};

//////////////////////////////////////////////////////////////////////////
//
// DIO Configuration View
//

class CDIO14ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Data Members
		CDIO14ModCfg *m_pItem;

	protected:
		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	

		// UI Creation
		void AddInputs(void);
		void AddInit(void);

		// Implementation
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DIO Logic View
//

class CDIO14LogicWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Data Members
		CDIO14ModCfg *m_pItem;

		void DrawTransparentBitmap(HDC hdc, HBITMAP hBitmap,
					   short xStart,short yStart,
					   short Width,short Height,		   
					   short Xoffset,
					   COLORREF cTransparentColor);

	protected:

		//------------------------------------

		//Undo symbol list
		struct UNDO_SYMBOL
		{
			UINT	Left;
			UINT	Top;
			UINT	Width;
			UINT	Height;
			UINT	Type;
			UINT	NumInputs;
			UINT	NumOutputs;
			UINT	PinSpace;
			UINT	PinOffset;
			UINT	BMPOffsetX;
			BYTE	Prop[NUM_PROPS];
			UINT	PinWire[NUM_PINS];
			UINT	WireEnd[NUM_PINS];
		
			} m_USym[NUM_UNDO_LEVELS][NUM_SYMS];

		//Undo wire list
		struct UNDO_WIRE
		{
			UINT	Sym1;
			UINT	Pin1;
			UINT	Sym2;
			UINT	Pin2;
			UINT	x1;
			UINT	y1;
			UINT	x2;
			UINT	y2;
			double	Frac;
			double	Nfrac;
			UINT	DrawMode;

			} m_UWire[NUM_UNDO_LEVELS][NUM_WIRES];

		UINT	m_UNSI[NUM_UNDO_LEVELS];
		UINT	m_UNumWires[NUM_UNDO_LEVELS];

		UINT	m_UlistIndex;
		UINT	m_UlistLimit;
		UINT	m_RlistLimit;

		//------------------------------------

		//Info Baloon object
		struct INFO_BALOON
		{
			BOOL	Disp;
			UINT	Type;
			UINT	Inst;
			UINT	NumLines;
			UINT	Width;
			CPoint  Pos;

			} m_InfoBal;

		//Button object
		struct BUTTON
		{
			UINT	Left;
			UINT	Top;
			UINT	Width;
			UINT	Height;
			BOOL	State;
			BOOL	Enabled;
			BOOL	Hover;

			} m_Button[NUM_BUTTONS];

		UINT	m_ButtonPrev;
		
		UINT	m_Oldx;
		UINT	m_Oldy;
		UINT	m_CursDeltaX;
		UINT	m_CursDeltaY;
		UINT	m_VertSegX;
		UINT	m_VertSegY;
		UINT	m_GridOldX;
		UINT	m_GridOldY;
		
		BYTE	m_BinEq[100*20];
		UINT	m_NumBinaryElements;

		UINT	m_PinSel[NUM_PINS];
		UINT	m_ConnectMode;
		UINT	m_EditMode;
		UINT	m_SelectedSymbol;
		UINT	m_SelectedRef;
		
		UINT	m_SymWireSel;
		UINT	m_WireStartSym;
		UINT	m_SymInpSel;
		UINT	m_SelectedWire;
		UINT	m_SelectedSegment;

		BOOL	m_DragFlag;
		BOOL	m_StartMove;
		BOOL	m_PSDraw;

		BYTE	m_OutputStateOld;

		BOOL	m_GridPosChange;
		BOOL	m_PinSelActive;

		BOOL	m_RefSelected;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	

		// Message Map
		AfxDeclareMessageMap();
	
		// UI Creation
		void InitUI(void);
		
		// Drawing Methods
		void DrawReferenceSyms(CDC &DC);
		
		void DrawJCT(UINT Index, CDC &DC);
		void DrawAND(UINT Index, CDC &DC);
		void DrawOR(UINT Index, CDC &DC);
		void DrawXOR(UINT Index, CDC &DC);
		void DrawNOT(UINT Index, CDC &DC);
		void DrawTIMERUP(UINT Index, CDC &DC);
		void DrawTIMERDN(UINT Index, CDC &DC);
		void DrawCOUNTUP(UINT Index, CDC &DC);
		void DrawCOUNTDN(UINT Index, CDC &DC);
		void DrawLATCH(UINT Index, CDC &DC);
		void DrawINCOIL(UINT Index, CDC &DC);
		void DrawOUTCOIL(UINT Index, CDC &DC);
				
		void DrawB_UNDO(UINT Index, CDC &DC);
		void DrawB_REDO(UINT Index, CDC &DC);
		void DrawB_REFRESH(UINT Index, CDC &DC);		

		void DrawInputBoxes(CDC &DC);
		void DrawOutputBoxes(BOOL mode,CDC &DC);
		void DrawGraphicsAreaText(CDC &DC);
		void DrawAllWires(CDC &DC);
		void DrawUserSymbols(CDC &DC);
		void DrawSymbolText(UINT type,UINT inst,UINT x,UINT y,BOOL mode,CDC &DC);
		void DrawSymbol(UINT inst, CPoint Pos, BYTE mode,CDC &DC);
		void DrawWire(UINT inst,UINT x1,UINT y1,UINT x2,UINT y2,BYTE draw,CDC &DC);
		INT  CalcVertSegPos(UINT inst,double frac);
		void DrawPinSelect(UINT x,UINT y,BOOL mode,CDC &DC);
		void DrawWireSelect(UINT inst,BOOL mode,BYTE color,CDC &DC);

		void EraseLogicScreen(CDC &DC);
		void RedrawLogicScreen(CDC &DC);
		void RedrawButtons(CDC &DC);

		BOOL CleanupJunctions(CDC &DC);

		BOOL OnEraseBkGnd(CDC &DC);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnRButtonDown(UINT uFlags, CPoint Pos);
				
		void ConnectMode(CPoint Pos,CDC &DC);
		void DragSegMode(CPoint Pos,CDC &DC);
		void WireIsValid(CDC &DC);
		UINT GetHeight(UINT inst);
		CPoint GetPinEnd(UINT inst,UINT pin);
		CPoint GetJctPinEnd(UINT inst,UINT pin);
		CPoint ForceGrid(CPoint pt);
		
		void CancelSymbolDrag(CPoint pt);
		void DeleteSymbol(UINT inst,CDC &DC);
		void DeleteWire(UINT wire,CDC &DC);
		void RepackWireList(UINT sel_wire);
		BOOL CheckInputBoxSel(CPoint pt,CDC &DC);
		
		UINT CheckButtonSel(CPoint pt,BOOL mode,CDC &DC);
		
		BOOL TestSymType(UINT inst, UINT start, UINT stop);
		BOOL TestInputZone(UINT inst,UINT pin,UINT x,UINT y);
		BOOL TestOutputZone(UINT inst,UINT x,UINT y);
		BOOL TestBodyZone(UINT inst,UINT x,UINT y);
		BOOL TestWireZoneA(UINT inst,UINT x,UINT y);
		BOOL TestWireZoneB(UINT inst,UINT x,UINT y);
		BOOL CheckRefSymbolSel(CPoint pt,BOOL mode);
		BOOL CheckUserSymbolSel(CPoint pt);
		void DebugText(char str[],UINT ypos,CDC &DC);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		BOOL LimitCursLoc(UINT inst,CPoint pt);

		void TagJunctions(UINT wire);
		
		void InfoBaloon(UINT type,UINT inst,CPoint pt,BOOL mode);
		CString GetTipText(UINT type,UINT inst,CDC &DC);
		UINT GetTipTextWidth(CString str,CDC &DC);

		void StartSimTimer(void);
		void KillSimTimer(void);

		void UpdateUndoList(BOOL mode);
		void RestoreUndoList(void);
		void HandleUndo(void);
		void HandleRedo(void);
		void HandleRefresh(CDC &DC);

		BOOL CheckCounterMapID(BYTE id);
		BOOL CheckTimerMapID(BYTE id);
		BOOL CheckInCoilMapID(BYTE id);
		BOOL CheckOutCoilMapID(BYTE id);		
		BOOL CheckNumberOfMapItems(void);

	};
 
//////////////////////////////////////////////////////////////////////////
//
// Resource Dialog
//

class CDIO14LogicDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDIO14LogicDialog(void);

	protected:	
		// Data Members
		CDlgLoader m_Loader;
		BOOL       m_fStacked;
		BOOL       m_fAutoClose;
		DWORD      m_Topic;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnClose(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);
	};

//////////////////////////////////////////////////////////////////////////
//
// Configure Symbol Dialog
//

class CDIO14CfgSym : public CDIO14LogicDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDIO14CfgSym(BYTE Data0,BYTE Data1,BYTE Data2);
		CDIO14CfgSym(PCTXT pComment, UINT ModelIndex);

		void SetType(UINT type);

		CString GetComment(void) const;
			
		CString	m_Data1;
		CString	m_Data2;

	protected:
		
		
		virtual BOOL OnCheckText(CString Text);

		// Message Map
		AfxDeclareMessageMap();
		
		void OnPaint(void);
		
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);

		UINT	m_Type;
		BYTE	m_Prop0;
		BYTE	m_Prop1;
		BYTE	m_Prop2;
	};


//////////////////////////////////////////////////////////////////////////
//
// Confirm Delete Dialog
//

class CDIO14Confirm : public CDIO14LogicDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDIO14Confirm(BYTE type);

	protected:
		
		// Message Map
		AfxDeclareMessageMap();
		
		BOOL OnCheckText(CString Text);
		void OnPaint(void);
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);

		UINT	m_Type;
	};




//////////////////////////////////////////////////////////////////////////
//
// General Information Dialog
// 

class CDIO14Info : public CDIO14LogicDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDIO14Info(BYTE message);

	protected:
		
		// Message Map
		AfxDeclareMessageMap();
		
		BOOL OnCheckText(CString Text);
		void OnPaint(void);
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnCommandOK(UINT uID);
		
		UINT	m_Type;
		BYTE	m_Message;
	};

// End of File

#endif
