
#include "intern.hpp"

#include "daro8mainwnd.hpp"

#include "daro8module.hpp"

#include "daro8outputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DARO8 Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDARO8MainWnd, CProxyViewWnd);

// Constructor

CDARO8MainWnd::CDARO8MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CDARO8MainWnd::OnAttach(void)
{
	m_pItem = (CDARO8Module *) CProxyViewWnd::m_pItem;

	AddDOPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CDARO8MainWnd::AddDOPages(void)
{
	CDARO8OutputConfig *pConfig = m_pItem->m_pOutputConfig;
	
	for( UINT n = 0; n < pConfig->GetPageCount(); n ++ ) {
	
		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);
		
		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
		}
	}

// End of File
