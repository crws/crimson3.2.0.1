
#include "intern.hpp"

#include "dadidoinputwnd.hpp"

#include "dadidoinputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOInputWnd, CUIViewWnd);

// Overibables

void CDADIDOInputWnd::OnAttach(void)
{
	m_pItem   = (CDADIDOInputConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dadio_inp"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDADIDOInputWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	EndPage(FALSE);
}

// Implementation

void CDADIDOInputWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 1);

	AddColHead(L"Mode");

	for( UINT n = 0; n < 8; n++ ) {

		AddRowHead(CPrintf(IDS_MODULE_INPUT, n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));
	}

	EndTable();
}

// End of File
