
#include "intern.hpp"

#include "DAUIN6Input.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dauin6props.h"

#include "import/manticore/dauin6dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI
//

// Runtime Class

AfxImplementRuntimeClass(CDAUIN6Input, CCommsItem);

// Property List

CCommsList const CDAUIN6Input::m_CommsList[] = {

	{ 1, "PV1",		PROPID_PV1,	    usageRead,  IDS_NAME_PV1 },
	{ 1, "PV2",		PROPID_PV2,	    usageRead,  IDS_NAME_PV2 },
	{ 1, "PV3",		PROPID_PV3,	    usageRead,  IDS_NAME_PV3 },
	{ 1, "PV4",		PROPID_PV4,	    usageRead,  IDS_NAME_PV4 },
	{ 1, "PV5",		PROPID_PV5,	    usageRead,  IDS_NAME_PV5 },
	{ 1, "PV6",		PROPID_PV6,	    usageRead,  IDS_NAME_PV6 },

	{ 1, "ColdJunc1",	PROPID_COLD_JUNC1,  usageRead,  IDS_NAME_CJ1 },
	{ 1, "ColdJunc2",	PROPID_COLD_JUNC2,  usageRead,  IDS_NAME_CJ2 },
	{ 1, "ColdJunc3",	PROPID_COLD_JUNC3,  usageRead,  IDS_NAME_CJ3 },
	{ 1, "ColdJunc4",	PROPID_COLD_JUNC4,  usageRead,  IDS_NAME_CJ4 },
	{ 1, "ColdJunc5",	PROPID_COLD_JUNC5,  usageRead,  IDS_NAME_CJ5 },
	{ 1, "ColdJunc6",	PROPID_COLD_JUNC6,  usageRead,  IDS_NAME_CJ6 },

	{ 1, "InputAlarm1",     PROPID_ALARM1,	    usageRead,  IDS_NAME_IA1 },
	{ 1, "InputAlarm2",     PROPID_ALARM2,	    usageRead,  IDS_NAME_IA2 },
	{ 1, "InputAlarm3",     PROPID_ALARM3,	    usageRead,  IDS_NAME_IA3 },
	{ 1, "InputAlarm4",     PROPID_ALARM4,      usageRead,  IDS_NAME_IA4 },
	{ 1, "InputAlarm5",     PROPID_ALARM5,	    usageRead,  IDS_NAME_IA5 },
	{ 1, "InputAlarm6",     PROPID_ALARM6,	    usageRead,  IDS_NAME_IA6 },

};

// Constructor

CDAUIN6Input::CDAUIN6Input(void)
{
	m_InputAlarm1 = 0;
	m_InputAlarm2 = 0;
	m_InputAlarm3 = 0;
	m_InputAlarm4 = 0;
	m_InputAlarm5 = 0;
	m_InputAlarm6 = 0;

	m_PV1         = 0;
	m_PV2         = 0;
	m_PV3         = 0;
	m_PV4         = 0;
	m_PV5         = 0;
	m_PV6         = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAUIN6Input::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return L"Status";
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDAUIN6Input::AddMetaData(void)
{
	Meta_AddInteger(InputAlarm1);
	Meta_AddInteger(InputAlarm2);
	Meta_AddInteger(InputAlarm3);
	Meta_AddInteger(InputAlarm4);
	Meta_AddInteger(InputAlarm5);
	Meta_AddInteger(InputAlarm6);

	Meta_AddInteger(PV1);
	Meta_AddInteger(PV2);
	Meta_AddInteger(PV3);
	Meta_AddInteger(PV4);
	Meta_AddInteger(PV5);
	Meta_AddInteger(PV6);

	CCommsItem::AddMetaData();
}


// End of File
