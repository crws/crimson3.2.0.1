
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LEGACY_HPP

#define	INCLUDE_LEGACY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Files
//

#include "import\props.h"

#include "import\dlcprops.h"

#include "import\8inprops.h"

#include "import\8inlprops.h"

#include "import\dio01props.h"

#include "import\rtd6props.h"

#include "import\sgprops.h"

#include "import\out4props.h"

#include "import\comms.h"

// End of File

#endif
