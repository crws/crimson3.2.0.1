
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SGLOOP_HPP

#define INCLUDE_SGLOOP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\sgdbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSGLoop;
class CSGLoopGeneralWnd;
class CSGLoopControlWnd;
class CSGLoopPowerWnd;
class CSGLoopAlarmWnd;
class CUISGProcess;
class CUISGPower;
class CSGPowerWnd;

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Item
//

class CSGLoop : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSGLoop(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Data Members
		CString	m_ProcUnits;
		INT	m_DispLo1;
		INT	m_DispHi1;
		INT	m_SigLoKey1;
		INT	m_SigHiKey1;
		INT	m_DispLo2;
		INT	m_DispHi2;
		INT	m_SigLoKey2;
		INT	m_SigHiKey2;
		UINT	m_ProcDP;
		UINT	m_Mode;
		UINT	m_DigHeat;
		UINT	m_DigCool;
		UINT	m_InputRange1;
		UINT	m_InputRange2;
		UINT	m_ExcitOut1;
		UINT	m_ExcitOut2;
		UINT	m_PVAssign;
		INT	m_PVLimitLo;
		INT	m_PVLimitHi;
		UINT	m_AlarmAssign1;
		UINT	m_AlarmAssign2;
		UINT	m_AlarmAssign3;
		UINT	m_AlarmAssign4;
		UINT	m_AlarmMode1;
		UINT	m_AlarmMode2;
		UINT	m_AlarmMode3;
		UINT	m_AlarmMode4;
		UINT	m_AlarmDelay1;
		UINT	m_AlarmDelay2;
		UINT	m_AlarmDelay3;
		UINT	m_AlarmDelay4;
		UINT	m_AlarmLatch1;
		UINT	m_AlarmLatch2;
		UINT	m_AlarmLatch3;
		UINT	m_AlarmLatch4;
		UINT	m_RangeLatch;
		UINT	m_TuneCode;
		UINT	m_InitData;
		UINT	m_DnLoadScaling;
		UINT	m_ReqManual;
		UINT	m_ReqTune;
		UINT	m_ReqUserPID;
		UINT	m_SelectScaling1;
		UINT	m_SelectScaling2;
		UINT	m_AlarmAccept1;
		UINT	m_AlarmAccept2;
		UINT	m_AlarmAccept3;
		UINT	m_AlarmAccept4;
		UINT	m_InputAccept;
		UINT	m_Power;
		UINT	m_ReqSP;
		UINT	m_SetHyst;
		INT	m_SetDead;
		UINT	m_SetRampBase;
		UINT	m_SetRamp;
		UINT	m_InputFilter;
		UINT	m_UserConstP;
		UINT	m_UserConstI;
		UINT	m_UserConstD;
		UINT	m_UserCLimit;
		UINT	m_UserHLimit;
		UINT	m_UserFilter;
		INT	m_PowerFault;
		UINT	m_FaultAssign;
		INT	m_PowerOffset;
		INT	m_PowerDead;
		UINT	m_PowerRevGain;
		UINT	m_PowerDirGain;
		UINT	m_PowerRevHyst;
		UINT	m_PowerDirHyst;
		UINT	m_RevLimitLo;
		UINT	m_RevLimitHi;
		UINT	m_DirLimitLo;
		UINT	m_DirLimitHi;
		INT	m_AlarmData1;
		INT	m_AlarmData2;
		INT	m_AlarmData3;
		INT	m_AlarmData4;
		INT	m_AlarmHyst1;
		INT	m_AlarmHyst2;
		INT	m_AlarmHyst3;
		INT	m_AlarmHyst4;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Data Scaling
		DWORD GetIntProp(PCTXT pTag);
		BOOL  IsTenTimes(CString Tag);

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop General View
//

class CSGLoopGeneralWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CSGLoop * m_pItem;

		// Overidables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	

		// UI Creation
		void AddOperation(void);
		void AddInputs(void);
		void AddSmart(void);
		void AddInitial(void);

		// Enabling
		void DoEnables(void);
		void EnableHeat(void);
		void EnableCool(void);
		void EnableInit(void);
		void EnableScaling(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Power View
//

class CSGLoopPowerWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CSGLoop    * m_pItem;
		CUISGPower * m_pPower;

		// Overidables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);

		// UI Creation
		void AddPower(void);
		void AddGraph(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Control View
//

class CSGLoopControlWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CSGLoop * m_pItem;

		// Overidables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddGeneral(void);
		void AddAuto(void);
		void AddPID(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Loop Alarm View
//

class CSGLoopAlarmWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CSGLoop * m_pItem;

		// Overidables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddAlarms(void);
		void AddAlarm(UINT n);
		void AddInput(void);

		// Enabling
		void DoEnables(void);
		void EnableAlarm(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphic UI Element -- Strain Gage Process Value
//

class CUISGProcess : public CUIEditBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUISGProcess(void);

		// Destructor
		~CUISGProcess(void);

		// Update Support
		static void CheckUpdate(CSGLoop *pLoop, CString const &Tag);

		// Operations
		void Update(BOOL fKeep);
		void UpdateUnits(void);

	protected:
		// Linked List
		static CUISGProcess * m_pHead;
		static CUISGProcess * m_pTail;

		// Data Members
		CUISGProcess  * m_pNext;
		CUISGProcess  * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Strain Gage Process Value
//

class CUITextSGProcess : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextSGProcess(void);

		// Destructor
		~CUITextSGProcess(void);

	protected:
		// Data Members
		CSGLoop * m_pLoop;
		char	  m_cType;
		double	  m_t1;
		double	  m_t2;

		// Core Overidables
		void OnBind(void);

		// Implementation
		void GetConfig(void);
		BYTE GetInputAssignment(void);
		void CheckType(void);
		void CheckFlags(void);

		// Scaling
		INT  StoreToDisp(INT nData);
		INT  DispToStore(INT nData);

		// Implementation
		INT     Scale(double a, double b, double c, double d);
		void    GetConstants(double &a, double &b, double &c);
		BOOL	IsOutputAbsolute(void);
		BOOL    IsOutputPercent(void);
		CString GetRateUnits(void);

		// Friend
		friend class CUISGProcess;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- PID Power Curve
//

class CUISGPower : public CUIControl
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUISGPower(CSGLoop *pLoop);

		// Destructor
		~CUISGPower(void);

		// Operations
		void Update(void);
		void Exclude(CWnd &Wnd, CDC &DC);

	protected:
		// Data Members
		CSGLoop     * m_pLoop;
		CLayFormPad * m_pLayout;
		CSGPowerWnd * m_pCtrl;

		// Core Overidables
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnMove(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PID Power Curve Window
//

class CSGPowerWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSGPowerWnd(CSGLoop *pLoop);

		// Destructor
		~CSGPowerWnd(void);

		// Operations
		void Update(void);
		void Exclude(CWnd &Wnd, CDC &DC);

	protected:
		// Data Members
		CSGLoop * m_pLoop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);

		// Implementation
		void Draw(CDC &DC, CRect Rect);
	};

// End of File

#endif
