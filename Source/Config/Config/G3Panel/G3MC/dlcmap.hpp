
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DLCMAP_HPP

#define INCLUDE_DLCMAP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDLCMapper;
class CDLCMapMainWnd;
class CDLCMapLEDsWnd;
class CUIDLCMap;

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Mapper Item
//

class CDLCMapper : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDLCMapper(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Data Members
		UINT	m_LinOutType;
		INT	m_LinOutMin;
		INT	m_LinOutMax;
		UINT	m_LinOutFilter;
		UINT	m_LinOutDead;
		UINT	m_LinOutUpdate;
		UINT	m_LinOutMap;
		UINT	m_DigOutMap1;
		UINT	m_DigOutMap2;
		UINT	m_DigOutMap3;
		UINT	m_DigOutMap4;
		UINT	m_LedOutMap1;
		UINT	m_LedOutMap2;
		UINT	m_LedOutMap3;
		UINT	m_LedOutMap4;
		UINT	m_LedOutMap5;
		UINT	m_LedOutMap6;
		UINT	m_CycleTime1;
		UINT	m_CycleTime2;
		UINT	m_CycleTime3;
		UINT	m_CycleTime4;
		UINT	m_DigRemote1;
		UINT	m_DigRemote2;
		UINT	m_DigRemote3;
		UINT	m_DigRemote4;
		UINT	m_OP1State;
		UINT	m_OP2State;
		UINT	m_OP3State;
		UINT	m_OP4State;

	protected:
		// Static Data
		static const CCommsList m_CommsList[];

		// Data Scaling
		DWORD GetIntProp(PCTXT pTag);
		BOOL  IsTenTimes(CString Tag);

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Mapper Main View
//

class CDLCMapMainWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CDLCMapper * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// UI Creation
		void AddDigital(void);

		// Enabling
		void DoEnables(void);
		void EnableLinear(void);
		void EnableOutput(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Mapper LED View
//

class CDLCMapLEDsWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CDLCMapper * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);	

		// UI Creation
		void AddLEDs(void);

		// Enabling
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- DLC Mapping
//

class CUITextDLCMap : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextDLCMap(void);

	protected:
		// Core Overidables
		void OnBind(void);

		// Implementation
		void AddCoreAnalog(void);
		void AddMiscAnalog(void);
		void AddDigital(void);
		void AddOutputs(void);
	};

// End of File

#endif
