
#include "intern.hpp"

#include "legacy.h"

#include "iviinp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

// Runtime Class

AfxImplementRuntimeClass(CIVIInput, CCommsItem);

// Property List

CCommsList const CIVIInput::m_CommsListStd[] = {

	{ 1, "PV1",		PROPID_PV1,			usageRead,	IDS_NAME_PV1	},
	{ 1, "PV2",		PROPID_PV2,			usageRead,	IDS_NAME_PV2	},
	{ 1, "PV3",		PROPID_PV3,			usageRead,	IDS_NAME_PV3	},
	{ 1, "PV4",		PROPID_PV4,			usageRead,	IDS_NAME_PV4	},
	{ 1, "PV5",		PROPID_PV5,			usageRead,	IDS_NAME_PV5	},
	{ 1, "PV6",		PROPID_PV6,			usageRead,	IDS_NAME_PV6	},
	{ 1, "PV7",		PROPID_PV7,			usageRead,	IDS_NAME_PV7	},
	{ 1, "PV8",		PROPID_PV8,			usageRead,	IDS_NAME_PV8	},

	{ 1, "InputAlarm1",	PROPID_INPUT_ALARM1,		usageRead,	IDS_NAME_IA1	},
	{ 1, "InputAlarm2",	PROPID_INPUT_ALARM2,		usageRead,	IDS_NAME_IA2	},
	{ 1, "InputAlarm3",	PROPID_INPUT_ALARM3,		usageRead,	IDS_NAME_IA3	},
	{ 1, "InputAlarm4",	PROPID_INPUT_ALARM4,		usageRead,	IDS_NAME_IA4	},
	{ 1, "InputAlarm5",	PROPID_INPUT_ALARM5,		usageRead,	IDS_NAME_IA5	},
	{ 1, "InputAlarm6",	PROPID_INPUT_ALARM6,		usageRead,	IDS_NAME_IA6	},
	{ 1, "InputAlarm7",	PROPID_INPUT_ALARM7,		usageRead,	IDS_NAME_IA7	},
	{ 1, "InputAlarm8",	PROPID_INPUT_ALARM8,		usageRead,	IDS_NAME_IA8	},

	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},

	{ 2, "ProcessMin1",	PROPID_PROC_MIN1,		usageWriteBoth,	IDS_NAME_PMI1	},
	{ 2, "ProcessMin2",	PROPID_PROC_MIN2,		usageWriteBoth,	IDS_NAME_PMI2	},
	{ 2, "ProcessMin3",	PROPID_PROC_MIN3,		usageWriteBoth,	IDS_NAME_PMI3	},
	{ 2, "ProcessMin4",	PROPID_PROC_MIN4,		usageWriteBoth,	IDS_NAME_PMI4	},
	{ 2, "ProcessMin5",	PROPID_PROC_MIN5,		usageWriteBoth,	IDS_NAME_PMI5	},
	{ 2, "ProcessMin6",	PROPID_PROC_MIN6,		usageWriteBoth,	IDS_NAME_PMI6	},
	{ 2, "ProcessMin7",	PROPID_PROC_MIN7,		usageWriteBoth,	IDS_NAME_PMI7	},
	{ 2, "ProcessMin8",	PROPID_PROC_MIN8,		usageWriteBoth,	IDS_NAME_PMI8	},

	{ 2, "ProcessMax1",	PROPID_PROC_MAX1,		usageWriteBoth,	IDS_NAME_PMA1	},
	{ 2, "ProcessMax2",	PROPID_PROC_MAX2,		usageWriteBoth,	IDS_NAME_PMA2	},
	{ 2, "ProcessMax3",	PROPID_PROC_MAX3,		usageWriteBoth,	IDS_NAME_PMA3	},
	{ 2, "ProcessMax4",	PROPID_PROC_MAX4,		usageWriteBoth,	IDS_NAME_PMA4	},
	{ 2, "ProcessMax5",	PROPID_PROC_MAX5,		usageWriteBoth,	IDS_NAME_PMA5	},
	{ 2, "ProcessMax6",	PROPID_PROC_MAX6,		usageWriteBoth,	IDS_NAME_PMA6	},
	{ 2, "ProcessMax7",	PROPID_PROC_MAX7,		usageWriteBoth,	IDS_NAME_PMA7	},
	{ 2, "ProcessMax8",	PROPID_PROC_MAX8,		usageWriteBoth,	IDS_NAME_PMA8	},

	{ 0, "PIRange",		PROPID_PI_RANGE,		usageWriteInit,	IDS_NAME_PIR	},
	{ 0, "PVRange",		PROPID_PV_RANGE,		usageWriteInit,	IDS_NAME_PVR	},

	{ 0, "ChanEnable1",	PROPID_CHAN_ENABLE1,		usageWriteInit,	IDS_NAME_CE1	},
	{ 0, "ChanEnable2",	PROPID_CHAN_ENABLE2,		usageWriteInit,	IDS_NAME_CE2	},
	{ 0, "ChanEnable3",	PROPID_CHAN_ENABLE3,		usageWriteInit,	IDS_NAME_CE3	},
	{ 0, "ChanEnable4",	PROPID_CHAN_ENABLE4,		usageWriteInit,	IDS_NAME_CE4	},
	{ 0, "ChanEnable5",	PROPID_CHAN_ENABLE5,		usageWriteInit,	IDS_NAME_CE5	},
	{ 0, "ChanEnable6",	PROPID_CHAN_ENABLE6,		usageWriteInit,	IDS_NAME_CE6	},
	{ 0, "ChanEnable7",	PROPID_CHAN_ENABLE7,		usageWriteInit,	IDS_NAME_CE7	},
	{ 0, "ChanEnable8",	PROPID_CHAN_ENABLE8,		usageWriteInit,	IDS_NAME_CE8	},

	{ 0, "SquareRt1",	PROPID_SQUARE_RT1,		usageWriteInit,	IDS_NAME_SQRT1	},
	{ 0, "SquareRt2",	PROPID_SQUARE_RT2,		usageWriteInit,	IDS_NAME_SQRT2	},
	{ 0, "SquareRt3",	PROPID_SQUARE_RT3,		usageWriteInit,	IDS_NAME_SQRT3	},
	{ 0, "SquareRt4",	PROPID_SQUARE_RT4,		usageWriteInit,	IDS_NAME_SQRT4	},
	{ 0, "SquareRt5",	PROPID_SQUARE_RT5,		usageWriteInit,	IDS_NAME_SQRT5	},
	{ 0, "SquareRt6",	PROPID_SQUARE_RT6,		usageWriteInit,	IDS_NAME_SQRT6	},
	{ 0, "SquareRt7",	PROPID_SQUARE_RT7,		usageWriteInit,	IDS_NAME_SQRT7	},
	{ 0, "SquareRt8",	PROPID_SQUARE_RT8,		usageWriteInit,	IDS_NAME_SQRT8	},

	{ 0, "ExtendPV1",	PROPID_EXTEND_PV1,		usageWriteInit,	IDS_NAME_EXPV1	},
	{ 0, "ExtendPV2",	PROPID_EXTEND_PV2,		usageWriteInit,	IDS_NAME_EXPV2	},
	{ 0, "ExtendPV3",	PROPID_EXTEND_PV3,		usageWriteInit,	IDS_NAME_EXPV3	},
	{ 0, "ExtendPV4",	PROPID_EXTEND_PV4,		usageWriteInit,	IDS_NAME_EXPV4	},
	{ 0, "ExtendPV5",	PROPID_EXTEND_PV5,		usageWriteInit,	IDS_NAME_EXPV5	},
	{ 0, "ExtendPV6",	PROPID_EXTEND_PV6,		usageWriteInit,	IDS_NAME_EXPV6	},
	{ 0, "ExtendPV7",	PROPID_EXTEND_PV7,		usageWriteInit,	IDS_NAME_EXPV7	},
	{ 0, "ExtendPV8",	PROPID_EXTEND_PV8,		usageWriteInit,	IDS_NAME_EXPV8	},
	};

CCommsList const CIVIInput::m_CommsListLin[] = {

	{ 1, "PV1",		PROPID_PV1,			usageRead,	IDS_NAME_PV1	},
	{ 1, "PV2",		PROPID_PV2,			usageRead,	IDS_NAME_PV2	},
	{ 1, "PV3",		PROPID_PV3,			usageRead,	IDS_NAME_PV3	},
	{ 1, "PV4",		PROPID_PV4,			usageRead,	IDS_NAME_PV4	},
	{ 1, "PV5",		PROPID_PV5,			usageRead,	IDS_NAME_PV5	},
	{ 1, "PV6",		PROPID_PV6,			usageRead,	IDS_NAME_PV6	},
	{ 1, "PV7",		PROPID_PV7,			usageRead,	IDS_NAME_PV7	},
	{ 1, "PV8",		PROPID_PV8,			usageRead,	IDS_NAME_PV8	},

	{ 1, "InputAlarm1",	PROPID_INPUT_ALARM1,		usageRead,	IDS_NAME_IA1	},
	{ 1, "InputAlarm2",	PROPID_INPUT_ALARM2,		usageRead,	IDS_NAME_IA2	},
	{ 1, "InputAlarm3",	PROPID_INPUT_ALARM3,		usageRead,	IDS_NAME_IA3	},
	{ 1, "InputAlarm4",	PROPID_INPUT_ALARM4,		usageRead,	IDS_NAME_IA4	},
	{ 1, "InputAlarm5",	PROPID_INPUT_ALARM5,		usageRead,	IDS_NAME_IA5	},
	{ 1, "InputAlarm6",	PROPID_INPUT_ALARM6,		usageRead,	IDS_NAME_IA6	},
	{ 1, "InputAlarm7",	PROPID_INPUT_ALARM7,		usageRead,	IDS_NAME_IA7	},
	{ 1, "InputAlarm8",	PROPID_INPUT_ALARM8,		usageRead,	IDS_NAME_IA8	},

	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},

	{ 2, "ProcessMin1",	PROPID_PROC_MIN1,		usageWriteBoth,	IDS_NAME_PMI1	},
	{ 2, "ProcessMin2",	PROPID_PROC_MIN2,		usageWriteBoth,	IDS_NAME_PMI2	},
	{ 2, "ProcessMin3",	PROPID_PROC_MIN3,		usageWriteBoth,	IDS_NAME_PMI3	},
	{ 2, "ProcessMin4",	PROPID_PROC_MIN4,		usageWriteBoth,	IDS_NAME_PMI4	},
	{ 2, "ProcessMin5",	PROPID_PROC_MIN5,		usageWriteBoth,	IDS_NAME_PMI5	},
	{ 2, "ProcessMin6",	PROPID_PROC_MIN6,		usageWriteBoth,	IDS_NAME_PMI6	},
	{ 2, "ProcessMin7",	PROPID_PROC_MIN7,		usageWriteBoth,	IDS_NAME_PMI7	},
	{ 2, "ProcessMin8",	PROPID_PROC_MIN8,		usageWriteBoth,	IDS_NAME_PMI8	},

	{ 2, "ProcessMax1",	PROPID_PROC_MAX1,		usageWriteBoth,	IDS_NAME_PMA1	},
	{ 2, "ProcessMax2",	PROPID_PROC_MAX2,		usageWriteBoth,	IDS_NAME_PMA2	},
	{ 2, "ProcessMax3",	PROPID_PROC_MAX3,		usageWriteBoth,	IDS_NAME_PMA3	},
	{ 2, "ProcessMax4",	PROPID_PROC_MAX4,		usageWriteBoth,	IDS_NAME_PMA4	},
	{ 2, "ProcessMax5",	PROPID_PROC_MAX5,		usageWriteBoth,	IDS_NAME_PMA5	},
	{ 2, "ProcessMax6",	PROPID_PROC_MAX6,		usageWriteBoth,	IDS_NAME_PMA6	},
	{ 2, "ProcessMax7",	PROPID_PROC_MAX7,		usageWriteBoth,	IDS_NAME_PMA7	},
	{ 2, "ProcessMax8",	PROPID_PROC_MAX8,		usageWriteBoth,	IDS_NAME_PMA8	},

	{ 2, "LinSlope[0]",	PROPID_INPUT_SLOPE1,		usageWriteBoth,	IDS_LINEAR_SLOPE1 },
	{ 2, "LinSlope[1]",	PROPID_INPUT_SLOPE2,		usageWriteBoth,	IDS_LINEAR_SLOPE2 },
	{ 2, "LinSlope[2]",	PROPID_INPUT_SLOPE3,		usageWriteBoth,	IDS_LINEAR_SLOPE3 },
	{ 2, "LinSlope[3]",	PROPID_INPUT_SLOPE4,		usageWriteBoth,	IDS_LINEAR_SLOPE4 },
	{ 2, "LinSlope[4]",	PROPID_INPUT_SLOPE5,		usageWriteBoth,	IDS_LINEAR_SLOPE5 },
	{ 2, "LinSlope[5]",	PROPID_INPUT_SLOPE6,		usageWriteBoth,	IDS_LINEAR_SLOPE6 },
	{ 2, "LinSlope[6]",	PROPID_INPUT_SLOPE7,		usageWriteBoth,	IDS_LINEAR_SLOPE7 },
	{ 2, "LinSlope[7]",	PROPID_INPUT_SLOPE8,		usageWriteBoth,	IDS_LINEAR_SLOPE8 },

	{ 2, "LinOffset[0]",	PROPID_INPUT_OFFSET1,		usageWriteBoth,	IDS_LINEAR_OFFSET1 },
	{ 2, "LinOffset[1]",	PROPID_INPUT_OFFSET2,		usageWriteBoth,	IDS_LINEAR_OFFSET2 },
	{ 2, "LinOffset[2]",	PROPID_INPUT_OFFSET3,		usageWriteBoth,	IDS_LINEAR_OFFSET3 },
	{ 2, "LinOffset[3]",	PROPID_INPUT_OFFSET4,		usageWriteBoth,	IDS_LINEAR_OFFSET4 },
	{ 2, "LinOffset[4]",	PROPID_INPUT_OFFSET5,		usageWriteBoth,	IDS_LINEAR_OFFSET5 },
	{ 2, "LinOffset[5]",	PROPID_INPUT_OFFSET6,		usageWriteBoth,	IDS_LINEAR_OFFSET6 },
	{ 2, "LinOffset[6]",	PROPID_INPUT_OFFSET7,		usageWriteBoth,	IDS_LINEAR_OFFSET7 },
	{ 2, "LinOffset[7]",	PROPID_INPUT_OFFSET8,		usageWriteBoth,	IDS_LINEAR_OFFSET8 },

	{ 0, "PIRange",		PROPID_PI_RANGE,		usageWriteInit,	IDS_NAME_PIR	},
	{ 0, "PVRange",		PROPID_PV_RANGE,		usageWriteInit,	IDS_NAME_PVR	},

	{ 0, "ChanEnable1",	PROPID_CHAN_ENABLE1,		usageWriteInit,	IDS_NAME_CE1	},
	{ 0, "ChanEnable2",	PROPID_CHAN_ENABLE2,		usageWriteInit,	IDS_NAME_CE2	},
	{ 0, "ChanEnable3",	PROPID_CHAN_ENABLE3,		usageWriteInit,	IDS_NAME_CE3	},
	{ 0, "ChanEnable4",	PROPID_CHAN_ENABLE4,		usageWriteInit,	IDS_NAME_CE4	},
	{ 0, "ChanEnable5",	PROPID_CHAN_ENABLE5,		usageWriteInit,	IDS_NAME_CE5	},
	{ 0, "ChanEnable6",	PROPID_CHAN_ENABLE6,		usageWriteInit,	IDS_NAME_CE6	},
	{ 0, "ChanEnable7",	PROPID_CHAN_ENABLE7,		usageWriteInit,	IDS_NAME_CE7	},
	{ 0, "ChanEnable8",	PROPID_CHAN_ENABLE8,		usageWriteInit,	IDS_NAME_CE8	},

	{ 0, "SquareRt1",	PROPID_SQUARE_RT1,		usageWriteInit,	IDS_NAME_SQRT1	},
	{ 0, "SquareRt2",	PROPID_SQUARE_RT2,		usageWriteInit,	IDS_NAME_SQRT2	},
	{ 0, "SquareRt3",	PROPID_SQUARE_RT3,		usageWriteInit,	IDS_NAME_SQRT3	},
	{ 0, "SquareRt4",	PROPID_SQUARE_RT4,		usageWriteInit,	IDS_NAME_SQRT4	},
	{ 0, "SquareRt5",	PROPID_SQUARE_RT5,		usageWriteInit,	IDS_NAME_SQRT5	},
	{ 0, "SquareRt6",	PROPID_SQUARE_RT6,		usageWriteInit,	IDS_NAME_SQRT6	},
	{ 0, "SquareRt7",	PROPID_SQUARE_RT7,		usageWriteInit,	IDS_NAME_SQRT7	},
	{ 0, "SquareRt8",	PROPID_SQUARE_RT8,		usageWriteInit,	IDS_NAME_SQRT8	},
	};

// Constructor

CIVIInput::CIVIInput(BYTE IVIModel)
{
	m_IVIModel      = IVIModel;

	m_PIRange	= 2;
	m_PVRange	= 1;

	m_ProcessDP1	= 2;
	m_ProcessDP2	= 2;
	m_ProcessDP3	= 2;
	m_ProcessDP4	= 2;
	m_ProcessDP5	= 2;
	m_ProcessDP6	= 2;
	m_ProcessDP7	= 2;
	m_ProcessDP8	= 2;

	m_InputFilter	= 20;

	m_ProcessMin1	= 0;
	m_ProcessMin2	= 0;
	m_ProcessMin3	= 0;
	m_ProcessMin4	= 0;
	m_ProcessMin5	= 0;
	m_ProcessMin6	= 0;
	m_ProcessMin7	= 0;
	m_ProcessMin8	= 0;

	m_ProcessMax1	= 10000;
	m_ProcessMax2	= 10000;
	m_ProcessMax3	= 10000;
	m_ProcessMax4	= 10000;
	m_ProcessMax5	= 10000;
	m_ProcessMax6	= 10000;
	m_ProcessMax7	= 10000;
	m_ProcessMax8	= 10000;

	m_SquareRt1	= FALSE;
	m_SquareRt2	= FALSE;
	m_SquareRt3	= FALSE;
	m_SquareRt4	= FALSE;
	m_SquareRt5	= FALSE;
	m_SquareRt6	= FALSE;
	m_SquareRt7	= FALSE;
	m_SquareRt8	= FALSE;

	m_ChanEnable1	= TRUE;
	m_ChanEnable2	= TRUE;
	m_ChanEnable3	= TRUE;
	m_ChanEnable4	= TRUE;
	m_ChanEnable5	= TRUE;
	m_ChanEnable6	= TRUE;
	m_ChanEnable7	= TRUE;
	m_ChanEnable8	= TRUE;

	m_ExtendPV1	= FALSE;
	m_ExtendPV2	= FALSE;
	m_ExtendPV3	= FALSE;
	m_ExtendPV4	= FALSE;
	m_ExtendPV5	= FALSE;
	m_ExtendPV6	= FALSE;
	m_ExtendPV7	= FALSE;
	m_ExtendPV8	= FALSE;

	if( m_IVIModel == ID_CSINI8L || m_IVIModel == ID_CSINV8L ) {

		m_LinChannel	= 1;

		m_LinDP		= 2;

		m_LinSlopeUI	= 1000;

		m_LinOffsetUI	= 0;

		for( UINT c = 0; c < HDW_CHANNELS; c++ ) {

			m_Linearize  [c] = FALSE;

			m_LinSlope   [c] = 1000;

			m_LinOffset  [c] = 0;

			m_LinSegments[c] = 20;

			UINT p;

			for( p = 0; p <= m_LinSegments[c]; p++ ) {

				if( m_IVIModel == ID_CSINV8L ) {

					m_LinPV   [c][p] = 500 * p;

					m_LinInput[c][p] = 500 * p;
					}
				else {
					m_LinPV   [c][p] = 500 * p;

					m_LinInput[c][p] = 800 * (p + 5);
					}
				}

			for( p = m_LinSegments[c] + 1; p < LIN_POINTS; p++ ) {

				m_LinInput[c][p] = 0;

				m_LinPV   [c][p] = 0;
				}
			}

		m_uCommsCount = elements(m_CommsListLin);
		
		m_pCommsData  = m_CommsListLin;
		}
	else {
		for( UINT c = 0; c < HDW_CHANNELS; c++ ) {

			m_Linearize[c] = FALSE;
			}

		m_uCommsCount = elements(m_CommsListStd);
		
		m_pCommsData  = m_CommsListStd;
		}

	CheckCommsData();
	}

// Group Names

CString CIVIInput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_STATUS);

		case 2:	return CString(IDS_MODULE_CTRL);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CIVIInput::GetPageCount(void)
{
	if( m_IVIModel == ID_CSINI8L || m_IVIModel == ID_CSINV8L ) {

		return 2;
		}

	return 1;
	}

CString CIVIInput::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_CONFIG);

		case 1: return CString(IDS_LINEARIZATION);
		}

	return L"";
	}

CViewWnd * CIVIInput::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CIVIConfigWnd;

		case 1: return New CIVILinearWnd;
		}

	return NULL;
	}

// Data Support

INT CIVIInput::GetScaledPV(UINT Channel, UINT SegPoint)
{
	LONG PV = LONG(m_LinPV[Channel][SegPoint] * m_LinSlope[Channel]);

	PV /= 1000;

	PV += LONG(m_LinOffset[Channel]);

	if( PV < -30000 ) {
		
		PV = -30000;
		}

	if( PV > +30000 ) {
		
		PV = +30000;
		}

	return INT(PV);
	}

BOOL CIVIInput::GetLinEnable(void)
{
	if( m_LinChannel >= 1 && m_LinChannel <= HDW_CHANNELS ) {

		return m_Linearize[m_LinChannel-1];
		}

	return FALSE;
	}

UINT CIVIInput::GetLinDP(void)
{
	switch( m_LinChannel ) {

		case 1:	return m_ProcessDP1;
		case 2:	return m_ProcessDP2;
		case 3:	return m_ProcessDP3;
		case 4:	return m_ProcessDP4;
		case 5:	return m_ProcessDP5;
		case 6:	return m_ProcessDP6;
		case 7:	return m_ProcessDP7;
		case 8:	return m_ProcessDP8;
		}

	return 0;
	}

void CIVIInput::SetLinDP(void)
{
	switch( m_LinChannel ) {

		case 1:	m_ProcessDP1 = m_LinDP; break;
		case 2:	m_ProcessDP2 = m_LinDP; break;
		case 3:	m_ProcessDP3 = m_LinDP; break;
		case 4:	m_ProcessDP4 = m_LinDP; break;
		case 5:	m_ProcessDP5 = m_LinDP; break;
		case 6:	m_ProcessDP6 = m_LinDP; break;
		case 7:	m_ProcessDP7 = m_LinDP; break;
		case 8:	m_ProcessDP8 = m_LinDP; break;
		}
	}

INT CIVIInput::GetLinSlope(void)
{
	if( m_LinChannel >= 1 && m_LinChannel <= HDW_CHANNELS ) {

		return m_LinSlope[m_LinChannel-1];
		}

	return 1;
	}

void CIVIInput::SetLinSlope(void)
{
	if( m_LinChannel >= 1 && m_LinChannel <= HDW_CHANNELS ) {

		m_LinSlope[m_LinChannel-1] = m_LinSlopeUI;
		}
	}

INT CIVIInput::GetLinOffset(void)
{
	if( m_LinChannel >= 1 && m_LinChannel <= HDW_CHANNELS ) {

		return m_LinOffset[m_LinChannel-1];
		}

	return 0;
	}

void CIVIInput::SetLinOffset(void)
{
	if( m_LinChannel >= 1 && m_LinChannel <= HDW_CHANNELS ) {

		m_LinOffset[m_LinChannel-1] = m_LinOffsetUI;
		}
	}

// Conversion

BOOL CIVIInput::Convert(CPropValue *pValue)
{
	if( pValue ) {

		ImportNumber(pValue, L"PIRange");

		ImportNumber(pValue, L"PVRange");
		
		ImportNumber(pValue, L"InputFilter");

		for( UINT n = 0; n < 8; n ++ ) {
			
			ImportNumber(pValue, CPrintf(L"ProcessDP%d",  n+1), CPrintf(L"ProcDP%d",     n+1));
			
			ImportNumber(pValue, CPrintf(L"ProcessMin%d", n+1), CPrintf(L"ProcMin%d",    n+1));
			
			ImportNumber(pValue, CPrintf(L"ProcessMax%d", n+1), CPrintf(L"ProcMax%d",    n+1));
			
			ImportNumber(pValue, CPrintf(L"SquareRt%d",   n+1), CPrintf(L"SquareRt%d",   n+1));
			
			ImportNumber(pValue, CPrintf(L"ChanEnable%d", n+1), CPrintf(L"ChanEnable%d", n+1));
			}		
		
		return TRUE;
		}

	return CCommsItem::Convert(pValue);
	}

// Property Save Filter

BOOL CIVIInput::SaveProp(CString Tag) const
{
	if( Tag.Left(3) == "Lin" ) {

		if( m_IVIModel == ID_CSINI8L || m_IVIModel == ID_CSINV8L ) {

			if( Tag.Left(5) == "LinPV" || Tag.Left(8) == "LinInput" ) {

				UINT p1 = Tag.Find('[');

				UINT p2 = Tag.Find('[', p1+1);

				UINT c  = watoi(PCTXT(Tag)+p1+1);

				UINT p  = watoi(PCTXT(Tag)+p2+1);

				if( m_Linearize[c] ) {

					if( p <= m_LinSegments[c] ) {

						return TRUE;
						}
					}

				return FALSE;
				}

			return TRUE;
			}

		return FALSE;
		}

	return CCommsItem::SaveProp(Tag);
	}

// Implementation

void CIVIInput::AddMetaData(void)
{
	Meta_AddInteger(PIRange);
	Meta_AddInteger(PVRange);

	Meta_AddInteger(ProcessDP1);
	Meta_AddInteger(ProcessDP2);
	Meta_AddInteger(ProcessDP3);
	Meta_AddInteger(ProcessDP4);
	Meta_AddInteger(ProcessDP5);
	Meta_AddInteger(ProcessDP6);
	Meta_AddInteger(ProcessDP7);
	Meta_AddInteger(ProcessDP8);

	Meta_AddInteger(InputFilter);

	Meta_AddInteger(ProcessMin1);
	Meta_AddInteger(ProcessMin2);
	Meta_AddInteger(ProcessMin3);
	Meta_AddInteger(ProcessMin4);
	Meta_AddInteger(ProcessMin5);
	Meta_AddInteger(ProcessMin6);
	Meta_AddInteger(ProcessMin7);
	Meta_AddInteger(ProcessMin8);

	Meta_AddInteger(ProcessMax1);
	Meta_AddInteger(ProcessMax2);
	Meta_AddInteger(ProcessMax3);
	Meta_AddInteger(ProcessMax4);
	Meta_AddInteger(ProcessMax5);
	Meta_AddInteger(ProcessMax6);
	Meta_AddInteger(ProcessMax7);
	Meta_AddInteger(ProcessMax8);

	Meta_AddInteger(SquareRt1);
	Meta_AddInteger(SquareRt2);
	Meta_AddInteger(SquareRt3);
	Meta_AddInteger(SquareRt4);
	Meta_AddInteger(SquareRt5);
	Meta_AddInteger(SquareRt6);
	Meta_AddInteger(SquareRt7);
	Meta_AddInteger(SquareRt8);

	Meta_AddInteger(ChanEnable1);
	Meta_AddInteger(ChanEnable2);
	Meta_AddInteger(ChanEnable3);
	Meta_AddInteger(ChanEnable4);
	Meta_AddInteger(ChanEnable5);
	Meta_AddInteger(ChanEnable6);
	Meta_AddInteger(ChanEnable7);
	Meta_AddInteger(ChanEnable8);

	Meta_AddInteger(ExtendPV1);
	Meta_AddInteger(ExtendPV2);
	Meta_AddInteger(ExtendPV3);
	Meta_AddInteger(ExtendPV4);
	Meta_AddInteger(ExtendPV5);
	Meta_AddInteger(ExtendPV6);
	Meta_AddInteger(ExtendPV7);
	Meta_AddInteger(ExtendPV8);

	for( UINT p, c = 0; c < HDW_CHANNELS; c++  ) {

		Meta_Add(CPrintf(  "Linearize[%u]", c), m_Linearize  [c], metaInteger);
		Meta_Add(CPrintf(   "LinSlope[%u]", c), m_LinSlope   [c], metaInteger);
		Meta_Add(CPrintf(  "LinOffset[%u]", c), m_LinOffset  [c], metaInteger);
		Meta_Add(CPrintf("LinSegments[%u]", c), m_LinSegments[c], metaInteger);

		for( p = 0; p < LIN_POINTS; p++  ) {

			Meta_Add( CPrintf("LinPV[%u][%u]", c, p),
				  m_LinPV[c][p],
				  metaInteger
				  );
			}
		for( p = 0; p < LIN_POINTS; p++  ) {

			Meta_Add( CPrintf( "LinInput[%u][%u]", c, p),
					   m_LinInput[c][p],
					   metaInteger
					   );
			}
		}

	Meta_AddInteger(LinChannel);

	Meta_AddInteger(LinDP);

	Meta_AddInteger(LinSlopeUI);

	Meta_AddInteger(LinOffsetUI);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CIVIConfigWnd, CUIViewWnd);

// Overidables

void CIVIConfigWnd::OnAttach(void)
{
	m_pItem   = (CIVIInput *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("iviinp"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CIVIConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddInputs();

	EndPage(TRUE);
	}

void CIVIConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UpdateUI("ProcessDP1");
		UpdateUI("ProcessDP2");
		UpdateUI("ProcessDP3");
		UpdateUI("ProcessDP4");
		UpdateUI("ProcessDP5");
		UpdateUI("ProcessDP6");
		UpdateUI("ProcessDP7");
		UpdateUI("ProcessDP8");

		DoEnables();
		}

	if( Tag == "PVRange" && m_pItem->m_PVRange == 2 ) {

		m_pItem->m_SquareRt1 = FALSE;
		m_pItem->m_SquareRt2 = FALSE;
		m_pItem->m_SquareRt3 = FALSE;
		m_pItem->m_SquareRt4 = FALSE;
		m_pItem->m_SquareRt5 = FALSE;
		m_pItem->m_SquareRt6 = FALSE;
		m_pItem->m_SquareRt7 = FALSE;
		m_pItem->m_SquareRt8 = FALSE;

		UpdateUI("SquareRt1");
		UpdateUI("SquareRt2");
		UpdateUI("SquareRt3");
		UpdateUI("SquareRt4");
		UpdateUI("SquareRt5");
		UpdateUI("SquareRt6");
		UpdateUI("SquareRt7");
		UpdateUI("SquareRt8");
		}

	if( Tag == "SquareRt1" && m_pItem->m_SquareRt1 ) {

		m_pItem->m_Linearize[0] = FALSE;
		m_pItem->m_ExtendPV1    = FALSE;

		UpdateUI("Linearize[0]");
		UpdateUI("ExtendPV1");
		}

	if( Tag == "SquareRt2" && m_pItem->m_SquareRt2 ) {

		m_pItem->m_Linearize[1] = FALSE;
		m_pItem->m_ExtendPV2    = FALSE;

		UpdateUI("Linearize[1]");
		UpdateUI("ExtendPV2");
		}

	if( Tag == "SquareRt3" && m_pItem->m_SquareRt3 ) {

		m_pItem->m_Linearize[2] = FALSE;
		m_pItem->m_ExtendPV3    = FALSE;

		UpdateUI("Linearize[2]");
		UpdateUI("ExtendPV3");
		}

	if( Tag == "SquareRt4" && m_pItem->m_SquareRt4 ) {

		m_pItem->m_Linearize[3] = FALSE;
		m_pItem->m_ExtendPV4    = FALSE;

		UpdateUI("Linearize[3]");
		UpdateUI("ExtendPV4");
		}

	if( Tag == "SquareRt5" && m_pItem->m_SquareRt5 ) {

		m_pItem->m_Linearize[4] = FALSE;
		m_pItem->m_ExtendPV5    = FALSE;

		UpdateUI("Linearize[4]");
		UpdateUI("ExtendPV5");
		}

	if( Tag == "SquareRt6" && m_pItem->m_SquareRt6 ) {

		m_pItem->m_Linearize[5] = FALSE;
		m_pItem->m_ExtendPV6    = FALSE;

		UpdateUI("Linearize[5]");
		UpdateUI("ExtendPV6");
		}

	if( Tag == "SquareRt7" && m_pItem->m_SquareRt7 ) {

		m_pItem->m_Linearize[6] = FALSE;
		m_pItem->m_ExtendPV7    = FALSE;

		UpdateUI("Linearize[6]");
		UpdateUI("ExtendPV7");
		}

	if( Tag == "SquareRt8" && m_pItem->m_SquareRt8 ) {

		m_pItem->m_Linearize[7] = FALSE;
		m_pItem->m_ExtendPV8    = FALSE;

		UpdateUI("Linearize[7]");
		UpdateUI("ExtendPV8");
		}

	if( Tag == "ExtendPV1" && m_pItem->m_ExtendPV1 ) {

		m_pItem->m_SquareRt1 = FALSE;

		UpdateUI("SquareRt1");
		}

	if( Tag == "ExtendPV2" && m_pItem->m_ExtendPV2 ) {

		m_pItem->m_SquareRt2 = FALSE;

		UpdateUI("SquareRt2");
		}

	if( Tag == "ExtendPV3" && m_pItem->m_ExtendPV3 ) {

		m_pItem->m_SquareRt3 = FALSE;

		UpdateUI("SquareRt3");
		}

	if( Tag == "ExtendPV4" && m_pItem->m_ExtendPV4 ) {

		m_pItem->m_SquareRt4 = FALSE;

		UpdateUI("SquareRt4");
		}

	if( Tag == "ExtendPV5" && m_pItem->m_ExtendPV5 ) {

		m_pItem->m_SquareRt5 = FALSE;

		UpdateUI("SquareRt5");
		}

	if( Tag == "ExtendPV6" && m_pItem->m_ExtendPV6 ) {

		m_pItem->m_SquareRt6 = FALSE;

		UpdateUI("SquareRt6");
		}

	if( Tag == "ExtendPV7" && m_pItem->m_ExtendPV7 ) {

		m_pItem->m_SquareRt7 = FALSE;

		UpdateUI("SquareRt7");
		}

	if( Tag == "ExtendPV8" && m_pItem->m_ExtendPV8 ) {

		m_pItem->m_SquareRt8 = FALSE;

		UpdateUI("SquareRt8");
		}

	if( Tag == "Linearize[0]" && m_pItem->m_Linearize[0] ) {

		m_pItem->m_SquareRt1   = FALSE;
		m_pItem->m_ProcessMin1 = 0;
		m_pItem->m_ProcessMax1 = 10000;

		UpdateUI("SquareRt1");
		UpdateUI("ProcessMin1");
		UpdateUI("ProcessMax1");
		}

	if( Tag == "Linearize[1]" && m_pItem->m_Linearize[1] ) {

		m_pItem->m_SquareRt2   = FALSE;
		m_pItem->m_ProcessMin2 = 0;
		m_pItem->m_ProcessMax2 = 10000;

		UpdateUI("SquareRt2");
		UpdateUI("ProcessMin2");
		UpdateUI("ProcessMax2");
		}

	if( Tag == "Linearize[2]" && m_pItem->m_Linearize[2] ) {

		m_pItem->m_SquareRt3   = FALSE;
		m_pItem->m_ProcessMin3 = 0;
		m_pItem->m_ProcessMax3 = 10000;

		UpdateUI("SquareRt3");
		UpdateUI("ProcessMin3");
		UpdateUI("ProcessMax3");
		}

	if( Tag == "Linearize[3]" && m_pItem->m_Linearize[3] ) {

		m_pItem->m_SquareRt4   = FALSE;
		m_pItem->m_ProcessMin4 = 0;
		m_pItem->m_ProcessMax4 = 10000;

		UpdateUI("SquareRt4");
		UpdateUI("ProcessMin4");
		UpdateUI("ProcessMax4");
		}

	if( Tag == "Linearize[4]" && m_pItem->m_Linearize[4] ) {

		m_pItem->m_SquareRt5   = FALSE;
		m_pItem->m_ProcessMin5 = 0;
		m_pItem->m_ProcessMax5 = 10000;

		UpdateUI("SquareRt5");
		UpdateUI("ProcessMin5");
		UpdateUI("ProcessMax5");
		}

	if( Tag == "Linearize[5]" && m_pItem->m_Linearize[5] ) {

		m_pItem->m_SquareRt6   = FALSE;
		m_pItem->m_ProcessMin6 = 0;
		m_pItem->m_ProcessMax6 = 10000;

		UpdateUI("SquareRt6");
		UpdateUI("ProcessMin6");
		UpdateUI("ProcessMax6");
		}

	if( Tag == "Linearize[6]" && m_pItem->m_Linearize[6] ) {

		m_pItem->m_SquareRt7   = FALSE;
		m_pItem->m_ProcessMin7 = 0;
		m_pItem->m_ProcessMax7 = 10000;

		UpdateUI("SquareRt7");
		UpdateUI("ProcessMin7");
		UpdateUI("ProcessMax7");
		}

	if( Tag == "Linearize[7]" && m_pItem->m_Linearize[7] ) {

		m_pItem->m_SquareRt8   = FALSE;
		m_pItem->m_ProcessMin8 = 0;
		m_pItem->m_ProcessMax8 = 10000;

		UpdateUI("SquareRt8");
		UpdateUI("ProcessMin8");
		UpdateUI("ProcessMax8");
		}

	DoEnables();

	CUIIVIProcess::CheckUpdate(m_pItem, Tag);
	}

// UI Creation

void CIVIConfigWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_GEN), 1);

	if( m_pItem->m_IVIModel == ID_CSINV8 || m_pItem->m_IVIModel == ID_CSINV8L ) {

		AddUI(m_pItem, TEXT("root"), TEXT("PVRange"));
		}
	else
		AddUI(m_pItem, TEXT("root"), TEXT("PIRange"));

	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));

	EndGroup(TRUE);
	}

void CIVIConfigWnd::AddInputs(void)
{
	BOOL fLinear;

	if( m_pItem->m_IVIModel == ID_CSINI8L || m_pItem->m_IVIModel == ID_CSINV8L ) {

		StartTable(CString(IDS_MODULE_INPUTS), 6);

		fLinear = TRUE;

		AddColHead(CString(IDS_MODULE_ENABLED));
		AddColHead(CString(IDS_MODULE_DECS));
		AddColHead(CPrintf(IDS_MODULE_PV_AT, L"0%"));
		AddColHead(CPrintf(IDS_MODULE_PV_AT, L"100%"));
		AddColHead(CString(IDS_MODULE_SR));
		AddColHead(CString(IDS_LINEARIZER));
		}
	else {
		fLinear = FALSE;

		StartTable(CString(IDS_MODULE_INPUTS), 6);

		AddColHead(L"");
		AddColHead(L"");
		AddColHead(L"");
		AddColHead(L"");
		AddColHead(CString(IDS_MODULE_SBPV1));
		AddColHead(L"");

		AddColHeadExtraRow();

		AddColHead(CString(IDS_MODULE_ENABLED));
		AddColHead(CString(IDS_MODULE_DECS));
		AddColHead(CPrintf(IDS_MODULE_PV_AT, L"0%"));
		AddColHead(CPrintf(IDS_MODULE_PV_AT, L"100%"));
		AddColHead(CString(IDS_MODULE_SBPV2));
		AddColHead(CString(IDS_MODULE_SR));
		}

	for( UINT c = 0; c < 8; c++ ) {
	
		AddRowHead(CPrintf(CString(IDS_MODULE_CHANNEL), c+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("ChanEnable%u", c+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("ProcessDP%u",  c+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("ProcessMin%u", c+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("ProcessMax%u", c+1));

		if( fLinear == FALSE ) {

			AddUI(m_pItem, TEXT("root"), CPrintf("ExtendPV%u", c+1));
			}
		
		AddUI(m_pItem, TEXT("root"), CPrintf("SquareRt%u", c+1));

		if( fLinear == TRUE ) {

			AddUI(m_pItem, TEXT("root"), CPrintf("Linearize[%u]", c));
			}
		}

	EndTable();
	}

// Enabling

void CIVIConfigWnd::DoEnables(void)
{
	if( m_pItem->m_PVRange == 2 ) {

		EnableUI("SquareRt1",    FALSE);
		EnableUI("SquareRt2",    FALSE);
		EnableUI("SquareRt3",    FALSE);
		EnableUI("SquareRt4",    FALSE);
		EnableUI("SquareRt5",    FALSE);
		EnableUI("SquareRt6",    FALSE);
		EnableUI("SquareRt7",    FALSE);
		EnableUI("SquareRt8",    FALSE);

		EnableUI("ExtendPV1",    TRUE);
		EnableUI("ExtendPV2",    TRUE);
		EnableUI("ExtendPV3",    TRUE);
		EnableUI("ExtendPV4",    TRUE);
		EnableUI("ExtendPV5",    TRUE);
		EnableUI("ExtendPV6",    TRUE);
		EnableUI("ExtendPV7",    TRUE);
		EnableUI("ExtendPV8",    TRUE);

		EnableUI("Linearize[0]", TRUE);
		EnableUI("Linearize[1]", TRUE);
		EnableUI("Linearize[2]", TRUE);
		EnableUI("Linearize[3]", TRUE);
		EnableUI("Linearize[4]", TRUE);
		EnableUI("Linearize[5]", TRUE);
		EnableUI("Linearize[6]", TRUE);
		EnableUI("Linearize[7]", TRUE);
		}
	else {
		if( m_pItem->m_IVIModel == ID_CSINI8L || m_pItem->m_IVIModel == ID_CSINV8L ) {

			EnableUI("SquareRt1",    !m_pItem->m_Linearize[0]);
			EnableUI("SquareRt2",    !m_pItem->m_Linearize[1]);
			EnableUI("SquareRt3",    !m_pItem->m_Linearize[2]);
			EnableUI("SquareRt4",    !m_pItem->m_Linearize[3]);
			EnableUI("SquareRt5",    !m_pItem->m_Linearize[4]);
			EnableUI("SquareRt6",    !m_pItem->m_Linearize[5]);
			EnableUI("SquareRt7",    !m_pItem->m_Linearize[6]);
			EnableUI("SquareRt8",    !m_pItem->m_Linearize[7]);

			EnableUI("Linearize[0]", !m_pItem->m_SquareRt1);
			EnableUI("Linearize[1]", !m_pItem->m_SquareRt2);
			EnableUI("Linearize[2]", !m_pItem->m_SquareRt3);
			EnableUI("Linearize[3]", !m_pItem->m_SquareRt4);
			EnableUI("Linearize[4]", !m_pItem->m_SquareRt5);
			EnableUI("Linearize[5]", !m_pItem->m_SquareRt6);
			EnableUI("Linearize[6]", !m_pItem->m_SquareRt7);
			EnableUI("Linearize[7]", !m_pItem->m_SquareRt8);
			}
		else {
			EnableUI("SquareRt1",	 !m_pItem->m_ExtendPV1);
			EnableUI("SquareRt2",	 !m_pItem->m_ExtendPV2);
			EnableUI("SquareRt3",	 !m_pItem->m_ExtendPV3);
			EnableUI("SquareRt4",	 !m_pItem->m_ExtendPV4);
			EnableUI("SquareRt5",	 !m_pItem->m_ExtendPV5);
			EnableUI("SquareRt6",	 !m_pItem->m_ExtendPV6);
			EnableUI("SquareRt7",	 !m_pItem->m_ExtendPV7);
			EnableUI("SquareRt8",	 !m_pItem->m_ExtendPV8);

			EnableUI("ExtendPV1",	 !m_pItem->m_SquareRt1);
			EnableUI("ExtendPV2",	 !m_pItem->m_SquareRt2);
			EnableUI("ExtendPV3",	 !m_pItem->m_SquareRt3);
			EnableUI("ExtendPV4",	 !m_pItem->m_SquareRt4);
			EnableUI("ExtendPV5",	 !m_pItem->m_SquareRt5);
			EnableUI("ExtendPV6",	 !m_pItem->m_SquareRt6);
			EnableUI("ExtendPV7",	 !m_pItem->m_SquareRt7);
			EnableUI("ExtendPV8",	 !m_pItem->m_SquareRt8);
			}
		}

	EnableUI("ProcessDP1",  !m_pItem->m_Linearize[0]);
	EnableUI("ProcessDP2",  !m_pItem->m_Linearize[1]);
	EnableUI("ProcessDP3",  !m_pItem->m_Linearize[2]);
	EnableUI("ProcessDP4",  !m_pItem->m_Linearize[3]);
	EnableUI("ProcessDP5",  !m_pItem->m_Linearize[4]);
	EnableUI("ProcessDP6",  !m_pItem->m_Linearize[5]);
	EnableUI("ProcessDP7",  !m_pItem->m_Linearize[6]);
	EnableUI("ProcessDP8",  !m_pItem->m_Linearize[7]);

	EnableUI("ProcessMin1", !m_pItem->m_Linearize[0]);
	EnableUI("ProcessMin2", !m_pItem->m_Linearize[1]);
	EnableUI("ProcessMin3", !m_pItem->m_Linearize[2]);
	EnableUI("ProcessMin4", !m_pItem->m_Linearize[3]);
	EnableUI("ProcessMin5", !m_pItem->m_Linearize[4]);
	EnableUI("ProcessMin6", !m_pItem->m_Linearize[5]);
	EnableUI("ProcessMin7", !m_pItem->m_Linearize[6]);
	EnableUI("ProcessMin8", !m_pItem->m_Linearize[7]);

	EnableUI("ProcessMax1", !m_pItem->m_Linearize[0]);
	EnableUI("ProcessMax2", !m_pItem->m_Linearize[1]);
	EnableUI("ProcessMax3", !m_pItem->m_Linearize[2]);
	EnableUI("ProcessMax4", !m_pItem->m_Linearize[3]);
	EnableUI("ProcessMax5", !m_pItem->m_Linearize[4]);
	EnableUI("ProcessMax6", !m_pItem->m_Linearize[5]);
	EnableUI("ProcessMax7", !m_pItem->m_Linearize[6]);
	EnableUI("ProcessMax8", !m_pItem->m_Linearize[7]);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CIVILinearWnd, CUIViewWnd);

// Overidables

void CIVILinearWnd::OnAttach(void)
{
	m_fScroll = FALSE;

	m_pItem   = (CIVIInput *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("iviinp"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CIVILinearWnd::OnUICreate(void)
{
	StartPage(1);

	AddLinear();

	AddCurve();

	EndPage(TRUE);
	}

void CIVILinearWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "LinChannel" ) {

		m_pItem->m_LinDP       = m_pItem->GetLinDP();

		m_pItem->m_LinSlopeUI  = m_pItem->GetLinSlope();

		m_pItem->m_LinOffsetUI = m_pItem->GetLinOffset();
		
		UpdateUI("LinDP");

		UpdateUI("LinSlopeUI");

		UpdateUI("LinOffsetUI");
		
		DoEnables();
		}

	if( Tag == "LinDP" ) {

		m_pItem->SetLinDP();
		}

	if( Tag == "LinSlopeUI" ) {

		m_pItem->SetLinSlope();
		}

	if( Tag == "LinOffsetUI" ) {

		m_pItem->SetLinOffset();
		}

	CUIIVIProcess::CheckUpdate(m_pItem, Tag);

	m_pItem->m_LinOffsetUI = m_pItem->GetLinOffset();

	UpdateUI("LinOffsetUI");

	m_pCurve->Update();
	}

// UI Creation

void CIVILinearWnd::AddLinear(void)
{
	StartGroup(CString(IDS_LINEARIZER), 4);

	AddUI(m_pItem, TEXT("root"), TEXT("LinChannel" ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinDP"      ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinSlopeUI" ));
	AddUI(m_pItem, TEXT("root"), TEXT("LinOffsetUI"));

	EndGroup(TRUE);
	}

void CIVILinearWnd::AddCurve(void)
{
	StartGroup(CString(IDS_LINEAR_CURVE), 1, FALSE);

	m_pCurve = New CUILinCurve(m_pItem);

	AddElement(m_pCurve);

	EndGroup(TRUE);
	}

// Enabling

void CIVILinearWnd::DoEnables(void)
{
	BOOL fLinEnabled = m_pItem->GetLinEnable();

	EnableUI("LinDP",       fLinEnabled);

	EnableUI("LinSlopeUI",  fLinEnabled);

	EnableUI("LinOffsetUI", fLinEnabled);
	}

// End of File
