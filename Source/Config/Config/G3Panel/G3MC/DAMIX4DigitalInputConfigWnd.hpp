
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4DigitalInputConfigWnd_HPP

#define INCLUDE_DAMix4DigitalInputConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMix4DigitalInputConfig;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input Config Window
//

class CDAMix4DigitalInputConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDAMix4DigitalInputConfig * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Implementation
	void AddInputs(void);
	void DoEnables(UINT uIndex);

	// Data Access
	UINT GetInteger(CMetaItem *pItem, CString Tag);
};

// End of File

#endif
