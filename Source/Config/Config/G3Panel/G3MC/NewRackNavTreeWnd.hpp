
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NewRackNavTreeWnd_HPP

#define INCLUDE_NewRackNavTreeWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Backplane Navigation Window
//

class CNewRackNavTreeWnd : public CNavTreeWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CNewRackNavTreeWnd(void);

	// Overridables
	void OnAttach(void);

protected:
	// Data
	CCommsSystem * m_pSystem;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnLoadTool(UINT uCode, CMenu &Menu);

	// Notification Handlers
	BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);

	// Command Handlers
	BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
	BOOL OnItemControl(UINT uID, CCmdSource &Src);
	BOOL OnItemCommand(UINT uID);
	BOOL OnItemRename(CString Name);
	BOOL OnEditControl(UINT uID, CCmdSource &Src);

	// Tree Loading
	void LoadImageList(void);

	// Item Hooks
	BOOL IncludeItem(CMetaItem *pItem);
	UINT GetRootImage(void);
	UINT GetItemImage(CMetaItem *pItem);
	BOOL GetItemMenu(CString &Name, BOOL fHit);
	void OnItemRenamed(CItem *pItem);
	void OnItemDeleted(CItem *pItem, BOOL fExec);
	void NewItemSelected(void);
};

// End of File

#endif
