
#include "intern.hpp"

#include "daro8outputconfig.hpp"

#include "daro8outputconfigwnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daro8props.h"

#include "import/manticore/daro8dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DARO8 DO Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDARO8OutputConfig, CCommsItem);

// Property List

CCommsList const CDARO8OutputConfig::m_CommsList[] = {

	{ 0, "Enable1",  PROPID_ENABLE1,   usageWriteInit,  IDS_NAME_TPE1 },
	{ 0, "Enable2",  PROPID_ENABLE2,   usageWriteInit,  IDS_NAME_TPE2 },
	{ 0, "Enable3",  PROPID_ENABLE3,   usageWriteInit,  IDS_NAME_TPE3 },
	{ 0, "Enable4",  PROPID_ENABLE4,   usageWriteInit,  IDS_NAME_TPE4 },
	{ 0, "Enable5",  PROPID_ENABLE5,   usageWriteInit,  IDS_NAME_TPE5 },
	{ 0, "Enable6",  PROPID_ENABLE6,   usageWriteInit,  IDS_NAME_TPE6 },
	{ 0, "Enable7",  PROPID_ENABLE7,   usageWriteInit,  IDS_NAME_TPE7 },
	{ 0, "Enable8",  PROPID_ENABLE8,   usageWriteInit,  IDS_NAME_TPE8 },

	{ 0, "Mode1",    PROPID_POMODE1,   usageWriteInit,  IDS_NAME_TPC1 },
	{ 0, "Mode2",    PROPID_POMODE2,   usageWriteInit,  IDS_NAME_TPC2 },
	{ 0, "Mode3",    PROPID_POMODE3,   usageWriteInit,  IDS_NAME_TPC3 },
	{ 0, "Mode4",    PROPID_POMODE4,   usageWriteInit,  IDS_NAME_TPC4 },
	{ 0, "Mode5",    PROPID_POMODE5,   usageWriteInit,  IDS_NAME_TPC5 },
	{ 0, "Mode6",    PROPID_POMODE6,   usageWriteInit,  IDS_NAME_TPC6 },
	{ 0, "Mode7",    PROPID_POMODE7,   usageWriteInit,  IDS_NAME_TPC7 },
	{ 0, "Mode8",    PROPID_POMODE8,   usageWriteInit,  IDS_NAME_TPC8 },

	};

// Constructor

CDARO8OutputConfig::CDARO8OutputConfig(void)
{
	m_Enable1 = 0;
	m_Enable2 = 0;
	m_Enable3 = 0;
	m_Enable4 = 0;
	m_Enable5 = 0;
	m_Enable6 = 0;
	m_Enable7 = 0;
	m_Enable8 = 0;
	m_Mode1   = 0;
	m_Mode2   = 0;
	m_Mode3   = 0;
	m_Mode4   = 0;
	m_Mode5   = 0;
	m_Mode6   = 0;
	m_Mode7   = 0;
	m_Mode8   = 0;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// View Pages

UINT CDARO8OutputConfig::GetPageCount(void)
{
	return 1;
	}

CString CDARO8OutputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0 : return L"Outputs";
		}

	return L"";
	}

CViewWnd * CDARO8OutputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0 : return New CDARO8OutputConfigWnd;
		}

	return NULL;
	}

// Conversion

BOOL CDARO8OutputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 8; n ++ ) {

			ImportNumber(pValue, CPrintf(L"Mode%d",n+1), CPrintf(L"OutMode%d",n+1));
			}
		
		return TRUE;
		}

	return FALSE;
	}

// Meta Data Creation

void CDARO8OutputConfig::AddMetaData(void)
{
	Meta_AddInteger(Enable1);
	Meta_AddInteger(Enable2);
	Meta_AddInteger(Enable3);
	Meta_AddInteger(Enable4);
	Meta_AddInteger(Enable5);
	Meta_AddInteger(Enable6);
	Meta_AddInteger(Enable7);
	Meta_AddInteger(Enable8);

	Meta_AddInteger(Mode1);
	Meta_AddInteger(Mode2);
	Meta_AddInteger(Mode3);
	Meta_AddInteger(Mode4);
	Meta_AddInteger(Mode5);
	Meta_AddInteger(Mode6);
	Meta_AddInteger(Mode7);
	Meta_AddInteger(Mode8);

	CCommsItem::AddMetaData();
	}

// End of File
