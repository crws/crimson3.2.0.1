
#include "intern.hpp"

#include "DAAO8Module.hpp"

#include "DAAO8MainWnd.hpp"

#include "DAAO8Output.hpp"

#include "DAAO8OutputConfig.hpp"

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daao8props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 Module
//

// Dynamic Class

AfxImplementDynamicClass(CDAAO8Module, CManticoreGenericModule);

// Constructor

CDAAO8Module::CDAAO8Module(void)
{
	m_pOutput       = New CDAAO8Output;

	m_pOutputConfig = New CDAAO8OutputConfig;

	m_Ident         = LOBYTE(ID_DAAO8);

	m_FirmID        = FIRM_DAAO8;

	m_Model         = "AO8";

	m_Power         = 36;

	m_Conv.Insert(L"Output", L"Output,Status,Config");
}

// UI Management

CViewWnd * CDAAO8Module::CreateMainView(void)
{
	return New CDAAO8MainWnd;
}

// Comms Object Access

UINT CDAAO8Module::GetObjectCount(void)
{
	return 2;
}

BOOL CDAAO8Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_OUTPUT;

			Data.Name  = L"Output";

			Data.pItem = m_pOutput;

			return TRUE;

		case 1:
			Data.ID    = OBJ_CONFIG_AO;

			Data.Name  = L"Config";

			Data.pItem = m_pOutputConfig;

			return TRUE;
	}

	return FALSE;
}

// Conversion

BOOL CDAAO8Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		// Graphite

		m_pOutputConfig->Convert(pValue->GetChild(L"Config"));

		// Legacy

		m_pOutputConfig->Convert(pValue->GetChild(L"Conf"));

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CDAAO8Module::AddMetaData(void)
{
	Meta_AddObject(Output);
	Meta_AddObject(OutputConfig);

	CGenericModule::AddMetaData();
}

// End of File
