
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2MainWnd_HPP

#define INCLUDE_DAMix2MainWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMix2Module;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Main Window
//

class CDAMix2MainWnd : public CProxyViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2MainWnd(void);

protected:
	// Data Members
	CMultiViewWnd * m_pMult;
	CDAMix2Module * m_pItem;

	// Overridables
	void OnAttach(void);

	// Implementation
	void AddAnalogInputPages(void);
	void AddAnalogOutputPages(void);
	void AddDigitalInputPages(void);
	void AddDigitalOutputPages(void);
	void AddDigitalConfigPage(void);
};

// End of File

#endif
