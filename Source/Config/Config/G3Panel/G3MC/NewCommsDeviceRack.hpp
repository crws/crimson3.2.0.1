
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NewCommsDeviceRack_HPP

#define INCLUDE_NewCommsDeviceRack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device
//

class DLLAPI CNewCommsDeviceRack : public CCommsDevice
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CNewCommsDeviceRack(void);

	// UI Creation
	CViewWnd * CreateView(UINT uType);

	// Attributes
	UINT GetJsonType(void) const;

	// Operations
	void SetClass(CLASS Class);

	// Naming
	CString GetHumanName(void) const;
	CString GetItemOrdinal(void) const;

	// Persistance
	void PostPaste(void);

	// Conversion
	BOOL PostConvert(CString const &Conv);

	// Data Members
	CGenericModule * m_pModule;
	UINT		 m_SlotNum;
	CString		 m_SlotTag;
	CString		 m_Where;

protected:
	// Meta Data Creation
	void AddMetaData(void);
};

// End of File

#endif
