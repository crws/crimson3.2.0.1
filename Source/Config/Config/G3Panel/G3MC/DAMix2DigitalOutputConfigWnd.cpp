
#include "intern.hpp"

#include "DAMix2DigitalOutputConfigWnd.hpp"

#include "DAMix2DigitalOutputConfig.hpp"

#include "DAMix2Module.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2DigitalOutputConfigWnd, CUIViewWnd);

// Overibables

void CDAMix2DigitalOutputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMix2DigitalOutputConfig *) CViewWnd::m_pItem;

	m_pModule = (CDAMix2Module *) m_pItem->GetParent(AfxRuntimeClass(CDAMix2Module));

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dadio_out"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix2DigitalOutputConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddOutputs();

	EndPage(FALSE);
}

void CDAMix2DigitalOutputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(1);
		DoEnables(2);
		DoEnables(3);
		DoEnables(4);
		DoEnables(5);
		DoEnables(6);
		DoEnables(7);
		DoEnables(8);
	}
}

// Implementation

void CDAMix2DigitalOutputConfigWnd::AddOutputs(void)
{
	StartTable(IDS("Control"), 1);

	AddColHead(IDS("Power On State"));

	for( UINT n = 0; n < 8; n++ ) {

		AddRowHead(CPrintf(IDS("Output %u"), n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));
	}

	EndTable();
}

void CDAMix2DigitalOutputConfigWnd::DoEnables(UINT uIndex)
{
	BOOL fMode = GetInteger(m_pModule->m_pDigitalConfig, CPrintf(L"ChanMode%u", uIndex));

	EnableUI(CPrintf(L"Mode%u", uIndex), fMode == 0);
}

// Data Access

UINT CDAMix2DigitalOutputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

// End of File
