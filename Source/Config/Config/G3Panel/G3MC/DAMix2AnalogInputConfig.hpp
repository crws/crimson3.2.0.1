
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2AnalogInputConfig_HPP

#define INCLUDE_DAMix2AnalogInputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Input Configuration
//

class CDAMix2AnalogInputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2AnalogInputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Group Names
	CString GetGroupName(WORD Group);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Persistence
	void Init(void);

	// Item Properties
	UINT m_Range1;
	UINT m_Range2;

	UINT m_TCType[2];

	UINT m_Sample1;
	UINT m_Sample2;

	UINT m_TempUnits1;
	UINT m_TempUnits2;

	UINT m_Offset1;
	UINT m_Offset2;

	UINT m_Slope1;
	UINT m_Slope2;

	UINT m_ProcMin1;
	UINT m_ProcMin2;

	UINT m_ProcMax1;
	UINT m_ProcMax2;

	UINT m_Root1;
	UINT m_Root2;

	UINT m_Filter1;
	UINT m_Filter2;

	UINT m_ProcDP1;
	UINT m_ProcDP2;

	CString m_ProcUnits1;
	CString m_ProcUnits2;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Property Filters
	BOOL SaveProp(CString const &Tag) const;

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void ConvertRange(UINT uIndex);
	void ConvertTempUnits(UINT uIndex);
};

// End of File

#endif
