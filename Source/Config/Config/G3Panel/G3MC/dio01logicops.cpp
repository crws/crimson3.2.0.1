
#include "intern.hpp"

#include "legacy.h"

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//------------------------------------------------------------------------

void CDIO14ModCfg::MakeASCIIEquations(void)
{
	/*-----------------------------------------------
	Equation format in CRIMSON:

		X=OP,Ia,Ib,Ic,...IN:P1,P2,...PN;

	Element	Description
	-----------------------
	X	Symbol number (output)
	OP	Symbol operation (AND,OR,etc.) 
	Ia..IN	Symbols connected to inputs
	P1..PN	Additional parameter list (delay,count,etc.)
	-----------------------------------------------*/
	
	char str[80];
	
	m_NumEquations = 0;
	
	UINT Ne = m_NumEquations;

	//Phase 1 - Scan symbols and generate raw equations.

	UINT i, j;
		
	for(i = 1; i < m_NSI; i++) {

		//Array protection
		if(i >= NUM_SYMS) {ArrayErr(0); return;}

		BOOL HasConnections = FALSE;
				
		//Handle special no-input cases
		if(m_Sym[i].Type == INC) {

			itoa(i,str,10);
			
			strcpy(m_Equation[Ne],str);

			strcat(m_Equation[Ne],"=");

			strcat(m_Equation[Ne],"INC");

			strcat(m_Equation[Ne],",0");

			HasConnections = TRUE;
			}

		for(j = 1; j < m_Sym[i].NumInputs+1; j++) {

			//Array protection
			if(j >= NUM_PINS) {ArrayErr(0); return;}
			
			UINT wire = m_Sym[i].PinWire[j];
			
			if(wire) {

				if(!HasConnections) {
								
					//Start the equation
					itoa(i,str,10);
					
					strcpy(m_Equation[Ne],str);

					strcat(m_Equation[Ne],"=");
					
					switch(m_Sym[i].Type) {
						
						case AND:
							strcat(m_Equation[Ne],"AND"); break;
						case OR:
							strcat(m_Equation[Ne],"OR");  break;
						case XOR:
							strcat(m_Equation[Ne],"XOR"); break;
						case NOT:
							strcat(m_Equation[Ne],"NOT"); break;
						case TIMERUP:
							strcat(m_Equation[Ne],"TUP"); break;
						case TIMERDN:
							strcat(m_Equation[Ne],"TDN"); break;
						case COUNTUP:
							strcat(m_Equation[Ne],"CUP"); break;
						case COUNTDN:
							strcat(m_Equation[Ne],"CDN"); break;
						case LAT:
							strcat(m_Equation[Ne],"LAT"); break;
						case INC:
							strcat(m_Equation[Ne],"INC"); break;
						case OUTC:
							strcat(m_Equation[Ne],"OUC"); break;
						case JCT:
							strcat(m_Equation[Ne],"JCT"); break;
						case MOUT:
							strcat(m_Equation[Ne],"OUT"); break;
						}
					
					HasConnections = TRUE;
					}
				
				UINT sym = 0;								

				BOOL SpecialSym = FALSE;
				
				if(m_Sym[i].Type == JCT) {
					
					SpecialSym = TRUE;

					sym = (UINT)m_Sym[i].Prop[0];
					
					j = NUM_PINS;
					}
				
				if(SpecialSym == FALSE) {
					
					if(m_Wire[wire].Sym1 != i) sym = m_Wire[wire].Sym1;
						
					else sym = m_Wire[wire].Sym2;
					}

				strcat(m_Equation[Ne],",");

				itoa(sym,str,10);
				
				strcat(m_Equation[Ne],str);
				}
			}

		if(HasConnections) {
			
			BOOL ExParams = FALSE;
			
			strcat(m_Equation[Ne],":");
			
			//Add additional parameters.
			if(m_Sym[i].Type == TIMERUP ||
			   m_Sym[i].Type == TIMERDN ||
			   m_Sym[i].Type == COUNTUP ||
			   m_Sym[i].Type == COUNTDN) {

				ExParams = TRUE;
				
				itoa(m_Sym[i].Prop[0],str,10);
				strcat(m_Equation[Ne],str);
				strcat(m_Equation[Ne],",");

				itoa(m_Sym[i].Prop[1],str,10);
				strcat(m_Equation[Ne],str);
				strcat(m_Equation[Ne],",");

				itoa(m_Sym[i].Prop[2],str,10);
				strcat(m_Equation[Ne],str);
				}
			
			//Add additional parameters.
			if(m_Sym[i].Type == INC  ||
			   m_Sym[i].Type == OUTC ||   
			   m_Sym[i].Type == LAT) {

				ExParams = TRUE;

				itoa(m_Sym[i].Prop[0],str,10);
				strcat(m_Equation[Ne],str);
				}
			
			if(ExParams == FALSE) strcat(m_Equation[Ne],"0");
			
			strcat(m_Equation[Ne],";");
			
			++Ne;
			}
		}
		
	m_NumEquations = Ne;
	
	//Phase 2 - Generate ordered equations.

	for(i = INP0; i <= INP7; i++) m_OutputIsDef[i] = TRUE;

	for(i = OUT0; i <= OUT5; i++) m_OutputIsDef[i] = FALSE;

	for(i = START_USER_SYM; i < m_NSI; i++) m_OutputIsDef[i] = FALSE;

	i = 0;
		
	while(i < Ne) {
		
		UINT sym = GetEquationOutput(i);
		
		UINT Inp_Count = 0;
		
		//Test input terms
		for(j = 1; j < m_Sym[sym].NumInputs+1; j++) {
					
			if(GetInputStatus(i,j)) ++Inp_Count;
			}
		
		if(m_Sym[sym].Type == JCT && Inp_Count == 1) {
			
			Inp_Count = m_Sym[sym].NumInputs;
			}
		
		//Equation is defined.
		if(Inp_Count == m_Sym[sym].NumInputs && m_OutputIsDef[sym] == FALSE) {
			
			m_OutputIsDef[sym] = TRUE;
			
			//Move equation to the lowest undefined
			//position in the list and re-parse the list.
			
			for(j = 0; j < Ne; j++) {

				sym = GetEquationOutput(j);
				
				if(!m_OutputIsDef[sym] && j < i) {
							
					char TempEqn[80];
					
					strcpy(TempEqn,m_Equation[i]);
					
					strcpy(m_Equation[i],m_Equation[j]);
					
					strcpy(m_Equation[j],TempEqn);
					
					i = 0;
					
					break;
					}
				}
			}
		++i;
		}
	}

void CDIO14ModCfg::MakeBinaryEquations(void)
{
	/*-----------------------------------------------
	Parse the ordered ASCII equations and generate
	binary equation blocks.

	ASCII:

		X=OP,Ia,Ib,Ic,...IN:P1,P2,...PN;
	  
	BLOB equation format used in the module:

		|X|OP|I1|...|In|2XX|P0|...|Pn|

	Here, each field is a byte.  All fields up to 2XX
	have a range of 1-199.  The first value at 200 or
	greater	indicates the end of input elements.
	The value XX tells how many parameter fields
	follow.  Each parameter field is a byte.
	Parameters are taken as two-byte words.
	-----------------------------------------------*/

	UINT j;

	UINT k;
		
	char str[80];

	m_NumBinElements = 0;
	
	for(UINT i = 0; i < m_NumEquations; i++) {
	
		//Get X
		j = 0;
				
		k = 0;

		while(m_Equation[i][j] != '=') {
		
			str[k] = m_Equation[i][j];

			++j;

			++k;
			}

		str[k] = '\0';

		m_BinEq[m_NumBinElements] = (BYTE)atoi(str);
		
		++m_NumBinElements;

		++j;
				
		//Get OP
		k = 0;

		while(m_Equation[i][j] != ',') {
		
			str[k] = m_Equation[i][j];

			++j;

			++k;
			}

		str[k] = '\0';

		if(strcmp(str,"AND") == 0) m_BinEq[m_NumBinElements] = AND;
		if(strcmp(str,"OR")  == 0) m_BinEq[m_NumBinElements] = OR;
		if(strcmp(str,"XOR") == 0) m_BinEq[m_NumBinElements] = XOR;
		if(strcmp(str,"NOT") == 0) m_BinEq[m_NumBinElements] = NOT;
		
		if(strcmp(str,"TUP") == 0) m_BinEq[m_NumBinElements] = TIMERUP;
		if(strcmp(str,"TDN") == 0) m_BinEq[m_NumBinElements] = TIMERDN;
		if(strcmp(str,"CUP") == 0) m_BinEq[m_NumBinElements] = COUNTUP;
		if(strcmp(str,"CDN") == 0) m_BinEq[m_NumBinElements] = COUNTDN;
		if(strcmp(str,"LAT") == 0) m_BinEq[m_NumBinElements] = LAT;

		if(strcmp(str,"INC") == 0) m_BinEq[m_NumBinElements] = INC;
		if(strcmp(str,"OUC") == 0) m_BinEq[m_NumBinElements] = OUTC;

		if(strcmp(str,"JCT") == 0) m_BinEq[m_NumBinElements] = JCT;
		if(strcmp(str,"OUT") == 0) m_BinEq[m_NumBinElements] = MOUT;

		++m_NumBinElements;

		++j;

		//Get input elements
		while(1) {
		
			k = 0;

			while(m_Equation[i][j] != ',' && m_Equation[i][j] != ':') {
			
				str[k] = m_Equation[i][j];

				++j;

				++k;
				}
			
			str[k] = '\0';
			
			m_BinEq[m_NumBinElements] = (BYTE)atoi(str);
						
			++m_NumBinElements;
			
			if(m_Equation[i][j] == ':') {++j; break;}

			++j;
			}

		//Save and skip the "num of parameters" location.
		UINT param_ind = m_NumBinElements;

		BYTE param_count = 0;

		//Get parameter elements
		while(1) {
		
			k = 0;

			while(m_Equation[i][j] != ',' && m_Equation[i][j] != ';') {
			
				str[k] = m_Equation[i][j];

				++j;

				++k;
				}
			
			str[k] = '\0';
						
			++m_NumBinElements;
			
			++param_count;
			
			m_BinEq[m_NumBinElements] = (BYTE)atoi(str);
						
			if(m_Equation[i][j] == ';') break;

			++j;
			}

		++m_NumBinElements;

		m_BinEq[param_ind] = (BYTE)(200+param_count);
		}
	}
	
void CDIO14ModCfg::LogicEngine(void)
{
	/*-----------------------------------------------
	Parse the ordered ASCII equations and generate user
	outputs	based on user inputs.
	-----------------------------------------------*/

	UINT i;
		
	//Pre-clear output states
	for(i = OUT0; i <= OUT5; i++) {

		m_EResult[i] = 0;
		}
	
	//Parse ordered equations and execute logic functions
	for(i = 0; i < m_NumEquations; i++) {
						
		GetEquationOperator(i);

		UINT SymOut = GetEquationOutput(i);
		
		//Array protection
		if(SymOut >= NUM_SYMS) {ArrayErr(0); return;}
		
		switch(m_Sym[SymOut].Type) {

			case AND:
				m_EResult[SymOut] = Do_AND_Function(SymOut,i); break;
			case OR:
				m_EResult[SymOut] = Do_OR_Function(SymOut,i);  break;
			case XOR:
				m_EResult[SymOut] = Do_XOR_Function(SymOut,i); break;
			case NOT:
				m_EResult[SymOut] = Do_NOT_Function(SymOut,i); break;
			case TIMERUP:
				m_EResult[SymOut] = Do_TIMERUP_Function(SymOut,i); break;
			case TIMERDN:
				m_EResult[SymOut] = Do_TIMERDN_Function(SymOut,i); break;
			case COUNTUP:
				m_EResult[SymOut] = Do_COUNTUP_Function(SymOut,i); break;
			case COUNTDN:
				m_EResult[SymOut] = Do_COUNTDN_Function(SymOut,i); break;
			case LAT:
				m_EResult[SymOut] = Do_LATCH_Function(SymOut,i);  break;
			case INC:
				m_EResult[SymOut] = Do_INCOIL_Function(SymOut,i);  break;
			case OUTC:
				m_EResult[SymOut] = Do_OUTCOIL_Function(SymOut,i);  break;
			case JCT:
				m_EResult[SymOut] = Do_JCT_Function(SymOut,i);  break;
			case MOUT:
				m_EResult[SymOut] = Do_MOUT_Function(SymOut,i); break;
			}

		if(m_OldResult[SymOut] != m_EResult[SymOut]) m_LogicStateChange = TRUE;
		
		m_OldResult[SymOut] = m_EResult[SymOut];
		}
	}

UINT CDIO14ModCfg::GetEquationOutput(UINT eqn)
{
	char str[10];

	//Array protection
	if(eqn >= NUM_EQUATIONS) {ArrayErr(0); return 0;}
	
	UINT i = 0;
	
	if(eqn > NUM_EQUATIONS) {ArrayErr(0); return 0;}

	while(m_Equation[eqn][i] != '=') {

		str[i] = m_Equation[eqn][i];

		++i;
				
		//Array protection
		if(i >= NUM_EQ_ELEMENTS) {ArrayErr(0); return 0;}
		}

	str[i] = '\0';
	
	return (UINT)atoi(str);
	}
	
void CDIO14ModCfg::GetEquationOperator(UINT eqn)
{
	char str[NUM_EQ_ELEMENTS];
	
	UINT i = 0;
	
	//Array protection
	if(eqn >= NUM_EQUATIONS) {ArrayErr(0); return;}

	while(m_Equation[eqn][i] != '=') {

		++i;

		//Array protection
		if(i >= NUM_EQ_ELEMENTS) {ArrayErr(0); return;}
		}
	++i;

	UINT j = 0;

	while(m_Equation[eqn][i] != ',') {

		str[j] = m_Equation[eqn][i];
		
		++i;

		++j;

		//Array protection
		if(i >= NUM_EQ_ELEMENTS) {ArrayErr(0); return;}
		if(j >= NUM_EQ_ELEMENTS) {ArrayErr(0); return;}
		}
			
	str[j] = '\0';
	
	strcpy(m_OprStr,str);
	}

BOOL CDIO14ModCfg::GetInputStatus(UINT eqn,UINT inp)
{
	UINT i, j = 0;
	
	if(eqn >= NUM_EQUATIONS) {ArrayErr(0); return FALSE;}

	//Get to the requested input term
	for(i = 0; i < inp; i++) {
		
		while(m_Equation[eqn][j] != ',') {
		
			++j;

			if(j >= NUM_EQ_ELEMENTS) {ArrayErr(0); return FALSE;}
			}
		++j;
		}
	
	i = 0;
	
	char str[NUM_EQ_ELEMENTS];
	
	//Read out the input term
	while(m_Equation[eqn][j] != ',' && m_Equation[eqn][j] != ':') {

		str[i] = m_Equation[eqn][j];

		++i;
		
		++j;
			
		if(i >= NUM_EQ_ELEMENTS) {ArrayErr(0); return FALSE;}
		if(j >= NUM_EQ_ELEMENTS) {ArrayErr(0); return FALSE;}
		}

	str[i] = '\0';
	
	UINT term = (UINT)atoi(str);

	if(term > NUM_SYMS-1) return 0;
	
	return m_OutputIsDef[term];
	}

UINT CDIO14ModCfg::GetInputElement(UINT eqn,UINT inp)
{
	UINT i, j = 0;
	
	if(eqn >= NUM_EQUATIONS) {ArrayErr(0); return 0;}
	
	//Get to the requested input term
	for(i = 0; i < inp; i++) {
		
		while(m_Equation[eqn][j] != ',') {
		
			++j;

			if(j >= NUM_EQ_ELEMENTS) {ArrayErr(0); return 0;}
			}
		++j;
		}
	
	i = 0;
	
	char str[NUM_EQ_ELEMENTS];
	
	//Read out the input term
	while(m_Equation[eqn][j] != ',' && m_Equation[eqn][j] != ':') {

		str[i] = m_Equation[eqn][j];

		++i;
		
		++j;

		if(i >= NUM_EQ_ELEMENTS) {ArrayErr(0); return 0;}
		if(j >= NUM_EQ_ELEMENTS) {ArrayErr(0); return 0;}
		}
	
	str[i] = '\0';
	
	UINT res = (UINT)atoi(str);
	
	if(res > NUM_SYMS-1) return 0;
	
	return res;
	}

void CDIO14ModCfg::RunLogic(void)
{
	MakeASCIIEquations();
	
	SetInputStates();

	LogicEngine();
	}

//////////////////////////////////////////////////////////////////////////
//
// Logic Elements
// 

BOOL CDIO14ModCfg::Do_AND_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/
		
	BOOL result = 1;
		
	//Test symbol inputs
	for(UINT i = 1; i < m_Sym[inst].NumInputs+1; i++) {
			
		UINT Element = GetInputElement(eqn,i);

		if(!m_EResult[Element]) result = 0;
		}
	
	return result;
	}
	
BOOL CDIO14ModCfg::Do_OR_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/
	
	BOOL result = 0;
	
	//Test symbol inputs
	for(UINT i = 1; i < m_Sym[inst].NumInputs+1; i++) {
			
		UINT Element = GetInputElement(eqn,i);

		if(m_EResult[Element]) result = 1;
		}

	return result;
	}

BOOL CDIO14ModCfg::Do_XOR_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/
	
	BOOL result = 0;

	BOOL test;

	UINT Element = GetInputElement(eqn,1);
	
	if(m_EResult[Element] == 1) test = 1;
	else test = 0;
	
	//Test symbol inputs
	for(UINT i = 2; i < m_Sym[inst].NumInputs+1; i++) {
			
		Element = GetInputElement(eqn,i);
		
		if(m_EResult[Element] != test) result = 1;
		}

	return result;
	}

BOOL CDIO14ModCfg::Do_NOT_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/
	
	UINT Element = GetInputElement(eqn,1);

	return !m_EResult[Element];
	}

BOOL CDIO14ModCfg::Do_TIMERUP_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	Prop 0 = Map ID
	Prop 1 = Preset High Byte
	Prop 2 = Preset Low Byte
	---------------------------------------------*/
		
	BOOL Result = 0;
		
	UINT Preset = (UINT)(m_Sym[inst].Prop[1]*256 + m_Sym[inst].Prop[2]);
		
	//Handle reset
	UINT Element = GetInputElement(eqn,2);

	if(m_EResult[Element]) {
		
		m_LogicTimer[inst] = 0;

		return 0;
		}
	
	Element = GetInputElement(eqn,1);

	if(m_EResult[Element]) {
		
		if(m_LogicTimer[inst] < Preset) ++m_LogicTimer[inst];	
		}
	
	if(m_LogicTimer[inst] >= Preset) Result = 1;
		
	return Result;
	}

BOOL CDIO14ModCfg::Do_TIMERDN_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	Prop 0 = Map ID
	Prop 1 = Preset High Byte
	Prop 2 = Preset Low Byte
	---------------------------------------------*/
	
	BOOL Result = 0;
		
	UINT Preset = (UINT)(m_Sym[inst].Prop[1]*256 + m_Sym[inst].Prop[2]);

	//Handle reset
	UINT Element = GetInputElement(eqn,2);

	if(m_EResult[Element]) {
		
		m_LogicTimer[inst] = 0;

		return 0;
		}
	
	Element = GetInputElement(eqn,1);
		
	if(m_EResult[Element]) {
		
		if(m_LogicTimer[inst] < Preset) ++m_LogicTimer[inst];	
		}
	
	if(m_LogicTimer[inst] >= Preset) Result = 1;
		
	return Result;
	}

BOOL CDIO14ModCfg::Do_COUNTUP_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	Prop 0 = Map ID
	Prop 1 = Preset High Byte
	Prop 2 = Preset Low Byte
	Prop 3 = Sim count High Byte
	Prop 4 = Sim count Low Byte
	---------------------------------------------*/
	
	UINT Element = GetInputElement(eqn,1);
	
	if(m_EResult[Element] && m_InputFlag[inst] == 0) {
		
		m_InputFlag[inst] = 1;
		
		if(m_Sym[inst].Prop[4] == 0xFF) ++m_Sym[inst].Prop[3];
			
		++m_Sym[inst].Prop[4];
		}
	
	if(!m_EResult[Element]) m_InputFlag[inst] = 0;
	
	Element = GetInputElement(eqn,2);

	if(m_EResult[Element]) {
		
		m_InputFlag[inst] = 0;
		
		m_Sym[inst].Prop[3] = 0;
		
		m_Sym[inst].Prop[4] = 0;
		}
			
	UINT preset = m_Sym[inst].Prop[1]*256 + m_Sym[inst].Prop[2];
	
	UINT count  = m_Sym[inst].Prop[3]*256 + m_Sym[inst].Prop[4];

	if(preset == 0) {

		if(count > 0) return 1;

		else return 0;
		}
	
	if(count >= preset) {

		m_Sym[inst].Prop[3] = m_Sym[inst].Prop[1];

		m_Sym[inst].Prop[4] = m_Sym[inst].Prop[2];
		
		return 1;
		}

	return 0;
	}

BOOL CDIO14ModCfg::Do_COUNTDN_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	Prop 0 = Map ID
	Prop 1 = Preset High Byte
	Prop 2 = Preset Low Byte
	Prop 3 = Sim count High Byte
	Prop 4 = Sim count Low Byte
	---------------------------------------------*/
	
	UINT Element = GetInputElement(eqn,1);
	
	if(m_EResult[Element] && m_InputFlag[inst] == 0) {
		
		m_InputFlag[inst] = 1;
		
		UINT count  = m_Sym[inst].Prop[3]*256 + m_Sym[inst].Prop[4];
		
		if(count > 0) {

			if(m_Sym[inst].Prop[4] == 0x00) --m_Sym[inst].Prop[3];
				
			--m_Sym[inst].Prop[4];
			}
		}
	
	if(!m_EResult[Element]) m_InputFlag[inst] = 0;
	
	Element = GetInputElement(eqn,2);

	if(m_EResult[Element]) {
		
		m_InputFlag[inst] = 0;
		
		m_Sym[inst].Prop[3] = m_Sym[inst].Prop[1];
		
		m_Sym[inst].Prop[4] = m_Sym[inst].Prop[2];
		}
			
	UINT count  = m_Sym[inst].Prop[3]*256 + m_Sym[inst].Prop[4];

	if(count == 0) {

		return 1;
		}

	return 0;
	}

BOOL CDIO14ModCfg::Do_LATCH_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	Prop 0 = Latch ID.
	Prop 1 = Sim Latch State.
	---------------------------------------------*/
	
	UINT Element = GetInputElement(eqn,1);
	
	//Handle reset.
	if(m_EResult[Element] == 1) m_Sym[inst].Prop[1] = 1;
		
	Element = GetInputElement(eqn,2);

	if(m_EResult[Element] == 1) m_Sym[inst].Prop[1] = 0;
	
	return (BOOL)m_Sym[inst].Prop[1];
	}

BOOL CDIO14ModCfg::Do_INCOIL_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/

	return 0;
	}

BOOL CDIO14ModCfg::Do_OUTCOIL_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/

	return 0;
	}

BOOL CDIO14ModCfg::Do_JCT_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	Prop 0 = Driving symbol instance.
	---------------------------------------------*/
	
	UINT Element = GetInputElement(eqn,1);

	return m_EResult[Element];
	}

BOOL CDIO14ModCfg::Do_MOUT_Function(UINT inst,UINT eqn)
{
	/*---------------------------------------------
	No properties.
	---------------------------------------------*/

	UINT Element = GetInputElement(eqn,1);
	
	return m_EResult[Element];
	}

//End of File
