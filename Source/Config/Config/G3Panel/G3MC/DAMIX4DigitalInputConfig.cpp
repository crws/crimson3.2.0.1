
#include "intern.hpp"

#include "DAMix4DigitalInputConfig.hpp"

#include "DAMix4DigitalInputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4DigitalInputConfig, CCommsItem);

// Property List

CCommsList const CDAMix4DigitalInputConfig::m_CommsList[] = {

	{ 0, "Mode1",      PROPID_MODE1,      usageWriteInit,  IDS_NAME_CT1  },
	{ 0, "Mode2",      PROPID_MODE2,      usageWriteInit,  IDS_NAME_CT2  },
	{ 0, "Mode3",      PROPID_MODE3,      usageWriteInit,  IDS_NAME_CT3  },

	{ 1, "Pull1",	PROPID_PULLMODE1,     usageWriteInit,  IDS_NAME_PULL1	},
	{ 1, "Pull2",	PROPID_PULLMODE2,     usageWriteInit,  IDS_NAME_PULL2	},
	{ 1, "Pull3",	PROPID_PULLMODE3,     usageWriteInit,  IDS_NAME_PULL3	},
};

// Constructor

CDAMix4DigitalInputConfig::CDAMix4DigitalInputConfig(void)
{
	m_Mode1  = 0;
	m_Mode2  = 0;
	m_Mode3  = 0;

	m_Pull1  = 0;
	m_Pull2  = 0;
	m_Pull3  = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAMix4DigitalInputConfig::GetPageCount(void)
{
	return 1;
}

CString CDAMix4DigitalInputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(L"Digital Inputs");
	}

	return L"";
}

CViewWnd * CDAMix4DigitalInputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDAMix4DigitalInputConfigWnd;
	}

	return NULL;
}

// Conversion

BOOL CDAMix4DigitalInputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 3; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"InputMode%d", n+1)) ) {

				// Legacy

				ConvertMode(n);

				continue;
			}

			if( ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"InpMode%d", n+1)) ) {

				// Graphite

				continue;
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Meta Data Creation

void CDAMix4DigitalInputConfig::AddMetaData(void)
{
	Meta_AddInteger(Mode1);
	Meta_AddInteger(Mode2);
	Meta_AddInteger(Mode3);

	Meta_AddInteger(Pull1);
	Meta_AddInteger(Pull2);
	Meta_AddInteger(Pull3);

	CCommsItem::AddMetaData();
}

// Implementation

void CDAMix4DigitalInputConfig::ConvertMode(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Mode%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT uPrev   = pData->ReadInteger(this);

		UINT uData[] = { 2, 1, 0 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
