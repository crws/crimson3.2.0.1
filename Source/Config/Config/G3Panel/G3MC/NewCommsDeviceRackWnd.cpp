
#include "intern.hpp"

#include "NewCommsDeviceRackWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "NewCommsDeviceRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device View
//

// Dynamic Class

AfxImplementDynamicClass(CNewCommsDeviceRackWnd, CProxyViewWnd);

// Constructor

CNewCommsDeviceRackWnd::CNewCommsDeviceRackWnd(void)
{
	m_fRecycle = FALSE;
	}

// Overridables

void CNewCommsDeviceRackWnd::OnAttach(void)
{
	CNewCommsDeviceRack *pDev = (CNewCommsDeviceRack *) m_pItem;

	CGenericModule      *pMod = pDev->m_pModule;

	m_pView = pMod->CreateView(viewItem);

	m_pView->Attach(pMod);
	}

// End of File
