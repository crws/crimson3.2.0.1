
//////////////////////////////////////////////////////////////////////////
//
// Modular Controller Configuration
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TC8ISOMOD_HPP

#define INCLUDE_TC8ISOMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "tc8inp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTC8ISOModule;
class CTC8ISOArtist;
class CTC8ISOMainWnd;

//////////////////////////////////////////////////////////////////////////
//
// Isolated 8-Channel Thermocouple Module
//

class CTC8ISOModule : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTC8ISOModule(void);

		// UI Management
		CViewWnd * CreateMainView(void);
		
		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Data Members
		CTC8Input * m_pInput;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Isolated 8-Channel Thermocouple Module Window
//

class CTC8ISOMainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTC8ISOMainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd *	m_pMult;
		CTC8ISOModule *	m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddTC8ISOPages(void);
	};

// End of File

#endif
