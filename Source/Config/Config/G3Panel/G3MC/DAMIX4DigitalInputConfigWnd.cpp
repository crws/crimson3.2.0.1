
#include "intern.hpp"

#include "DAMix4DigitalInputConfigWnd.hpp"

#include "DAMix4DigitalInputConfig.hpp"

#include "DAMix4Module.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4DigitalInputConfigWnd, CUIViewWnd);

// Overibables

void CDAMix4DigitalInputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMix4DigitalInputConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("damix_di_cfg"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix4DigitalInputConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	EndPage(FALSE);
}

void CDAMix4DigitalInputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(1);
		DoEnables(2);
		DoEnables(3);
	}
}

// Implementation

void CDAMix4DigitalInputConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 2);

	AddColHead(IDS("Input Mode"));

	AddColHead(IDS("Source/Sink Mode"));

	for( UINT n = 0; n < 3; n++ ) {

		AddRowHead(CPrintf(IDS_MODULE_INPUT, n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Pull%d", n+1));
	}

	EndTable();
}

void CDAMix4DigitalInputConfigWnd::DoEnables(UINT uIndex)
{
	CDAMix4Module *pModule = (CDAMix4Module *) m_pItem->GetParent(AfxRuntimeClass(CDAMix4Module));

	BOOL fMode = GetInteger(pModule->m_pDigitalConfig, CPrintf(L"ChanMode%u", uIndex));

	EnableUI(CPrintf(L"Mode%u", uIndex), fMode == 1);

	EnableUI(CPrintf(L"Pull%u", uIndex), fMode == 1);
}

// Data Access

UINT CDAMix4DigitalInputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

// End of File
