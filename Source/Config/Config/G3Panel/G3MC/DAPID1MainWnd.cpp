
#include "intern.hpp"

#include "DAPID1MainWnd.hpp"

#include "DAPID1ViewWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Digitail Input Output Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAPID1MainWnd, CSLCMainWnd);

// Constructor

CDAPID1MainWnd::CDAPID1MainWnd(void)
{
}

// Overribables

void CDAPID1MainWnd::OnAttach(void)
{
	m_pItem = (CSLCModule *) CProxyViewWnd::m_pItem;

	AddPIDPages();

	AddMapPages();

	AddIdentPage();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDAPID1MainWnd::AddIdentPage(void)
{
	CString   Name  = IDS("Hardware");

	CViewWnd *pPage = New CDAPID1ViewWnd;

	m_pMult->AddView(Name, pPage);

	pPage->Attach(m_pItem);
}

// End of File
