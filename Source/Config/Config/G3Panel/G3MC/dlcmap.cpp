
#include "intern.hpp"

#include "legacy.h"

#include "dlcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Mapper
//

// Runtime Class

AfxImplementRuntimeClass(CDLCMapper, CCommsItem);

// Property List

CCommsList const CDLCMapper::m_CommsList[] = {

	{ 0, "DigOutMap1",		PROPID_DIG_OUT_MAP_1,	usageWriteInit,	IDS_NAME_DOM1	},
	{ 0, "DigOutMap2",		PROPID_DIG_OUT_MAP_2,	usageWriteInit,	IDS_NAME_DOM2	},
	{ 0, "DigOutMap3",		PROPID_DIG_OUT_MAP_3,	usageWriteInit,	IDS_NAME_DOM3	},
	{ 0, "DigOutMap4",		PROPID_DIG_OUT_MAP_4,	usageWriteInit,	IDS_NAME_DOM4	},
	
	{ 0, "LedOutMap1",		PROPID_LED_OUT_MAP_1,	usageWriteInit,	IDS_NAME_LOM1	},
	{ 0, "LedOutMap2",		PROPID_LED_OUT_MAP_2,	usageWriteInit,	IDS_NAME_LOM2	},
	{ 0, "LedOutMap3",		PROPID_LED_OUT_MAP_3,	usageWriteInit,	IDS_NAME_LOM3	},
	{ 0, "LedOutMap4",		PROPID_LED_OUT_MAP_4,	usageWriteInit,	IDS_NAME_LOM4	},
	{ 0, "LedOutMap5",		PROPID_LED_OUT_MAP_5,	usageWriteInit,	IDS_NAME_LOM5	},
	{ 0, "LedOutMap6",		PROPID_LED_OUT_MAP_6,	usageWriteInit,	IDS_NAME_LOM6	},
									
	{ 1, "CycleTime1",		PROPID_CYCLE_TIME_1,	usageWriteBoth,	IDS_NAME_CT1	},
	{ 1, "CycleTime2",		PROPID_CYCLE_TIME_2,	usageWriteBoth,	IDS_NAME_CT2	},
	{ 1, "CycleTime3",		PROPID_CYCLE_TIME_3,	usageWriteBoth,	IDS_NAME_CT3	},
	{ 1, "CycleTime4",		PROPID_CYCLE_TIME_4,	usageWriteBoth,	IDS_NAME_CT4	},
	
	{ 2, "DigRemote1",		PROPID_DIG_REMOTE_1,	usageWriteUser,	IDS_NAME_DR1	},
	{ 2, "DigRemote2",		PROPID_DIG_REMOTE_2,	usageWriteUser,	IDS_NAME_DR2	},
	{ 2, "DigRemote3",		PROPID_DIG_REMOTE_3,	usageWriteUser,	IDS_NAME_DR3	},
	{ 2, "DigRemote4",		PROPID_DIG_REMOTE_4,	usageWriteUser,	IDS_NAME_DR4	},

	{ 3, "OP1State",		PROPID_OP1_STATE,	usageRead,	IDS_OP1_STATE	},
	{ 3, "OP2State",		PROPID_OP2_STATE,	usageRead,	IDS_OP2_STATE	},
	{ 3, "OP3State",		PROPID_OP3_STATE,	usageRead,	IDS_OP3_STATE	},
	{ 3, "OP4State",		PROPID_OP4_STATE,	usageRead,	IDS_OP4_STATE	},

	};

// Constructor

CDLCMapper::CDLCMapper(void)
{
	m_DigOutMap1	= ANL_HEAT;
	m_DigOutMap2	= 0;
	m_DigOutMap3	= ANL_HEAT_2;
	m_DigOutMap4	= 0;
	m_LedOutMap1	= LED_OUT1;
	m_LedOutMap2	= LED_OUT2;
	m_LedOutMap3	= LED_OUT3;
	m_LedOutMap4	= LED_OUT4;
	m_LedOutMap5	= ANY_EITHER_1;	
	m_LedOutMap6	= ANY_EITHER_2;	
	m_CycleTime1	= 20;
	m_CycleTime2	= 20;
	m_CycleTime3	= 20;
	m_CycleTime4	= 20;		
	m_DigRemote1	= 0;
	m_DigRemote2	= 0;
	m_DigRemote3	= 0;
	m_DigRemote4	= 0;
	m_OP1State	= 0;
	m_OP2State	= 0;
	m_OP3State	= 0;
	m_OP4State	= 0;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CDLCMapper::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_CYCLE_TIME);

		case 2:	return CString(IDS_MODULE_REMOTE);

		case 3:	return CString(IDS_MODULE_INFO);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CDLCMapper::GetPageCount(void)
{
	return 2;
	}

CString CDLCMapper::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_OUTPUTS);
		
		case 1: return CString(IDS_MODULE_LEDS);

		}

	return L"";
	}

CViewWnd * CDLCMapper::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDLCMapMainWnd;

		case 1: return New CDLCMapLEDsWnd;

		}

	return NULL;
	}

// Data Scaling

DWORD CDLCMapper::GetIntProp(PCTXT pTag)
{
	if( IsTenTimes(pTag) ) {

		CMetaData const *pMeta = FindMetaData(pTag);

		if( pTag ) {

			INT nData = pMeta->ReadInteger(this);

			if( nData > 0 ) {

				nData = (nData + 5) / 10;
				}

			if( nData < 0 ) {

				nData = (nData - 5) / 10;
				}

			return DWORD(nData);
			}

		return 0;
		}

	return CCommsItem::GetIntProp(pTag);
	}

BOOL CDLCMapper::IsTenTimes(CString Tag)
{
	if( Tag == "LinOutMin" ) {

 		return TRUE;
		}

	if( Tag == "LinOutMax" ) {

		return TRUE;
		}

	if( Tag == "LinOutDead" ) {

 		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CDLCMapper::AddMetaData(void)
{
	Meta_AddInteger(DigOutMap1);
	Meta_AddInteger(DigOutMap2);
	Meta_AddInteger(DigOutMap3);
	Meta_AddInteger(DigOutMap4);
	Meta_AddInteger(LedOutMap1);
	Meta_AddInteger(LedOutMap2);
	Meta_AddInteger(LedOutMap3);
	Meta_AddInteger(LedOutMap4);
	Meta_AddInteger(LedOutMap5);	
	Meta_AddInteger(LedOutMap6);	
	Meta_AddInteger(CycleTime1);
	Meta_AddInteger(CycleTime2);
	Meta_AddInteger(CycleTime3);
	Meta_AddInteger(CycleTime4);	
	Meta_AddInteger(DigRemote1);
	Meta_AddInteger(DigRemote2);
	Meta_AddInteger(DigRemote3);
	Meta_AddInteger(DigRemote4);
	Meta_AddInteger(OP1State);
	Meta_AddInteger(OP2State);
	Meta_AddInteger(OP3State);
	Meta_AddInteger(OP4State);

	CMetaItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Mapper Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDLCMapMainWnd, CUIViewWnd);

// Overibables

void CDLCMapMainWnd::OnAttach(void)
{
	m_pItem   = (CDLCMapper *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dlcmap"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CDLCMapMainWnd::OnUICreate(void)
{
	StartPage(1);

	AddDigital();

	EndPage(FALSE);
	}

void CDLCMapMainWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}

	if( Tag == "DigOutMap1" ) {

		EnableOutput(1);
		}

	if( Tag == "DigOutMap2" ) {

		EnableOutput(2);
		}

	if( Tag == "DigOutMap3" ) {

		EnableOutput(3);
		}

	if( Tag == "DigOutMap4" ) {	

		EnableOutput(4);	
		}

	CDLCModule *pModule = (CDLCModule *) m_pItem->GetParent();

	CUIPIDProcess::CheckUpdate(pModule->m_pLoop1, Tag);

	CUIPIDProcess::CheckUpdate(pModule->m_pLoop2, Tag);
	}

// UI Creation

void CDLCMapMainWnd::AddDigital(void)
{
	StartGroup(CString(IDS_MODULE_DIG_OUTS), 2);
	
	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap1"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime1"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap2"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime2"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap3"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime3"));
	AddUI(m_pItem, TEXT("root"), TEXT("DigOutMap4"));
	AddUI(m_pItem, TEXT("root"), TEXT("CycleTime4"));
	
	EndGroup(TRUE);
	}

// Enabling

void CDLCMapMainWnd::DoEnables(void)
{
	EnableOutput(1);

	EnableOutput(2);
	
	EnableOutput(3);

	EnableOutput(4);	
	}

void CDLCMapMainWnd::EnableOutput(UINT n)
{
	BYTE bCode = 0x00;

	switch( n ) {

		case 1: bCode = BYTE(m_pItem->m_DigOutMap1); break;
		case 2: bCode = BYTE(m_pItem->m_DigOutMap2); break;
		case 3: bCode = BYTE(m_pItem->m_DigOutMap3); break;
		case 4: bCode = BYTE(m_pItem->m_DigOutMap4); break;	

		}

	BOOL fEnable = ((bCode & 0xF0) == 0x70);

	EnableUI(CPrintf("CycleTime%u", n), fEnable);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Mapper LEDs View
//

// Runtime Class

AfxImplementRuntimeClass(CDLCMapLEDsWnd, CUIViewWnd);

// Overibables

void CDLCMapLEDsWnd::OnAttach(void)
{
	m_pItem   = (CDLCMapper *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("dlcmap"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CDLCMapLEDsWnd::OnUICreate(void)
{
	StartPage(1);

	AddLEDs();

	EndPage(FALSE);
	}

void CDLCMapLEDsWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
		}
	}

// UI Creation

void CDLCMapLEDsWnd::AddLEDs(void)
{
	StartGroup(CString(IDS_MODULE_LED_FUNC), 1);
	
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap1"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap2"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap3"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap4"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap5"));
	AddUI(m_pItem, TEXT("root"), TEXT("LedOutMap6"));
	
	EndGroup(TRUE);
	}

// Enabling

void CDLCMapLEDsWnd::DoEnables(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- DLC Mapping
//

// Dynamic Class

AfxImplementDynamicClass(CUITextDLCMap, CUITextEnum);

// Constructor

CUITextDLCMap::CUITextDLCMap(void)
{
	}

// Core Overidables

void CUITextDLCMap::OnBind(void)
{
	char cKey = char(m_UIData.m_Format[0]);

	AddData(0x00, CString(IDS_MODULE_UNASSIGNED));

	switch( cKey ) {

		case 'A':
			AddCoreAnalog();
			AddMiscAnalog();
			break;

		case 'D':
			AddCoreAnalog();
			AddDigital();
			break;

		case 'L':
			AddOutputs();
			AddDigital();
			break;

		default:
			AfxAssert(FALSE);
			break;
		}
	}

// Implementation

void CUITextDLCMap::AddCoreAnalog(void)
{
	AddData(0x70, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_PWR_HEAT)));		
	AddData(0x71, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_PWR_COOL)));

	AddData(0x76, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_PWR_HEAT)));
	AddData(0x77, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_PWR_COOL)));	
	}

void CUITextDLCMap::AddMiscAnalog(void)
{
	AddData(0x72, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_SP_REQ))); 
	AddData(0x73, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_SP_AC))); 
	AddData(0x74, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_PROC_VALUE))); 
	AddData(0x75, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_PROC_ERROR))); 
	
	AddData(0x78, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_SP_REQ)));
	AddData(0x79, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_SP_AC)));
	AddData(0x7A, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_PROC_VALUE))); 
	AddData(0x7B, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_PROC_ERROR))); 

	}

void CUITextDLCMap::AddDigital(void)
{
	AddData(0x42, CPrintf(CString(IDS_MODULE_ALRM_ANY), L""));	 
	AddData(0x40, CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_PROC)));	
	AddData(0x41, CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_HCM)));	
	 
	AddData(0x45, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_ANY), L"")));
	AddData(0x43, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_PROC))));
	AddData(0x44, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_HCM))));

	AddData(0x20, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_STAT), 1)));
	AddData(0x21, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_STAT), 2)));
	AddData(0x22, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_STAT), 3)));
	AddData(0x23, CPrintf(CString(IDS_MODULE_CH1), CPrintf(CString(IDS_MODULE_ALRM_STAT), 4)));
	AddData(0x24, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_HCM_LO)));
	AddData(0x25, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_HCM_HI)));

	AddData(0x26, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_INP_FAULT)));
	AddData(0x10, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_MODE_MAN)));
	AddData(0x30, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_OUT_PEGLO)));
	AddData(0x31, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_OUT_PEGHI)));
	AddData(0x11, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_AUTO_BUSY)));
	AddData(0x12, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_AUTO_DONE)));
	AddData(0x13, CPrintf(CString(IDS_MODULE_CH1), CString(IDS_MODULE_AUTO_FAIL)));

	AddData(0x48, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_ANY), L"")));
	AddData(0x46, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_PROC))));
	AddData(0x47, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_ANY), CString(IDS_MODULE_HCM))));

	AddData(0x27, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_STAT), 1)));
	AddData(0x28, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_STAT), 2)));
	AddData(0x29, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_STAT), 3)));
	AddData(0x2A, CPrintf(CString(IDS_MODULE_CH2), CPrintf(CString(IDS_MODULE_ALRM_STAT), 4)));

	AddData(0x2B, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_HCM_LO)));
	AddData(0x2C, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_HCM_HI)));

	AddData(0x2D, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_INP_FAULT)));
	AddData(0x14, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_MODE_MAN)));
	AddData(0x37, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_OUT_PEGLO)));
	AddData(0x38, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_OUT_PEGHI)));
	AddData(0x15, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_AUTO_BUSY)));
	AddData(0x16, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_AUTO_DONE)));
	AddData(0x17, CPrintf(CString(IDS_MODULE_CH2), CString(IDS_MODULE_AUTO_FAIL)));

	AddData(0x33, CPrintf(CString(IDS_MODULE_REM_DIG), 1));
	AddData(0x34, CPrintf(CString(IDS_MODULE_REM_DIG), 2));
	AddData(0x35, CPrintf(CString(IDS_MODULE_REM_DIG), 3));
	AddData(0x36, CPrintf(CString(IDS_MODULE_REM_DIG), 4));
	}

void CUITextDLCMap::AddOutputs(void)
{
	AddData(0x50, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 1));
	AddData(0x51, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 2));
	AddData(0x52, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 3));
	AddData(0x53, CPrintf(CString(IDS_MODULE_OUT_FOLLOW), 4));
	}

// End of File
