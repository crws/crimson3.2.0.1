
#include "intern.hpp"

#include "gmdio14.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/dio14props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMDIO14Module, CGraphiteGenericModule);

// Constructor

CGMDIO14Module::CGMDIO14Module(void)
{
	m_pConfig = New CGraphiteDIO14Config;

	m_Ident   = LOBYTE(ID_GMDIO14);

	m_FirmID  = FIRM_GMDIO14;

	m_Model   = "GMDIO14";

	m_Power   = 36;

	m_Conv.Insert(L"Legacy", L"CDIO14Module");

	m_Conv.Insert(L"Manticore", L"CDADIDOModule");

	m_Conv.Insert(L"DI", IDS("Variables"));

	m_Conv.Insert(L"DO", IDS("Variables"));
	}

// UI Management

CViewWnd * CGMDIO14Module::CreateMainView(void)
{
	return New CGraphiteDIO14MainWnd;
	}

// Comms Object Access

UINT CGMDIO14Module::GetObjectCount(void)
{
	return 1;
	}

BOOL CGMDIO14Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_VAR);
			
			Data.pItem = m_pConfig;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CGMDIO14Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pConfig->Convert(pValue->GetChild(L"Input"));

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CGMDIO14Module::AddMetaData(void)
{
	Meta_AddObject(Config);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteDIO14MainWnd, CProxyViewWnd);

// Constructor

CGraphiteDIO14MainWnd::CGraphiteDIO14MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CGraphiteDIO14MainWnd::OnAttach(void)
{
	m_pItem = (CGMDIO14Module *) CProxyViewWnd::m_pItem;

	AddPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CGraphiteDIO14MainWnd::AddPages(void)
{
	CGraphiteDIO14Config *pConfig = m_pItem->m_pConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(pConfig->GetPageName(n), pPage);

		pPage->Attach(pConfig);
		}
	}

// End of File
