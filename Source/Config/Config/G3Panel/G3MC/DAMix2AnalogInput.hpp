
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2AnalogInput_HPP

#define INCLUDE_DAMix2AnalogInput_HPP

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 Inputs
//

class CDAMix2AnalogInput : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2AnalogInput(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_InputAlarm1;
	UINT m_InputAlarm2;

	UINT m_PV1;
	UINT m_PV2;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
