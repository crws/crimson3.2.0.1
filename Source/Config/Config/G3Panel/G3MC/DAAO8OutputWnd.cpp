
#include "intern.hpp"

#include "DAAO8OutputWnd.hpp"

#include "DAAO8Output.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 AI Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAAO8OutputWnd, CUIViewWnd);

// Overibables

void CDAAO8OutputWnd::OnAttach(void)
{
	m_pItem   = (CDAAO8Output *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("daao8_out"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAAO8OutputWnd::OnUICreate(void)
{
	StartPage(1);

	AddOutputs();

	EndPage(FALSE);
}

// Implementation

void CDAAO8OutputWnd::AddOutputs(void)
{
	StartGroup(IDS("Initialization"), 1);

	AddUI(m_pItem, L"root", L"InitData");

	for( UINT n = 0; n < 8; n++ ) {

		AddUI(m_pItem, L"root", CPrintf("Data%u", n + 1));
	}

	EndGroup(TRUE);
}

// End of File
