
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DARO8Output_HPP

#define INCLUDE_DARO8Output_HPP

//////////////////////////////////////////////////////////////////////////
//
// DARO8 DO
//

class CDARO8Output : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDARO8Output(void);

		// Group Names
		CString GetGroupName(WORD wGroup);

		// Item Properties
		UINT m_Output1;
		UINT m_Output2;
		UINT m_Output3;
		UINT m_Output4;
		UINT m_Output5;
		UINT m_Output6;
		UINT m_Output7;
		UINT m_Output8;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Implementation
		void AddMetaData(void);
	};

// End of File

#endif
