
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UIInputProcess_HPP

#define INCLUDE_UIInputProcess_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Input Process Value
//

class CUIInputProcess : public CUIEditBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIInputProcess(void);

		// Destructor
		~CUIInputProcess(void);

		// Update Support
		static void CheckUpdate(CCommsItem *pConms, CString const &Tag);

		// Operations
		void Update(BOOL fFlag);

	protected:
		// Linked List
		static CUIInputProcess * m_pHead;
		static CUIInputProcess * m_pTail;

		// Data Members
		CUIInputProcess * m_pNext;
		CUIInputProcess * m_pPrev;

		// Core Overridables
		void OnBind(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Input Process Value
//

class CUITextInputProcess : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextInputProcess(void);

		// Destructor
		~CUITextInputProcess(void);

	protected:
		// Data Members
		CCommsItem    * m_pComms;
		char		m_cType;	

		// Core Overidables
		void OnBind(void);

		// Implementation
		void    GetConfig(void);
		void    CheckFlags(void);
		UINT    GetInteger(CString Tag);
		CString GetString (CString Tag);

		// Friends
		friend class CUIInputProcess;
	};

// End of File

#endif
