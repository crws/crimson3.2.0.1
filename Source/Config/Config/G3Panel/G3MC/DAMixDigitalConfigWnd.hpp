
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMixDigitalConfigWnd_HPP

#define INCLUDE_DAMixDigitalConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMixDigitalConfig;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Config Window
//

class CDAMixDigitalConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDAMixDigitalConfig * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);

	// Implementation
	void AddConfig(void);
};

// End of File

#endif

