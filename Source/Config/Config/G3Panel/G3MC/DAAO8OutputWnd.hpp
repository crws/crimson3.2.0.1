
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAAO8OutputWnd_HPP

#define INCLUDE_DAAO8OutputWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAAO8Output;

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 AI Config Window
//

class CDAAO8OutputWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDAAO8Output * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);

	// Implementation
	void AddOutputs(void);
};

// End of File

#endif
