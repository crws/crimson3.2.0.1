
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4AnalogOutputWnd_HPP

#define INCLUDE_DAMix4AnalogOutputWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMix4AnalogOutput;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output Main View
//

class CDAMix4AnalogOutputWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDAMix4AnalogOutput * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);

	// Implementation
	void AddOutputs(void);
};

// End of File

#endif
