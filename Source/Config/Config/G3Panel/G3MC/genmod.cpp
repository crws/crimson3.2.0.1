
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Module
//

// Runtime Class

AfxImplementRuntimeClass(CGenericModule, CObjectItem);

// Constructor

CGenericModule::CGenericModule(void)
{
	m_FirmID = FIRM_NONE;

	m_Guid.CreateUnique();

	m_Drop   = 0;

	m_Slot   = 0;

	m_Power  = 0;
	}

// UI Creation

CViewWnd * CGenericModule::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateMainView();
		}

	return CObjectItem::CreateView(uType);
	}

// UI Management

CViewWnd * CGenericModule::CreateMainView(void)
{
	return NULL;
	}

// Comms Object Access

UINT CGenericModule::GetObjectCount(void)
{
	return 0;
	}

BOOL CGenericModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	return FALSE;
	}

CString CGenericModule::GetObjectConv(CString Name) const
{
	INDEX i = m_Conv.FindName(Name);

	return m_Conv.Failed(i) ? L"" : m_Conv[i];
	}

// Conversion

BOOL CGenericModule::Convert(CPropValue *pValue)
{
	if( pValue ) {

		UINT c = GetObjectCount();

		for( UINT n = 0; n < c; n++ ) {

			CObjectData Object;

			if( GetObjectData(n, Object) ) {

				CPropValue *pObject = pValue->GetChild(Object.Name);

				AfxAssert(pObject);

				Object.pItem->Convert(pObject);
				
				continue;
				}

			break;
			}

		return TRUE;
		}
	
	return FALSE;
	}

// Conversion Class

CString CGenericModule::GetClassConv(void) const
{
	CString Model = m_pDbase->GetSystemItem()->GetModel();
	
	if( Model.StartsWith(L"da") ) {

		if( CString(GetClassName()).StartsWith(L"CDA") ) {

			return L"KEEP";
		}

		INDEX i = m_Conv.FindName(CString("Manticore"));

		return m_Conv.Failed(i) ? L"" : m_Conv[i];
		}

	if( Model.StartsWith(L"g") ) {

		if( CString(GetClassName()).StartsWith(L"CGM") ) {

			return L"KEEP";
		}

		INDEX i = m_Conv.FindName(CString("Graphite"));

		return m_Conv.Failed(i) ? L"" : m_Conv[i];
		}
	
	INDEX i = m_Conv.FindName(CString("Legacy"));

	return m_Conv.Failed(i) ? L"" : m_Conv[i];
	}

// Download Support

BOOL CGenericModule::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Ident));

	MakeConfigData(Init);

	Init.AddGuid(m_Guid);

	MakeObjectData(Init);

	return CObjectItem::MakeInitData(Init);
	}

// Implementation

void CGenericModule::AddMetaData(void)
{
	CObjectItem::AddMetaData();

	Meta_AddGuid(Guid);
	Meta_AddInteger(Drop);
	Meta_AddInteger(Slot);
	}

// Dirty Management

void CGenericModule::OnSetDirty(void)
{
	m_Guid.CreateUnique();
	}

// Download Data

void CGenericModule::MakeConfigData(CInitData &Init)
{
	Init.AddWord(0);
	}

void CGenericModule::MakeObjectData(CInitData &Init)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphite Generic Module
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteGenericModule, CGenericModule);

// Constructor

CGraphiteGenericModule::CGraphiteGenericModule(void)
{
	}

// Download Data

void CGraphiteGenericModule::MakeConfigData(CInitData &Init)
{
	Init.AddByte(BYTE(m_FirmID));

	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);
	}

// End of File
