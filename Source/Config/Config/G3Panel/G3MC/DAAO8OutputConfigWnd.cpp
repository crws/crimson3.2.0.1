
#include "intern.hpp"

#include "DAAO8OutputConfigWnd.hpp"

#include "DAAO8OutputConfig.hpp"

#include "DAAO8Module.hpp"

#include "UIAO8Dynamic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 AI Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAAO8OutputConfigWnd, CUIViewWnd);

// Constructor

CDAAO8OutputConfigWnd::CDAAO8OutputConfigWnd(UINT uPage)
{
	m_uPage = uPage;
}

// Overibables

void CDAAO8OutputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAAO8OutputConfig *) CViewWnd::m_pItem;

	m_pModule = (CDAAO8Module *) m_pItem->GetParent(AfxRuntimeClass(CDAAO8Module));

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("daao8_cfg"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAAO8OutputConfigWnd::OnUICreate(void)
{
	if( m_uPage < 8 ) {

		StartPage(1);

		AddOutputs();

		EndPage(FALSE);
	}
	else {
		StartPage(1);

		StartGroup(IDS("Initialization"), 1);

		AddUI(m_pItem, L"root", L"InitData");

		for( UINT n = 0; n < 8; n++ ) {

			AddUI(m_pItem, L"root", CPrintf("DataInit%u", n + 1));
		}

		EndGroup(TRUE);

		EndPage(FALSE);
	}
}

void CDAAO8OutputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "InitData" ) {

		DoEnables();
	}

	CUIAO8Dynamic::CheckUpdate(m_pItem, Tag);
}

// Implementation

void CDAAO8OutputConfigWnd::AddOutputs(void)
{
	StartGroup(IDS("Properties"), 1);

	AddUI(m_pItem, L"root", CPrintf("Type%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("DP%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("DataLo%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("DataHi%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("OutputLo%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("OutputHi%u", m_uPage + 1));

	EndGroup(TRUE);
}

// Data Access

UINT CDAAO8OutputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

void CDAAO8OutputConfigWnd::PutInteger(CMetaItem *pItem, CString Tag, UINT Data)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	pData->WriteInteger(pItem, Data);
}

// Enabling

void CDAAO8OutputConfigWnd::DoEnables(void)
{
	BOOL fEnable = m_pItem->m_InitData;

	EnableUI("DataInit1", fEnable);
	EnableUI("DataInit2", fEnable);
	EnableUI("DataInit3", fEnable);
	EnableUI("DataInit4", fEnable);
	EnableUI("DataInit5", fEnable);
	EnableUI("DataInit6", fEnable);
	EnableUI("DataInit7", fEnable);
	EnableUI("DataInit8", fEnable);
}

// End of File
