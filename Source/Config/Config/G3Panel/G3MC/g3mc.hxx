
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3MC_HXX
	
#define	INCLUDE_G3MC_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdesk.hxx>

#include <pcwin.hxx>

// End of File

#endif
