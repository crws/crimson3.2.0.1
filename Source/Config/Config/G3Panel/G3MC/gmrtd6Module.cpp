
#include "intern.hpp"

#include "rtd6mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module
//

class CGMRTD6Module : public CRTD6Module
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMRTD6Module(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMRTD6Module, CRTD6Module);

// Constructor

CGMRTD6Module::CGMRTD6Module(void)
{
	m_FirmID = FIRM_GMRTD6;

	m_Ident  = LOBYTE(ID_GMRTD6);

	m_Model  = "GMRTD6";

	m_Power  = 14;

	m_Conv.Insert(L"Legacy", L"CRTD6Module");
	}

// Download Support

void CGMRTD6Module::MakeConfigData(CInitData &Init)
{
	Init.AddByte(BYTE(m_FirmID));

	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);
	}

// End of File
