
#include "intern.hpp"

#include "legacy.h"

#include "master.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Master Module
//

// Dynamic Class

AfxImplementDynamicClass(CMasterModule, CGenericModule);

// Constructor

CMasterModule::CMasterModule(void)
{
	m_pFirm = New CFirmwareList;

	m_pData = New CMasterData;

	m_Conv.Insert(L"Graphite", GetClassName());

	m_Conv.Insert(L"Manticore", GetClassName());

	m_Conv.Insert(L"Legacy", GetClassName());
	}

// Destructor

CMasterModule::~CMasterModule(void)
{
	delete m_pData;
	}

// Comms Object Access

UINT CMasterModule::GetObjectCount(void)
{
	return 1;
	}

BOOL CMasterModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_COMMS;
			
			Data.Name  = "Comms";
			
			Data.pItem = m_pData;

			return TRUE;
		}

	return FALSE;
	}

// Downoad Support

BOOL CMasterModule::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	Init.AddByte(ID_CSPID1);

	Init.AddByte(0);

	m_pFirm->MakeInitData(Init);

	return TRUE;
	}

// Implementation

void CMasterModule::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Firm);
	}

//////////////////////////////////////////////////////////////////////////
//
// Master Module Data
//

// Dynamic Class

AfxImplementDynamicClass(CMasterData, CCommsItem);

// Property List

CCommsList CMasterData::m_CommsList[] = {

	{ 1, "Error01",		PROPID_ERROR_01,		usageRead,	IDS_NAME_ERR1	},
	{ 1, "Error02",		PROPID_ERROR_02,		usageRead,	IDS_NAME_ERR2	},
	{ 1, "Error03",		PROPID_ERROR_03,		usageRead,	IDS_NAME_ERR3	},
	{ 1, "Error04",		PROPID_ERROR_04,		usageRead,	IDS_NAME_ERR4	},
	{ 1, "Error05",		PROPID_ERROR_05,		usageRead,	IDS_NAME_ERR5	},
	{ 1, "Error06",		PROPID_ERROR_06,		usageRead,	IDS_NAME_ERR6	},
	{ 1, "Error07",		PROPID_ERROR_07,		usageRead,	IDS_NAME_ERR7	},
	{ 1, "Error08",		PROPID_ERROR_08,		usageRead,	IDS_NAME_ERR8	},
	{ 1, "Error09",		PROPID_ERROR_09,		usageRead,	IDS_NAME_ERR9	},
	{ 1, "Error10",		PROPID_ERROR_10,		usageRead,	IDS_NAME_ERR10	},
	{ 1, "Error11",		PROPID_ERROR_11,		usageRead,	IDS_NAME_ERR11	},
	{ 1, "Error12",		PROPID_ERROR_12,		usageRead,	IDS_NAME_ERR12	},
	{ 1, "Error13",		PROPID_ERROR_13,		usageRead,	IDS_NAME_ERR13	},
	{ 1, "Error14",		PROPID_ERROR_14,		usageRead,	IDS_NAME_ERR14	},
	{ 1, "Error15",		PROPID_ERROR_15,		usageRead,	IDS_NAME_ERR15	},
	{ 1, "Error16",		PROPID_ERROR_16,		usageRead,	IDS_NAME_ERR16	},

	{ 2, "WatchDog",	PROPID_TOOLS_WATCHDOG,		usageRead,	IDS_NAME_WD	},

	};

// Constructor

CMasterData::CMasterData(void)
{
	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CMasterData::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1: return CString(IDS_MASTER_MODSTAT);

		case 2: return CString(IDS_MASTER_TOOLS);

		}

	return CCommsItem::GetGroupName(Group);
	}

// Implementation

void CMasterData::AddMetaData(void)
{
	}

// End of File
