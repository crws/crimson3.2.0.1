
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GMRC_HPP

#define INCLUDE_GMRC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Rate Count Configuration
//

class CGraphiteRCConfig : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteRCConfig(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Item Properties
		UINT m_TestProp1;
		UINT m_TestProp2;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rate Count Module
//

class CGMRCModule : public CGraphiteGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMRCModule(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Item Properties
		CGraphiteRCConfig * m_pConfig;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rate Count Module Window
//

class CGraphiteRCMainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteRCMainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd * m_pMult;
		CGMRCModule   * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddPages(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rate Count Input Configuration View
//

class CGraphiteRCConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		CGraphiteRCConfigWnd(void);

	protected:
		// Data Members
		CGraphiteRCConfig * m_pItem;
		UINT	   	    m_uPage;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// Implementation
		void AddInputs(void);
	};

// End of File

#endif
