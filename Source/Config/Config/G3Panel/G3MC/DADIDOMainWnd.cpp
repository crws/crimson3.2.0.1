
#include "intern.hpp"

#include "dadidomainwnd.hpp"

#include "dadidoviewwnd.hpp"

#include "dadidomodule.hpp"

#include "dadidoinputconfig.hpp"

#include "dadidooutputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Digitail Input Output Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOMainWnd, CProxyViewWnd);

// Constructor

CDADIDOMainWnd::CDADIDOMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
}

// Overridables

void CDADIDOMainWnd::OnAttach(void)
{
	m_pItem = (CDADIDOModule *) CProxyViewWnd::m_pItem;

	AddDIPages();

	AddDOPages();

	AddIdentPage();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDADIDOMainWnd::AddIdentPage(void)
{
	CString   Name  = IDS("Hardware");

	CViewWnd *pPage = New CDADIDOViewWnd;

	m_pMult->AddView(Name, pPage);

	pPage->Attach(m_pItem);
}

void CDADIDOMainWnd::AddDIPages(void)
{
	CDADIDOInputConfig *pConfig = m_pItem->m_pInputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDADIDOMainWnd::AddDOPages(void)
{
	CDADIDOOutputConfig *pConfig = m_pItem->m_pOutputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

// End of File
