
#include "intern.hpp"

#include "gmdio14.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/dio14props.h"

#include "import/graphite/dio14dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4 Analog Input Module Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteDIO14Config, CCommsItem);

// Property List

CCommsList const CGraphiteDIO14Config::m_CommsList[] = {

	{ 1, "Input1",		PROPID_DIO_IN1,		usageRead,	IDS_NAME_I1	},
	{ 1, "Input2",		PROPID_DIO_IN2,		usageRead,	IDS_NAME_I2	},
	{ 1, "Input3",		PROPID_DIO_IN3,		usageRead,	IDS_NAME_I3	},
	{ 1, "Input4",		PROPID_DIO_IN4,		usageRead,	IDS_NAME_I4	},
	{ 1, "Input5",		PROPID_DIO_IN5,		usageRead,	IDS_NAME_I5	},
	{ 1, "Input6",		PROPID_DIO_IN6,		usageRead,	IDS_NAME_I6	},
	{ 1, "Input7",		PROPID_DIO_IN7,		usageRead,	IDS_NAME_I7	},
	{ 1, "Input8",		PROPID_DIO_IN8,		usageRead,	IDS_NAME_I8	},

	{ 2, "Output1",		PROPID_DIO_OUT1,	usageWriteBoth,	IDS_NAME_O1	},
	{ 2, "Output2",		PROPID_DIO_OUT2,	usageWriteBoth,	IDS_NAME_O2	},
	{ 2, "Output3",		PROPID_DIO_OUT3,	usageWriteBoth,	IDS_NAME_O3	},
	{ 2, "Output4",		PROPID_DIO_OUT4,	usageWriteBoth,	IDS_NAME_O4	},
	{ 2, "Output5",		PROPID_DIO_OUT5,	usageWriteBoth,	IDS_NAME_O5	},
	{ 2, "Output6",		PROPID_DIO_OUT6,	usageWriteBoth,	IDS_NAME_O6	},

	{ 0, "InpMode1",	PROPID_INPUT_MODE1,	usageWriteInit,	IDS_NAME_IPM1	},
	{ 0, "InpMode2",	PROPID_INPUT_MODE2,	usageWriteInit,	IDS_NAME_IPM2	},
	{ 0, "InpMode3",	PROPID_INPUT_MODE3,	usageWriteInit,	IDS_NAME_IPM3	},
	{ 0, "InpMode4",	PROPID_INPUT_MODE4,	usageWriteInit,	IDS_NAME_IPM4	},
	{ 0, "InpMode5",	PROPID_INPUT_MODE5,	usageWriteInit,	IDS_NAME_IPM5	},
	{ 0, "InpMode6",	PROPID_INPUT_MODE6,	usageWriteInit,	IDS_NAME_IPM6	},
	{ 0, "InpMode7",	PROPID_INPUT_MODE7,	usageWriteInit,	IDS_NAME_IPM7	},
	{ 0, "InpMode8",	PROPID_INPUT_MODE8,	usageWriteInit,	IDS_NAME_IPM8	},

	{ 0, "OutMode1",	PROPID_OUTPUT_MODE1,	usageWriteInit,	IDS_NAME_OPM1	},
	{ 0, "OutMode2",	PROPID_OUTPUT_MODE2,	usageWriteInit,	IDS_NAME_OPM2	},
	{ 0, "OutMode3",	PROPID_OUTPUT_MODE3,	usageWriteInit,	IDS_NAME_OPM3	},
	{ 0, "OutMode4",	PROPID_OUTPUT_MODE4,	usageWriteInit,	IDS_NAME_OPM4	},
	{ 0, "OutMode5",	PROPID_OUTPUT_MODE5,	usageWriteInit,	IDS_NAME_OPM5	},
	{ 0, "OutMode6",	PROPID_OUTPUT_MODE6,	usageWriteInit,	IDS_NAME_OPM6	},

	{ 0, "Test1",		PROPID_DIO_TEST1,	usageWriteBoth,	IDS_NAME_T1	},
	{ 0, "Test2",		PROPID_DIO_TEST2,	usageWriteBoth,	IDS_NAME_T2	},
	{ 0, "Test3",		PROPID_DIO_TEST3,	usageWriteBoth,	IDS_NAME_T3	},
	{ 0, "Test4",		PROPID_DIO_TEST4,	usageWriteBoth,	IDS_NAME_T4	},
	{ 0, "Test5",		PROPID_DIO_TEST5,	usageWriteBoth,	IDS_NAME_T5	},
	{ 0, "Test6",		PROPID_DIO_TEST6,	usageWriteBoth,	IDS_NAME_T6	},

	{ 0, "Holder",		PROPID_DIO_HOLDER,	usageWriteBoth,	IDS_NAME_H	},

	};

// Constructor

CGraphiteDIO14Config::CGraphiteDIO14Config(void)
{
	m_InpMode1 = ACTIVE_HI;
	m_InpMode2 = ACTIVE_HI;
	m_InpMode3 = ACTIVE_HI;
	m_InpMode4 = ACTIVE_HI;
	m_InpMode5 = ACTIVE_HI;
	m_InpMode6 = ACTIVE_HI;
	m_InpMode7 = ACTIVE_HI;
	m_InpMode8 = ACTIVE_HI;
	m_Output1  = 0;
	m_Output2  = 0;
	m_Output3  = 0;
	m_Output4  = 0;
	m_Output5  = 0;
	m_Output6  = 0;
	m_Input1   = 0;
	m_Input2   = 0;
	m_Input3   = 0;
	m_Input4   = 0;
	m_Input5   = 0;
	m_Input6   = 0;
	m_Input7   = 0;
	m_Input8   = 0;
	m_Test1    = 0;
	m_Test2    = 0;
	m_Test3    = 0;
	m_Test4    = 0;
	m_Test5    = 0;
	m_Test6    = 0;
	m_OutMode1 = 0;
	m_OutMode2 = 0;
	m_OutMode3 = 0;
	m_OutMode4 = 0;
	m_OutMode5 = 0;
	m_OutMode6 = 0;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CGraphiteDIO14Config::GetGroupName(WORD Group)
{
	switch( Group ) {
		
		case 1:	return CString(IDS_MODULE_INPUTS);
		case 2:	return CString(IDS_MODULE_OUTPUTS);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CGraphiteDIO14Config::GetPageCount(void)
{
	return 1;
	}

CString CGraphiteDIO14Config::GetPageName(UINT n)
{
	return CString(IDS_MODULE_CONFIG);
	}

CViewWnd * CGraphiteDIO14Config::CreatePage(UINT n)
{
	return New CGraphiteDIO14ConfigWnd;
	}

// Conversion

BOOL CGraphiteDIO14Config::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 8; n ++ ) {

			ImportNumber(pValue, CPrintf(L"InpMode%d",n+1), CPrintf(L"InputMode%d",n+1));

			ImportNumber(pValue, CPrintf(L"Output%d", n+1));

			ImportNumber(pValue, CPrintf(L"Input%d",  n+1));
			}
		
		return TRUE;
		}

	return FALSE;
	}

// Persistance

void CGraphiteDIO14Config::Load(CTreeFile &File)
{
	AddMeta();

	while( !File.IsEndOfData() ) {
		
		CString  Name  = File.GetName();

		if( Name.StartsWith(L"InputMode") ) {

			File.GetValueAsInteger();

			UINT n = wcstoul(PCTXT(Name.Mid(9)), NULL, 10);

			CMetaData const *pMeta = FindMetaData(CPrintf(L"InpMode%u", n));

			if( pMeta ) {
				
				pMeta->WriteInteger(this, ACTIVE_HI);
				}

			continue;
			}

		CMetaData const *pMeta = FindMetaData(Name);

		if( pMeta ) {

			LoadProp(File, pMeta);
			}
		}

	RegisterHandle();
	}

// Property Filter

BOOL CGraphiteDIO14Config::IncludeProp(WORD PropID)
{
	if( PropID == PROPID_DIO_HOLDER ) {
		
		return FALSE;
		}

	return TRUE;
	}

// Meta Data Creation

void CGraphiteDIO14Config::AddMetaData(void)
{
	Meta_AddInteger(InpMode1);
	Meta_AddInteger(InpMode2);
	Meta_AddInteger(InpMode3);
	Meta_AddInteger(InpMode4);
	Meta_AddInteger(InpMode5);
	Meta_AddInteger(InpMode6);
	Meta_AddInteger(InpMode7);
	Meta_AddInteger(InpMode8);

	Meta_AddInteger(Output1);
	Meta_AddInteger(Output2);
	Meta_AddInteger(Output3);
	Meta_AddInteger(Output4);
	Meta_AddInteger(Output5);
	Meta_AddInteger(Output6);

	Meta_AddInteger(Input1);
	Meta_AddInteger(Input2);
	Meta_AddInteger(Input3);
	Meta_AddInteger(Input4);
	Meta_AddInteger(Input5);
	Meta_AddInteger(Input6);
	Meta_AddInteger(Input7);
	Meta_AddInteger(Input8);

	Meta_AddInteger(Test1);
	Meta_AddInteger(Test2);
	Meta_AddInteger(Test3);
	Meta_AddInteger(Test4);
	Meta_AddInteger(Test5);
	Meta_AddInteger(Test6);

	Meta_AddInteger(OutMode1);
	Meta_AddInteger(OutMode2);
	Meta_AddInteger(OutMode3);
	Meta_AddInteger(OutMode4);
	Meta_AddInteger(OutMode5);
	Meta_AddInteger(OutMode6);

	CCommsItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CGraphiteDIO14ConfigWnd, CUIViewWnd);

// Constructor

CGraphiteDIO14ConfigWnd::CGraphiteDIO14ConfigWnd(void)
{
	}

// Overidables

void CGraphiteDIO14ConfigWnd::OnAttach(void)
{
	CUIViewWnd::OnAttach();

	m_pItem   = (CGraphiteDIO14Config *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("gmdio14"));

	CUIViewWnd::OnAttach();
	}

// UI Management

void CGraphiteDIO14ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	AddOutputs();

	EndPage(TRUE);
	}

void CGraphiteDIO14ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	}

void CGraphiteDIO14ConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 1);

	AddColHead(L"Input Mode");

	for( UINT n = 0; n < 8; n ++ ) {

		AddRowHead(CPrintf(IDS_MODULE_INPUT, n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("InpMode%d", n+1));
		}

	EndTable();
	}

void CGraphiteDIO14ConfigWnd::AddOutputs(void)
{
	StartTable(IDS("Outputs"), 1);

	AddColHead(L"Output Mode");

	for( UINT n = 0; n < 6; n ++ ) {

		AddRowHead(CPrintf(IDS("Output %u"), n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("OutMode%d", n+1));
		}

	EndTable();
	}

// End of File
