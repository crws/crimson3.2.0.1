
#include "intern.hpp"

#include "DAAO8Output.hpp"

#include "DAAO8OutputWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daao8props.h"

#include "import/manticore/daao8dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI
//

// Runtime Class

AfxImplementRuntimeClass(CDAAO8Output, CCommsItem);

// Property List

CCommsList const CDAAO8Output::m_CommsList[] = {

	{ 1, "Data1",		PROPID_DATA1,	usageWriteBoth,  IDS_NAME_DATA1	},
	{ 1, "Data2",		PROPID_DATA2,	usageWriteBoth,  IDS_NAME_DATA2	},
	{ 1, "Data3",		PROPID_DATA3,	usageWriteBoth,  IDS_NAME_DATA3	},
	{ 1, "Data4",		PROPID_DATA4,	usageWriteBoth,  IDS_NAME_DATA4	},
	{ 1, "Data5",		PROPID_DATA5,	usageWriteBoth,  IDS_NAME_DATA5	},
	{ 1, "Data6",		PROPID_DATA6,	usageWriteBoth,  IDS_NAME_DATA6	},
	{ 1, "Data7",		PROPID_DATA7,	usageWriteBoth,  IDS_NAME_DATA7	},
	{ 1, "Data8",		PROPID_DATA8,	usageWriteBoth,  IDS_NAME_DATA8	},

	{ 2, "Alarm1",		PROPID_ALARM1,	usageRead,	 IDS_NAME_A1	},
	{ 2, "Alarm2",		PROPID_ALARM2,	usageRead,	 IDS_NAME_A2	},
	{ 2, "Alarm3",		PROPID_ALARM3,	usageRead,	 IDS_NAME_A3	},
	{ 2, "Alarm4",		PROPID_ALARM4,	usageRead,	 IDS_NAME_A4	},
	{ 2, "Alarm5",		PROPID_ALARM5,	usageRead,	 IDS_NAME_A5	},
	{ 2, "Alarm6",		PROPID_ALARM6,	usageRead,	 IDS_NAME_A6	},
	{ 2, "Alarm7",		PROPID_ALARM7,	usageRead,	 IDS_NAME_A7	},
	{ 2, "Alarm8",		PROPID_ALARM8,	usageRead,	 IDS_NAME_A8	},

};

// Constructor

CDAAO8Output::CDAAO8Output(void)
{
	m_Data1 = 0;
	m_Data2 = 0;
	m_Data3 = 0;
	m_Data4 = 0;
	m_Data5 = 0;
	m_Data6 = 0;
	m_Data7 = 0;
	m_Data8 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAAO8Output::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Data");

		case 2:	return CString(L"Alarms");
	}

	return CCommsItem::GetGroupName(Group);
}

// Property Filter

BOOL CDAAO8Output::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_DATA1:
			case PROPID_DATA2:
			case PROPID_DATA3:
			case PROPID_DATA4:
			case PROPID_DATA5:
			case PROPID_DATA6:
			case PROPID_DATA7:
			case PROPID_DATA8:

				return FALSE;
		}
	}

	return TRUE;
}

// Meta Data Creation

void CDAAO8Output::AddMetaData(void)
{
	Meta_AddInteger(Data1);
	Meta_AddInteger(Data2);
	Meta_AddInteger(Data3);
	Meta_AddInteger(Data4);
	Meta_AddInteger(Data5);
	Meta_AddInteger(Data6);
	Meta_AddInteger(Data7);
	Meta_AddInteger(Data8);

	Meta_AddInteger(InitData);

	CCommsItem::AddMetaData();
}

// End of File
