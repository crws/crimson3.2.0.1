
#include "intern.hpp"

#include "legacy.h"

#include "tc8mod.hpp"

#include "tc8isomod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- TC8 Linear Millivolt Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITC8Process, CUIEditBox)

// Linked List

CUITC8Process * CUITC8Process::m_pHead = NULL;

CUITC8Process * CUITC8Process::m_pTail = NULL;

// Constructor

CUITC8Process::CUITC8Process(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Destructor

CUITC8Process::~CUITC8Process(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Update Support

void CUITC8Process::CheckUpdate(CTC8Input *pLoop, CString const &Tag)
{
	if( Tag == "ProcDP" ){

		CUITC8Process *pScan = m_pHead;

		while( pScan ) {

			CUITextTC8Process *pText = (CUITextTC8Process *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				pScan->Update(TRUE);
				}

			pScan = pScan->m_pNext;
			}
		}
	}

// Operations

void CUITC8Process::Update(BOOL fKeep)
{
	CUITextTC8Process *pText = (CUITextTC8Process *) m_pText;

	pText->GetConfig();

	if( fKeep ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
		}
	else {
		INT nData = m_pData->ReadInteger(m_pItem);

		INT nCopy = nData;

		pText->Check(CError(FALSE), nData);

		if( nData != nCopy ) {

			m_pData->WriteInteger(m_pItem, UINT(nData));

			m_pItem->SetDirty();
			}
		
		LoadUI();
		}

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

void CUITC8Process::UpdateUnits(void)
{
	CUITextTC8Process *pText = (CUITextTC8Process *) m_pText;

	pText->GetConfig();

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- TC8 Linear Millivolt Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextTC8Process, CUITextInteger)

// Constructor

CUITextTC8Process::CUITextTC8Process(void)
{
	}

// Destructor

CUITextTC8Process::~CUITextTC8Process(void)
{
	}

// Core Overidables

void CUITextTC8Process::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CTC8Input)) ) {

		m_pLoop = (CTC8Input *) m_pItem;
		}
	else
		AfxAssert(FALSE);

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
	}

// Implementation

void CUITextTC8Process::GetConfig(void)
{
	m_uPlaces = m_pLoop->m_ProcDP;

	m_Units   = "";

	m_nMin = -30000;

	m_nMax = +30000;

	CheckFlags();
	}

void CUITextTC8Process::CheckFlags(void)
{
	if( m_uPlaces ) {
		
		m_uFlags |= textPlaces;
		}
	else 
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
		}
	else 
		m_uFlags &= ~textSigned;
	}
 
// Scaling

INT CUITextTC8Process::StoreToDisp(INT nData)
{
	return nData;
	}

INT CUITextTC8Process::DispToStore(INT nData)
{
	return nData;
	}

// End of File
