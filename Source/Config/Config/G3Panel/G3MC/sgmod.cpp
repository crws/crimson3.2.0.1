
#include "intern.hpp"

#include "legacy.h"

#include "sgmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module
//

// Dynamic Class

AfxImplementDynamicClass(CSGModule, CGenericModule);

// Constructor

CSGModule::CSGModule(void)
{
	m_pMapper = New CSGMapper;

	m_pLoop   = New CSGLoop;

	m_FirmID  = FIRM_SG;

	m_Ident   = ID_CSSG;

	m_Model   = "CSSG1";

	m_Conv.Insert(L"Graphite", L"CGMSGModule");

	m_Conv.Insert(L"Manticore", L"CDASG1Module");
	}

// UI Management

CViewWnd * CSGModule::CreateMainView(void)
{
	return New CSGMainWnd;
	}

// Comms Object Access

UINT CSGModule::GetObjectCount(void)
{
	return 2;
	}

BOOL CSGModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_LOOP);
			
			Data.pItem = m_pLoop;

			return TRUE;

		case 1:
			Data.ID    = OBJ_MAPPER;

			Data.Name  = CString(IDS_MODULE_OUTPUTS);
			
			Data.pItem = m_pMapper;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CSGModule::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pLoop->  Convert(pValue->GetChild(L"Loop"));

		m_pMapper->Convert(pValue->GetChild(L"Mapper"));
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CSGModule::AddMetaData(void)
{
	Meta_AddObject(Mapper);

	Meta_AddObject(Loop);

	CGenericModule::AddMetaData();
	}

// Download Support

void CSGModule::MakeConfigData(CInitData &Init)
{
	Init.AddWord(WORD(m_pLoop->m_PVLimitLo));

	Init.AddWord(WORD(m_pLoop->m_PVLimitHi));

	Init.AddWord(WORD(m_pLoop->m_InputRange1));

	Init.AddWord(WORD(m_pLoop->m_InputRange2));

	Init.AddWord(0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CSGMainWnd, CProxyViewWnd);

// Constructor

CSGMainWnd::CSGMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overribables

void CSGMainWnd::OnAttach(void)
{
	m_pItem = (CSGModule *) CProxyViewWnd::m_pItem;

	AddPIDPages();

	AddMapPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CSGMainWnd::AddPIDPages(void)
{
	CSGLoop *pLoop = m_pItem->m_pLoop;

	for( UINT n = 0; n < pLoop->GetPageCount(); n++ ) {

		CViewWnd *pPage = pLoop->CreatePage(n);

		m_pMult->AddView(pLoop->GetPageName(n), pPage);

		pPage->Attach(pLoop);
		}
	}

void CSGMainWnd::AddMapPages(void)
{
	CSGMapper *pMapper = m_pItem->m_pMapper;

	for( UINT n =0; n < pMapper->GetPageCount(); n++ ) {

		CViewWnd *pPage = pMapper->CreatePage(n);

		m_pMult->AddView(pMapper->GetPageName(n), pPage);

		pPage->Attach(pMapper);
		}
	}

// End of File
