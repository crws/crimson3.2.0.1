
#include "intern.hpp"

#include "UITextMix4AODynamic.hpp"

#include "UIMix4AODynamic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Analog Output Module Dynamic Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextMix4AODynamic, CUITextInteger)

// Constructor

CUITextMix4AODynamic::CUITextMix4AODynamic(void)
{
}

// Destructor

CUITextMix4AODynamic::~CUITextMix4AODynamic(void)
{
}

// Core Overidables

void CUITextMix4AODynamic::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CDAMix4AnalogOutputConfig)) ) {

		m_pConfig = (CDAMix4AnalogOutputConfig *) m_pItem;
	}
	else
		AfxAssert(FALSE);

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
}

// Implementation

void CUITextMix4AODynamic::GetConfig(void)
{
	m_Type = m_UIData.m_Format;

	FindPlaces();

	FindUnits();

	FindRanges();

	CheckFlags();
}

void CUITextMix4AODynamic::FindPlaces(void)
{
	switch( m_Type[0] ) {

		case 'A':
		case 'B':
		case 'E':

			switch( m_Type[1] ) {

				case '1':	m_uPlaces = m_pConfig->m_DP1;	break;
				case '2':	m_uPlaces = m_pConfig->m_DP2;	break;

				default:	m_uPlaces = 3;			break;
			}

			break;

		default:
			m_uPlaces = 3;
			break;
	}
}

void CUITextMix4AODynamic::FindUnits(void)
{
	switch( m_Type[0] ) {

		case 'C':
		case 'D':

			switch( m_Type[1] ) {

				case '1':	m_Units = (m_pConfig->m_Type1 >= 7) ? "mA" : "V";	break;
				case '2':	m_Units = (m_pConfig->m_Type2 >= 7) ? "mA" : "V";	break;
			}
			break;

		default:
			m_Units = "";
			break;
	}
}

void CUITextMix4AODynamic::FindRanges(void)
{
	switch( m_Type[0] ) {

		case 'A':

			switch( m_Type[1] ) {

				case '1':	SetMinMax(m_pConfig->m_Type1);	break;
				case '2':	SetMinMax(m_pConfig->m_Type2);	break;
			}

			break;

		case 'B':
			switch( m_Type[1] ) {

				case 1:
					m_nMin = m_pConfig->m_DataLo1;
					m_nMax = m_pConfig->m_DataHi1;
					break;

				case 2:
					m_nMin = m_pConfig->m_DataLo2;
					m_nMax = m_pConfig->m_DataHi2;
					break;

			}
			break;

		default:
			SetMinMax(0);
			break;
	}
}

void CUITextMix4AODynamic::SetMinMax(UINT OutType)
{
	switch( OutType ) {

		case 1:
			m_nMin =      0;
			m_nMax =   5000;
			break;

		case 4:
			m_nMin =      0;
			m_nMax =  10000;
			break;

		case 5:
			m_nMin = -10000;
			m_nMax =  10000;
			break;

		case 7:
			m_nMin =      0;
			m_nMax =  20000;
			break;

		case 8:
			m_nMin =   4000;
			m_nMax =  20000;
			break;

		default:
			m_nMin = -30000;
			m_nMax =  30000;
			break;
	}
}

void CUITextMix4AODynamic::CheckFlags(void)
{
	if( m_uPlaces ) {

		m_uFlags |= textPlaces;
	}
	else
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
	}
	else
		m_uFlags &= ~textSigned;
}

// End of File
