
#include "intern.hpp"

#include "DAMix2MainWnd.hpp"

#include "DAMix2Module.hpp"

#include "DAMix2AnalogInput.hpp"

#include "DAMix2AnalogInputConfig.hpp"

#include "DAMix2AnalogOutput.hpp"

#include "DAMix2AnalogOutputConfig.hpp"

#include "DAMix2DigitalInput.hpp"

#include "DAMix2DigitalInputConfig.hpp"

#include "DAMix2DigitalOutput.hpp"

#include "DAMix2DigitalOutputConfig.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2MainWnd, CProxyViewWnd);

// Constructor

CDAMix2MainWnd::CDAMix2MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
}

// Overridables

void CDAMix2MainWnd::OnAttach(void)
{
	m_pItem = (CDAMix2Module *) CProxyViewWnd::m_pItem;

	AddAnalogInputPages();

	AddAnalogOutputPages();

	AddDigitalInputPages();

	AddDigitalOutputPages();

	AddDigitalConfigPage();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDAMix2MainWnd::AddAnalogInputPages(void)
{
	CDAMix2AnalogInputConfig *pConfig = m_pItem->m_pAnalogInputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix2MainWnd::AddAnalogOutputPages(void)
{
	CDAMix2AnalogOutputConfig *pConfig = m_pItem->m_pAnalogOutputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix2MainWnd::AddDigitalInputPages(void)
{
	CDAMix2DigitalInputConfig *pConfig = m_pItem->m_pDigitalInputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix2MainWnd::AddDigitalOutputPages(void)
{
	CDAMix2DigitalOutputConfig *pConfig = m_pItem->m_pDigitalOutputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix2MainWnd::AddDigitalConfigPage(void)
{
	CDAMixDigitalConfig *pConfig = m_pItem->m_pDigitalConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

// End of File
