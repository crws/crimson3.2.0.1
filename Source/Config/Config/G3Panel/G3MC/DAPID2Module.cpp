
#include "intern.hpp"

#include "DAPID2Module.hpp"

#include "DAPID2MainWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

// Dynamic Class

AfxImplementDynamicClass(CDAPID2Module, CDLCModule);

// Constructor

CDAPID2Module::CDAPID2Module(void)
{
	m_FirmID = FIRM_DAPID2;

	m_Ident  = LOBYTE(ID_DAPID2_SM);

	m_Model  = "PID2";

	m_Power  = 33;

	m_Conv.Insert(L"Legacy", L"CDLCModule");

	m_Conv.Insert(CString(IDS_MODULE_LOOP), CString(IDS_MODULE_LOOP1));
}

// UI Management

CViewWnd * CDAPID2Module::CreateMainView(void)
{
	return New CDAPID2MainWnd;
}

// Download Support

void CDAPID2Module::MakeConfigData(CInitData &Init)
{
	Init.AddByte(m_FirmID);

	CDLCModule::MakeConfigData(Init);
}

// Implementation

void CDAPID2Module::AddMetaData(void)
{
	Meta_AddInteger(Ident);

	CDLCModule::AddMetaData();
}

// End of File
