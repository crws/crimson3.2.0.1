
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAUIN6InputConfig_HPP

#define INCLUDE_DAUIN6InputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 AI Configuration
//

class CDAUIN6InputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAUIN6InputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Group Names
	CString GetGroupName(WORD Group);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Persistence
	void Init(void);

	// Item Properties
	UINT m_Range1;
	UINT m_Range2;
	UINT m_Range3;
	UINT m_Range4;
	UINT m_Range5;
	UINT m_Range6;

	UINT m_TCType[6];

	UINT m_Sample1;
	UINT m_Sample2;
	UINT m_Sample3;
	UINT m_Sample4;
	UINT m_Sample5;
	UINT m_Sample6;

	UINT m_TempUnits1;
	UINT m_TempUnits2;
	UINT m_TempUnits3;
	UINT m_TempUnits4;
	UINT m_TempUnits5;
	UINT m_TempUnits6;

	UINT m_Offset1;
	UINT m_Offset2;
	UINT m_Offset3;
	UINT m_Offset4;
	UINT m_Offset5;
	UINT m_Offset6;

	UINT m_Slope1;
	UINT m_Slope2;
	UINT m_Slope3;
	UINT m_Slope4;
	UINT m_Slope5;
	UINT m_Slope6;

	UINT m_ProcMin1;
	UINT m_ProcMin2;
	UINT m_ProcMin3;
	UINT m_ProcMin4;
	UINT m_ProcMin5;
	UINT m_ProcMin6;

	UINT m_ProcMax1;
	UINT m_ProcMax2;
	UINT m_ProcMax3;
	UINT m_ProcMax4;
	UINT m_ProcMax5;
	UINT m_ProcMax6;

	UINT m_Root1;
	UINT m_Root2;
	UINT m_Root3;
	UINT m_Root4;
	UINT m_Root5;
	UINT m_Root6;

	UINT m_Filter1;
	UINT m_Filter2;
	UINT m_Filter3;
	UINT m_Filter4;
	UINT m_Filter5;
	UINT m_Filter6;

	UINT m_ProcDP1;
	UINT m_ProcDP2;
	UINT m_ProcDP3;
	UINT m_ProcDP4;
	UINT m_ProcDP5;
	UINT m_ProcDP6;

	CString m_ProcUnits1;
	CString m_ProcUnits2;
	CString m_ProcUnits3;
	CString m_ProcUnits4;
	CString m_ProcUnits5;
	CString m_ProcUnits6;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Property Filters
	BOOL SaveProp(CString const &Tag) const;

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void ConvertRange(UINT uIndex);
	void ConvertTempUnits(UINT uIndex);
};

// End of File

#endif
