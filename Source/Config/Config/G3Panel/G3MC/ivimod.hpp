
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IVIMOD_HPP

#define INCLUDE_IVIMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "iviinp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CIVIModule;
class CII8Module;
class CIV8Module;
class CII8LModule;
class CIV8LModule;
class CIVIArtist;
class CIVIMainWnd;

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current and Process Volt Modules
//

class CIVIModule : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		CIVIInput * m_pInput;

	protected:
		// Download Support
		void MakeObjectData(CInitData &Init);

		// Implementation
		void AddMetaData(void);
		WORD GetProp(WORD PropID);
		WORD OffsetInput(INT nInput);
		WORD OffsetPV(INT nPV, UINT Channel);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current Module
//

class CII8Module : public CIVIModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CII8Module(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current Module
//

class CGMINI8Module : public CIVIModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMINI8Module(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt Module
//

class CIV8Module : public CIVIModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIV8Module(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current Linearizer Module
//

class CII8LModule : public CIVIModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CII8LModule(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt Linearizer Module
//

class CIV8LModule : public CIVIModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIV8LModule(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current and Process Volt Modules Window
//

class CIVIMainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIVIMainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd *	m_pMult;
		CIVIModule    *	m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddIVIPages(void);
	};

// End of File

#endif
