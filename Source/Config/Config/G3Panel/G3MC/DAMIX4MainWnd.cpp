
#include "intern.hpp"

#include "DAMix4MainWnd.hpp"

#include "DAMix4Module.hpp"

#include "DAMix4AnalogInput.hpp"

#include "DAMix4AnalogInputConfig.hpp"

#include "DAMix4AnalogOutput.hpp"

#include "DAMix4AnalogOutputConfig.hpp"

#include "DAMix4DigitalInput.hpp"

#include "DAMix4DigitalInputConfig.hpp"

#include "DAMix4DigitalOutput.hpp"

#include "DAMix4DigitalOutputConfig.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4MainWnd, CProxyViewWnd);

// Constructor

CDAMix4MainWnd::CDAMix4MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
}

// Overridables

void CDAMix4MainWnd::OnAttach(void)
{
	m_pItem = (CDAMix4Module *) CProxyViewWnd::m_pItem;

	AddAnalogInputPages();

	AddAnalogOutputPages();

	AddDigitalInputPages();

	AddDigitalOutputPages();

	AddDigitalConfigPage();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDAMix4MainWnd::AddAnalogInputPages(void)
{
	CDAMix4AnalogInputConfig *pConfig = m_pItem->m_pAnalogInputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix4MainWnd::AddAnalogOutputPages(void)
{
	CDAMix4AnalogOutputConfig *pConfig = m_pItem->m_pAnalogOutputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix4MainWnd::AddDigitalInputPages(void)
{
	CDAMix4DigitalInputConfig *pConfig = m_pItem->m_pDigitalInputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix4MainWnd::AddDigitalOutputPages(void)
{
	CDAMix4DigitalOutputConfig *pConfig = m_pItem->m_pDigitalOutputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

void CDAMix4MainWnd::AddDigitalConfigPage(void)
{
	CDAMixDigitalConfig *pConfig = m_pItem->m_pDigitalConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

// End of File
