
#include "intern.hpp"

#include "dlcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Double Loop Module
//

class CGMPID2Module : public CDLCModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMPID2Module(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Double Loop Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMPID2Module, CDLCModule);

// Constructor

CGMPID2Module::CGMPID2Module(void)
{
	m_FirmID = FIRM_GMPID2;

	m_Ident  = LOBYTE(ID_GMPID2);

	m_Model  = "GMPID2";

	m_Power  = 27;

	m_Conv.Insert(L"Legacy", L"CDLCModule");

	m_Conv.Insert(L"Manticore", L"CDAPID2Module");
	}

// Download Support

void CGMPID2Module::MakeConfigData(CInitData &Init)
{
	Init.AddWord(WORD(m_Slot));
	
	Init.AddLong(m_Drop);

	CDLCModule::MakeConfigData(Init);
	}

// End of File
