
#include "intern.hpp"

#include "gmuin4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/uin4props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module
//

// Dynamic Class

AfxImplementDynamicClass(CGMUIN4Module, CGraphiteGenericModule);

// Constructor

CGMUIN4Module::CGMUIN4Module(void)
{
	m_pConfig = New CGraphiteUIN4Config;

	m_Ident   = LOBYTE(ID_GMUIN4);

	m_FirmID  = FIRM_GMUIN4;

	m_Model   = "GMUIN4";

	m_Power   = 18;

	m_Conv.Insert(L"Manticore", L"CDAUIN6Module");
	}

// UI Management

CViewWnd * CGMUIN4Module::CreateMainView(void)
{
	return New CGraphiteUIN4MainWnd;
	}

// Comms Object Access

UINT CGMUIN4Module::GetObjectCount(void)
{
	return 1;
	}

BOOL CGMUIN4Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_INP);
			
			Data.pItem = m_pConfig;

			return TRUE;
		}

	return FALSE;
	}

// Implementation

void CGMUIN4Module::AddMetaData(void)
{
	Meta_AddObject(Config);

	CGraphiteGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteUIN4MainWnd, CProxyViewWnd);

// Constructor

CGraphiteUIN4MainWnd::CGraphiteUIN4MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CGraphiteUIN4MainWnd::OnAttach(void)
{
	m_pItem = (CGMUIN4Module *) CProxyViewWnd::m_pItem;

	AddPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CGraphiteUIN4MainWnd::AddPages(void)
{
	CGraphiteUIN4Config *pConfig = m_pItem->m_pConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(pConfig->GetPageName(n), pPage);

		pPage->Attach(pConfig);
		}
	}

// End of File
