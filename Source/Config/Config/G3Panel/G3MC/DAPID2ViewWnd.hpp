
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAPID2ViewWnd_HPP

#define INCLUDE_DAPID2iewWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAPID2Module;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Config Window
//

class CDAPID2ViewWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDAPID2Module * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);

	// Implementation
	void AddIdent(void);
};

// End of File

#endif
