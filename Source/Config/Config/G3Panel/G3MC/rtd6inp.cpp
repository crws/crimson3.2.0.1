
#include "intern.hpp"

#include "legacy.h"

#include "rtd6inp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\rtd6dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Item
//

// Runtime Class

AfxImplementRuntimeClass(CRTD6Input, CCommsItem);

// Property List

CCommsList const CRTD6Input::m_CommsList[] = {

	{ 1, "PV1",		PROPID_PV1,			usageRead,	IDS_NAME_PV1	},
	{ 1, "PV2",		PROPID_PV2,			usageRead,	IDS_NAME_PV2	},
	{ 1, "PV3",		PROPID_PV3,			usageRead,	IDS_NAME_PV3	},
	{ 1, "PV4",		PROPID_PV4,			usageRead,	IDS_NAME_PV4	},
	{ 1, "PV5",		PROPID_PV5,			usageRead,	IDS_NAME_PV5	},
	{ 1, "PV6",		PROPID_PV6,			usageRead,	IDS_NAME_PV6	},

	{ 1, "InputAlarm1",	PROPID_INPUT_ALARM1,		usageRead,	IDS_NAME_IA1	},
	{ 1, "InputAlarm2",	PROPID_INPUT_ALARM2,		usageRead,	IDS_NAME_IA2	},
	{ 1, "InputAlarm3",	PROPID_INPUT_ALARM3,		usageRead,	IDS_NAME_IA3	},
	{ 1, "InputAlarm4",	PROPID_INPUT_ALARM4,		usageRead,	IDS_NAME_IA4	},
	{ 1, "InputAlarm5",	PROPID_INPUT_ALARM5,		usageRead,	IDS_NAME_IA5	},
	{ 1, "InputAlarm6",	PROPID_INPUT_ALARM6,		usageRead,	IDS_NAME_IA6	},

	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},

	{ 2, "InputOffset1",	PROPID_INPUT_OFFSET1,		usageWriteBoth,	IDS_NAME_IO1	},
	{ 2, "InputOffset2",	PROPID_INPUT_OFFSET2,		usageWriteBoth,	IDS_NAME_IO2	},
	{ 2, "InputOffset3",	PROPID_INPUT_OFFSET3,		usageWriteBoth,	IDS_NAME_IO3	},
	{ 2, "InputOffset4",	PROPID_INPUT_OFFSET4,		usageWriteBoth,	IDS_NAME_IO4	},
	{ 2, "InputOffset5",	PROPID_INPUT_OFFSET5,		usageWriteBoth,	IDS_NAME_IO5	},
	{ 2, "InputOffset6",	PROPID_INPUT_OFFSET6,		usageWriteBoth,	IDS_NAME_IO6	},

	{ 2, "InputSlope1",	PROPID_INPUT_SLOPE1,		usageWriteBoth,	IDS_NAME_IS1	},
	{ 2, "InputSlope2",	PROPID_INPUT_SLOPE2,		usageWriteBoth,	IDS_NAME_IS2	},
	{ 2, "InputSlope3",	PROPID_INPUT_SLOPE3,		usageWriteBoth,	IDS_NAME_IS3	},
	{ 2, "InputSlope4",	PROPID_INPUT_SLOPE4,		usageWriteBoth,	IDS_NAME_IS4	},
	{ 2, "InputSlope5",	PROPID_INPUT_SLOPE5,		usageWriteBoth,	IDS_NAME_IS5	},
	{ 2, "InputSlope6",	PROPID_INPUT_SLOPE6,		usageWriteBoth,	IDS_NAME_IS6	},

	{ 0, "InputType1",	PROPID_INPUT_TYPE1,		usageWriteInit,	IDS_NAME_IT1	},
	{ 0, "InputType2",	PROPID_INPUT_TYPE2,		usageWriteInit,	IDS_NAME_IT2	},
	{ 0, "InputType3",	PROPID_INPUT_TYPE3,		usageWriteInit,	IDS_NAME_IT3	},
	{ 0, "InputType4",	PROPID_INPUT_TYPE4,		usageWriteInit,	IDS_NAME_IT4	},
	{ 0, "InputType5",	PROPID_INPUT_TYPE5,		usageWriteInit,	IDS_NAME_IT5	},
	{ 0, "InputType6",	PROPID_INPUT_TYPE6,		usageWriteInit,	IDS_NAME_IT6	},

	{ 0, "ChanEnable1",	PROPID_CHAN_ENABLE1,		usageWriteInit,	IDS_NAME_CE1	},
	{ 0, "ChanEnable2",	PROPID_CHAN_ENABLE2,		usageWriteInit,	IDS_NAME_CE2	},
	{ 0, "ChanEnable3",	PROPID_CHAN_ENABLE3,		usageWriteInit,	IDS_NAME_CE3	},
	{ 0, "ChanEnable4",	PROPID_CHAN_ENABLE4,		usageWriteInit,	IDS_NAME_CE4	},
	{ 0, "ChanEnable5",	PROPID_CHAN_ENABLE5,		usageWriteInit,	IDS_NAME_CE5	},
	{ 0, "ChanEnable6",	PROPID_CHAN_ENABLE6,		usageWriteInit,	IDS_NAME_CE6	},

	{ 0, "TempUnits",	PROPID_TEMP_UNITS,		usageWriteInit,	IDS_TEMP_UNITS	},

	};

// Constructor

CRTD6Input::CRTD6Input(void)
{
	m_TempUnits	= 1;

	m_InputType1	= RTD_TYPE385;
	m_InputType2	= RTD_TYPE385;
	m_InputType3	= RTD_TYPE385;
	m_InputType4	= RTD_TYPE385;
	m_InputType5	= RTD_TYPE385;
	m_InputType6	= RTD_TYPE385;

	m_InputFilter	= 20;

	m_InputOffset1	= 0;
	m_InputOffset2	= 0;
	m_InputOffset3	= 0;
	m_InputOffset4	= 0;
	m_InputOffset5	= 0;
	m_InputOffset6	= 0;

	m_InputSlope1	= 1000;
	m_InputSlope2	= 1000;
	m_InputSlope3	= 1000;
	m_InputSlope4	= 1000;
	m_InputSlope5	= 1000;
	m_InputSlope6	= 1000;

	m_ChanEnable1	= TRUE;
	m_ChanEnable2	= TRUE;
	m_ChanEnable3	= TRUE;
	m_ChanEnable4	= TRUE;
	m_ChanEnable5	= TRUE;
	m_ChanEnable6	= TRUE;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CRTD6Input::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_STATUS);

		case 2:	return CString(IDS_MODULE_CTRL);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CRTD6Input::GetPageCount(void)
{
	return 1;
	}

CString CRTD6Input::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_CONFIG);
		}

	return L"";
	}

CViewWnd * CRTD6Input::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CRTD6ConfigWnd;
		}

	return NULL;
	}

// Implementation

void CRTD6Input::AddMetaData(void)
{
	Meta_AddInteger(TempUnits);

	Meta_AddInteger(InputType1);
	Meta_AddInteger(InputType2);
	Meta_AddInteger(InputType3);
	Meta_AddInteger(InputType4);
	Meta_AddInteger(InputType5);
	Meta_AddInteger(InputType6);

	Meta_AddInteger(InputFilter);

	Meta_AddInteger(InputOffset1);
	Meta_AddInteger(InputOffset2);
	Meta_AddInteger(InputOffset3);
	Meta_AddInteger(InputOffset4);
	Meta_AddInteger(InputOffset5);
	Meta_AddInteger(InputOffset6);

	Meta_AddInteger(InputSlope1);
	Meta_AddInteger(InputSlope2);
	Meta_AddInteger(InputSlope3);
	Meta_AddInteger(InputSlope4);
	Meta_AddInteger(InputSlope5);
	Meta_AddInteger(InputSlope6);

	Meta_AddInteger(ChanEnable1);
	Meta_AddInteger(ChanEnable2);
	Meta_AddInteger(ChanEnable3);
	Meta_AddInteger(ChanEnable4);
	Meta_AddInteger(ChanEnable5);
	Meta_AddInteger(ChanEnable6);

	CCommsItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CRTD6ConfigWnd, CUIViewWnd);

// Overidables

void CRTD6ConfigWnd::OnAttach(void)
{
	m_pItem   = (CRTD6Input *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("rtd6inp"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void CRTD6ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddInputs();

	EndPage(TRUE);
	}

void CRTD6ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	}

// UI Creation

void CRTD6ConfigWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_GEN), 2);

	AddUI(m_pItem, TEXT("root"), TEXT("TempUnits"  ));
	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));

	EndGroup(TRUE);
	}

void CRTD6ConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 4);

	AddColHead(CString(IDS_MODULE_ENABLED));
	AddColHead(CString(IDS_MODULE_RTD));
	AddColHead(CString(IDS_MODULE_SLOPE));
	AddColHead(CString(IDS_MODULE_OFFSET));

	for( UINT n = 1; n <= 6; n++ ) {

		AddRowHead(CPrintf(CString(IDS_MODULE_CHANNEL), n));

		AddUI(m_pItem, TEXT("root"), CPrintf("ChanEnable%u",  n));
		AddUI(m_pItem, TEXT("root"), CPrintf("InputType%u",   n));
		AddUI(m_pItem, TEXT("root"), CPrintf("InputSlope%u",  n));
		AddUI(m_pItem, TEXT("root"), CPrintf("InputOffset%u", n));
		}

	EndTable();
	}

// End of File
