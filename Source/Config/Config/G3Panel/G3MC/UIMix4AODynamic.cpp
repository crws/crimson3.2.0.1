
#include "intern.hpp"

#include "UIMix4AODynamic.hpp"

#include "UITextMix4AODynamic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Analog Output Module Dynamic Value
//

// Dynamic Class

AfxImplementDynamicClass(CUIMix4AODynamic, CUIEditBox)

// Linked List

CUIMix4AODynamic * CUIMix4AODynamic::m_pHead = NULL;

CUIMix4AODynamic * CUIMix4AODynamic::m_pTail = NULL;

// Constructor

CUIMix4AODynamic::CUIMix4AODynamic(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
}

// Destructor

CUIMix4AODynamic::~CUIMix4AODynamic(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
}

// Update Support

void CUIMix4AODynamic::CheckUpdate(CDAMix4AnalogOutputConfig *pConfig, CString const &Tag)
{
	if( Tag.Left(4) == "Type" ) {

		CUIMix4AODynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextMix4AODynamic *pText = (CUITextMix4AODynamic *) pScan->m_pText;

			if( pText->m_pConfig == pConfig ) {

				if( pText->m_Type[0] == 'A' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'B' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'C' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'D' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'E' ) {

					pScan->Update(TRUE);
				}
			}

			pScan = pScan->m_pNext;
		}
	}

	if( Tag.Left(2) == "DP" ) {

		CUIMix4AODynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextMix4AODynamic *pText = (CUITextMix4AODynamic *) pScan->m_pText;

			if( pText->m_pConfig == pConfig ) {

				if( pText->m_Type[0] == 'A' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'B' ) {

					pScan->Update(TRUE);
				}

				if( pText->m_Type[0] == 'E' ) {

					pScan->Update(TRUE);
				}
			}

			pScan = pScan->m_pNext;
		}
	}

	if( Tag.Left(4) == "Data" ) {

		CUIMix4AODynamic *pScan = m_pHead;

		while( pScan ) {

			CUITextMix4AODynamic *pText = (CUITextMix4AODynamic *) pScan->m_pText;

			if( pText->m_pConfig == pConfig ) {

				if( pText->m_Type[0] == 'E' ) {

					pScan->Update(TRUE);
				}
			}

			pScan = pScan->m_pNext;
		}
	}
}

// Operations

void CUIMix4AODynamic::Update(BOOL fKeep)
{
	CUITextMix4AODynamic *pText = (CUITextMix4AODynamic *) m_pText;

	pText->GetConfig();

	if( fKeep ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
	}
	else {
		INT nData = m_pData->ReadInteger(m_pItem);

		INT nCopy = nData;

		pText->Check(CError(FALSE), nData);

		if( nData != nCopy ) {

			m_pData->WriteInteger(m_pItem, UINT(nData));

			m_pItem->SetDirty();
		}

		LoadUI();
	}

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
}

void CUIMix4AODynamic::UpdateUnits(void)
{
	CUITextMix4AODynamic *pText = (CUITextMix4AODynamic *) m_pText;

	pText->GetConfig();

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
}

// End of File
