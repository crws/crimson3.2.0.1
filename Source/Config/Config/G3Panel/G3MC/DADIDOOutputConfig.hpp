
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOOutputConfig_HPP

#define INCLUDE_DADIDOOutputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Configuration
//

class CDADIDOOutputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDADIDOOutputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	UINT m_Enable1;
	UINT m_Enable2;
	UINT m_Enable3;
	UINT m_Enable4;
	UINT m_Enable5;
	UINT m_Enable6;
	UINT m_Enable7;
	UINT m_Enable8;

	UINT m_Mode1;
	UINT m_Mode2;
	UINT m_Mode3;
	UINT m_Mode4;
	UINT m_Mode5;
	UINT m_Mode6;
	UINT m_Mode7;
	UINT m_Mode8;

	UINT m_Value1;
	UINT m_Value2;
	UINT m_Value3;
	UINT m_Value4;
	UINT m_Value5;
	UINT m_Value6;
	UINT m_Value7;
	UINT m_Value8;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Meta Data Creation
	void AddMetaData(void);
};

// End of File

#endif
