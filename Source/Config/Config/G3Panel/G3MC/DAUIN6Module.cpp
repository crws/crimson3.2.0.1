
#include "intern.hpp"

#include "DAUIN6Module.hpp"

#include "DAUIN6MainWnd.hpp"

#include "DAUIN6Input.hpp"

#include "DAUIN6InputConfig.hpp"

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dauin6props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 Module
//

// Dynamic Class

AfxImplementDynamicClass(CDAUIN6Module, CManticoreGenericModule);

// Constructor

CDAUIN6Module::CDAUIN6Module(void)
{
	m_pInput        = New CDAUIN6Input;

	m_pInputConfig  = New CDAUIN6InputConfig;

	m_Ident         = LOBYTE(ID_DAUIN6);

	m_FirmID        = FIRM_DAUIN6;

	m_Model         = "UIN6";

	m_Power         = 36;

	m_Conv.Insert(L"Input", L"Inputs,Status,Config");
}

// UI Management

CViewWnd * CDAUIN6Module::CreateMainView(void)
{
	return New CDAUIN6MainWnd;
}

// Comms Object Access

UINT CDAUIN6Module::GetObjectCount(void)
{
	return 2;
}

BOOL CDAUIN6Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_INPUT;

			Data.Name  = L"Inputs";

			Data.pItem = m_pInput;

			return TRUE;

		case 1:
			Data.ID    = OBJ_CONFIG_AI;

			Data.Name  = L"Config";

			Data.pItem = m_pInputConfig;

			return TRUE;
	}

	return FALSE;
}

// Conversion

BOOL CDAUIN6Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		// Graphite

		if( m_pInputConfig->Convert(pValue->GetChild(L"Config")) ) {

			return TRUE;
		}

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CDAUIN6Module::AddMetaData(void)
{
	Meta_AddObject(Input);
	Meta_AddObject(InputConfig);

	CGenericModule::AddMetaData();
}

// End of File
