
#include "intern.hpp"

#include "Import/comms.h"

#include "DAMixDigitalConfig.hpp"

#include "DAMixDigitalConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/damixprops.h"

#include "import/manticore/damixdbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mix Module Digital Input Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAMixDigitalConfig, CCommsItem);

// Property List

CCommsList const CDAMixDigitalConfig::m_CommsList[] = {

	{ 0, "ChanMode1",      PROPID_MODE1,      usageWriteInit,  IDS_NAME_CH1  },
	{ 0, "ChanMode2",      PROPID_MODE2,      usageWriteInit,  IDS_NAME_CH2  },
	{ 0, "ChanMode3",      PROPID_MODE3,      usageWriteInit,  IDS_NAME_CH3  },
	{ 0, "ChanMode4",      PROPID_MODE4,      usageWriteInit,  IDS_NAME_CH4  },
	{ 0, "ChanMode5",      PROPID_MODE5,      usageWriteInit,  IDS_NAME_CH5  },
	{ 0, "ChanMode6",      PROPID_MODE6,      usageWriteInit,  IDS_NAME_CH6  },
	{ 0, "ChanMode7",      PROPID_MODE7,      usageWriteInit,  IDS_NAME_CH7  },
	{ 0, "ChanMode8",      PROPID_MODE8,      usageWriteInit,  IDS_NAME_CH8  },

};

// Constructor

CDAMixDigitalConfig::CDAMixDigitalConfig(UINT uChans)
{
	m_ChanMode1      = 0;
	m_ChanMode2      = 0;
	m_ChanMode3      = 0;
	m_ChanMode4      = 0;
	m_ChanMode5      = 0;
	m_ChanMode6      = 0;
	m_ChanMode7      = 0;
	m_ChanMode8      = 0;

	m_uChans     =  uChans;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAMixDigitalConfig::GetPageCount(void)
{
	return 1;
}

CString CDAMixDigitalConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(L"Digital Configuration");
	}

	return L"";
}

CViewWnd * CDAMixDigitalConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDAMixDigitalConfigWnd;
	}

	return NULL;
}

// Download Support

BOOL CDAMixDigitalConfig::MakeInitData(CInitData &Init)
{
	WORD Value = 0; 

	for( UINT n = 0; n < m_uChans; n++ ) {

		CCommsData Data;

		GetCommsData(n, Data);

		DWORD x = GetIntProp(Data.PropName);

		if( x ) {

			Value |= (1 << n);
		}
	}

	WORD PropID = MAKEPROP(TYPE_BYTE, 7);

	PropID |= (OBJ_GLOBAL << 11);

	PropID |= (CMD_WRITE << 8);

	Init.AddWord(PropID);

	Init.AddWord(Value);

	return TRUE;
}

// Meta Data Creation

void CDAMixDigitalConfig::AddMetaData(void)
{
	Meta_AddInteger(ChanMode1);
	Meta_AddInteger(ChanMode2);
	Meta_AddInteger(ChanMode3);
	Meta_AddInteger(ChanMode4);
	Meta_AddInteger(ChanMode5);
	Meta_AddInteger(ChanMode6);
	Meta_AddInteger(ChanMode7);
	Meta_AddInteger(ChanMode8);

	CCommsItem::AddMetaData();
}

// End of File
