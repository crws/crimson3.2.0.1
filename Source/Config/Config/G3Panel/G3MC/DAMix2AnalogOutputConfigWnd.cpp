
#include "intern.hpp"

#include "DAMix2AnalogOutputConfigWnd.hpp"

#include "DAMix2AnalogOutputConfig.hpp"

#include "UIMix2AODynamic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output Config Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2AnalogOutputConfigWnd, CUIViewWnd);

// Constructor

CDAMix2AnalogOutputConfigWnd::CDAMix2AnalogOutputConfigWnd(UINT uPage)
{
	m_uPage = uPage;
}

// Overibables

void CDAMix2AnalogOutputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMix2AnalogOutputConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("damix2ao_cfg"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix2AnalogOutputConfigWnd::OnUICreate(void)
{
	if( m_uPage < 2 ) {

		StartPage(1);

		AddOutputs();

		EndPage(FALSE);
	}
	else {
		StartPage(1);

		StartGroup(IDS("Initialization"), 1);

		AddUI(m_pItem, L"root", L"InitData");

		for( UINT n = 0; n < 2; n++ ) {

			AddUI(m_pItem, L"root", CPrintf("DataInit%u", n + 1));
		}

		EndGroup(TRUE);

		EndPage(FALSE);
	}
}

void CDAMix2AnalogOutputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "InitData" ) {

		DoEnables();
	}

	CUIMix2AODynamic::CheckUpdate(m_pItem, Tag);
}

// Implementation

void CDAMix2AnalogOutputConfigWnd::AddOutputs(void)
{
	StartGroup(IDS("Properties"), 1);

	AddUI(m_pItem, L"root", CPrintf("Type%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("DP%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("DataLo%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("DataHi%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("OutputLo%u", m_uPage + 1));

	AddUI(m_pItem, L"root", CPrintf("OutputHi%u", m_uPage + 1));

	EndGroup(TRUE);
}

// Data Access

UINT CDAMix2AnalogOutputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

void CDAMix2AnalogOutputConfigWnd::PutInteger(CMetaItem *pItem, CString Tag, UINT Data)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	pData->WriteInteger(pItem, Data);
}

// Enabling

void CDAMix2AnalogOutputConfigWnd::DoEnables(void)
{
	BOOL fEnable = m_pItem->m_InitData;

	EnableUI("DataInit1", fEnable);
	EnableUI("DataInit2", fEnable);
}

// End of File
