
#include "intern.hpp"

#include "legacy.h"

#include "out4conf.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Out4 Configuration Item
//

// Runtime Class

AfxImplementRuntimeClass(COut4Conf, CCommsItem);

// Property List

CCommsList const COut4Conf::m_CommsList[] = {

	{ 1, "Data1",		PROPID_DATA_1,			usageWriteBoth,	IDS_NAME_DATA1 },
	{ 1, "Data2",		PROPID_DATA_2,			usageWriteBoth,	IDS_NAME_DATA2 },
	{ 1, "Data3",		PROPID_DATA_3,			usageWriteBoth,	IDS_NAME_DATA3 },
	{ 1, "Data4",		PROPID_DATA_4,			usageWriteBoth,	IDS_NAME_DATA4 },

	{ 2, "DataLo1",		PROPID_DATA_LO_1,		usageWriteBoth,	IDS_NAME_DATALO1 },
	{ 2, "DataLo2",		PROPID_DATA_LO_2,		usageWriteBoth,	IDS_NAME_DATALO2 },
	{ 2, "DataLo3",		PROPID_DATA_LO_3,		usageWriteBoth,	IDS_NAME_DATALO3 },
	{ 2, "DataLo4",		PROPID_DATA_LO_4,		usageWriteBoth,	IDS_NAME_DATALO4 },
	{ 2, "DataHi1",		PROPID_DATA_HI_1,		usageWriteBoth,	IDS_NAME_DATAHI1 },
	{ 2, "DataHi2",		PROPID_DATA_HI_2,		usageWriteBoth,	IDS_NAME_DATAHI2 },
	{ 2, "DataHi3",		PROPID_DATA_HI_3,		usageWriteBoth,	IDS_NAME_DATAHI3 },
	{ 2, "DataHi4",		PROPID_DATA_HI_4,		usageWriteBoth,	IDS_NAME_DATAHI4 },
	{ 2, "OutputLo1",	PROPID_OUT_LO_1,		usageWriteBoth,	IDS_NAME_OUTLO1 },
	{ 2, "OutputLo2",	PROPID_OUT_LO_2,		usageWriteBoth,	IDS_NAME_OUTLO2 },
	{ 2, "OutputLo3",	PROPID_OUT_LO_3,		usageWriteBoth,	IDS_NAME_OUTLO3 },
	{ 2, "OutputLo4",	PROPID_OUT_LO_4,		usageWriteBoth,	IDS_NAME_OUTLO4 },
	{ 2, "OutputHi1",	PROPID_OUT_HI_1,		usageWriteBoth,	IDS_NAME_OUTHI1 },
	{ 2, "OutputHi2",	PROPID_OUT_HI_2,		usageWriteBoth,	IDS_NAME_OUTHI2 },
	{ 2, "OutputHi3",	PROPID_OUT_HI_3,		usageWriteBoth,	IDS_NAME_OUTHI3 },
	{ 2, "OutputHi4",	PROPID_OUT_HI_4,		usageWriteBoth,	IDS_NAME_OUTHI4 },

	{ 3, "Alarm1",		PROPID_ALARM_1,			usageRead,	IDS_NAME_A1 },
	{ 3, "Alarm2",		PROPID_ALARM_2,			usageRead,	IDS_NAME_A2 },
	{ 3, "Alarm3",		PROPID_ALARM_3,			usageRead,	IDS_NAME_A3 },
	{ 3, "Alarm4",		PROPID_ALARM_4,			usageRead,	IDS_NAME_A4 },

	{ 0, "OutType1",	PROPID_OUT_TYPE_1,		usageWriteInit,	IDS_NAME_LOT },
	{ 0, "OutType2",	PROPID_OUT_TYPE_2,		usageWriteInit,	IDS_NAME_LOT },
	{ 0, "OutType3",	PROPID_OUT_TYPE_3,		usageWriteInit,	IDS_NAME_LOT },
	{ 0, "OutType4",	PROPID_OUT_TYPE_4,		usageWriteInit,	IDS_NAME_LOT },

	};

// Constructor

COut4Conf::COut4Conf(void)
{
	m_OutType1	= OUT_10V;
	m_OutType2	= OUT_10V;
	m_OutType3	= OUT_10V;
	m_OutType4	= OUT_10V;
	m_DP1		= 3;
	m_DP2		= 3;
	m_DP3		= 3;
	m_DP4		= 3;
	m_Data1		= 0;
	m_Data2		= 0;
	m_Data3		= 0;
	m_Data4		= 0;
	m_DataLo1	= 0;
	m_DataLo2	= 0;
	m_DataLo3	= 0;
	m_DataLo4	= 0;
	m_DataHi1	= 10000;
	m_DataHi2	= 10000;
	m_DataHi3	= 10000;
	m_DataHi4	= 10000;
	m_OutputLo1	= 0;
	m_OutputLo2	= 0;
	m_OutputLo3	= 0;
	m_OutputLo4	= 0;
	m_OutputHi1	= 10000;
	m_OutputHi2	= 10000;
	m_OutputHi3	= 10000;
	m_OutputHi4	= 10000;

	m_InitData	= TRUE;

	m_uCommsCount   = elements(m_CommsList);

	m_pCommsData    = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString COut4Conf::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_DATA);

		case 2:	return CString(IDS_MODULE_SCALING);

		case 3:	return CString(IDS_MODULE_ALARMS);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT COut4Conf::GetPageCount(void)
{
	return 2;
	}

CString COut4Conf::GetPageName(UINT n)
{
	switch( n ) {
 
		case 0: return CString(IDS_MODULE_CONFIG);

		case 1: return CString(IDS_INITIAL_OUTPUT);

		}

	return L"";
	}

CViewWnd * COut4Conf::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New COut4ConfigWnd;

		case 1: return New COut4InitOutWnd;

		}

	return NULL;
	}

// Property Filter

BOOL COut4Conf::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_DATA_1:
			case PROPID_DATA_2:
			case PROPID_DATA_3:
			case PROPID_DATA_4:

				return FALSE;
			}
		}

	return TRUE;
	}

// Implementation

void COut4Conf::AddMetaData(void)
{
	Meta_AddInteger(OutType1);
	Meta_AddInteger(OutType2);
	Meta_AddInteger(OutType3);
	Meta_AddInteger(OutType4);
	Meta_AddInteger(DP1);
	Meta_AddInteger(DP2);
	Meta_AddInteger(DP3);
	Meta_AddInteger(DP4);
	Meta_AddInteger(Data1);
	Meta_AddInteger(Data2);
	Meta_AddInteger(Data3);
	Meta_AddInteger(Data4);
	Meta_AddInteger(DataLo1);
	Meta_AddInteger(DataLo2);
	Meta_AddInteger(DataLo3);
	Meta_AddInteger(DataLo4);
	Meta_AddInteger(DataHi1);
	Meta_AddInteger(DataHi2);
	Meta_AddInteger(DataHi3);
	Meta_AddInteger(DataHi4);
	Meta_AddInteger(OutputLo1);
	Meta_AddInteger(OutputLo2);
	Meta_AddInteger(OutputLo3);
	Meta_AddInteger(OutputLo4);
	Meta_AddInteger(OutputHi1);
	Meta_AddInteger(OutputHi2);
	Meta_AddInteger(OutputHi3);
	Meta_AddInteger(OutputHi4);
	Meta_AddInteger(InitData);

	CMetaItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Out4 Configuration View
//

// Runtime Class

AfxImplementRuntimeClass(COut4ConfigWnd, CUIViewWnd);

// Overidables

void COut4ConfigWnd::OnAttach(void)
{
	m_pItem   = (COut4Conf *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("out4conf"));

	CUIViewWnd::OnAttach();
	}

// UI Update

void COut4ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	CUIOut4Dynamic::CheckUpdate(m_pItem, Tag);
	}

void COut4ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	for( UINT n = 0; n < 4; n++ ) {

		AddOutput(n);
		}

	EndPage(FALSE);
	}

// UI Creation

void COut4ConfigWnd::AddOutput(UINT Channel)
{
	switch (Channel){

		case 0:
			StartGroup(CString(IDS_NAME_OUT)+L" 1", 1, FALSE);
			AddUI(m_pItem, TEXT("root"), TEXT("OutType1"  ));
			AddUI(m_pItem, TEXT("root"), TEXT("DP1"       ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataLo1"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataHi1"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputLo1" ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputHi1" ));
			EndGroup(TRUE);
			break;

		case 1:
			StartGroup(CString(IDS_NAME_OUT)+L" 2", 1, FALSE);
			AddUI(m_pItem, TEXT("root"), TEXT("OutType2"  ));
			AddUI(m_pItem, TEXT("root"), TEXT("DP2"       ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataLo2"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataHi2"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputLo2" ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputHi2" ));
			EndGroup(TRUE);
			break;

		case 2:
			StartGroup(CString(IDS_NAME_OUT)+L" 3", 1, FALSE);
			AddUI(m_pItem, TEXT("root"), TEXT("OutType3"  ));
			AddUI(m_pItem, TEXT("root"), TEXT("DP3"       ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataLo3"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataHi3"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputLo3" ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputHi3" ));
			EndGroup(TRUE);
			break;

		case 3:
			StartGroup(CString(IDS_NAME_OUT)+L" 4", 1, FALSE);
			AddUI(m_pItem, TEXT("root"), TEXT("OutType4"  ));
			AddUI(m_pItem, TEXT("root"), TEXT("DP4"       ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataLo4"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("DataHi4"   ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputLo4" ));
			AddUI(m_pItem, TEXT("root"), TEXT("OutputHi4" ));
			EndGroup(TRUE);
			break;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Out4 Initial Output View
//

// Runtime Class

AfxImplementRuntimeClass(COut4InitOutWnd, CUIViewWnd);

// Overidables

void COut4InitOutWnd::OnAttach(void)
{
	m_pItem   = (COut4Conf *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("out4conf"));

	CUIViewWnd::OnAttach();
 	}

// UI Update

void COut4InitOutWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "InitData" ) {

		DoEnables();
		}

	CUIOut4Dynamic::CheckUpdate(m_pItem, Tag);
	}

void COut4InitOutWnd::OnUICreate(void)
{
	StartPage(1);

	AddInit();

	EndPage(TRUE);
	}

// UI Creation

void COut4InitOutWnd::AddInit(void)
{
	StartGroup(CString(IDS_MODULE_INIT), 1);
	AddUI(m_pItem, TEXT("root"), TEXT("InitData"));
	AddUI(m_pItem, TEXT("root"), TEXT("Data1"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("Data2"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("Data3"   ));
	AddUI(m_pItem, TEXT("root"), TEXT("Data4"   ));
	EndGroup(TRUE);
	}

// Enabling

void COut4InitOutWnd::DoEnables(void)
{
	BOOL fEnable = m_pItem->m_InitData;

	EnableUI("Data1", fEnable);
	EnableUI("Data2", fEnable);
	EnableUI("Data3", fEnable);
	EnableUI("Data4", fEnable);
	}

// End of File
