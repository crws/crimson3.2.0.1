
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4DigitalOutput_HPP

#define INCLUDE_DAMix4DigitalOutput_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output 
//

class CDAMix4DigitalOutput : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix4DigitalOutput(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_Output1;
	UINT m_Output2;
	UINT m_Output3;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
