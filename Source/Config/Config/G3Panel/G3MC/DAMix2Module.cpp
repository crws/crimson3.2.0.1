
#include "intern.hpp"

#include "DAMix2Module.hpp"

#include "DAMix2MainWnd.hpp"

#include "DAMix2AnalogInput.hpp"

#include "DAMix2AnalogInputConfig.hpp"

#include "DAMix2AnalogOutput.hpp"

#include "DAMix2AnalogOutputConfig.hpp"

#include "DAMix2DigitalInput.hpp"

#include "DAMix2DigitalInputConfig.hpp"

#include "DAMix2DigitalOutput.hpp"

#include "DAMix2DigitalOutputConfig.hpp"

#include "DAMixDigitalConfig.hpp"

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dauin6props.h"

#define TAG_DIGITAL (1 << 4)

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 2-Channel Mix Module
//

// Dynamic Class

AfxImplementDynamicClass(CDAMix2Module, CManticoreGenericModule);

// Constructor

CDAMix2Module::CDAMix2Module(void)
{
	m_pAnalogInput         = New CDAMix2AnalogInput;

	m_pAnalogInputConfig   = New CDAMix2AnalogInputConfig;

	m_pAnalogOutput        = New CDAMix2AnalogOutput;

	m_pAnalogOutputConfig  = New CDAMix2AnalogOutputConfig;

	m_pDigitalInput        = New CDAMix2DigitalInput;

	m_pDigitalInputConfig  = New CDAMix2DigitalInputConfig;

	m_pDigitalOutput       = New CDAMix2DigitalOutput;

	m_pDigitalOutputConfig = New CDAMix2DigitalOutputConfig;

	m_pDigitalConfig       = New CDAMixDigitalConfig(8);

	m_Ident         = LOBYTE(ID_DAMIX2);

	m_FirmID        = FIRM_DAMIX2;

	m_Model         = "MIX2";

	m_Power         = 36;
}

// UI Management

CViewWnd * CDAMix2Module::CreateMainView(void)
{
	return New CDAMix2MainWnd;
}

// Comms Object Access

UINT CDAMix2Module::GetObjectCount(void)
{
	return 9;
}

BOOL CDAMix2Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_INPUT;

			Data.Name  = L"Analog Inputs";

			Data.pItem = m_pAnalogInput;

			return TRUE;

		case 1:
			Data.ID    = OBJ_CONFIG_AI;

			Data.Name  = L"Analog Input Config";

			Data.pItem = m_pAnalogInputConfig;

			return TRUE;

		case 2:
			Data.ID    = OBJ_INPUT | TAG_DIGITAL;

			Data.Name  = L"Digital Inputs";

			Data.pItem = m_pDigitalInput;

			return TRUE;

		case 3:
			Data.ID    = OBJ_CONFIG_DI;

			Data.Name  = L"Digital Input Config";

			Data.pItem = m_pDigitalInputConfig;

			return TRUE;

		case 4:
			Data.ID    = OBJ_OUTPUT;

			Data.Name  = L"Analog Outputs";

			Data.pItem = m_pAnalogOutput;

			return TRUE;

		case 5:
			Data.ID    = OBJ_CONFIG_AO;

			Data.Name  = L"Analog Output Config";

			Data.pItem = m_pAnalogOutputConfig;

			return TRUE;

		case 6:
			Data.ID    = OBJ_OUTPUT | TAG_DIGITAL;

			Data.Name  = L"Digital Outputs";

			Data.pItem = m_pDigitalOutput;

			return TRUE;

		case 7:
			Data.ID    = OBJ_CONFIG_DO;

			Data.Name  = L"Digital Output Config";

			Data.pItem = m_pDigitalOutputConfig;

			return TRUE;

		case 8:
			Data.ID    = OBJ_GLOBAL;

			Data.Name  = L"Digital Config";

			Data.pItem = m_pDigitalConfig;

			return TRUE;
	}

	return FALSE;
}

// Conversion

BOOL CDAMix2Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		// Graphite

		if( m_pAnalogInputConfig->Convert(pValue->GetChild(L"Config")) ) {

			return TRUE;
		}

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CDAMix2Module::AddMetaData(void)
{
	Meta_AddObject(AnalogInput);
	Meta_AddObject(AnalogInputConfig);
	Meta_AddObject(AnalogOutput);
	Meta_AddObject(AnalogOutputConfig);
	Meta_AddObject(DigitalInput);
	Meta_AddObject(DigitalInputConfig);
	Meta_AddObject(DigitalOutput);
	Meta_AddObject(DigitalOutputConfig);
	Meta_AddObject(DigitalConfig);

	CGenericModule::AddMetaData();
}

// End of File
