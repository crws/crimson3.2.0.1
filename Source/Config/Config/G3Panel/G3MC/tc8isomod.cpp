
#include "intern.hpp"

#include "legacy.h"

#include "tc8isomod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modular Controller Configuration
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Isolated 8-Channel Thermocouple Module
//

// Dynamic Class

AfxImplementDynamicClass(CTC8ISOModule, CGenericModule);

// Constructor

CTC8ISOModule::CTC8ISOModule(void)
{
	m_Ident  = ID_CSTC8ISO;

	m_pInput = New CTC8Input;

	m_FirmID = FIRM_TC8ISO;

	m_Model  = "CSTC8ISO";
	}

// UI Management

CViewWnd * CTC8ISOModule::CreateMainView(void)
{
	return New CTC8ISOMainWnd;
	}

// Comms Object Access

UINT CTC8ISOModule::GetObjectCount(void)
{
	return 1;
	}

BOOL CTC8ISOModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_INP);
			
			Data.pItem = m_pInput;

			return TRUE;
		}

	return FALSE;
	}

// Implementation

void CTC8ISOModule::AddMetaData(void)
{
	Meta_AddObject(Input);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Isolated 8-Channel Thermocouple Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CTC8ISOMainWnd, CProxyViewWnd);

// Constructor

CTC8ISOMainWnd::CTC8ISOMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CTC8ISOMainWnd::OnAttach(void)
{
	m_pItem = (CTC8ISOModule *) CProxyViewWnd::m_pItem;

	AddTC8ISOPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CTC8ISOMainWnd::AddTC8ISOPages(void)
{
	CTC8Input *pInput = m_pItem->m_pInput;

	for( UINT n = 0; n < pInput->GetPageCount(); n++ ) {

		CViewWnd *pPage = pInput->CreatePage(n);

		m_pMult->AddView(pInput->GetPageName(n), pPage);

		pPage->Attach(pInput);
		}
	}

// End of File
