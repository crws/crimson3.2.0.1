
#include "intern.hpp"

#include "DAMix2DigitalInput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2DigitalInput, CCommsItem);

// Property List

CCommsList const CDAMix2DigitalInput::m_CommsList[] = {

	{ 1, "Input1",	PROPID_INPUT1,	usageRead,  IDS_NAME_I1  },
	{ 1, "Input2",	PROPID_INPUT2,	usageRead,  IDS_NAME_I2  },
	{ 1, "Input3",	PROPID_INPUT3,	usageRead,  IDS_NAME_I3  },
	{ 1, "Input4",	PROPID_INPUT4,	usageRead,  IDS_NAME_I4  },
	{ 1, "Input5",	PROPID_INPUT5,	usageRead,  IDS_NAME_I5  },
	{ 1, "Input6",	PROPID_INPUT6,	usageRead,  IDS_NAME_I6  },
	{ 1, "Input7",	PROPID_INPUT7,	usageRead,  IDS_NAME_I7  },
	{ 1, "Input8",	PROPID_INPUT8,	usageRead,  IDS_NAME_I8  },

};

// Constructor

CDAMix2DigitalInput::CDAMix2DigitalInput(void)
{
	m_Input1 = 0;
	m_Input2 = 0;
	m_Input3 = 0;
	m_Input4 = 0;
	m_Input5 = 0;
	m_Input6 = 0;
	m_Input7 = 0;
	m_Input8 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAMix2DigitalInput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Digital Inputs");
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDAMix2DigitalInput::AddMetaData(void)
{
	Meta_AddInteger(Input1);
	Meta_AddInteger(Input2);
	Meta_AddInteger(Input3);
	Meta_AddInteger(Input4);
	Meta_AddInteger(Input5);
	Meta_AddInteger(Input6);
	Meta_AddInteger(Input7);
	Meta_AddInteger(Input8);

	CCommsItem::AddMetaData();
}


// End of File
