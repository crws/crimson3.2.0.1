
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3MC_HPP
	
#define	INCLUDE_G3MC_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <g3comms.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3mc.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3MC

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3mc.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CGenericModule;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCommsPortRack;
class CCommsDeviceRack;
class CCommsSlotList;
class CCommsPortSlot;

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Port
//

class DLLAPI CCommsPortRack : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortRack(void);

		// Operations
		void UpgradeSlots(void);
		void MakeSlots(void);
		void RemapSlots(BOOL fForce);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Driver Creation
		ICommsDriver * CreateDriver(UINT uID);

		// Item Naming
		BOOL    IsHumanRoot(void) const;
		CString GetHumanName(void) const;
		CString GetItemOrdinal(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &File);
		void PostLoad(void);
		void Save(CTreeFile &File);

		// Conversion
		void PostConvert(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data
		CCommsSlotList * m_pSlots;

	protected:
		// Module Definition
		struct CModel {
			
			PCTXT m_pName;
			PCTXT m_pClass;
			};

		// Data
		CArray<UINT> m_Order;

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void MakeSlots(COptionCardRackItem *pItem);
		void MakeSlots(COptionCardList * pItem);
		void MakeSlots(COptionCardItem *pItem);
		void MigrateSlots(void);
		void AdjustSlotData(CArray <HGLOBAL> &Data, UINT uSlot);
		void LoadModules(CStringArray &List);
		void LoadModules(CStringArray &List, CModel *pType, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Slot List
//

class DLLAPI CCommsSlotList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsSlotList(void);

		// Item Access
		CCommsPortSlot * GetItem(INDEX Index) const;
		CCommsPortSlot * GetItem(UINT  uPos ) const;
		CCommsPortSlot * FindItem(CCommsDeviceRack *pRack) const;
		CCommsPortSlot * FindSlot(UINT uSlot) const;
		CCommsPortSlot * FindRack(UINT uRack) const;

		// Operations
		void SaveFixed (CTreeFile &File);
		void SaveTether(CTreeFile &File);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Slot
//

class DLLAPI CCommsPortSlot : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortSlot(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Attributes
		CCommsDeviceRack    * GetDevice(void) const;
		COptionCardItem     * GetOption(void) const;
		COptionCardRackItem * GetRack(void) const;
		UINT                  GetDrop(void) const;
		UINT 		      GetType(void) const;
		UINT		      GetJsonType(void) const;
		UINT		      GetPower(void) const;
		UINT                  GetSlot(void) const;

		// Operations
		void SetDevice(CCommsDeviceRack *pDev);
		void SetPort(COptionCardItem *pPort);
		void SetRack(COptionCardRackItem *pRack);
		void Upgrade(void);

		// Naming
		void    UpdateName(void);
		UINT    GetTreeImage(void) const;
		CString GetTreeLabel(void) const;
		CString GetHumanName(void) const;
		CString GetFixedName(void) const;

		// Persistance
		void Load(CTreeFile &Tree);
		void PostLoad(void);
		void Save(CTreeFile &Tree);

		// Conversion
		void PostConvert(void);

		// Download Support
		void PrepareData(void);

		// Item Properties	
		CString	m_Name;
		UINT    m_Dev;
		UINT	m_Slot;
		UINT    m_Rack;
		UINT    m_SlotNum;

	protected:
		// Data
		CCommsDeviceRack    * m_pDev;
		COptionCardItem     * m_pSlot;
		COptionCardRackItem * m_pRack;
		CString               m_RackPath;
		CString		      m_RackName;

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindDevice(void);
		void FindSlot(void);
		void FindRack(void);
		void FindName(void);
		void FindRackItem(void);
		void FindRackName(void);
		void FindRackPath(void);
		void UpdateSlot(void);
		void UpdateModule(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device List
//

class DLLAPI CCommsDeviceRackList : public CCommsDeviceList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsDeviceRackList(void);

		// Persistance
		void Init(void);
		void Load(CTreeFile &File);
		void PostLoad(void);
		void Save(CTreeFile &File);

		// Operations
		void SaveFixed (CTreeFile &File);
		void SaveTether(CTreeFile &File);

	protected:
		// Data
		CCommsSlotList * m_pSlots;

		// Implementation
		void FindSlots(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device
//

class DLLAPI CCommsDeviceRack : public CCommsDevice
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsDeviceRack(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		void SetClass(CLASS Class);

		// Naming
		CString GetHumanName(void) const;
		CString GetItemOrdinal(void) const;

		// Persistance
		void Init(void);
		void PostPaste(void);

		// Conversion
		BOOL PostConvert(CString const &Conv);

		// Data Members
		CGenericModule * m_pModule;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
