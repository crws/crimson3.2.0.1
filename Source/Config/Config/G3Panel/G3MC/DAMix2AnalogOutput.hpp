
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2AnalogOutput_HPP

#define INCLUDE_DAMix2AnalogOutput_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output
//

class CDAMix2AnalogOutput : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2AnalogOutput(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_Data1;
	UINT m_Data2;
	UINT m_Data3;
	UINT m_Data4;
	UINT m_Data5;
	UINT m_Data6;
	UINT m_Data7;
	UINT m_Data8;
	UINT m_InitData;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Property Filter
	BOOL IncludeProp(WORD PropID);

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
