
#include "intern.hpp"

#include "gmrc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/rcprops.h"

#include "import/graphite/rcdbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4 Analog Input Module Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteRCConfig, CCommsItem);

// Property List

CCommsList const CGraphiteRCConfig::m_CommsList[] = {

	{ 1, "TestProp1",	PROPID_RC_TESTPROP1,	usageRead,	IDS_NAME_P1	},
	{ 1, "TestProp2",	PROPID_RC_TESTPROP2,	usageRead,	IDS_NAME_P2	},

	};

// Constructor

CGraphiteRCConfig::CGraphiteRCConfig(void)
{
	m_TestProp1 = 0;
	m_TestProp2 = 0;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CGraphiteRCConfig::GetGroupName(WORD Group)
{
	switch( Group ) {
		
		case 1:	return CString(L"TestGroup");
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CGraphiteRCConfig::GetPageCount(void)
{
	return 1;
	}

CString CGraphiteRCConfig::GetPageName(UINT n)
{
	return CString(IDS_MODULE_CONFIG);
	}

CViewWnd * CGraphiteRCConfig::CreatePage(UINT n)
{
	return New CGraphiteRCConfigWnd;
	}

// Conversion

BOOL CGraphiteRCConfig::Convert(CPropValue *pValue)
{
	return FALSE;
	}

// Property Filter

BOOL CGraphiteRCConfig::IncludeProp(WORD PropID)
{
	return TRUE;
	}

// Meta Data Creation

void CGraphiteRCConfig::AddMetaData(void)
{
	Meta_AddInteger(TestProp1);
	Meta_AddInteger(TestProp2);

	CCommsItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CGraphiteRCConfigWnd, CUIViewWnd);

// Constructor

CGraphiteRCConfigWnd::CGraphiteRCConfigWnd(void)
{
	}

// Overidables

void CGraphiteRCConfigWnd::OnAttach(void)
{
	CUIViewWnd::OnAttach();

	m_pItem   = (CGraphiteRCConfig *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("gmrc"));

	CUIViewWnd::OnAttach();
	}

// UI Management

void CGraphiteRCConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	EndPage(TRUE);
	}

void CGraphiteRCConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	}

void CGraphiteRCConfigWnd::AddInputs(void)
{
	StartGroup(CString(IDS_MODULE_INPUTS), 1, FALSE);

	AddUI(m_pItem, TEXT("root"), CPrintf("TestProp1"));
	AddUI(m_pItem, TEXT("root"), CPrintf("TestProp2"));

	EndGroup(TRUE);
	}

// End of File
