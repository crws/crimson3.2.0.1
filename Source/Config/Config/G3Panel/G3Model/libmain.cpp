
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Model\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			CButtonBitmap *pCats16 = New CButtonBitmap( CBitmap(L"ToolCats16"),
								    CSize(16, 16),
								    22
								    );

			CButtonBitmap *pCats24 = New CButtonBitmap( CBitmap(L"ToolCats24"),
								    CSize(24, 24),
								    22
								    );

			afxButton->AppendBitmap(0x2000, pCats16);
			
			afxButton->AppendBitmap(0x2100, pCats24);

			AfxTouch(AfxRuntimeClass(CPageEditorWnd));

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Model\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
