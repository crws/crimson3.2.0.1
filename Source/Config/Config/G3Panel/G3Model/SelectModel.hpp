
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SelectModel_HPP

#define INCLUDE_SelectModel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

#include <G3Schema.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Select Model Dialog
//

class CSelectModelDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CSelectModelDialog(UINT uMode, CString const &Model);

	// Destructor
	~CSelectModelDialog(void);

	// Attributes
	CString GetModelDesc(void) const;

protected:
	// Data Members
	UINT         m_uMode;
	CStringArray m_ModelList;
	CTreeView *  m_pModelTree;
	IPxeModel *  m_pModel;
	HGLOBAL	     m_hImage;
	CStringArray m_Init;
	CStringArray m_Data;
	CString      m_Info;
	CString	     m_Part;
	CString      m_Image;
	CString	     m_Desc;
	CString	     m_Link;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	void OnFocusNotify(UINT uID, CWnd &Wnd);
	void OnPaint(void);

	// Notification Handlers
	void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
	void OnTreeDblClk(UINT uID, NMHDR &Info);
	void OnListSelChange(UINT uID, CWnd &Wnd);

	// Command Handlers
	BOOL OnOkay(UINT uID);
	BOOL OnLink(UINT uID);
	BOOL OnAuto(UINT uID);

	// Implementation
	void LoadModelList(void);
	void MakeModelTree(void);
	void LoadModelTree(void);
	void ModelTreeSelect(LPARAM lParam);
	void ShowPair(UINT n, bool show);
	void LoadVariants(CString const &Base, CString const &Variants);
	void LoadGroups(void);
	void LoadResolutions(void);
	void LoadSled(UINT s);
	void UpdateImageFromModel(void);
	void UpdateImageFromSled(UINT uSled);
	void UpdateImage(CString const &Image);
	void UpdateInfoFromModel(void);
	void UpdateInfoFromVariant(UINT uVariant);
	void UpdateInfoFromGroup(UINT uGroup);
	void UpdateInfoFromSled(UINT uSled);
	void UpdateInfoFromResource(ENTITY Entity);
	void UpdateInfo(CString const &Info);
	void FindGroupRect(CRect &Rect, UINT uID);
	void AdjustGroupRect(CRect &Rect);
	void PaintImageViaBitmap(CDC &DC, CRect Rect);
	void PaintInfoViaBitmap(CDC &DC, CRect Rect);
	void PaintImage(CDC &DC, CRect Rect, HGLOBAL hData);
	void PaintInfo(CDC &DC, CRect Rect);
	UINT FindInitValue(CString const &Name);
	void MakePartNumber(void);
	void DoEnables(void);
};

// End of File

#endif
