
#include "intern.hpp"

#include <g3et3.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Model Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Et3 System Item
//

class CEt3CommsItem : public CEt3CommsSystem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEt3CommsItem(void);

		// Overridables
		UINT GetModuleIdent(void);
		UINT GetModuleBase(void);
		UINT GetPhysDIs(void);
		UINT GetPhysDOs(void);
		UINT GetPhysAIs(void);
		UINT GetPhysAOs(void);
		UINT GetNumCounters(void);
		UINT GetNumHiSpeedCounters(void);
		UINT GetNumTPOs(void);
		
		CString GetAIClass(void);
		CString GetAOClass(void);
		CString GetDIClass(void);
		CString GetDOClass(void);

		// Download Config
		CString GetDownloadConfig(void) const;

	protected:
		// Data Members
		UINT	m_uModuleIdent;
		UINT	m_uModuleBase;
		UINT	m_uMaxDIs;
		UINT	m_uMaxDOs;
		UINT	m_uMaxAIs;
		UINT	m_uMaxAOs;
		UINT	m_uNumHSC;
		UINT	m_uNumCounters;
		UINT	m_uNumTPO;
		CString	m_AIClass;
		CString	m_AOClass;
		CString	m_DIClass;
		CString	m_DOClass;

		// Port Creation
		void MakePorts(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Et3 System Item
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsItem, CEt3CommsSystem);

// Constructor

CEt3CommsItem::CEt3CommsItem(void)
{
	m_uModuleIdent	= 0;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumCounters	= 0;

	m_uNumTPO	= 0;
	}

// Overridables

UINT CEt3CommsItem::GetModuleIdent(void)
{
	return m_uModuleIdent;
	}

UINT CEt3CommsItem::GetModuleBase(void)
{
	return m_uModuleBase;
	}

UINT CEt3CommsItem::GetPhysDIs(void)
{
	return m_uMaxDIs;
	}

UINT CEt3CommsItem::GetPhysDOs(void)
{
	return m_uMaxDOs;
	}

UINT CEt3CommsItem::GetPhysAIs(void)
{
	return m_uMaxAIs;
	}

UINT CEt3CommsItem::GetPhysAOs(void)
{
	return m_uMaxAOs;
	}

UINT CEt3CommsItem::GetNumHiSpeedCounters(void)
{
	return m_uNumHSC;
	}

UINT CEt3CommsItem::GetNumCounters(void)
{
	return m_uNumCounters;
	}

UINT CEt3CommsItem::GetNumTPOs(void)
{
	return m_uNumTPO;
	}

CString CEt3CommsItem::GetAIClass(void)
{
	return L"CAnalogInput" + m_AIClass + L"Item";
	}

CString CEt3CommsItem::GetAOClass(void)
{
	return L"CAnalogOutput" + m_AOClass + L"Item";
	}

CString CEt3CommsItem::GetDIClass(void)
{
	return L"CDiscreteInput" + m_DIClass + L"Item";
	}

CString CEt3CommsItem::GetDOClass(void)
{
	return L"CDiscreteOutput" + m_DOClass + L"Item";
	}

// Download Config

CString CEt3CommsItem::GetDownloadConfig(void) const
{
	return L"EtherTrak3";
	}

// Port Creation

void CEt3CommsItem::MakePorts(void)
{
	CEt3CommsPort *pPort = New CEt3CommsPortSerial;

	pPort->m_Name = CString(IDS_RS_COMMS_PORT_2);

	m_pComms->m_pPorts->AppendItem(pPort);
	}

//////////////////////////////////////////////////////////////////////////
//
// 24-Channel Discrete Input
// 8-Channel Discrete Output
// 8-Channel Analog Current Input
//

class CETMIX24880Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CETMIX24880Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 24-Channel Discrete Input
// 8-Channel Discrete Output
// 8-Channel Analog Current Input
//

// Dynamic Class

AfxImplementDynamicClass(CETMIX24880Item, CEt3CommsItem);

// Constructor

CETMIX24880Item::CETMIX24880Item(void)
{
	m_uModuleIdent	= E3_MOD_MIX24880;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 24;

	m_uMaxDOs	= 8;

	m_uMaxAIs	= 8;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 2;

	m_uNumCounters  = 16;

	m_uNumTPO	= 8;

	m_Flags.Insert(L"UseLast8");

	m_DOClass	= L"TPO";

	m_AIClass	= L"I";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");

	m_Flags.Insert(L"HasDIFiltering");

	m_Flags.Insert(L"HasDISourceSink");

	m_Flags.Insert(L"HasDICounters");

	m_Flags.Insert(L"HasDIHiSpeedCounters");
	}

//////////////////////////////////////////////////////////////////////////
//
// 24-Channel Discrete Input
// 8-Channel Discrete Output
// 8-Channel Analog Current Input
// 2-Channel Analog Current Output
//

class CETMIX24882Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CETMIX24882Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 24-Channel Discrete Input
// 8-Channel Discrete Output
// 8-Channel Analog Current Input
// 2-Channel Analog Current Output
//

// Dynamic Class

AfxImplementDynamicClass(CETMIX24882Item, CEt3CommsItem);

// Constructor

CETMIX24882Item::CETMIX24882Item(void)
{
	m_uModuleIdent	= E3_MOD_MIX24882;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 24;

	m_uMaxDOs	= 8;

	m_uMaxAIs	= 8;

	m_uMaxAOs	= 2;

	m_uNumHSC	= 2;

	m_uNumCounters  = 16;

	m_uNumTPO	= 8;

	m_Flags.Insert(L"UseLast8");

	m_DOClass	= L"TPO";

	m_AIClass	= L"I";

	m_AOClass	= L"Mix20882";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");

	m_Flags.Insert(L"HasDIFiltering");

	m_Flags.Insert(L"HasDISourceSink");

	m_Flags.Insert(L"HasDICounters");

	m_Flags.Insert(L"HasDIHiSpeedCounters");
	}

//////////////////////////////////////////////////////////////////////////
//
// 20-Channel Discrete Input
// 8-Channel Discrete Output
// 8-Channel Analog Current Input
// 4-Channel Analog Current Output
//

class CETMIX20884Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CETMIX20884Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 20-Channel Discrete Input
// 8-Channel Discrete Output
// 8-Channel Analog Current Input
// 4-Channel Analog Current Output
//

// Dynamic Class

AfxImplementDynamicClass(CETMIX20884Item, CEt3CommsItem);

// Constructor

CETMIX20884Item::CETMIX20884Item(void)
{
	m_uModuleIdent	= E3_MOD_MIX24884;

	m_uModuleBase	= E3_BASE_MX;

	m_uMaxDIs	= 20;

	m_uMaxDOs	= 8;

	m_uMaxAIs	= 8;

	m_uMaxAOs	= 4;

	m_uNumHSC	= 4;

	m_uNumCounters	= 12;

	m_uNumTPO	= 8;

	m_Flags.Insert(L"UseLast8");

	m_DOClass	= L"TPO";

	m_AIClass	= L"I";

	m_AOClass	= L"Mix20884";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");

	m_Flags.Insert(L"HasDIFiltering");

	m_Flags.Insert(L"HasDISourceSink");

	m_Flags.Insert(L"HasDICounters");

	m_Flags.Insert(L"HasDIHiSpeedCounters");
	}

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Discrete Input
//

class CET32DI24Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET32DI24Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Discrete Input
//

// Dynamic Class

AfxImplementDynamicClass(CET32DI24Item, CEt3CommsItem);

// Constructor

CET32DI24Item::CET32DI24Item(void)
{
	m_uModuleIdent	= E3_MOD_32DI24;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 32;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 0;

	m_uNumHSC	= 2;

	m_uNumCounters	= 32;

	m_uNumTPO	= 8;

	m_Flags.Insert(L"HasDIFiltering");

	m_Flags.Insert(L"HasDISourceSink");

	m_Flags.Insert(L"HasDICounters");

	m_Flags.Insert(L"HasDIHiSpeedCounters");
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Discrete Input
//

class CET16DI24Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16DI24Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Discrete Input
//

// Dynamic Class

AfxImplementDynamicClass(CET16DI24Item, CEt3CommsItem);

// Constructor

CET16DI24Item::CET16DI24Item(void)
{
	m_uModuleIdent	= E3_MOD_16DI24;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 16;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 2;

	m_uNumCounters	= 16;

	m_uNumTPO	= 0;

	m_Flags.Insert(L"HasDIFiltering");

	m_Flags.Insert(L"HasDICounters");

	m_Flags.Insert(L"HasDIHiSpeedCounters");
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Discrete AC Input
//

class CET16DIACItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16DIACItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Discrete AC Input
//

// Dynamic Class

AfxImplementDynamicClass(CET16DIACItem, CEt3CommsItem);

// Constructor

CET16DIACItem::CET16DIACItem(void)
{
	m_uModuleIdent	= E3_MOD_16DIAC;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 16;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumCounters	= 16;

	m_uNumTPO	= 0;

	m_Flags.Insert(L"UseDISlowFiltering");

	m_Flags.Insert(L"HasDICounters");
	}

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Discrete Output
//

class CET32DO24Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET32DO24Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Discrete Output
//

// Dynamic Class

AfxImplementDynamicClass(CET32DO24Item, CEt3CommsItem);

// Constructor

CET32DO24Item::CET32DO24Item(void)
{
	m_uModuleIdent	= E3_MOD_32DO24;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 32;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 32;

	m_DOClass	= L"TPO";	
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Discrete Output
//

class CET16DO24Item : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16DO24Item(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Discrete Output
//

// Dynamic Class

AfxImplementDynamicClass(CET16DO24Item, CEt3CommsItem);

// Constructor

CET16DO24Item::CET16DO24Item(void)
{
	m_uModuleIdent	= E3_MOD_16DO24;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 16;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 16;

	m_DOClass	= L"TPO";	
	}

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Analog Current Input
//

class CET32AI20MItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET32AI20MItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Analog Current Input
//

// Dynamic Class

AfxImplementDynamicClass(CET32AI20MItem, CEt3CommsItem);

// Constructor

CET32AI20MItem::CET32AI20MItem(void)
{
	m_uModuleIdent	= E3_MOD_32AI20M;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 32;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"I";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");

	m_Flags.Insert(L"HasAICurrentType");
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Analog Current Input
//

class CET16AI20MItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16AI20MItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Analog Current Input
//

// Dynamic Class

AfxImplementDynamicClass(CET16AI20MItem, CEt3CommsItem);

// Constructor

CET16AI20MItem::CET16AI20MItem(void)
{
	m_uModuleIdent	= E3_MOD_16AI20M;

	m_uModuleBase	= E3_BASE_2C;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 16;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"I";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");

	m_Flags.Insert(L"HasAICurrentType");
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Analog Current Output
//

class CET8AO20MItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET8AO20MItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Analog Current Output
//

// Dynamic Class

AfxImplementDynamicClass(CET8AO20MItem, CEt3CommsItem);

// Constructor

CET8AO20MItem::CET8AO20MItem(void)
{
	m_uModuleIdent	= E3_MOD_8AO20M;

	m_uModuleBase	= E3_BASE_2B;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 8;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AOClass	= L"AO20M";
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Analog Current Input
// 8-Channel Analog Current Output
//

class CET16AI8AOItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16AI8AOItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Analog Current Input
// 8-Channel Analog Current Output
//

// Dynamic Class

AfxImplementDynamicClass(CET16AI8AOItem, CEt3CommsItem);

// Constructor

CET16AI8AOItem::CET16AI8AOItem(void)
{
	m_uModuleIdent	= E3_MOD_16AI8AO;

	m_uModuleBase	= E3_BASE_2D;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 16;

	m_uMaxAOs	= 8;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"I";

	m_AOClass	= L"AO20M";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");
	}

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Analog Voltage Input
//

class CET32AI10VItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET32AI10VItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 32-Channel Analog Voltage Input
//

// Dynamic Class

AfxImplementDynamicClass(CET32AI10VItem, CEt3CommsItem);

// Constructor

CET32AI10VItem::CET32AI10VItem(void)
{
	m_uModuleIdent	= E3_MOD_32AI10V;

	m_uModuleBase	= E3_BASE_2A;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 32;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"V";

	m_Flags.Insert(L"HasAIResolution");

	m_Flags.Insert(L"HasAIIntegration");
	}

//////////////////////////////////////////////////////////////////////////
//
// 10-Channel RTD Input
//

class CET10RTDItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET10RTDItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 10-Channel RTD Input
//

// Dynamic Class

AfxImplementDynamicClass(CET10RTDItem, CEt3CommsItem);

// Constructor

CET10RTDItem::CET10RTDItem(void)
{
	m_uModuleIdent	= E3_MOD_10RTD;

	m_uModuleBase	= E3_BASE_1;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 10;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"RTD";

	m_Flags.Insert(L"HasRTDType");

	m_Flags.Insert(L"HasAITemperature");
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Isolated Thermocouple Input
//

class CET16ISOTCItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16ISOTCItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Isolated Thermocouple Input
//

// Dynamic Class

AfxImplementDynamicClass(CET16ISOTCItem, CEt3CommsItem);

// Constructor

CET16ISOTCItem::CET16ISOTCItem(void)
{
	m_uModuleIdent	= E3_MOD_16ISOTC;

	m_uModuleBase	= E3_BASE_TC;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 16;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"TC";

	m_Flags.Insert(L"HasTCType");

	m_Flags.Insert(L"HasAIIntegration");

	m_Flags.Insert(L"HasAITemperature");
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Isolated Current Input
//

class CET16ISO20MItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16ISO20MItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Isolated Current Input
//

// Dynamic Class

AfxImplementDynamicClass(CET16ISO20MItem, CEt3CommsItem);

// Constructor

CET16ISO20MItem::CET16ISO20MItem(void)
{
	m_uModuleIdent	= E3_MOD_16ISO20M;

	m_uModuleBase	= E3_BASE_20M;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 0;

	m_uMaxAIs	= 16;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	m_AIClass	= L"I";

	m_Flags.Insert(L"HasAIIntegration");
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Relay Output
//

class CET16DORLYItem : public CEt3CommsItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CET16DORLYItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-Channel Relay Output
//

// Dynamic Class

AfxImplementDynamicClass(CET16DORLYItem, CEt3CommsItem);

// Constructor

CET16DORLYItem::CET16DORLYItem(void)
{
	m_uModuleIdent	= E3_MOD_16DORLY;

	m_uModuleBase	= E3_BASE_1;

	m_uMaxDIs	= 0;

	m_uMaxDOs	= 16;

	m_uMaxAIs	= 0;

	m_uMaxAOs	= 0;

	m_uNumHSC	= 0;

	m_uNumTPO	= 0;

	//
	m_DOClass = L"Relay";
	}

// End of File
