
#include "intern.hpp"

#include "SelectModel.hpp"

#include "gdiplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Model Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <shlink.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Model APIs
//

// Externals

DLLAPI IPxeModel * Create_LinuxModelData(CString const &Model);

// Model Data

static PCTXT m_pList[] = {

	L"FlexEdge|DA50|0B",
	L"FlexEdge|DA70|0F,0G",
};

// Instantiator

extern "C" BOOL __declspec(dllexport) SelectModel(HWND hWnd, UINT uMode, CString &Model)
{
	CSelectModelDialog Dlg(uMode, Model);

	if( Dlg.Execute(CWnd::FromHandle(hWnd)) ) {

		Model = Dlg.GetModelDesc();

		return TRUE;
	}

	return FALSE;
}

extern "C" BOOL __declspec(dllexport) CheckModel(CString &Model)
{
	CStringArray List;

	if( Model.Tokenize(List, L'|') >= 4 ) {

		for( UINT n = 0; n < elements(m_pList); n++ ) {

			CStringArray Def;

			CString(m_pList[n]).Tokenize(Def, '|');

			if( List[0] == Def[1] ) {

				CStringArray Vars;

				Def[2].Tokenize(Vars, ',');

				for( UINT v = 0; v < Vars.GetCount(); v++ ) {

					if( List[1] == Vars[v] ) {

						return TRUE;
					}
				}
			}
		}
	}

	Model = L"DA70|0F|800x480|G=4";

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// Select Model Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSelectModelDialog, CStdDialog);

// Constructors

CSelectModelDialog::CSelectModelDialog(UINT uMode, CString const &Model)
{
	m_uMode      = uMode;

	m_pModelTree = New CTreeView;

	m_pModel     = NULL;

	m_hImage     = NULL;

	Model.Tokenize(m_Init, L'|');

	if( m_uMode == 0 || m_uMode == 1 ) {

		if( m_uMode == 1 ) {

			if( m_Init.GetCount() == 5 ) {

				m_Init.SetAt(3, m_Init[3] + L',' + m_Init[4]);
			}
		}
	}
	else {
		if( m_Init[1].StartsWith(L"DA") ) {

			m_Init.SetAt(0, m_Init[1].Left(4));

			m_Init.SetAt(1, m_Init[1].Mid(5, 2));
		}
		else {
			if( FindInitValue(L"h") == 70 ) {

				m_Init.SetAt(0, L"DA70");

				m_Init.SetAt(1, L"0F");
			}
			else {
				m_Init.SetAt(0, L"DA50");

				m_Init.SetAt(1, L"0B");
			}
		}
	}

	SetName(L"SelectModelDlg");
}

// Destructor

CSelectModelDialog::~CSelectModelDialog(void)
{
	if( m_hImage ) {

		GlobalFree(m_hImage);
	}

	AfxRelease(m_pModel);
}

// Attributes

CString CSelectModelDialog::GetModelDesc(void) const
{
	return m_Desc;
}

// Message Map

AfxMessageMap(CSelectModelDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(3000, OnLink)
	AfxDispatchCommand(3002, OnAuto)

	AfxDispatchNotify(200, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(200, NM_DBLCLK, OnTreeDblClk)
	AfxDispatchNotify(2001, LBN_SELCHANGE, OnListSelChange)
	AfxDispatchNotify(2003, LBN_SELCHANGE, OnListSelChange)
	AfxDispatchNotify(2005, LBN_SELCHANGE, OnListSelChange)
	AfxDispatchNotify(2007, LBN_SELCHANGE, OnListSelChange)
	AfxDispatchNotify(2009, LBN_SELCHANGE, OnListSelChange)
	AfxDispatchNotify(2011, LBN_SELCHANGE, OnListSelChange)

	AfxMessageEnd(CSelectModelDialog)
};

// Message Handlers

BOOL CSelectModelDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	switch( m_uMode ) {

		case 0:
			SetWindowText(IDS("New Database"));
			break;

		case 1:
			SetWindowText(IDS("Convert Database"));
			break;

		case 2:
			SetWindowText(IDS("Select Target Model for Import"));
			break;
	}

	LoadModelList();

	MakeModelTree();

	LoadModelTree();

	m_Init.Empty();

	m_pModelTree->SetFocus();

	return FALSE;
}

void CSelectModelDialog::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID == 3000 ) {

		return;
	}

	if( uID >= 2007 && uID <= 2011 ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

		UINT uSled = Combo.GetCurSelData();

		UpdateInfoFromSled(uSled);

		UpdateImageFromSled(uSled);
	}
	else {
		switch( uID ) {

			case 2001:
			{
				CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

				UINT uVariant = Combo.GetCurSelData();

				UpdateInfoFromVariant(uVariant);
			}
			break;

			case 2003:
			{
				CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

				UINT uGroup = Combo.GetCurSelData();

				UpdateInfoFromGroup(uGroup);
			}
			break;

			default:
			{
				UpdateInfoFromModel();
			}
			break;
		}

		UpdateImageFromModel();
	}
}

void CSelectModelDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect1, Rect2;

	FindGroupRect(Rect1, 1003);

	FindGroupRect(Rect2, 1002);

	PaintImageViaBitmap(DC, Rect1);

	PaintInfoViaBitmap(DC, Rect2);
}

// Notification Handlers

void CSelectModelDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	ModelTreeSelect(Info.itemNew.lParam);
}

void CSelectModelDialog::OnTreeDblClk(UINT uID, NMHDR &Info)
{
}

void CSelectModelDialog::OnListSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 2003 ) {

		DoEnables();
	}

	if( uID == 2001 ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

		UINT uVariant = Combo.GetCurSelData();

		UpdateInfoFromVariant(uVariant);
	}

	if( uID == 2003 ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

		UINT uGroup = Combo.GetCurSelData();

		UpdateInfoFromGroup(uGroup);
	}

	if( uID >= 2007 && uID <= 2011 ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

		UINT uSled = Combo.GetCurSelData();

		UpdateImageFromSled(uSled);

		UpdateInfoFromSled(uSled);
	}

	MakePartNumber();
}

// Command Handlers

BOOL CSelectModelDialog::OnOkay(UINT uID)
{
	if( !m_Data.IsEmpty() ) {

		m_Desc  = m_Data[1];

		m_Desc += L'|';

		UINT uVariant = ((CComboBox &) GetDlgItem(2001)).GetCurSelData();

		UINT uGroup   = ((CComboBox &) GetDlgItem(2003)).GetCurSelData();

		UINT dwSize   = ((CComboBox &) GetDlgItem(2005)).GetCurSelData();

		if( uVariant < NOTHING ) {

			CStringArray List;

			m_Data[2].Tokenize(List, L',');

			m_Desc += List[uVariant];
		}

		m_Desc += L'|';

		m_Desc += CPrintf("%ux%u", LOWORD(dwSize), HIWORD(dwSize));

		m_Desc += L'|';

		m_Desc += CPrintf("g=%u", uGroup);

		UINT ns = m_pModel->GetObjCount('x');

		BOOL in = FALSE;

		for( UINT s = 0; s < ns; s++ ) {

			UINT uSled = ((CComboBox &) GetDlgItem(2007 + 2 * s)).GetCurSelData();

			if( uSled ) {

				m_Desc += in ? L',' : L'|';

				m_Desc += CPrintf("s%u=%u", s, uSled);

				in = TRUE;
			}
		}

		m_Desc.MakeUpper();

		EndDialog(TRUE);

		return TRUE;
	}

	return FALSE;
}

BOOL CSelectModelDialog::OnLink(UINT uID)
{
	ShellExecute(ThisObject,
		     L"open",
		     m_Link,
		     NULL,
		     NULL,
		     SW_SHOWNORMAL
	);

	return TRUE;
}

BOOL CSelectModelDialog::OnAuto(UINT uID)
{
	CString Model;

	if( Link_DetectModel(Model) ) {

		Model.Tokenize(m_Init, L'|');

		HTREEITEM hRoot = m_pModelTree->GetRoot();

		HTREEITEM hScan = m_pModelTree->GetChild(hRoot);

		while( hScan ) {

			if( m_pModelTree->GetItemText(hScan) == m_Init[0] ) {

				LPARAM lParam = m_pModelTree->GetItemParam(hScan);

				ModelTreeSelect(lParam);

				break;
			}

			hScan = m_pModelTree->GetNextItem(hScan, TVGN_NEXT);
		}

		m_Init.Empty();

		m_pModelTree->SetFocus();
	}

	return TRUE;
}

// Implementation

void CSelectModelDialog::LoadModelList(void)
{
	for( UINT n = 0; n < elements(m_pList); n++ ) {

		m_ModelList.Append(m_pList[n]);
	}
}

void CSelectModelDialog::MakeModelTree(void)
{
	CRect Rect;

	FindGroupRect(Rect, 1000);

	DWORD dwStyle = TVS_HASBUTTONS
		| TVS_NOTOOLTIPS
		| TVS_SHOWSELALWAYS
		| TVS_DISABLEDRAGDROP
		| TVS_HASLINES
		| TVS_LINESATROOT;

	m_pModelTree->Create(dwStyle | WS_VISIBLE | WS_TABSTOP,
			     Rect,
			     ThisObject,
			     200
	);
}

void CSelectModelDialog::LoadModelTree(void)
{
	m_pModelTree->DeleteChildren(NULL);

	CString   Family;

	HTREEITEM hRoot = NULL;

	HTREEITEM hInit = NULL;

	for( UINT n = 0; n < m_ModelList.GetCount(); n++ ) {

		CStringArray Data;

		m_ModelList[n].Tokenize(Data, L'|');

		if( Family != Data[0] ) {

			if( hRoot ) {

				m_pModelTree->Expand(hRoot, TVE_EXPAND);
			}

			CTreeViewItem Node;

			Node.SetText(Data[0]);

			Node.SetParam(NOTHING);

			hRoot  = m_pModelTree->InsertItem(NULL, NULL, Node);

			Family = Data[0];
		}

		CTreeViewItem Node;

		Node.SetText(Data[1]);

		Node.SetParam(n);

		HTREEITEM hItem = m_pModelTree->InsertItem(hRoot, NULL, Node);

		if( !hInit ) {

			if( m_Init.GetCount() < 1 || m_Init[0] == Data[1] ) {

				hInit = hItem;
			}
		}
	}

	if( hRoot ) {

		m_pModelTree->Expand(hRoot, TVE_EXPAND);
	}

	if( hInit ) {

		m_pModelTree->SelectItem(hInit);
	}
}

void CSelectModelDialog::ModelTreeSelect(LPARAM lParam)
{
	if( lParam == NOTHING ) {

		m_Data.Empty();

		for( UINT p = 0; p < 6; p++ ) {

			ShowPair(p, false);
		}

		AfxRelease(m_pModel);

		m_pModel = NULL;

		UpdateImageFromModel();

		UpdateInfoFromModel();
	}
	else {
		m_Data.Empty();

		m_ModelList[lParam].Tokenize(m_Data, L'|');

		AfxRelease(m_pModel);

		m_pModel = Create_LinuxModelData(m_Data[1]);

		LoadVariants(m_Data[1], m_Data[2]);

		LoadGroups();

		LoadResolutions();

		UINT ns = m_pModel->GetObjCount('x');

		for( UINT p = 0; p < 6; p++ ) {

			ShowPair(p, p < 3 + ns);
		}

		for( UINT s = 0; s < ns; s++ ) {

			LoadSled(s);
		}

		UpdateImageFromModel();

		UpdateInfoFromModel();
	}

	MakePartNumber();

	DoEnables();
}

void CSelectModelDialog::ShowPair(UINT n, bool show)
{
	GetDlgItem(2000 + 2*n).ShowWindow(show ? SW_SHOW : SW_HIDE);

	GetDlgItem(2001 + 2*n).ShowWindow(show ? SW_SHOW : SW_HIDE);
}

void CSelectModelDialog::LoadVariants(CString const &Base, CString const &Variants)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2001);

	CStringArray List;

	Variants.Tokenize(List, L',');

	Combo.ResetContent();

	UINT uInit = NOTHING;

	if( List.GetCount() ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			Combo.AddString(Base + L'A' + List[n], n);

			if( uInit == NOTHING ) {

				if( m_Init.GetCount() < 2 || m_Init[1] == List[n] ) {

					uInit = n;
				}
			}
		}
	}
	else {
		Combo.AddString(Base, NOTHING);
	}

	Combo.SelectData(uInit);
}

void CSelectModelDialog::LoadGroups(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2003);

	UINT uMask = m_pModel->GetObjCount('g');

	INT  nInit = -1;

	UINT uFind = FindInitValue(L"g");

	if( Combo.GetCount() ) {

		if( m_Init.IsEmpty() ) {

			UINT uPrev = Combo.GetCurSelData();
			
			if( uMask & (1 << uPrev) ) {

				uFind = uPrev;
			}
		}

		Combo.ResetContent();
	}

	for( UINT n = 0; n < 6; n++ ) {

		if( uMask & (1 << n) ) {

			CString Text;

			switch( n ) {

				case 0:
					Text = IDS("Group 1 - Advanced Networking Only");
					break;

				case 1:
					Text = IDS("Group 2 - Crimson Protocol Conversion");
					break;

				case 2:
				case 3:
				case 4:
					Text = IDS("Group 3 - Full Crimson Functionality");
					break;

				case 5:
					Text = IDS("Group 4 - Crimson plus Crimson Control");
					break;
			}

			if( n <= 4 ) {

				MakeMax(nInit, INT(n));
			}

			Combo.AddString(Text, n);
		}
	}

	Combo.SelectData((uFind < NOTHING) ? DWORD(uFind) : DWORD(nInit));
}

void CSelectModelDialog::LoadResolutions(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

	CLongArray List;

	m_pModel->GetDispList(List);

	DWORD dwInit = 0;

	if( Combo.GetCount() ) {
		
		if( m_Init.IsEmpty() ) {

			dwInit = Combo.GetCurSelData();
		}

		Combo.ResetContent();
	}

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		int cx = LOWORD(List[n]);

		int cy = HIWORD(List[n]);

		CString Text;

		Text.Printf(L"%u by %u", cx, cy);

		if( List.GetCount() <= 2 ) {

			if( n == 1 ) {

				Text += IDS(" Emulated");
			}
		}
		else {
			Text = IDS("Virtual ") + Text;
		}

		if( !dwInit ) {

			if( m_Init.GetCount() >= 3 && m_Init[2] == CPrintf("%ux%u", cx, cy) ) {

				dwInit = List[n];
			}
		}

		Combo.AddString(Text, List[n]);
	}

	Combo.SelectData(dwInit ? dwInit : MAKELONG(800, 480));
}

void CSelectModelDialog::LoadSled(UINT s)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2007 + 2 * s);

	UINT uFind = FindInitValue(CPrintf("s%u", s));

	if( Combo.GetCount() ) {

		if( m_Init.IsEmpty() ) {

			uFind = Combo.GetCurSelData();
		}

		Combo.ResetContent();
	}

	Combo.AddString(IDS("Not Fitted"), 0);

	Combo.AddString(IDS("Cellular Modem"), 100);

	Combo.AddString(IDS("Dual Ethernet"), 101);

	Combo.AddString(IDS("Wi-Fi Interface"), 102);

	Combo.AddString(IDS("Dual RS-232"), 103);

	Combo.AddString(IDS("Dual RS-485"), 104);

	Combo.AddString(IDS("Mixed Serial"), 105);

	Combo.SelectData((uFind == NOTHING) ? 0 : uFind);
}

void CSelectModelDialog::UpdateImageFromModel(void)
{
	if( m_Data.IsEmpty() ) {

		UpdateImage(L"");
	}
	else {
		UpdateImage(CPrintf(L"model-%s", PCTXT(m_Data[1])));
	}
}

void CSelectModelDialog::UpdateImageFromSled(UINT uSled)
{
	if( uSled ) {

		UpdateImage(CPrintf(L"sled-%u", uSled));
	}
	else {
		UpdateImageFromModel();
	}
}

void CSelectModelDialog::UpdateImage(CString const &Image)
{
	if( m_Image != Image ) {

		if( m_hImage ) {

			GlobalFree(m_hImage);

			m_hImage = NULL;
		}

		if( !(m_Image = Image).IsEmpty() ) {

			UINT    uFrom = 0;

			HGLOBAL hFrom = afxModule->LoadResource(PTXT(PCTXT(m_Image)), L"PNG", uFrom);

			HGLOBAL hData = GlobalAlloc(GHND, uFrom);

			PBYTE   pFrom = PBYTE(LockResource(hFrom));

			PBYTE   pData = PBYTE(GlobalLock(hData));

			memcpy(pData, pFrom, uFrom);

			UnlockResource(hFrom);

			FreeResource(hFrom);

			GlobalUnlock(hData);

			m_hImage = hData;
		}

		CRect Rect;

		FindGroupRect(Rect, 1003);

		Invalidate(Rect, FALSE);
	}
}

void CSelectModelDialog::UpdateInfoFromModel(void)
{
	if( m_Data.IsEmpty() ) {

		UpdateInfo(L"");
	}
	else {
		CString Name = m_Data[1] + L"_Model";

		UpdateInfoFromResource(CEntity(Name));
	}
}

void CSelectModelDialog::UpdateInfoFromVariant(UINT uVariant)
{
	CStringArray List;

	m_Data[2].Tokenize(List, L',');

	CString Name = m_Data[1] + L'_' + List[uVariant] + L"_Variant";

	UpdateInfoFromResource(CEntity(Name));
}

void CSelectModelDialog::UpdateInfoFromGroup(UINT uGroup)
{
	switch( uGroup ) {

		case 0:
			UpdateInfoFromResource(CEntity(L"1_Group"));
			break;

		case 1:
			UpdateInfoFromResource(CEntity(L"2_Group"));
			break;

		case 2:
		case 3:
		case 4:
			UpdateInfoFromResource(CEntity(L"3_Group"));
			break;

		case 5:
			UpdateInfoFromResource(CEntity(L"4_Group"));
			break;
	}
}

void CSelectModelDialog::UpdateInfoFromSled(UINT uSled)
{
	if( uSled ) {

		CPrintf Name(L"%u_Sled", uSled);

		UpdateInfoFromResource(CEntity(Name));
	}
	else {
		UpdateInfoFromModel();
	}
}

void CSelectModelDialog::UpdateInfoFromResource(ENTITY Entity)
{
	HANDLE hInfo = afxModule->LoadResource(Entity, RT_RCDATA);

	if( hInfo ) {

		CString Text = PCTXT(LockResource(hInfo));

		UpdateInfo(Text);

		UnlockResource(hInfo);
	}
}

void CSelectModelDialog::UpdateInfo(CString const &Info)
{
	if( m_Info != Info ) {

		m_Info = Info;

		CRect Rect;

		FindGroupRect(Rect, 1002);

		Invalidate(Rect, FALSE);
	}
}

void CSelectModelDialog::FindGroupRect(CRect &Rect, UINT uID)
{
	Rect = GetDlgItem(uID).GetWindowRect();

	ScreenToClient(Rect);

	AdjustGroupRect(Rect);
}

void CSelectModelDialog::AdjustGroupRect(CRect &Rect)
{
	Rect.left   += 8;
	Rect.top    += 20;
	Rect.right  -= 8;
	Rect.bottom -= 8;
}

void CSelectModelDialog::PaintImageViaBitmap(CDC &DC, CRect Rect)
{
	if( m_hImage ) {

		CSize Size(Rect.GetSize());

		CRect Draw(Size);

		CMemoryDC MC(DC);

		CBitmap   BM(DC, Size);

		MC.Select(BM);

		MC.FillRect(Size, afxBrush(WHITE));

		if( m_Image.StartsWith(L"sled-") ) {

			Draw -= 60;
		}

		PaintImage(MC, Draw, m_hImage);

		DC.BitBlt(Rect, MC, CPoint(0, 0), SRCCOPY);

		return;
	}

	DC.FillRect(Rect, afxBrush(WHITE));
}

void CSelectModelDialog::PaintInfoViaBitmap(CDC &DC, CRect Rect)
{
	if( !m_Info.IsEmpty() ) {

		CSize Size(Rect.GetSize());

		CRect Draw(Size);

		CMemoryDC MC(DC);

		CBitmap   BM(DC, Size);

		MC.Select(BM);

		MC.FillRect(Size, afxBrush(TabFace));

		PaintInfo(MC, Draw);

		DC.BitBlt(Rect, MC, CPoint(0, 0), SRCCOPY);

		return;
	}

	DC.FillRect(Rect, afxBrush(TabFace));

	m_Link.Empty();

	GetDlgItem(3000).ShowWindow(FALSE);
}

void CSelectModelDialog::PaintImage(CDC &DC, CRect Rect, HGLOBAL hData)
{
	IStream *pStream = NULL;

	CreateStreamOnHGlobal(hData, FALSE, &pStream);

	if( pStream ) {

		GpImage *pImage  = NULL;

		GdipLoadImageFromStream(pStream, &pImage);

		if( pImage ) {

			int ix, iy;

			GdipGetImageWidth(pImage, (UINT *) &ix);

			GdipGetImageHeight(pImage, (UINT *) &iy);

			Rect.MatchAspect(CSize(ix, iy));

			GpGraphics *pGraph = NULL;

			GdipCreateFromHDC(DC.GetHandle(), &pGraph);

			GdipDrawImageRectI(pGraph,
					   pImage,
					   Rect.left,
					   Rect.top,
					   Rect.cx(),
					   Rect.cy()
			);

			GdipDeleteGraphics(pGraph);

			GdipDisposeImage(pImage);
		}

		pStream->Release();
	}
}

void CSelectModelDialog::PaintInfo(CDC &DC, CRect Rect)
{
	DC.Select(afxFont(Bolder));

	int xp = Rect.left;

	int yp = Rect.top;

	int cx = DC.GetTextExtent(L"X").cx + 4;

	int cy = DC.GetTextExtent(L"X").cy + 2;

	DC.SetBkMode(TRANSPARENT);

	CStringArray List;

	m_Info.Tokenize(List, '\n');

	if( List.GetCount() ) {

		UINT link = List.GetCount() - 1;

		if( List[link].StartsWith(L"https://") ) {

			GetDlgItem(3000).ShowWindow(TRUE);

			m_Link = List[link];

			List.Remove(link);
		}
		else {
			m_Link.Empty();

			GetDlgItem(3000).ShowWindow(FALSE);
		}
	}
	else {
		m_Link.Empty();

		GetDlgItem(3000).ShowWindow(FALSE);
	}

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		CString Info = List[n];

		bool    init = true;

		while( !Info.IsEmpty() ) {

			CString Line = Info.StripToken('|');

			if( n ) {

				if( init ) {

					DC.TextOut(xp, yp - (n ? 1 : 0), L"\x25CF");

					init = false;
				}

				DC.TextOut(xp + cx, yp, Line);
			}
			else {
				DC.TextOut(xp, yp, Line);

				yp += 8;
			}

			yp += cy;
		}

		if( n == 0 ) {

			DC.Replace(afxFont(Dialog));
		}
	}

	DC.Deselect();
}

UINT CSelectModelDialog::FindInitValue(CString const &Name)
{
	if( m_Init.GetCount() >= 4 ) {

		CStringArray List;

		m_Init[3].Tokenize(List, L',');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString Data = List[n];

			CString Item = Data.StripToken('=');

			if( Name == Item ) {

				return watoi(Data);
			}
		}
	}

	return NOTHING;
}

void CSelectModelDialog::MakePartNumber(void)
{
	if( m_pModel ) {

		m_Part = m_Data[1] + L"A-";

		UINT uVariant = ((CComboBox &) GetDlgItem(2001)).GetCurSelData();

		UINT uGroup   = ((CComboBox &) GetDlgItem(2003)).GetCurSelData();

		if( uVariant < NOTHING ) {

			CStringArray List;

			m_Data[2].Tokenize(List, L',');

			m_Part += List[uVariant];

			m_Part += L'-';
		}

		UINT ns = m_pModel->GetObjCount('x');

		for( UINT s = 0; s < 3; s++ ) {

			if( s < ns ) {

				UINT uSled = ((CComboBox &) GetDlgItem(2007 + 2 * s)).GetCurSelData();

				switch( uSled ) {

					case 0:
						m_Part += L"NN";
						break;

					case 100:
						m_Part += L"1N";
						break;

					case 101:
						m_Part += L"5E";
						break;

					case 102:
						m_Part += L"4A";
						break;

					case 103:
						m_Part += L"5A";
						break;

					case 104:
						m_Part += L"5B";
						break;

					case 105:
						m_Part += L"5C";
						break;

					default:
						m_Part += L"??";
						break;
				}
			}
			else {
				m_Part += L"00";
			}
		}

		m_Part += L"-0";

		switch( uGroup ) {

			case 0:
				m_Part += L'1';
				break;

			case 1:
				m_Part += L'2';
				break;

			case 2:
			case 3:
			case 4:
				m_Part += L'3';
				break;

			case 5:
				m_Part += L'4';
				break;
		}

		m_Part += L"0";
	}
	else {
		m_Part.Empty();
	}

	GetDlgItem(3001).SetWindowText(m_Part);
}

void CSelectModelDialog::DoEnables(void)
{
	UINT uGroup = ((CComboBox &) GetDlgItem(2003)).GetCurSelData();

	GetDlgItem(2005).EnableWindow(uGroup > 1);

	GetDlgItem(IDOK).EnableWindow(!m_Data.IsEmpty());
}

// End of File
