
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Colorado Panel Framer
//

class CColoradoFramer : public IFramer
{
	public:
		// Constructor
		CColoradoFramer(void);

		// Management
		void Release(void);

		// Attributes
		int    GetTotalKeys(void) const;
		CRect  GetFrameRect(void) const;
		CSize  GetTotalSize(void) const;
		HRGN   GetClipRegion(int nScale) const;
		CRect  GetKeyRect(int nKey) const;
		UINT   GetKeyCode(int nKey) const;
		CColor GetKeyColor(UINT uCode) const;

		// Operations
		void FindLayout(CSize Size, int nKeys);
		void DrawFrame(CDC &DC, BOOL fWarn, BOOL fSim);
		void DrawKeyBorder(CDC &DC, CRect Rect);
		void DrawKeyText(CDC &DC, CRect Rect, CString Text);
		void DrawKeyIcon(CDC &DC, CRect Rect, UINT uCode);

	protected:
		// Coloring
		CBrush m_Panel;
		CBrush m_Middle;
		CBrush m_Strip;
		CPen   m_Border;

		// Layout Data
		int     m_nKeys;
		int	m_nScale;
		int	m_nPad;
		CSize	m_DispSize;
		CRect	m_FrameRect;
		CSize   m_FrameSize;
		CSize   m_TotalSize;
		CFont   m_FontKeys;
		CFont   m_FontLogo;
		CFont   m_FontFunc;
		CFont   m_FontText;
		CFont	m_FontWarn;
		CRect   m_FramePanel;
		CRect   m_FrameMiddle;
		CRect   m_FrameDisplay;
		CRect	m_FrameStrip;
		CRect	m_FrameKeys;
		CPoint	m_FrameLogo;

		// Implementation
		void MakeTools(void);
		void FindLayoutKeys(void);
		void FindLayoutNone(void);
		void FindLayoutLogo(void);
		void FindScale(void);
		int  FrameScale(int n) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Colorado Panel Framer
//

// Instantiator

IFramer * Create_ColoradoFramer(void)
{
	return New CColoradoFramer;
	}

// Constructor

CColoradoFramer::CColoradoFramer(void)
{
	MakeTools();
	}

// Management

void CColoradoFramer::Release(void)
{
	delete this;
	}

// Attributes

int CColoradoFramer::GetTotalKeys(void) const
{
	return m_nKeys;
	}

CRect CColoradoFramer::GetFrameRect(void) const
{
	return m_FrameRect;
	}

CSize CColoradoFramer::GetTotalSize(void) const
{
	return m_TotalSize;
	}

HRGN CColoradoFramer::GetClipRegion(int nScale) const
{
	return CreateRoundRectRgn( 0+MulDiv(nScale, m_FramePanel.left  , 100),
				   0+MulDiv(nScale, m_FramePanel.top   , 100),
				   1+MulDiv(nScale, m_FramePanel.right , 100),
				   1+MulDiv(nScale, m_FramePanel.bottom, 100),
				   0+MulDiv(nScale, FrameScale(3)      , 100),
				   0+MulDiv(nScale, FrameScale(3)      , 100)
				   );
	}

CRect CColoradoFramer::GetKeyRect(int nKey) const
{
	CRect Key;

	if( nKey < m_nKeys ) {

		Key = m_FrameKeys;

		Key.left   += ((m_FrameKeys.cx() - Key.cy()) / 2) + (Key.cy() * 2) * (nKey + 2 - m_nKeys);

		Key.right  = Key.left            + Key.cy();
		}

	return Key;
	}

UINT CColoradoFramer::GetKeyCode(int nKey) const
{
	if( nKey == m_nKeys - 1 ) {

		return COPS_VK_MENU;
		}

	if( nKey <= m_nKeys - 2 ) {

		return COPS_VK_SOFT1 + nKey;
		}

	return 0;
	}

CColor CColoradoFramer::GetKeyColor(UINT uCode) const
{
	switch( uCode ) {

		case COPS_VK_SOFT1:
			return CColor(128,32,0);

		case COPS_VK_SOFT2:
			return CColor(0,32,128);

		case COPS_VK_SOFT3:
			return CColor(0,128+32,0);

		case COPS_VK_MENU:
			return CColor(0,128+32,0);

		default:
			return afxColor(BLACK);
		}	
	}

// Operations

void CColoradoFramer::FindLayout(CSize Size, int nKeys)
{
	m_DispSize  = Size;

	m_nKeys     = nKeys;

	FindScale();

	// cppcheck-suppress duplicateExpressionTernary
	m_FrameRect = CRect( FrameScale(m_nKeys ?  6 : 6) + m_nPad,
			     FrameScale(m_nKeys ?  8 : 7) + m_nPad,
	// cppcheck-suppress duplicateExpressionTernary
			     FrameScale(m_nKeys ?  6 : 6) + m_nPad,
			     FrameScale(m_nKeys ? 10 : 7) + m_nPad
			     );

	m_FrameSize.cx = m_FrameRect.left + m_FrameRect.right;
	
	m_FrameSize.cy = m_FrameRect.top  + m_FrameRect.bottom;

	m_TotalSize    = m_DispSize + m_FrameSize;

	if( m_nKeys ) {

		FindLayoutKeys();
		}
	else
		FindLayoutNone();

	UINT n = 6 * CString(IDS_NOTE_EDITING_IS).GetLength() / 11;

	MakeMax(n, 30);

	m_FontKeys.Create(L"rlc31",   CSize(0, FrameScale(4)      ), FALSE);

	m_FontText.Create(L"Arial",   CSize(0, FrameScale(5)  /  4), FALSE);

	m_FontFunc.Create(L"Arial",   CSize(0, FrameScale(5)  /  2),  TRUE);

	m_FontLogo.Create(L"rlc31",   CSize(0, FrameScale(4)      ), FALSE);

	m_FontWarn.Create(afxDlgFont, CSize(0, m_TotalSize.cy /  n), FALSE);
	}

void CColoradoFramer::DrawFrame(CDC &DC, BOOL fWarn, BOOL fSim)
{
	DC.Select(m_Border);

	DC.Select(m_Panel);

	DC.RoundRect(m_FramePanel,  FrameScale(3));

	DC.Deselect();

	DC.Deselect();

	////////

	DC.Select(CGdiObject(NULL_PEN));

	DC.Select(m_Middle);

	DC.RoundRect(m_FrameMiddle, FrameScale(2));

	DC.Replace(m_Strip);

	DC.RoundRect(m_FrameStrip,  FrameScale(2));

	DC.Deselect();

	DC.Deselect();

	DC.FrameRect(m_FrameDisplay, afxColor(WHITE));

	if( fWarn ) {

		DC.Select(m_FontWarn);

		DC.SetBkMode(TRANSPARENT);

		DC.SetTextColor(afxColor(WHITE));

		CString Text = IDS_NOTE_EDITING_IS;

		CRect   Rect = m_FramePanel;

		if( m_nKeys ) {

			Rect.top = Rect.bottom - FrameScale(10);
			}
		else 
			Rect.top = Rect.bottom - FrameScale(8);

		CSize   Size = DC.GetTextExtent(Text);

		CPoint  Pos;

		if( C3OemFeature(L"RedLion", FALSE) ) {

			Pos = CPoint(m_FrameDisplay.left, Rect.top + (Rect.cy() - Size.cy) / 2);
			}
		else
			Pos = Rect.GetTopLeft() + (Rect.GetSize() - Size) / 2;

		DC.TextOut(Pos, Text);

		DC.Deselect();
		}

	if( C3OemFeature(L"RedLion", FALSE) ) {

		DC.Select(m_FontLogo);

		CPoint Posn = m_FrameLogo;

		CSize  Size = DC.GetTextExtent(L"\x049");

		Posn.x -= Size.cx;

		Posn.y -= Size.cy / 2;

		DC.SetBkMode(TRANSPARENT);

		DC.SetTextColor(afxColor(WHITE));

		DC.TextOut(Posn, L"\x049");

		DC.SetTextColor(afxColor(RED));

		DC.TextOut(Posn, L"\x069");

		DC.Deselect();
		}
	}

void CColoradoFramer::DrawKeyBorder(CDC &DC, CRect Rect)
{
	}

void CColoradoFramer::DrawKeyText(CDC &DC, CRect Rect, CString Text)
{
	if( Text.GetLength() == 2 ) {

		DC.Select(m_FontFunc);
		}
	else
		DC.Select(m_FontText);

	DC.DrawText(Text, Rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	DC.Deselect();
	}

void CColoradoFramer::DrawKeyIcon(CDC &DC, CRect Rect, UINT uCode)
{
	DC.Select(m_FontKeys);

	UINT uIndex;

	switch( uCode ) {

		case COPS_VK_MENU:			

			uIndex = 2;
			break;

		default:
			uIndex = uCode - COPS_VK_SOFT1;
			break;
		}

	UINT uText = L"VUW"[uIndex];

	DC.DrawText(CPrintf("%c", uText), Rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	DC.Deselect();
	}

// Implementation

void CColoradoFramer::MakeTools(void)
{
	CColor Panel = C3OemGetColor(L"PanelOuter", CColor(0,0,0));

	m_Panel .Create(Panel);

	m_Middle.Create(C3OemGetColor(L"PanelMiddle", afxColor(BLACK)));
	
	m_Strip .Create(C3OemGetColor(L"PanelStrip",  afxColor(BLACK)));

	m_Border.Create(0, 2, Panel ? afxColor(BLACK) : afxColor(WHITE));
	}

void CColoradoFramer::FindLayoutKeys(void)
{
	m_FramePanel  = CRect(m_TotalSize) - m_nPad;

	m_FrameMiddle = m_FramePanel;

	m_FrameMiddle.Deflate(FrameScale(1));

	m_FrameDisplay = m_FrameRect;

	m_FrameDisplay.right += m_DispSize.cx;

	m_FrameDisplay.bottom = m_FrameDisplay.top + m_DispSize.cy;

	m_FrameDisplay.Inflate(1);

	m_FrameStrip = m_FrameMiddle;

	m_FrameStrip.left   += FrameScale(2);

	m_FrameStrip.right  -= FrameScale(2);

	m_FrameStrip.bottom -= FrameScale(2);

	m_FrameStrip.bottom  = m_FrameStrip.top + FrameScale(6);

	m_FrameKeys = m_FrameStrip;

	m_FrameKeys.Deflate(FrameScale(1));

	FindLayoutLogo();
	}

void CColoradoFramer::FindLayoutNone(void)
{
	m_FramePanel  = CRect(m_TotalSize) - m_nPad;

	m_FrameMiddle = m_FramePanel;

	m_FrameMiddle.left   += FrameScale(5);

	m_FrameMiddle.right  -= FrameScale(5);

	m_FrameMiddle.top    += FrameScale(6);

	m_FrameMiddle.bottom -= FrameScale(6);

	m_FrameDisplay = m_FrameRect;

	m_FrameDisplay.right += m_DispSize.cx;

	m_FrameDisplay.bottom = m_FrameDisplay.top + m_DispSize.cy;

	m_FrameDisplay.Inflate(1);

	FindLayoutLogo();
	}

void CColoradoFramer::FindLayoutLogo(void)
{
	CPoint Pos1 = m_FrameDisplay.GetBottomRight();

	CPoint Pos2 = m_FramePanel.GetBottomRight();

	m_FrameLogo = CPoint(Pos1.x, (Pos1.y + Pos2.y) / 2);
	}

void CColoradoFramer::FindScale(void)
{
	switch( m_DispSize.cx ) {

		case 480:
			m_nScale = 8;
			break;

		case 800:
			m_nScale = 15;
			break;

		default:
			m_nScale = 12;
			break;
		}
	
	m_nPad = 4;
	}

int CColoradoFramer::FrameScale(int n) const
{
	return n * m_nScale;
	}

// End of File

