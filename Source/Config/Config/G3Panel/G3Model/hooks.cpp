
#include "intern.hpp"

#include "g3straton.hpp"

#include "g3graph.hpp"

#include "g3syms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Init and Term Hooks
//

extern "C" void __declspec(dllexport) OnInit(void)
{
	G3GraphInit();

	StratonInit();
	}

extern "C" void __declspec(dllexport) OnTerm(void)
{
	StratonTerm();

	Sym_Term();

	G3GraphTerm();
	}

// End of File
