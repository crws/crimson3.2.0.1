
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_GenericSystem_HPP

#define INCLUDE_GenericSystem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCodedItem;
class CTimeSync;
class CFTPServer;
class CMailManager;

class CModemClientDriverOptionsOptionCard;
class CWifiStationDriverOptions;

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <g3mc.hpp>

#include "../G3MC/NewCommsPortRack.hpp"

#include "../G3MC/NewCommsDeviceRack.hpp"

#include <g3control.hpp>

#include <G3DevCon.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Generic System Item
//

class CGenericSystemItem : public CUISystem
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CGenericSystemItem(void);

	// Download Config
	CString GetDownloadConfig(void) const;

	// Model Mapping
	CString GetModelList(void) const;
	CString GetModelInfo(CString Model) const;

	// DLD Selection
	CString GetDldFolder(void) const;

	// Conversion
	void PostConvert(void);

	// Persistance
	void Init(void);
	void PostLoad(void);

	// System Data
	void AddNavCats(void);
	void AddResCats(void);

protected:
	// Data Members
	CStringArray m_Apply;

	// Model Check
	void CheckModel(void);

	// System Hooks
	void ChangeModel(void);
	void ApplyHardware(BOOL fInit);

	// New Schema Support
	void ApplyPorts(BOOL fInit);
	void ApplyModules(BOOL fInit);
	BOOL SavePortListPorts(CMap<CString, HGLOBAL> &Save, CCommsPortList *pList, PCTXT pTag);
	BOOL SaveCardListPorts(CMap<CString, HGLOBAL> &Save, COptionCardList *pCards, PCTXT pTag);
	BOOL SaveRackListPorts(CMap<CString, HGLOBAL> &Save, COptionCardRackList *pRacks, PCTXT pTag);
	BOOL SaveNewRackModules(CMap<CString, HGLOBAL> &Save, CCommsDeviceList *pList);
	BOOL SaveOldRackModules(CMap<CString, HGLOBAL> &Save, CCommsPortRack *pPort);
	BOOL SaveRedBusModules(CMap<CString, HGLOBAL> &Save, CCommsPortRack *pPort);
	BOOL ImportCardListPorts(CJsonData *pList, COptionCardList *pCards);
	BOOL ImportRackListPorts(CJsonData *pExp, COptionCardRackList *pRacks);
	BOOL ImportRackModules(CJsonData *pList, CCommsSlotList *pSlots, UINT uRack);
	BOOL ImportModules(CJsonData *pExp, CCommsSlotList *pSlots);
	BOOL ImportRedBusModules(CJsonData *pModules, CCommsDeviceList *pDevs);
	void ImportExpansion(void);
	BOOL UseRedBus(void);

	// Config Import
	void ImportConfig(void);
	void ImportSleds(void);
	void ImportCell(CString const &Root, CModemClientDriverOptionsOptionCard *pOpt);
	void ImportWifi(CString const &Root, CWifiStationDriverOptions *pOpt);
	void ImportIdent(void);
	void ImportDownload(void);
	void ImportCerts(void);
	void ImportRoutes(void);
	void ImportServices(void);
	void ImportTimeSync(CTimeSync *pTime);
	void ImportFtpServer(CFTPServer *pFtp);
	void ImportMailManager(CMailManager *pMail);
	BOOL ImportExpression(PCTXT pPath, CCodedItem * &pItem, UINT Type, PCTXT pForm = NULL);
	void ImportInteger(PCTXT pPath, CCodedItem * &pItem, PCTXT pForm = NULL, INT (*pfnFunc)(INT) = NULL);
	void ImportInteger(PCTXT pPath, INT nData);
	void ImportIpAddress(PCTXT pPath, CCodedItem * &pItem);
	void ImportIpAddress(PCTXT pPath, DWORD ip);
	void ImportString(PCTXT pPath, CCodedItem * &pItem);
	void ImportString(PCTXT pPath, CString const &Text);
	void KillCodedItem(CCodedItem * &pItem);
	void AddToPersonality(PCTXT pPath);
	void InvokeApplyCode(void);
	BOOL IsEnabled(CCodedItem *pItem);
	BOOL AdjustAlphaModels(void);
	void WalkModuleTypes(BOOL fCopy);

	// Meta Data
	void AddMetaData(void);
};

// End of File

#endif
