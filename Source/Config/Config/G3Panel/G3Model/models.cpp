
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Resolution Helper
//

static CString GetResolution(PCTXT pSub, int cx, int cy, BOOL fShort)
{
	CString Desc;

	if( pSub[0] ) {

		if( !wstrcmp(pSub, L"M") ) {

			Desc += CString(IDS_VIRTUAL_MONO);
		}

		if( !wstrcmp(pSub, L"Q") ) {

			Desc += CString(IDS_VIRTUAL_X);
		}

		if( !wstrcmp(pSub, L"V") ) {

			Desc += CString(IDS_VIRTUAL_X_2);
		}

		if( !wstrcmp(pSub, L"V2") ) {

			Desc += CString(IDS_X);
		}

		if( !wstrcmp(pSub, L"E2") ) {

			Desc += CString(IDS_X_2);
		}

		if( !wstrcmp(pSub, L"N") ) {

			Desc += CString(IDS_WVGA_MODE);
		}

		if( !wstrcmp(pSub, L"E") ) {

			Desc += CString(IDS_QVGA_EMULATION);
		}

		if( !wstrcmp(pSub, L"EQ") ) {

			Desc += CString(IDS_X_EMULATED);
		}

		if( !wstrcmp(pSub, L"EV") ) {

			Desc += CString(IDS_X_EMULATED_2);
		}

		if( !wstrcmp(pSub, L"WX") ) {

			Desc += CString(IDS_VIRTUAL_X_3);
		}

		if( !wstrcmp(pSub, L".") ) {

			Desc += CPrintf(L"%ux%u", cx, cy);
		}

		if( !wstrcmp(pSub, L"R") ) {

			Desc += CPrintf(L"%ux%u", cx, cy);
		}

		if( !wstrcmp(pSub, L"W") ) {

			Desc += CString(IDS_VIRTUAL_X_4);
		}

		if( !wstrcmp(pSub, L"WQ") ) {

			Desc += CString(IDS_VIRTUAL_X_5);
		}

		if( !wstrcmp(pSub, L"WV") ) {

			Desc += CString(IDS_VIRTUAL_X_6);
		}

		return Desc;
	}

	if( fShort ) {

		return CPrintf(L"%ux%u", cx, cy);
	}

	return L"";
}

//////////////////////////////////////////////////////////////////////////
//
// Model List
//

extern "C" void __declspec(dllexport) ListModels(CStringArray &List, BOOL fShort)
{
	struct CInfo
	{
		UINT	m_uGroup;
		UINT	m_uDesc;
		int	m_cx;
		int	m_cy;
		int	m_ni;
		PCTXT	m_pClass;
		PCTXT	m_pSub;
		UINT	m_uBase;
		UINT	m_uPart;
		PCTXT	m_pLink;
		PCTXT	m_pSpecies;
	};

	static CInfo t[] = {

		{ IDS_GROUP_DA,		 IDS_COLORADO_DA,        0,   0,  0,	L"DA10D",	L"",     IDS_DA10_BASE,		IDS_DA10_PART,         L"", L"g3", },

		{ IDS_GROUP_DA,		 IDS_CANYON_DA,        480, 272,  0,	L"DA30D",	L"WQ",   IDS_DA30_BASE,		IDS_DA30_PART,         L"", L"g3", },
		{ IDS_GROUP_DA,		 IDS_CANYON_DA,        800, 480,  0,	L"DA30D",	L"WV",   IDS_DA30_BASE,		IDS_DA30_PART,         L"", L"g3", },
		{ IDS_GROUP_DA,		 IDS_CANYON_DA,       1280, 720,  0,	L"DA30D",	L"WX",   IDS_DA30_BASE,		IDS_DA30_PART,         L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA50X0A",	L"WQ",   IDS_DA50_BASE,		IDS_DA500A_PART,       L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA50X0A",	L"WV",   IDS_DA50_BASE,		IDS_DA500A_PART,       L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA50X0A",	L"WX",   IDS_DA50_BASE,		IDS_DA500A_PART,       L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA50X0B",	L"WQ",   IDS_DA50_BASE,		IDS_DA500B_PART,       L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA50X0B",	L"WV",   IDS_DA50_BASE,		IDS_DA500B_PART,       L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA50X0B",	L"WX",   IDS_DA50_BASE,		IDS_DA500B_PART,       L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA50X0D",	L"WQ",   IDS_DA50_BASE,		IDS_DA500D_PART,       L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA50X0D",	L"WV",   IDS_DA50_BASE,		IDS_DA500D_PART,       L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA50X0D",	L"WX",   IDS_DA50_BASE,		IDS_DA500D_PART,       L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA70X0E",	L"WQ",   IDS_DA70_BASE,		IDS_DA70X0E_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA70X0E",	L"WV",   IDS_DA70_BASE,		IDS_DA70X0E_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA70X0E",	L"WX",   IDS_DA70_BASE,		IDS_DA70X0E_PART,      L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA70X0F",	L"WQ",   IDS_DA70_BASE,		IDS_DA70X0F_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA70X0F",	L"WV",   IDS_DA70_BASE,		IDS_DA70X0F_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA70X0F",	L"WX",   IDS_DA70_BASE,		IDS_DA70X0F_PART,      L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA70X0G",	L"WQ",   IDS_DA70_BASE,		IDS_DA70X0G_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA70X0G",	L"WV",   IDS_DA70_BASE,		IDS_DA70X0G_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA70X0G",	L"WX",   IDS_DA70_BASE,		IDS_DA70X0G_PART,      L"", L"g3", },

		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   480, 272,  0,	L"DA70X0H",	L"WQ",   IDS_DA70_BASE,		IDS_DA70X0H_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,   800, 480,  0,	L"DA70X0H",	L"WV",   IDS_DA70_BASE,		IDS_DA70X0H_PART,      L"", L"g3", },
		{ IDS_GROUP_MANTICORE,	 IDS_MANTICORE_DA,  1280, 720,  0,	L"DA70X0H",	L"WX",   IDS_DA70_BASE,		IDS_DA70X0H_PART,      L"", L"g3", },

		{ IDS_GROUP_COLORADO,	 IDS_COLOR_TOUCH,      480, 272,  4,	L"CO04",	L"",     IDS_C04_BASE,		IDS_C04_PART,          L"", L"g3", },
		{ IDS_GROUP_COLORADO,	 IDS_COLOR_TOUCH,      800, 480,  7,	L"CO07",	L".",    IDS_C07_BASE,		IDS_C07_PART,          L"", L"g3", },
		{ IDS_GROUP_COLORADO,	 IDS_COLOR_TOUCH,      320, 240,  7,	L"CO07",	L"EQ",   IDS_C07Q_BASE,		IDS_C07_PART,          L"", L"g3", },
		{ IDS_GROUP_COLORADO,	 IDS_COLOR_TOUCH,      800, 600, 10,	L"CO10",	L".",    IDS_C10_BASE,		IDS_C10_PART,          L"", L"g3", },
		{ IDS_GROUP_COLORADO,	 IDS_COLOR_TOUCH,      640, 480, 10,	L"CO10",	L"EV",   IDS_C10V_BASE,		IDS_C10_PART,          L"", L"g3", },

		{ IDS_GROUP_CANYON,	 IDS_COLOR_TOUCH,      480, 272,  4,	L"CA04",	L"",     IDS_CA04_BASE,		IDS_CA04_PART,         L"", L"g3", },
		{ IDS_GROUP_CANYON,	 IDS_COLOR_TOUCH,      800, 480,  7,	L"CA07",	L".",    IDS_CA07_BASE,		IDS_CA07_PART,         L"", L"g3", },
		{ IDS_GROUP_CANYON,	 IDS_COLOR_TOUCH,      320, 240,  7,	L"CA07",	L"EQ",   IDS_CA07Q_BASE,	IDS_CA07_PART,         L"", L"g3", },
		{ IDS_GROUP_CANYON,	 IDS_COLOR_TOUCH,      800, 600, 10,	L"CA10",	L".",    IDS_CA10_BASE,		IDS_CA10_PART,         L"", L"g3", },
		{ IDS_GROUP_CANYON,	 IDS_COLOR_TOUCH,      640, 480, 10,	L"CA10",	L"EV",   IDS_CA10V_BASE,	IDS_CA10_PART,         L"", L"g3", },
		{ IDS_GROUP_CANYON,	 IDS_COLOR_TOUCH,     1024, 768, 15,	L"CA15",	L"",     IDS_CA15_BASE,		IDS_CA15_PART,         L"", L"g3", },

		{ IDS_GROUP_CANYON_C,	 IDS_COLOR_TOUCH,      480, 272,  4,	L"CA04C",	L"",     IDS_CA04C_BASE,	IDS_CA04C_PART,        L"", L"g3", },
		{ IDS_GROUP_CANYON_C,	 IDS_COLOR_TOUCH,      800, 480,  7,	L"CA07C",	L".",    IDS_CA07C_BASE,	IDS_CA07C_PART,        L"", L"g3", },
		{ IDS_GROUP_CANYON_C,	 IDS_COLOR_TOUCH,      320, 240,  7,	L"CA07C",	L"EQ",   IDS_CA07CQ_BASE,	IDS_CA07C_PART,        L"", L"g3", },
		{ IDS_GROUP_CANYON_C,	 IDS_COLOR_TOUCH,      800, 600, 10,	L"CA10C",	L".",    IDS_CA10C_BASE,	IDS_CA10C_PART,        L"", L"g3", },
		{ IDS_GROUP_CANYON_C,	 IDS_COLOR_TOUCH,      640, 480, 10,	L"CA10C",	L"EV",   IDS_CA10CV_BASE,	IDS_CA10C_PART,        L"", L"g3", },
		{ IDS_GROUP_CANYON_C,	 IDS_COLOR_TOUCH,     1024, 768, 15,	L"CA15C",	L"",     IDS_CA15C_BASE,	IDS_CA15C_PART,        L"", L"g3", },

		{ IDS_GROUP_GRAPHITE,	 IDS_COLOR_TOUCH,      800, 480,  7,	L"G07",		L"",     IDS_G07_BASE,		IDS_G07_PART,          L"", L"g3", },
		{ IDS_GROUP_GRAPHITE,	 IDS_COLOR_TOUCH,      800, 480,  9,	L"G09",		L"",     IDS_G09_BASE,		IDS_G09_PART,          L"", L"g3", },
		{ IDS_GROUP_GRAPHITE,	 IDS_COLOR_TOUCH,      640, 480, 10,	L"G10",		L".",    IDS_G10_BASE,		IDS_G10_PART,          L"", L"g3", },
		{ IDS_GROUP_GRAPHITE,	 IDS_COLOR_TOUCH,      800, 600, 10,	L"G10",		L"R",    IDS_G10R_BASE,		IDS_G10R_PART,         L"", L"g3", },
		{ IDS_GROUP_GRAPHITE,	 IDS_COLOR_TOUCH,     1280, 800, 12,	L"G12",		L"",     IDS_G12_BASE,		IDS_G12_PART,          L"", L"g3", },
		{ IDS_GROUP_GRAPHITE,	 IDS_COLOR_TOUCH,     1024, 768, 15,	L"G15",		L"",     IDS_G15_BASE,		IDS_G15_PART,          L"", L"g3", },

		{ IDS_GROUP_GRAPHITE_C,	 IDS_GRAPHITE_C,       320, 240,  0,	L"GSR",		L"Q",    IDS_GSC_BASE,		IDS_GSC_PART,	       L"", L"g3", },
		{ IDS_GROUP_GRAPHITE_C,	 IDS_GRAPHITE_C,       640, 480,  0,	L"GSR",		L"V",    IDS_GSC_BASE,		IDS_GSC_PART,	       L"", L"g3", },
		{ IDS_GROUP_GRAPHITE_C,	 IDS_GRAPHITE_C,      1280, 800,  0,	L"GSR",		L"W",    IDS_GSC_BASE,		IDS_GSC_PART,	       L"", L"g3", },

		{ IDS_GROUP_GRAPHITE_C,	 IDS_GRAPHITE_C,       320, 240,  0,	L"GCE",		L"Q",    IDS_GEX_BASE,		IDS_GEX_PART,	       L"", L"g3", },
		{ IDS_GROUP_GRAPHITE_C,	 IDS_GRAPHITE_C,       640, 480,  0,	L"GCE",		L"V",    IDS_GEX_BASE,		IDS_GEX_PART,	       L"", L"g3", },
		{ IDS_GROUP_GRAPHITE_C,	 IDS_GRAPHITE_C,      1280, 800,  0,	L"GCE",		L"W",    IDS_GEX_BASE,		IDS_GEX_PART,	       L"", L"g3", },

		{ IDS_GROUP_GRAPHITE_CN, IDS_GRAPHITE_CN,      320, 240,  0,	L"GCEN",	L"Q",    IDS_GENX_BASE,		IDS_GENX_PART,	       L"", L"g3", },
		{ IDS_GROUP_GRAPHITE_CN, IDS_GRAPHITE_CN,      640, 480,  0,	L"GCEN",	L"V",    IDS_GENX_BASE,		IDS_GENX_PART,	       L"", L"g3", },
		{ IDS_GROUP_GRAPHITE_CN, IDS_GRAPHITE_CN,     1280, 800,  0,	L"GCEN",	L"W",    IDS_GENX_BASE,		IDS_GENX_PART,	       L"", L"g3", },

		#if 0

		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ETMIX24880",  L"",     IDS_ETMIX24880_BASE,	IDS_ETMIX24880_PART,   L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ETMIX24882",  L"",     IDS_ETMIX24882_BASE,	IDS_ETMIX24882_PART,   L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ETMIX20884",  L"",     IDS_ETMIX20884_BASE,	IDS_ETMIX20884_PART,   L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET32DI24",    L"",     IDS_ET32DI24_BASE,	IDS_ET32DI24_PART,     L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16DI24",    L"",     IDS_ET16DI24_BASE,	IDS_ET16DI24_PART,     L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16DIAC",    L"",     IDS_ET16DIAC_BASE,	IDS_ET16DIAC_PART,     L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET32DO24",    L"",     IDS_ET32DO24_BASE,	IDS_ET32DO24_PART,     L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16DO24",    L"",     IDS_ET16DO24_BASE,	IDS_ET16DO24_PART,     L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET32AI20M",   L"",     IDS_ET32AI20M_BASE,	IDS_ET32AI20M_PART,    L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16AI20M",   L"",     IDS_ET16AI20M_BASE,	IDS_ET16AI20M_PART,    L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET8AO20M",    L"",     IDS_ET8AO20M_BASE,	IDS_ET8AO20M_PART,     L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16AI8AO",   L"",     IDS_ET16AI8AO_BASE,	IDS_ET16AI8AO_PART,    L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET32AI10V",   L"",     IDS_ET32AI10V_BASE,	IDS_ET32AI10V_PART,    L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET10RTD",     L"",     IDS_ET10RTD_BASE,	IDS_ET10RTD_PART,      L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16ISOTC",   L"",     IDS_ET16ISOTC_BASE,	IDS_ET16ISOTC_PART,    L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16ISO20M",  L"",     IDS_ET16ISO20M_BASE,	IDS_ET16ISO20M_PART,   L"", L"e3", },
		{ IDS_GROUP_ET3,	IDS_ETHERTRAK3,         0,   0,  0,	L"ET16DORLY",   L"",     IDS_ET16DORLY_BASE,	IDS_ET16DORLY_PART,    L"", L"e3", },

		#endif
	};

	for( UINT n = 0; n < elements(t); n++ ) {

		List.Append(CString(t[n].m_uGroup));

		CString Name = t[n].m_pClass;

		C3OemStrings(Name);

		if( TRUE ) {

			CString Desc;

			Desc += Name;

			Desc += L'\t';

			Desc += CString(t[n].m_uDesc);

			Desc += L'\t';

			Desc += GetResolution(t[n].m_pSub, t[n].m_cx, t[n].m_cy, fShort);

			Desc += L'\t';

			Desc += CString(t[n].m_uBase);

			Desc += L'\t';

			Desc += CString(t[n].m_uPart);

			if( t[n].m_pLink[0] ) {

				Desc += L'\t';

				Desc += t[n].m_pLink;
			}
			else {
				Desc += L'\t';

				switch( t[n].m_uGroup ) {

					case IDS_GROUP_COLORADO:

						Desc += L"https://www.redlion.net/node/38129";

						break;

					case IDS_GROUP_CANYON:

						Desc += L"https://www.redlion.net/node/38129";

						break;

					case IDS_GROUP_GRAPHITE:

						Desc += L"https://www.redlion.net/node/27580";

						break;

					case IDS_GROUP_GRAPHITE_C:

						Desc += L"https://www.redlion.net/node/37591";

						break;

					case IDS_GROUP_ET3:

						Desc += L"https://www.redlion.net/node/29202";

						break;

					default:

						Desc += L"https://www.redlion.net/link/wp.asp?id=6158";

						break;
				}
			}

			List.Append(Desc);
		}

		if( TRUE ) {

			CString Desc;

			Desc += Name;

			Desc += L'\t';

			Desc += CString(t[n].m_uDesc);

			if( t[n].m_cx ) {

				Desc += L'\t';

				Desc += GetResolution(t[n].m_pSub, t[n].m_cx, t[n].m_cy, fShort);
			}
			else {
				Desc += L'\t';

				Desc += CString(IDS_NONE);
			}

			if( t[n].m_ni ) {

				Desc += L'\t';

				Desc += CPrintf(L"%u\"", t[n].m_ni);
			}
			else {
				Desc += L'\t';

				if( t[n].m_cx ) {

					Desc += CString(IDS_VIRTUAL);
				}
				else
					Desc += CString(IDS_NONE);
			}

			List.Append(Desc);
		}

		CString Class = t[n].m_pClass;

		if( t[n].m_pSub[0] && t[n].m_pSub[0] != '.' ) {

			Class += t[n].m_pSub;
		}

		List.Append(Class);

		List.Append(t[n].m_pSpecies);
	}
}

extern "C" PCTXT __declspec(dllexport) GetUpgradePath(CString Model)
{
	// Codes:
	//
	// I = Identical Screen
	// B = Vertical Bars
	// C = Centered with Frame
	// N = Integral Scalings
	// R = Non-Integral Scaling
	// E = Integral Emulation
	// F = Fuzzy Emulation
	// X = No Display
	//
	// * = Preferred Model

	if( Model == L"G306" ) return L"G07/N,CA07/N,*CA07EQ/E";
	if( Model == L"G308V2" ) return L"G07/B,G09/B,CA07/B,CA10/R,*CA10/C,CA10EV/F";
	if( Model == L"G310V2" ) return L"G10/I,G10R/R,G10R/C,CA07/B,CA10/R,*CA10/C,CA10EV/F";
	if( Model == L"G310E2" ) return L"G10R/I,*CA10/I";
	if( Model == L"G315" ) return L"G15/I,*CA15/I";

	if( Model == L"G304K2" ) return L"CA04/I,*CO04/I";
	if( Model == L"G307K2N" ) return L"CA07/I,*CO07/I";
	if( Model == L"G307K2E" ) return L"CA07EQ/I,*CO07EQ/I";

	if( Model == L"G304K" ) return L"CA04/I,*CO04/I";
	if( Model == L"G306K" ) return L"CA07/N,CA07EQ/E,CO07/N,*CO07EQ/E";
	if( Model == L"G308K" ) return L"CA07/B,CA10/R,CA10/C,CA10EV/F,CO07/B,CO10/R,*CO10/C,CO10EV/F";

	if( Model == L"DSPLE" ) return L"*DA10D/X,DA30DWQ/C,DA50X0A/X,DA50X0AWQ/C";
	if( Model == L"DSPSXQ" ) return L"DA30DWQ/R,*DA30DWQ/C,DA50X0AWQ/C,DA50X0AWQ/R";
	if( Model == L"DSPGTQ" ) return L"DA30DWQ/R,*DA30DWQ/C,DA50X0AWQ/C,DA50X0AWQ/R";
	if( Model == L"DSPGTV" ) return L"*DA30DWV/C,DA50X0AWV/C";
	if( Model == L"DSPZRQ" ) return L"DA30DWQ/R,*DA30DWQ/C,DA50X0AWQ/C,DA50X0AWQ/R";
	if( Model == L"DSPZRV" ) return L"*DA30DWV/C,DA50X0AWV/C";
	if( Model == L"DSPZRWX" ) return L"*DA30DWX/I,DA50X0AWX/I";

	if( Model == L"MC2V2" ) return L"*DA70X0F/X,DA70X0FWQ/C,DA70X0G/X,DA70X0GWQ/C";
	if( Model == L"MC2LE" ) return L"*DA70X0F/X,DA70X0FWQ/C,DA70X0G/X,DA70X0GWQ/C";
	if( Model == L"MC2SXQ" ) return L"*DA70X0FWQ/C,DA70X0FWQ/R,DA70X0GWQ/C,DA70X0GWQ/R";
	if( Model == L"MC2GTQ" ) return L"*DA70X0FWQ/C,DA70X0FWQ/R,DA70X0GWQ/C,DA70X0GWQ/R";
	if( Model == L"MC2GTV" ) return L"*DA70X0FWV/C,DA70X0GWV/C";
	if( Model == L"MCZRQ" ) return L"*DA70X0FWQ/C,DA70X0FWQ/R,DA70X0GWQ/C,DA70X0GWQ/R";
	if( Model == L"MCZRV" ) return L"*DA70X0FWV/C,DA70X0GWV/C";
	if( Model == L"MCZRWX" ) return L"*DA70X0FWX/I,DA70X0GWX/I";

	return NULL;
}

// End of File
