
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Pax2A System Item
//

class CPAX2AItem : public CUISystem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPAX2AItem(void);

		// Model Mapping
		CString GetModelList(void) const;
		CString GetModelInfo(CString Model) const;

	};


//////////////////////////////////////////////////////////////////////////
//
// Pax2A System Item
//

// Dynamic Class

AfxImplementDynamicClass(CPAX2AItem, CUISystem);

// Constructor

CPAX2AItem::CPAX2AItem(void) : CUISystem(FALSE)
{	

	}

// Model Mapping

CString CPAX2AItem::GetModelList(void) const
{
	return L"pax2a";
	}

CString CPAX2AItem::GetModelInfo(CString Model) const
{
	if( Model == L"pax2a" ) {

		return L"pax2a"; //TODO ,promvs,loadvs";
		}

	return L"";
	}


// End of File