
#include "Intern.hpp"

#include "DevConEditCtrl.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Edit Control
//

// Base Class

#define CBaseClass CEditCtrl

// Runtime Class

AfxImplementRuntimeClass(CDevConEditCtrl, CBaseClass);

// Static Data

UINT CDevConEditCtrl::m_timerQuick = CWnd::AllocTimerID();

// Constants

static TCHAR const cSep = 0xB6;

// Constructor

CDevConEditCtrl::CDevConEditCtrl(CDevConElement *pElem)
{
	m_pElem = pElem;

	Construct();
}

// IUnknown

HRESULT CDevConEditCtrl::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
		}

		return E_NOINTERFACE;
	}

	return E_POINTER;
}

ULONG CDevConEditCtrl::AddRef(void)
{
	return 1;
}

ULONG CDevConEditCtrl::Release(void)
{
	return 1;
}

// IDropTarget

HRESULT CDevConEditCtrl::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( !IsItemReadOnly() ) {

		if( !IsReadOnly() ) {

			FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

			if( pData->QueryGetData(&Fmt) == S_OK ) {

				SetFocus();

				if( *pEffect & DROPEFFECT_LINK ) {

					*pEffect = DROPEFFECT_LINK;
				}
				else
					*pEffect = DROPEFFECT_COPY;

				m_dwEffect = *pEffect;

				SetDrop(1);

				return S_OK;
			}
		}

		if( m_pElem ) {

			if( m_pElem->CanAcceptData(pData, *pEffect) ) {

				m_dwEffect = *pEffect;

				SetDrop(2);

				return S_OK;
			}
		}
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConEditCtrl::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConEditCtrl::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
}

HRESULT CDevConEditCtrl::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

		STGMEDIUM Med = { 0, NULL, NULL };

		if( pData->GetData(&Fmt, &Med) == S_OK ) {

			CString Text  = PCTXT(GlobalLock(Med.hGlobal));

			CString Prev  = GetWindowText();

			int     nLen  = Prev.GetLength();

			CRange  Range = GetSel();

			GlobalUnlock(Med.hGlobal);

			ReleaseStgMedium(&Med);

			if( Range.m_nFrom == 0 && Range.m_nTo == nLen ) {

				if( Text.CompareC(Prev) ) {

					SetWindowText(Text);

					SetModify(TRUE);

					SendNotify(EN_RETURN);
				}
			}
			else {
				Prev.Delete(Range.m_nFrom, Range.m_nTo - Range.m_nFrom);

				Prev.Insert(Range.m_nFrom, Text);

				SetWindowText(Prev);

				SetModify(TRUE);

				int nPos = Range.m_nFrom + Text.GetLength();

				SetSel(CRange(nPos, nPos));
			}

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
		}
	}

	if( m_uDrop == 2 ) {

		if( m_pElem->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			SendNotify(EN_DROP);

			return S_OK;
		}
	}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);

	return S_OK;
}


// Attributes

BOOL CDevConEditCtrl::GetError(void) const
{
	return m_fError;
}

// Operations

void CDevConEditCtrl::SetContent(UINT uContent)
{
	m_uContent = uContent;
}

void CDevConEditCtrl::SetScroll(BOOL fScroll)
{
	m_fScroll = fScroll;
}

void CDevConEditCtrl::SetError(BOOL fError)
{
	if( m_fError != fError ) {

		m_fError = fError;

		if( IsWindow() ) {

			Invalidate(FALSE);
		}
	}
}

void CDevConEditCtrl::SetDefault(PCTXT pDefault)
{
	m_Default = pDefault;
}

// Message Map

AfxMessageMap(CDevConEditCtrl, CBaseClass)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_NCCALCSIZE)
	AfxDispatchMessage(WM_NCPAINT)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_GETDLGCODE)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_KEYUP)
	AfxDispatchMessage(WM_CHAR)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CDevConEditCtrl)
};

// Message Handlers

void CDevConEditCtrl::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
}

void CDevConEditCtrl::OnPreDestroy(void)
{
	m_fDying = TRUE;
}

UINT CDevConEditCtrl::OnNCCalcSize(BOOL fCalcRects, NCCALCSIZE_PARAMS &Params)
{
	Params.rgrc[0].top++;

	Params.rgrc[0].bottom--;

	return AfxCallDefProc();
}

void CDevConEditCtrl::OnNCPaint(void)
{
	CWindowDC DC(ThisObject);

	CRect Rect(GetWindowRect());

	Rect -= Rect.GetTopLeft();

	CRect r1(Rect);

	CRect r2(Rect);

	r1.bottom = r1.top + 1;

	r2.top = r2.bottom - 1;

	DC.FillRect(r1, afxBrush(TabFace));

	DC.FillRect(r2, afxBrush(TabFace));
}

void CDevConEditCtrl::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_fError ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxBrush(RED));

		return;
	}

	if( m_uDrop ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxBrush(Orange1));

		return;
	}

	if( !IsEnabled() ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxColor(Disabled));

		return;
	}

	if( !m_Default.IsEmpty() ) {

		if( !GetWindowTextLength() && IsEnabled() ) {

			CRect Rect = GetClientRect();

			DC.FillRect(Rect, afxBrush(WindowBack));

			DC.FrameRect(Rect, afxBrush(Enabled));

			DC.SetTextColor(afxColor(3dShadow));

			DC.SetBkMode(TRANSPARENT);

			DC.Select(afxFont(Dialog));

			DC.TextOut(5, 2, m_Default);

			DC.Deselect();

			return;
		}
	}

	DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

	DC.FrameRect(GetClientRect(), afxColor(Enabled));
}

void CDevConEditCtrl::OnKillFocus(CWnd &Wnd)
{
	if( !m_Default.IsEmpty() ) {

		if( !GetWindowTextLength() && IsEnabled() ) {

			Invalidate(TRUE);
		}
	}

	AfxCallDefProc();
}

void CDevConEditCtrl::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( GetWindowStyle() & ES_READONLY ) {

		AfxCallDefProc();

		SetFocus();

		SendNotify(IDM_UI_SPECIAL);

		return;
	}

	AfxCallDefProc();
}

void CDevConEditCtrl::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
	}

	if( m_fScroll ) {

		if( nDelta > 0 ) {

			m_pElem->ScrollData(SB_LINEUP);
		}
		else
			m_pElem->ScrollData(SB_LINEDOWN);

		return;
	}

	AfxCallDefProc();
}

UINT CDevConEditCtrl::OnGetDlgCode(MSG *pMsg)
{
	UINT uCode = DLGC_WANTCHARS | DLGC_HASSETSEL | DLGC_WANTARROWS;

	if( pMsg ) {

		if( pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP ) {

			if( pMsg->wParam == VK_TAB ) {

				uCode |= DLGC_WANTMESSAGE;
			}

			if( pMsg->wParam == VK_RETURN ) {

				if( GetModify() || GetError() ) {

					uCode |= DLGC_WANTMESSAGE;
				}
			}

			if( pMsg->wParam == VK_ESCAPE ) {

				if( GetModify() || GetError() ) {

					uCode |= DLGC_WANTMESSAGE;
				}
			}
		}
	}

	return uCode;
}

void CDevConEditCtrl::OnKeyDown(UINT uCode, DWORD dwData)
{
	if( m_fDying ) {

		return;
	}

	if( !IsItemReadOnly() ) {

		if( m_fScroll ) {

			switch( uCode ) {

				case VK_UP:

					if( m_fFirst ) {

						SetTimer(m_timerQuick, 1000);

						m_fFirst = FALSE;
					}

					m_pElem->ScrollData(m_fQuick ? SB_PAGEUP : SB_LINEUP);

					return;

				case VK_DOWN:

					if( m_fFirst ) {

						SetTimer(m_timerQuick, 1000);

						m_fFirst = FALSE;
					}

					m_pElem->ScrollData(m_fQuick ? SB_PAGEDOWN : SB_LINEDOWN);

					return;

				case VK_HOME:

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						m_pElem->ScrollData(SB_TOP);

						return;
					}

					break;

				case VK_END:

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						m_pElem->ScrollData(SB_BOTTOM);

						return;
					}

					break;

				case VK_PRIOR:

					m_pElem->ScrollData(SB_PAGEUP);

					return;

				case VK_NEXT:

					m_pElem->ScrollData(SB_PAGEDOWN);

					return;
			}
		}
	}

	switch( uCode ) {

		case VK_TAB:

			if( GetModify() || m_fError ) {

				SendNotify(EN_TAB_AWAY);

				if( !m_fError ) {

					HandleTab();
				}
			}
			else {
				HandleTab();
			}

			return;

		case VK_RETURN:

			if( GetModify() ) {

				SendNotify(EN_RETURN);
			}

			return;

		case VK_ESCAPE:

			SendNotify(EN_CANCEL);

			return;
	}

	AfxCallDefProc();
}

void CDevConEditCtrl::OnKeyUp(UINT uCode, DWORD dwData)
{
	if( m_fDying ) {

		return;
	}

	if( !IsItemReadOnly() ) {

		if( m_fScroll ) {

			switch( uCode ) {

				case VK_UP:
				case VK_DOWN:

					m_fFirst = TRUE;

					m_fQuick = FALSE;

					KillTimer(m_timerQuick);

					return;

				case VK_HOME:
				case VK_END:

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						return;
					}

					break;

				case VK_PRIOR:
				case VK_NEXT:

					return;
			}
		}
	}

	switch( uCode ) {

		case VK_TAB:
		case VK_RETURN:
		case VK_ESCAPE:

			return;
	}

	AfxCallDefProc();
}

void CDevConEditCtrl::OnChar(UINT uCode, DWORD dwData)
{
	if( m_fDying ) {

		return;
	}

	if( IsItemReadOnly() ) {

		return;
	}

	if( isprint(uCode) ) {

		switch( m_uContent ) {

			case EC_HOSTNAME:
			{
				if( uCode == ' ' ) {

					return;
				}
			}
			break;

			case EC_NUMBER:
			{
				if( !isdigit(uCode) ) {

					return;
				}
			}
			break;

			case EC_IP:
			case EC_FLOAT:
			{
				if( !isdigit(uCode) && uCode != '.' ) {

					return;
				}
			}
			break;
		}
	}

	switch( uCode ) {

		case VK_TAB:
		case VK_RETURN:
		case VK_ESCAPE:

			return;
	}

	AfxCallDefProc();
}

void CDevConEditCtrl::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerQuick ) {

		m_fQuick = TRUE;
	}

	AfxCallDefProc();
}

// Command Handlers

BOOL CDevConEditCtrl::OnPasteControl(UINT uID, CCmdSource &Src)
{
	if( IsItemReadOnly() ) {

		Src.EnableItem(FALSE);

		return TRUE;
	}

	if( InEditMode() ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

			if( pData->QueryGetData(&Fmt) == S_OK ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
			}

			pData->Release();
		}

		return TRUE;
	}

	if( m_pElem ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			DWORD dwEffect;

			if( m_pElem->CanAcceptData(pData, dwEffect) ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
			}

			pData->Release();
		}
	}

	return FALSE;
}

BOOL CDevConEditCtrl::OnPasteCommand(UINT uID)
{
	if( InEditMode() ) {

		Paste();

		return TRUE;
	}

	if( m_pElem ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			if( m_pElem->AcceptData(pData) ) {

				pData->Release();

				return TRUE;
			}

			pData->Release();
		}
	}

	return FALSE;
}

// Implementation

void CDevConEditCtrl::Construct(void)
{
	m_fScroll  = FALSE;

	m_uContent = EC_ANY;

	m_fFirst   = TRUE;

	m_fQuick   = FALSE;

	m_uDrop    = 0;

	m_fError   = FALSE;

	m_fDying   = FALSE;
}

void CDevConEditCtrl::HandleTab(void)
{
	BOOL fPrev = (GetKeyState(VK_SHIFT) & 0x8000) ? TRUE : FALSE;

	CWnd  &Dlg = FindParent();

	HWND  hWnd = GetNextDlgTabItem(Dlg, m_hWnd, fPrev);

	CWnd  &Wnd = CWnd::FromHandle(hWnd);

	((CDialog &) Dlg).SetDlgFocus(Wnd);
}

CWnd & CDevConEditCtrl::FindParent(void)
{
	HWND hWnd = m_hWnd;

	for( ;;) {

		hWnd = ::GetParent(hWnd);

		if( ::GetWindowLong(hWnd, GWL_EXSTYLE) & WS_EX_CONTROLPARENT ) {

			continue;
		}

		break;
	}

	return CWnd::FromHandle(hWnd);
}

BOOL CDevConEditCtrl::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
	}

	return FALSE;
}

BOOL CDevConEditCtrl::InEditMode(void)
{
	if( HasFocus() ) {

		CRange Edit = GetSel();

		int    nLen = GetWindowTextLength();

		if( Edit.m_nFrom == 0 && Edit.m_nTo == nLen ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CDevConEditCtrl::IsItemReadOnly(void)
{
	return m_pElem->IsReadOnly();
}

// End of File
