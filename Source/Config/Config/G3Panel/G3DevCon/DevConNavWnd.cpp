
#include "Intern.hpp"

#include "DevConNavWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevCon.hpp"

#include "DevConPart.hpp"

#include "DevConPartHardware.hpp"

#include "DevConNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Navigation Window
//

// Base Class

#define CBaseClass CViewWnd

// Dynamic Class

AfxImplementDynamicClass(CDevConNavWnd, CBaseClass);

// Static Data

UINT CDevConNavWnd::m_timerDouble = AllocTimerID();

// Constructor

CDevConNavWnd::CDevConNavWnd(void)
{
	m_pTree    = New CTreeView;

	m_dwStyle  = TVS_NOTOOLTIPS| TVS_TRACKSELECT | TVS_FULLROWSELECT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS;

	m_fLoading = FALSE;

	m_hSelect  = NULL;

	m_pSelect  = NULL;

	m_LinkCursor.Create(L"LinkCursor");

	m_MoveCursor.Create(L"MoveCursor");

	m_CopyCursor.Create(L"CopyCursor");
}

// Destructor

CDevConNavWnd::~CDevConNavWnd(void)
{
}

// IUnknown

HRESULT CDevConNavWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropSource ) {

			*ppObject = (IDropSource *) this;

			return S_OK;
		}

		if( iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
		}

		return E_NOINTERFACE;
	}

	return E_POINTER;
}

ULONG CDevConNavWnd::AddRef(void)
{
	return 1;
}

ULONG CDevConNavWnd::Release(void)
{
	return 1;
}

// IUpdate

HRESULT CDevConNavWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		if( pItem == m_pItem ) {

			RefreshTree();
		}
	}

	return S_OK;
}

// IDropSource

HRESULT CDevConNavWnd::QueryContinueDrag(BOOL fEscape, DWORD dwKeys)
{
	if( !fEscape ) {

		if( dwKeys & MK_RBUTTON ) {

			return DRAGDROP_S_CANCEL;
		}

		if( dwKeys & MK_LBUTTON ) {

			return S_OK;
		}

		return DRAGDROP_S_DROP;
	}

	return DRAGDROP_S_CANCEL;
}

HRESULT CDevConNavWnd::GiveFeedback(DWORD dwEffect)
{
	if( dwEffect == DROPEFFECT_LINK ) {

		SetCursor(m_LinkCursor);

		return S_OK;
	}

	if( dwEffect == DROPEFFECT_MOVE ) {

		SetCursor(m_MoveCursor);

		return S_OK;
	}

	if( dwEffect == DROPEFFECT_COPY ) {

		SetCursor(m_CopyCursor);

		return S_OK;
	}

	SetCursor(LoadCursor(NULL, IDC_NO));

	return S_OK;
}

// IDropTarget

HRESULT CDevConNavWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConNavWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConNavWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	return S_OK;
}

HRESULT CDevConNavWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

// Overridables

void CDevConNavWnd::OnAttach(void)
{
	m_pItem = (CDevCon *) CBaseClass::m_pItem;
}

CString CDevConNavWnd::OnGetNavPos(void)
{
	if( m_pSelect ) {

		return m_pSelect->m_pItem->GetFixedPath();
	}

	return m_pItem->GetFixedPath();
}

BOOL CDevConNavWnd::OnNavigate(CString const &Nav)
{
	if( Nav == m_pItem->GetFixedPath() ) {

		m_pTree->SelectItem(m_hRoot);
	}
	else {
		INDEX i = m_Names.FindName(Nav);

		if( m_Names.Failed(i) ) {

			m_pTree->SelectItem(m_hRoot);
		}
		else {
			HTREEITEM hItem = m_Names.GetData(i);

			if( m_hSelect != hItem ) {

				m_pTree->SelectItem(hItem);
			}

			if( m_pSelect->m_Path.IsEmpty() ) {

				m_System.SetViewedItem(m_pSelect->m_pItem->GetParent());
			}
			else {
				m_System.SetViewedItem(m_pSelect->m_pItem);
			}
		}
	}

	return TRUE;
}

// Message Map

AfxMessageMap(CDevConNavWnd, CBaseClass)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_MOUSEACTIVATE)

	AfxDispatchNotify(100, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown)
	AfxDispatchNotify(100, NM_CUSTOMDRAW, OnTreeCustomDraw)
	AfxDispatchNotify(100, NM_RETURN, OnTreeReturn)
	AfxDispatchNotify(100, NM_DBLCLK, OnTreeDblClk)
	AfxDispatchNotify(100, TVN_BEGINDRAG, OnTreeBeginDrag)
	AfxDispatchNotify(100, TVN_ITEMEXPANDING, OnTreeExpanding)
	AfxDispatchNotify(100, TVN_CONTEXTMENU, OnTreeContextMenu)
	AfxDispatchNotify(100, TVN_OFFERFOCUS, OnTreeOfferFocus)

	AfxMessageEnd(CDevConNavWnd)
};

// Accelerator

BOOL CDevConNavWnd::OnAccelerator(MSG &Msg)
{
	if( m_Accel.Translate(Msg) ) {

		return TRUE;
	}

	return FALSE;
}

// Message Handlers

void CDevConNavWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	m_pTree->Create(m_dwStyle, GetClientRect() - 1, ThisObject, 100);

	m_pTree->SetFont(afxFont(Dialog));

	LoadImageList();

	m_pTree->SetImageList(TVSIL_NORMAL, m_Images);

	m_pTree->SetImageList(TVSIL_STATE, m_Images);

	m_fLoading = TRUE;

	LoadTree();

	m_fLoading = FALSE;

	SelectInit();

	m_DropHelp.Bind(m_hWnd, this);

	m_pTree->ShowWindow(SW_SHOW);

	m_System.RegisterForUpdates(this);
}

void CDevConNavWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"DevConTool"));
	}
}

void CDevConNavWnd::OnSize(UINT uCode, CSize Size)
{
	m_pTree->MoveWindow(GetClientRect() - 1, TRUE);
}

void CDevConNavWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( fShow ) {

		if( !m_System.IsNavBusy() ) {

			SetViewedItem(TRUE);
		}
	}
}

void CDevConNavWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.FrameRect(Rect, afxBrush(WHITE));
}

void CDevConNavWnd::OnSetFocus(CWnd &Wnd)
{
	m_pTree->SetFocus();
}

UINT CDevConNavWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	return AfxCallDefProc();
}

// Notification Handlers

void CDevConNavWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			m_hSelect = Info.itemNew.hItem;

			if( m_hSelect == m_hRoot || m_hSelect == NULL ) {

				m_pSelect = NULL;
			}
			else {
				m_pSelect = &m_Nodes[m_hSelect];
			}

			/*if( !m_fLocked ) {*/

			SetViewedItem(FALSE);
		}
	}
}

BOOL CDevConNavWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_TAB:

			m_System.FlipToItemView(TRUE);

			return TRUE;

		case VK_ESCAPE:

			afxMainWnd->PostMessage(WM_CANCELMODE);

			return TRUE;
	}

	return FALSE;
}

UINT CDevConNavWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
	}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM       hItem = HTREEITEM(Info.dwItemSpec);

		if( hItem ) {

			UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

			if( uTest & TVIS_DROPHILITED ) {

				Extra.clrTextBk = afxColor(Orange2);

				Extra.clrText   = afxColor(BLACK);

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
			}
		}

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				Extra.clrTextBk = afxColor(Orange2);
			}
			else
				Extra.clrTextBk = afxColor(Orange3);

			Extra.clrText = afxColor(BLACK);

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
		}
	}

	return 0;
}

void CDevConNavWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{
	if( m_pTree->GetChild(m_hSelect) ) {

		ToggleItem(m_hSelect);

		return;
	}
}

void CDevConNavWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( m_pTree->GetChild(m_hSelect) ) {

			ToggleItem(m_hSelect);

			return;
		}
	}
}

void CDevConNavWnd::OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info)
{
	HTREEITEM hItem = Info.itemNew.hItem;

	m_pTree->SelectItem(hItem);

	afxMainWnd->PostMessage(WM_CANCELMODE);

	DragItem();
}

BOOL CDevConNavWnd::OnTreeExpanding(UINT uID, NMTREEVIEW &Info)
{
	if( Info.itemNew.hItem == m_hRoot ) {

		if( Info.action == TVE_COLLAPSE ) {

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

void CDevConNavWnd::OnTreeContextMenu(UINT uID, NMTREEVIEW &Info)
{
	CString Name;

	if( FALSE ) {

		// Hook Menu Here
	}

	if( !Name.IsEmpty() ) {

		CMenu   Load = CMenu(PCTXT(Name));

		CMenu & Menu = Load.GetSubMenu(0);

		Menu.SendInitMessage();

		Menu.DeleteDisabled();

		if( Menu.GetMenuItemCount() ) {

			m_pTree->ClientToScreen(Info.ptDrag);

			Menu.MakeOwnerDraw(FALSE);

			Menu.TrackPopupMenu(TPM_LEFTALIGN,
					    Info.ptDrag,
					    afxMainWnd->GetHandle()
			);

			Menu.FreeOwnerDraw();
		}
	}
}

void CDevConNavWnd::OnTreeOfferFocus(UINT uID, NMTREEVIEW &Info)
{
	m_pTree->SetFocus();
}

// Data Object Construction

BOOL CDevConNavWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	return MakeDataObject(pData);
}

BOOL CDevConNavWnd::MakeDataObject(IDataObject * &pData)
{
	return FALSE;
}

// Drag Support

BOOL CDevConNavWnd::DragItem(void)
{
	IDataObject *pData = NULL;

	if( MakeDataObject(pData, TRUE) ) {

		m_System.LockResPane(TRUE);

		FindDragMetrics();

		MakeDragImage();

		CDragHelper *pHelp = New CDragHelper;

		pHelp->AddImage(pData, m_DragImage, m_DragSize, m_DragOffset);

		delete pHelp;

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, 0);

		m_pTree->UpdateWindow();

		DWORD dwAllow  = FindDragAllowed();

		DWORD dwResult = DROPEFFECT_NONE;

		DoDragDrop(pData, this, dwAllow, &dwResult);

		DragComplete(dwResult);

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, m_dwStyle);

		pData->Release();

		m_System.LockResPane(FALSE);

		return TRUE;
	}

	m_System.LockResPane(FALSE);

	return FALSE;
}

// Drag Hooks

void CDevConNavWnd::FindDragMetrics(void)
{
	CRect Rect = m_pTree->GetItemRect(m_hSelect, TRUE);

	m_pTree->ClientToScreen(Rect);

	Rect.left    = Rect.left - 16;

	m_DragPos    = GetCursorPos();

	m_DragSize   = Rect.GetSize();

	m_DragOffset = m_DragPos - Rect.GetTopLeft();

	MakeMin(m_DragOffset.cx, Rect.cx());

	MakeMin(m_DragOffset.cy, Rect.cy());

	MakeMax(m_DragOffset.cx, 0);

	MakeMax(m_DragOffset.cy, 0);
}

void CDevConNavWnd::MakeDragImage(void)
{
	CRect Rect = m_DragSize;

	m_DragImage.Create(CClientDC(NULL), m_DragSize);

	CMemoryDC DC;

	DC.Select(m_DragImage);

	DC.FillRect(Rect, afxBrush(MAGENTA));

	DC.SetTextColor(RGB(1, 1, 1));

	DC.Select(afxFont(DialogNA));

	DC.SetBkMode(TRANSPARENT);

	CString Text   = m_pTree->GetItemText(m_hSelect);

	UINT    uImage = m_pTree->GetItemImage(m_hSelect);

	CSize   Size   = DC.GetTextExtent(Text);

	CPoint  Origin = CPoint(16, 0);

	CSize   Adjust = Rect.GetSize() - Size - CSize(16, 0);

	DC.TextOut(Origin + Adjust / 2, Text);

	m_Images.Draw(uImage, DC, 0, 0, ILD_NORMAL);

	DC.Deselect();

	DC.Deselect();
}

DWORD CDevConNavWnd::FindDragAllowed(void)
{
	return DROPEFFECT_COPY;
}

void CDevConNavWnd::DragComplete(DWORD dwEffect)
{
}

// Tree Loading

void CDevConNavWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"ProgTreeIcon16"), afxColor(MAGENTA));
}

void CDevConNavWnd::LoadTree(void)
{
	afxThread->SetWaitMode(TRUE);

	afxThread->SetStatusText(IDS("Loading View..."));

	m_Nodes.Empty();

	m_Names.Empty();

	CTreeViewItem Root;

	Root.SetText(IDS("Device Configuration"));

	Root.SetImages(2 + 3 * 4);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Root);

	LoadPart(m_hRoot, m_pItem->m_pHConfig);

	LoadPart(m_hRoot, m_pItem->m_pSConfig);

	LoadPart(m_hRoot, m_pItem->m_pPConfig);

	LoadPart(m_hRoot, m_pItem->m_pUConfig);

	afxThread->SetStatusText(L"");

	afxThread->SetWaitMode(FALSE);

	SelectRoot();
}

void CDevConNavWnd::LoadPart(HTREEITEM hRoot, CDevConPart *pPart)
{
	CTreeViewItem Part;

	Part.SetText(pPart->GetHumanName());

	Part.SetImages(pPart->GetTreeImage());

	HTREEITEM  hPart   = m_pTree->InsertItem(m_hRoot, NULL, Part);

	CJsonData *pSchema = pPart->GetSchema();

	LoadChildren(hPart, pPart, L"", L"", pSchema);

	m_pTree->Expand(hRoot, TVE_EXPAND);

	m_Names.Insert(pPart->GetFixedPath(), hPart);
}

void CDevConNavWnd::LoadChildren(HTREEITEM hRoot, CDevConPart *pPart, CString const &Path, CString const &Label, CJsonData *pNode)
{
	CJsonData *pChildren = pNode->GetChild(L"children");

	if( pChildren ) {

		UINT c = pChildren->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonData *pChild = pChildren->GetChild(n);

			CString ThisLabel = pChild->GetValue(L"label");

			CString ThisName  = pChild->GetValue(L"name");

			CString NextPath  = Path;

			CString NextLabel = Label;

			if( !ThisName.IsEmpty() ) {

				if( !NextPath.IsEmpty() ) {

					NextPath += L".";
				}

				NextPath += ThisName;
			}

			if( !ThisLabel.IsEmpty() ) {

				if( !NextLabel.IsEmpty() ) {

					NextLabel += L" - ";
				}

				NextLabel += ThisLabel;
			}

			CTreeViewItem Item;

			Item.SetText(ThisLabel);

			Item.SetImages(pPart->GetTreeImage());

			HTREEITEM hItem = m_pTree->InsertItem(hRoot, NULL, Item);

			LoadChildren(hItem, pPart, NextPath, NextLabel, pChild);
		}

		if( pNode->GetValue(L"state", L"") != L"collapsed" ) {

			m_pTree->Expand(hRoot, TVE_EXPAND);
		}
	}

	CNode Node;

	Node.m_Path  = Path;

	Node.m_pNode = pNode;

	Node.m_pItem = pPart->GetNodeItem(Path);

	Node.m_pItem->m_pNode = pNode;

	Node.m_pItem->m_Label = Label;

	CJsonData *pTab = pNode->GetChild(L"tabs.0000");

	if( pTab ) {

		CString Note = pTab->GetValue(L"note");

		if( !Note.IsEmpty() ) {

			Node.m_pItem->m_Ordinal = Note;
		}
	}

	m_Names.Insert(Node.m_pItem->GetFixedPath(), hRoot);

	m_Nodes.Insert(hRoot, Node);
}

// Implementation

BOOL CDevConNavWnd::IsParent(void)
{
	return IsParent(m_hSelect);
}

BOOL CDevConNavWnd::IsParent(HTREEITEM hItem)
{
	return m_pTree->GetChild(hItem) ? TRUE : FALSE;
}

BOOL CDevConNavWnd::ExpandItem(HTREEITEM hItem, UINT uAction)
{
	if( IsParent(hItem) ) {

		m_pTree->Expand(hItem, uAction);

		return TRUE;
	}

	return FALSE;
}

void CDevConNavWnd::ExpandItem(HTREEITEM hItem)
{
	if( !m_pTree->IsExpanded(hItem) ) {

		ExpandItem(hItem, TVE_EXPAND);
	}
}

void CDevConNavWnd::ToggleItem(HTREEITEM hItem)
{
	if( hItem != m_hRoot ) {

		UINT uAction = m_pTree->IsExpanded(hItem) ? TVE_COLLAPSE : TVE_EXPAND;

		ExpandItem(hItem, uAction);
	}
}

void CDevConNavWnd::SelectRoot(void)
{
	m_pTree->SelectItem(NULL);

	m_pTree->SelectItem(m_hRoot);

	ExpandItem(m_hRoot);
}

BOOL CDevConNavWnd::SelectInit(void)
{
	if( !m_System.IsNavBusy() ) {

		SelectRoot();
	}

	return FALSE;
}

void CDevConNavWnd::SetRedraw(BOOL fRedraw)
{
	if( fRedraw ) {

		m_pTree->SetRedraw(TRUE);

		m_pTree->Invalidate(FALSE);

		return;
	}

	m_pTree->SetRedraw(FALSE);
}

void CDevConNavWnd::RefreshTree(void)
{
	CString ss = SaveSelState(); // ????

	m_fLoading = TRUE;

	SetRedraw(FALSE);

	m_pTree->DeleteItem(m_hRoot);

	LoadTree();

	m_fLoading = FALSE;

	LoadSelState(ss); // ????

	SetRedraw(TRUE);
}

BOOL CDevConNavWnd::IsReadOnly(void)
{
	return m_pItem->GetDatabase()->IsReadOnly();
}

BOOL CDevConNavWnd::IsPrivate(void)
{
	return m_pItem->GetDatabase()->IsPrivate();
}

void CDevConNavWnd::SetViewedItem(BOOL fCheck)
{
	if( fCheck ) {

		m_System.SetNavCheckpoint();
	}

	if( m_pSelect ) {

		if( m_pSelect->m_Path.IsEmpty() ) {

			m_System.SetViewedItem(m_pSelect->m_pItem->GetParent());
		}
		else {
			m_System.SetViewedItem(m_pSelect->m_pItem);
		}
	}
	else {
		m_System.SetViewedItem(m_pItem);
	}
}

// Selection State

CString CDevConNavWnd::SaveSelState(void)
{
	return m_pSelect ? m_pSelect->m_pItem->GetFixedPath() : L"";
}

void CDevConNavWnd::LoadSelState(CString State)
{
	State.IsEmpty() ? SelectRoot() : Navigate(State);
}

// End of File
