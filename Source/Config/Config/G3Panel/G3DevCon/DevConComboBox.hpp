
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConComboBox_HPP

#define INCLUDE_DevConComboBox_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConElement;

//////////////////////////////////////////////////////////////////////////
//
// Notification Codes
//

#define CBN_DROP	0x8801

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Combo Box
//

class CDevConComboBox : public CComboBox, public IDropTarget
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConComboBox(CDevConElement *pElem);

	// IUnknown
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
	ULONG   METHOD AddRef(void);
	ULONG   METHOD Release(void);

	// IDropTarget
	HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragLeave(void);
	HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

protected:
	// Data Members
	CDevConElement * m_pElem;
	CDropHelper      m_DropHelp;
	DWORD	         m_dwEffect;
	UINT             m_uDrop;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnPostCreate(void);
	void OnPaint(void);

	// Implementation
	void Construct(void);
	BOOL SetDrop(UINT uDrop);
	BOOL IsItemReadOnly(void);
};

// End of File

#endif
