
#include "Intern.hpp"

#include "DevConElement.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConElementIp.hpp"
#include "DevConElementEnum.hpp"
#include "DevConElementNumber.hpp"
#include "DevConElementFloat.hpp"
#include "DevConElementPassword.hpp"
#include "DevConElementFile.hpp"
#include "DevConElementString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration UI Element
//

// Base Class

#define CBaseClass CObject

// Dynamic Class

AfxImplementDynamicClass(CDevConElement, CBaseClass);

// Constructor

CDevConElement::CDevConElement(void)
{
	m_fShow   = FALSE;

	m_pPerson = NULL;
}

// Destructor

CDevConElement::~CDevConElement(void)
{
}

// Creation

CDevConElement * CDevConElement::Create(CString const &Type)
{
	CDevConElement *pElem = NULL;

	if( Type == L"ip" ) {

		pElem = New CDevConElementIp;
	}

	if( Type == L"enum" ) {

		pElem = New CDevConElementEnum;
	}

	if( Type == L"number" ) {

		pElem = New CDevConElementNumber;
	}

	if( Type == L"float" || Type == L"gps" ) {

		pElem = New CDevConElementFloat;
	}

	if( Type == L"password" ) {

		pElem = New CDevConElementPassword;
	}

	if( Type == L"file" ) {

		pElem = New CDevConElementFile;
	}

	if( Type == L"string" || Type == L"ssid" || Type == L"hidden" ) {

		pElem = New CDevConElementString;
	}

	AfxAssert(pElem);

	return pElem;
}

// Operations

BOOL CDevConElement::IsReadOnly(void)
{
	return FALSE;
}

void CDevConElement::BindItem(CItem *pItem)
{
	m_pItem = pItem;
}

void CDevConElement::SetPersonality(CJsonData *pPerson)
{
	m_pPerson = pPerson;
}

void CDevConElement::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
}

void CDevConElement::AddLayout(CLayFormation *pForm)
{
}

void CDevConElement::CreateControls(CWnd &Wnd, UINT &id)
{
}

void CDevConElement::ShowControls(BOOL fShow)
{
	if( m_fShow != fShow ) {

		for( UINT n = 0; n < m_Controls.GetCount(); n++ ) {

			m_Controls[n]->ShowWindow(fShow ? SW_SHOW : SW_HIDE);
		}

		m_fShow = fShow;
	}
}

void CDevConElement::DestroyControls(void)
{
	for( UINT n = 0; n < m_Controls.GetCount(); n++ ) {

		m_Controls[n]->DestroyWindow(TRUE);
	}

	m_Controls.Empty();
}

void CDevConElement::EnableControls(BOOL fEnable)
{
	for( UINT n = 0; n < m_Controls.GetCount(); n++ ) {

		CCtrlWnd *pCtrl = m_Controls[n];

		pCtrl->EnableWindow(fEnable && IsControlEnabled(pCtrl));
	}
}

BOOL CDevConElement::FindFocus(CWnd * &pWnd)
{
	for( UINT n = 0; n < m_Controls.GetCount(); n++ ) {

		if( m_Controls[n]->GetWindowStyle() & WS_TABSTOP ) {

			if( m_Controls[n]->IsEnabled() ) {

				pWnd = m_Controls[n];

				return TRUE;
			}
		}
	}

	return FALSE;
}

UINT CDevConElement::OnNotify(UINT uID, UINT uNotify, CWnd &Wnd)
{
	return actionNone;
}

UINT CDevConElement::OnNotify(UINT uID, NMHDR &Info)
{
	return actionNone;
}

BOOL CDevConElement::CanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	// TODO -- Add drag-and-drop support!!!

	return TRUE;
}

BOOL CDevConElement::AcceptData(IDataObject *pData)
{
	return TRUE;
}

CString CDevConElement::FormatData(CString const &Data)
{
	return Data;
}

void CDevConElement::SetData(CString const &Data)
{
	AfxAssert(FALSE);
}

CString CDevConElement::GetData(void)
{
	AfxAssert(FALSE);

	return L"";
}

CString CDevConElement::GetDefault(void)
{
	return L"";
}

void CDevConElement::ScrollData(UINT uCode)
{
}

// Overridables

BOOL CDevConElement::IsControlEnabled(CCtrlWnd *pCtrl)
{
	return TRUE;
}

// Implementation

void CDevConElement::AddControl(CCtrlWnd *pCtrl)
{
	m_Controls.Append(pCtrl);
}

// End of File
