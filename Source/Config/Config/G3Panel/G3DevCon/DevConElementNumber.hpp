
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementNumber_HPP

#define INCLUDE_DevConElementNumber_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementEditBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Number UI Element
//

class CDevConElementNumber : public CDevConElementEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementNumber(void);

	// Destructor
	~CDevConElementNumber(void);

	// Operations
	void CreateControls(CWnd &Wnd, UINT &id);
	void ParseConfig(CJsonData *pSchema, CJsonData *pField);

protected:
	// Data Members
	CString m_Default;
	UINT    m_uDef;
	UINT    m_uMin;
	UINT    m_uMax;

	// Overridables
	void OnSetData(void);
	BOOL OnCheckData(CString &Data);
};

// End of File

#endif
