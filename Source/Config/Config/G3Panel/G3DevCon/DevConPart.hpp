
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConPart_HPP

#define INCLUDE_DevConPart_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevCon;

class CDevConNode;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Part
//

class DLLAPI CDevConPart : public CUIItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConPart(char cTag);

	// Destructor
	~CDevConPart(void);

	// UI Creation
	CViewWnd * CreateView(UINT uType);

	// Item Display
	CString GetHumanName(void) const;
	CString GetFileName(void) const;
	UINT    GetTreeImage(void) const;

	// Attributes
	CJsonData * GetConfig(void) const;
	CJsonData * GetSchema(void) const;

	// Operations
	void ImportFromText(void);
	void SetConfig(CString const &Text);
	void FindSchema(void);
	void UpdateSchema(void);
	void CommitEdits(void);

	// Node Creation
	CDevConNode * GetNodeItem(CString const &Path);

	// Persistance
	void Init(void);
	void Kill(void);
	void Save(CTreeFile &Tree);
	void PostLoad(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);
	BOOL MakeInitData(CByteArray &Data);

	// Item Properties
	char    m_cTag;
	UINT    m_Handle;
	UINT	m_Enable;
	CString m_Config;
	CString m_Schema;

protected:
	// Node Item Map
	CZeroMap <CString, CDevConNode *> m_Nodes;

	// Data Members
	CDevCon   * m_pDevCon;
	CJsonData * m_pConfig;
	CJsonData * m_pSchema;

	// Active Config
	virtual CString const & GetInitJson(void);

	// Meta Data Creation
	void AddMetaData(void);

	// Dirty Control
	void OnSetDirty(void);
};

// End of File

#endif
