
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConDialog_HPP

#define INCLUDE_DevConDialog_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Referenced Classes
//

//////////////////////////////////////////////////////////////////////////
//								
// Device Configuration Dialog
//

class DLLAPI CDevConDialog : public CDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConDialog(CString Caption, CViewWnd *pView);

	// Dialog Operations
	BOOL Create(CWnd &Parent);

protected:
	// Data Members
	CViewWnd   * m_pView;
	CSize	     m_Border;
	CSize	     m_Button;
	CButton	   * m_pButton1;
	CButton	   * m_pButton2;

	// Class Definition
	PCTXT GetDefaultClassName(void) const;
	BOOL  GetClassDetails(WNDCLASSEX &Class) const;

	// Routing Control
	BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

	// Message Map
	AfxDeclareMessageMap();

	// Command Handlers
	BOOL OnButton(UINT uID);

	// Message Handlers
	void OnPostCreate(void);
	void OnClose(void);
	void OnSetFocus(CWnd &Wnd);
	void OnSize(UINT uCode, CSize Size);

	// Implemenataion
	void  FindLayout(void);
	void  CreateView(void);
	void  CreateButtons(void);
	void  FindBestSize(void);
	CRect GetViewRect(void);
	void  MoveButtons(void);
};

// End of File

#endif
