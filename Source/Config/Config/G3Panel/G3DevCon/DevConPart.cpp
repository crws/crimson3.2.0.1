
#include "Intern.hpp"

#include "DevConPart.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConPartWnd.hpp"

#include "DevCon.hpp"

#include "DevConNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Part
//

// Base Class

#define CBaseClass CUIItem

// Runtime Class

AfxImplementRuntimeClass(CDevConPart, CBaseClass);

// Constructor

CDevConPart::CDevConPart(char cTag) : m_cTag(cTag)
{
	switch( m_cTag ) {

		case 'h': m_Handle = HANDLE_HCON; break;
		case 'p': m_Handle = HANDLE_PCON; break;
		case 's': m_Handle = HANDLE_SCON; break;
		case 'u': m_Handle = HANDLE_UCON; break;
	}

	m_pDevCon = NULL;

	m_pConfig = New CJsonData;

	m_pSchema = New CJsonData;

	m_Enable  = (cTag == 'u') ? 0 : 1;
}

// Destructor

CDevConPart::~CDevConPart(void)
{
	delete m_pConfig;

	delete m_pSchema;
}

// UI Creation

CViewWnd * CDevConPart::CreateView(UINT uType)
{
	if( uType == viewItem ) {
		
		CUIPageList *pList = New CUIPageList;

		OnLoadPages(pList);

		CUIViewWnd  *pView = New CDevConPartWnd(pList);

		pView->SetBorder(6);

		return pView;
	}

	return CBaseClass::CreateView(uType);
}

// Item Display

CString CDevConPart::GetHumanName(void) const
{
	switch( m_cTag ) {

		case 'h': return IDS("Hardware Configuration");
		case 'p': return IDS("Device Personality");
		case 's': return IDS("System Configuration");
		case 'u': return IDS("User Configuration");
	}

	AfxAssert(FALSE);

	return L"";
}

CString CDevConPart::GetFileName(void) const
{
	CString Name;

	if( m_pDevCon->m_pSchema->GetNaming(m_cTag, Name) ) {

		return Name;
	}

	AfxAssert(FALSE);

	return L"";
}

UINT CDevConPart::GetTreeImage(void) const
{
	switch( m_cTag ) {

		case 'h': return 2 + 2 * 4;
		case 'p': return 2 + 1 * 4;
		case 's': return 2 + 0 * 4;
		case 'u': return 2 + 4 * 4;
	}

	return 2 + 3 * 4;
}

// Attributes

CJsonData * CDevConPart::GetConfig(void) const
{
	return m_pConfig;
}

CJsonData * CDevConPart::GetSchema(void) const
{
	return m_pSchema;
}

// Operations

void CDevConPart::ImportFromText(void)
{
	m_pConfig->Parse(m_Config);
}

void CDevConPart::SetConfig(CString const &Text)
{
	m_pConfig->Parse(Text);

	SetDirty();
}

void CDevConPart::FindSchema(void)
{
	m_pDevCon->m_pSchema->GetSchema(m_cTag, m_Schema);

	m_pSchema->Parse(m_Schema);

	m_pConfig->Parse(m_Config);
}

void CDevConPart::UpdateSchema(void)
{
	m_pDevCon->m_pSchema->GetSchema(m_cTag, m_Schema);

	m_pSchema->Parse(m_Schema);

	for( INDEX i = m_Nodes.GetHead(); !m_Nodes.Failed(i); m_Nodes.GetNext(i) ) {

		CDevConNode *pItem = m_Nodes.GetData(i);

		pItem->m_pSchema   = m_pSchema;
	}
}

void CDevConPart::CommitEdits(void)
{
	m_Config = m_pConfig->GetAsText(FALSE);
}

// Node Creation

CDevConNode * CDevConPart::GetNodeItem(CString const &Path)
{
	INDEX i = m_Nodes.FindName(Path);

	if( m_Nodes.Failed(i) ) {

		CDevConNode *pItem = New CDevConNode;

		pItem->SetParent(this);

		pItem->Init();

		pItem->m_cTag    = m_cTag;

		pItem->m_pData   = m_pConfig;

		pItem->m_pSchema = m_pSchema;

		pItem->m_pNode   = NULL;

		pItem->m_Path    = Path;

		pItem->m_pPerson = NULL;

		if( m_cTag == 's' ) {

			CDevCon *pDevCon = (CDevCon *) GetParent();

			pItem->m_pPerson = pDevCon->m_pPConfig->GetConfig();
		}

		m_Nodes.Insert(Path, pItem);

		return pItem;
	}

	return m_Nodes.GetData(i);
}

// Persistance

void CDevConPart::Init(void)
{
	m_pDevCon = (CDevCon *) GetParent();

	m_pDevCon->m_pSchema->GetDefault(m_cTag, m_Config);

	CBaseClass::Init();
}

void CDevConPart::Kill(void)
{
	for( INDEX i = m_Nodes.GetHead(); !m_Nodes.Failed(i); m_Nodes.GetNext(i) ) {

		CDevConNode *pItem = m_Nodes.GetData(i);

		pItem->Kill();

		delete pItem;
	}

	CBaseClass::Kill();
}

void CDevConPart::PostLoad(void)
{
	m_pDevCon = (CDevCon *) GetParent();

	CBaseClass::PostLoad();
}

void CDevConPart::Save(CTreeFile &Tree)
{
	m_Config = m_pConfig->GetAsText(FALSE);

	CBaseClass::Save(Tree);
}

// Download Support

BOOL CDevConPart::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddWord(WORD(m_Enable));

	MakeInitData((CByteArray &) Init);

	return TRUE;
}

BOOL CDevConPart::MakeInitData(CByteArray &Data)
{
	if( m_Enable ) {

		CString const &Config = GetInitJson();

		UINT  uData = Config.GetLength() + 1;

		PCTXT pData = PCTXT(Config);

		for( UINT n = 0; n < uData; n++ ) {

			Data.Append(BYTE(pData[n]));
		}

		return TRUE;
	}

	return FALSE;
}

// Active Config

CString const & CDevConPart::GetInitJson(void)
{
	m_Config = m_pConfig->GetAsText(FALSE);

	return m_Config;
}

// Meta Data Creation

void CDevConPart::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Handle);

	Meta_AddString(Config);

	Meta_AddInteger(Enable);

	Meta_SetName(IDS("Device Configuration Part"));
}

// Dirty Control

void CDevConPart::OnSetDirty(void)
{
	if( m_cTag == 'p' ) {

		CSysProxy System;

		System.Bind();

		System.ItemUpdated(m_pDevCon, updateProps);
	}

	CBaseClass::OnSetDirty();
}

// End of File
