
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConModeButton_HPP

#define INCLUDE_DevConModeButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConDropButton.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Mode Button
//

class CDevConModeButton : public CDevConDropButton
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConModeButton(CDevConElement *pElem);

	// Option Data
	struct COption
	{
		UINT	m_uID;
		CString	m_Text;
		BOOL	m_fEnable;
		BOOL    m_fHidden;
		DWORD	m_Image;
	};

	// Creation
	BOOL Create(CRect const &Rect, HWND hParent, UINT uID);

	// Attributes
	CSize GetSize(void);
	CSize GetSize(CDC &DC);
	UINT  GetData(void) const;

	// Operations
	void ClearOptions(void);
	BOOL AddOption(COption const &Opt);
	void SetData(UINT uData);
	UINT ShowMenu(void);

protected:
	// Data Member
	UINT                m_uData;
	UINT                m_uSlot;
	CArray <COption>    m_Opt;
	CMap   <UINT, UINT> m_Map;
	CSize               m_Size;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	UINT OnGetDlgCode(MSG *pMsg);
	void OnChar(UINT uCode, DWORD dwFlags);
	void OnEnable(BOOL fEnable);
	void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);

	// Notification Handlers
	UINT OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

	// Menu Control
	BOOL OnControl(UINT uID, CCmdSource &Src);
	BOOL OnGetInfo(UINT uID, CCmdInfo &Info);

	// Implementation
	void CalcSize(CDC &DC);
	BOOL Update(void);
};

// End of File

#endif
