
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConTabWnd_CmdEdit_HPP

#define INCLUDE_DevConTabWnd_CmdEdit_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConTabWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Editing Command
//

class CDevConTabWnd::CCmdEdit : public CStdCmd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CCmdEdit(CString Menu, CString Item, CString Name, CString Prev, CString Data);

	// Data Members
	CString m_Name;
	CString m_Prev;
	CString m_Data;
};

// End of File

#endif
