
#include "Intern.hpp"

#include "DevConGroupLabel.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Group Control Label
//

// Dynamic Class

AfxImplementDynamicClass(CDevConGroupLabel, CCtrlWnd);

// Constructor

CDevConGroupLabel::CDevConGroupLabel(void)
{
}

// Message Map

AfxMessageMap(CDevConGroupLabel, CCtrlWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)
		AfxDispatchMessage(WM_PAINT)
		AfxDispatchMessage(WM_LBUTTONDOWN)

		AfxMessageEnd(CDevConGroupLabel)
};

// Message Handlers

BOOL CDevConGroupLabel::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
}

void CDevConGroupLabel::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.Select(afxFont(Bolder));

	DC.SetTextColor(afxColor(TabGroup));

	DC.SetBkColor(afxColor(TabFace));

	CRect   Rect = GetClientRect();

	CString Text = GetWindowText();

	CSize   Size = DC.GetTextExtent(Text);

	DC.FillRect(Rect, afxBrush(TabFace));

	Rect.left += 0;

	Rect.top  += 4;

	DC.TextOut(Rect.left, Rect.top, Text);

	Rect.left += Size.cx + 8;

	Rect.top  += Size.cy / 2;

	DC.DrawTopEdge(Rect, 1, afxBrush(3dShadow));

	DC.Deselect();
}

void CDevConGroupLabel::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	CWnd *pCtrl = this;

	for( ;;) {

		if( (pCtrl = pCtrl->GetWindowPtr(GW_HWNDPREV))->IsWindow() ) {

			if( pCtrl->IsKindOf(AfxRuntimeClass(CDevConGroupLabel)) ) {

				break;
			}

			continue;
		}

		pCtrl = GetWindowPtr(GW_HWNDFIRST);

		break;
	}

	while( pCtrl->IsWindow() ) {

		if( pCtrl->GetWindowStyle() & WS_TABSTOP ) {

			if( pCtrl->IsWindowEnabled() ) {

				pCtrl->SetFocus();

				break;
			}
		}

		pCtrl = pCtrl->GetWindowPtr(GW_HWNDNEXT);
	}
}

// End of File
