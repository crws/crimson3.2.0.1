
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IpAddr_HPP

#define	INCLUDE_IpAddr_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CString;

//////////////////////////////////////////////////////////////////////////
//
// Wrapper Reference
//

typedef class CIpAddr const & IPREF;

//////////////////////////////////////////////////////////////////////////
//								
// Standard IP Addresses
//

#define	IP_EMPTY ((IPREF) CIpAddr::m_Empty)
#define	IP_BROAD ((IPREF) CIpAddr::m_Broad)
#define IP_MULTI ((IPREF) CIpAddr::m_Multi)
#define IP_EXPER ((IPREF) CIpAddr::m_Exper)
#define	IP_LOOP	 ((IPREF) CIpAddr::m_Loop)

//////////////////////////////////////////////////////////////////////////////
//
// IP Address Record
//

#pragma pack(1)

struct IPADDR
{
	union {
		struct
		{
			BYTE	m_b1;
			BYTE	m_b2;
			BYTE	m_b3;
			BYTE	m_b4;
		};

		struct
		{
			BYTE	m_b[4];
		};

		struct
		{
			DWORD	m_dw;
		};
	};
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////////
//
// IP Address Wrapper
//

class DLLAPI CIpAddr : public IPADDR
{
public:
	// Constructors
	STRONG_INLINE CIpAddr(void);
	STRONG_INLINE CIpAddr(IPADDR const &Addr);
	STRONG_INLINE CIpAddr(BYTE b1, BYTE b2, BYTE b3, BYTE b4);
	STRONG_INLINE CIpAddr(PCTXT pText);
	STRONG_INLINE CIpAddr(DWORD ip);

	// Simple Assignment
	STRONG_INLINE CIpAddr const & operator = (IPADDR  const &Addr);

	// String Assignment
	CIpAddr const & operator = (PCTXT pText);

	// Value Access
	STRONG_INLINE DWORD GetValue(void) const;

	// Comparison Operators
	STRONG_INLINE BOOL operator == (IPADDR const &That) const;
	STRONG_INLINE BOOL operator != (IPADDR const &That) const;

	// Bitwise Operators
	DWORD operator & (IPREF IP) const;
	DWORD operator | (IPREF IP) const;
	DWORD operator ~ (void)     const;

	// Conversion
	CString GetAsText(void) const;

	// Attributes
	BOOL IsSpecial(void) const;
	BOOL IsPrivate(void) const;
	UINT GetBitCount(void) const;
	BOOL IsValidMask(void) const;

	// Attributes
	STRONG_INLINE BOOL IsBroadcast(void) const;
	STRONG_INLINE BOOL IsBroadcast(IPREF Mask) const;
	STRONG_INLINE BOOL IsDirected(void) const;
	STRONG_INLINE BOOL IsMulticast(void) const;
	STRONG_INLINE BOOL IsGroup(void) const;
	STRONG_INLINE BOOL IsExperimental(void) const;
	STRONG_INLINE BOOL IsEmpty(void) const;
	STRONG_INLINE BOOL IsLoopback(void) const;
	STRONG_INLINE BOOL OnSubnet(IPREF IP, IPREF Mask) const;
	STRONG_INLINE UINT GetDirection(void) const;

	// Operations
	STRONG_INLINE void MakeBroadcast(void);
	STRONG_INLINE void MakeDirected(UINT uFace);
	STRONG_INLINE void MakeMulticast(void);
	STRONG_INLINE void MakeExperimental(void);
	STRONG_INLINE void MakeEmpty(void);
	STRONG_INLINE void MakeLoopback(void);

	// Static Data
	static IPADDR const m_Broad;
	static IPADDR const m_Multi;
	static IPADDR const m_Exper;
	static IPADDR const m_Empty;
	static IPADDR const m_Loop;
};

////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

STRONG_INLINE CIpAddr::CIpAddr(void)
{
	m_dw = m_Empty.m_dw;
}

STRONG_INLINE CIpAddr::CIpAddr(IPADDR const &Addr)
{
	m_dw = Addr.m_dw;
}

STRONG_INLINE CIpAddr::CIpAddr(BYTE b1, BYTE b2, BYTE b3, BYTE b4)
{
	m_b1 = b1;
	m_b2 = b2;
	m_b3 = b3;
	m_b4 = b4;
}

STRONG_INLINE CIpAddr::CIpAddr(PCTXT pText)
{
	operator = (pText);
}

STRONG_INLINE CIpAddr::CIpAddr(DWORD ip)
{
	m_dw = ip;
}

// Simple Assignment

STRONG_INLINE CIpAddr const & CIpAddr::operator = (IPADDR const &Addr)
{
	m_dw = Addr.m_dw;

	return *this;
}

// Comparison Operators

STRONG_INLINE BOOL CIpAddr::operator == (IPADDR const &That) const
{
	return m_dw == That.m_dw;
}

STRONG_INLINE BOOL CIpAddr::operator != (IPADDR const &That) const
{
	return m_dw != That.m_dw;
}

// Bitwise Operators

STRONG_INLINE DWORD CIpAddr::operator & (IPREF IP) const
{
	return m_dw & IP.m_dw;
}

STRONG_INLINE DWORD CIpAddr::operator | (IPREF IP) const
{
	return m_dw | IP.m_dw;
}

STRONG_INLINE DWORD CIpAddr::operator ~ (void) const
{
	return ~m_dw;
}

// Value Access

STRONG_INLINE DWORD CIpAddr::GetValue(void) const
{
	return m_dw;
}

// Attributes

STRONG_INLINE BOOL CIpAddr::IsBroadcast(void) const
{
	return m_b4 == 0xFF;
}

STRONG_INLINE BOOL CIpAddr::IsBroadcast(IPREF Mask) const
{
	if( Mask == IP_BROAD ) {

		return operator == (m_Broad);
	}

	return (m_dw & ~Mask.m_dw) == ~Mask.m_dw;
}

STRONG_INLINE BOOL CIpAddr::IsDirected(void) const
{
	return m_b1 == 0xFF && m_b2 == 0xFF && m_b3 == 0xFF && m_b4 != 0xFF;
}

STRONG_INLINE BOOL CIpAddr::IsMulticast(void) const
{
	return (m_b1 & 0xF0) == 0xE0;
}

STRONG_INLINE BOOL CIpAddr::IsGroup(void) const
{
	return m_b4 == 0xFF || (m_b1 & 0xF0) == 0xE0;
}

STRONG_INLINE BOOL CIpAddr::IsExperimental(void) const
{
	return (m_b1 & 0xF0) == 0xF0 && (m_b1 & 0x0F) != 0x0F;
}

STRONG_INLINE BOOL CIpAddr::IsEmpty(void) const
{
	return operator == (m_Empty);
}

STRONG_INLINE BOOL CIpAddr::IsLoopback(void) const
{
	return m_b1 == 0x7F;
}

STRONG_INLINE BOOL CIpAddr::OnSubnet(IPREF IP, IPREF Mask) const
{
	return (IP.m_dw & Mask.m_dw) == (m_dw & Mask.m_dw);
}

STRONG_INLINE UINT CIpAddr::GetDirection(void) const
{
	return m_b4;
}

// Operations

STRONG_INLINE void CIpAddr::MakeBroadcast(void)
{
	m_dw = m_Broad.m_dw;
}

STRONG_INLINE void CIpAddr::MakeDirected(UINT uFace)
{
	MakeBroadcast();

	m_b4 = BYTE(uFace);
}

STRONG_INLINE void CIpAddr::MakeMulticast(void)
{
	m_dw = m_Multi.m_dw;
}

STRONG_INLINE void CIpAddr::MakeExperimental(void)
{
	m_dw = m_Exper.m_dw;
}

STRONG_INLINE void CIpAddr::MakeEmpty(void)
{
	m_dw = m_Empty.m_dw;
}

STRONG_INLINE void CIpAddr::MakeLoopback(void)
{
	m_dw = m_Loop.m_dw;
}

// End of File

#endif
