
#include "Intern.hpp"

#include "DevConElementNumber.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Number UI Element
//

// Base Class

#define CBaseClass CDevConElementEditBox

// Dynamic Class

AfxImplementDynamicClass(CDevConElementNumber, CBaseClass);

// Constructor

CDevConElementNumber::CDevConElementNumber(void)
{
}

// Destructor

CDevConElementNumber::~CDevConElementNumber(void)
{
}

// Operations

void CDevConElementNumber::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	m_pEditCtrl->SetContent(EC_NUMBER);

	m_pEditCtrl->SetDefault(m_Default);
}

void CDevConElementNumber::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CStringArray List;

	pField->GetValue(L"format").Tokenize(List, ',');

	m_uDef    = !List[0].IsEmpty() ? watoi(List[0]) : 0;

	m_uMin    = !List[1].IsEmpty() ? watoi(List[1]) : 0;

	m_uMax    = !List[2].IsEmpty() ? watoi(List[2]) : 99999;

	m_Units   = List[3];

	m_Default = List[4];

	m_uWidth  = 10;
}

// Overridables

void CDevConElementNumber::OnSetData(void)
{
	if( m_Data.IsEmpty() ) {

		m_Data.Printf(L"%u", m_uDef);
	}

	if( !m_Default.IsEmpty() ) {

		if( m_Data == CPrintf(L"%u", m_uMin) ) {

			SetEditCtrl(L"");

			return;
		}
	}

	CBaseClass::OnSetData();
}

BOOL CDevConElementNumber::OnCheckData(CString &Data)
{
	if( !m_Default.IsEmpty() ) {

		if( Data.IsEmpty() ) {

			Data.Printf(L"%u", m_uMin);

			return TRUE;
		}
	}

	UINT uData = watoi(Data);

	if( uData < m_uMin || uData > m_uMax ) {

		CPrintf Error(IDS("The value must be between %u and %u."), m_uMin, m_uMax);

		m_pEditCtrl->Error(Error);

		return FALSE;
	}

	return TRUE;
}

// End of File
