
#include "Intern.hpp"

#include "DevConElementLabel.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Label UI Element
//

// Base Class

#define CBaseClass CDevConElement

// Runtime Class

AfxImplementRuntimeClass(CDevConElementLabel, CBaseClass);

// Constructor

CDevConElementLabel::CDevConElementLabel(CString const &Label, BOOL fIndent) : m_Label(Label)
{
	m_fIndent     = fIndent;

	m_pEditLayout = NULL;

	m_pMainLayout = NULL;

	m_pLabelCtrl  = New CStatic;
}

// Destructor

CDevConElementLabel::~CDevConElementLabel(void)
{
}

// Operations

void CDevConElementLabel::AddLayout(CLayFormation *pForm)
{
	CRect Rect(m_fIndent ? 24 : 4, 4, 16, 4);

	m_pEditLayout = New CLayItemText(CString(L'X', 20), 1, 1);

	m_pMainLayout = New CLayFormPad(m_pEditLayout, Rect, horzLeft | vertCenter);

	pForm->AddItem(m_pMainLayout);
}

void CDevConElementLabel::CreateControls(CWnd &Wnd, UINT &id)
{
	CRect Rect = m_pEditLayout->GetRect();

	m_pLabelCtrl->Create(m_Label,
			     WS_CHILD | SS_LEFT,
			     Rect,
			     Wnd,
			     id++
	);

	m_pLabelCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pLabelCtrl);
}

// End of File
