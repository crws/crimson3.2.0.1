
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementFloat_HPP

#define INCLUDE_DevConElementFloat_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementEditBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Float UI Element
//

class CDevConElementFloat : public CDevConElementEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementFloat(void);

	// Destructor
	~CDevConElementFloat(void);

	// Operations
	void CreateControls(CWnd &Wnd, UINT &id);
	void ParseConfig(CJsonData *pSchema, CJsonData *pField);

protected:
	// Data Members
	double m_rDef;
	double m_rMin;
	double m_rMax;

	// Overridables
	void OnSetData(void);
	BOOL OnCheckData(CString &Data);
};

// End of File

#endif
