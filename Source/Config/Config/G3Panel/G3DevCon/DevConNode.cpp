
#include "Intern.hpp"

#include "DevConNode.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConNodeWnd.hpp"

#include "DevConTabWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Node
//

// Base Class

#define CBaseClass CMetaItem

// Dynamic Class

AfxImplementDynamicClass(CDevConNode, CBaseClass);

// Constructor

CDevConNode::CDevConNode(void)
{
}

// UI Creation

CViewWnd * CDevConNode::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		if( m_Path.IsEmpty() ) {

			CItem *pPart = GetParent();

			return pPart->CreateView(uType);
		}
		else {
			CJsonData *pTabs = m_pNode->GetChild(L"tabs");

			if( pTabs ) {

				if( pTabs->GetCount() > 1 ) {

					return New CDevConNodeWnd;
				}

				CJsonData *pTab = pTabs->GetChild(0U);

				return New CDevConTabWnd(m_pData, m_pSchema, m_pPerson, m_Path, pTab);
			}
		}
	}

	return CBaseClass::CreateView(uType);
}

// Item Naming

CString CDevConNode::GetHumanName(void) const
{
	return m_Label;
}

CString CDevConNode::GetItemOrdinal(void) const
{
	return m_Ordinal;
}

// End of File
