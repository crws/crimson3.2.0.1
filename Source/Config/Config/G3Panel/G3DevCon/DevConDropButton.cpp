
#include "Intern.hpp"

#include "DevConDropButton.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Drop-Target Button
//

// Base Class

#define CBaseClass CButton

// Runtime Class

AfxImplementRuntimeClass(CDevConDropButton, CBaseClass);

// Constructor

CDevConDropButton::CDevConDropButton(CDevConElement *pElem)
{
	m_pElem = pElem;

	m_uDrop = 0;
}

// IUnknown

HRESULT CDevConDropButton::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
		}

		return E_NOINTERFACE;
	}

	return E_POINTER;
}

ULONG CDevConDropButton::AddRef(void)
{
	return 1;
}

ULONG CDevConDropButton::Release(void)
{
	return 1;
}

// IDropTarget

HRESULT CDevConDropButton::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( m_pElem ) {

		if( !IsItemReadOnly() ) {

			if( m_pElem->CanAcceptData(pData, *pEffect) ) {

				m_dwEffect = *pEffect;

				SetDrop(1);

				return S_OK;
			}
		}
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConDropButton::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConDropButton::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
}

HRESULT CDevConDropButton::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		if( m_pElem->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			SendNotify(BN_DROP);

			return S_OK;
		}
	}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);

	return S_OK;
}

// Message Map

AfxMessageMap(CDevConDropButton, CBaseClass)
{
	AfxDispatchMessage(WM_POSTCREATE)

		AfxDispatchNotify(0, NM_CUSTOMDRAW, OnCustomDraw)

		AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
		AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

		AfxMessageEnd(CDevConDropButton)
};

// Message Handlers

void CDevConDropButton::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
}

// Notification Handlers

UINT CDevConDropButton::OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYPOSTPAINT;
	}

	if( Info.dwDrawStage == CDDS_POSTPAINT ) {

		if( m_uDrop ) {

			CDC DC(Info.hdc);

			CRect Rect = Info.rc;

			DC.FrameRect(Rect, afxBrush(Orange1));

			DC.Detach(FALSE);
		}
	}

	return 0;
}

// Command Handlers

BOOL CDevConDropButton::OnPasteControl(UINT uID, CCmdSource &Src)
{
	if( m_pElem ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			DWORD dwEffect;

			if( m_pElem->CanAcceptData(pData, dwEffect) ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
			}

			pData->Release();
		}
	}

	return FALSE;
}

BOOL CDevConDropButton::OnPasteCommand(UINT uID)
{
	if( m_pElem ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			if( m_pElem->AcceptData(pData) ) {

				pData->Release();

				return TRUE;
			}

			pData->Release();
		}
	}

	return FALSE;
}

// Implementation

BOOL CDevConDropButton::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
	}

	return FALSE;
}

BOOL CDevConDropButton::IsItemReadOnly(void)
{
	return m_pElem->IsReadOnly();
}

// End of File
