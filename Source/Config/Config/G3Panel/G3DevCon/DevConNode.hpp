
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConNode_HPP

#define INCLUDE_DevConNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Node
//

class CDevConNode : public CMetaItem
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConNode(void);

	// UI Creation
	CViewWnd * CreateView(UINT uType);

	// Item Naming
	CString GetHumanName(void) const;
	CString GetItemOrdinal(void) const;

	// Item Properties
	char	    m_cTag;
	CString     m_Path;
	CString     m_Label;
	CString	    m_Ordinal;
	CJsonData * m_pData;
	CJsonData * m_pSchema;
	CJsonData * m_pNode;
	CJsonData * m_pPerson;
};

// End of File

#endif
