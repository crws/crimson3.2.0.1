
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConDropButton_HPP

#define INCLUDE_DevConDropButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConElement;

//////////////////////////////////////////////////////////////////////////
//
// Notification Codes
//

#define BN_DROP		0x8804

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Drop-Target Button
//

class CDevConDropButton : public CButton, public IDropTarget
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConDropButton(CDevConElement *pElem);

	// IUnknown
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
	ULONG   METHOD AddRef(void);
	ULONG   METHOD Release(void);

	// IDropTarget
	HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragLeave(void);
	HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

protected:
	// Data Member
	CDevConElement * m_pElem;
	CDropHelper      m_DropHelp;
	DWORD	         m_dwEffect;
	UINT             m_uDrop;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnPostCreate(void);

	// Notification Handlers
	UINT OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

	// Command Handlers
	BOOL OnPasteControl(UINT uID, CCmdSource &Src);
	BOOL OnPasteCommand(UINT uID);

	// Implementation
	BOOL SetDrop(UINT uDrop);
	BOOL IsItemReadOnly(void);
};

// End of File

#endif
