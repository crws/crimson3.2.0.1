
#include "Intern.hpp"

#include "DevConElementEditBox.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Edit Box UI Element
//

// Base Class

#define CBaseClass CDevConElementBase

// Dynamic Class

AfxImplementDynamicClass(CDevConElementEditBox, CBaseClass);

// Constructor

CDevConElementEditBox::CDevConElementEditBox(void)
{
	m_pUnitLayout = NULL;

	m_uWidth      = 30;

	m_dwStyle     = ES_LEFT | ES_AUTOHSCROLL;

	m_fBusy       = FALSE;
}

// Destructor

CDevConElementEditBox::~CDevConElementEditBox(void)
{
}

// Operations

void CDevConElementEditBox::AddLayout(CLayFormation *pForm)
{
	m_pDataLayout = New CLayItemText(m_uWidth, CSize(5, 4), CSize(1, 1));

	AddToMain(m_pDataLayout, horzLeft | vertCenter);

	if( !m_Units.IsEmpty() ) {

		m_pUnitLayout = New CLayItemText(m_Units, 1, 4);

		AddToMain(m_pUnitLayout, horzNone | vertCenter);
	}

	FinalizeLayout(pForm);
}

void CDevConElementEditBox::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	if( m_pDataLayout ) {

		CRect Rect  = m_pDataLayout->GetRect();

		m_pEditCtrl = New CDevConEditCtrl(this);

		m_pEditCtrl->Create(L"",
				    WS_CHILD | WS_TABSTOP | WS_BORDER | m_dwStyle,
				    Rect,
				    Wnd,
				    id++
		);

		m_pEditCtrl->SetFont(afxFont(Dialog));

		AddControl(m_pEditCtrl);
	}

	if( m_pUnitLayout ) {

		CRect Rect  = m_pUnitLayout->GetRect();

		m_pUnitCtrl = New CStatic;

		m_pUnitCtrl->Create(m_Units,
				    WS_CHILD | SS_LEFT,
				    Rect,
				    Wnd,
				    id++
		);

		m_pUnitCtrl->SetFont(afxFont(Dialog));

		AddControl(m_pUnitCtrl);
	}
}

UINT CDevConElementEditBox::OnNotify(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID == m_pEditCtrl->GetID() ) {

		if( uNotify == EN_DROP ) {

			return actionChange;
		}

		if( uNotify == EN_CHANGE ) {

			m_pEditCtrl->SetError(FALSE);
		}

		if( !m_fBusy ) {

			if( uNotify == EN_KILLFOCUS ) {

				if( m_pEditCtrl->GetError() || m_pEditCtrl->GetModify() ) {

					uNotify = EN_TAB_AWAY;
				}
			}
		}

		if( uNotify == EN_TAB_AWAY ) {

			if( m_pEditCtrl->GetError() ) {

				uNotify = EN_CANCEL;
			}
		}

		if( uNotify == EN_TAB_AWAY || uNotify == EN_RETURN ) {

			if( m_pEditCtrl->GetModify() ) {

				CPrintf Data(m_pEditCtrl->GetWindowText());

				m_fBusy = TRUE;

				if( OnCheckData(Data) ) {

					if( Data.CompareC(m_Data) ) {

						m_Data = Data;

						OnSetData();

						m_fBusy = FALSE;

						return actionChange;
					}
				}
				else {
					if( uNotify == EN_RETURN ) {

						m_pEditCtrl->SetSel(CRange(TRUE));

						m_pEditCtrl->SetError(TRUE);

						m_pEditCtrl->SetModify(FALSE);
					}
					else {
						OnSetData();
					}
				}

				m_fBusy = FALSE;
			}
		}

		if( uNotify == EN_CANCEL ) {

			OnSetData();
		}
	}

	return CBaseClass::OnNotify(uID, uNotify, Wnd);
}

// Overridables

void CDevConElementEditBox::OnSetData(void)
{
	SetEditCtrl(m_Data);
}

// Implementation

void CDevConElementEditBox::SetEditCtrl(CString const &Data)
{
	m_pEditCtrl->SetError(FALSE);

	m_pEditCtrl->SetWindowText(Data);

	m_pEditCtrl->SetSel(CRange(TRUE));

	m_pEditCtrl->SetModify(FALSE);
}

// End of File
