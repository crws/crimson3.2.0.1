
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementFile_HPP

#define INCLUDE_DevConElementFile_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConEditCtrl;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration File UI Element
//

class CDevConElementFile : public CDevConElementBase
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementFile(void);

	// Destructor
	~CDevConElementFile(void);

	// Operations
	void	AddLayout(CLayFormation *pForm);
	void	CreateControls(CWnd &Wnd, UINT &id);
	void	ParseConfig(CJsonData *pSchema, CJsonData *pField);
	UINT	OnNotify(UINT uID, UINT uNotify, CWnd &Wnd);
	BOOL	CanAcceptData(IDataObject *pData, DWORD &dwEffect);
	BOOL	AcceptData(IDataObject *pData);
	CString FormatData(CString const &Data);

protected:
	// Data Member
	CString		  m_Filter;
	UINT		  m_cfFile;
	CFont		  m_Font1;
	CFont		  m_Font2;
	CLayItem	* m_pUploadLayout;
	CLayItem	* m_pDownloadLayout;
	CLayItem	* m_pClearLayout;
	CDevConEditCtrl * m_pNameCtrl;
	CButton		* m_pUploadCtrl;
	CButton		* m_pDownloadCtrl;
	CButton		* m_pClearCtrl;

	// Overridables
	BOOL IsControlEnabled(CCtrlWnd *pCtrl);
	void OnSetData(void);

	// Implementation
	BOOL    LoadFromFile(CFilename const &Name);
	BOOL    SaveToFile(CFilename const &Name, CString const &Data);
	CString GetMimeType(CString const &Type);
};

// End of File

#endif
