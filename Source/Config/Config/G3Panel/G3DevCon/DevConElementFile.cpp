
#include "Intern.hpp"

#include "DevConElementFile.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

#include "HttpBase64.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration File UI Element
//

// Base Class

#define CBaseClass CDevConElementBase

// Dynamic Class

AfxImplementDynamicClass(CDevConElementFile, CBaseClass);

// Constructor

CDevConElementFile::CDevConElementFile(void)
{
	m_cfFile = RegisterClipboardFormat(L"FileNameW");
}

// Destructor

CDevConElementFile::~CDevConElementFile(void)
{
}

// Operations

void CDevConElementFile::AddLayout(CLayFormation *pForm)
{
	m_pDataLayout     = New CLayItemText(25, CSize(5, 3), CSize(1, 1));

	m_pUploadLayout   = New CLayItemText(L"X", CSize(8, 3), CSize(1, 1));

	m_pDownloadLayout = New CLayItemText(L"X", CSize(8, 3), CSize(1, 1));

	m_pClearLayout    = New CLayItemText(L"X", CSize(8, 3), CSize(1, 1));

	AddToMain(m_pDataLayout, horzLeft | vertCenter);

	AddToMain(m_pUploadLayout, horzNone | vertCenter);

	AddToMain(m_pDownloadLayout, horzNone | vertCenter);

	AddToMain(m_pClearLayout, horzNone | vertCenter);

	FinalizeLayout(pForm);
}

void CDevConElementFile::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	m_Font1.Create(CClientDC(Wnd), L"WingDings 3", 10, 0);

	m_Font2.Create(CClientDC(Wnd), L"WingDings 2", 10, 0);

	if( m_pDataLayout ) {

		CRect Rect  = m_pDataLayout->GetRect();

		m_pNameCtrl = New CDevConEditCtrl(this);

		m_pNameCtrl->Create(L"",
				    WS_CHILD | WS_BORDER | ES_READONLY,
				    Rect,
				    Wnd,
				    id++
		);

		AddControl(m_pNameCtrl);
	}

	if( m_pUploadLayout ) {

		CRect Rect = m_pUploadLayout->GetRect();

		m_pUploadCtrl = New CButton;

		m_pUploadCtrl->Create(L"\u0070",
				      WS_CHILD | WS_TABSTOP | BS_PUSHBUTTON,
				      Rect,
				      Wnd,
				      id++
		);

		m_pUploadCtrl->SetFont(m_Font1);

		AddControl(m_pUploadCtrl);
	}

	if( m_pUploadLayout ) {

		CRect Rect = m_pDownloadLayout->GetRect();

		m_pDownloadCtrl = New CButton;

		m_pDownloadCtrl->Create(L"\u0071",
					WS_CHILD | WS_TABSTOP | BS_PUSHBUTTON,
					Rect,
					Wnd,
					id++
		);

		m_pDownloadCtrl->SetFont(m_Font1);

		AddControl(m_pDownloadCtrl);
	}

	if( m_pClearLayout ) {

		CRect Rect = m_pClearLayout->GetRect();

		m_pClearCtrl = New CButton;

		m_pClearCtrl->Create(L"\u00D2",
				     WS_CHILD | WS_TABSTOP | BS_PUSHBUTTON,
				     Rect,
				     Wnd,
				     id++
		);

		m_pClearCtrl->SetFont(m_Font2);

		AddControl(m_pClearCtrl);
	}
}

void CDevConElementFile::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CStringArray List;

	pField->GetValue(L"format").Tokenize(List, ',');

	CString Type = List[0].Mid(1);

	m_Filter.Printf(IDS("%s Files (*.%s)|*.%s|All Files (*.*)|*.*"), Type.ToUpper(), Type.ToLower(), Type.ToLower());
}

UINT CDevConElementFile::OnNotify(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID == m_pNameCtrl->GetID() ) {

		if( uNotify == EN_DROP ) {

			return actionChange;
		}

		return actionNone;
	}

	if( uID == m_pUploadCtrl->GetID() ) {

		COpenFileDialog Dlg;

		Dlg.LoadLastPath(L"DevCon");

		Dlg.SetFilter(m_Filter);

		Dlg.SetCaption(IDS("Open File"));

		if( Dlg.Execute(*afxMainWnd) ) {

			if( LoadFromFile(Dlg.GetFilename()) ) {

				Dlg.SaveLastPath(L"DevCon");

				return actionChange;
			}
		}

		return actionNone;
	}

	if( uID == m_pDownloadCtrl->GetID() ) {

		CString Data = m_Data;

		CString Name = Data.StripToken('|');

		CString Rest = Data.Mid(Data.Find(',')+1);

		CSaveFileDialog::LoadLastPath(L"DevCon");

		for( ;;) {

			CSaveFileDialog Dlg;

			Dlg.SetFilter(m_Filter);

			Dlg.SetCaption(IDS("Save File"));

			Dlg.SetFilename(Name);

			if( Dlg.Execute(*afxMainWnd) ) {

				CFilename Save = Dlg.GetFilename();

				if( Save.Exists() ) {

					CPrintf Warn = CPrintf(IDS("The file %s already exists.\n\nDo you want to overwrite it?"), Save.GetName());

					switch( afxMainWnd->YesNoCancel(Warn) ) {

						case IDNO:

							continue;

						case IDCANCEL:

							return actionNone;
					}
				}

				SaveToFile(Save, Rest);

				Dlg.SaveLastPath(L"DevCon");

				return TRUE;
			}

			return FALSE;
		}

		return actionNone;
	}

	if( uID == m_pClearCtrl->GetID() ) {

		m_Data.Empty();

		OnSetData();

		m_pUploadCtrl->SetFocus();

		return actionChange;
	}

	return CBaseClass::OnNotify(uID, uNotify, Wnd);
}

BOOL CDevConElementFile::CanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	FORMATETC Fmt = { WORD(m_cfFile), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
	}

	return CBaseClass::CanAcceptData(pData, dwEffect);
}

BOOL CDevConElementFile::AcceptData(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfFile), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		HGLOBAL hName = Med.hGlobal;

		CString Name  = LPOLESTR(GlobalLock(hName));

		GlobalUnlock(hName);

		ReleaseStgMedium(&Med);

		if( LoadFromFile(Name) ) {

			return TRUE;
		}
	}

	return CBaseClass::AcceptData(pData);
}

CString CDevConElementFile::FormatData(CString const &Data)
{
	return Data.TokenLeft(L'|');
}

// Overridables

BOOL CDevConElementFile::IsControlEnabled(CCtrlWnd *pCtrl)
{
	if( pCtrl == m_pDownloadCtrl || pCtrl == m_pClearCtrl ) {

		if( m_Data.IsEmpty() ) {

			return FALSE;
		}
	}

	return CBaseClass::IsControlEnabled(pCtrl);
}

void CDevConElementFile::OnSetData(void)
{
	if( !m_Data.IsEmpty() ) {

		CString Name = m_Data.Left(m_Data.Find('|'));

		m_pNameCtrl->SetWindowText(Name);

		m_pNameCtrl->SetFont(afxFont(Dialog));
	}
	else {
		m_pNameCtrl->SetWindowText(IDS("Drop File or Click Upload"));

		m_pNameCtrl->SetFont(afxFont(Italic));
	}
}

// Implementation

BOOL CDevConElementFile::LoadFromFile(CFilename const &Name)
{
	HANDLE hFile = Name.OpenRead();

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT  uSize = GetFileSize(hFile, NULL);

		DWORD uDone = 0;

		PBYTE pData = New BYTE[uSize];

		ReadFile(hFile, pData, uSize, &uDone, NULL);

		CloseHandle(hFile);

		if( uDone == uSize ) {

			UINT uCode = CHttpBase64::GetEncodeSize(uSize);

			PSTR pCode = New char[uCode+1];

			CHttpBase64::Encode(pCode, pData, uSize, FALSE);

			CPrintf Data(L"%s|data:%s;base64,%s", Name.GetName(), GetMimeType(Name.GetType()), CString(pCode));

			delete[] pCode;

			delete[] pData;

			if( m_Data.CompareC(Data) ) {

				m_Data = Data;

				OnSetData();

				return TRUE;
			}

			afxMainWnd->Information(IDS("The file is identical to that already loaded."));

			return FALSE;
		}

		delete[] pData;
	}

	afxMainWnd->Error(IDS("Failed to open file."));

	return FALSE;
}

BOOL CDevConElementFile::SaveToFile(CFilename const &Name, CString const &Data)
{
	HANDLE hFile = Name.OpenWrite();

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT  uText = Data.GetLength();

		PSTR  pText = New char[uText+1];

		for( UINT n = 0; (pText[n] = char(Data[n])); n++ );

		UINT  uData = CHttpBase64::GetDecodeSize(pText);

		PBYTE pData = New BYTE[uData];

		CHttpBase64::Decode(pData, pText);

		DWORD uDone = 0;

		WriteFile(hFile, pData, uData, &uDone, NULL);

		CloseHandle(hFile);

		delete[] pData;

		delete[] pText;

		if( uDone == uData ) {

			return TRUE;
		}

		DeleteFile(Name);
	}

	afxMainWnd->Error(IDS("Unable to save file."));

	return FALSE;
}

CString CDevConElementFile::GetMimeType(CString const &Type)
{
	if( Type == L"crt" ) {

		return L"application/x-x509-ca-cert";
	}

	return L"application/binary";
}

// End of File
