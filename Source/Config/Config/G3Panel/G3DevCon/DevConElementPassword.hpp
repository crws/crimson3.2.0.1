
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementPassword_HPP

#define INCLUDE_DevConElementPassword_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementEditBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration String UI Element
//

class CDevConElementPassword : public CDevConElementEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementPassword(void);

	// Destructor
	~CDevConElementPassword(void);

	// Operations
	void CreateControls(CWnd &Wnd, UINT &id);
	void ParseConfig(CJsonData *pSchema, CJsonData *pField);

protected:
	// Data Members
	CString m_Type;
	CString m_Default;

	// Overridables
	BOOL OnCheckData(CString &Data);

	// Implementation
	BOOL IsNumber(CString const &Data);
};

// End of File

#endif
