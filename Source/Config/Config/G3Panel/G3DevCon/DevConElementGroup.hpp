
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementGroup_HPP

#define INCLUDE_DevConElementGroup_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Group UI Element
//

class CDevConElementGroup : public CDevConElement
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConElementGroup(CString const &Label);

	// Destructor
	~CDevConElementGroup(void);

	// Operations
	void AddLayout(CLayFormation *pForm);
	void CreateControls(CWnd &Wnd, UINT &id);

protected:
	// Data Member
	CString		m_Label;
	CLayItem      * m_pEditLayout;
	CLayFormation * m_pMainLayout;
	CCtrlWnd      * m_pGroupCtrl;
};

// End of File

#endif
