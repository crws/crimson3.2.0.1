
#include "Intern.hpp"

#include "DevConElementGroup.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Group Control Spacer
//

class CDevConRulerWnd : public CCtrlWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConRulerWnd(void);

protected:
	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnEraseBkGnd(CDC &DC);
	void OnPaint(void);
	void OnLButtonDown(UINT uFlags, CPoint Pos);
};

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Group UI Element
//

// Base Class

#define CBaseClass CDevConElement

// Runtime Class

AfxImplementRuntimeClass(CDevConElementGroup, CBaseClass);

// Constructor

CDevConElementGroup::CDevConElementGroup(CString const &Label) : m_Label(Label)
{
	m_pEditLayout = NULL;

	m_pMainLayout = NULL;

	m_pGroupCtrl  = New CDevConRulerWnd;;
}

// Destructor

CDevConElementGroup::~CDevConElementGroup(void)
{
}

// Operations

void CDevConElementGroup::AddLayout(CLayFormation *pForm)
{
	CRect Rect(4, 4, 16, 8);

	m_pEditLayout = New CLayItemText(m_Label);

	m_pMainLayout = New CLayFormPad(m_pEditLayout, Rect, horzNone | vertNone | horzGrow);

	pForm->AddItem(m_pMainLayout);
}

void CDevConElementGroup::CreateControls(CWnd &Wnd, UINT &id)
{
	CRect Rect = m_pEditLayout->GetRect();

	m_pGroupCtrl->Create(m_Label,
			     WS_CHILD,
			     Rect,
			     Wnd,
			     id++
	);

	m_pGroupCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pGroupCtrl);
}

//////////////////////////////////////////////////////////////////////////
//
// Group Control Spacer
//

// Dynamic Class

AfxImplementDynamicClass(CDevConRulerWnd, CCtrlWnd);

// Constructor

CDevConRulerWnd::CDevConRulerWnd(void)
{
}

// Message Map

AfxMessageMap(CDevConRulerWnd, CCtrlWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)
		AfxDispatchMessage(WM_PAINT)
		AfxDispatchMessage(WM_LBUTTONDOWN)

		AfxMessageEnd(CDevConRulerWnd)
};

// Message Handlers

BOOL CDevConRulerWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
}

void CDevConRulerWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	DC.Select(afxFont(Bolder));

	DC.SetTextColor(afxColor(TabGroup));

	DC.SetBkColor(afxColor(TabFace));

	CRect   Rect = GetClientRect();

	CString Text = GetWindowText();

	CSize   Size = DC.GetTextExtent(Text);

	DC.FillRect(Rect, afxBrush(TabFace));

	Rect.left += 0;

	Rect.top  += 4;

	DC.TextOut(Rect.left, Rect.top, Text);

	Rect.left += Size.cx + 8;

	Rect.top  += Size.cy / 2;

	DC.DrawTopEdge(Rect, 1, afxBrush(3dShadow));

	DC.Deselect();
}

void CDevConRulerWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	CWnd *pCtrl = this;

	for( ;;) {

		if( (pCtrl = pCtrl->GetWindowPtr(GW_HWNDPREV))->IsWindow() ) {

			if( pCtrl->IsKindOf(AfxRuntimeClass(CDevConRulerWnd)) ) {

				break;
			}

			continue;
		}

		pCtrl = GetWindowPtr(GW_HWNDFIRST);

		break;
	}

	while( pCtrl->IsWindow() ) {

		if( pCtrl->GetWindowStyle() & WS_TABSTOP ) {

			if( pCtrl->IsWindowEnabled() ) {

				pCtrl->SetFocus();

				break;
			}
		}

		pCtrl = pCtrl->GetWindowPtr(GW_HWNDNEXT);
	}
}

// End of File
