
#include "Intern.hpp"

#include "DevConResWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevCon.hpp"

#include "DevConPart.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CDevConResWnd, CStdTreeWnd);

// Constructors

CDevConResWnd::CDevConResWnd(void)
{
	m_dwStyle   = m_dwStyle | TVS_HASBUTTONS;

	m_cfPerson  = RegisterClipboardFormat(L"C3.2 Personality");

	m_cfCode[0] = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfCode[1] = RegisterClipboardFormat(L"C3.1 Code Fragment Int");

	m_cfCode[2] = RegisterClipboardFormat(L"C3.1 Code Fragment Real");

	m_cfCode[3] = RegisterClipboardFormat(L"C3.1 Code Fragment String");

	m_pKeys     = NULL;

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"ProgTreeIcon16"), afxColor(MAGENTA));

	m_Accel1.Create(L"ResTreeAccel");
}

// IUnknown

HRESULT CDevConResWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
}

ULONG CDevConResWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
}

ULONG CDevConResWnd::Release(void)
{
	return CStdTreeWnd::Release();
}

// IUpdate

HRESULT CDevConResWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateProps ) {

		if( pItem == m_pItem ) {

			CJsonData *pConfig = m_pPerson->GetConfig();

			CJsonData *pSet    = pConfig->GetChild(L"set");

			m_pKeys = pSet ? pSet->GetChild(L"keys") : NULL;
		}

		RefreshTree();
	}

	return S_OK;
}

// Overridables

void CDevConResWnd::OnAttach(void)
{
	m_pPerson = ((CDevCon *) CViewWnd::m_pItem)->m_pPConfig;

	CJsonData *pConfig = m_pPerson->GetConfig();

	CJsonData *pSet    = pConfig->GetChild(L"set");

	m_pKeys = pSet ? pSet->GetChild(L"keys") : NULL;

	CStdTreeWnd::OnAttach();
}

// Message Map

AfxMessageMap(CDevConResWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
		AfxDispatchMessage(WM_DESTROY)
		AfxDispatchMessage(WM_LOADTOOL)

		AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown)
		AfxDispatchNotify(100, TVN_SELCHANGED, OnTreeSelChanged)
		AfxDispatchNotify(100, NM_RETURN, OnTreeDblClk)
		AfxDispatchNotify(100, NM_DBLCLK, OnTreeDblClk)

		AfxMessageEnd(CDevConResWnd)
};

// Message Handlers

void CDevConResWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
}

void CDevConResWnd::OnDestroy(void)
{
	KillTree(m_hRoot);
}

void CDevConResWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ResTreeTool"));
	}
}

// Notification Handlers

BOOL CDevConResWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_TAB:

			m_System.FlipToNavPane(TRUE);

			return TRUE;
	}

	return CStdTreeWnd::OnTreeKeyDown(uID, Info);
}

void CDevConResWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			if( Info.action == TVC_BYMOUSE ) {

				SetFocus();
			}

			m_hSelect = Info.itemNew.hItem;

			m_pSelect = NULL;

			NewItemSelected();
		}
	}
}

void CDevConResWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( !m_pTree->GetChild(m_hSelect) ) {

			IDataObject *pData = NULL;

			if( MakeDataObject(pData) ) {

				OleSetClipboard(pData);

				OleFlushClipboard();

				pData->Release();

				m_System.SendPaneCommand(IDM_EDIT_PASTE);

				m_System.FlipToItemView(FALSE);
			}

			return;
		}

		ToggleItem(m_hSelect);
	}
}

// Data Object Construction

BOOL CDevConResWnd::MakeDataObject(IDataObject * &pData)
{
	if( m_hSelect != m_hRoot ) {

		CDataObject *pMake = New CDataObject;

		for( UINT n = 0; n < 4; n++ ) {

			CString Text;

			switch( n ) {

				case 0: // Untyped
				{
					switch( m_pTree->GetItemParam(m_hSelect) ) {

						case valInteger:
							Text += L"GetPersonalityInt(\"";
							break;

						case valFloat:
							Text += L"GetPersonalityFloat(\"";
							break;

						case valString:
							Text += L"GetPersonalityString(\"";
							break;

						case valIp:
							Text += L"GetPersonalityIp(\"";
							break;
					}
				}
				break;

				case 1: // Integer
				{
					switch( m_pTree->GetItemParam(m_hSelect) ) {

						case valInteger:
						case valFloat:
							Text += L"GetPersonalityInt(\"";
							break;

						case valIp:
							Text += L"GetPersonalityIp(\"";
							break;
					}
				}
				break;

				case 2: // Real
				{
					switch( m_pTree->GetItemParam(m_hSelect) ) {

						case valInteger:
						case valFloat:
							Text += L"GetPersonalityFloat(\"";
							break;
					}
				}
				break;

				case 3: // String
				{
					Text += L"GetPersonalityString(\"";
				}
				break;
			}

			if( !Text.IsEmpty() ) {

				Text += m_pTree->GetItemText(m_hSelect);

				Text += L"\")";

				pMake->AddText(m_cfCode[n], Text);
			}
		}

		if( TRUE ) {

			CString Text;

			Text  = m_pItem->GetDatabase()->GetUniqueSig();

			Text += L'|';

			Text += m_pTree->GetItemText(m_hSelect);

			pMake->AddText(m_cfPerson, Text);
		}

		if( !pMake->IsEmpty() ) {

			pData = pMake;

			return TRUE;
		}

		delete pMake;
	}

	pData = NULL;

	return FALSE;
}

// Tree Loading

void CDevConResWnd::LoadTree(void)
{
	LoadRoot();

	ExpandItem(m_hRoot);

	m_pTree->SelectItem(m_hRoot);
}

void CDevConResWnd::LoadRoot(void)
{
	CTreeViewItem Node;

	Node.SetText(IDS("Keys"));

	Node.SetParam(NULL);

	Node.SetImages(2 + 1 * 4);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);

	if( m_pKeys ) {

		UINT c = m_pKeys->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonData *pKey = m_pKeys->GetChild(n);

			CString    Name = pKey->GetValue(L"name");

			CString    Data = pKey->GetValue(L"value");

			if( !Name.IsEmpty() ) {

				CTreeViewItem Node;

				Node.SetText(Name);

				int nd = 0;
				int np = 0;
				int no = 0;

				for( UINT n = 0; n < Data.GetLength(); n++ ) {

					if( isdigit(Data[n]) )
						nd++;
					else {
						if( Data[n] == '.' )
							np++;
						else
							no++;
					}
				}

				if( nd && !no ) {

					if( !np ) {

						Node.SetParam(valInteger);
					}
					else {
						switch( np ) {

							case 1:
								Node.SetParam(valFloat);
								break;

							case 3:
								Node.SetParam(valIp);
								break;

							default:
								Node.SetParam(valString);
								break;
						}
					}
				}
				else {
					Node.SetParam(valString);
				}

				Node.SetImages(2 + 1 * 4);

				m_pTree->InsertItem(m_hRoot, NULL, Node);
			}
		}
	}
}

// Drag Hooks

DWORD CDevConResWnd::FindDragAllowed(void)
{
	return DROPEFFECT_COPY | DROPEFFECT_LINK;
}

// Selection Hooks

CString CDevConResWnd::SaveSelState(void)
{
	// LATER -- Is there a way to implement this?

	return L"";
}

void CDevConResWnd::LoadSelState(CString State)
{
	SelectRoot();
}

// End of File
