
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementEnum_HPP

#define INCLUDE_DevConElementEnum_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Enumerated UI Element
//

class CDevConElementEnum : public CDevConElementBase
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementEnum(void);

	// Destructor
	~CDevConElementEnum(void);

	// Operations
	void    AddLayout(CLayFormation *pForm);
	void    CreateControls(CWnd &Wnd, UINT &id);
	void    ParseConfig(CJsonData *pSchema, CJsonData *pField);
	UINT    OnNotify(UINT uID, UINT uNotify, CWnd &Wnd);
	CString FormatData(CString const &Data);
	CString GetDefault(void);

protected:
	// Data Member
	CComboBox     * m_pDropCtrl;
	CFont	        m_Font;
	CArray<CString> m_Strings;
	CArray<UINT>    m_Values;
	CString		m_Default;

	// Overridables
	void OnSetData(void);
};

// End of File

#endif
