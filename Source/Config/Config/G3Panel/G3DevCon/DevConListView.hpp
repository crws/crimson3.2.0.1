
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConListView_HPP

#define INCLUDE_DevConListView_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConElement;

//////////////////////////////////////////////////////////////////////////
//
// Notification Codes
//

#define LVN_COMMAND	0x8801

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration List View
//

class CDevConListView : public CListView
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConListView(CDevConElement *pElem, BOOL fFixed, BOOL fFull);

	// Attributes
	UINT HitTestControl(NMITEMACTIVATE &Info) const;

	// Operations
	void InsertButtons(void);

protected:
	// Data Members
	CDevConElement * m_pElem;
	BOOL		 m_fFixed;
	BOOL             m_fFull;
	CImageList       m_Images;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	UINT OnGetDlgCode(MSG *pMsg);

	// Notification Handlers
	UINT OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

	// Command Handlers
	BOOL OnEditControl(UINT uID, CCmdSource &Src);
	BOOL OnEditCommand(UINT uID);

	// Images
	void  LoadImageList(void);
	CRect GetItemButtonRect(UINT uItem) const;
	UINT  GetItemButtonCount(UINT uParam) const;
	BOOL  GetItemButtonEnable(UINT uParam, UINT uButton) const;
	UINT  GetItemButtonImage(UINT uParam, UINT uButton) const;
};

// End of File

#endif
