
#include "Intern.hpp"

#include "DevConElementEnum.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConComboBox.hpp"

#include "DevConNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Enumerated UI Element
//

// Base Class

#define CBaseClass CDevConElementBase

// Dynamic Class

AfxImplementDynamicClass(CDevConElementEnum, CBaseClass);

// Constructor

CDevConElementEnum::CDevConElementEnum(void)
{
}

// Destructor

CDevConElementEnum::~CDevConElementEnum(void)
{
}

// Operations

void CDevConElementEnum::AddLayout(CLayFormation *pForm)
{
	// TODO -- Dynamic layout?

	m_pDataLayout = New CLayItemText(30, CSize(5, 4), CSize(1, 1));

	AddToMain(m_pDataLayout, horzLeft | vertCenter);

	FinalizeLayout(pForm);
}

void CDevConElementEnum::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	if( m_pDataLayout ) {

		CRect Rect  = m_pDataLayout->GetRect();

		Rect.bottom = Rect.top + 128;

		m_pDropCtrl = New CDevConComboBox(this);

		m_pDropCtrl->Create(L"",
				    WS_CHILD | WS_TABSTOP | CBS_DROPDOWNLIST,
				    Rect,
				    Wnd,
				    id++
		);

		m_pDropCtrl->SetFont(afxFont(Dialog));

		for( UINT n = 0; n < m_Strings.GetCount(); n++ ) {

			m_pDropCtrl->AddString(m_Strings[n], m_Values[n]);
		}

		AddControl(m_pDropCtrl);
	}
}

void CDevConElementEnum::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CString Format = pField->GetValue(L"format");

	if( Format.StartsWith(L"=") ) {

		CJsonData *pDict = pSchema->GetChild(L"aliases");

		Format = pDict->GetValue(Format.Mid(1));
	}

	CStringTree  Used;

	CStringArray List;

	Format.Tokenize(List, '|');

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		CString Text = List[n];

		if( Text[0] == '<' ) {

			CDevConNode *pNode = (CDevConNode *) m_pItem;

			CJsonData   *pList = pNode->m_pData->GetChild(Text.Mid(1, Text.GetLength()-2));

			if( pList ) {

				for( UINT n = 0; n < pList->GetCount(); n++ ) {

					CJsonData *pRow = pList->GetChild(n);

					UINT       uKey = watoi(pRow->GetValue(L"key"));

					CString    Name = pRow->GetValue(L"name");

					if( uKey && Name.GetLength() ) {

						if( Used.Failed(Used.Find(Name)) ) {

							if( m_pPerson ) {

								Name = CPrintf(L"[%u] ", uKey) + Name;
							}

							m_Strings.Append(Name);

							m_Values.Append(uKey);

							Used.Insert(Name);
						}
					}
				}
			}
		}
		else {
			UINT uPos  = Text.Find('=');

			UINT uData = n;

			if( uPos < NOTHING ) {

				uData = watoi(Text.Left(uPos));

				Text  = Text.Mid(uPos+1);
			}

			if( m_pPerson ) {

				Text = CPrintf(L"[%u] ", n) + Text;
			}

			m_Strings.Append(Text);

			m_Values.Append(uData);
		}
	}

	m_Default = pField->GetValue(L"default");
}

UINT CDevConElementEnum::OnNotify(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID == m_pDropCtrl->GetID() ) {

		if( uNotify == CBN_DROP ) {

			return actionChange;
		}

		if( uNotify == CBN_SELCHANGE ) {

			CPrintf Data(L"%u", m_pDropCtrl->GetItemData(m_pDropCtrl->GetCurSel()));

			if( m_Data != Data ) {

				m_Data = Data;

				return actionChange;
			}
		}
	}

	return CBaseClass::OnNotify(uID, uNotify, Wnd);
}

CString CDevConElementEnum::FormatData(CString const &Data)
{
	UINT d = watoi(Data.IsEmpty() ? m_Default : Data);

	for( UINT n = 0; n < m_Values.GetCount(); n++ ) {

		if( m_Values[n] == d ) {

			return m_Strings[n];
		}
	}

	return L"";
}

CString CDevConElementEnum::GetDefault(void)
{
	return m_Default;
}

// Overridables

void CDevConElementEnum::OnSetData(void)
{
	if( m_Data.IsEmpty() && !m_Default.IsEmpty() ) {

		m_Data = m_Default;
	}

	m_pDropCtrl->SelectData(watoi(m_Data));
}

// End of File
