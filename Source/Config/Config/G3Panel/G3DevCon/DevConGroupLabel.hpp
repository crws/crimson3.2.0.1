
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConGroupLabel_HPP

#define INCLUDE_DevConGroupLabel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Group Control Label
//

class CDevConGroupLabel : public CCtrlWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConGroupLabel(void);

protected:
	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnEraseBkGnd(CDC &DC);
	void OnPaint(void);
	void OnLButtonDown(UINT uFlags, CPoint Pos);
};

// End of File

#endif
