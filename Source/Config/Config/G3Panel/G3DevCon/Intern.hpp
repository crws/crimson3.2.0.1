
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <shlink.hpp>

#include "G3DevCon.hpp"

#include "Intern.hxx"

#include <float.h>

#include <limits.h>

#include <math.h>

// End of File

#endif
