
#include "Intern.hpp"

#include "DevConElementBase.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConModeButton.hpp"

#include "DevConComboBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Basic UI Element
//

// Base Class

#define CBaseClass CDevConElement

// Dynamic Class

AfxImplementDynamicClass(CDevConElementBase, CBaseClass);

// Static Data

UINT CDevConElementBase::m_cfPerson = RegisterClipboardFormat(L"C3.2 Personality");

// Constructor

CDevConElementBase::CDevConElementBase(void)
{
	m_pFullLayout = NULL;

	m_pMainLayout = NULL;

	m_pDataLayout = NULL;

	m_pModeLayout = NULL;

	m_pModeCtrl   = NULL;

	m_pKeyCtrl    = NULL;
}

// Destructor

CDevConElementBase::~CDevConElementBase(void)
{
}

// Operations

void CDevConElementBase::CreateControls(CWnd &Wnd, UINT &id)
{
	if( m_pModeLayout ) {

		CRect Rect = m_pModeLayout->GetRect();

		m_pModeCtrl->Create(Rect, Wnd, id++);

		AddControl(m_pModeCtrl);

		if( m_pDataLayout ) {
			
			CClientDC DC(Wnd);

			DC.Select(afxFont(Dialog));

			CRect Rect  = m_pMainLayout->GetRect() - CSize(4, 5);

			Rect.bottom = Rect.top  + 128;

			Rect.right  = Rect.left + DC.GetTextExtent(CString('X', 30)).cx + 10;

			m_pKeyCtrl  = New CDevConComboBox(this);

			m_pKeyCtrl->Create(L"",
					   WS_CHILD | WS_TABSTOP | CBS_DROPDOWNLIST,
					   Rect,
					   Wnd,
					   id++
			);

			m_pKeyCtrl->SetFont(afxFont(Dialog));

			CJsonData *pSet = m_pPerson->GetChild(L"set");

			if( pSet ) {

				m_pKeyCtrl->AddString(IDS("[Undefined]"));

				CJsonData *pKeys = pSet->GetChild(L"keys");

				for( UINT n = 0; n < pKeys->GetCount(); n++ ) {

					CJsonData *pKey = pKeys->GetChild(n);

					m_pKeyCtrl->AddString(pKey->GetValue(L"name"));
				}
			}

			AddControl(m_pKeyCtrl);
		}
	}

	CBaseClass::CreateControls(Wnd, id);
}

void CDevConElementBase::ShowControls(BOOL fShow)
{
	if( m_fShow != fShow ) {

		BOOL fCustom = m_Data.StartsWith(L"=");

		for( UINT n = 0; n < m_Controls.GetCount(); n++ ) {

			UINT uCmd = fShow ? SW_SHOW : SW_HIDE;

			if( fCustom ) {

				if( m_Controls[n] != m_pKeyCtrl && m_Controls[n] != m_pModeCtrl ) {

					uCmd = SW_HIDE;
				}
			}
			else {
				if( m_Controls[n] == m_pKeyCtrl ) {

					uCmd = SW_HIDE;
				}
			}

			m_Controls[n]->ShowWindow(uCmd);
		}

		m_fShow = fShow;
	}
}

UINT CDevConElementBase::OnNotify(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( m_pModeCtrl ) {

		if( uID == m_pModeCtrl->GetID() ) {

			if( uNotify == BN_DROP ) {

				return actionChange;
			}

			if( !uNotify ) {

				uNotify = m_pModeCtrl->ShowMenu();
			}

			if( uNotify == 100 || uNotify == 200 ) {

				if( uNotify == 100 ) {

					SetData(L"");
				}
				else {
					m_Data = L"=" + m_pKeyCtrl->GetWindowText();
				}

				m_fShow = FALSE;

				ShowControls(TRUE);

				m_pModeCtrl->SetData(uNotify);

				return actionChange;
			}
		}
	}

	if( m_pKeyCtrl ) {

		if( uID == m_pKeyCtrl->GetID() ) {

			if( uNotify == CBN_DROP ) {

				return actionChange;
			}

			if( uNotify == CBN_SELCHANGE ) {

				CString Data(L"=" + m_pKeyCtrl->GetWindowText());

				if( m_Data != Data ) {

					m_Data = Data;

					return actionChange;
				}
			}
		}
	}

	return actionNone;
}

BOOL CDevConElementBase::CanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	FORMATETC Fmt = { WORD(m_cfPerson), NULL, 1, -1, TYMED_HGLOBAL };

	if( pData->QueryGetData(&Fmt) == S_OK ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
	}

	return FALSE;
}

BOOL CDevConElementBase::AcceptData(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfPerson), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text(PCTXT(GlobalLock(Med.hGlobal)));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		if( Text.StripToken('|') == m_pItem->GetDatabase()->GetUniqueSig() ) {

			m_Data = L"=" + Text;

			LoadData();

			return TRUE;
		}
	}

	return FALSE;
}

void CDevConElementBase::SetData(CString const &Data)
{
	m_Data = Data;

	LoadData();
}

CString CDevConElementBase::GetData(void)
{
	return m_Data;
}

// Overridables

void CDevConElementBase::OnSetData(void)
{
	AfxAssert(FALSE);
}

BOOL CDevConElementBase::OnCheckData(CString &Data)
{
	return TRUE;
}

BOOL CDevConElementBase::AllowPersonality(void)
{
	return m_pPerson ? TRUE : FALSE;
}

// Implementation

void CDevConElementBase::LoadOptions(void)
{
	CDevConModeButton::COption Opt;

	Opt.m_fEnable = TRUE;
	
	Opt.m_fHidden = FALSE;
	
	Opt.m_Image   = 0;

	Opt.m_Text = IDS("Regular");
	
	Opt.m_uID  = 100;

	m_pModeCtrl->AddOption(Opt);

	Opt.m_Text = IDS("Custom");
	
	Opt.m_uID  = 200;

	m_pModeCtrl->AddOption(Opt);
}

void CDevConElementBase::AddToMain(CLayItem *pItem, UINT Flags)
{
	if( !m_pMainLayout ) {

		m_pMainLayout = New CLayFormRow;
	}

	m_pMainLayout->AddItem(New CLayFormPad(pItem, Flags));
}

void CDevConElementBase::AddToFull(CLayItem *pItem, UINT Flags)
{
	if( !m_pFullLayout ) {

		m_pFullLayout = New CLayFormRow;
	}

	m_pFullLayout->AddItem(New CLayFormPad(pItem, Flags));
}

void CDevConElementBase::FinalizeLayout(CLayFormation *pForm)
{
	if( AllowPersonality() ) {

		m_pModeCtrl   = New CDevConModeButton(this);

		LoadOptions();

		m_pModeLayout = New CLayItemSize(m_pModeCtrl->GetSize(), 0, 1);

		AddToFull(m_pModeLayout, horzNone | vertCenter);
	}

	if( !m_pFullLayout ) {

		m_pFullLayout = New CLayFormRow;
	}

	m_pFullLayout->AddItem(m_pMainLayout);

	pForm->AddItem(m_pFullLayout);
}

void CDevConElementBase::Select(CComboBox *pCombo, CString const &Text)
{
	UINT uPos = pCombo->FindStringExact(Text);

	if( uPos == NOTHING ) {

		uPos = pCombo->AddString(Text + L" [?]");
	}

	pCombo->SetCurSel(uPos);
}

void CDevConElementBase::LoadData(void)
{
	if( m_Data.StartsWith(L"=") ) {

		if( m_pKeyCtrl ) {

			m_pModeCtrl->SetData(200);

			Select(m_pKeyCtrl, m_Data.Mid(1));
		}

		if( m_fShow ) {

			if( m_pKeyCtrl ) {

				m_pModeCtrl->EnableWindow(TRUE);

				m_pKeyCtrl->EnableWindow(TRUE);

				m_pKeyCtrl->ShowWindow(SW_SHOW);
			}

			ShowData(FALSE);
		}
	}
	else {
		OnSetData();

		if( m_pKeyCtrl ) {

			m_pModeCtrl->SetData(100);

			m_pKeyCtrl->SetCurSel(0);
		}

		if( m_fShow ) {

			if( m_pKeyCtrl ) {

				m_pKeyCtrl->ShowWindow(SW_HIDE);

				m_pModeCtrl->EnableWindow(FALSE);

				m_pKeyCtrl->EnableWindow(FALSE);
			}

			ShowData(TRUE);
		}
	}
}

void CDevConElementBase::ShowData(BOOL fShow)
{
	UINT uCmd = fShow ? SW_SHOW : SW_HIDE;

	for( UINT n = 0; n < m_Controls.GetCount(); n++ ) {

		if( m_Controls[n] == m_pKeyCtrl ) {

			continue;
		}

		if( m_Controls[n] == m_pModeCtrl ) {

			continue;
		}

		m_Controls[n]->ShowWindow(uCmd);
	}
}

// End of File
