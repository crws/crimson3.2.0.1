
#include "Intern.hpp"

#include "DevConWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevCon.hpp"

#include "DevConPartHardware.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration View Window
//

// Base Class

#define CBaseClass CUIViewWnd

// Runtime Class

AfxImplementDynamicClass(CDevConWnd, CBaseClass);

// Constructor

CDevConWnd::CDevConWnd(void)
{
	m_fRecycle = FALSE;
}

// Destructor

CDevConWnd::~CDevConWnd(void)
{
}

// UI Update

void CDevConWnd::OnUICreate(void)
{
	StartPage(1);

	StartGroup(IDS("Operations"), 1);

	CString Name(IDS("Device Configuration"));

	// TODO -- Add ability to import from JSON.

	AddButton(CPrintf(IDS("Export %s as JSON"), Name), L"", L"ExportButton");

	AddButton(CPrintf(IDS("Extract %s from Device"), Name), L"", L"ExtractButton");

	EndGroup(FALSE);

	EndPage(FALSE);

	CBaseClass::OnUICreate();
}

void CDevConWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag == L"ExtractButton" ) {

		OnExtract();
	}

	if( Tag == L"ExportButton" ) {

		OnExport();
	}

	CBaseClass::OnUIChange(pItem, Tag);
}

// Overridables

void CDevConWnd::OnAttach(void)
{
	CBaseClass::OnAttach();

	m_pItem = (CDevCon *) CBaseClass::m_pItem;
}

// Implementation

BOOL CDevConWnd::OnExport(void)
{
	CSaveFileDialog::LoadLastPath(L"DevCon");

	for( ;;) {

		// TODO -- Add a dialog box to select what to export.

		CSaveFileDialog Dlg;

		Dlg.SetFilter(IDS("JSON Files (*.json)|*.json"));

		Dlg.SetCaption(IDS("Export Device Configuration"));

		Dlg.SetFilename(L"devcon.json");

		if( Dlg.Execute(*afxMainWnd) ) {

			CFilename Save = Dlg.GetFilename();

			if( Save.Exists() ) {

				CPrintf Warn = CPrintf(IDS("The file %s already exists.\n\nDo you want to overwrite it?"), Save.GetName());

				switch( afxMainWnd->YesNoCancel(Warn) ) {

					case IDNO:

						continue;

					case IDCANCEL:

						return FALSE;
				}
			}

			CString Text;

			PCSTR   tag = "hspu";

			for( UINT n = 0; n < 4; n++ ) {

				char    cTag = tag[n];

				CString Part;

				m_pItem->GetConfig(cTag, Part);

				Text += Text.IsEmpty() ? "{" : ",";

				Text += CPrintf(L"\"%cconfig\":", cTag);

				Text += Part;
			}

			Text += "}";

			if( SaveFile(Save, Text) ) {

				return TRUE;
			}
		}

		return FALSE;
	}
}

BOOL CDevConWnd::OnExtract(void)
{
	CSysProxy Proxy;

	Proxy.Bind();

	if( Proxy.KillUndoList() ) {

		// TODO -- Add a dialog box to select what to upload.

		CString SConfig, PConfig, UConfig;

		if( Link_ExtractDevCon('s', SConfig) ) {

			if( Link_ExtractDevCon('p', PConfig) ) {

				if( Link_ExtractDevCon('u', UConfig) ) {

					UINT    uPos = SConfig.Find(L'\xFF');

					CString Hard = SConfig.Mid(uPos+1);

					SConfig = SConfig.Left(uPos);

					m_pItem->m_pHConfig->SetConfig(Hard);

					m_pItem->m_pHConfig->Commit();

					m_pItem->m_pSConfig->SetConfig(SConfig);

					m_pItem->m_pPConfig->SetConfig(PConfig);

					m_pItem->m_pUConfig->SetConfig(UConfig);

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CDevConWnd::SaveFile(CFilename const &Name, CString const &Data)
{
	HANDLE hFile = Name.OpenWrite();

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT uText = Data.GetLength();

		PSTR pText = New char[uText+1];

		for( UINT n = 0; (pText[n] = char(Data[n])); n++ );

		DWORD uDone = 0;

		WriteFile(hFile, pText, uText, &uDone, NULL);

		CloseHandle(hFile);

		delete[] pText;

		if( uDone == uText ) {

			return TRUE;
		}

		DeleteFile(Name);
	}

	Error(IDS("Unable to save file."));

	return FALSE;
}

// End of File
