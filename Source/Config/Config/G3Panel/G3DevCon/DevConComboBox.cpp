
#include "Intern.hpp"

#include "DevConComboBox.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Combo Box
//

// Base Class

#define CBaseClass CComboBox

// Runtime Class

AfxImplementRuntimeClass(CDevConComboBox, CBaseClass);

// Constructor

CDevConComboBox::CDevConComboBox(CDevConElement *pElem)
{
	m_pElem = pElem;

	Construct();
}

// IUnknown

HRESULT CDevConComboBox::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
		}

		return E_NOINTERFACE;
	}

	return E_POINTER;
}

ULONG CDevConComboBox::AddRef(void)
{
	return 1;
}

ULONG CDevConComboBox::Release(void)
{
	return 1;
}

// IDropTarget

HRESULT CDevConComboBox::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( !IsItemReadOnly() ) {

		if( m_pElem ) {

			if( m_pElem->CanAcceptData(pData, *pEffect) ) {

				m_dwEffect = *pEffect;

				SetDrop(1);

				return S_OK;
			}
		}
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConComboBox::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
	}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConComboBox::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
}

HRESULT CDevConComboBox::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		if( m_pElem->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			SendNotify(CBN_DROP);

			return S_OK;
		}
	}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);

	return S_OK;
}

// Message Map

AfxMessageMap(CDevConComboBox, CBaseClass)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CDevConComboBox)
};

// Message Handlers

void CDevConComboBox::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
}

void CDevConComboBox::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_uDrop ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxBrush(Orange1));

		return;
	}

	DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

	DC.FrameRect(GetClientRect(), afxColor(Enabled));
}

// Implementation

void CDevConComboBox::Construct(void)
{
	m_uDrop = 0;
}

BOOL CDevConComboBox::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
	}

	return FALSE;
}

BOOL CDevConComboBox::IsItemReadOnly(void)
{
	return m_pElem->IsReadOnly();
}

// End of File
