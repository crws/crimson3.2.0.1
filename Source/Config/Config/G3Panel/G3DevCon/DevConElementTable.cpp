
#include "Intern.hpp"

#include "DevConElementTable.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "HttpBase64.hpp"

#include "DevConListView.hpp"

#include "DevConTabWnd.hpp"

#include "DevConDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Table UI Element
//

// Base Class

#define CBaseClass CDevConElementBase

// Dynamic Class

AfxImplementDynamicClass(CDevConElementTable, CBaseClass);

// Constructor

CDevConElementTable::CDevConElementTable(void)
{
	m_pRoot  = NULL;

	m_fFixed = FALSE;

	m_fFull  = TRUE;
}

// Destructor

CDevConElementTable::~CDevConElementTable(void)
{
	delete m_pRoot;
}

// Operations

void CDevConElementTable::AddLayout(CLayFormation *pForm)
{
	m_pDataLayout = New CLayItemSize(CSize(300, 300), CSize(0, 0), CSize(4, 1));

	AddToMain(m_pDataLayout, horzLeft | vertTop);

	FinalizeLayout(pForm);
}

void CDevConElementTable::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	if( m_pDataLayout ) {

		CRect Rect  = m_pDataLayout->GetRect();

		Rect.left  += 24;

		Rect.right -= 24;

		m_pListCtrl = New CDevConListView(this, m_fFixed, m_fFull);

		m_pListCtrl->Create(L"",
				    WS_CHILD | WS_BORDER | WS_TABSTOP | LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS,
				    Rect,
				    Wnd,
				    id++
		);

		DWORD dwExStyle = LVS_EX_FULLROWSELECT | LVS_EX_ONECLICKACTIVATE | LVS_EX_GRIDLINES;

		m_pListCtrl->SetExtendedListViewStyle(dwExStyle, dwExStyle);

		CString Name = m_pTable->GetValue(L"rname");

		int     xCol = Name.IsEmpty() ? 30 : GetWidth(Wnd, Name);

		CListViewColumn ListCol(0, LVCFMT_CENTER | LVCFMT_FIXED_WIDTH, xCol, Name);

		m_pListCtrl->InsertColumn(0, ListCol);

		UINT nc = m_Cols.GetCount();

		for( UINT c = 0; c < nc; c++ ) {

			CColumn const &Col = m_Cols[c];

			if( Col.m_uSub ) {

				CListViewColumn ListCol(Col.m_uSub, LVCFMT_LEFT, Col.m_xSize, Col.m_Label);

				m_pListCtrl->InsertColumn(Col.m_uSub, ListCol);
			}
		}

		m_pListCtrl->InsertButtons();

		AddControl(m_pListCtrl);
	}
}

void CDevConElementTable::ParseConfig(CJsonData *pSchema, CJsonData *pTable)
{
	m_pSchema = pSchema;

	m_pTable  = pTable;

	m_pFields = m_pTable->GetChild(L"fields");

	UINT nc = m_pFields->GetCount();

	UINT cp = 1;

	for( UINT c = 0; c < nc; c++ ) {

		CColumn Col;

		Col.m_pField = m_pFields->GetChild(c);

		Col.m_uSub   = 0;

		if( Col.m_pField->GetValue(L"hide", L"").IsEmpty() ) {

			Col.m_xSize = watoi(Col.m_pField->GetValue(L"width", L"150"));

			Col.m_Label = Col.m_pField->GetValue(L"label");

			Col.m_uSub = cp++;
		}

		Col.m_Type  = Col.m_pField->GetValue(L"type");

		Col.m_pElem = CDevConElement::Create(Col.m_Type);

		Col.m_pElem->BindItem(m_pItem);

		Col.m_pElem->ParseConfig(m_pSchema, Col.m_pField);

		m_Cols.Append(Col);
	}

	if( pTable->GetChild(L"template") ) {

		m_fFull  = FALSE;

		m_fFixed = TRUE;
	}

	if( pTable->GetValue(L"rows").GetLength() ) {

		m_fFixed = TRUE;
	}
}

UINT CDevConElementTable::OnNotify(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID == m_pListCtrl->GetID() ) {

		if( uNotify == IDM_EDIT_PROPERTIES ) {

			UINT uRow = m_pListCtrl->GetSelection();

			return EditRow(uRow, IDS("Edit %s"));
		}

		if( uNotify == IDM_EDIT_DELETE ) {

			UINT uRow = m_pListCtrl->GetSelection();

			return DeleteRow(uRow);
		}

		if( uNotify == IDM_EDIT_INSERT ) {

			return InsertRow();
		}
	}

	return actionNone;
}

UINT CDevConElementTable::OnNotify(UINT uID, NMHDR &Info)
{
	if( uID == m_pListCtrl->GetID() ) {

		if( Info.code == NM_CLICK ) {

			NMITEMACTIVATE &Notify = (NMITEMACTIVATE &) Info;

			UINT uHit = m_pListCtrl->HitTestControl(Notify);

			if( uHit < NOTHING ) {

				if( uHit == 0 ) {

					if( UINT(Notify.iItem) < m_pData->GetCount() ) {

						return EditRow(Notify.iItem, IDS("Edit %s"));
					}

					return InsertRow();
				}

				if( uHit == 1 ) {

					if( m_fFixed ) {

						return ClearRow(Notify.iItem);
					}

					return DeleteRow(Notify.iItem);
				}

				if( uHit == 2 ) {

					return MoveRowDn(Notify.iItem);
				}

				if( uHit == 3 ) {

					return MoveRowUp(Notify.iItem);
				}
			}
		}

		if( Info.code == NM_DBLCLK ) {

			NMITEMACTIVATE &Notify = (NMITEMACTIVATE &) Info;

			return EditRow(Notify.iItem, IDS("Edit %s"));
		}

		if( Info.code == NM_RETURN ) {

			UINT uRow = m_pListCtrl->GetSelection();

			return EditRow(uRow, IDS("Edit %s"));
		}

		if( Info.code == NM_RCLICK ) {

			// TODO -- Why doesn't this work on the first click?!!!

			CMenu   Load = CMenu(L"DevConTableCtxMenu");

			CMenu & Menu = Load.GetSubMenu(0);

			Menu.SendInitMessage();

			Menu.DeleteDisabled();

			if( Menu.GetMenuItemCount() ) {

				Menu.MakeOwnerDraw(FALSE);

				Menu.TrackPopupMenu(TPM_LEFTALIGN, GetCursorPos(), *afxMainWnd);

				Menu.FreeOwnerDraw();
			}
		}
	}

	return actionNone;
}

// Overridables

void CDevConElementTable::OnSetData(void)
{
	delete m_pRoot;

	m_pRoot = New CJsonData;

	m_pRoot->Parse(L"{\"data\": " + m_Data + L"}");

	m_pData = m_pRoot->GetChild(L"data");

	LoadData();
}

BOOL CDevConElementTable::AllowPersonality(void)
{
	return FALSE;
}

// Implementation

void CDevConElementTable::LoadData(void)
{
	m_pListCtrl->DeleteAllItems();

	UINT rc = m_pData->GetCount();

	for( UINT r = 0; r < rc; r++ ) {

		CJsonData *pRow = m_pData->GetChild(r);

		m_pListCtrl->InsertItem(CListViewItem(r, 0, CPrintf(L"%u", r+1), 0, LPARAM(r)));

		SetRow(r, pRow);
	}

	AddEmptyRow();
}

void CDevConElementTable::SetRow(UINT uRow, CJsonData *pRow)
{
	UINT nc = m_Cols.GetCount();

	for( UINT c = 0; c < nc; c++ ) {

		CColumn const &Col = m_Cols[c];

		if( Col.m_uSub ) {

			CString Data = pRow->GetValue(Col.m_pField->GetValue(L"name"), L"");

			CString Disp = Col.m_pElem->FormatData(Data);

			m_pListCtrl->SetItem(CListViewItem(uRow, Col.m_uSub, Disp, 0));
		}
	}
}

UINT CDevConElementTable::ClearRow(UINT uRow)
{
	if( uRow < NOTHING ) {

		CJsonData *pRow = m_pData->GetChild(uRow);

		for( UINT i = 0; i < m_Cols.GetCount(); i++ ) {

			CJsonData *pField = m_Cols[i].m_pField;

			CString    Name   = pField->GetValue(L"name");

			CString    Data   = pField->GetValue(L"default");

			pRow->AddValue(Name, Data);
		}

		CString Edit = m_pData->GetAsText(FALSE);

		if( m_Data.CompareC(Edit) ) {

			m_Data = Edit;

			SetRow(uRow, pRow);

			return actionChange;
		}
	}

	return actionNone;
}

UINT CDevConElementTable::DeleteRow(UINT uRow)
{
	if( uRow < NOTHING ) {

		m_pData->Delete(uRow);

		// Hack to close up missing array elemenmts. We
		// really need to re-write our JSON handler...

		m_Data = m_pData->GetAsText(FALSE);

		m_pData->Parse(m_Data);

		m_pListCtrl->DeleteItem(uRow);

		m_pListCtrl->SetItemState(uRow ? uRow-1 : 0, LVIS_FOCUSED | LVIS_SELECTED, NOTHING);

		Renumber();

		return actionChange;
	}

	return actionNone;
}

UINT CDevConElementTable::InsertRow(void)
{
	UINT       uRow = m_pData->GetCount();

	CJsonData *pRow = NULL;

	m_pData->AddObject(pRow);

	if( EditRow(uRow, IDS("Insert %s")) == actionChange ) {

		CJsonData *pRow = m_pData->GetChild(uRow);

		if( m_pFields->GetChild(0U)->GetValue(L"name") == L"key" ) {

			UINT m = 99;

			for( UINT c = 0; c < m_pData->GetCount(); c++ ) {

				CJsonData *pScan = m_pData->GetChild(c);

				UINT k = watoi(pScan->GetValue(L"key"));

				MakeMax(m, k);
			}

			pRow->AddValue(L"key", CPrintf(L"%u", ++m), jsonString);
		}

		DelEmptyRow();
		
		m_pListCtrl->InsertItem(CListViewItem(uRow, 0, CPrintf(L"%u", uRow+1), 0, LPARAM(uRow)));

		SetRow(uRow, pRow);

		m_pListCtrl->SetItemState(uRow, LVIS_FOCUSED | LVIS_SELECTED, NOTHING);

		m_Data = m_pData->GetAsText(FALSE);

		AddEmptyRow();

		return actionChange;
	}

	m_pData->Parse(m_Data);

	return actionNone;
}

UINT CDevConElementTable::EditRow(UINT uRow, CString const &Verb)
{
	if( uRow < NOTHING ) {

		CJsonData *pRow = m_pData->GetChild(uRow);

		if( pRow ) {

			CViewWnd *pView = New CDevConTabWnd(pRow, m_pSchema, m_pPerson, L"", m_pTable);

			CString   Label = m_pTable->GetValue(L"label", IDS("Row"));

			CPrintf   Title = CPrintf(Verb, Label);

			pView->Attach(m_pItem);

			CDevConDialog Dialog(Title, pView);

			CSysProxy     System;

			System.Bind();

			if( System.ExecSemiModal(Dialog) == IDOK ) {

				CString Edit = m_pData->GetAsText(FALSE);

				if( m_Data.CompareC(Edit) ) {

					m_Data = Edit;

					SetRow(uRow, pRow);

					return actionChange;
				}
			}
		}
	}

	return actionNone;
}

UINT CDevConElementTable::MoveRowUp(UINT uRow)
{
	if( uRow < NOTHING ) {

		CJsonData *pData0 = m_pData->GetChild(uRow-0);
		
		CJsonData *pData1 = m_pData->GetChild(uRow-1);

		CString    Text   = pData0->GetAsText(FALSE);

		pData0->Parse(pData1->GetAsText(FALSE));

		pData1->Parse(Text);

		SetRow(uRow-0, pData0);

		SetRow(uRow-1, pData1);

		m_pListCtrl->SetItemState(uRow-1, LVIS_FOCUSED | LVIS_SELECTED, NOTHING);

		m_Data = m_pData->GetAsText(FALSE);

		return actionChange;
	}

	return actionNone;
}

UINT CDevConElementTable::MoveRowDn(UINT uRow)
{
	if( uRow < NOTHING ) {

		CJsonData *pData0 = m_pData->GetChild(uRow+0);

		CJsonData *pData1 = m_pData->GetChild(uRow+1);

		CString    Text   = pData0->GetAsText(FALSE);

		pData0->Parse(pData1->GetAsText(FALSE));

		pData1->Parse(Text);

		SetRow(uRow+0, pData0);

		SetRow(uRow+1, pData1);

		m_pListCtrl->SetItemState(uRow+1, LVIS_FOCUSED | LVIS_SELECTED, NOTHING);

		m_Data = m_pData->GetAsText(FALSE);

		return actionChange;
	}

	return actionNone;
}

void CDevConElementTable::Renumber(void)
{
	for( UINT n = 0; n < m_pListCtrl->GetItemCount(); n++ ) {

		if( !(m_pListCtrl->GetItemParam(n) & 0x80000000) ) {

			m_pListCtrl->SetItemText(n, CPrintf(L"%u", n+1));
		
			m_pListCtrl->SetItemParam(n, LPARAM(n));
			}
		else {
			m_pListCtrl->SetItemParam(n, LPARAM(n | 0x80000000));
		}
	}
}

void CDevConElementTable::DelEmptyRow(void)
{
	if( !m_fFixed ) {

		UINT uRow = m_pListCtrl->GetItemCount();

		if( uRow ) {

			m_pListCtrl->DeleteItem(uRow-1);
		}
	}
}

void CDevConElementTable::AddEmptyRow(void)
{
	if( !m_fFixed ) {

		UINT uRow = m_pListCtrl->GetItemCount();

		m_pListCtrl->InsertItem(CListViewItem(uRow, 0, L"", 0, LPARAM(uRow | 0x80000000)));
	}
}

int CDevConElementTable::GetWidth(CWnd &Wnd, CString const &Text)
{
	CClientDC DC(Wnd);

	DC.Select(afxFont(Dialog));

	return DC.GetTextExtent(Text).cx + 20;
}

// End of File
