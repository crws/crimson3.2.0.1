
#include "Intern.hpp"

#include "DevConElementString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration String UI Element
//

// Base Class

#define CBaseClass CDevConElementEditBox

// Dynamic Class

AfxImplementDynamicClass(CDevConElementString, CBaseClass);

// Constructor

CDevConElementString::CDevConElementString(void)
{
}

// Destructor

CDevConElementString::~CDevConElementString(void)
{
}

// Operations

void CDevConElementString::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	m_pEditCtrl->SetDefault(m_Default);
}

void CDevConElementString::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CStringArray List;

	pField->GetValue(L"format").Tokenize(List, ',');

	m_Type    = List[0];

	m_Default = List[1];
}

CString CDevConElementString::FormatData(CString const &Data)
{
	return Data.IsEmpty() ? m_Default : Data;
}

// Overridables

BOOL CDevConElementString::OnCheckData(CString &Data)
{
	// TODO -- Add content checks!!!

	return TRUE;
}

// End of File
