
#include "Intern.hpp"

#include "DevConElementIp.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

#include "IpAddr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration IP Address UI Element
//

// Base Class

#define CBaseClass CDevConElementEditBox

// Dynamic Class

AfxImplementDynamicClass(CDevConElementIp, CBaseClass);

// Constructor

CDevConElementIp::CDevConElementIp(void)
{
}

// Destructor

CDevConElementIp::~CDevConElementIp(void)
{
}

// Operations

void CDevConElementIp::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	m_pEditCtrl->SetContent(EC_IP);

	m_pEditCtrl->SetDefault(m_Default);
}

void CDevConElementIp::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CStringArray List;

	pField->GetValue(L"format").Tokenize(List, ',');

	m_Type    = List[0];

	m_Default = List[1];

	m_uWidth  = 15;
}

// Overridables

void CDevConElementIp::OnSetData(void)
{
	if( m_Type != L"optional" ) {

		if( m_Data.IsEmpty() && !m_Default.IsEmpty() ) {

			m_Data = m_Default;
		}
	}

	CBaseClass::OnSetData();
}

BOOL CDevConElementIp::OnCheckData(CString &Data)
{
	if( !Data.IsEmpty() ) {

		if( m_Type == L"mask" || m_Type == L"rmask" ) {

			CIpAddr Ip(Data);

			if( Data.Count('.') ) {

				if( !Ip.IsValidMask() ) {

					m_pEditCtrl->Error(IDS("This field must contain a valid IP address mask."));

					return FALSE;
				}
			}
			else {
				if( !Ip.IsEmpty() ) {

					Data = Ip.GetAsText();

					return TRUE;
				}

				m_pEditCtrl->Error(IDS("This field must contain a valid IP address mask."));

				return FALSE;
			}

			if( m_Type == L"mask" ) {

				if( Ip == IP_EMPTY || Ip == IP_BROAD ) {

					m_pEditCtrl->Error(IDS("This mask value is not valid in this context."));

					return FALSE;
				}
			}
		}

		// TODO -- Add content checks!!!

		Data = CIpAddr(Data).GetAsText();
	}

	return TRUE;
}

// End of File
