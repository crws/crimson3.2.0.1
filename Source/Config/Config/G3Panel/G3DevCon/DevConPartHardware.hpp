
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConPartHardware_HPP

#define INCLUDE_DevConPartHardware_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConPart.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Hardware Part
//

class DLLAPI CDevConPartHardware : public CDevConPart
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConPartHardware(void);

	// Destructor
	~CDevConPartHardware(void);

	// Change Check
	BOOL HasChanged(void);
	
	// Attributes
	CJsonData * GetActive(void) const;

	// Attributes
	UINT GetSoftwareGroup(void) const;

	// Operations
	void ImportFromJson(void);
	void Commit(void);
	void Revert(void);
	void SetSoftwareGroup(UINT uGroup);

	// Persistance
	void Init(void);
	void Load(CTreeFile &Tree);
	void PostLoad(void);

	// Item Properties
	CString m_Active;
	UINT    m_Level;

protected:
	// Data Members
	UINT	    m_uChange;
	CJsonData * m_pActive;

	// Active Config
	CString const & GetInitJson(void);

	// Meta Data Creation
	void AddMetaData(void);

	// Dirty Control
	void OnSetDirty(void);

	// Implementation
	BOOL FixList(CString &Data, CString const &Name, CString const &Null);
};

// End of File

#endif
