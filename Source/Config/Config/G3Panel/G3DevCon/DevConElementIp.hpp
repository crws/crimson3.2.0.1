
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementIp_HPP

#define INCLUDE_DevConElementIp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementEditBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration IP Address UI Element
//

class CDevConElementIp : public CDevConElementEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementIp(void);

	// Destructor
	~CDevConElementIp(void);

	// Operations
	void CreateControls(CWnd &Wnd, UINT &id);
	void ParseConfig(CJsonData *pSchema, CJsonData *pField);

protected:
	// Data Members
	CString m_Type;
	CString m_Default;

	// Overridables
	void OnSetData(void);
	BOOL OnCheckData(CString &Data);
};

// End of File

#endif
