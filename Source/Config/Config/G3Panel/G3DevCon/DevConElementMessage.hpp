

#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementMessage_HPP

#define INCLUDE_DevConElementMessage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Message UI Element
//

class CDevConElementMessage : public CDevConElement
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConElementMessage(CString const &Message);

	// Destructor
	~CDevConElementMessage(void);

	// Operations
	void AddLayout(CLayFormation *pForm);
	void CreateControls(CWnd &Wnd, UINT &id);

protected:
	// Data Member
	CString		m_Message;
	CLayItem      * m_pEditLayout;
	CLayFormation * m_pMainLayout;
	CCtrlWnd      * m_pTextCtrl;
};

// End of File

#endif
