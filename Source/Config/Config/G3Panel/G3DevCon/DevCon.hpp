
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevCon_HPP

#define INCLUDE_DevCon_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConPart;

class CDevConPartHardware;

class CCommsPortList;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration
//

class DLLAPI CDevCon : public CMetaItem, public IConfigStorage
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevCon(void);

	// Destructor
	~CDevCon(void);

	// Operations
	void  FinalizeImport(void);
	void  UpdateHardware(void);
	UINT  GetSoftwareGroup(void);
	void  SetSoftwareGroup(UINT uGroup);
	BOOL  HasExpansion(void);
	CSize GetDisplaySize(void);
	void  ApplyModelSpec(void);
	void  AppendModelSpec(CString &Model);
	void  GetPorts(CStringArray &List);
	void  GetModules(CStringArray &List);
	void  GetFaceList(CUIntArray &Data, CStringArray &Text);
	void  GetCertList(CUIntArray &Data, CStringArray &Text, BOOL fClient);
	UINT  ImportCert(CString const &Base, CByteArray const &Cert, CByteArray const &Priv);
	UINT  ImportCert(CString const &Base, CByteArray const &Cert);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IConfigStorage
	bool METHOD AddUpdateSink(IConfigUpdate *pUpdate);
	bool METHOD SetConfig(char cTag, CString const &Text, bool fEdit);
	bool METHOD GetConfig(char cTag, CString &Text);

	// UI Creation
	CViewWnd * CreateView(UINT uType);

	// Item Naming
	CString GetHumanName(void) const;

	// Persistance
	void Init(void);
	void PostLoad(void);

	// Item Properties
	CString		      m_Model;
	CDevConPartHardware * m_pHConfig;
	CDevConPart         * m_pPConfig;
	CDevConPart         * m_pSConfig;
	CDevConPart         * m_pUConfig;

	// Schema Objects
	IPxeModel         * m_pModel;
	ISchemaGenerator  * m_pSchema;
	IConfigUpdate     * m_pUpdate;
	IConfigApplicator * m_pApply;

protected:
	// Data Members
	ULONG m_uRefs;

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void FindObjects(void);
	void FindSchemas(void);
	void CommitEdits(void);
	BOOL AddFile(CJsonData *pJson, PCTXT pKey, PCTXT pName, CByteArray const &Data, PCTXT pType);
};

// End of File

#endif
