
#include "Intern.hpp"

#include "DevConPartWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevCon.hpp"

#include "DevConPart.hpp"

#include "DevConPartHardware.hpp"

#include "DevConGroupLabel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Part Window
//

// Base Class

#define CBaseClass CUIItemViewWnd

// Runtime Class

AfxImplementRuntimeClass(CDevConPartWnd, CBaseClass);

// Constructor

CDevConPartWnd::CDevConPartWnd(CUIPageList *pList) : CBaseClass(pList)
{
	m_pPart    = NULL;

	m_fRecycle = FALSE;
}

// Destructor

CDevConPartWnd::~CDevConPartWnd(void)
{
}

// UI Update

void CDevConPartWnd::OnUICreate(void)
{
	m_pPart = (CDevConPart *) m_pItem;

	CBaseClass::OnUICreate();

	StartGroup(IDS("Operations"), 1);

	CString Name(m_pPart->GetHumanName());

	// TODO -- Add ability to import from JSON.

	AddButton(CPrintf(IDS("Export %s as JSON"), Name), L"", L"ExportButton");

	AddButton(CPrintf(IDS("Extract %s from Device"), Name), L"", L"ExtractButton");

	EndGroup(FALSE);

	if( m_pPart->m_cTag == 'h' ) {

		StartGroup(IDS("Hardware Changes"), 1);

		AddButton(IDS("Commit Changes"), L"", L"CommitButton");

		AddButton(IDS("Revert Changes"), L"", L"RevertButton");

		EndGroup(FALSE);
	}
}

void CDevConPartWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables();
	}

	if( Tag == L"ExtractButton" ) {

		OnExtract();

		DoEnables();
	}

	if( Tag == L"ExportButton" ) {

		OnExport();

		DoEnables();
	}

	if( Tag == L"CommitButton" ) {

		OnCommit(TRUE);

		DoEnables();
	}

	if( Tag == L"RevertButton" ) {

		OnRevert();

		DoEnables();
	}

	CBaseClass::OnUIChange(pItem, Tag);
}

// Implementation

void CDevConPartWnd::DoEnables(void)
{
	if( m_pPart->m_cTag == 'h' ) {

		CDevConPartHardware *pHard = (CDevConPartHardware *) m_pPart;

		EnableUI(m_pItem, "CommitButton", pHard->HasChanged());

		EnableUI(m_pItem, "RevertButton", pHard->HasChanged());
	}
}

BOOL CDevConPartWnd::OnExport(void)
{
	CSaveFileDialog::LoadLastPath(L"DevCon");

	for( ;;) {

		CSaveFileDialog Dlg;

		Dlg.SetFilter(IDS("JSON Files (*.json)|*.json"));

		Dlg.SetCaption(CPrintf(IDS("Export %s"), m_pPart->GetHumanName()));

		Dlg.SetFilename(m_pPart->GetFileName());

		if( Dlg.Execute(*afxMainWnd) ) {

			CFilename Save = Dlg.GetFilename();

			if( Save.Exists() ) {

				CPrintf Warn = CPrintf(IDS("The file %s already exists.\n\nDo you want to overwrite it?"), Save.GetName());

				switch( afxMainWnd->YesNoCancel(Warn) ) {

					case IDNO:

						continue;

					case IDCANCEL:

						return FALSE;
				}
			}

			if( SaveFile(Save, m_pPart->GetConfig()->GetAsText(TRUE)) ) {

				return TRUE;
			}
		}

		return FALSE;
	}
}

BOOL CDevConPartWnd::OnExtract(void)
{
	CSysProxy Proxy;

	Proxy.Bind();

	if( Proxy.KillUndoList() ) {

		CString Text;

		if( Link_ExtractDevCon(m_pPart->m_cTag, Text) ) {

			if( m_pPart->m_cTag == 's' ) {

				UINT    uPos = Text.Find(L'\xFF');

				CString Hard = Text.Mid(uPos+1);

				Text = Text.Left(uPos);

				CDevCon		    *pCon  = (CDevCon *) m_pPart->GetParent();

				CDevConPartHardware *pHard = pCon->m_pHConfig;

				CString Check = pHard->GetConfig()->GetAsText(FALSE);

				if( Check.CompareC(Hard) ) {

					CString Prompt;

					Prompt += IDS("The device has a different hardware configuration.\n\n");

					Prompt += IDS("Do you want to update your database to match?");

					UINT uCode = YesNoCancel(Prompt);

					if( uCode == IDCANCEL ) {

						return TRUE;
					}

					if( uCode == IDYES ) {

						pHard->SetConfig(Hard);

						pHard->Commit();
					}
				}
			}

			if( TRUE ) {

				m_pPart->SetConfig(Text);
			}

			if( m_pPart->m_cTag == 'h' ) {

				CDevConPartHardware *pHard = (CDevConPartHardware *) m_pPart;

				if( pHard->HasChanged() ) {

					CString Text(IDS("Do you want to commit these hardware configuration changes?"));

					if( YesNo(Text) == IDYES ) {

						OnCommit(FALSE);
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CDevConPartWnd::OnCommit(BOOL fWarn)
{
	if( m_pPart->IsKindOf(AfxRuntimeClass(CDevConPartHardware)) ) {

		CSysProxy Proxy;

		Proxy.Bind();

		if( !fWarn || Proxy.KillUndoList() ) {

			CDevConPartHardware *pHard = (CDevConPartHardware *) m_pPart;

			pHard->Commit();
		}
	}

	return TRUE;
}

BOOL CDevConPartWnd::OnRevert(void)
{
	if( m_pPart->IsKindOf(AfxRuntimeClass(CDevConPartHardware)) ) {

		CSysProxy Proxy;

		Proxy.Bind();

		if( Proxy.KillUndoList() ) {

			CDevConPartHardware *pHard = (CDevConPartHardware *) m_pPart;

			pHard->Revert();
		}
	}

	return TRUE;
}

BOOL CDevConPartWnd::SaveFile(CFilename const &Name, CString const &Data)
{
	HANDLE hFile = Name.OpenWrite();

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT uText = Data.GetLength();

		PSTR pText = New char[uText+1];

		for( UINT n = 0; (pText[n] = char(Data[n])); n++ );

		DWORD uDone = 0;

		WriteFile(hFile, pText, uText, &uDone, NULL);

		CloseHandle(hFile);

		delete[] pText;

		if( uDone == uText ) {

			return TRUE;
		}

		DeleteFile(Name);
	}

	Error(IDS("Unable to save file."));

	return FALSE;
}

// End of File
