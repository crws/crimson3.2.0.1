
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Barcode Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3BARCODE_HPP
	
#define	INCLUDE_G3BARCODE_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pccore.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3barcode.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3BARCODE

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3barcode.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Barcode Engine Access
//

DLLAPI void Barcode_Render( PCTXT   pText,
			    UINT    uCode,
			    int     xSpace,
			    int     ySpace,
			    UINT    uMask,
			    PBYTE & pData,
			    int   & nStep,
			    int   & xSize,
			    int   & ySize
			    );

// End of File

#endif
