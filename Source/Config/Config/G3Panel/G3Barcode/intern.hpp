
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Barcode Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Project Specific Warning Control
//

#pragma warning(disable: 6001)
#pragma warning(disable: 6011)
#pragma warning(disable: 6054)

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3barcode.hpp"

#include "intern.hxx"

// End of File

#endif
