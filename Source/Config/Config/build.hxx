
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Configuration
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifdef _DEBUG

#include "..\..\..\Build\Bin\Version\Debug\version.hxx"

#else

#include "..\..\..\Build\Bin\Version\Release\version.hxx"

#endif

// End of File
