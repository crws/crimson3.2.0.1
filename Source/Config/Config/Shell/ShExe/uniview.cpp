
#include "intern.hpp"

#include "image.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Single Views
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Single View
//

// Runtime Class

AfxImplementRuntimeClass(CSingleView, CCrimsonView);

// Constructor

CSingleView::CSingleView(CCrimsonWnd *pParent) : CCrimsonView(pParent)
{
	
	}

// Destructor

CSingleView::~CSingleView(void)
{
	
	}

// Message Map

AfxMessageMap(CSingleView, CCrimsonView)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	
	AfxMessageEnd(CSingleView)
	};

// Message Handlers

void CSingleView::OnPostCreate(void)
{
	}

void CSingleView::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	
	}

void CSingleView::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.FillRect(Rect, afxBrush(WindowBack));

	DC.DrawRightEdge (Rect, 3, afxBrush(3dHighlight));

	DC.DrawLeftEdge  (Rect, 3, afxBrush(3dHighlight));

	DC.DrawTopEdge   (Rect, 3, afxBrush(3dHighlight));

	DC.DrawBottomEdge(Rect, 3, afxBrush(3dHighlight));

	DrawText(DC, Rect);
	}

// View Menu Support

BOOL CSingleView::OnViewControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_VIEW_SHOW_LEFT:
		case IDM_VIEW_SHOW_RIGHT:
		case IDM_VIEW_WATCH:
		case IDM_VIEW_FIND_RESULTS:
			
			Src.EnableItem(FALSE);
			Src.CheckItem (FALSE);

			return TRUE;
		}
		
	return CCrimsonView::OnViewControl(uID, Src);
	}

// Implementation

void CSingleView::DrawText(CPaintDC& DC, CRect Rect)
{
	
	}

//////////////////////////////////////////////////////////////////////////
//
// Hidden View
//

// Runtime Class

AfxImplementRuntimeClass(CHiddenView, CSingleView);

// Constructor

CHiddenView::CHiddenView(CCrimsonWnd *pParent, CString Message) : CSingleView(pParent)
{
	m_Message = Message;
	}

// Destructor

CHiddenView::~CHiddenView(void)
{
	
	}

// Message Map

AfxMessageMap(CHiddenView, CSingleView)
{
	AfxDispatchControlType(IDM_FILE, OnFileControl)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchControlType(IDM_LINK, OnLinkControl)		
	
	AfxMessageEnd(CHiddenView)
	};

// File Menu Support

BOOL CHiddenView::OnFileControl(UINT uID, CCmdSource &Src)
{
	return CCrimsonView::OnFileControl(uID, Src);
	}

// Edit Menu Support

BOOL CHiddenView::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_EDIT_FIND:
		case IDM_EDIT_FIND_ALL:

			Src.EnableItem(FALSE);
			
			return TRUE;
		}
		
	return CCrimsonView::OnEditControl(uID, Src);
	}

// Link Menu Support

BOOL CHiddenView::OnLinkControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_LINK_UPLOAD:
		case IDM_LINK_SEND_IMAGE:		
				
			Src.EnableItem(FALSE);

			return TRUE;
		}

	return CCrimsonView::OnLinkControl(uID, Src);
	}

// Implementation

void CHiddenView::DrawText(CPaintDC& DC, CRect Rect)
{
	CFont Font;

	int nSize = min(Rect.cx() / int(m_Message.GetLength()), Rect.cy() / 10);
	
	Font.Create(DC, L"Arial Black", nSize, FALSE);
	
	DC.Select(Font);

	DC.SetTextColor(afxColor(Enabled));

	CSize Size = DC.GetTextExtent(m_Message);

	int yp = (Rect.top  + Rect.bottom - Size.cy) / 2;

	int xp = (Rect.left + Rect.right  - Size.cx) / 2;

	DC.SetBkMode(TRANSPARENT);

	DC.TextOut(xp, yp - 20, m_Message);
	}

// End of File
