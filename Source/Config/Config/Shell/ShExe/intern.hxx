
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_ALL_FILES_MUST_BE   0x4000
#define IDS_AMEND_YOUR          0x4001
#define IDS_APP_BUILD           0x4002
#define IDS_AUTOSAVE_FILE       0x4004
#define IDS_AUTOSAVING          0x4005
#define IDS_BALLOON_HELP        0x4006
#define IDS_BEN_DRUCK_TESTERN   0x4007
#define IDS_BUILD_THIS          0x4008 /* NOT USED */
#define IDS_CANCEL              0x4009
#define IDS_CD3                 0x400A
#define IDS_CD31                0x400B
#define IDS_CENTERED            0x400C
#define IDS_CHECK_FOR_UPDATED   0x400D
#define IDS_CHECK_SAVE          0x400E
#define IDS_CHECK_UNTITLED      0x400F
#define IDS_CIRCULAR_1          0x4010
#define IDS_CIRCULAR_2          0x4011
#define IDS_CIRCULAR_3          0x4012
#define IDS_CMD_BAD_SWITCH      0x4013
#define IDS_CMD_EX_FILE         0x4014
#define IDS_CMD_NO_FILE         0x4015
#define IDS_CONFIGURATION_HAS   0x4016 /* NOT USED */
#define IDS_CONFIGURATION_HAS_2 0x4017
#define IDS_CONTAINS_LIBG       0x4018
#define IDS_CONTAINS_ZINT       0x4019
#define IDS_CONVERTED           0x401A
#define IDS_COPYRIGHT_ROBIN     0x401B
#define IDS_CREATING_CRIMSON    0x401C /* NOT USED */
#define IDS_CRIMSON             0x401D
#define IDS_CRIMSON_CAN         0x401E
#define IDS_CRIMSON_DATABASE    0x401F
#define IDS_CRIMSON_REQUIRES    0x4020
#define IDS_DATABASE_WAS        0x4021
#define IDS_DBASE_UPSUPPORT     0x4022
#define IDS_DEVICES_HAVE        0x4023
#define IDS_DID_YOU_ALREADY     0x4024 /* NOT USED */
#define IDS_DOWNLOAD_MODE       0x4025
#define IDS_DO_YOU_WANT_TO      0x4026
#define IDS_DO_YOU_WANT_TO_2    0x4027
#define IDS_DO_YOU_WANT_TO_3    0x4028 /* NOT USED */
#define IDS_DO_YOU_WANT_TO_4    0x4029
#define IDS_ERRORS              0x402A
#define IDS_ERRORS_IN           0x402B
#define IDS_ERRORS_STILL        0x402C
#define IDS_ERRORS_TEXT_1       0x402D
#define IDS_ERRORS_TEXT_2       0x402E
#define IDS_ERRORS_TEXT_3       0x402F
#define IDS_ERRORS_TEXT_4       0x4030
#define IDS_ERRORS_TEXT_5       0x4031
#define IDS_FILE_COULD_NOT_BE   0x4032 /* NOT USED */
#define IDS_FILE_IS_OPEN        0x4033
#define IDS_FILE_TARGET_SELECT  0x4034
#define IDS_FILE_WAS            0x4035
#define IDS_FILE_WAS_2          0x4036
#define IDS_F_KEY_TO_DISPLAY    0x4037
#define IDS_HIT_CANCEL_TO       0x4038 /* NOT USED */
#define IDS_HIT_NO_TO           0x4039 /* NOT USED */
#define IDS_HIT_YES_TO_BUILD    0x403A /* NOT USED */
#define IDS_ICON_AT_RIGHTHAND   0x403B
#define IDS_IF_PRESSING_F_KEY   0x403C
#define IDS_IMAGE_EXT           0x403D
#define IDS_IMAGE_FILTER        0x403E
#define IDS_IMAGE_SAVE          0x403F
#define IDS_IMPORTED_CRIMSON    0x4040
#define IDS_IMPORTED_FILE       0x4041
#define IDS_IMPORT_CRIMSON      0x4042
#define IDS_IMPORT_FILTER       0x4043
#define IDS_JEREMY_HOWELL       0x4044
#define IDS_KATHY_SNELL         0x4045
#define IDS_LOAD_ERROR          0x4046
#define IDS_LOAD_FILTER         0x4047
#define IDS_LOAD_FILTER_1       0x4048
#define IDS_LOUISE_YINGLING     0x4049
#define IDS_MARK_STEPHENS       0x404A
#define IDS_MIKE_GRANBY         0x404B
#define IDS_MODEL_UNSUPPORTED   0x404C /* NOT USED */
#define IDS_NATHAN_CHADMAZIRA   0x404D
#define IDS_NATIVE              0x404E
#define IDS_NEW_DATABASE        0x404F
#define IDS_NIEC_CONTROL        0x4050
#define IDS_NN                  0x4051
#define IDS_OLD_PAGES_WILL_BE   0x4052
#define IDS_OLD_PAGES_WILL_BE_2 0x4053
#define IDS_OPENING_FILE        0x4054
#define IDS_OPEN_SOURCE         0x4055
#define IDS_OPTIONS_ON_HELP     0x4056
#define IDS_OR_IF_YOU_WOULD     0x4057
#define IDS_OSS_INFO            0x4058
#define IDS_PAGES_WILL_STAY     0x4059
#define IDS_PAGES_WILL_STAY_2   0x405A
#define IDS_PAUL_PLOWRIGHT      0x405B
#define IDS_PLEASE_BE_SURE_TO   0x405C /* NOT USED */
#define IDS_PORTIONS            0x405D
#define IDS_POWERED_BY          0x405E
#define IDS_PREVENTING          0x405F
#define IDS_QUESTIONS_VIA       0x4060
#define IDS_READ                0x4061
#define IDS_REG_ABORT           0x4062
#define IDS_REG_AMEND           0x4063
#define IDS_REG_COMPANY         0x4064
#define IDS_REG_CONNECT         0x4065
#define IDS_REG_EMAIL           0x4066
#define IDS_REG_EMAIL_ERROR     0x4067
#define IDS_REG_ERROR           0x4068
#define IDS_REG_NAME            0x4069
#define IDS_REG_PATH            0x406A
#define IDS_REG_PRODUCTS        0x406B
#define IDS_REG_SENDING         0x406C
#define IDS_REG_SERVER          0x406D
#define IDS_REG_SKIP            0x406E
#define IDS_REG_TITLE           0x406F
#define IDS_REG_UPDATES         0x4070
#define IDS_REG_UPGRADE         0x4071
#define IDS_REG_USERINFO        0x4072
#define IDS_REG_VERSION         0x4073
#define IDS_ROBERT_SPENCER      0x4074
#define IDS_SAVE_CONVERSION     0x4075
#define IDS_SAVE_CONVERSION_2   0x4076
#define IDS_SAVE_ERROR          0x4077
#define IDS_SAVE_FILTER         0x4078
#define IDS_SAVING_FILE         0x4079
#define IDS_SCALED              0x407A
#define IDS_SELECT_CANCEL_TO    0x407B
#define IDS_SELECT_OK_TO_EXIT   0x407C
#define IDS_SUPPORT_URL         0x407D
#define IDS_TEAM                0x407E
#define IDS_THANK_YOU_FOR       0x407F /* NOT USED */
#define IDS_THANK_YOU_FOR_2     0x4080
#define IDS_THERE_IS_NO         0x4081
#define IDS_THIS_MODEL_DOES     0x4082
#define IDS_TRANSLATE           0x4083
#define IDS_TRANSLATE_THESE     0x4084
#define IDS_UNABLE_TO_READ      0x4085
#define IDS_UNABLE_TO_WRITE     0x4086
#define IDS_UNABLE_TO_WRITE_2   0x4087
#define IDS_UNKNOWN_METHOD      0x4088
#define IDS_UNREGISTERED        0x4089
#define IDS_UNTITLED_FILE       0x408A
#define IDS_UPDATE_ABORT        0x408B
#define IDS_UPDATE_AGENT        0x408C
#define IDS_UPDATE_AVAILABLE    0x408D
#define IDS_UPDATE_BUILD        0x408E
#define IDS_UPDATE_CLOSE        0x408F
#define IDS_UPDATE_ERROR        0x4090
#define IDS_UPDATE_FAILED       0x4091
#define IDS_UPDATE_FIND_DATA    0x4092
#define IDS_UPDATE_FIND_SETUP   0x4093
#define IDS_UPDATE_INVALID      0x4094
#define IDS_UPDATE_NONE         0x4095
#define IDS_UPDATE_OPEN_CONN    0x4096
#define IDS_UPDATE_OP_OK        0x4097
#define IDS_UPDATE_READ         0x4098
#define IDS_UPDATE_SETUP        0x4099
#define IDS_UPDATE_STATUS       0x409A
#define IDS_UPON_IMPORT_OLD     0x409B
#define IDS_UPON_IMPORT_OLD_2   0x409C
#define IDS_VALIDATING          0x409D
#define IDS_WILL_DISABLE_IP     0x409E
#define IDS_YOUR_DATABASE_MAY   0x409F
#define IDS_YOUR_INFORMATION    0x40A0 /* NOT USED */
#define IDS_YOU_ARE_ABOUT_TO    0x40A1
#define IDS_YOU_HAVE            0x40A2
#define IDS_YOU_HAVE_UNBUILT    0x40A3 /* NOT USED */
#define IDS_YOU_HAVE_UNBUILT_2  0x40A4
#define IDS_YOU_MAY_ALSO        0x40A5 /* NOT USED */
#define IDS_YOU_WILL_NOW_BE     0x40A6
#define IDS_CD32                0x40A7

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_FILE_SAVE_IMAGE	 0x8160
#define	IDM_FILE_CONVERT	 0x8161
#define	IDM_FILE_SECURITY	 0x8162
#define IDM_FILE_IMPORT		 0x8163
#define IDM_FILE_UPDATE		 0x8164
#define	IDM_FILE_EXTRA		 0x8170

#define	IDM_LINK		 0xB0
#define	IDM_LINK_TITLE		 0xB000
#define	IDM_LINK_SEND		 0xB001
#define	IDM_LINK_UPDATE		 0xB002
#define	IDM_LINK_SEND_IMAGE	 0xB003
#define	IDM_LINK_UPLOAD		 0xB004
#define	IDM_LINK_SET_TIME	 0xB005
#define	IDM_LINK_OPTIONS	 0xB008
#define	IDM_LINK_CALIBRATE	 0xB00A
#define	IDM_LINK_EMULATE	 0xB00C
#define	IDM_LINK_VERIFY		 0xB00D
#define	IDM_LINK_ONLINE		 0xB00E
#define IDM_LINK_BROWSE		 0xB011
#define IDM_LINK_FIND		 0xB012
#define IDM_LINK_FILES		 0xB013

#define	IDM_WARN		 0xB1
#define	IDM_WARN_TITLE		 0xB100
#define	IDM_WARN_SHOW_ERROR	 0xB101
#define	IDM_WARN_SHOW_CIRCLE     0xB102
#define	IDM_WARN_RECOMPILE	 0xB103
#define IDM_WARN_REBLOCK_COMMS	 0xB104
#define	IDM_WARN_REMAP_PERSIST	 0xB105

#define	IDM_HELP_UPDATE		 0x8610
#define	IDM_HELP_REGISTER	 0x8611
#define	IDM_HELP_NOTES		 0x8612
#define	IDM_HELP_REFERENCE	 0x8613

// End of File

#endif
