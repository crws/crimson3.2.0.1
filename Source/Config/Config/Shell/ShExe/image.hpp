
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Image File Builder
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMAGE_HPP

#define INCLUDE_IMAGE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Image File Structures
//

struct CImageHeader
{
	DWORD	m_dwMagic;
	WORD	m_wVersion;
	WORD	m_wFlags;
	WORD	m_wTargCount;
	WORD	m_wFileCount;
	WORD	m_wDbase;
	BYTE	m_bPad[18];
	char	m_sOEM[32];
	};

struct CImageTargRecord
{
	char	m_sName[16];
	WORD	m_wFile[8];
	};

struct CImageFileRecord
{
	char	m_sName[8];
	DWORD	m_dwPos;
	DWORD	m_dwSize;
	BYTE	m_bGUID[16];
	};

//////////////////////////////////////////////////////////////////////////
//
// Image File Builder
//

class CImageBuilder
{
	public:
		// Constructor
		CImageBuilder(CDatabase *pDbase);

		// Destructor
		~CImageBuilder(void);

		// Operations
		BOOL SaveImage(CFilename const &File);

	protected:
		// Target Data
		struct CTargData
		{
			CTargData(void)
			{
				memset(&m_Rec, 0, sizeof(m_Rec));
				}

			CString		 m_Name;
			CString		 m_Proc;
			CArray <CString> m_FileList;
			CImageTargRecord m_Rec;
			};

		// File Data
		struct CFileData
		{
			CFileData(void)
			{
				memset(&m_Rec, 0, sizeof(m_Rec));
				}

			CString		 m_Name;
			CFilename	 m_Path;
			HANDLE		 m_hSrc;
			CImageFileRecord m_Rec;
			};

		// Data Members
		CDatabase   * m_pDbase;
		CSystemItem * m_pSystem;
		HANDLE        m_hFile;
		CFilename     m_Temp;
		LONG          m_dwTable;
		LONG	      m_dwAlloc;

		// Data Talbs
		CArray <CTargData *>   m_TargList;
		CArray <CFileData *>   m_FileList;
		CMap   <CString, UINT> m_FileIndex;

		// Implementation
		BOOL SaveDbaseImage(void);
		BOOL OpenFile(CFilename const &File);
		void CloseFile(void);
		BOOL BuildTargList(void);
		BOOL BuildFileList(void);
		BOOL ReadFileInfo(void);
		BOOL OutputHeader(void);
		BOOL OutputTargTable(void);
		BOOL OutputFileTable(void);
		BOOL OutputFiles(void);
		void SetText(char *pText, PCTXT pWide);
		BOOL FindGUID(CFileData *pFile);
		BOOL OutputCRC(void);

		UINT FindVersionPos(void);
		BOOL AllowCRC(void);
	};

// End of File

#endif
