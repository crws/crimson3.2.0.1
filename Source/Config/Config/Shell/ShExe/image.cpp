
#include "intern.hpp"

#include "image.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Image File Builder
//

// Constructor

CImageBuilder::CImageBuilder(CDatabase *pDbase)
{
	m_pDbase  = pDbase;

	m_pSystem = pDbase->GetSystemItem();

	m_Temp.MakeTemporary();
}

// Destructor

CImageBuilder::~CImageBuilder(void)
{
	for( UINT t = 0; t < m_TargList.GetCount(); t++ ) {

		delete m_TargList[t];
	}

	for( UINT f = 0; f < m_FileList.GetCount(); f++ ) {

		CloseHandle(m_FileList[f]->m_hSrc);

		delete m_FileList[f];
	}

	DeleteFile(m_Temp);
}

// Operations

BOOL CImageBuilder::SaveImage(CFilename const &File)
{
	m_pDbase->PrepareData();

	if( SaveDbaseImage() ) {

		BuildTargList();

		BuildFileList();

		if( ReadFileInfo() ) {

			if( OpenFile(File) ) {

				OutputHeader();

				OutputTargTable();

				OutputFileTable();

				if( OutputFiles() ) {

					if( OutputCRC() ) {

						CloseFile();

						return TRUE;
					}
				}

				CloseFile();

				DeleteFile(File);
			}
		}
	}

	return FALSE;
}

// Implementation

BOOL CImageBuilder::SaveDbaseImage(void)
{
	return m_pDbase->SaveImage(m_Temp);
}

BOOL CImageBuilder::OpenFile(CFilename const &File)
{
	m_hFile = CreateFile(File,
			     GENERIC_WRITE | GENERIC_READ,
			     FILE_SHARE_WRITE,
			     NULL,
			     CREATE_ALWAYS,
			     FILE_ATTRIBUTE_NORMAL,
			     NULL
	);

	return m_hFile != INVALID_HANDLE_VALUE;
}

void CImageBuilder::CloseFile(void)
{
	CloseHandle(m_hFile);
}

BOOL CImageBuilder::BuildTargList(void)
{
	CStringArray List;

	CString Text = m_pSystem->GetModelList();

	Text.Tokenize(List, ',');

	for( UINT t = 0; t < List.GetCount(); t++ ) {

		CTargData *pTarg = New CTargData;

		CString    Name  = List[t];

		SetText(pTarg->m_Rec.m_sName, Name);

		pTarg->m_Name = Name;

		pTarg->m_Proc = m_pSystem->GetProcessor();

		CStringArray Files;

		m_pSystem->GetModelInfo(Name).Tokenize(Files, ',');

		pTarg->m_FileList.Append(Files[0]);

		pTarg->m_FileList.Append(Files[1]);

		if( !Files[3].IsEmpty() ) {

			pTarg->m_FileList.Append(Files[3]);
		}

		m_TargList.Append(pTarg);
	}

	return TRUE;
}

BOOL CImageBuilder::BuildFileList(void)
{
	CString Path;

	for( UINT t = 0; t < m_TargList.GetCount(); t++ ) {

		CTargData *pTarg = m_TargList[t];

		if( Link_FindFirmware(Path, pTarg->m_Proc) ) {

			UINT f;

			for( f = 0; f < pTarg->m_FileList.GetCount(); f++ ) {

				CString Name  = pTarg->m_FileList[f];

				INDEX   Index = m_FileIndex.FindName(Name);

				if( m_FileIndex.Failed(Index) ) {

					CFileData *pFile = New CFileData;

					pFile->m_Name = Name;

					pFile->m_hSrc = INVALID_HANDLE_VALUE;

					pFile->m_Path = Path + Name + L".bin";

					SetText(pFile->m_Rec.m_sName, Name);

					UINT uPos = m_FileList.Append(pFile);

					pTarg->m_Rec.m_wFile[f] = WORD(uPos);

					m_FileIndex.Insert(Name, uPos);
				}
				else {
					UINT uPos = m_FileIndex.GetData(Index);

					pTarg->m_Rec.m_wFile[f] = WORD(uPos);
				}
			}

			while( f < elements(pTarg->m_Rec.m_wFile) ) {

				pTarg->m_Rec.m_wFile[f] = 0xFFFF;

				f++;
			}

			continue;
		}

		return FALSE;
	}

	CFileData *pFile = New CFileData;

	pFile->m_Name = m_Temp;

	pFile->m_hSrc = INVALID_HANDLE_VALUE;

	pFile->m_Path = m_Temp;

	SetText(pFile->m_Rec.m_sName, L"dbase");

	UINT uPos = m_FileList.Append(pFile);

	m_FileIndex.Insert(m_Temp, uPos);

	return TRUE;
}

BOOL CImageBuilder::ReadFileInfo(void)
{
	DWORD dwAlloc = 0;

	dwAlloc += sizeof(CImageHeader);

	dwAlloc += sizeof(CImageTargRecord) * m_TargList.GetCount();

	dwAlloc += sizeof(CImageFileRecord) * m_FileList.GetCount();

	dwAlloc  = 32 * ((dwAlloc + 31) / 32);

	for( UINT f = 0; f < m_FileList.GetCount(); f++ ) {

		CFileData *pFile = m_FileList[f];

		pFile->m_hSrc = CreateFile(pFile->m_Path,
					   GENERIC_READ,
					   FILE_SHARE_READ,
					   NULL,
					   OPEN_EXISTING,
					   0,
					   NULL
		);

		if( pFile->m_hSrc == INVALID_HANDLE_VALUE ) {

			return FALSE;
		}

		if( f < m_FileList.GetCount() - 1 ) {

			if( !FindGUID(pFile) ) {

				return FALSE;
			}
		}
		else {
			CGuid const &Guid = m_pDbase->GetGuid();

			memcpy(pFile->m_Rec.m_bGUID, &Guid, 16);
		}

		pFile->m_Rec.m_dwSize = GetFileSize(pFile->m_hSrc, NULL);

		pFile->m_Rec.m_dwPos  = dwAlloc;

		dwAlloc += 32 * ((pFile->m_Rec.m_dwSize + 31) / 32);
	}

	return TRUE;
}

BOOL CImageBuilder::OutputHeader(void)
{
	CImageHeader Head;

	memset(&Head, 0, sizeof(Head));

	Head.m_dwMagic    = '3IC';
	Head.m_wVersion   = 0;
	Head.m_wFlags     = AllowCRC() ? 4 : 0;
	Head.m_wTargCount = WORD(m_TargList.GetCount());
	Head.m_wFileCount = WORD(m_FileList.GetCount());
	Head.m_wDbase     = WORD(Head.m_wFileCount - 1);

	SetText(Head.m_sOEM, C3OemCompany());

	DWORD dwPut;

	WriteFile(m_hFile,
		  &Head,
		  sizeof(Head),
		  &dwPut,
		  NULL
	);

	if( dwPut != sizeof(Head) ) {

		return FALSE;
	}

	return TRUE;
}

BOOL CImageBuilder::OutputTargTable(void)
{
	for( UINT t = 0; t < m_TargList.GetCount(); t++ ) {

		CTargData *pTarg = m_TargList[t];

		DWORD dwPut;

		WriteFile(m_hFile,
			  &pTarg->m_Rec,
			  sizeof(pTarg->m_Rec),
			  &dwPut,
			  NULL
		);

		if( dwPut != sizeof(pTarg->m_Rec) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CImageBuilder::OutputFileTable(void)
{
	for( UINT f = 0; f < m_FileList.GetCount(); f++ ) {

		CFileData *pFile = m_FileList[f];

		DWORD dwPut;

		WriteFile(m_hFile,
			  &pFile->m_Rec,
			  sizeof(pFile->m_Rec),
			  &dwPut,
			  NULL
		);

		if( dwPut != sizeof(pFile->m_Rec) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CImageBuilder::OutputFiles(void)
{
	for( UINT f = 0; f < m_FileList.GetCount(); f++ ) {

		CFileData *pFile = m_FileList[f];

		PBYTE      pData = New BYTE[pFile->m_Rec.m_dwSize];

		DWORD      dwGet = 0;

		DWORD      dwPut = 0;

		SetFilePointer(pFile->m_hSrc,
			       0,
			       NULL,
			       FILE_BEGIN
		);

		ReadFile(pFile->m_hSrc,
			 pData,
			 pFile->m_Rec.m_dwSize,
			 &dwGet,
			 NULL
		);

		if( dwGet == pFile->m_Rec.m_dwSize ) {

			SetFilePointer(m_hFile,
				       pFile->m_Rec.m_dwPos,
				       NULL,
				       FILE_BEGIN
			);

			WriteFile(m_hFile,
				  pData,
				  pFile->m_Rec.m_dwSize,
				  &dwPut,
				  NULL
			);

			if( dwPut == pFile->m_Rec.m_dwSize ) {

				delete[] pData;

				continue;
			}
		}

		delete[] pData;

		return FALSE;
	}

	return TRUE;
}

void CImageBuilder::SetText(char *pText, PCTXT pWide)
{
	while( *pWide ) {

		*pText++ = char(*pWide++);
	}
}

BOOL CImageBuilder::FindGUID(CFileData *pFile)
{
	char  sMark[8] = { 0 };

	DWORD dwGet    = 0;

	DWORD dwBuild  = 0;

	SetFilePointer(pFile->m_hSrc,
		       -24,
		       NULL,
		       FILE_END
	);

	ReadFile(pFile->m_hSrc,
		 sMark,
		 8,
		 &dwGet,
		 NULL
	);

	if( !strcmp(sMark, "GUID-->") ) {

		if( pFile->m_Name.StartsWith(L"Distro") ) {

			SetFilePointer(pFile->m_hSrc,
				       -40,
				       NULL,
				       FILE_END
			);

			ReadFile(pFile->m_hSrc,
				 sMark,
				 8,
				 &dwGet,
				 NULL
			);

			if( !strcmp(sMark, "DATA-->") ) {

				ReadFile(pFile->m_hSrc,
					 sMark,
					 8,
					 &dwGet,
					 NULL
				);

				sMark[7] = 0;

				dwBuild  = atoi(sMark);

				pFile->m_Rec.m_bGUID[0] = PBYTE(&dwBuild)[0];
				pFile->m_Rec.m_bGUID[1] = PBYTE(&dwBuild)[1];
				pFile->m_Rec.m_bGUID[2] = PBYTE(&dwBuild)[2];
				pFile->m_Rec.m_bGUID[3] = PBYTE(&dwBuild)[3];

				return TRUE;
			}
		}

		ReadFile(pFile->m_hSrc,
			 pFile->m_Rec.m_bGUID,
			 16,
			 &dwGet,
			 NULL
		);

		return dwGet == 16;
	}
	else {
		SetFilePointer(pFile->m_hSrc,
			       FindVersionPos(),
			       NULL,
			       FILE_BEGIN
		);

		ReadFile(pFile->m_hSrc,
			 &dwBuild,
			 4,
			 &dwGet,
			 NULL
		);

		if( dwGet == 4 ) {

			pFile->m_Rec.m_bGUID[0] = PBYTE(&dwBuild)[0];
			pFile->m_Rec.m_bGUID[1] = PBYTE(&dwBuild)[1];
			pFile->m_Rec.m_bGUID[2] = PBYTE(&dwBuild)[2];
			pFile->m_Rec.m_bGUID[3] = PBYTE(&dwBuild)[3];

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CImageBuilder::OutputCRC(void)
{
	if( AllowCRC() ) {

		UINT  uSize  = GetFileSize(m_hFile, NULL);

		PBYTE pCheck = new BYTE[uSize];

		if( pCheck ) {

			DWORD dwGet  = 0;

			SetFilePointer(m_hFile,
				       0,
				       NULL,
				       FILE_BEGIN
			);

			ReadFile(m_hFile,
				 pCheck,
				 uSize,
				 &dwGet,
				 NULL
			);

			if( dwGet ) {

				DWORD dwCRC  = CRC32(pCheck, uSize);

				SetFilePointer(m_hFile,
					       0,
					       NULL,
					       FILE_END
				);

				WriteFile(m_hFile,
					  &dwCRC,
					  sizeof(dwCRC),
					  &dwGet,
					  NULL);

				delete[] pCheck;

				return (dwGet == sizeof(dwCRC));
			}
		}

		delete[] pCheck;

		return FALSE;
	}

	return TRUE;
}

UINT CImageBuilder::FindVersionPos(void)
{
	if( m_pDbase->HasFlag(L"Canyon") )	return 2060;

	if( m_pDbase->HasFlag(L"Colorado") )	return 2060;

	if( m_pDbase->HasFlag(L"Graphite") )	return   28;

	if( m_pDbase->HasFlag(L"Kadet2") )	return   36;

	if( m_pDbase->HasFlag(L"Kadet") )	return   60;

	return 1028;
}

BOOL CImageBuilder::AllowCRC(void)
{
	if( m_pDbase->HasFlag(L"Kadet2") )	return TRUE;

	if( m_pDbase->HasFlag(L"Kadet") )	return FALSE;

	return TRUE;
}

// End of File
