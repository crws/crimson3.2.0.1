
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Web Update Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UPDATE_HPP

#define INCLUDE_UPDATE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <wininet.h>

#include <softpub.h>

#include <wincrypt.h>

#include <wintrust.h>

///////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma comment(lib, "wininet")

#pragma comment(lib, "wintrust")

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUpdateThread;
class CUpdateDialog;

//////////////////////////////////////////////////////////////////////////
//
// Update Commands
//

#define IDINIT		500
#define	IDDONE		501
#define	IDFAIL		502

//////////////////////////////////////////////////////////////////////////
//
// Update Thread
//

class CUpdateThread : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUpdateThread(void);

		// Destructor
		~CUpdateThread(void);

		// Management
		void Bind(CWnd *pWnd);
		BOOL Terminate(DWORD Timeout);

		// Attributes
		UINT GetBuild(void) const;
		BYTE GetSubVer(void) const;
		UINT GetFileSize(void) const;
		BOOL IsEndOfFile(void) const;

		// Operations
		void Connect(void);
		void FindBuild(void);
		void ReadBuild(void);
		void FindSetup(void);
		void ReadSetup(void);
		void TestSetup(void);
		void ExecSetup(void);

	protected:
		// Data Members
		CWnd *		m_pWnd;
		CEvent		m_UpdateEvent;
		UINT		m_uCode;
		HINTERNET	m_hNet;
		HINTERNET	m_hFile;
		BYTE		m_bData[32768];
		UINT		m_uPos;
		BOOL		m_fEOF;
		CFilename	m_Setup;
		HANDLE          m_hSave;

		// Overridables
		BOOL OnInit(void);
		UINT OnExec(void);
		void OnTerm(void);

		// Implementation
		BOOL DoConnect(void);
		BOOL DoFindBuild(void);
		BOOL DoReadBuild(void);
		BOOL DoFindSetup(void);
		BOOL DoReadSetup(void);
		BOOL DoTestSetup(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Update Dialog Box
//

class CUpdateDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUpdateDialog(BOOL fAuto);

	public:
		// Data Members
		BOOL	      m_fAuto;
		CUpdateThread m_Update;
		BOOL	      m_fDone;
		BOOL	      m_fError;
		BOOL	      m_fUpdate;
		UINT	      m_uState;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnUpdateDone(UINT uID);
		BOOL OnUpdateFail(UINT uID);

		// Implementation
		void SetDone(BOOL fError);
		void SetError(PCTXT pText);
		void ShowStatus(PCTXT pText);
		void TxFrame(void);
		BOOL RxFrame(void);
	};

// End of File

#endif
