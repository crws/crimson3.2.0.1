
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3look.hpp>

#include <shlink.hpp>

#include <htmlhelp.h>

#include <shellapi.h>

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Build Data
//

#include "..\..\build.hxx"

//////////////////////////////////////////////////////////////////////////
//
// System Libraries
//

#pragma comment(lib, "shell32.lib")

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CErrorsGadget;
class CCircleGadget;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCrimsonApp;
class CCrimsonWnd;
class CCrimsonView;
class CSingleView;
class CHiddenView;

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CCrimsonApp : public CThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCrimsonApp(void);

		// Destructor
		~CCrimsonApp(void);

		// Operations
		void HideSplash(void);

	protected:
		// Data Members
		CWnd * m_pSplash;
		UINT   m_uTicks;

		// Overridables
		BOOL OnInitialize(void);
		void OnTerminate(void);
		BOOL OnTranslateMessage(MSG &Msg);

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);

		// Registration Helper
		BOOL RegisterApp(void);

		// Implementation
		void ParseCommandLine(void);
		BOOL SetLanguage(CString const &Lang);
		BOOL CheckValidLanguage(void);
		void LoadTools(void);
		void LoadColors(void);
		void ShowSplash(void);
		BOOL CheckDotNet(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Application Window
//

class CCrimsonWnd : public CMainWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCrimsonWnd(void);

		// Destructor
		~CCrimsonWnd(void);

		// Initialization
		BOOL Initialize(void);
		void ShowWindow(void);

		// Attributes
		CDatabase * GetDatabase();

		// Operations
		BOOL AddRecent(CFilename const &Name);
		void UpdateTitleBar(void);
		BOOL CheckReg(void);

	protected:
		// Command Switches
		enum
		{
			cmdNone	  = 0,
			cmdSend   = 1,
			cmdUpdate = 2,
			};

		// Data Members
		UINT		m_uInst;
		CString         m_Model;
		CAccelerator    m_Accel;
		CIcon	        m_IconL;
		CIcon	        m_IconS;
		UINT	        m_uCommand;
		CString		m_WndClass;
		BOOL	        m_fOnline;
		CCrimsonView  * m_pView;
		CFilename       m_LoadFile;
		CString	        m_Unreg;
		CFilename       m_HelpFile;
		DWORD	        m_HelpData;
		CFilename       m_HelpManual;
		CRect	        m_ShowRect;
		BOOL	        m_ShowZoom;
		CRect	        m_SaveRect;
		BOOL	        m_SaveZoom;
		CString		m_Method;
		CString		m_UserName;
		CString		m_Password;
		BOOL		m_fEatUpdate;

		// Interface Control
		void OnUpdateInterface(void);
		void OnCreateStatusBar(void);
		void OnUpdateStatusBar(void);

		// Message Procedures
		LRESULT DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Class Definition
		PCTXT GetDefaultClassName(void) const;
		BOOL  GetClassDetails(WNDCLASSEX &Class) const;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnClose(void);
		BOOL OnQueryEndSession(void);
		void OnGetMinMaxInfo(MINMAXINFO &Info);
		void OnSize(UINT uCode, CSize Size);
		void OnMove(CPoint Pos);
		void OnEnable(BOOL fEnable);

		// Command Handlers
		BOOL OnViewRefresh(UINT uID);
		BOOL OnFileGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnFileControl(UINT uID, CCmdSource &Src);
		BOOL OnFileExecute(UINT uID);
		BOOL OnLinkControl(UINT uID, CCmdSource &Src);
		BOOL OnLinkExecute(UINT uID);
		BOOL OnHelpGetInfo(UINT uID, CCmdInfo  &Info);
		BOOL OnHelpControl(UINT uID, CCmdSource &Src);
		BOOL OnHelpExecute(UINT uID);
		BOOL OnWarnControl(UINT uID, CCmdSource &Src);
		BOOL OnWarnExecute(UINT uID);

		// Core File Commands
		BOOL OnFileNew(void);
		BOOL OnFileOpen(void);
		BOOL OnFileConvert(void);
		BOOL OnFileUpdate(void);

		// File Menu Update
		void AddExtraCmds(CMenu &Menu);

		// Link Commands
		BOOL CanLinkUpload(void);
		void OnLinkUpload(void);

		// Help Commands
		BOOL OnHelpContents(void);
		BOOL OnHelpReference(void);
		BOOL OnHelpSupport(void);
		BOOL OnHelpNotes(void);
		BOOL OnHelpUpdate(void);
		BOOL OnHelpAbout(void);

		// Model Management
		BOOL LoadModel(void);
		void SaveModel(void);
		void NewModel(void);

		// Implementation
		void LoadDisambig(void);
		BOOL AdjustMenu(CMenu &Menu);
		BOOL Navigate(CString Nav);
		BOOL OpenFile(CFilename const &Name, BOOL fRead);
		BOOL OpenFile(CFilename const &Name, BOOL fRead, BOOL fAuto);
		BOOL IsFileOpen(CFilename const &Name);
		BOOL CheckImportFrom3x(CFilename const &Name, CString &Force, BOOL &fIntegral, BOOL &fSame);
		BOOL Is30File(CFilename const &Name, CString &Model);
		BOOL Is31File(CFilename const &Name, CString &Model);
		BOOL Is3xFile(CFilename const &Name, CString &Model, UINT uMin, UINT uMax);
		BOOL Is30Filename(CFilename const &Name);
		BOOL Is31Filename(CFilename const &Name);
		void FindWindowPos(void);
		BOOL SaveWindowPos(void);
		BOOL ParseCommandLine(void);
		void HideSplash(void);
		BOOL PromptNew(void);
		BOOL InitToDefault(CDatabase * &pDbase);
		BOOL HasCalibrate(void);
		UINT GetCrimsonCount(void);
		BOOL SendCommand(BOOL fUpdate);
		BOOL CheckRecover(void);
		BOOL WarnConvert(void);
		BOOL HasContents(void);
		BOOL HasReference(void);

		// Link Hooks
		BOOL LinkUpload(CFilename &File);
		BOOL LinkSend(CDatabase *pDbase);
		BOOL LinkUpdate(CDatabase *pDbase);
	};

//////////////////////////////////////////////////////////////////////////
//
// Application View
//

class CCrimsonView : public CSystemWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCrimsonView(CCrimsonWnd *pParent);

		// Destructor
		~CCrimsonView(void);

		// Attributes
		BOOL	    IsDirty(void) const;
		BOOL	    IsMucky(void) const;
		BOOL	    HasFilename(void) const;
		CDatabase * GetDatabase(void);
		CFilename   GetFilename(void) const;
		CString     GetFullModel(void) const;
		CString     GetDisplayName(void) const;
		CString     GetCaption(void) const;

		// Operations
		void SetNewFile(void);
		void SetCaption(void);
		void SetDirty(void);
		void ClearDirty(void);
		void SaveConversion(void);
		BOOL CheckSave(void);
		BOOL OnFileSaveAs(void);

	protected:
		// Static Data
		static UINT m_timerSave;
		static UINT m_uSequence;

		// Data Members
		CCrimsonWnd *m_pParent;
		CString	     m_Caption;
		BOOL	     m_fAutoSave;
		UINT	     m_uAutoTime;
		CFilename    m_AutoFile;
		HANDLE	     m_hMarker;
		BOOL         m_fHasSim;
		BOOL	     m_fOnline;
		BOOL	     m_fUpload;

		// Overribables
		void OnAttach(void);
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Link Menu Support
		BOOL OnLinkGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnLinkControl(UINT uID, CCmdSource &Src);
		BOOL OnLinkCommand(UINT uID);

		// File Menu Support
		BOOL OnFileControl(UINT uID, CCmdSource &Src);
		BOOL OnFileCommand(UINT uID);
		void OnFileSave(void);
		BOOL OnFileSaveImage(void);

		// Implementation
		BOOL SaveFile(CFilename const &Name);
		BOOL SaveImage(CFilename const &Name);
		BOOL SendCommit(void);
		BOOL DropLink(void);
		BOOL CheckPending(void);
		BOOL CheckControl(void);
		BOOL CheckQueries(void);
		BOOL FindAutoParams(void);
		void LoadAutoTimer(UINT uMins);
		void KillAutoTimer(void);
		void LinkSend(void);
		void LinkUpdate(void);
		void LinkVerify(void);
		void LinkOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Single View
//

class CSingleView : public CCrimsonView
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSingleView(CCrimsonWnd *pParent);

		// Destructor
		~CSingleView(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnPaint(void);
		
		// Implementation
	virtual	void DrawText(CPaintDC &DC, CRect Rect);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Hidden View
//

class CHiddenView : public CSingleView
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CHiddenView(CCrimsonWnd *pParent, CString Message);

		// Destructor
		~CHiddenView(void);

	protected:

		CString m_Message;		

		// Message Map
		AfxDeclareMessageMap();

		// Commmand Handlers
		BOOL OnFileControl(UINT uID, CCmdSource &Src);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnLinkControl(UINT uID, CCmdSource &Src);

		// Overridables
		void DrawText(CPaintDC &DC, CRect Rect);
	};


//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSplashWnd;
class CAboutBox;

//////////////////////////////////////////////////////////////////////////
//
// Splash Window
//

class CSplashWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSplashWnd(void);

		// Destructor
		~CSplashWnd(void);

	protected:
		// Data Members
		CBitmap      m_Bitmap;
		CSize        m_Size;
		CStringArray m_List;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Implementation
		void BuildList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// About Message Box
//

class CAboutBox : public CMessageBox
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAboutBox(PCTXT pText);
		
	protected:
		// Data Members
		BOOL  m_fShow;
		CRect m_Logo;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnPaint(void);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);

		// Button Location
		BOOL FindButton(TCHAR cTag, CButtonInfo &Info);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CWarningGadget;
class CErrorsGadget;
class CCircleGadget;

//////////////////////////////////////////////////////////////////////////
//
// Warning Gadget
//

class CWarningGadget : public CTextGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CWarningGadget(CCrimsonWnd *pWnd, UINT uID, CString Text);

		// Destructor
		~CWarningGadget(void);

		// Operations
		BOOL HideToolTip(void);

		// Overridables
		void OnCreate(CDC &DC);
		void OnPaint(CDC &DC);
		void OnSetPosition(void);
		BOOL OnMouseMenu(CPoint const &Pos);
		BOOL OnMouseDown(CPoint const &Pos);
		void OnMouseUp(CPoint const &Pos, BOOL fHit);
		void OnMouseMove(CPoint const &Pos, BOOL fHit);
		void OnSetFlags(void);

	protected:
		// Static Data
		static BOOL m_fShow;

		// Data Members
		CCrimsonWnd * m_pMainWnd;
		CToolTip    * m_pToolTip;
		BOOL          m_fWarn;
		CString       m_Title;
		CString       m_Prompt;
		CString       m_Remain;
		UINT	      m_uCmd;
		CMenu	      m_Menu;

		// Overridables
		virtual BOOL HasError(CSystemItem *pSystem)   = 0;
		virtual void ClearError(CSystemItem *pSystem) = 0;
		virtual void ShowError(CSystemItem *pSystem)  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Database Errors Gadget
//

class CErrorsGadget : public CWarningGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CErrorsGadget(CCrimsonWnd *pWnd, UINT uID);

	protected:
		// Overridables
		BOOL HasError(CSystemItem *pSystem);
		void ClearError(CSystemItem *pSystem);
		void ShowError(CSystemItem *pSystem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Circular Reference Gadget
//

class CCircleGadget : public CWarningGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCircleGadget(CCrimsonWnd *pWnd, UINT uID);

	protected:
		// Overridables
		BOOL HasError(CSystemItem *pSystem);
		void ClearError(CSystemItem *pSystem);
		void ShowError(CSystemItem *pSystem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Hardware Changes Gadget
//

class CHardwareGadget : public CWarningGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CHardwareGadget(CCrimsonWnd *pWnd, UINT uID);

	protected:
		// Overridables
		BOOL HasError(CSystemItem *pSystem);
		void ClearError(CSystemItem *pSystem);
		void ShowError(CSystemItem *pSystem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Database Convert Dialog
//

class CConvertDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CConvertDialog(void);
		
	protected:
		// Data Members

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnSelect(UINT uID);

		// Implementation
		void DoEnables(void);
		void SaveData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Support Dialog
//

class CSupportDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CSupportDialog(BOOL fOem);
		
	protected:
		// Data Members
		CRect         m_Rect;
		CString       m_Text;
		CFont         m_Font;
		CToolTip    * m_pToolTip;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPaint(void);
		void OnMove(CPoint Pos);

		// Command Handlers
		BOOL OnEmail(UINT uID);
		BOOL OnWebsite(UINT uID);
		BOOL OnCommandOK(UINT uID);

		// Tool Tip
		void CreateToolTip(void);
		void ShowToolTip(void);

		// Implementation
		void FindRects(void);
		void PaintImage(CDC &DC, HGLOBAL hData);
	};

// End of File

#endif
