
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include <c3look.hxx>
	
#include "intern.hxx"
	
//////////////////////////////////////////////////////////////////////////
//								
// Version Data
//

#include "version.rcc"

//////////////////////////////////////////////////////////////////////////
//
// Manifest
//

1 RT_MANIFEST "shexe.man"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#include "shexe.loc"
				      
//////////////////////////////////////////////////////////////////////////
//
// Images
//

F1Key GIF "images/f1key.gif"

//////////////////////////////////////////////////////////////////////////
//
// Icons
//

Not_C3App ICON "images\c3app.ico"

Not_C3Doc ICON "images\c3doc.ico"

Not_C3Img ICON "images\c3img.ico"

Not_C3Wid ICON "images\c3wid.ico"

//////////////////////////////////////////////////////////////////////////
//
// String Table
//

STRINGTABLE
BEGIN
	IDS_ALL_FILES_MUST_BE   "All files must be saved with the Crimson 3.2 file type."
	IDS_AMEND_YOUR          "Amend Your Crimson 3.2 Registration"
	IDS_APP_BUILD           "Crimson %s (%s Build 3.2.%4.4u.%u)\n\nCopyright %s 1996-%u %s\n\nAll Rights Reserved Worldwide\n\n\n"
	IDS_APP_CAPTION         "Crimson 3.2"
	IDS_AUTOSAVE_FILE       "An auto-save file from your last session has been found."
	IDS_AUTOSAVING          "Auto-saving database..."
	IDS_BALLOON_HELP        "Balloon Help"
	IDS_BEN_DRUCK_TESTERN   "Ben Druck, Tester\n"
	IDS_BUILD_THIS          "build this control project before downloading to the target device, \n" /* NOT USED */
	IDS_CANCEL              "Cancel"
	IDS_CD3                 ".cd3"
	IDS_CD31                ".cd31"
	IDS_CD32                ".cd32"
	IDS_CENTERED            " Centered"
	IDS_CHECK_FOR_UPDATED   "Check for an updated version using the command on the Help menu."
	IDS_CHECK_SAVE          "Do you want to save changes to %s?"
	IDS_CHECK_UNTITLED      "the untitled file"
	IDS_CIRCULAR_1          "Circular"
	IDS_CIRCULAR_2          "Circular References"
	IDS_CIRCULAR_3          "Circular references still exist in the database."
	IDS_CMD_BAD_SWITCH      "Invalid switch on command line."
	IDS_CMD_EX_FILE         "Too many filenames on command line."
	IDS_CMD_NO_FILE         "No filename specified on command line."
	IDS_CONFIGURATION_HAS   "The configuration has been changed.\n\n" /* NOT USED */
	IDS_CONFIGURATION_HAS_2 "The configuration has been changed.\n\nDo you want to download the changes?"
	IDS_CONTAINS_LIBG       "Contains LibG from the GCC Compiler Collection"
	IDS_CONTAINS_ZINT       "Contains Zint - The Open Source Barcode Library\n"
	IDS_COPYRIGHT_ROBIN     "Copyright � 2008 Robin Stuart (robin@zint.org.uk)\n\n"
	IDS_CREATING_CRIMSON    "Creating Crimson 3.2 objects..." /* NOT USED */
	IDS_CRIMSON             "CRIMSON 3.2 DEVELOPMENT TEAM\n\n"
	IDS_CRIMSON_CAN         "Crimson 3.2 can provide you with immediate answers to many of your "
	IDS_CRIMSON_DATABASE    "The Crimson 3.0 database that you have opened is for a device that is not supported by Crimson 3.2. You must therefore select a device to which you would like the database to be converted. There are several options, shown below. If a display is fitted and the display size of the new device does not match that of the old, the method to be used to convert the Display Pages will be described in the Import Notes box."
	IDS_CRIMSON_REQUIRES    "Crimson 3.2 requires the Microsoft .NET Framework 4.0 or later.\n\n"
	IDS_DATABASE_WAS        "The database was created for a device not supported by this revision of Crimson.\n\n"
	IDS_DBASE_UPSUPPORT     "The image file can be built as to support database upload.\n\nDo you want to include support for this feature?"
	IDS_DEVICES_HAVE        "The devices have identical displays."
	IDS_DID_YOU_ALREADY     "Did you already perform this operation?" /* NOT USED */
	IDS_DOWNLOAD_MODE       "Download Only Mode"
	IDS_DO_YOU_WANT_TO      "Do you want to attempt to recover this file?"
	IDS_DO_YOU_WANT_TO_2    "Do you want to view the new Release Notes?"
	IDS_DO_YOU_WANT_TO_3    "Do you want to download the changes?" /* NOT USED */
	IDS_DO_YOU_WANT_TO_4    "Do you want to continue with this operation?"
	IDS_ERRORS              "Errors"
	IDS_ERRORS_IN           "Errors in Database"
	IDS_ERRORS_STILL        "Errors still exist in the database."
	IDS_ERRORS_TEXT_1       "You have one or more errors in the database. "
	IDS_ERRORS_TEXT_2       "Click the red notification to "
	IDS_ERRORS_TEXT_3       "find all the errors. You can then step through them using the F4. You may also right-click to "
	IDS_ERRORS_TEXT_4       "see more options."
	IDS_ERRORS_TEXT_5       "You have circular references in the database. "
	IDS_FILE_COULD_NOT_BE   "The file could not be imported%s." /* NOT USED */
	IDS_FILE_IS_OPEN        "The file '%s' is currently open and has been changed.\n\nDo you want to revert to the previous version?"
	IDS_FILE_TARGET_SELECT  "To select a target device other than the %s, use the New command on the File menu."
	IDS_FILE_WAS_2          "The file was recovered successfully."
	IDS_F_KEY_TO_DISPLAY    "F1 key to display information about the settings that are available."
	IDS_HIT_CANCEL_TO       "Hit Cancel to cancel the download.\n" /* NOT USED */
	IDS_HIT_NO_TO           "Hit No to download without a built project.\n\n" /* NOT USED */
	IDS_HIT_YES_TO_BUILD    "Hit Yes to build the project before download.\n\n" /* NOT USED */
	IDS_ICON_AT_RIGHTHAND   "icon at the right-hand edge of the toolbar, or select one of the "
	IDS_IF_PRESSING_F_KEY   "If pressing the F1 key did not result in the appearance of help information, "
	IDS_IMAGE_EXT           ".ci3"
	IDS_IMAGE_FILTER        "Crimson 3.1 Image Files (*.ci3)|*.ci3"
	IDS_IMAGE_SAVE          "Save Image File"
	IDS_IMPORTED_CRIMSON    "Imported Crimson 3.0 or 3.1 files must be saved as Crimson 3.2 files.\n\n"
	IDS_IMPORTED_FILE       "Imported File"
	IDS_IMPORT_CRIMSON      "Import Crimson 3.x Database"
	IDS_IMPORT_FILTER       "Crimson 3.1 Databases (*.cd31)|*.cd31|Crimson 3.0 Databases (*.cd3)|*.cd3"
	IDS_JEREMY_HOWELL       "Jeremy Howell, Lead Tester\n"
	IDS_KATHY_SNELL         "Kathy Snell, Developer\n"
	IDS_LOAD_ERROR          "Unable to load from the specified file."
	IDS_LOAD_FILTER         "Crimson 3.2 Databases (*.cd32)|*.cd32|Crimson 3.1 Databases (*.cd31)|*.cd31|Crimson 3.0 Databases (*.cd3)|*.cd3|Crimson 3.x Databases (*.cd3*)|*.cd3*"
	IDS_LOAD_FILTER_1       "Crimson 3.2 Databases (*.cd32)|*.cd32"
	IDS_LOUISE_YINGLING     "Louise Yingling, Tester"
	IDS_MARK_STEPHENS       "Mark Stephens, Developer\n"
	IDS_MIKE_GRANBY         "Mike Granby, Architect and Lead Developer\n"
	IDS_MODEL_UNSUPPORTED   " - Model is not supported" /* NOT USED */
	IDS_NATHAN_CHADMAZIRA   "Nathan Chadmazira, Developer\n\n"
	IDS_NATIVE              " Native"
	IDS_NEW_DATABASE        "New Database"
	IDS_NIEC_CONTROL        "\nIEC 61131-3 Control"
	IDS_NN                  "\n\n"
	IDS_OLD_PAGES_WILL_BE   "The old pages will be centered within the new device's display with black vertical bars on either side. The pages will not otherwise be modified."
	IDS_OLD_PAGES_WILL_BE_2 "The old pages will be centered within the new device's display with a black frame around them. The pages will not otherwise be modified."
	IDS_OPENING_FILE        "Opening File..."
	IDS_OPEN_SOURCE         "OPEN SOURCE LIBRARIES\n\n"
	IDS_OPTIONS_ON_HELP     "options on the Help Menu."
	IDS_OR_IF_YOU_WOULD     "or if you would like to adjust the behavior of Balloon Help, click on the "
	IDS_OSS_INFO            "OSS Info..."
	IDS_PAGES_WILL_STAY     "The pages will stay the same size in Crimson but the new device will run in an emulation mode, scaling each pixel by 200%. The active display area will be centered within the device's display with vertical black bars on either side."
	IDS_PAGES_WILL_STAY_2   "The pages will stay the same size in Crimson but the new device will run in an emulation mode, scaling each pixel by 125%. The page will occupy all of the new device's display. The non-integral scaling may reduce display quality."
	IDS_PAUL_PLOWRIGHT      "Paul Plowright, Team Manager and Senior Developer\n"
	IDS_PLEASE_BE_SURE_TO   "Please be sure to check for software updates on a regular\nbasis by using the command provided on the Help menu.\n\n" /* NOT USED */
	IDS_PORTIONS            "Portions Copyright � 2017 The FreeType Project\n\n"
	IDS_POWERED_BY          "Powered By"
	IDS_PREVENTING          "preventing download via this method in future.\n\n"
	IDS_QUESTIONS_VIA       "questions via Balloon Help. Simply select a parameter and press the "
	IDS_READ                "READ"
	IDS_REG_ABORT           "Are you sure you want to abort the registration process?"
	IDS_REG_AMEND           "This copy of Crimson 3.2 is already registered.\n\nDo you wish to amend your registration details?"
	IDS_REG_COMPANY         "Red Lion Controls"
	IDS_REG_CONNECT         "Opening internet connection."
	IDS_REG_EMAIL           "Validating email address."
	IDS_REG_EMAIL_ERROR     "The email address cannot be validated."
	IDS_REG_ERROR           "The registration could not be completed."
	IDS_REG_NAME            "Crimson"
	IDS_REG_PATH            "/forms/registration_handler.asp"
	IDS_REG_PRODUCTS        "Send me data on Red Lion products."
	IDS_REG_SENDING         "Sending registration information."
	IDS_REG_SERVER          "sellmore.redlion.net"
	IDS_REG_SKIP            "Are you sure you want to skip the registration process?"
	IDS_REG_TITLE           "Register Your Copy of Crimson 3.2"
	IDS_REG_UPDATES         "Send me data on Crimson 3.2 updates."
	IDS_REG_UPGRADE         "You have installed a new version of Crimson. Do you want\nto register the upgrade so you can be kept up-to-date with\ninformation relating to this build?"
	IDS_REG_USERINFO        "Collecting user information."
	IDS_REG_VERSION         "3.2"
	IDS_ROBERT_SPENCER      "Robert Spencer, Senior System Developer\n\n"
	IDS_SAVE_CONVERSION     "Save Conversion"
	IDS_SAVE_CONVERSION_2   "Save Conversion to Crimson 3.2"
	IDS_SAVE_ERROR          "Unable to save to the specified file."
	IDS_SAVE_FILTER         "Crimson 3.2 Databases (*.cd32)|*.cd32"
	IDS_SAVING_FILE         "Saving File..."
	IDS_SCALED              " Scaled"
	IDS_SELECT_CANCEL_TO    "Select Cancel to exit Crimson and install the software yourself."
	IDS_SELECT_OK_TO_EXIT   "Select OK to exit Crimson and install this software automatically.\n\n"
	IDS_SUPPORT_URL         "https://www.redlion.net/support"
	IDS_TEAM                "Team..."
	IDS_THANK_YOU_FOR       "Thank you for registering Crimson 3.2!\n\n" /* NOT USED */
	IDS_THANK_YOU_FOR_2     "Thank you for registering Crimson 3.2!\n\nYour information has been transmitted.\n\nPlease be sure to check for software updates on a regular\nbasis by using the command provided on the Help menu.\n\nYou may also visit us at http://www.redlion.net"
	IDS_THERE_IS_NO         "There is no upgrade path available for this database."
	IDS_THIS_MODEL_DOES     "This model does not offer display support."
	IDS_TRANSLATE           "Translate"
	IDS_TRANSLATE_THESE     "translate these programs before downloading to the target device,\n"
	IDS_UNABLE_TO_READ      "Unable to read from temporary file."
	IDS_UNABLE_TO_WRITE     "Unable to write to new file."
	IDS_UNABLE_TO_WRITE_2   "Unable to write to temporary file."
	IDS_UNKNOWN_METHOD      "Unknown Method!"
	IDS_UNREGISTERED        " - UNREGISTERED COPY"
	IDS_UNTITLED_FILE       "Untitled File"
	IDS_UPDATE_ABORT        "&Abort"
	IDS_UPDATE_AGENT        "C3.2 Update"
	IDS_UPDATE_AVAILABLE    "An updated version of Crimson 3.2 is available.\n\nDo you want to download and install this version?"
	IDS_UPDATE_BUILD        "https://update.redlion.net/c32/update/build.txt"
	IDS_UPDATE_CLOSE        "&Close"
	IDS_UPDATE_ERROR        "ERROR - %s"
	IDS_UPDATE_FAILED       "The operation encountered an error."
	IDS_UPDATE_FIND_DATA    "Finding update build data."
	IDS_UPDATE_FIND_SETUP   "Finding update setup program."
	IDS_UPDATE_INVALID      "The link entered an invalid state."
	IDS_UPDATE_NONE         "No software update is available."
	IDS_UPDATE_OPEN_CONN    "Opening internet connection."
	IDS_UPDATE_OP_OK        "The operation completed without error."
	IDS_UPDATE_READ         "Reading update build data."
	IDS_UPDATE_SETUP        "https://update.redlion.net/c32/update/setup.exe"
	IDS_UPDATE_STATUS       "Reading update setup program -- Received %uK"
	IDS_UPON_IMPORT_OLD     "Upon import, the old pages will be enlarged by a factor of 200% with fonts being substituted with larger fonts where possible. The page will be centered within the new display with vertical black bars on either side."
	IDS_UPON_IMPORT_OLD_2   "Upon import, the old pages will be enlarged by a factor of 125% with fonts being substituted with larger fonts where possible. The page will occupy all of the new device's display."
	IDS_VALIDATING          "Validating signature..."
	IDS_WILL_DISABLE_IP     "will disable IP download support in the target device, thereby\n"
	IDS_YOUR_DATABASE_MAY   "your database may not function as expected.\n\n"
	IDS_YOUR_INFORMATION    "Your information has been transmitted.\n\n" /* NOT USED */
	IDS_YOU_ARE_ABOUT_TO    "You are about to download via IP, but the current database\n"
	IDS_YOU_HAVE            "You have un-translated programs in your database. If you do not\n"
	IDS_YOU_HAVE_UNBUILT    "You have an un-built control project in your database. If you do not\n" /* NOT USED */
	IDS_YOU_HAVE_UNBUILT_2  "You have an un-built control project in your database. If you do not\nbuild this control project before downloading to the target device,\nyour database may not function as expected.\n\nHit Yes to build the project before download.\n\nHit No to download without a built project.\n\nHit Cancel to cancel the download.\n"
	IDS_YOU_MAY_ALSO        "You may also visit us at https://www.redlion.net" /* NOT USED */
	IDS_YOU_WILL_NOW_BE     "You will now be asked to select the location to which to save."
END

//////////////////////////////////////////////////////////////////////////
//
// Prompt Table
//

STRINGTABLE
BEGIN
	IDM_LINK_TITLE		"Contains commands for downloading databases."
	IDM_LINK_SEND		"[Send]Send the entire database to the target device."
	IDM_LINK_UPDATE		"[Update]Send database changes to the target device."
	IDM_LINK_VERIFY		"Verify whether the database matches the one in the target device."
	IDM_LINK_SEND_IMAGE	"[Support Upload]Indicate whether to send an uploadable image of the database to the target device."
	IDM_LINK_UPLOAD		"[Extract Database]Upload the source database from the target device."
	IDM_LINK_SET_TIME	"[Set Time]Send the PC's real time clock setting to the device."
	IDM_LINK_OPTIONS	"Configure the serial or USB link to the target device."
	IDM_LINK_CALIBRATE	"Display the calibration tool for Controller Series modules."
	IDM_LINK_EMULATE	"[Emulator]Enable or disable the G3 Emulator."
	IDM_LINK_BROWSE		"[Browse Device]Access the target device via your web browser."
	IDM_LINK_FILES		"[Open FTP Site]Access the target device via an FTP connection."
	IDM_LINK_FIND		"[Find Device]Select Crimson device via USB or on your local network."
	IDM_FILE_SAVE_IMAGE	"Save a database image capable of being transferred via a Memory Card."
	IDM_FILE_CONVERT	"Convert the database for use on a different target device."
	IDM_FILE_SECURITY	"Edit the protection settings for this database."
	IDM_FILE_IMPORT		"Import a Crimson 3.0 database and convert it to the latest version."
	IDM_HELP_UPDATE		"Check to see if an updated version of Crimson is available."
	IDM_HELP_REGISTER	"Register this copy of the software."
	IDM_HELP_NOTES		"Show the release notes for the current version."
	IDM_HELP_REFERENCE	"Show the Crimson 3.2 Reference Manual."
	IDM_WARN_SHOW_ERROR	"Jump to the first error in the database."
	IDM_WARN_SHOW_CIRCLE    "Jump to the first circular reference in the database."
	IDM_WARN_RECOMPILE	"Recompile all items in the database."
	IDM_WARN_REBLOCK_COMMS	"Optimize the blocking of communuications items."
	IDM_EDIT_FIND		"[Find]Find the first occurrence of a text string within the current item."
	IDM_EDIT_FIND_ALL	"[Global Find]Find all occurrences of a text string within the database."
END

//////////////////////////////////////////////////////////////////////////
//
// Main Accelerators
//

MainMenu ACCELERATORS
BEGIN
	"N",		IDM_FILE_NEW,		VIRTKEY,	CONTROL
	"O",		IDM_FILE_OPEN,		VIRTKEY,	CONTROL
	"S",		IDM_FILE_SAVE,		VIRTKEY,	CONTROL
	"Z",		IDM_EDIT_UNDO,		VIRTKEY,	CONTROL
	"X",		IDM_EDIT_CUT,		VIRTKEY,	CONTROL
	"C",		IDM_EDIT_COPY,		VIRTKEY,	CONTROL
	"V",		IDM_EDIT_PASTE,		VIRTKEY,	CONTROL
	VK_BACK,	IDM_EDIT_UNDO,		VIRTKEY,	ALT
	VK_DELETE,	IDM_EDIT_CUT,		VIRTKEY,	SHIFT
	VK_INSERT,	IDM_EDIT_COPY,		VIRTKEY,	CONTROL
	VK_INSERT,	IDM_EDIT_PASTE,		VIRTKEY,	SHIFT
	VK_F9,		IDM_LINK_SEND,		VIRTKEY,	SHIFT
	VK_F9,		IDM_LINK_UPDATE,	VIRTKEY
	VK_TAB,		IDM_VIEW_TAB_NEXT,	VIRTKEY,	CONTROL
	VK_TAB,		IDM_VIEW_TAB_PREV,	VIRTKEY,	CONTROL,SHIFT
	VK_F1,		IDM_HELP_BUTTON,	VIRTKEY
	VK_F5,		IDM_VIEW_REFRESH,	VIRTKEY
	VK_F5,		IDM_VIEW_CYCLE_LANG,	VIRTKEY,	CONTROL,SHIFT
END

//////////////////////////////////////////////////////////////////////////
//
// Head Menu
//

HeadMenu MENU
BEGIN
	POPUP "&File"
	BEGIN
		MENUITEM "&New...\tCtrl+N", 		IDM_FILE_NEW
		MENUITEM "&Open...\tCtrl+O", 		IDM_FILE_OPEN
		MENUITEM "&Convert...",			IDM_FILE_CONVERT
		MENUITEM SEPARATOR
		MENUITEM "&Save...\tCtrl+S", 		IDM_FILE_SAVE
		MENUITEM "Save &As...", 		IDM_FILE_SAVE_AS
		MENUITEM "Save I&mage...", 		IDM_FILE_SAVE_IMAGE
		MENUITEM SEPARATOR
		MENUITEM "Prot&ection...",		IDM_FILE_SECURITY
		MENUITEM SEPARATOR
		POPUP	 "&Utilities"
		BEGIN
		MENUITEM SEPARATOR
		END
		MENUITEM SEPARATOR
		MENUITEM "E&xit",			IDM_FILE_EXIT
	END
	POPUP "&Edit"
	BEGIN
		MENUITEM "Can't Undo\tCtrl+Z",		IDM_EDIT_UNDO
		MENUITEM "Can't Redo\tCtrl+Y",		IDM_EDIT_REDO
		MENUITEM SEPARATOR
		MENUITEM "Cu&t\tCtrl+X",		IDM_EDIT_CUT
		MENUITEM "&Copy\tCtrl+C",		IDM_EDIT_COPY
		MENUITEM "&Paste\tCtrl+V",		IDM_EDIT_PASTE
		MENUITEM "Paste &Special...",		IDM_EDIT_PASTE_SPECIAL
		MENUITEM "&Delete",			IDM_EDIT_DELETE
		MENUITEM SEPARATOR
		POPUP    "&Find"
		BEGIN
		MENUITEM "&Find...\tCtrl+F",		IDM_EDIT_FIND
		MENUITEM "Find &Next\tF3",		IDM_EDIT_FIND_NEXT
		MENUITEM "Find &Previous\tShift+F3",	IDM_EDIT_FIND_PREV
		END
		POPUP    "Find &Global"
		BEGIN
		MENUITEM "Find...\tCtrl+Shift+F",	IDM_EDIT_FIND_ALL
		MENUITEM "Find &Next\tF4",		IDM_EDIT_FIND_ALL_NEXT
		MENUITEM "Find &Previous\tShift+F4",	IDM_EDIT_FIND_ALL_PREV
		END
	END
	POPUP "&View"
	BEGIN
		MENUITEM "&Toolbar",			IDM_VIEW_TOOLBAR
		MENUITEM "&Quick Bars",			IDM_VIEW_GHOSTBAR
		MENUITEM SEPARATOR
		MENUITEM "&Navigation Pane\tAlt+F1",	IDM_VIEW_SHOW_LEFT
		MENUITEM "&Resource Pane\tAlt+F2",	IDM_VIEW_SHOW_RIGHT
		MENUITEM "&Watch Window\tF7",		IDM_VIEW_WATCH
		MENUITEM "&Search Results\tF8",		IDM_VIEW_FIND_RESULTS
	END
	POPUP "&Go"
	BEGIN
		MENUITEM "&Back\tAlt+Left",		IDM_GO_BACK
		MENUITEM "&Forward\tAlt+Right",		IDM_GO_FORWARD
		MENUITEM "&Previous\tAlt+Up",		IDM_GO_PREV_1
		MENUITEM "&Next\tAlt+Down",		IDM_GO_NEXT_1
		MENUITEM SEPARATOR
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Tail Menu
//

TailMenu MENU
BEGIN
	POPUP "&Link"
	BEGIN
		MENUITEM "&Update...\tF9",		IDM_LINK_UPDATE
		MENUITEM "&Send...\tShift+F9",		IDM_LINK_SEND
		MENUITEM "&Verify...",			IDM_LINK_VERIFY
		MENUITEM "&Extract...",			IDM_LINK_UPLOAD
		MENUITEM SEPARATOR
		MENUITEM "Support Up&load",		IDM_LINK_SEND_IMAGE
		MENUITEM SEPARATOR
		MENUITEM "&Find Device...",		IDM_LINK_FIND
		MENUITEM "&Browse Device...",		IDM_LINK_BROWSE
		MENUITEM "O&pen FTP Site...",		IDM_LINK_FILES
		MENUITEM "Send &Time...",		IDM_LINK_SET_TIME
		MENUITEM "&Calibration...",		IDM_LINK_CALIBRATE
		MENUITEM SEPARATOR
		MENUITEM "&Options...",			IDM_LINK_OPTIONS
	END
	POPUP "&Help"
	BEGIN
		MENUITEM "&Contents...",		IDM_HELP_CONTENTS
		MENUITEM "Re&ference...",		IDM_HELP_REFERENCE
		MENUITEM SEPARATOR
		MENUITEM "&Show Balloon Now\tF1",	IDM_HELP_BUTTON
		MENUITEM SEPARATOR
		POPUP    "&Balloon Help"
		BEGIN
		MENUITEM "When &Requested",		IDM_HELP_BALLOON_OFF
		MENUITEM "When &Mouse Over",		IDM_HELP_BALLOON_SOFT
		MENUITEM "When &Selected",		IDM_HELP_BALLOON_HARD
		END
		MENUITEM SEPARATOR
		MENUITEM "&Technical Support...",	IDM_HELP_SUPPORT
		MENUITEM "Check for &Update...",	IDM_HELP_UPDATE
		MENUITEM "&Register Software...",	IDM_HELP_REGISTER
		MENUITEM "Show Release &Notes...",	IDM_HELP_NOTES
		MENUITEM SEPARATOR
		MENUITEM "&About...",			IDM_HELP_ABOUT
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Head Toolbar
//

HeadTool MENU
BEGIN
	MENUITEM "10000009",			IDM_GO_BACK
	MENUITEM "1000000A",			IDM_GO_FORWARD
	MENUITEM SEPARATOR
	MENUITEM "10000000",			IDM_FILE_NEW
	MENUITEM "10000001",			IDM_FILE_OPEN
	MENUITEM "10000002",			IDM_FILE_SAVE
	MENUITEM SEPARATOR
	MENUITEM "10000003",			IDM_EDIT_UNDO
	MENUITEM "10000004",			IDM_EDIT_REDO
	MENUITEM SEPARATOR
	MENUITEM "10000005",			IDM_EDIT_CUT
	MENUITEM "10000006",			IDM_EDIT_COPY
	MENUITEM "10000007",			IDM_EDIT_PASTE
	MENUITEM SEPARATOR
	MENUITEM "1000002E",			IDM_EDIT_FIND_ALL
	MENUITEM "10000031",			IDM_VIEW_FIND_RESULTS
//	MENUITEM "1000002F",			IDM_EDIT_FIND_NEXT
//	MENUITEM "10000030",			IDM_EDIT_FIND_PREV
	MENUITEM SEPARATOR
END

//////////////////////////////////////////////////////////////////////////
//
// Tail Toolbar
//

TailTool MENU
BEGIN
	MENUITEM "10000036",			IDM_VIEW_WATCH
	MENUITEM SEPARATOR
	MENUITEM "10000034",			IDM_LINK_EMULATE
	MENUITEM SEPARATOR
	MENUITEM "10000040",			IDM_LINK_FIND
	MENUITEM "1000003F",			IDM_LINK_BROWSE
	MENUITEM "10000041",			IDM_LINK_FILES
	MENUITEM SEPARATOR
	MENUITEM "10000022",			IDM_LINK_UPDATE
	MENUITEM SEPARATOR
	POPUP "4000000D"
	BEGIN
	MENUITEM "Show &Now\tF1",		IDM_HELP_BUTTON
	MENUITEM SEPARATOR
	MENUITEM "When &Requested",		IDM_HELP_BALLOON_OFF
	MENUITEM "When Mouse &Over",		IDM_HELP_BALLOON_SOFT
	MENUITEM "When &Selected",		IDM_HELP_BALLOON_HARD
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Errors Menu
//

ErrorsMenu MENU
BEGIN
	POPUP ""
	BEGIN
		MENUITEM "Find Errors",			IDM_WARN_SHOW_ERROR
		MENUITEM SEPARATOR
		MENUITEM "Recompile Database",		IDM_WARN_RECOMPILE
		MENUITEM "Rebuild Comms Blocks",	IDM_WARN_REBLOCK_COMMS
//		MENUITEM "Remap Persistent Data",	IDM_WARN_REMAP_PERSIST
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Circle Menu
//

CircleMenu MENU
BEGIN
	POPUP ""
	BEGIN
		MENUITEM "Find Circular References",	IDM_WARN_SHOW_CIRCLE
		MENUITEM SEPARATOR
		MENUITEM "Recompile Database",		IDM_WARN_RECOMPILE
		MENUITEM "Rebuild Comms Blocks",	IDM_WARN_REBLOCK_COMMS
//		MENUITEM "Remap Persistent Data",	IDM_WARN_REMAP_PERSIST
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Check Update Dialog Box
//

UpdateDlg DIALOG 0, 0, 0, 0
CAPTION "Check for Update"
BEGIN
	GROUPBOX	"&Status", 0, 4, 4, 160, 64
	LTEXT		"Opening internet connection.", 100, 10, 25, 148, 10
	DEFPUSHBUTTON   "&Abort", IDOK, 4, 72, 40, 14, XS_BUTTONFIRST
END	

//////////////////////////////////////////////////////////////////////////
//
// Registration Dialog Box
//

RegisterDlg DIALOG 0, 0, 0, 0
CAPTION "Register Your Copy of Crimson 3.2"
BEGIN
	GROUPBOX	"Details", -1, 4,   4, 240, 230
	LTEXT		"Name:",  -1, 10, 20, 40, 10
	EDITTEXT	200, 50, 18, 184, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"Email:",  -1, 10, 40, 40, 10
	EDITTEXT	201, 50, 38, 184, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"Company:",  -1, 10, 60, 40, 10
	EDITTEXT	202, 50, 58, 184, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"Street:",  -1, 10, 80, 40, 10
	EDITTEXT	203, 50, 78, 184, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"City:",  -1, 10, 100, 40, 10
	EDITTEXT	204, 50, 98, 184, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"State:",  -1, 10, 120, 40, 10
	EDITTEXT	205, 50, 118, 184, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"ZIP:",  -1, 10, 140, 40, 10
	EDITTEXT	206, 50, 138, 72, 12, WS_BORDER | WS_TABSTOP
	LTEXT		"Country:",  -1, 10, 160, 40, 10
	COMBOBOX	207, 50, 158, 184, 112, XS_DROPDOWNLIST | CBS_SORT
	LTEXT		"Product ID:",  -1, 10, 180, 40, 10
	EDITTEXT	208, 50, 178, 184, 12, WS_BORDER | WS_TABSTOP | ES_READONLY
	CONTROL		"Send me data on Crimson 2.0 updates.", 400, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 200, 184, 14 
	CONTROL		"Send me data on Red Lion products.",   401, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 214, 184, 14 
	GROUPBOX	"Status",  -1, 4, 238, 240, 38
	LTEXT		"Collecting user information.", 100, 10, 254, 188, 10
	DEFPUSHBUTTON   "Register", IDOK,      4, 282, 40, 14, XS_BUTTONFIRST
	PUSHBUTTON	"Skip",     IDCANCEL, 48, 282, 40, 14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// Select Model Dialog
//

SelectModelDlg DIALOG 0, 0, 0, 0
CAPTION "New Database"
BEGIN
	GROUPBOX	"&Family",	1000,		  4,   4,  90, 120
	LISTBOX				1001,		 10,  18,  77, 108, XS_LISTBOX | LBS_USETABSTOPS
	GROUPBOX	"&Model",	1100,		 98,   4, 220, 120
	LISTBOX				1101,		104,  18, 207, 108, XS_LISTBOX | LBS_USETABSTOPS
	DEFPUSHBUTTON   "OK",		IDOK,		214, 128,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	258, 128,  40,  14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// Select Model Tree Dialog
//

SelectModelTreeDlg DIALOG 0, 0, 0, 0
CAPTION "New Database"
BEGIN
	GROUPBOX	"&Models",		1000,		  4,   4, 120, 264
	GROUPBOX	"Details",		1300,		128,   4, 126, 130
	GROUPBOX	"&Variants",		1100,		128, 138, 126, 130
	GROUPBOX	"Image",		1200,		258,   4, 180, 264
	CONTROL		"Click for More Information", 1201, "CHotLinkCtrl", WS_TABSTOP, 258, 272, 100, 14 
	DEFPUSHBUTTON   "OK",			IDOK,		  4, 272,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	 48, 272,  40,  14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// Import Model Tree Dialog
//

ImportModelTreeDlg DIALOG 0, 0, 0, 0
CAPTION "Import Database"
BEGIN
	GROUPBOX	"Instructions",		1500,		  4,   4, 434,  56
	GROUPBOX	"&Models",		1000,		  4,  64, 120, 160
	GROUPBOX	"Import Notes",		1400,		  4, 228, 120, 100
	GROUPBOX	"Details",		1300,		128,  64, 126, 130
	GROUPBOX	"&Variants",		1100,		128, 198, 126, 130
	GROUPBOX	"Image",		1200,		258,  64, 180, 264
	CONTROL		"Click for More Information", 1201, "CHotLinkCtrl", WS_TABSTOP, 258, 332, 100, 14 
	DEFPUSHBUTTON   "OK",			IDOK,		  4, 332,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	 48, 332,  40,  14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// Change Model Dialog
//

ChangeModelDlg DIALOG 0, 0, 0, 0
CAPTION "Save Conversion"
BEGIN
	GROUPBOX	"Instructions",	-1,		  4,   4, 314,  78
	GROUPBOX	"&Family",	1000,		  4,  84, 100, 120
	LISTBOX				1001,		 10,  98,  87, 108, XS_LISTBOX | LBS_USETABSTOPS
	GROUPBOX	"&Model",	1100,		108,  84, 210, 120
	LISTBOX				1101,		114,  98, 197, 108, XS_LISTBOX | LBS_USETABSTOPS
	DEFPUSHBUTTON   "&Convert",	IDOK,		234, 208,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	278, 208,  40,  14, XS_BUTTONREST
	LTEXT		"The database conversion process involves saving a new copy of your existing database in a",	-1, 14, 18, 270, 10
	LTEXT		"form that is suitable for use in a new target device. To begin the process, select the new",	-1, 14, 28, 270, 10
	LTEXT		"target device using the lists below. You will then be asked to select a filename under which",	-1, 14, 38, 270, 10
	LTEXT		"the new database will be saved. After the conversion process has been completed, the new",	-1, 14, 48, 270, 10
	LTEXT		"database will be opened for editing.",								-1, 14, 58, 270, 10
END	

//////////////////////////////////////////////////////////////////////////
//
// Change Model Dialog (Wide)
//

ChangeModelDlg_Wide DIALOG 0, 0, 0, 0
CAPTION "Save Conversion"
BEGIN
	GROUPBOX	"Instructions",	-1,		  4,   4, 364,  78
	GROUPBOX	"&Family",	1000,		  4,  84, 120, 120
	LISTBOX				1001,		 10,  98, 107, 108, XS_LISTBOX | LBS_USETABSTOPS
	GROUPBOX	"&Model",	1100,		128,  84, 240, 120
	LISTBOX				1101,		134,  98, 227, 108, XS_LISTBOX | LBS_USETABSTOPS
	DEFPUSHBUTTON   "&Convert",	IDOK,		284, 208,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	328, 208,  40,  14, XS_BUTTONREST
	LTEXT		"The database conversion process involves saving a new copy of your existing database in a",	-1, 14, 18, 340, 10
	LTEXT		"form that is suitable for use in a new target device. To begin the process, select the new",	-1, 14, 28, 340, 10
	LTEXT		"target device using the lists below. You will then be asked to select a filename under which",	-1, 14, 38, 340, 10
	LTEXT		"the new database will be saved. After the conversion process has been completed, the new",	-1, 14, 48, 340, 10
	LTEXT		"database will be opened for editing.",								-1, 14, 58, 340, 10
END	

//////////////////////////////////////////////////////////////////////////
//
// Import Dialog
//

ImportDlg DIALOG 0, 0, 0, 0
CAPTION "Import C2 Database"
BEGIN
	GROUPBOX	"Instructions",	-1,		  4,   4, 294,  92
	CONTROL		" I accept that the importation process may be less than 100% complete. ", 100, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 104, 220, 14 
	CONTROL		" I have performed the Validate All Tags operation in %s. ",               101, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 120, 220, 14 	
	DEFPUSHBUTTON   "&Import",		IDOK,		  4,  140,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",		IDCANCEL,	 48,  140,  40,  14, XS_BUTTONREST	
	LTEXT		"The database importation process allows a database created using %s to be loaded into",	  1000, 14, 18, 270, 10
	LTEXT		"%s.  However, some properties may not be imported correctly and may have undesired",		  1001, 14, 28, 270, 10
	LTEXT		"results.  If you are happy with your %s database and do not need the richer functionality",      1002, 14, 38, 270, 10 
	LTEXT		"provided by %s, you should not import your %s database.",                                        1003, 14, 48, 270, 10
	LTEXT		"Before starting the process, you must perform a Validate All Tags operation on the %s",	  1004, 14, 68, 270, 10
	LTEXT		"database before importing it into %s, or errors could occur after the import.",                  1005, 14, 78, 270, 10
	
END

//////////////////////////////////////////////////////////////////////////
//
// Convert Dialog
//

ConvertDlg DIALOG 0, 0, 0, 0
CAPTION "Convert C3 Database"
BEGIN
	GROUPBOX	"Instructions",	-1, 4, 4, 294, 42
	CONTROL		" I accept that the conversion process may be less than 100% complete. ", 100, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 54, 220, 14 	
	CONTROL		" Do not show this warning again. ",					  101, "button", WS_TABSTOP | BS_AUTOCHECKBOX, 10, 70, 220, 14 	
	DEFPUSHBUTTON   "&Continue", IDOK,      4, 90, 40, 14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",    IDCANCEL, 48, 90, 40, 14, XS_BUTTONREST	
	LTEXT		"The database conversion process allows a database created for one %s model to be converted into",   1000, 14, 18, 270, 10
	LTEXT		"another.  However, some properties may not be converted correctly and may have undesired results.", 1001, 14, 28, 270, 10
	
END

//////////////////////////////////////////////////////////////////////////
//
// Support Dialog
//

SupportDlg DIALOG 0, 0, 0, 0
CAPTION "Technical Support"
BEGIN
	GROUPBOX	"Read This First",		1000,	 4,   4, 292,  173
	CONTROL		"Balloon Help",			1002,  "static", 0,  70, 22, 120, 24

	GROUPBOX	"&Internet Support",		  -1,	 4, 181, 144,  36	
	CONTROL		"https://www.redlion.net/support", 1001, "CHotLinkCtrl", WS_TABSTOP, 8, 194, 136, 14 

	GROUPBOX	"Company &Website",		  -1,  152, 181, 144,  36
	CONTROL		"https://www.redlion.net", 1003, "CHotLinkCtrl", WS_TABSTOP, 156, 194, 136, 14 

	GROUPBOX	"Worldwide &Headquarters",	  -1,	 4, 221, 144,  48	
	LTEXT		"Email:",			  -1,   10, 235,  40,  14
	CONTROL		"support@redlion.net",	2001, "CHotLinkCtrl", WS_TABSTOP, 50, 232,  80, 14 
	LTEXT		"Phone:",			  -1,   10, 251,  40,  10
	LTEXT		"+1 (717) 767-6511",		2002,   50, 251,  80,  10

	GROUPBOX	"&Europe, Middle East and Africa",	  -1,  152, 221, 144,  48	
	LTEXT		"Email:",			  -1,  158, 235,  40,  14
	CONTROL		"europe@redlion.net",		3001, "CHotLinkCtrl", WS_TABSTOP, 198, 232,  80, 14
	LTEXT		"Phone:",			  -1,  158, 251,  40,  10
	LTEXT		"+31 (0) 33-4723-225",		3002,  198, 251,  80,  10

	GROUPBOX	"&China",			  -1,	 4, 273, 144,  48
	LTEXT		"Email:",			  -1,   10, 287,  40,  14
	CONTROL		"asiatech@redlion.net",		4001, "CHotLinkCtrl", WS_TABSTOP, 50, 284,  80, 14 
	LTEXT		"Phone:",			  -1,   10, 303,  40,  10
	LTEXT		"+86 21 6113-3688",		4002,   50, 303,  80,  10

	GROUPBOX	"&India",		          -1,  152, 273, 144,  48
	LTEXT		"Email:",			  -1,  158, 287,  40,  14
	CONTROL		"india@redlion.net",		5001, "CHotLinkCtrl", WS_TABSTOP, 198, 284,  80, 14
	LTEXT		"Phone:",			  -1,  158, 303,  40,  10
	LTEXT		"+91 98795-40403",		5002,  198, 303,  80,  10
	
	DEFPUSHBUTTON   "Close",		        IDOK,    4, 327, 40,  14, XS_BUTTONFIRST
END	

//////////////////////////////////////////////////////////////////////////
//
// Support Dialog -- OEM 
//

SupportOEMDlg DIALOG 0, 0, 0, 0
CAPTION "Technical Support"
BEGIN
	GROUPBOX	"Important",			1000,	 4,   4, 292,  173
	LTEXT		"Web Page:",			  -1,   10,  160,  40,  14
	CONTROL		"www.redlion.net",		1001, "CHotLinkCtrl", WS_TABSTOP,  50,  157,  180, 14 
	CONTROL		"Balloon Help",			1002,  "static", 0,                70,   22, 120, 24

	DEFPUSHBUTTON   "Close",		        IDOK,    4, 181, 40, 14, XS_BUTTONFIRST
END	

// End of File
