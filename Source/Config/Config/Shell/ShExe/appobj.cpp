
#include "intern.hpp"

#include "register.hpp"

#include "ModelInfo.hpp"

#include <c3comms.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Runtime Class

AfxImplementRuntimeClass(CCrimsonApp, CThread);

// Constructor

CCrimsonApp::CCrimsonApp(void)
{
	m_pSplash = NULL;
	
	LoadTools();

	LoadColors();

	RegisterApp();
	}

// Destructor

CCrimsonApp::~CCrimsonApp(void)
{
	}

// Operations

void CCrimsonApp::HideSplash(void)
{
	if( m_pSplash ) {

		UINT uGone = GetTickCount() - m_uTicks;

		UINT uTime = 1000;

		if( uGone < uTime ) {

			Sleep(max(uTime - uGone, 200));
			}
		else
			Sleep(200);

		m_pSplash->DestroyWindow(FALSE);

		m_pSplash = NULL;
		}
	}

// Overridables

BOOL CCrimsonApp::OnInitialize(void)
{
	ParseCommandLine();

	ShowSplash();

	CModelInfo::LoadLib();

	CCrimsonWnd *pWnd = New CCrimsonWnd;

	pWnd->Create( CString(IDS_APP_CAPTION),
		      /*WS_EX_COMPOSITED,*/
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      CRect(),
		      AfxNull(CWnd),
		      AfxNull(CMenu),
		      NULL
		      );

	if( pWnd->Initialize() ) {

		if( CheckDotNet() ) {

			BOOL fRegister = C3OemFeature(L"Registration", TRUE);

			BOOL fExtract  = C3OemFeature(L"AutoExtract",  FALSE);

			if( fRegister ) {

				HideSplash();

				if( !CRegisterDialog().Check(*pWnd, FALSE) ) {

					if( C3_BETA ) {

						pWnd->Error( L"This software must be registered.\n\n"
							     L"Crimson 3.2 will now close."
							     );

						pWnd->DestroyWindow(TRUE);

						delete this;

						return FALSE;
						}
					}

				pWnd->CheckReg();
				}

			pWnd->ShowWindow();

			pWnd->UpdateWindow();

			HideSplash();

			if( fExtract ) {

				pWnd->PostMessage(WM_COMMAND, IDM_LINK_ONLINE);
				}

			return TRUE;
			}
		}

	HideSplash();

	pWnd->PostMessage(WM_COMMAND, IDM_FILE_EXIT);

	return TRUE;
	}

void CCrimsonApp::OnTerminate(void)
{
	Link_KillEmulate();

	CModelInfo::FreeLib();

	delete this;
	}

BOOL CCrimsonApp::OnTranslateMessage(MSG &Msg)
{
	return CDialogViewWnd::IsModelessMessage(Msg);
	}

// Message Map

AfxMessageMap(CCrimsonApp, CThread)
{
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CCrimsonApp)
	};

// Command Handlers

BOOL CCrimsonApp::OnCommandExecute(UINT uID)
{
	switch( uID ) {

		case IDM_FILE_EXIT:

			afxMainWnd->SendMessage(WM_CLOSE);

			break;

		default:

			return FALSE;
		}
		
	return TRUE;
	}

BOOL CCrimsonApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:

			Src.EnableItem(TRUE);
			
			break;
		
		default:

			return FALSE;
		}
		
	return TRUE;
	}

// Registration Helper

BOOL CCrimsonApp::RegisterApp(void)
{
	CString   Type = L".wid31";

	CString   Name = L"Crimson31.Widget";

	CString   Desc = L"Crimson 3.1 Widget File";

	CFilename Prog = afxModule->GetFilename();

	CFilename Path = Prog.GetDirectory();

	if( TRUE ) {

		CRegKey Key(HKEY_CLASSES_ROOT);

		Key.MoveTo(Type);

		Key.SetValue(NULL, Name);
		}

	if( TRUE ) {

		CRegKey Key1(HKEY_CLASSES_ROOT);

		CRegKey Key2(HKEY_CLASSES_ROOT);

		Key1.MoveTo(Name);

		Key1.SetValue(NULL, Desc);

		Key2.MoveTo(L"DefaultIcon");

		Key2.SetValue(NULL, Prog + L",3");
		}

	if( TRUE ) {

		CRegKey Key(HKEY_LOCAL_MACHINE);

		Key.MoveTo(L"Software\\Microsoft\\Windows\\CurrentVersion\\App Paths");

		Key.Delete(L"shexe.exe");
		}

	return TRUE;
	}

// Implementation

void CCrimsonApp::ParseCommandLine(void)
{
	UINT uCount = afxModule->GetArgumentCount();
	
	BOOL fMatch = FALSE;

	BOOL fNoLoc = FALSE;

	for( UINT n = 0; n < uCount; n++ ) {
		
		CString Arg = afxModule->GetArgument(n);

		if( Arg[0] == '-' || Arg[0] == '/' ) {

			if( Arg.Mid(1) == L"beta" ) {

				C3AllowBetaDrivers();

				continue;
				}

			if( Arg.Mid(1) == L"lang" ) {

				CString Lang = afxModule->GetArgument(++n);

				if( !fNoLoc ) {

					if( C3OemFeature(L"Localize", TRUE) ) {

						if( SetLanguage(Lang) ) {

							fMatch = TRUE;
							}
						}
					}

				continue;
				}
			}
		}

	if( fNoLoc ) {

		SetLanguage(L"en-us");
		}
	else {
		if( !fMatch ) {

			if( C3OemFeature(L"Localize", TRUE) ) {

				CRegKey Reg  = afxModule->GetUserRegKey();

				Reg.MoveTo(L"Language");

				CString Lang = Reg.GetValue(L"Code", L"");

				if( !Lang.IsEmpty() ) {

					if( SetLanguage(Lang) ) {

						return;
						}
					}
				}

			CheckValidLanguage();
			}
		}
	}

BOOL CCrimsonApp::SetLanguage(CString const &Lang)
{
	WORD l = LANG_NEUTRAL;

	WORD s = SUBLANG_DEFAULT;

	struct CList { PCTXT p; WORD l; WORD s; };

	static CList const List[] = {

		{	L"en",		LANG_ENGLISH,	SUBLANG_ENGLISH_US		},
		{	L"en-us",	LANG_ENGLISH,	SUBLANG_ENGLISH_US		},
		{	L"en-uk",	LANG_ENGLISH,	SUBLANG_ENGLISH_UK		},
	//	{	L"fr",		LANG_FRENCH,	SUBLANG_FRENCH			},
	//	{	L"de",		LANG_GERMAN,	SUBLANG_GERMAN			},
	//	{	L"es",		LANG_SPANISH,	SUBLANG_SPANISH_MODERN		},
	//	{	L"zh-cn",	LANG_CHINESE,	SUBLANG_CHINESE_SIMPLIFIED	},

		};

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; n < elements(List); n++ ) {

			CString a = Lang;

			CString b = List[n].p;

			if( p == 1 ) {

				a = a.Left(2);

				b = b.Left(2);
				}

			if( a == b ) {

				l = List[n].l;

				s = List[n].s;

				break;
				}
			}
		}

	if( l != LANG_NEUTRAL ) {

		if( afxVista ) {

			HMODULE hModule = LoadLibrary(L"kernel32");

			FARPROC pfnProc = GetProcAddress(hModule, "SetThreadUILanguage");

			if( pfnProc ) {

				typedef void (__stdcall * PSETLANG)(LANGID);

				PSETLANG pfnSetLang = PSETLANG(pfnProc);

				(*pfnSetLang)(LANGID(MAKELANGID(l, s)));
				}

			FreeLibrary(hModule);
			}

		SetThreadLocale(MAKELCID(MAKELANGID(l, s), SORT_DEFAULT));

		return TRUE;
		}

	return FALSE;
	}

BOOL CCrimsonApp::CheckValidLanguage(void)
{
	LCID Lang = GetThreadLocale();

	switch( PRIMARYLANGID(Lang) ) {

		case LANG_ENGLISH:
	//	case LANG_FRENCH:
	//	case LANG_GERMAN:
	//	case LANG_SPANISH:
	//	case LANG_CHINESE:

			return TRUE;
		}

	SetLanguage(L"en-us");

	return FALSE;
	}

void CCrimsonApp::LoadTools(void)
{
	}

void CCrimsonApp::LoadColors(void)
{
	if( C3OemFeature(L"OemColors", FALSE) ) {

		CStdTools *pStd = afxStdTools;

		pStd->SetColor(CStdTools::colOrange1, C3OemGetColor(L"Orange1", afxColor(Orange1)));
		pStd->SetColor(CStdTools::colOrange2, C3OemGetColor(L"Orange2", afxColor(Orange2)));
		pStd->SetColor(CStdTools::colOrange3, C3OemGetColor(L"Orange3", afxColor(Orange3)));

		pStd->SetColor(CStdTools::colBlue1, C3OemGetColor(L"Blue1", afxColor(Blue1)));
		pStd->SetColor(CStdTools::colBlue2, C3OemGetColor(L"Blue2", afxColor(Blue2)));
		pStd->SetColor(CStdTools::colBlue3, C3OemGetColor(L"Blue3", afxColor(Blue3)));
		pStd->SetColor(CStdTools::colBlue4, C3OemGetColor(L"Blue4", afxColor(Blue4)));

		pStd->SetColor(CStdTools::colNavBar1, C3OemGetColor(L"NavBar1", afxColor(NavBar1)));
		pStd->SetColor(CStdTools::colNavBar2, C3OemGetColor(L"NavBar2", afxColor(NavBar2)));
		pStd->SetColor(CStdTools::colNavBar3, C3OemGetColor(L"NavBar3", afxColor(NavBar3)));
		pStd->SetColor(CStdTools::colNavBar4, C3OemGetColor(L"NavBar4", afxColor(NavBar4)));
		}
	}

void CCrimsonApp::ShowSplash(void)
{
	if( C3OemFeature(L"ShowSplash", TRUE) ) {

		CRegKey Reg = afxModule->GetUserRegKey();

		Reg.MoveTo(L"Splash");

		if( Reg.GetValue(L"Show", 1) ) {

			m_pSplash = New CSplashWnd;

			m_pSplash->Create( L"CrimsonSplash",
					   WS_EX_TOOLWINDOW,
					   WS_POPUP,
					   CRect(),
					   GetDesktopWindow(),
					   AfxNull(CMenu),
					   NULL
					   );

			m_pSplash->MakeTopmost(TRUE);

			m_pSplash->ShowWindow(SW_SHOW);

			m_pSplash->UpdateWindow();

			m_uTicks = GetTickCount();
			}
		}
	}

BOOL CCrimsonApp::CheckDotNet(void)
{
	CRegKey Root(HKEY_LOCAL_MACHINE);

	CRegKey Key1 = Root.Open(L"Software\\Microsoft\\NET Framework Setup\\NDP\\v4");

	CRegKey Key2 = Root.Open(L"Software\\Microsoft\\NET Framework Setup\\NDP\\v4.0");

	if( !Key1.IsValid() && !Key2.IsValid() ) {

		HideSplash();

		CString Text;

		Text += CString(IDS_CRIMSON_REQUIRES);

		Text += CString(IDS_SELECT_OK_TO_EXIT);

		Text += CString(IDS_SELECT_CANCEL_TO);

		C3OemStrings(Text);

		if( afxMainWnd->OkCancel(Text) == IDOK ) {

			CString Program = afxModule->GetFilename().WithName(L"Installers\\dotnet4.exe");

			SHELLEXECUTEINFO Info;

			memset(&Info, 0, sizeof(Info));

			Info.cbSize       = sizeof(Info);

			Info.fMask        = 0;

			Info.lpFile       = Program;

			Info.lpParameters = NULL;

			Info.nShow        = SW_SHOW;

			ShellExecuteEx(&Info);
			}

		return FALSE;
		}

	return TRUE;
	}

// End of File
