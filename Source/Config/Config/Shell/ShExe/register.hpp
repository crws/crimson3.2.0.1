
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Web Registration Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_REGISTER_HPP

#define INCLUDE_REGISTER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <wininet.h>

#include <windns.h>

///////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma comment(lib, "wininet")

#pragma comment(lib, "dnsapi")

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRegisterThread;
class CRegisterDialog;

//////////////////////////////////////////////////////////////////////////
//
// Registration Commands
//

#define IDINIT		500
#define	IDDONE		501
#define	IDFAIL		502

//////////////////////////////////////////////////////////////////////////
//
// Registration Thread
//

class CRegisterThread : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRegisterThread(void);

		// Destructor
		~CRegisterThread(void);

		// Management
		void Bind(CWnd *pWnd);
		BOOL Terminate(DWORD Timeout);

		// Attributes
		BOOL IsEmailValid(void) const;

		// Operations
		void Validate(CString Email);
		void Connect (void);
		void Send    (CString Post);

	protected:
		// Data Members
		CWnd *		m_pWnd;
		CEvent		m_WorkEvent;
		UINT		m_uCode;
		HINTERNET	m_hNet;
		CString		m_Data;
		BOOL		m_fValid;

		// Overridables
		BOOL OnInit(void);
		UINT OnExec(void);
		void OnTerm(void);

		// Implementation
		BOOL DoValidate(void);
		BOOL DoConnect(void);
		BOOL DoSend(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Registration Dialog Box
//

class CRegisterDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRegisterDialog(void);

		// Operations
		BOOL Check(CWnd &Wnd, BOOL fForce);

	public:
		// Data Members
		CRegisterThread m_Thread;
		UINT	        m_uState;
		UINT		m_uType;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnCommandSkip(UINT uID);
		BOOL OnThreadDone(UINT uID);
		BOOL OnThreadFail(UINT uID);

		// Notification Handlers
		void OnEditChange(UINT uID, CWnd &Ctrl);

		// Data Encoding
		CString EncodeString(UINT uID);
		CString EncodeCheck (UINT uID);

		// Implementation
		void SetDone(void);
		void SetError(PCTXT pText, UINT uID);
		void ShowStatus(PCTXT pText);
		void TxFrame(void);
		BOOL RxFrame(void);
		void Preload(void);
		void DoEnables(void);
		BOOL IsValidEmail(CString Email);
		void EnableEntry(BOOL fEnable);

		// Load Helpers
		void LoadData(void);
		BOOL LoadTextField(UINT ID, CRegKey &Reg, CString Name);
		BOOL LoadListField(UINT ID, CRegKey &Reg, CString Name);
		BOOL LoadTickField(UINT ID, CRegKey &Reg, CString Name);
		
		// Save Helpers
		void SaveData(void);
		void SaveTextField(UINT ID, CRegKey &Reg, CString Name);
		void SaveListField(UINT ID, CRegKey &Reg, CString Name);
		void SaveTickField(UINT ID, CRegKey &Reg, CString Name);

		// Country Access
		CString GetCountry(UINT ID);
		DWORD   GetCountryCode(void);
	};

// End of File

#endif
