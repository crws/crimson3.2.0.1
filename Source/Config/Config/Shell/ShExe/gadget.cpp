
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Sub-Classed Tool Tip Control
//

class CWarnToolTip : public CToolTip
{
public:
	// Constructor
	CWarnToolTip(CWarningGadget *pGadget);

	// Static Data
	static UINT m_timerClose;

protected:
	// Data Members
	CWarningGadget * m_pGadget;

	// Window Procedure
	LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);
};

//////////////////////////////////////////////////////////////////////////
//
// Warnings Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CWarningGadget, CTextGadget);

// Static Data

BOOL CWarningGadget::m_fShow = FALSE;

// Constructor

CWarningGadget::CWarningGadget(CCrimsonWnd *pWnd, UINT uID, CString Text) : CTextGadget(uID, Text, textRed)
{
	m_pMainWnd = pWnd;

	m_fWarn    = TRUE;

	m_pToolTip = New CWarnToolTip(this);
}

// Destructor

CWarningGadget::~CWarningGadget(void)
{
	m_Menu.FreeOwnerDraw();
}

// Operations

BOOL CWarningGadget::HideToolTip(void)
{
	if( m_pToolTip->IsWindowVisible() ) {

		HWND hWnd = m_pWnd->GetHandle();

		CToolInfo Info(hWnd, 100);

		m_pToolTip->TrackActivate(FALSE, Info);

		m_fShow = FALSE;

		return TRUE;
	}

	return FALSE;
}

// Overridables

void CWarningGadget::OnCreate(CDC &DC)
{
	CTextGadget::OnCreate(DC);

	DWORD dwStyle = TTS_NOFADE | TTS_NOANIMATE | TTS_NOPREFIX | TTS_ALWAYSTIP | TTS_BALLOON;

	m_pToolTip->Create(m_pWnd->GetHandle(), 0, dwStyle);

	m_pToolTip->SetMaxTipWidth(250);

	m_pToolTip->SetTitle(1, m_Title);

	m_pToolTip->SetTipBkColor(CColor(255, 192, 192));

	CToolInfo Info(m_pWnd->GetHandle(), 100, m_pWnd->GetClientRect(), m_Prompt, TTF_TRACK);

	m_pToolTip->AddTool(Info);
}

void CWarningGadget::OnPaint(CDC &DC)
{
	if( m_Rect.GetWidth() ) {

		if( TestFlag(MF_HILITE) ) {

			CRect Rect = m_Rect;

			Rect.top    += 1;

			Rect.bottom -= 2;

			if( TestFlag(MF_DISABLED) ) {

				DC.FrameRect(Rect--, afxBrush(3dDkShadow));
			}
			else {
				if( TestFlag(MF_PRESSED) ) {

					DC.FrameRect(Rect--, afxBrush(3dDkShadow));

					DC.GradVert(Rect--, afxColor(Orange1), afxColor(Orange2));
				}
				else {
					DC.FrameRect(Rect--, afxBrush(3dDkShadow));

					DC.GradVert(Rect--, afxColor(Orange3), afxColor(Orange2));
				}
			}
		}
	}

	CTextGadget::OnPaint(DC);
}

void CWarningGadget::OnSetPosition(void)
{
	HideToolTip();

	CTextGadget::OnSetPosition();
}

BOOL CWarningGadget::OnMouseMenu(CPoint const &Pos)
{
	CPoint Org = Pos;

	m_pWnd->ClientToScreen(Org);

	((CMainWnd *) afxMainWnd)->LockPrompt(TRUE);

	m_Menu.GetSubMenu(0).TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
					    Org,
					    afxMainWnd->GetHandle()
	);

	((CMainWnd *) afxMainWnd)->LockPrompt(FALSE);

	return TRUE;
}

BOOL CWarningGadget::OnMouseDown(CPoint const &Pos)
{
	HideToolTip();

	if( !TestFlag(MF_DISABLED) ) {

		AdjustFlag(MF_PRESSED, TRUE);

		return TRUE;
	}

	return FALSE;
}

void CWarningGadget::OnMouseUp(CPoint const &Pos, BOOL fHit)
{
	if( fHit ) {

		CDatabase   *pDbase  = m_pMainWnd->GetDatabase();

		CSystemItem *pSystem = pDbase->GetSystemItem();

		if( HasError(pSystem) ) {

			ShowError(pSystem);
		}
		else {
			ClearError(pSystem);

			AdjustFlag(MF_DISABLED, FALSE);
		}

		AdjustFlag(MF_PRESSED, FALSE);
	}
}

void CWarningGadget::OnMouseMove(CPoint const &Pos, BOOL fHit)
{
	AdjustFlag(MF_PRESSED, fHit);
}

void CWarningGadget::OnSetFlags(void)
{
	if( !TestFlag(MF_DISABLED) ) {

		CDatabase   *pDbase  = m_pMainWnd->GetDatabase();

		CSystemItem *pSystem = pDbase->GetSystemItem();

		if( !HasError(pSystem) ) {

			m_uFlags |= MF_DISABLED;
		}
		else {
			if( !m_fShow ) {

				if( m_fWarn ) {

					HWND   hWnd = m_pWnd->GetHandle();

					CPoint Pos  = m_Rect.GetCenter();

					m_pWnd->ClientToScreen(Pos);

					CToolInfo Info(hWnd, 100);

					m_pToolTip->TrackPosition(Pos);

					m_pToolTip->TrackActivate(TRUE, Info);

					m_pToolTip->SetTimer(CWarnToolTip::m_timerClose, 15000);

					m_fShow = TRUE;
				}

				m_fWarn = FALSE;
			}
		}
	}
	else {
		HideToolTip();

		m_fWarn = TRUE;
	}

	CTextGadget::OnSetFlags();
}

//////////////////////////////////////////////////////////////////////////
//
// Database Errors Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CErrorsGadget, CWarningGadget);

// Constructor

CErrorsGadget::CErrorsGadget(CCrimsonWnd *pWnd, UINT uID) : CWarningGadget(pWnd, uID, CString(IDS_ERRORS))
{
	m_Title  = CString(IDS_ERRORS_IN);

	m_Prompt = CString(IDS_ERRORS_TEXT_1) + CString(IDS_ERRORS_TEXT_2) + CString(IDS_ERRORS_TEXT_3) + CString(IDS_ERRORS_TEXT_4);

	m_Remain = CString(IDS_ERRORS_STILL);

	m_Menu.Create(L"ErrorsMenu");

	m_Menu.MakeOwnerDraw(FALSE);
}

// Implementation

BOOL CErrorsGadget::HasError(CSystemItem *pSystem)
{
	return pSystem->HasBroken();
}

void CErrorsGadget::ClearError(CSystemItem *pSystem)
{
	CDatabase *pDbase = m_pMainWnd->GetDatabase();

	pDbase->SetRecomp(FALSE);
}

void CErrorsGadget::ShowError(CSystemItem *pSystem)
{
	afxMainWnd->PostMessage(WM_COMMAND, IDM_WARN_SHOW_ERROR);
}

//////////////////////////////////////////////////////////////////////////
//
// Circular References Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CCircleGadget, CWarningGadget);

// Constructor

CCircleGadget::CCircleGadget(CCrimsonWnd *pWnd, UINT uID) : CWarningGadget(pWnd, uID, CString(IDS_CIRCULAR_1))
{
	m_Title  = CString(IDS_CIRCULAR_2);

	m_Prompt = CString(IDS_ERRORS_TEXT_5) + CString(IDS_ERRORS_TEXT_2) + CString(IDS_ERRORS_TEXT_3) + CString(IDS_ERRORS_TEXT_4);

	m_Remain = CString(IDS_CIRCULAR_3);

	m_Menu.Create(L"CircleMenu");

	m_Menu.MakeOwnerDraw(FALSE);
}

// Implementation

BOOL CCircleGadget::HasError(CSystemItem *pSystem)
{
	return pSystem->HasCircular();
}

void CCircleGadget::ClearError(CSystemItem *pSystem)
{
	CDatabase *pDbase = m_pMainWnd->GetDatabase();

	pDbase->SetCircle(FALSE);
}

void CCircleGadget::ShowError(CSystemItem *pSystem)
{
	afxMainWnd->PostMessage(WM_COMMAND, IDM_WARN_SHOW_CIRCLE);
}

//////////////////////////////////////////////////////////////////////////
//
// Hardware Changes Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CHardwareGadget, CWarningGadget);

// Constructor

CHardwareGadget::CHardwareGadget(CCrimsonWnd *pWnd, UINT uID) : CWarningGadget(pWnd, uID, IDS("Hardware"))
{
	m_Title   = IDS("Hardware Changes");

	m_Prompt  = IDS("You have made changes to your hardware configuration. ");

	m_Prompt += IDS("Commit these changes to update the rest of your database.");
}

// Implementation

BOOL CHardwareGadget::HasError(CSystemItem *pSystem)
{
	return pSystem->HasHardware();
}

void CHardwareGadget::ClearError(CSystemItem *pSystem)
{
}

void CHardwareGadget::ShowError(CSystemItem *pSystem)
{
	pSystem->ShowHardware();
}

//////////////////////////////////////////////////////////////////////////
//
// Sub-Classed Tool Tip Control
//

// Static Data

UINT CWarnToolTip::m_timerClose = CWnd::AllocTimerID();

// Constructor

CWarnToolTip::CWarnToolTip(CWarningGadget *pGadget)
{
	m_pGadget = pGadget;
}

// Window Procedure

LRESULT CWarnToolTip::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_SHOWWINDOW ) {

		if( !wParam ) {

			KillTimer(m_timerClose);
		}
	}

	if( uMessage == WM_TIMER ) {

		if( wParam == m_timerClose ) {

			KillTimer(m_timerClose);

			m_pGadget->HideToolTip();
		}
	}

	if( uMessage == WM_LBUTTONDOWN ) {

		m_pGadget->HideToolTip();
	}

	return CToolTip::WndProc(uMessage, wParam, lParam);
}

// End of File
