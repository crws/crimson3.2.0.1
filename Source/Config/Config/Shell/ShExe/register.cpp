
#include "intern.hpp"

#include "register.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Web Registration Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Registration Dialog Box
//

// Runtime Class

AfxImplementRuntimeClass(CRegisterDialog, CStdDialog);

// Registration Types

enum
{
	typeInitial,
	typeUpgrade,
	typeAmend
	};

// State Machine

enum
{
	stateCollect,
	stateValidate,
	stateConnect,
	stateSend,
	stateDone
	};

// Constructors

CRegisterDialog::CRegisterDialog(void)
{
	m_fAutoClose = FALSE;

	m_uType      = typeInitial;

	m_uState     = stateCollect;

	SetName(L"RegisterDlg");
	}

// Operations

BOOL CRegisterDialog::Check(CWnd &Wnd, BOOL fForce)
{
	CRegKey Reg1 = afxModule->GetUserRegKey();

	CRegKey Reg2 = CRegKey(HKEY_LOCAL_MACHINE, 64u);

	CRegKey Reg3 = CRegKey(HKEY_LOCAL_MACHINE);

	Reg1.MoveTo(L"Registration");

	Reg2.MoveTo(L"Software");
	
	Reg2.MoveTo(L"Microsoft");
	
	Reg2.MoveTo(L"Windows NT");
	
	Reg2.MoveTo(L"CurrentVersion");

	Reg3.MoveTo(L"Software");
	
	Reg3.MoveTo(L"Microsoft");
	
	Reg3.MoveTo(L"Windows NT");
	
	Reg3.MoveTo(L"CurrentVersion");

	CString Product1 = Reg2.GetValue(L"ProductID", L"");

	CString Product2 = Reg3.GetValue(L"ProductID", L"");

	if( Product1.IsEmpty() ) Product1 = L"EMPTY";

	if( Product2.IsEmpty() ) Product2 = L"EMPTY";

	DWORD CRC1     = CRC32(PCBYTE(PCTXT(Product1)), Product1.GetLength());

	DWORD CRC2     = CRC32(PCBYTE(PCTXT(Product2)), Product2.GetLength());

	DWORD Complete = Reg1.GetValue(L"Complete",  UINT(0));

	DWORD Build    = Reg1.GetValue(L"Build",     UINT(0));

	DWORD Count    = Reg1.GetValue(L"Count",     UINT(0));

	if( CRC1 == Complete || CRC2 == Complete ) {

		if( fForce ) {

			m_uType = typeAmend;

			if( Execute(Wnd) ) {

				Reg1.SetValue(L"Count", DWORD(0));

				return TRUE;
				}
			}
		else {
			if( C3_BUILD > Build ) {

				if( C3_BETA || Count % 8 == 0 ) {

					CString Text = IDS_REG_UPGRADE;

					if( YesNo(Text) == IDYES ) {

						m_uType = typeUpgrade;

						if( Execute(Wnd) ) {

							Reg1.SetValue(L"Count", DWORD(0));

							if( C3OemFeature(L"BuildNotes", TRUE) ) {

								CString Prompt = CString(IDS_DO_YOU_WANT_TO_2);

								if( YesNo(Prompt) == IDYES ) {

									afxMainWnd->PostMessage(WM_COMMAND, IDM_HELP_NOTES);
									}
								}

							return TRUE;
							}
						}
					}

				Reg1.SetValue(L"Count", Count+1);

				return FALSE;
				}
			}

		return TRUE;
		}
	else {
		if( C3_BETA || fForce || Count < 8 || Count % 4 == 0 ) {

			if( Execute(Wnd) ) {

				Reg1.SetValue(L"Count", DWORD(0));

				return TRUE;
				}

			Reg1.SetValue(L"Status", DWORD(0));
			}

		Reg1.SetValue(L"Count", Count+1);

		return FALSE;
		}
	}

// Message Map

AfxMessageMap(CRegisterDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOkay)
	AfxDispatchCommand(IDCANCEL, OnCommandSkip)
 	AfxDispatchCommand(IDDONE,   OnThreadDone )
 	AfxDispatchCommand(IDFAIL,   OnThreadFail )

	AfxDispatchNotify(200, EN_CHANGE, OnEditChange)
	AfxDispatchNotify(201, EN_CHANGE, OnEditChange)
	AfxDispatchNotify(202, EN_CHANGE, OnEditChange)

	AfxMessageEnd(CRegisterDialog)
	};

// Message Handlers

BOOL CRegisterDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_Thread.Bind(this);

	m_Thread.Create();

	Preload();

	if( m_uType != typeInitial ) {

		LoadData();

		if( m_uType == typeUpgrade ) {

			PostMessage(WM_COMMAND, IDOK, 0);
			}

		if( m_uType == typeAmend ) {

			GetDlgItem(IDCANCEL).SetWindowText(CString(IDS_CANCEL));

			SetWindowText(CString(IDS_AMEND_YOUR));
			}
		}

	DoEnables();

	return TRUE;
	}

// Command Handlers

BOOL CRegisterDialog::OnCommandOkay(UINT uID)
{
	if( m_uState == stateCollect ) {

		EnableEntry(FALSE);

		m_uState = stateValidate;

		TxFrame();
		}

	return TRUE;
	}

BOOL CRegisterDialog::OnCommandSkip(UINT uID)
{
	CString Check;

	UINT    uWait;

	if( m_uState == stateCollect ) {

		if( m_uType == typeAmend ) {

			m_Thread.Terminate(INFINITE);

			EndDialog(FALSE);

			return TRUE;
			}

		Check = CString(IDS_REG_SKIP);

		uWait = INFINITE;
		}
	else {
		Check = CString(IDS_REG_ABORT);

		uWait = 5000;
		}

	m_Thread.Suspend();

	if( NoYes(Check) == IDYES ) {

		m_Thread.Resume();

		m_Thread.Terminate(uWait);

		EndDialog(FALSE);

		return TRUE;
		}

	m_Thread.Resume();

	return TRUE;
	}

BOOL CRegisterDialog::OnThreadDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
		}

	return TRUE;
	}

BOOL CRegisterDialog::OnThreadFail(UINT uID)
{
	SetError(CString(IDS_REG_ERROR), IDOK);

	return TRUE;
	}

// Notification Handlers

void CRegisterDialog::OnEditChange(UINT uID, CWnd &Ctrl)
{
	DoEnables();

	GetDlgItem(100).SetWindowText(CString(IDS_REG_USERINFO));
	}

// Data Encoding

CString CRegisterDialog::EncodeString(UINT uID)
{
	CString Text = GetDlgItem(uID).GetWindowText();

	CString HTML;

	for( UINT i = 0; Text[i]; i++ ) {

		switch( Text[i] ) {

			case ' ':
				
				HTML += L"+";
				
				break;

			case '+':
			case '&':
			case '=':
			case ';':
			case '/':
			case '?':
			case '@':

				HTML += CPrintf(L"%%%2.2X", Text[i]);

				break;

			default:
				if( (Text[i] & 127) <= 32 || (Text[i] & 127) == 127 ) {

					HTML += CPrintf(L"%%%2.2X", Text[i]);

					break;
					}

				HTML += Text[i];

				break;
			}
		}

	return HTML;
	}

CString CRegisterDialog::EncodeCheck(UINT uID)
{
	CButton &Check = (CButton &) GetDlgItem(uID);

	return Check.IsChecked() ? L"1" : L"0";
	}

// Implementation

void CRegisterDialog::SetDone(void)
{
	SaveData();

	if( m_uType == typeInitial ) {

		Information(CString(IDS_THANK_YOU_FOR_2));
		}

	m_Thread.Terminate(INFINITE);

	EndDialog(TRUE);
	}

void CRegisterDialog::SetError(PCTXT pText, UINT uID)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_UPDATE_ERROR, pText));

	MessageBeep(MB_ICONEXCLAMATION);

	EnableEntry(TRUE);

	SetDlgFocus(uID);

	m_uState = stateCollect;
	}

void CRegisterDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
	}

void CRegisterDialog::TxFrame(void)
{
	if( m_uState == stateValidate ) {

		ShowStatus(CString(IDS_REG_EMAIL));

		m_Thread.Validate(GetDlgItem(201).GetWindowText());

		return;
		}

	if( m_uState == stateConnect ) {

		ShowStatus(CString(IDS_REG_CONNECT));

		m_Thread.Connect();

		return;
		}

	if( m_uState == stateSend ) {

		ShowStatus(CString(IDS_REG_SENDING));

		CString Post;

		Post += L"Product="     + CString(L"C3.2");
		Post += L"&Build="      + CPrintf(L"%u", C3_BUILD);
		Post += L"&Upgrade="    + CPrintf(L"%u", m_uType );
		Post += L"&Name="       + EncodeString(200);
		Post += L"&Email="      + EncodeString(201);
		Post += L"&Company="    + EncodeString(202);
		Post += L"&Street="     + EncodeString(203);
		Post += L"&City="       + EncodeString(204);
		Post += L"&State="      + EncodeString(205);
		Post += L"&ZIP="        + EncodeString(206);
		Post += L"&Country="    + EncodeString(207);
		Post += L"&OSID="       + EncodeString(208);
		Post += L"&SendUpdate=" + EncodeCheck (400);
		Post += L"&SendInfo="   + EncodeCheck (401);

		m_Thread.Send(Post);

		return;
		}
	}

BOOL CRegisterDialog::RxFrame(void)
{
	if( m_uState == stateValidate ) {

		if( m_Thread.IsEmailValid() ) {

			m_uState = stateConnect;

			return TRUE;
			}

		SetError(CString(IDS_REG_EMAIL_ERROR), 202);

		return FALSE;
		}

	if( m_uState == stateConnect ) {

		m_uState = stateSend;

		return TRUE;
		}

	if( m_uState == stateSend ) {

		SetDone();

		return TRUE;
		}

	SetError(CString(IDS_REG_ERROR), IDOK);

	return FALSE;
	}

void CRegisterDialog::Preload(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(207);

	for( UINT id = 0; id < 999; id++ ) {

		CString Name = GetCountry(id);

		if( !Name.IsEmpty() && Name.GetLength() < 32 ) {

			Combo.AddString(Name, DWORD(id));
			}
		}

	Combo.SelectData(GetCountryCode());

	((CButton &) GetDlgItem(400)).SetCheck(TRUE);
	
	((CButton &) GetDlgItem(401)).SetCheck(TRUE);

	CRegKey Reg(HKEY_LOCAL_MACHINE, 64u);

	Reg.MoveTo(L"Software");
	
	Reg.MoveTo(L"Microsoft");
	
	Reg.MoveTo(L"Windows NT");
	
	Reg.MoveTo(L"CurrentVersion");

	CString Product = Reg.GetValue(L"ProductID", L"EMPTY");

	CString Owner   = Reg.GetValue(L"RegisteredOwner",        L"");

	CString Company = Reg.GetValue(L"RegisteredOrganization", L"");

	if( Owner == L"Microsoft" ) {

		GetDlgItem(200).SetWindowText(L"");

		GetDlgItem(202).SetWindowText(L"");
		}
	else {
		GetDlgItem(200).SetWindowText(Owner);

		GetDlgItem(202).SetWindowText(Company);
		}

	GetDlgItem(208).SetWindowText(Product.IsEmpty() ? L"EMPTY" : Product);

	GetDlgItem(400).SetWindowText(CString(IDS_REG_UPDATES));

	GetDlgItem(401).SetWindowText(CString(IDS_REG_PRODUCTS));

	SetWindowText(CString(IDS_REG_TITLE));
	}

void CRegisterDialog::DoEnables(void)
{
	BOOL fEnable = TRUE;

	fEnable = fEnable && GetDlgItem(200).GetWindowTextLength() > 0;

	fEnable = fEnable && GetDlgItem(201).GetWindowTextLength() > 0;

	fEnable = fEnable && GetDlgItem(202).GetWindowTextLength() > 0;

	fEnable = fEnable && IsValidEmail(GetDlgItem(201).GetWindowText());

	GetDlgItem(IDOK).EnableWindow(fEnable);
	}

BOOL CRegisterDialog::IsValidEmail(CString Email)
{
	UINT uAt = Email.Find('@');

	if( uAt < NOTHING ) {

		CString Name   = Email.Left(uAt);

		CString Domain = Email.Mid (uAt+1);

		if( !Name.IsEmpty() && !Domain.IsEmpty() ) {

			UINT uDot = Domain.FindRev('.');

			if( uDot < NOTHING ) {

				CString Top  = Domain.Mid (uDot+1);

				CString Rest = Domain.Left(uDot);

				if( !Rest.IsEmpty() && !Top.IsEmpty() ) {

					if( Top.GetLength() < 2 ) {

						return FALSE;
						}

					if( Top == L"co" || Top == L"ne" ) {

						return FALSE;
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CRegisterDialog::EnableEntry(BOOL fEnable)
{
	GetDlgItem(IDOK).EnableWindow(fEnable);
	
	GetDlgItem(200).EnableWindow(fEnable);
	GetDlgItem(201).EnableWindow(fEnable);
	GetDlgItem(202).EnableWindow(fEnable);
	GetDlgItem(203).EnableWindow(fEnable);
	GetDlgItem(204).EnableWindow(fEnable);
	GetDlgItem(205).EnableWindow(fEnable);
	GetDlgItem(206).EnableWindow(fEnable);
	GetDlgItem(207).EnableWindow(fEnable);
	GetDlgItem(208).EnableWindow(fEnable);
	GetDlgItem(400).EnableWindow(fEnable);
	GetDlgItem(401).EnableWindow(fEnable);
	}

// Load Helpers

void CRegisterDialog::LoadData(void)
{
	CRegKey Reg = afxModule->GetUserRegKey();

	Reg.MoveTo(L"Registration");

	LoadTextField(200, Reg, L"Name");
	LoadTextField(201, Reg, L"Email");
	LoadTextField(202, Reg, L"Company");
	LoadTextField(203, Reg, L"Street");
	LoadTextField(204, Reg, L"City");
	LoadTextField(205, Reg, L"State");
	LoadTextField(206, Reg, L"ZIP");
	LoadListField(207, Reg, L"Country");
	LoadTickField(400, Reg, L"SendUpdate");
	LoadTickField(401, Reg, L"SendInfo");
	}

BOOL CRegisterDialog::LoadTextField(UINT ID, CRegKey &Reg, CString Name)
{
	CString Data = Reg.GetValue(Name, L"");

	if( !Data.IsEmpty() ) {

		CWnd &Wnd = (CWnd &) GetDlgItem(ID);

		Wnd.SetWindowText(Data);

		return TRUE;
		}

	return FALSE;
	}

BOOL CRegisterDialog::LoadListField(UINT ID, CRegKey &Reg, CString Name)
{
	CString Data = Reg.GetValue(Name, L"");

	if( !Data.IsEmpty() ) {

		CComboBox &Wnd = (CComboBox &) GetDlgItem(ID);

		Wnd.SelectString(NOTHING, Data);

		return TRUE;
		}

	return FALSE;
	}

BOOL CRegisterDialog::LoadTickField(UINT ID, CRegKey &Reg, CString Name)
{
	DWORD dwData = Reg.GetValue(Name, UINT(0));

	if( dwData ) {

		CButton &Wnd = (CButton &) GetDlgItem(ID);

		Wnd.SetCheck(TRUE);

		return TRUE;
		}

	return FALSE;
	}

// Save Helpers

void CRegisterDialog::SaveData(void)
{
	CString Product = GetDlgItem(208).GetWindowText();

	DWORD   Check   = CRC32(PCBYTE(PCTXT(Product)), Product.GetLength());

	CRegKey Reg = afxModule->GetUserRegKey();

	Reg.MoveTo(L"Registration");

	SaveTextField(200, Reg, L"Name");
	SaveTextField(201, Reg, L"Email");
	SaveTextField(202, Reg, L"Company");
	SaveTextField(203, Reg, L"Street");
	SaveTextField(204, Reg, L"City");
	SaveTextField(205, Reg, L"State");
	SaveTextField(206, Reg, L"ZIP");
	SaveListField(207, Reg, L"Country");
	SaveTextField(208, Reg, L"Product");
	SaveTickField(400, Reg, L"SendUpdate");
	SaveTickField(401, Reg, L"SendInfo");

	Reg.SetValue(L"Complete", DWORD(Check));
	Reg.SetValue(L"Status",   DWORD(1));
	Reg.SetValue(L"Build",    DWORD(C3_BUILD));
	}

void CRegisterDialog::SaveTextField(UINT ID, CRegKey &Reg, CString Name)
{
	CWnd &Wnd = (CWnd &) GetDlgItem(ID);

	Reg.SetValue(Name, Wnd.GetWindowText());
	}

void CRegisterDialog::SaveListField(UINT ID, CRegKey &Reg, CString Name)
{
	CWnd &Wnd = (CWnd &) GetDlgItem(ID);

	Reg.SetValue(Name, Wnd.GetWindowText());
	}

void CRegisterDialog::SaveTickField(UINT ID, CRegKey &Reg, CString Name)
{
	CButton &Wnd = (CButton &) GetDlgItem(ID);

	Reg.SetValue(Name, DWORD(Wnd.IsChecked() ? TRUE : FALSE));
	}

// Country Access

CString CRegisterDialog::GetCountry(UINT ID)
{
	typedef INT (WINAPI *PGETGEO)(GEOID,GEOTYPE,LPTSTR,int,LANGID);

	static  PGETGEO pGetGeo = NULL;

	static  BOOL    fLinked = FALSE;

	if( !fLinked ) {
	
		HANDLE hKernel = LoadLibrary(L"kernel32.dll");

		pGetGeo = (PGETGEO) GetProcAddress(hKernel, "GetGeoInfoW");

		fLinked = TRUE;

		FreeLibrary(hKernel);
		}

	if( pGetGeo ) {

		TCHAR sText[256] = { 0 };

		(*pGetGeo)( ID,
			    GEO_FRIENDLYNAME,
			    sText,
			    sizeof(sText),
			    NULL
			    );

		wstrupr(sText);

		return sText;
		}

	if( ID == 244 ) {

		return L"UNITED STATES";
		}

	return L"";
	}

DWORD CRegisterDialog::GetCountryCode(void)
{
	typedef GEOID (WINAPI *PGETCODE)(GEOCLASS);

	static  PGETCODE pGetCode = NULL;

	static  BOOL     fLinked  = FALSE;

	if( !fLinked ) {
	
		HANDLE hKernel = LoadLibrary(L"kernel32.dll");

		pGetCode = (PGETCODE) GetProcAddress(hKernel, "GetUserGeoID");

		fLinked  = TRUE;

		FreeLibrary(hKernel);
		}

	if( pGetCode ) {

		return DWORD((*pGetCode)(GEOCLASS_NATION));
		}

	return 244;
	}

//////////////////////////////////////////////////////////////////////////
//
// Registration Thread
//

// Runtime Class

AfxImplementRuntimeClass(CRegisterThread, CRawThread);

// Constructor

CRegisterThread::CRegisterThread(void) : m_WorkEvent(FALSE)
{
	m_pWnd  = NULL;

	m_hNet  = NULL;
	}

// Destructor

CRegisterThread::~CRegisterThread(void)
{
	if( m_hNet ) {

		InternetCloseHandle(m_hNet);
		}
	}

// Management

void CRegisterThread::Bind(CWnd *pWnd)
{
	m_pWnd = pWnd;
	}

BOOL CRegisterThread::Terminate(DWORD Timeout)
{
	return CRawThread::Terminate(Timeout);
	}

// Attributes

BOOL CRegisterThread::IsEmailValid(void) const
{
	return m_fValid;
	}

// Operations

void CRegisterThread::Validate(CString Email)
{
	m_Data = Email;

	m_uCode = 1;

	m_WorkEvent.Set();
	}

void CRegisterThread::Connect(void)
{
	m_uCode = 2;

	m_WorkEvent.Set();
	}

void CRegisterThread::Send(CString Post)
{
	m_Data  = Post;

	m_uCode = 3;

	m_WorkEvent.Set();
	}

// Overridables

BOOL CRegisterThread::OnInit(void)
{
	return TRUE;
	}

UINT CRegisterThread::OnExec(void)
{
	CWaitableList List(m_WorkEvent, m_TermEvent);

	for(;;) {

		if( List.WaitForAnyObject(INFINITE) == waitSignal ) {

			if( List.GetObjectIndex() == 0 ) {

				BOOL fOkay = FALSE;

				UINT uTime = GetTickCount();

				switch( m_uCode ) {

					case 1:
						fOkay = DoValidate();
						break;
					
					case 2:
						fOkay = DoConnect();
						break;
					
					case 3:
						fOkay = DoSend();
						break;
					}

				UINT uGone = GetTickCount() - uTime;

				if( uGone < 500 ) {
					
					Sleep(500 - uGone);
					}

				m_pWnd->PostMessage(WM_COMMAND, fOkay ? IDDONE : IDFAIL);

				continue;

				}
			break;
			}
		}

	return 0;
	}

void CRegisterThread::OnTerm(void)
{
	}

// Implementation

BOOL CRegisterThread::DoValidate(void)
{
	CString Domain  = m_Data.Mid(m_Data.Find('@')+1);

	WORD	wType[] = { DNS_TYPE_MX,
			    DNS_TYPE_A,
			    DNS_TYPE_CNAME,
			    DNS_TYPE_SOA
			    };

	for( UINT n = 0; n < elements(wType); n++ ) {

		PDNS_RECORD pRecord = NULL;

		#pragma warning(suppress: 6001)

		DnsQuery( Domain, 
			  wType[n],
			  DNS_QUERY_STANDARD,
			  NULL,
			  &pRecord,
			  NULL
			  );

		if( pRecord ) {

			switch( wType[n] ) {

				case DNS_TYPE_MX:

					if( pRecord->Data.MX.pNameExchange ) {

						DnsRecordListFree(pRecord, DnsFreeRecordList);

						m_fValid = TRUE;

						return TRUE;				
						}
					break;

				case DNS_TYPE_A:

					if( pRecord->Data.A.IpAddress ) {

						DnsRecordListFree(pRecord, DnsFreeRecordList);

						m_fValid = TRUE;

						return TRUE;				
						}
					break;

				case DNS_TYPE_CNAME:

					if( pRecord->Data.CNAME.pNameHost ) {

						DnsRecordListFree(pRecord, DnsFreeRecordList);

						m_fValid = TRUE;

						return TRUE;				
						}
					break;

				case DNS_TYPE_SOA:

					if( pRecord->Data.SOA.pNamePrimaryServer ) {

						DnsRecordListFree(pRecord, DnsFreeRecordList);

						m_fValid = TRUE;

						return TRUE;
						}
					break;
				}

			DnsRecordListFree(pRecord, DnsFreeRecordList);
			}
		}

	m_fValid = FALSE;

	return TRUE;
	}

BOOL CRegisterThread::DoConnect(void)
{
	m_hNet = InternetOpen(	CString(IDS_UPDATE_AGENT),
				INTERNET_OPEN_TYPE_PRECONFIG,
				NULL,
				NULL,
				0
				);

	return m_hNet ? TRUE : FALSE;
	}

BOOL CRegisterThread::DoSend(void)
{
	CString RegPort = C3OemOption(L"RegPort", L"80");

	INTERNET_PORT iRegPort = (INTERNET_PORT)watoi(RegPort);
	
	HINTERNET hCon = InternetConnect( m_hNet,
					  CString(IDS_REG_SERVER),
					  iRegPort,
					  L"",
					  L"",
					  INTERNET_SERVICE_HTTP,
					  0,
					  0
					  );

	if( hCon ) {

		HINTERNET hReq = HttpOpenRequest( hCon,
						  L"POST",
						  CString(IDS_REG_PATH),
						  NULL,
						  NULL,
						  NULL,
						  INTERNET_FLAG_DONT_CACHE,
						  0
						  );

		if( hReq ) {

			PTXT    h = L"Content-Type: application/x-www-form-urlencoded";

			CString p = L"\r\n" + m_Data;

			UINT    n = p.GetLength()+1;

			PBYTE   w = New BYTE [ n ];

			for( UINT i = 0; i < n; i++ ) {

				w[i] = BYTE(PCTXT(p)[i]);
				}

			if( HttpSendRequest(hReq, h, wstrlen(h), w, n) ) {

				delete [] w;

				InternetCloseHandle(hReq);

				InternetCloseHandle(hCon);

				return TRUE;
				}

			delete [] w;

			InternetCloseHandle(hReq);
			}

		InternetCloseHandle(hCon);
		}

	return FALSE;
	}

// End of File
