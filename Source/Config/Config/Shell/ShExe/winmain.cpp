
#include "intern.hpp"

#include <shlink.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Forced Static Binding
//

#if defined(_DEBUG)

#include <g3prims.hpp>

#include <g3graph.hpp>

#include <g3mc.hpp>

#include <g3straton.hpp>

#endif

//////////////////////////////////////////////////////////////////////////
//
// Forced Static Binding
//

static void StaticBind(void)
{
	#if defined(_DEBUG)

	AfxTouch(AfxRuntimeClass(CUISystem));

	AfxTouch(AfxRuntimeClass(CPageEditorWnd));

//	AfxTouch(AfxRuntimeClass(CCommsPortRack));

//	AfxTouch(&CStratonDatabase::FindInstance);

	#endif
	}

//////////////////////////////////////////////////////////////////////////
//
// Category Filter
//

static void CheckCategoryFilter(void)
{
	if( C3OemFeature(L"CatFilter", FALSE) ) {

		CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

		Reg.MoveTo(L"Category");

		if( !Reg.GetValue(L"Manual", UINT(FALSE)) ) {

			Reg.SetValue(L"Manual", DWORD(FALSE));

			Reg.SetValue(L"CommsOnly", DWORD(TRUE));
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Test Harness
//

int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	StaticBind();

	afxModule = New CModule(modApplication);

	if( !afxModule->InitApp(hThis, GetCommandLine(), nCmdShow) ) {

		AfxTrace(L"ERROR: Failed to initialize application\n");

		delete afxModule;

		afxModule = NULL;

		return 1;
		}
	else {
		#if 0 && !defined(_DEBUG)

		if( !C3OemCheckSigs() ) {

			int Result = MessageBox( NULL,
				    L"This is not a genuine copy of the Crimson 3.1 software.",
				    L"Validation Failure",
				    MB_YESNO
				    );

			C3OemTerm();

			afxModule->Terminate();

			delete afxModule;

			afxModule = NULL;

			return 0;
			}

		#endif

		CString Name = CString(IDS_REG_COMPANY);

		UINT    uPos = Name.Find('/');

		afxModule->SetRegCompany(Name.Left(uPos));

		afxModule->SetRegName   (CString(IDS_REG_NAME));

		afxModule->SetRegVersion(CString(IDS_REG_VERSION));

		SetCurrentDirectory(afxModule->GetFilename().GetDirectory());

		CheckCategoryFilter();

		Link_Init();

		(New CCrimsonApp)->Execute();

		Link_Term();

		C3OemTerm();

		afxModule->Terminate();
		}

	delete afxModule;

	afxModule = NULL;

	TerminateProcess(GetCurrentProcess(), 0);

	return 0;
	}

// End of File
