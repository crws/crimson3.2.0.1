
#include "intern.hpp"

#include "gdiplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Web Registration Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Support Dialog Box
//

// Runtime Class

AfxImplementRuntimeClass(CSupportDialog, CStdDialog);
		
// Constructors

CSupportDialog::CSupportDialog(BOOL fOem)
{
	m_pToolTip = New CToolTip;
	
	SetName(fOem ? L"SupportOEMDlg" : L"SupportDlg");
	}

// Message Map

AfxMessageMap(CSupportDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_MOVE)

	AfxDispatchCommand(1001, OnWebsite)
	AfxDispatchCommand(1003, OnWebsite)
	AfxDispatchCommand(2001, OnEmail)
	AfxDispatchCommand(3001, OnEmail)
	AfxDispatchCommand(4001, OnEmail)
	AfxDispatchCommand(5001, OnEmail)

	AfxDispatchCommand(IDOK, OnCommandOK)
	
	AfxMessageEnd(CSupportDialog)
	};

// Message Handlers

BOOL CSupportDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	FindRects();

	CClientDC DC(ThisObject);

	m_Font.Create(DC, L"Arial",  24, UINT(1));

	GetDlgItem(1002).SetFont(m_Font);

	m_Text += CString(IDS_CRIMSON_CAN);
	
	m_Text += CString(IDS_QUESTIONS_VIA);
	
	m_Text += CString(IDS_F_KEY_TO_DISPLAY);

	m_Text += L"\n\n";

	m_Text += CString(IDS_IF_PRESSING_F_KEY);

	m_Text += CString(IDS_OR_IF_YOU_WOULD);

	m_Text += CString(IDS_ICON_AT_RIGHTHAND);

	m_Text += CString(IDS_OPTIONS_ON_HELP);

	CreateToolTip();

	CString Web = CString(IDS_SUPPORT_URL);

	GetDlgItem(1001).SetWindowTextW(Web);

	return FALSE;
	}

void CSupportDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	UINT    uFrom = 0;

	HGLOBAL hFrom = afxModule->LoadResource(L"F1Key", L"GIF", uFrom);

	HGLOBAL hData = GlobalAlloc(GHND, uFrom);

	AfxAssume(hData);

	PBYTE   pFrom = PBYTE(LockResource(hFrom));

	PBYTE   pData = PBYTE(GlobalLock(hData));

	memcpy(pData, pFrom, uFrom);

	UnlockResource(hFrom);

	FreeResource(hFrom);

	GlobalUnlock(hData);

	PaintImage(DC, hData);

	GlobalFree(hData);
	}

void CSupportDialog::OnMove(CPoint Pos)
{
	ShowToolTip();
	}

// Command Handlers

BOOL CSupportDialog::OnWebsite(UINT uID)
{
	CString File = GetDlgItem(uID).GetWindowText();
	
	ShellExecute( ThisObject, 
		      L"open", 
		      File,
		      NULL, 
		      NULL, 
		      SW_SHOWNORMAL
		      );

	return TRUE;
	}

BOOL CSupportDialog::OnEmail(UINT uID)
{
	CString Text = GetDlgItem(uID).GetWindowText();
	
	CString File = CPrintf(L"mailto:%s", Text);
	
	ShellExecute( ThisObject, 
		      L"open", 
		      File,
		      NULL, 
		      NULL, 
		      SW_SHOWNORMAL
		      );


	return TRUE;
	}

BOOL CSupportDialog::OnCommandOK(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

// Tool Tip

void CSupportDialog::CreateToolTip(void)
{
	CWnd    &Wnd = GetDlgItem(1000);

	CRect   Rect = Wnd.GetWindowRect();

	Rect.left   += 8;
	
	Rect.right  -= 12;
	
	DWORD dwStyle = TTS_NOFADE    |
			TTS_NOANIMATE |
			TTS_NOPREFIX  |
			TTS_ALWAYSTIP |
			TTS_BALLOON;

	m_pToolTip->Create(m_hWnd, 0, dwStyle);

	m_pToolTip->SetMaxTipWidth(Rect.cx() - 120);

	// TODO -- Suppress title to get more space for verbose languages?

	m_pToolTip->SetTitle(1, CString(IDS_BALLOON_HELP));

	CToolInfo Info(m_hWnd, 100, GetClientRect(), m_Text, TTF_TRACK);

	m_pToolTip->AddTool(Info);
	}

void CSupportDialog::ShowToolTip(void)
{
	CPoint Pos  = m_Rect.GetBottomRight();

	ClientToScreen(Pos);
	
	CToolInfo Info(m_hWnd, 100);

	m_pToolTip->TrackPosition(Pos);

	m_pToolTip->TrackActivate(TRUE, Info);
	}

// Implementation

void CSupportDialog::FindRects(void)
{
	CWnd  &Wnd = GetDlgItem(1000);

	CRect Rect = Wnd.GetWindowRect();

	ScreenToClient(Rect);

	Rect.left   += 8;

	Rect.top    += 20;
	
	Rect.right  -= 8;
	
	Rect.bottom -= 8 + 32;

	m_Rect        = Rect;

	m_Rect.right  = m_Rect.left + 60;

	m_Rect.bottom = m_Rect.top  + m_Rect.cx();
	}

void CSupportDialog::PaintImage(CDC &DC, HGLOBAL hData)
{
	IStream *pStream = NULL;

	CreateStreamOnHGlobal(hData, FALSE, &pStream);

	if( pStream ) {

		GpImage *pImage  = NULL;

		GdipLoadImageFromStream(pStream, &pImage);

		if( pImage ) {

			CRect       Rect   = GetClientRect();

			GpGraphics *pGraph = NULL;

			GdipCreateFromHDC(DC.GetHandle(), &pGraph);

			GdipDrawImageRectI( pGraph,
					    pImage,
					    m_Rect.left,
					    m_Rect.top,
					    m_Rect.cx(),
					    m_Rect.cy()
					    );

			GdipDeleteGraphics(pGraph);

			GdipDisposeImage(pImage);
			}

		pStream->Release();
		}
	}

// End of File
