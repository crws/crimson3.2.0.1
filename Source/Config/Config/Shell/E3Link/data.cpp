
#include "intern.hpp"

#include "file.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

// Constructor

CFileSysData::CFileSysData(PCSTR pName, UINT uSize)
{
	m_pName		= pName;

	m_uSize		= uSize;

	m_pData		= New BYTE [ m_uSize ];

	m_fValid	= FALSE;
	}

// Destructor

CFileSysData::~CFileSysData(void)
{
	delete [] m_pData;
	}

// IFileData

void CFileSysData::Release(void)
{
	delete this;
	}

PCSTR CFileSysData::GetFile(void)
{
	return m_pName;
	}

BOOL CFileSysData::IsValid(void)
{
	return m_fValid;
	}

void CFileSysData::SetValid(BOOL fValid)
{
	m_fValid = fValid;
	}

void CFileSysData::Create(UINT uSize)
{
	AfxAssert(m_pData == NULL);

	m_uSize = uSize;

	m_pData = New BYTE [ m_uSize ];
	}

PBYTE CFileSysData::GetDataBuffer(void)
{ 
	return m_pData;
	}

UINT CFileSysData::GetDataSize(void)
{
	return m_uSize;
	}

BYTE CFileSysData::GetByte(UINT &uPtr)
{
	return 0;
	}

WORD CFileSysData::GetWord(UINT &uPtr)
{
	return 0;
	}

DWORD CFileSysData::GetLong(UINT &uPtr)
{
	return 0;
	}

PCBYTE CFileSysData::GetData(UINT &uPtr, UINT uSize)
{
	return NULL;
	}

CString CFileSysData::GetText(UINT &uPtr, UINT uSize)
{
	return CString();
	}
		
void CFileSysData::PutByte(UINT &uPtr, BYTE Data)
{
	}

void CFileSysData::PutWord(UINT &uPtr, WORD Data)
{
	}

void CFileSysData::PutLong(UINT &uPtr, DWORD Data)
{
	}

void CFileSysData::PutData(UINT &uPtr, PCBYTE pData, UINT uSize)
{
	}

void CFileSysData::PutText(UINT &uPtr, CString pData, UINT uSize)
{
	}

void CFileSysData::Dump(void)
{
	}

// End of File
