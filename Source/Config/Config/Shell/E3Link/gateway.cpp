
#include "intern.hpp"

#include "imports\udr.h"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Gateway Wrapper
//

// Constructor

CGateway::CGateway(PCSTR pConfig, char cFormat)
{
	m_pConfig  = pConfig;

	m_Format   = cFormat;

	m_uTimeout = 2000;

	m_nSeq     = 0;
	}

// Destructor

CGateway::~CGateway(void)
{
	}

// Operations

BOOL CGateway::Reset(BOOL fHard)
{
	INT nResult = gateway_reset( PVOID(m_pConfig), 
				     m_uTimeout, 
				     m_nSeq++, 
				     m_Format,
				     fHard,
				     ANY_SERIAL,
				     NULL, 
				     NULL, 
				     0, 
				     NULL
				     );

	return nResult == UDR_SUCCESS;
	}

// End of File
