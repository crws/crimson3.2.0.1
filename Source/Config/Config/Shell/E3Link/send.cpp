
#include "intern.hpp"

#include "file.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Download Dialog Box
//

// Runtime Class

AfxImplementRuntimeClass(CLinkSendDialog, CStdDialog);
		
// Constructors

CLinkSendDialog::CLinkSendDialog(CDatabase *pDbase, UINT uSend)
{
	m_pDbase     = pDbase;

	m_pComms     = New CCommsThread(pDbase);

	m_uSend	     = uSend;

	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fVerify    = FALSE;

	m_fChanges   = FALSE;

	m_uState     = stateDone;

	m_pFile      = NULL;

	m_pFirm      = NULL;

	m_dwSize     = 0;

	m_pSystem    = (CEt3CommsSystem *) m_pDbase->GetSystemItem();

	SetName(L"LinkSendDlg");
	}

// Destructor

CLinkSendDialog::~CLinkSendDialog(void)
{
	delete m_pComms;
	}

// Message Map

AfxMessageMap(CLinkSendDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOkay)
	AfxDispatchCommand(IDINIT,   OnCommsInit  )
	AfxDispatchCommand(IDDONE,   OnCommsDone  )
	AfxDispatchCommand(IDFAIL,   OnCommsFail  )

	AfxMessageEnd(CLinkSendDialog)
	};

// Message Handlers

BOOL CLinkSendDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	switch( m_uSend ) {

		case sendSend:
		case sendUpdate:

			if( m_fTest ) {

				m_uFile = 0;
				
				m_uState = stateReadConfig;				
				}
			else {
				m_uState = stateCheckModel;
				}
			
			break;

		case sendVerify:

			SetWindowText(CString(IDS_VERIFY_DATABASE));

			m_fVerify = TRUE;

			m_uState = stateCheckModel;
			
			break;
		}

	m_pComms->Bind(this);

	m_pComms->Create();

	return TRUE;
	}

// Command Handlers

BOOL CLinkSendDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		GetDlgItem(IDOK).EnableWindow(FALSE);

		m_pComms->Terminate(2000);

		EndDialog(FALSE);
		}
	else
		EndDialog(!m_fError);

	CloseFile();

	return TRUE;
	}

BOOL CLinkSendDialog::OnCommsInit(UINT uID)
{
	TxFrame();

	return TRUE;
	}

BOOL CLinkSendDialog::OnCommsDone(UINT uID)
{
	if( RxFrame() ) {
		
		TxFrame();
		}

	return TRUE;
	}

BOOL CLinkSendDialog::OnCommsFail(UINT uID)
{
	if( !m_fDone ) {

		CString Text = m_pComms->GetErrorText();

		if( Text.Find('\n') < NOTHING ) {

			SetError(CString(IDS_UNABLE_TO));

			Error(Text);

			return TRUE;
			}

		SetError(Text);
		}

	return TRUE;
	}

// Implementation

void CLinkSendDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_pComms->Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			CloseFile();

			if( !m_fVerify ) {

				EndDialog(TRUE);

				return;
				}
			}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_CLOSE));
		}
	}

void CLinkSendDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf("ERROR - %s.", pText));

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
	}

void CLinkSendDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
	}

BOOL CLinkSendDialog::GetContinue(PCTXT pText)
{
	CString Text(pText);

	Text += CString(IDS_NNDO_YOU_WANT_TO);

	MessageBeep(MB_ICONEXCLAMATION);	

	return YesNo(Text) == IDYES;
	}

void CLinkSendDialog::TxFrame(void)
{
	if( m_uState == stateCheckModel ) {
		
		ShowStatus(CString(IDS_CHECKING_DEVICE));

		m_pComms->CheckBaseModel();

		return;
		}

	if( m_uState == stateCheckSerial ) {

		ShowStatus(CString(IDS_CHECKING_DEVICE_2));

		m_pComms->CheckBaseModel();

		return;
		}

	if( m_uState == stateCheckRevision ) {

		ShowStatus(CString(IDS_CHECKING_MODULE));

		m_pComms->ReadFirmwareRevision();

		return;
		}

	if( m_uState == stateWriteFirmware ) {

		ShowStatus(CString(IDS_UPGRADING_MODULE));

		m_pComms->ClearFile(&m_Firm);

		return;
		}

	if( m_uState == stateCheckFirmware ) {

		ShowStatus(CString(IDS_CHECKING_MODULE_2));

		m_pComms->ReadFile(&m_Firm);

		return;
		}

	if( m_uState == stateCheckJumper1 ) {

		ShowStatus(CString(IDS_CHECKING_DEVICE_3));

		m_pComms->ReadCfgSetup();

		return;
		}

	if( m_uState == stateCheckJumper2 ) {

		ShowStatus(CString(IDS_CHECKING_DEVICE_4));

		m_pComms->ReadCfgSetup();

		return;
		}

	if( m_uState == stateReadConfig ) {

		IFileData *pData = m_pSystem->m_Files[m_uFile];

		CString Name(pData->GetFile());

		ShowStatus(CPrintf("Reading %s file.", Name));

		m_pComms->ReadFile(pData);

		return;
		}

	if( m_uState == statePrepareItem ) {

		m_uItem = m_pDbase->GetHandleValue(m_Index);

		m_pItem = m_pDbase->GetHandleItem(m_Index);

		if( m_uItem == 0 && m_pDbase->HasImage() ) {

			ShowStatus(CString(IDS_PREPARING));

			m_Init.Empty();

			m_pItem->MakeInitData(m_Init);

			m_pComms->PrepareItem();

			return;
			}

		ShowStatus(CPrintf("Preparing item %4.4u (%s).", m_uItem, m_pItem->GetHumanName()));

		m_pItem->PrepareData();

		m_pComms->PrepareItem();
		
		return;
		}

	if( m_uState == stateCheckConfig ) {
		
		IFileData *pData = m_pSystem->m_Files[m_uFile];

		PCSTR pName = pData->GetFile();
		
		UINT uSize  = pData->GetDataSize();

		m_pFile     = New CFileSysData(pName, uSize);

		CString Name(m_pFile->GetFile());

		ShowStatus(CPrintf("Checking %s file.", Name));

		m_pComms->ReadFile(m_pFile);

		return;
		}

	if( m_uState == stateWriteConfig ) {
		
		IFileData *pData = m_pSystem->m_Files[m_uFile];	

		CString Name(pData->GetFile());

		ShowStatus(CPrintf("Writing %s file.", Name));

		m_pComms->WriteFile(pData);

		return;
		}

	if( m_uState == stateWriteImage ) {

		CString Name(m_Image.GetFile());

		if( m_pDbase->HasImage() ) {

			m_Image.Create(m_Init);
			}

		ShowStatus(CPrintf("Writing %s file.", Name));

		m_pComms->ClearFile(&m_Image);
		
		return;
		}

	if( m_uState == stateResetStation ) {
		
		ShowStatus(CString(IDS_RESETTING_UNIT));

		m_pComms->ResetStation();

		return;
		}

	if( m_fVerify ) {

		ShowStatus(CString(IDS_DONE_DEVICE));

		if( m_uSend == sendVerify ) {

			PlaySound(L"notify.wav", NULL, SND_FILENAME | SND_ASYNC);
			}
		
		SetDone(FALSE);

		return;
		}

	SetDone(FALSE);
	}

BOOL CLinkSendDialog::RxFrame(void)
{
	if( m_uState == stateCheckModel ) {

		UINT uBase, uModule;

		if( m_pComms->GetBaseModel(uBase, uModule) ) {

			if( m_pSystem->GetModuleBase() == uBase ) {

				if( m_pSystem->GetModuleIdent() == uModule ) {

					if( GetKeyState(VK_CONTROL) & 0x8000 ) {

						m_uFile  = 0;

						m_uState = stateReadConfig;
						}
					else {
						m_uState = stateCheckSerial;
						}

					return TRUE;
					}
				}

			SetError(CString(IDS_TARGET_DEVICE_IS));
			
			return FALSE;
			}

		SetError(CString(IDS_TARGET_DEVICE));
		
		return FALSE;
		}

	if( m_uState == stateCheckRevision ) {

		UINT uModule;

		if( m_pComms->GetModuleFirmwareRevision(uModule) ) {

			UINT uTarget = m_pSystem->m_Revision;

			if( uTarget > uModule ) {

				if( !OpenFirmware() ) {

					SetError(CString(IDS_UNABLE_TO_OPEN));
				
					return FALSE;
					}

				CString Text = CString(IDS_TARGET_DEVICE_2);

				if( TRUE || GetContinue(CFormat(Text, uModule, uTarget)) ) {

					m_uState = stateWriteFirmware;

					return TRUE;
					}
				}

			m_uFile = 0;

			m_uState = stateReadConfig;

			return TRUE;
			}

		SetError(CString(IDS_UNABLE_TO_READ));

		return FALSE;
		}

	if( m_uState == stateCheckFirmware ) {

		if( OpenFile(L"firmware") ) {

			PBYTE pBuff = New BYTE [m_dwSize];

			if( fread(pBuff, sizeof(BYTE), m_dwSize, m_pFirm) ) {
				
				if( memcmp(pBuff, m_Firm.GetDataBuffer(), m_dwSize) ) {

					SetError(CString(IDS_FAILED_TO_VERIFY));

					delete [] pBuff;
					
					return FALSE;
					}
				}

			delete [] pBuff;
			}		

		m_uFile = 0;

		m_uState = stateReadConfig;

		return TRUE;
		}

	if( m_uState == stateWriteFirmware ) {

		m_uState = stateCheckFirmware;

		return TRUE;
		}

	if( m_uState == stateCheckSerial ) {

		UINT uSerial;
		
		if( m_pComms->GetBaseSerialNumber(uSerial) ) {

			CString Text = CString(IDS_TARGET_DEVICE_3);
			
			if( m_pSystem->m_pComms->m_Serial == uSerial || GetContinue(CFormat(Text, uSerial)) ) {

				m_uState = stateCheckRevision;

				return TRUE;
				}
			
			SetDone(FALSE);

			return FALSE;
			}

		SetError( CString(IDS_COULD_NOT_GET) );

		return FALSE;
		}

	if( m_uState == stateCheckJumper1 ) {

		UINT uMode = m_pSystem->m_CfgComms.GetNetModeJumper();

		if( uMode == 3 ) {

			m_uState = stateCheckJumper2;

			return TRUE;
			}

		UINT uHard;

		if( m_pComms->GetBaseNetModeJumper(uHard) ) {
				
			if( uMode == uHard ) {

				m_uState = stateCheckJumper2;

				return TRUE;
				}

			CString Text = CPrintf(CString(IDS_TARGET_DEVICES), 
						    EnumNetModeJumper(uHard), 
						    EnumNetModeJumper(uMode)
						    );

			SetError(Text);

			return FALSE;
			}

		SetError(CString(IDS_TARGET_DEVICE_4));
		
		return FALSE;
		}

	if( m_uState == stateCheckJumper2 ) {

		UINT uMode = m_pSystem->m_CfgSetup.GetModuleSourceSinkJumper()[0];

		if( uMode == 2 ) {

			m_uFile  = 0;

			m_uState = stateWriteConfig;
			
			return TRUE;
			}

		UINT uHard;

		if( m_pComms->GetBaseSourceSinkJumper(uHard) ) {
				
			if( uMode == uHard ) {

				m_uFile  = 0;

				m_uState = stateWriteConfig;

				return TRUE;
				}

			CString Text = CPrintf(CString(IDS_TARGET_DEVICES_2), 
						    EnumSourceSinkJumper(uHard), 
						    EnumSourceSinkJumper(uMode)
						    );

			SetError(Text);

			return FALSE;
			}

		SetError(CString(IDS_TARGET_DEVICE_4));
		
		return FALSE;
		}

	if( m_uState == stateReadConfig ) {

		IFileData *pData = m_pSystem->m_Files[m_uFile];

		if( pData->IsValid() ) {

			if( ++ m_uFile < m_pSystem->m_Files.GetCount() ) {

				m_uState = stateReadConfig;

				return TRUE;
				}		

			m_Index  = m_pDbase->GetHeadHandle();

			m_uState = statePrepareItem;

			return TRUE;
			}

		CString Name = CString(pData->GetFile());

		SetError(CPrintf("Unable to read %s file", Name));
			
		return FALSE;
		}

	if( m_uState == statePrepareItem ) {

		// TODO -- check for new handles?

		if( m_pDbase->GetNextHandle(m_Index) ) {

			m_uState = statePrepareItem;
			}
		else {
			if( m_fVerify ) {

				m_uFile  = 0;
				
				m_uState = stateCheckConfig;
				}
			else {
				if( m_fTest ) {

					m_uState = stateDone;

					return TRUE;
					}
				else {
					m_uState = stateCheckJumper1;
					}
				}
			}
		
		return TRUE;
		}

	if( m_uState == stateCheckConfig ) {

		if( m_pFile ) {
			
			PBYTE pData = m_pFile->GetDataBuffer();

			UINT  uSize = m_pFile->GetDataSize();

			IFileData *pFile = m_pSystem->m_Files[m_uFile];

			if( !memcmp(pFile->GetDataBuffer(), pData, uSize) ) {

				m_pFile->Release();

				m_pFile = NULL;

				if( ++ m_uFile < m_pSystem->m_Files.GetCount() ) {									
					
					m_uState = stateCheckConfig;
					
					return TRUE;
					}				

				m_uState = stateDone;

				return TRUE;
				}
			else {
				Compare(pFile, uSize);

				m_pFile->Release();

				m_pFile = NULL;

				if( Verify() ) {					
					
					m_uState = stateCheckJumper1;

					return TRUE;
					}

				return FALSE;
				}
			}		
		
		return FALSE;
		}

	if( m_uState == stateWriteConfig ) {

		IFileData *pData = m_pSystem->m_Files[m_uFile];

		if( pData->IsValid() ) {

			if( ++ m_uFile < m_pSystem->m_Files.GetCount() ) {

				m_uState = stateWriteConfig;

				return TRUE;
				}

			m_uState = stateWriteImage;

			return TRUE;
			}

		CString Name = CString(pData->GetFile());

		SetError(CPrintf("Unable to write %s file.", Name));
		
		return FALSE;
		}

	if( m_uState == stateWriteImage ) {

		m_uState = stateResetStation;
		
		return TRUE;
		}

	if( m_uState == stateResetStation ) {

		m_uState = stateDone;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CLinkSendDialog::Verify(void)
{
	if( m_fVerify ) {

		PCTXT pText = CString(IDS_DATABASE_IN);
		
		if( NoYes(pText) == IDYES ) {
			
			m_fVerify = FALSE;
			
			return TRUE;
			}

		SetError(CString(IDS_DEVICE_DOES_NOT));

		return FALSE;
		}

	return TRUE;
	}

void CLinkSendDialog::Compare(IFileData *pFile, UINT uSize)
{
	if( FALSE ) {

		BOOL fInit = FALSE;

		PBYTE p1 = m_pFile->GetDataBuffer();

		PBYTE p2 = pFile->GetDataBuffer();

		for( UINT n = 0; n < uSize; n ++ ) {

			if( *p1++ == *p2++ ) {
							
				continue;
				}

			if( !fInit ) {

				AfxTrace(L"differ at");
				
				fInit = TRUE;
				}

			AfxTrace(L" %d [%2.2X]<%2.2X>\n", n, (p1-1)[0], (p2-1)[0]);
			}
		}
	}

PCTXT CLinkSendDialog::EnumNetModeJumper(UINT uMode)
{
	switch( uMode ) {
		
		case 0:	return	L"Pass-though";
		case 1:	return	L"Ring switch";
		case 2:	return	L"Dual Network";
		case 3:	return	L"Follow Jumper";
		}

	return L"";
	}

PCTXT CLinkSendDialog::EnumSourceSinkJumper(UINT uMode)
{
	switch( uMode ) {
		
		case 0:	return	L"Source (DI-)";
		case 1:	return	L"Sink (DI+)";
		case 2:	return	L"Follow Jumper";
		}

	return L"";
	}

BOOL CLinkSendDialog::OpenFirmware(void)
{
	if( OpenFile(L"firmware") ) {
		
		PBYTE pBuff = New BYTE [m_dwSize];

		if( fread(pBuff, sizeof(BYTE), m_dwSize, m_pFirm) ) {

			m_Firm.Create(m_dwSize);

			UINT uPtr = 0;

			m_Firm.PutData(uPtr, pBuff, m_dwSize);

			delete [] pBuff;

			CloseFile();
			
			return TRUE;
			}

		delete [] pBuff;

		CloseFile();
		}

	return FALSE;
	}

BOOL CLinkSendDialog::OpenFile(PCTXT pName)
{
	CloseFile();

	CFilename File = afxModule->GetFilename();

	File.ChangeName(CPrintf(L"firmware\\E3\\%s.bin", pName));

	if( m_pFirm = _wfopen(File, L"rb") ) {

		fseek(m_pFirm, 0, SEEK_END);

		m_dwSize = ftell(m_pFirm);
		
		fseek(m_pFirm, 0, SEEK_SET);		

		return TRUE;
		}

	return FALSE;
	}

void CLinkSendDialog::CloseFile(void)
{
	if( m_pFirm ) {
		
		fclose(m_pFirm);

		m_pFirm = NULL;
		}
	}

// End of File
	
