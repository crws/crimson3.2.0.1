
#include "intern.hpp"

#include "file.hpp"

#include "imports\udr.h"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Sys Wrapper
//

// Constructor

CFileSystem::CFileSystem(void)
{
	m_hHandle  = NULL;

	m_uTimeout = 2000;

	m_pData	   = NULL;	

	m_uError   = 0;
	}

// Destructor

CFileSystem::~CFileSystem(void)
{
	}

// Management

void CFileSystem::Bind(IFileData *pData)
{
	m_pData = pData;
	}

void CFileSystem::SetConfig(PCSTR pConfig, char cFormat)
{
	m_pConfig = pConfig;

	m_Format = cFormat;

	m_nSeq   = 0;
	}

// Attributes

UINT CFileSystem::GetErrorCode(void)
{
	return m_uError;
	}

INT CFileSystem::GetResultCode(void)
{
	return m_nResult;
	}

// Operations

BOOL CFileSystem::Read(void)
{
	if( GetAlias() ) {		

		if( m_pData->GetDataSize() == 0 ) {

			UINT uAlloc;
			
			if( Stat(m_pData->GetFile(), uAlloc) ) {				

				m_pData->Create(uAlloc);
				}
			}

		PBYTE pData = m_pData->GetDataBuffer();

		UINT uCount = m_pData->GetDataSize();

		UINT uPosn  = 0;		

		UINT uLimit = 230;

		while( uCount ) {

			UINT uLump = uCount;

			MakeMin(uLump, uLimit);
			
			if( Read(pData + uPosn, uPosn, WORD(uLump)) )  { 

				uCount -= uLump;

				uPosn  += uLump;
				
				continue;
				}

			m_pData->SetValid(FALSE);

			CloseAlias();

			return FALSE;
			}

		m_pData->SetValid(TRUE);

		CloseAlias();

		return TRUE;
		}

	m_pData->SetValid(FALSE);

	return FALSE;
	}

BOOL CFileSystem::Write(void)
{
	if( GetAlias() ) {

		PBYTE pData = m_pData->GetDataBuffer();

		UINT uCount = m_pData->GetDataSize();
		
		UINT uPosn  = 0;		

		UINT uLimit = 230;

		while( uCount ) {

			UINT uLump = uCount;

			MakeMin(uLump, uLimit);

			if( Write(pData + uPosn, uPosn, WORD(uLump)) )  { 

				uCount -= uLump;

				uPosn  += uLump;

				continue;
				}

			CloseAlias();

			return FALSE;
			}

		CloseAlias();

		return TRUE;
		}

	return FALSE;
	}

BOOL CFileSystem::Create(void)
{
	return Create(m_pData->GetFile(), m_pData->GetDataSize());
	}

BOOL CFileSystem::Delete(void)
{
	if( GetAlias() ) {

		if( Delete(m_pData->GetFile()) ) {

			CloseAlias();

			return TRUE;
			}

		CloseAlias();
		}

	return FALSE;
	}

BOOL CFileSystem::DeletePath(void)
{
	PSTR pPath = strdup(m_pData->GetFile());

	PSTR pName = strrchr(pPath, '/');

	*pName = 0;

	if( Delete(pPath) ) {

		free(pPath);
		
		return TRUE;
		}

	free(pPath);

	return FALSE;
	}

// Implementation

BOOL CFileSystem::GetAlias(void)
{
	BYTE	bError;	

	m_nResult = filesys_get_alias( PVOID(m_pConfig),
					 m_uTimeout, 
					 m_nSeq++, 
					 m_Format,
					 m_pData->GetFile(), 
					 0, 
					 &bError, 
					 &m_uAlias,
					 NULL, 
					 NULL, 
					 0, 
					 NULL
					 );

	m_uError = bError;

	return m_nResult == UDR_SUCCESS;
	}

BOOL CFileSystem::Read(PBYTE pData, UINT uPosn, WORD wSize)
{
	WORD wRead = wSize;

	BYTE bError;

	m_nResult = filesys_read( PVOID(m_pConfig),
				    m_uTimeout,
				    m_nSeq++,
				    m_Format,
				    &bError,
				    &m_uAlias,
				    &uPosn,
				    &wRead,
				    pData, 
				    NULL, 
				    NULL, 
				    0, 
				    NULL
				    );

	m_uError = bError;

	return m_nResult == UDR_SUCCESS;
	}

BOOL CFileSystem::Write(PBYTE pData, UINT uPosn, WORD wSize)
{
	WORD wSend = wSize;

	BYTE bError;		

	m_nResult = filesys_write( PVOID(m_pConfig),
				     m_uTimeout,
				     m_nSeq++,
				     m_Format,
				     &bError,
				     &m_uAlias,
				     &uPosn,
				     &wSend,
				     pData, 
				     NULL, 
				     NULL, 
				     0, 
				     NULL
				     );

	m_uError = bError;

	if( m_nResult == UDR_SUCCESS ) {

		if( wSend == wSize ) {
			
			return m_nResult == UDR_SUCCESS;
			}

		UINT uRetry = 6;		

		while( --uRetry ) {
			
			if( Write(pData, uPosn, wSize) ) {
				
				return TRUE;
				}
			}

		return FALSE;
		}

	return m_nResult == UDR_SUCCESS;
	}

BOOL CFileSystem::Create(PCSTR pName, UINT uSize)
{
	BYTE bError;

	m_nResult = filesys_create( PVOID(m_pConfig),
				      m_uTimeout, 
				      0, 
				      m_Format,
				      0,
				      uSize,
				      pName,
				      &bError,
				      &m_uAlias,
				      NULL, 
				      NULL, 
				      0, 
				      NULL
				      );

	m_uError = bError;

	return m_nResult == UDR_SUCCESS;
	}

BOOL CFileSystem::Delete(PCSTR pName)
{
	BYTE bError;

	m_nResult = filesys_delete( PVOID(m_pConfig),
				      m_uTimeout,
				      0, 
				      m_Format, 
				      0, 
				      pName,
				      &bError,
				      NULL, 
				      NULL, 
				      0, 
				      NULL
				      );

	m_uError = bError;

	if( m_nResult == UDR_SUCCESS ) {

		return m_nResult == UDR_SUCCESS;
		}
		
	/*AfxTrace(L"delete result %d\t[%s]\n", m_nResult, EnumResult(m_nResult));*/
		
	return m_nResult == UDR_SUCCESS;
	}

BOOL CFileSystem::Stat(PCSTR pName, UINT &uSize)
{
	filesys_stat_d d;

	BYTE bError;

	m_nResult = filesys_stat( PVOID(m_pConfig), 
				    m_uTimeout, 
				    0, 
				    m_Format, 
				    0, 
				    m_uAlias, 
				    &d, 
				    &bError, 
				    NULL, 
				    NULL, 
				    0, 
				    NULL
				    );

	m_uError = bError;

	if( m_nResult == UDR_SUCCESS ) {

		uSize = d.size;

		return m_nResult == UDR_SUCCESS;
		}
		
	/*AfxTrace(L"stat result %d\t[%s]\n", m_nResult, EnumResult(m_nResult));*/
		
	return m_nResult == UDR_SUCCESS;
	}

BOOL CFileSystem::CloseAlias(void)
{
	m_nResult = filesys_close_alias( PVOID(m_pConfig),
					   m_uTimeout,
					   0,
					   m_Format,
					   m_uAlias,
					   NULL,
					   NULL, 
					   0,
					   NULL
					   );

	if( m_nResult == UDR_SUCCESS ) {

		return m_nResult == UDR_SUCCESS;
		}

	/*AfxTrace(L"close alias result result %d\t[%s]\n", m_nResult, EnumResult(m_nResult));*/

	return m_nResult == UDR_SUCCESS;
	}

PCTXT CFileSystem::EnumResult(int nResult)
{
	switch( nResult ) {
		
		case -1:	return L"send error";
		case -2:	return L"receive error";
		case -3:	return L"not our packet";
		case -4:	return L"nak received";
		case -5:	return L"reply timeout";
		case -6:	return L"socket error";
		case -7:	return L"bad sequence";
		case -8:	return L"buffer size error";
		case -9:	return L"out of memory";
		case -10:	return L"authentication failure";
		case -11:	return L"parse error";
		case -12:	return L"invalid options";
		case -13:	return L"file open error";
		case -14:	return L"allocation error";
		case -15:	return L"file delete error";
		case -16:	return L"invalid alias";
		case -17:	return L"access error";
		case -18:	return L"file create error";
		case -19:	return L"file create size error";
		case -20:	return L"file seek error";
		case -21:	return L"unknown error";
		case -22:	return L"invalid filename";
		
		default:	return L"success";
		}
	}

// End of File
