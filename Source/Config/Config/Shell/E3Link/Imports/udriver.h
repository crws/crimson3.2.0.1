/**
 * @file
 * UDR protocol header file
 */

/*
 * Copyright 2012 SIXNET, LLC.
 *
 * Notice: This document contains proprietary, confidential information
 * owned by SIXNET, LLC and is protected as an unpublished work under
 * the Copyright Law of the United States. No part of this document may
 * be copied, disclosed to others or used for any purpose without the
 * written permission of SIXNET, LLC.
 */

#ifndef UDRIVER_H_
#define UDRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define MD5HASHLEN 16

#include "rl_platform.h"
#define MAX_FILENAME_LEN 52  /**< This includes the NUL terminator. */

/**
 * UDR Protocol Version Major and Minor Enums
 */
enum {UDR_PROTOCOL_MAJOR = 2};
enum {UDR_PROTOCOL_MINOR = 0};

/**
 * @defgroup C_BINDINGS C Bindings 
 * @{
 * @defgroup COMMANDS Commands 
 *
 * UDR command definitions.
 * @{
 */

#define UDR_NOP	   0x0000  /**< No Op */

#define UDR_ACK    0x0001  /**< Acknowledge */

#define UDR_NAK	   0x0002  /**< Negative Acknowledge */

#define UDR_VERS   0x0003  /**< Version Information */

#define UDR_NIO	   0x0004  /**< Number I/O Request */

#define UDR_GETD   0x000A  /**< Get Discretes */

#define UDR_GETB   0x000B  /**< Get Bytes */

#define UDR_GETA   0x000C  /**< Get Analogs */

#define UDR_GETS   0x000D  /**< Get String */

#define UDR_PUTD   0x000E  /**< Put Discretes */

#define UDR_PUTB   0x000F  /**< Put Bytes */

#define UDR_PUTA   0x0010  /**< Put Analogs */

#define UDR_SETD   0x0011  /**< Set Discretes */

#define UDR_CLRD   0x0012  /**< Clear Discretes */

#define UDR_REMOTE_OPEN 0x0014 /**< Open a remote file */

#define UDR_REMOTE_CLOSE 0x0015 /**< Close a remote file */

#define UDR_REMOTE_READ 0x0016 /**< Read a block from remote file */

#define UDR_REMOTE_WRITE 0x0017 /**< Write a block to a remote file */

#define UDR_SET_CLOCK  0x0018  /**< Set clock */

#define UDR_GET_CLOCK  0x0019  /**< Get clock */

#define UDR_FILESYS 0x001A  /**< Filesys Commands */

#define UDR_DATALOG 0x001B  /**< Datalog Commands */

#define UDR_IOXCHG  0x0020  /**< I/O Exchange Message */

#define GWY_MSGS    0x6010  /**< Gateway Messages */

#define ISAGRAF_MSGS 0x6011  /**< ISaGRAF Messages */
/**
 * @}
 */

/**
 * @cond PRIVATE
 * @defgroup FILESYS File System Commands
 *
 * These are subcommands of @link COMMANDS UDR_FILESYS
 * @endlink as the main command.
 * @{
 */

#define FILESYS_GETALIAS   0x00    /**< File System Get Alias */

#define FILESYS_READ       0x01    /**< File System Read */

#define FILESYS_WRITE      0x02    /**< File System Write */

#define FILESYS_CREATE     0x03    /**< File System Create */

#define FILESYS_DELETE     0x04    /**< File System Delete */

#define FILESYS_DIR        0x05    /**< File System Directory */

#define FILESYS_STAT       0x06    /**< File System Get Status */

#define FILESYS_COMPRESS   0x07    /**< File System Compress */

#define FILESYS_CHKDSK     0x08    /**< File System Check Disk Integrity */

#define FILESYS_RENAME	   0x09    /**< File System Rename Existing File */

#define FILESYS_MEMAVAIL   0x0A    /**< File System Get Memory Available */

#define FILESYS_FREESPACE  0x0B    /**< File System Get Memory 
                                      Available (IPm) */

#define FILESYS_CLOSEALIAS 0x0C    /**< Closes an alias */

/**
 * @}
 *
 * @defgroup DATALOG Datalog Commands
 *
 * These are subcommands of @link COMMANDS UDR_DATALOG
 * @endlink as the main command.
 *
 * @{
 */
#define DATALOG_GET_RECORD_TIME   0x00    /**< Get record time */

#define DATALOG_FIRST_AFTER_TIME  0x01    /**< Get first record after
                                             specified time */

#define DATALOG_GET_RECORDS       0x02    /**< Get datalog records */

#define DATALOG_GET_AND_CLEAR     0x03    /**< Get and clear datalog records */

#define DATALOG_MOVE_TAIL         0x04    /**< Move tail of datalog records */

#define DATALOG_GET_REC_SEGMENT   0x05    /**< Get a record segment */

#define DATALOG_SEND_INFO         0x11    /**<  Send information about
                                             the datalog file */

#define DATALOG_INITIATE_CLIENT_TRANSFER  0x12    /**<  Initiate a client
                                                   transfer */

/**
 * @}
 */


/**
 * @defgroup GATEWAY_MESSAGES Gateway Message System Commands
 *
 * These are subcommands of @link COMMANDS GWY_MSGS
 * @endlink as the main command.
 *
 * @{
 */
#define GWY_READ_MODULE_LIST           0x00    /**< Read module list */

#define GWY_MISCELLANEOUS	       0x01    /**< Gateway Sub-Command */

#define GWY_SET_MODULE_BLINK_RATE      0x01    /**< Set module blink rate */

#define GWY_SET_ALL_MODULES_BLINK_RATE 0x02    /**< Set blink rate for all
                                                modules */

#define GWY_SET_MODULE_SCAN_LIST       0x03    /**< Set module scan list */

#define GWY_SET_PORT_EEROM_COMM_PARAMS 0x04    /**< Set port params in eerom */

#define GWY_GET_PORT_EEROM_COMM_PARAMS 0x05    /**< Get port params from
                                                  eerom */

#define GWY_USE_PORT_EEROM_COMM_PARAMS 0x06    /**< Use port params from
                                                  eerom */

#define GWY_READ_MODULE_EEROM	       0x07    /**< Read Module EEROM */

#define GWY_WRITE_MODULE_EEROM	       0x08    /**< Write Module EEROM */


#define GWY_SET_MODULE_CALIBRATION_CONSTANTS 0x0B   /**< Set module calibration
                                                       constants */

#define GWY_GET_MODULE_CALIBRATION_CONSTANTS 0x0C   /**< Get module calibration
                                                       constants */

#define GWY_USE_MODULE_CALIBRATION_CONSTANTS 0x0D   /**< Use module calibration
                                                       constants */

#define GWY_READ_EEROM                       0x0E   /**< Read EEROM */

#define GWY_WRITE_EEROM                      0x0F   /**< Write EEROM */

#define GWY_RESET_MODULE                     0x10   /**< Reset module */

#define GWY_GET_MODULE_SCAN_LIST             0x11   /**< Get module scan list */

#define GWY_RESET_GATEWAY	             0x12   /**< Reset Gateway */

#define GWY_STATUS                           0x13   /**< Gateway status */

#define GWY_VERS                             0x14   /**< Gateway version */

#define GWY_READ_CONFIGURATION_BLOCK         0x15   /**< Read configuration
                                                       block */

#define GWY_WRITE_CONFIGURATION_BLOCK        0x16   /**< Write configuration
                                                       block */

#define GWY_DELETE_CONFIGURATION_BLOCK       0x17 /**< Delete configuration
                                                     block */

#define GWY_USE_PORT_EEROM_PROTOCOL          0x19 /**< Use port eerom
                                                     protocol */



#define GWY_RESET_IPM          0x1A    /**< Reset IPM */
#define GWY_RESET_REMOTETRAK	0x1A    /**< Reset RemoteTrak Module */

#define GWY_READ_AMBIENT	0x1B    /**< Read Ambient Temperature Sensor 
                                           (INS) */

#define GWY_IPM_READ_SETTINGS	0x1C    /**< Read Settings Command */

#define GWY_IPM_TEMP_SETTINGS	0x1D    /**< Temp Settings Command */

#define GWY_IPM_EXECUTE         0x1E    /**< Gateway Execute Command */

/**
 * @defgroup GATEWAY_MISC Gateway Miscellaneous sub-subcommands
 *
 * These are subcommands of @link COMMANDS GWY_MSGS
 * @endlink as the main command and @link GATEWAY_MESSAGES
 * GWY_MISCELLANEOUS @endlink as the sub command.
 *
 * @{
 */

#define GWY_READ_DHCP_STATUS	0x20   /**< read DHCP Status */

#define GWY_RELEASE_DHCP_LEASE	0x21   /**< release DHCP */

#define GWY_RENEW_DHCP_LEASE	0x22   /**< renew DHCP */

/**
 * @}
 */

/**
 * @defgroup DHCP_PARAMETERS DHCP Parameters 
 *
 * These parameters are used with the @link GATEWAY_MISC DHCP-related
 * subcommands @endlink of the @link GATEWAY_MESSAGES
 * GWY_MISCELLANEOUS suncommand @endlink.
 *
 * @{
 */

#define DHCP_ID_CLIENT_IP	0x00    /**< Client IP */

#define DHCP_ID_SERVER_IP	0x01    /**< Server IP */

#define DHCP_ID_SUBNET		0x02    /**< Subnet Mask */

#define DHCP_ID_GATEWAY		0x03    /**< Default Gateway */

#define DHCP_ID_OBTAIN		0x04    /**< Time Obtained */

#define DHCP_ID_EXPIRE		0x05    /**< Time Expires */

#define DHCP_ID_RENEW		0x06    /**< Time Renew */

#define DHCP_ID_REBIND		0x07    /**< Time Rebind */

#define DHCP_ID_ERRORCODE	0x08    /**< Error Code */

#define DHCP_ID_CLOCK		0x09    /**< current time */

/**
 * @}
 * @}
 * @endcond
 */

/**
 * @defgroup IO_TYPES I/O Types
 *
 * UDR I/O data type definitions.
 * @{
 */

#define TYPE_AX	 0 /**< Analog in */

#define TYPE_AY	 1 /**< Analog out */

#define TYPE_X	10 /**< Discrete in */

#define TYPE_Y	11 /**< Discrete out */

#define TYPE_LX	20 /**< Long in */

#define TYPE_LY	21 /**< Long out */

#define TYPE_FX	22 /**< Floating point in */

#define TYPE_FY	23 /**< Floating point out */

/**
 * @}
 */

/**
 * @defgroup PRODUCT_CODES Product codes
 *
 * @brief These are used with the #UDR_VERS command to determine what type of 
 * unit you are (when sending with #UDR_VERS) and what type is responding (when
 * it responds with a #UDR_ACK).
 *
 * @{
 */

#define VERS_OLD_NONSIXNET     0 /**< Non-Sixnet with old Universal
                                      Protocol capabilities
                                      (doesn’t support non-byte
                                      boundaries) */

#define VERS_IOMUX_VERSAMUX  110 /**< IOMUX and Versamux */

#define VERS_60_IBM_N        140 /**< 60-IBM/N */

#define VERS_SIXTRAK         200 /**< SIXTRAK Gateway */

#define VERS_ETHERTRAK       220 /**< EtherTRAK */

#define VERS_ETHERTRAK2      221 /**< EtherTRAK-2 */

#define VERS_SIXNET_SOFTWARE 250 /**< Sixnet Control Room Software */

#define VERS_NEW_NONSIXNET   254 /**< Non-Sixnet with new Universal
                                      Protocol capabilities
                                      (supports non-byte
                                      boundaries) */

/**
 * @}
 */

/**
 * @defgroup CONVENIENCE_CONSTANTS Convenience constants
 *
 * @brief These are a collection of convenience aliases for commonly used
 * constants.
 *
 * @{
 */

#define ANY_STATION   0x603F  /**< ANY Station Flag for UDR */

#define ANY_PASS      0x6030  /**< ANY Station Flag for UDR with
                                 passthrough enabled */

#define ANY_ETRAK     0x6031  /**< ANY EtherTrak Station */

#define ANY_SERIAL    0xFFFFFFFFL  /**< Serial Number Wildcard */

/**
 *@}
 */

/**
 * @defgroup LEADER Lead characters
 *
 * @brief The lead character specifies the format of the message that is to
 * follow.
 * @{
 */

#define HEX_LEADER    ']'  /**< Universal Driver HEX mode leader */

#define BIN_LEADER    ')'  /**< Universal Driver Binary mode leader */

#define NOCRC_LEADER  '}'  /**< Universal Driver Binary mode leader */

/**
 *@}
 */

/**
 * @defgroup CHALLENGE_RESPONSE Challenge/response Types
 *
 * The Challenge/response type is used to specify the type of a trailer. 
 * @{
 */
#define DIGEST_CHALLENGE 0  /**< UDR-A digest challenge */

#define AUTHENTICATION_TRAILER 1 /**< UDR-A authentication trailer */

/**
 * @}   
 * @}
 */
#define UDR_USERNAME_MAXLEN 32

/**
 * UDR Authentication trailer
 */
PACK(
typedef struct {
    uint8_t auth_vers;               /**< Authentication version */
    uint8_t num_bytes;               /**< Number of bytes to end of
                                        authentication data */
    uint8_t type_challenge_response; /**< Message type. 0 = Digest
                                        challenge. 1 = Authentication
                                        trailer */
    uint8_t reserved1[5];            /**< Reserved */
    uint8_t qop;                     /**< Quality of protection. 1 =
                                        auth, the only currently
                                        supported value */
    uint32_t nc;                     /**< Nonce counter. Incremented
                                        on every request using a given
                                        nonce, which should be on
                                        every message (unless the
                                        server sends a new nonce). The
                                        server should enforce that it
                                        is increasing. */
    uint8_t cnonce[4];               /**< Client nonce. A unique value
                                        generated by the client for
                                        each response. */
    uint8_t nonce[16];               /**< Server nonce. This is
                                        calculated once per challenge
                                        and is passed between client
                                        and server in future
                                        messages. */
    uint8_t opaque[16];              /**< Opaque data to be passed
                                        back to the server unmodified
                                        by the client. Typically used
                                        by the server to store
                                        state. */
    uint8_t response[MD5HASHLEN];    /**< Response value to the
                                        challenge. Note that this is
                                        the full HA2 hash - the HA1
                                        hash is never stored in this
                                        structure. */
    char user_name[UDR_USERNAME_MAXLEN]; /**< User name. NUL padded */
    uint8_t reserved2[4];            /**< Reserved. Should be 0 */
} ) udr_authentication_trailer; 


/**
 * UDR Digest challenge trailer
 */
PACK(
typedef struct {
    uint8_t auth_vers;               /**< Authentication version */
    uint8_t num_bytes;               /**< Number of bytes to end of
                                        authentication data */
    uint8_t type_challenge_response; /**< Message type. 0 = Digest
                                        challenge. 1 = Authentication
                                        trailer */
    uint8_t reserved1[5];            /**< Reserved */
    uint8_t qop;                     /**< Quality of protection. 1 =
                                        auth, the only currently
                                        supported value */
    uint8_t nonce[16];               /**< Server nonce */
    uint8_t opaque[16];              /**< Opaque data to be passed
                                        around unmodified */
    uint8_t stale;                   /**< Stale flag. The server sets
                                        this flag in a reply to force
                                        the client to accept a new
                                        nonce. 0x01 if the response
                                        is valid but the nonce is
                                        stale, 0x00 otherwise.*/
    uint8_t reserved2[8];            /**< Reserved. Should be 0 */
} ) udr_digest_challenge;

/**
 * UDR Message structure
 *
 * @note This is a command structure with a lot of metadata. It
 * *must not* be used as an overlay on to a raw packet.
 */
typedef struct {
    uint8_t egressPort;         /**< Egress port (interface) to send on (if
                                     Ethernet) */
    uint8_t ethernetAddress[6]; /**< Ethernet Address (If port is Ethernet) */
    uint32_t sourceIPAddress;       /**< IP Address incoming is from (If
                                         IP message) */
    uint32_t destinationIPAddress;  /**< IP Address of receiving
                                       interface (If IP message) */
    uint16_t sourceUDPPort;       /**< UDP Port incoming message is from */
    uint16_t destinationUDPPort;  /**< UDP Port incoming message is to */
    uint8_t format;        /**< Message Format */
    uint8_t length;        /**< Message Length. Note that this is
                                *not* the actual number of bytes
                                transmitted, because certain bytes are
                                not counted here. */
    uint16_t sourceStation;      /**< Source UDR station */
    uint16_t destinationStation; /**< Dest UDR station */
    uint16_t session;      /**< Session of UDR message */
    uint8_t sequence;      /**< Sequence of UDR message */
    uint16_t command;      /**< Universal Driver Command */
    uint16_t subcommand;   /**< Universal Driver Subcommand */
    uint16_t subcommand2;  /**< Universal Driver Subcommand2 */
    uint16_t crc;          /**< Universal Driver CRC */
    uint16_t dataBytesMax; /**< The size of the data buffer, in
                                bytes. */
    uint16_t currentByte;  /**< Index to current byte (used for
                                packet filling and parsing). When the
                                packet is done, this is also the
                                total number of bytes in the message */
    uint8_t *data;         /**< Pointer to message Data Buffer */
    int isBroadcast;       /**< 1 if broadcast message, 0 if not */
    int receivedMessageSize;    /**< Size reported by the receive
                                   function used (typically recv or
                                   recvfrom). This is used to ensure
                                   we don't blow past of the end of
                                   the buffer looking for the trailer */
    const char *bindInterface;   /**< Interface used to limit sending
                                      of message, if any (requires
                                      root permissions) */
    udr_authentication_trailer* 
        authenticationTrailer; /**< Pointer to authentication
                                  trailer. NULL if not present or
                                  packet is a digest challenge */
    udr_digest_challenge* digestTrailer; /**< Pointer to digest
                                             trailer. NULL if not
                                             present or packet is an
                                             authentication packet */
    char* realm; /**< Digest realm. This data comes after the fixed
                    struct sizes and is NUL terminated */
} udr_message;

/**
 * #UDR_VERS command payload overlay struct
 *
 * @note The version numbers are in the packet, but are pretty much
 * worthless. Even though this has been in the protocol since the
 * early days, it was mis-implemented even then, always returning a
 * fixed value. Consequently, getting the firmware version should be
 * done via a file command (and is likely specific to the target) and
 * the value here should not be used.
 */
PACK(
typedef struct {
    uint16_t productCode; /**< @link PRODUCT_CODES Product code
                             @endlink */
    uint8_t majorVersion; /**< Module major version number */
    uint8_t minorVersion; /**< Module minor version number */
} ) vers_payload;

/**
 * @link COMMANDS PUT/GET @endlink command instructions
 * overlay struct
 */
PACK(
typedef struct {
    uint8_t type;               /**< @link IO_TYPES I/O type @endlink on which
                                   to operate */
    uint16_t startingAddress;   /**< Address of starting register */
    uint16_t numberOfRegisters; /**< Number of registers to which
                                     the command applies */
} ) data_command;

/**
 * Fixed portion of the UDR header.
 *
 * These two fields MUST exist and MUST be these sizes.
 */
PACK(
typedef struct {
    uint8_t lead;   /**< Lead character, which determines UDR message
                         format */
    uint8_t length; /**< Length of message, not including certain
                         double-byte fields, or the CRC */
} ) udr_fixed_header;

/**
 * @cond private
 */
PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint8_t options; /**< Options for the filesys_getalias command */
    char filename[0]; /**< Variable length, NUL terminated */
} ) filename_payload;

PACK(
typedef struct {
    uint8_t  error_code; /**< Error code in get_alias reply.
                          *   0 no error
                          *   1 invalid option
                          *   2 failed to open file
                          */
    uint32_t alias; /**< 4-byte alias/reference to the file. */
} ) alias_reply_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint32_t alias; /**< alias to file to perform read operation on. */
    uint32_t position; /**< position in file to start the read operation */
    uint16_t num_bytes; /**< number of bytes to read from file */
} ) read_payload;

PACK(
typedef struct {
    uint32_t alias;
    uint8_t error_code;
    uint32_t position;
    uint16_t num_bytes;
    uint8_t data[0];
} ) read_reply_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint32_t alias; /**< alias to file to perform write operation on. */
    uint32_t position; /**< position in file to start the write operation */
    uint16_t num_bytes; /**< number of bytes to write to the file */
    uint8_t data[0]; /**< data to be written to the file */
} ) write_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint8_t options; /**< Options for the filesys_getalias command */
    uint32_t filesize; /**< size of file to create - if 0, then the file
                          is dynamic */
    char filename[0]; /**< Variable length, NUL terminated */
} ) create_payload;

PACK(
typedef struct {
    uint8_t  error_code; /**< Error code. */
    uint32_t alias; /**< 4-byte alias/reference to the file. */
} ) create_reply_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint8_t options; /**< Options for filesys_dir
                      *  0 - standard reply
                      *  1 - extended information
                      */
    uint32_t alias; /**< Alias handle for directory */
    uint32_t position; /**< Position in directory */
} ) filesys_dir_payload;

PACK(
typedef struct {
    uint8_t error_code; /**< error codes for FILESYS_DIR
                         * 0 - no error
                         * 1 - invalid option byte */
    uint32_t position; /**< Position in directory */
    uint8_t count; /**< number of directory entries in response */
    uint8_t data[0]; /**< data - buffer containing the diretory entries */
} ) filesys_dir_reply_payload;

typedef struct {
    char name[MAX_FILENAME_LEN]; /**< filename */
    uint32_t size; /**< size of the file in bytes */
    uint32_t last_modified; /**< time file was last modified */
} dir_standard_info;

PACK(
typedef struct {
    char name[MAX_FILENAME_LEN]; /**< filename */
    uint32_t size; /**< size of the file in bytes */
    uint32_t modified; /**< time file was last modified */
    uint32_t accessed; /**< time file was last accessed */
    uint32_t changed; /**< time file was last changed */
    uint32_t mode; /**< mode for the file */
    uint32_t uid; /**< User id for the owner of the file */
    uint32_t gid; /**< Group id for the owner of the file */
} ) dir_extended_info;

PACK(
typedef struct {
    uint8_t sub_command;
    uint8_t options;
    uint32_t alias;
} ) alias_payload;

typedef struct {
    char filename[52];
    uint32_t size;
    uint32_t last_modified;
    uint8_t access;
} filesys_stat_d;

PACK(
typedef struct {
    uint8_t sub_command;
    uint8_t options;
} ) compress_payload;

PACK(
typedef struct {
    uint8_t sub_command;
    uint8_t options;
} ) chkdsk_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint8_t options; /**< none defined, must be zero */
    char data[0]; /**< source and destination names are contained here */
} ) rename_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint8_t options; /**< none defined, must be zero */
} ) memavail_payload;

PACK(
typedef struct {
    uint8_t error_code;
    uint32_t mem_avail;
} ) filesys_memavail_reply_payload;

PACK(
typedef struct {
    uint8_t sub_command;
    uint8_t options;
    uint8_t length;
    char path[0];
} ) freespace_payload;

PACK(
typedef struct {
    uint8_t error_code;
    uint8_t compression_type;
    uint32_t mem_avail;
} ) filesys_freespace_reply_payload;

PACK(
typedef struct {
    uint8_t sub_command; /**< FILESYS sub-command */
    uint32_t alias; /**< FILESYS alias to close */
} ) close_alias_payload;

PACK (
typedef struct {
    uint8_t sub_command; /**< GWY_MSGS sub-command */
    uint32_t module_id; /**< module id */
    uint8_t is_hard_reset;
} ) gateway_reset_payload;
#define UDR_PORT 1594

/**
 * @endcond
 *
 * @addtogroup C_BINDINGS
 * @{
 *
 * @defgroup RETURN_CODES Return codes
 * 
 * These are a collection of return codes that are sent from a UDR device.
 * @{
 */

#define UDR_SUCCESS 0 /**< Success (No errors) */

#define UDR_ERROR_SENDFAIL -1 /**< Error sending packet */

#define UDR_ERROR_RECEIVEFAIL -2 /**< Error receiving packet */

#define UDR_ERROR_NOTOURS -3 /**< Packet received is not for us */

#define UDR_ERROR_NAK -4 /**< Got a NAK packet as a reply */

#define UDR_ERROR_TIMEOUT -5 /**< Timed out waiting for reply */

#define UDR_ERROR_SOCKET -6 /**< Error setting up the socket */

#define UDR_ERROR_SEQUENCE -7 /**< Got a packet with the wrong
                                 sequence number */

#define UDR_ERROR_INSUFFICIENT_BUFFER -8 /**< The buffer is too small
                                            for the data we want to
                                            put in it */

#define UDR_ERROR_OUT_OF_MEMORY -9 /**< The system is out of memory
                                      (malloc() failed) */

#define UDR_ERROR_AUTHENTICATION_FAIL -10 /**< Authentication failure */

#define UDR_ERROR_INCOMPLETE -11 /**< Packet is not long enough to parse */

#define UDR_ERROR_INVALID_OPTION -12 /**< Options parameter invalid */

#define UDR_ERROR_FAILED_OPEN -13 /**< Failed to open a file */

#define UDR_ERROR_FAILED_ALLOC_SIZE -14 /**< Failed to create file with specified size */

#define UDR_ERROR_FAILED_DELETE -15 /**< Failed to delete a file */

#define UDR_ERROR_INVALID_ALIAS -16 /**< Invalid alias */

#define UDR_ERROR_FILESYS_ACCESS -17 /**< Filesystem access failure */

#define UDR_ERROR_CREATE_FILE -18 /**< Failed to create file */

#define UDR_ERROR_CREATE_FILE_SIZE -19 /**< Failed to create file with specified size */

#define UDR_ERROR_SEEK -20 /**< file seek/postion error */

#define UDR_ERROR_UNKNOWN_CODE -21 /**< Unknown filesys error return code */

#define UDR_ERROR_INVALID_FILENAME -22 /**< Invalid filename */
/**
 * @}
 */

/**
 * @defgroup PERMISSION_BITS Permission Bits
 *
 * These are a collection of possible permission
 * @{
 */

#define VIEW_IO           (1 << 0) /**< View I/O values */
#define MODIFY_IO         (1 << 1) /**< Modify I/O values */
#define CONFIGURE_IO      (1 << 2) /**< Configure I/O tags and parameters */
#define CONFIGURE_USERS   (1 << 3) /**< Configure users */
#define CONFIGURE_COMMS   (1 << 4) /**< Configure communications */
#define CALIBRATE         (1 << 5) /**< Calibrate */
#define LOAD_FIRMWARE     (1 << 6) /**< Load firmware */
#define VIEW_FILESYS      (1 << 7) /**< Filesystem read operations */
#define MODIFY_FILESYS    (1 << 8) /**< Filesystem modify operations */

// < Bit 9-15: Reserved 

#define VIEW_GATEWAY      (1 << 16) /**< Gateway read operations */
#define MODIFY_GATEWAY    (1 << 17) /**< Gateway execute operations */
#define VIEW_DATALOG      (1 << 18) /**< Datalog read operations */
#define MODIFY_DATALOG    (1 << 19) /**< Datalog modify operations */
#define VIEW_SYSTEM       (1 << 20) /**< System read operations */
#define MODIFY_SYSTEM     (1 << 21) /**< System modify operations */
#define ISAGRAF_PERM      (1 << 22) /**< ISaGRAF operations */

/** 
 * @}
 */

// < Bit 23-31: Reserved 

/**
 *@}
 */

void seedNonce();
void writeNonce(void *dest, int bytes);
void addToLength(udr_message *message, int length);
int appendCRC(udr_message *message);
int buildUDRMessageHeader(udr_message *message);
int parseUDRPacketHeader(udr_message *message);
int parseUDRATrailer(udr_message *message);
int validateAuthenticationByHash(udr_message *message, uint8_t *digest);
int validateAuthenticationByPassword(udr_message *message,
                                     const char *userName,
                                     const char *password,
                                     const char *realm);
int appendDataCommandParameters(udr_message *message,
                                int type, int addr, int numregs);
int parseDataCommandParameters(udr_message *message, uint16_t command,
                               int *type, int *addr, int *numregs);
int appendPutDataCommandParameters(udr_message *message,
                                   int type,
                                   int addr,
                                   int numregs, unsigned int *data);
int appendDigestChallenge(udr_message *message, uint8_t stale,
                          uint8_t qop, const char *realm);
int appendAuthenticationTrailerWithPassword(udr_message * message, 
                                            uint32_t nonceCount,
                                            uint8_t *cnonce, uint8_t *nonce,
                                            uint8_t *opaque, uint8_t qop, 
                                            const char *user_name, 
                                            const char *realm,
                                            const char *password);
int appendAuthenticationTrailerWithHash(udr_message * message, 
                                        uint32_t nonceCount,
                                        uint8_t *cnonce, uint8_t *nonce,
                                        uint8_t *opaque, uint8_t qop,
                                        const char *userName,
                                        const char *realm,
                                        unsigned char *digest);
int appendAuthenticationTrailer(udr_message *message, 
                                udr_authentication_trailer 
                                *authenticationTrailer,
                                const char *realm);
int UDRToHostDataBuffer(int command, int type, int numberOfRegisters,
                        uint8_t *data);
int HostToUDRDataBuffer(int command, int type, int numberOfRegisters,
                        uint8_t *data);
int unpackDataPacket(udr_message *message, int numregs,
                     int command, int type, unsigned int *data,
                     int dataSize);
int packDataPacket(udr_message *message, int numregs, int type,
                   unsigned int *data);
int createHA1Hash(const char *userName, const char *password,
                  const char *realm, unsigned char *digest);
int createResponseHash(uint32_t nonceCount, uint8_t *cnonce,
                       uint8_t *nonce, uint8_t qop, uint8_t *passwordHash,
                       uint8_t *responseHash);
int checkPermissions(int permissions,
                     uint16_t command,
                     uint8_t subcommand,
                     uint8_t subcommand2);
int checkFilesysPermissions(int permissions,
                            uint16_t command,
                            uint8_t subcommand);
int checkGatewayPermissions(int permissions,
                            uint16_t command,
                            uint8_t subcommand,
                            uint8_t subcommand2);
int checkDatalogPermissions(int permissions,
                            uint16_t command,
                            uint8_t subcommand);
void populateAuthenticationTrailer(
    udr_authentication_trailer *authenticationTrailer,
    uint32_t nonceCount, uint8_t *cnonce, uint8_t *nonce,
    uint8_t *opaque, uint8_t qop, const char *userName, 
    const char *realm, uint8_t *response);
int populateAuthenticationTrailerWithHash(
    udr_authentication_trailer *authenticationTrailer,
    uint32_t nonceCount, uint8_t *cnonce, uint8_t *nonce,
    uint8_t *opaque, uint8_t qop, const char *userName, 
    const char *realm, uint8_t *digest);
int populateAuthenticationTrailerWithPassword(
    udr_authentication_trailer *authenticationTrailer,
    uint32_t nonceCount, uint8_t *cnonce, uint8_t *nonce,
    uint8_t *opaque, uint8_t qop, const char *userName, 
    const char *realm, const char *password);

#ifdef __cplusplus
}
#endif
#endif /* UDRIVER_H_ */

/* vi: set sw=4: */
