
/**
 * @file
 * 
 * UDR master functions
 */

/*
 *
 * Copyright 2012 SIXNET, LLC.
 *
 * Notice: This document contains proprietary, confidential information
 * owned by SIXNET, LLC and is protected as an unpublished work under
 * the Copyright Law of the United States. No part of this document may
 * be copied, disclosed to others or used for any purpose without the
 * written permission of SIXNET, LLC.
 */

#ifndef MASTER_H
#define MASTER_H

#include "udriver.h"
#include "udr_list.h"

#include <stdint.h>

// In most applications, STATION is the station's IP address as a string and
// LibUDR functions use it to open a socket, bind it, etc.  Some internal, 
// alternative implementations manage the socket in the application and pass
// an opaque pointer.
#if defined(__gnu_linux__) || defined(SWIG) || defined(DOXYGEN)
/**  
 * IPv4 address as a string (e.g., "10.1.0.1") 
 */
#define STATION const char *
#else
// Cast to pointer to driver class to access ->SendFrame(), etc.
#define STATION void *
#endif

#if defined(__gnu_linux__)
#define HANDLE int
#else
typedef void * HANDLE;
#endif

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#if defined(_WINDLL)

#ifdef	PROJECT_LIBUDR

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "libudr.lib")

#endif

#else

#define DLLAPI

#endif

DLLAPI int geta(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         int dataSize, udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);
DLLAPI int puta(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);

DLLAPI int getd(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         int dataSize, udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);
DLLAPI int putd(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);

DLLAPI int getb(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         int dataSize, udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);
DLLAPI int putb(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);

DLLAPI int getl(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         int dataSize, udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);
DLLAPI int putl(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, unsigned int *data,
         udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);

DLLAPI int getf(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, float *data,
         int dataSize, udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);
DLLAPI int putf(STATION station, int timeout_ms, int sequence, char format,
         int type, int addr, int numberOfRegisters, float *data,
         udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);

DLLAPI int vers(STATION station, int timeout_ms, int sequence, char format,
         int *productCode, int *majorVersion, int *minorVersion,
         udr_authentication_trailer *authenticationTrailer,
         char *realm, int realmLength, udr_digest_challenge *digestTrailer);

/*
 * UDR file system command prototypes.
 */

DLLAPI int filesys_get_alias(STATION station, int timeout_ms, int sequence,
                      char format, const char *filename, uint8_t options,
                      uint8_t *error_code, uint32_t *alias,
                      udr_authentication_trailer *authenticationTrailer,
                      char *realm, int realmLength,
                      udr_digest_challenge *digestTrailer);

DLLAPI int filesys_read(STATION station, int timeout_ms, int sequence, char format,
                 uint8_t *error_code, uint32_t *alias,
                 uint32_t *position, uint16_t *num_bytes, uint8_t *data,
                 udr_authentication_trailer *authenticationTrailer,
                 char *realm, int realmLength,
                 udr_digest_challenge *digestTrailer);

DLLAPI int filesys_write(STATION station, int timeout_ms, int sequence, char format,
                  uint8_t *error_code, uint32_t *alias,
                  uint32_t *position, uint16_t *num_bytes, uint8_t *data,
                  udr_authentication_trailer *authenticationTrailer,
                  char *realm, int realmLength,
                  udr_digest_challenge *digestTrailer);

DLLAPI int filesys_create(STATION station, int timeout_ms, int sequence, char format,
                   uint8_t options, uint32_t filesize, const char *filename,
                   uint8_t *error_code, uint32_t *alias,
                   udr_authentication_trailer *authenticationTrailer,
                   char *realm, int realmLength,
                   udr_digest_challenge *digestTrailer);

DLLAPI int filesys_delete(STATION station, int timeout_ms, int sequence, char format,
                   uint8_t options, const char *filename,
                   uint8_t *error_code,
                   udr_authentication_trailer *authenticationTrailer,
                   char *realm, int realmLength,
                   udr_digest_challenge *digestTrailer);

DLLAPI int filesys_dir(STATION station, int timeout_ms, int sequence,
                char format, uint8_t options, uint32_t alias,
                uint8_t *error_code, uint8_t *count,
                uint32_t *position, uint8_t *data,
                udr_authentication_trailer *authenticationTrailer,
                char *realm, int realmLength,
                udr_digest_challenge *digestTrailer);

DLLAPI int filesys_stat(STATION station, int timeout_ms, int sequence, char format,
                 uint8_t options, uint32_t alias, filesys_stat_d *stat,
                 uint8_t *error_code,
                 udr_authentication_trailer *authenticationTrailer,
                 char *realm, int realmLength,
                 udr_digest_challenge *digestTrailer);

DLLAPI int filesys_compress(STATION station, int timeout_ms, int sequence,
                     char format, uint8_t options, uint8_t *error_code,
                     udr_authentication_trailer *authenticationTrailer,
                     char *realm, int realmLength,
                     udr_digest_challenge *digestTrailer);

DLLAPI int filesys_chkdsk(STATION station, int timeout_ms, int sequence, char format,
                   uint8_t options, uint8_t *error_code,
                   udr_authentication_trailer *authenticationTrailer,
                   char *realm, int realmLength,
                   udr_digest_challenge *digestTrailer);

DLLAPI int filesys_rename(STATION station, int timeout_ms, int sequence, char format,
                   uint8_t options, const char *source_file,
                   const char *destination_file, uint8_t *error_code,
                   udr_authentication_trailer *authenticationTrailer,
                   char *realm, int realmLength,
                   udr_digest_challenge *digestTrailer);

DLLAPI int filesys_memavail(STATION station, int timeout_ms, int sequence,
                     char format, uint8_t options, uint8_t *error_code,
                     uint32_t *mem_avail,
                     udr_authentication_trailer *authenticationTrailer,
                     char *realm, int realmLength,
                     udr_digest_challenge *digestTrailer);

DLLAPI int filesys_freespace(STATION station, int timeout_ms, int sequence,
                      char format, uint8_t options, const char *path,
                      uint8_t *error_code, uint8_t *compression_type,
                      uint32_t *mem_avail,
                      udr_authentication_trailer *authenticationTrailer,
                      char *realm, int realmLength,
                      udr_digest_challenge *digestTrailer);

DLLAPI int filesys_close_alias(STATION station, int timeout_ms, int sequence,
                        char format, uint32_t alias,
                        udr_authentication_trailer *authenticationTrailer,
                        char *realm, int realmLength,
                        udr_digest_challenge *digestTrailer);

#ifdef __gnu_linux__ // Only on Linux.
int probe(const char *interface, int timeout_ms, int sequence, char format,
          int productCode, int majorVersion, int minorVersion,
          udr_message_list ** responses, int *responseCount);
#endif

DLLAPI int parseVersPacketReply(udr_message * packet, int *productCode,
                         int *majorVersion, int *minorVersion);

/*
 * UDR gateway command prototypes.
 */
DLLAPI int gateway_reset(STATION station, int timeout_ms, int sequence,
                         char format, int is_hard_reset, uint32_t module_id,
                         udr_authentication_trailer *authenticationTrailer,
                         char *realm, int realmLength,
                         udr_digest_challenge *digestTrailer);

DLLAPI HANDLE system_open_comm(const char *config);
DLLAPI void system_close_comm(HANDLE fd);


#endif // MASTER_H
