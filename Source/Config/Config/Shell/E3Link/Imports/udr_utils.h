
/**
 * @file
 *
 * UDR protocol generic utility functions header file
 */

/*
 *
 * Copyright 2012 SIXNET, LLC.
 *
 * Notice: This document contains proprietary, confidential information
 * owned by SIXNET, LLC and is protected as an unpublished work under
 * the Copyright Law of the United States. No part of this document may
 * be copied, disclosed to others or used for any purpose without the
 * written permission of SIXNET, LLC.
 */

#ifndef UDR_UTIL_H_
#define UDR_UTIL_H_

#include <string.h>     // for size_t

//////////////////////////////////////////////////////////////////////////
//
// Export Management
//

#undef  DLLAPI

#if defined(_WINDLL)

#ifdef	PROJECT_LIBUDR

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "libudr.lib")

#endif

#else

#define DLLAPI

#endif

DLLAPI const char *getProductCodeDescriptionString(int code);
DLLAPI const char *getReturnCodeDescriptionString(int code);

DLLAPI int packDiscretes(unsigned int * unpackedArray, unsigned int numRegisters, 
                  unsigned int * packedArray, size_t packedArraySize);
DLLAPI int unpackDiscretes(unsigned int * packedArray, size_t packedArraySize,
                    unsigned int * unpackedArray, unsigned int numRegisters); 
                   

#endif // UDR_UTIL_H
