
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Upload Dialog Box
//

// Libraries

#pragma	comment(lib, "lz32.lib")

// Runtime Class

AfxImplementRuntimeClass(CLinkUploadDialog, CStdDialog);

//

BOOL DLLAPI Et3Link_CanUpload(void)
{
	return TRUE;
	}

BOOL DLLAPI Et3Link_Upload(CDatabase *pDbase, CFilename &File)
{
	CLinkUploadDialog Dlg(pDbase);

	if( Dlg.Execute(*afxMainWnd) ) {

		File = Dlg.GetFilename();

		return TRUE;
		}

	return FALSE;
	}

// Constructors

CLinkUploadDialog::CLinkUploadDialog(CDatabase *pDbase)
{
	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fAutoClose = FALSE;

	m_pDbase     = pDbase;

	m_pComms     = New CCommsThread(pDbase);

	SetName(L"LinkUploadDlg");
	}

// Destructor

CLinkUploadDialog::~CLinkUploadDialog(void)
{
	delete m_pComms;
	}

// Attributes

CString CLinkUploadDialog::GetFilename(void) const
{
	return m_File2;
	}

// Message Map

AfxMessageMap(CLinkUploadDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOkay)
	AfxDispatchCommand(IDINIT,   OnCommsInit  )
 	AfxDispatchCommand(IDDONE,   OnCommsDone  )
 	AfxDispatchCommand(IDFAIL,   OnCommsFail  )

	AfxMessageEnd(CLinkUploadDialog)
	};

// Message Handlers

BOOL CLinkUploadDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_uState = stateReadImage;

	if( !OpenFile() ) {

		SetError(CString(IDS_UNABLE_TO_OPEN_2));

		return TRUE;
		}

	m_pComms->Bind(this);

	m_pComms->Create();

	return TRUE;
	}

// Command Handlers

BOOL CLinkUploadDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		if( TRUE || NoYes(CString(IDS_ABORT)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_pComms->Terminate(2000);

			EndDialog(!m_fError);
			}
		}
	
	else
		EndDialog(FALSE);
	
	CloseFile();

	DeleteFile(m_File1);
	
	return TRUE;
	}

BOOL CLinkUploadDialog::OnCommsInit(UINT uID)
{
	TxFrame();

	return TRUE;
	}

BOOL CLinkUploadDialog::OnCommsDone(UINT uID)
{
	if( RxFrame() ) {
		
		TxFrame();
		}

	return TRUE;
	}

BOOL CLinkUploadDialog::OnCommsFail(UINT uID)
{
	SetError(m_pComms->GetErrorText());

	return TRUE;
	}

// Implementation

void CLinkUploadDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_pComms->Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			CloseFile();
			
			DeleteFile(m_File1);

			EndDialog(TRUE);

			return;
			}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_CLOSE));
		}
	}

void CLinkUploadDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf("ERROR - %s.", pText));

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
	}

void CLinkUploadDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
	}

void CLinkUploadDialog::TxFrame(void)
{
	if( m_uState == stateReadImage ) {

		m_pComms->ReadImage();
		
		return;
		}

	ShowStatus(CString(IDS_OPERATION));

	CloseFile();

	SaveFile();
		
	SetDone(FALSE);
	}

BOOL CLinkUploadDialog::RxFrame(void)
{
	if( m_uState == stateReadImage ) {

		if( m_pComms->CopyImageIntoFile(m_hFile) ) {

			m_uState = stateDone;
			
			return TRUE;
			}

		SetError(CString(IDS_FAILED_TO_READ));
		
		return FALSE;
		}
	
	SetError(CString(IDS_LINK_ENTERED));

	return FALSE;
	}

BOOL CLinkUploadDialog::OpenFile(void)
{
	m_File1.MakeTemporary();

	m_hFile = m_File1.OpenWrite();

	return m_hFile < INVALID_HANDLE_VALUE;
	}

BOOL CLinkUploadDialog::CloseFile(void)
{
	if( m_hFile < INVALID_HANDLE_VALUE ) {

		CloseHandle(m_hFile);

		m_hFile = INVALID_HANDLE_VALUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CLinkUploadDialog::SaveFile(void)
{
	for(;;) {

		CSaveFileDialog Dialog;

		Dialog.SetCaption(CString(IDS_SAVE_UPLOADED));

		Dialog.SetFilter (CString(IDS_CRIMSON_DATABASE));

		if( Dialog.Execute(ThisObject) ) {

			m_File2 = Dialog.GetFilename();

			switch( CanOverwrite(m_File2) ) {

				case IDNO:

					continue;

				case IDCANCEL:

					m_File2.Empty();

					return FALSE;
				}

			afxMainWnd->UpdateWindow();

			if( m_File2.Exists() ) {
				
				if( !DeleteFile(m_File2) ) {

					Error(CString(IDS_UNABLE_TO_WRITE));

					continue;
					}

				afxMainWnd->UpdateWindow();
				}

			if( !MoveFile(m_File1, m_File2) ) {

				Error(CString(IDS_UNABLE_TO_WRITE));

				continue;
				}

			return TRUE;
			}

		m_File2.Empty();

		return FALSE;
		}
	}

// End of File
