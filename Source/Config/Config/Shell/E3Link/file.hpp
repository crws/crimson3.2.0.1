
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Download Link
//
// Copyright (c) 1993-2015 Red Lion Controls Inc. Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// File Sys Wrapper
//

class CFileSystem
{
	public:
		// Constructor
		CFileSystem(void);

		// Destructor
		~CFileSystem(void);

		// Management
		void Bind(IFileData *pData);
		void SetConfig(PCSTR pConfig, char cFormat);

		// Attributes
		UINT GetErrorCode(void);
		INT  GetResultCode(void);

		// Operations
		BOOL Read(void);
		BOOL Write(void);
		BOOL Create(void);
		BOOL Delete(void);
		BOOL DeletePath(void);

	protected:
		// Data
		PSTR		m_pIP;
		UINT		m_uTimeout;
		UINT		m_uError;
		INT		m_nResult;
		UINT		m_uAlias;
		IFileData     * m_pData;		
		UINT		m_uLimit;
		char		m_Format;
		PCSTR		m_pConfig;
		INT		m_nSeq;
		HANDLE		m_hHandle;		

		// Implementation
		BOOL OpenComm(PCSTR pConfig);
		void CloseComm(void);
		BOOL GetAlias(void);
		BOOL Read (PBYTE pData, UINT uPosn, WORD wSize);
		BOOL Write(PBYTE pData, UINT uPosn, WORD wSize);
		BOOL Create(PCSTR pName, UINT uSize);
		BOOL Delete(PCSTR pName);
		BOOL Stat(PCSTR pName, UINT &uSize);
		BOOL CloseAlias(void);

	public:
		// Debug
		PCTXT EnumResult(int nResult);
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

class CFileSysData : public IFileData
{
	public:
		// Constructor
		CFileSysData(PCSTR pName, UINT uSize);

		// Destructor
		~CFileSysData(void);

		// IFileData
		void    Release(void);
		PCSTR	GetFile(void);
		BOOL	IsValid(void);
		void	SetValid(BOOL fValid);
		void    Create(UINT uSize);
		PBYTE	GetDataBuffer(void);
		UINT	GetDataSize(void);

		BYTE	GetByte(UINT &uPtr);
		WORD	GetWord(UINT &uPtr);
		DWORD	GetLong(UINT &uPtr);
		PCBYTE	GetData(UINT &uPtr, UINT uSize);
		CString	GetText(UINT &uPtr, UINT uSize);
		
		void	PutByte(UINT &uPtr, BYTE Data);
		void	PutWord(UINT &uPtr, WORD Data);
		void	PutLong(UINT &uPtr, DWORD Data);
		void	PutData(UINT &uPtr, PCBYTE pData, UINT uSize);
		void    PutText(UINT &uPtr, CString pData, UINT uSize);

		void	Dump(void);

	protected:
		// Data
		PCSTR		m_pName;
		PBYTE		m_pData;
		UINT		m_uSize;
		BOOL		m_fValid;
	};

// End of File
