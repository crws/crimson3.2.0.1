
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Download Link
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "e3link.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Version Data
//

#include "version.rcc"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#include "e3link.loc"
				      
//////////////////////////////////////////////////////////////////////////
//
// String Table
//

STRINGTABLE
BEGIN
	IDS_ABORT               "&Abort"
	IDS_CHECKING_DEVICE     "Checking device model."
	IDS_CHECKING_DEVICE_2   "Checking device serial number"
	IDS_CHECKING_DEVICE_3   "Checking device Ethernet jumper configuration."
	IDS_CHECKING_DEVICE_4   "Checking device Source/Sink jumper configuration."
	IDS_CHECKING_MODULE     "Checking module firmware revision"
	IDS_CHECKING_MODULE_2   "Checking module firmware"
	IDS_CLOSE               "&Close"
	IDS_COULD_NOT_GET       "Could not get target device serial number"
	IDS_CRIMSON_DATABASE    "Crimson 3.1 Database (*.cd31)|*.cd31"
	IDS_DATABASE_IN         "The database in the target device does not match the currently\nloaded configuration.\n\nDo you want to update the unit with the current database?"
	IDS_DEVICE_DOES_NOT     "The device does not match the current database."
	IDS_DONE_DEVICE         "DONE - The device matches the currently loaded database."
	IDS_E3_LINK_CHECK       "Checking EtherTrak3 Item" /* NOT USED */
	IDS_E3_LINK_END         "Ending EtherTrak3 download" /* NOT USED */
	IDS_E3_LINK_START       "Starting EtherTrak3 download" /* NOT USED */
	IDS_E3_LINK_START_SYS   "Starting EtherTrak3 system" /* NOT USED */
	IDS_E3_LINK_WRITE       "Writing EtherTrak3 Item" /* NOT USED */
	IDS_FAILED_TO_CREATE    "Failed to create transport"
	IDS_FAILED_TO_READ      "Failed to read the image."
	IDS_FAILED_TO_RESET     "Failed to reset station"
	IDS_FAILED_TO_SEND      "Failed to send vers command."
	IDS_FAILED_TO_VERIFY    "Failed to verify the firmware in the target device"
	IDS_LINK_ENTERED        "The link entered an invalid state."
	IDS_MORE_THAN_ONE_E     "More than one E3 device found via USB. Ensure that only one E3 device is connected via USB and restart the operation"
	IDS_NNDO_YOU_WANT_TO    "\n\nDo you want to continue with this operation?"
	IDS_NO_E_DEVICES        "No E3 devices detected via USB"
	IDS_OPERATION           "The operation completed without error."
	IDS_PREPARING           "Preparing database image."
	IDS_RESETTING_UNIT      "Resetting Unit."
	IDS_SAVE_UPLOADED       "Save Uploaded Image"
	IDS_TARGET_DEVICE       "The target device could not be identified"
	IDS_TARGET_DEVICES      "The target device's Ethernet mode jumper (%s) is incompatible with the value configured in the database (%s)"
	IDS_TARGET_DEVICES_2    "The target device's Source/Sinking mode jumper (%s) is incompatible with the value configured in the database (%s)"
	IDS_TARGET_DEVICE_2     "Target device firmware revision (%1!4.4X!) requires upgrade to (%2!4.4X!)"
	IDS_TARGET_DEVICE_3     "Target device serial number (%1!u!) does not match the value configured in the database."
	IDS_TARGET_DEVICE_4     "The target device jumper settings could not be read"
	IDS_TARGET_DEVICE_IS    "The target device is not compatible with this file"
	IDS_UNABLE_TO           "Unable to initialize transport."
	IDS_UNABLE_TO_OPEN      "Unable to open the firmware file"
	IDS_UNABLE_TO_OPEN_2    "Unable to open temporary file."
	IDS_UNABLE_TO_OPEN_3    "Unable to open communications port"
	IDS_UNABLE_TO_READ      "Unable to read the firmware revision from the target device"
	IDS_UNABLE_TO_WRITE     "Unable to write to the selected file."
	IDS_UPGRADING_MODULE    "Upgrading module firmware"
	IDS_VERIFY_DATABASE     "Verify Database"
	IDS_VIA                 " via "
END

//////////////////////////////////////////////////////////////////////////
//
// Link Send Dialog Box
//

LinkSendDlg DIALOG 0, 0, 0, 0
CAPTION "Download Database"
BEGIN
	GROUPBOX	"Status", 0, 4, 4, 180, 64
	LTEXT		"Opening transport layer.", 100, 10, 25, 168, 30
	DEFPUSHBUTTON   "&Abort", IDOK, 4, 72, 40, 14, XS_BUTTONFIRST
END

//////////////////////////////////////////////////////////////////////////
//
// Link Upload Dialog Box
//

LinkUploadDlg DIALOG 0, 0, 0, 0
CAPTION "Extract Image"
BEGIN
	GROUPBOX	"&Status", 0, 4, 4, 180, 64
	LTEXT		"Opening transport layer.", 100, 10, 25, 168, 40
	DEFPUSHBUTTON   "&Abort", IDOK, 4, 72, 40, 14, XS_BUTTONFIRST
END	

//////////////////////////////////////////////////////////////////////////
//								
// Link Options Dialog
//

LinkOptionsDlg DIALOG 0, 0, 0, 0
CAPTION "Link Options"
BEGIN
	GROUPBOX	"Communications Port",-1, 4, 4, 146, 44, WS_GROUP
	RADIOBUTTON	" &TCP/IP",     1102, 10,  14, 30, 14, XS_RADIOREST
	RADIOBUTTON	" &COM ",       1101, 10,  28, 30, 14, XS_RADIOREST
	EDITTEXT		        2000, 42,  29, 20, 12, WS_BORDER | WS_TABSTOP | ES_NUMBER
	
	DEFPUSHBUTTON   "OK",          IDOK,  4, 54, 40, 14, XS_BUTTONFIRST
	PUSHBUTTON      "Cancel",  IDCANCEL, 48, 54, 40, 14, XS_BUTTONREST
END

//////////////////////////////////////////////////////////////////////////
//								
// Link Options Dialog
//

LinkOptionsDlg1 DIALOG 0, 0, 0, 0
CAPTION "Link Options"
BEGIN
	GROUPBOX	"Communications Port",-1, 4, 4, 146, 58, WS_GROUP
	RADIOBUTTON	" &TCP/IP",     1102, 10,  14, 30, 14, XS_RADIOREST
	RADIOBUTTON	" &USB ",       1100, 10,  28, 30, 14, XS_RADIOREST
	RADIOBUTTON	" &COM ",       1101, 10,  42, 30, 14, XS_RADIOREST
	EDITTEXT		        2000, 42,  43, 20, 12, WS_BORDER | WS_TABSTOP | ES_NUMBER
	
	DEFPUSHBUTTON   "OK",          IDOK,  4, 68, 40, 14, XS_BUTTONFIRST
	PUSHBUTTON      "Cancel",  IDCANCEL, 48, 68, 40, 14, XS_BUTTONREST
END

// End of File
