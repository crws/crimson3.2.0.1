
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Not supported
//

#define	m_fEnableUSB	TRUE

#define	m_fShowSlow	FALSE

//////////////////////////////////////////////////////////////////////////
//
// Link Options Dialog
//

class CLinkOptionsDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CLinkOptionsDialog(CDatabase *pDbase);
		
	protected:
		// Data Members
		CDatabase * m_pDbase;
		UINT        m_uMode;
		UINT        m_uLast;
		BOOL        m_fSlow;
		UINT        m_uPort;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnSelect(UINT uID);

		// Implementation
		void LoadData(void);
		void SaveData(void);
		void LoadDialog(void);
		void ReadDialog(void);
		void DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Link Options Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CLinkOptionsDialog, CStdDialog);

// External API

BOOL DLLAPI Et3Link_SetOptions(CDatabase *pDbase)
{
	CLinkOptionsDialog Dlg(pDbase);

	return Dlg.Execute(*afxMainWnd);
	}

// Constructors

CLinkOptionsDialog::CLinkOptionsDialog(CDatabase *pDbase)
{
	m_pDbase = pDbase;

	SetName(m_fEnableUSB ? L"LinkOptionsDlg1" : L"LinkOptionsDlg");
	}

// Message Map

AfxMessageMap(CLinkOptionsDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)
	AfxDispatchCommand(1100,     OnSelect)
	AfxDispatchCommand(1101,     OnSelect)
	AfxDispatchCommand(1102,     OnSelect)

	AfxMessageEnd(CLinkOptionsDialog)
	};

// Message Handlers

BOOL CLinkOptionsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadData();

	LoadDialog();

	DoEnables();

	return TRUE;
	}

// Command Handlers

BOOL CLinkOptionsDialog::OnCommandOK(UINT uID)
{
	ReadDialog();

	SaveData();

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CLinkOptionsDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CLinkOptionsDialog::OnSelect(UINT uID)
{
	DoEnables();

	return TRUE;
	}

// Implementation

void CLinkOptionsDialog::LoadData(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"E3Link");

	Reg.MoveTo(L"Category"); 

	Reg.MoveTo(m_pDbase->GetDownloadConfig());

	UINT uMethod = Reg.GetValue(L"Method", 0U);

	switch( uMethod ) {

		case 0:
			m_uMode = 0;

			m_uPort = Reg.GetValue(L"COM", 5U);
			
			break;

		case 1:
			m_uMode = 1;

			m_uPort = Reg.GetValue(L"COM", 5U);

			break;

		case 2:
			m_uMode = 2;

			m_fSlow = Reg.GetValue(L"Slow", 0U);

			m_uPort = Reg.GetValue(L"COM", 5U);

			break;
		}

	m_uLast = m_uMode;		
	}

void CLinkOptionsDialog::SaveData(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"E3Link");

	Reg.MoveTo(L"Category");

	Reg.MoveTo(m_pDbase->GetDownloadConfig());

	switch( m_uMode ) {

		case 0:
			Reg.SetValue(L"Method", 0LU);

			break;

		case 1:
			Reg.SetValue(L"Method", 1LU);
			
			Reg.SetValue(L"COM", m_uPort);
			
			break;

		case 2:
			Reg.SetValue(L"Method", 2LU);

			Reg.SetValue(L"Slow", m_fSlow);

			Reg.SetValue(L"COM", m_uPort);

			break;
		}
	}

void CLinkOptionsDialog::LoadDialog(void)
{
	SetRadioGroup(1100, 1102, m_uMode);	

	GetDlgItem(2000).SetWindowText(CPrintf(L"%u", m_uPort));

	((CButton &) GetDlgItem(2001)).SetCheck(m_fSlow);
	}

void CLinkOptionsDialog::ReadDialog(void)
{
	m_uMode = GetRadioGroup(1100, 1102);

	m_uPort = watoi(GetDlgItem(2000).GetWindowText());

	m_fSlow = ((CButton &) GetDlgItem(2001)).IsChecked();
	}

void CLinkOptionsDialog::DoEnables(void)
{
	GetDlgItem(1100).EnableWindow(m_fEnableUSB);
	GetDlgItem(1101).EnableWindow(TRUE);
	GetDlgItem(1102).EnableWindow(TRUE);

	GetDlgItem(2000).EnableWindow(GetRadioGroup(1100, 1102) == 1);
	GetDlgItem(2001).EnableWindow(GetRadioGroup(1100, 1102) == 2);

	GetDlgItem(2001).ShowWindow  (m_fShowSlow);
	}

// End of File
