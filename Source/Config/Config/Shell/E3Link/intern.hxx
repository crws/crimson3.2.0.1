
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Download Link
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_ABORT               0x4000
#define IDS_CHECKING_DEVICE     0x4001
#define IDS_CHECKING_DEVICE_2   0x4002
#define IDS_CHECKING_DEVICE_3   0x4003
#define IDS_CHECKING_DEVICE_4   0x4004
#define IDS_CHECKING_MODULE     0x4005
#define IDS_CHECKING_MODULE_2   0x4006
#define IDS_CLOSE               0x4007
#define IDS_COULD_NOT_GET       0x4008
#define IDS_CRIMSON_DATABASE    0x4009
#define IDS_DATABASE_IN         0x400A
#define IDS_DEVICE_DOES_NOT     0x400B
#define IDS_DONE_DEVICE         0x400C
#define IDS_E3_LINK_CHECK       0x400D /* NOT USED */
#define IDS_E3_LINK_END         0x400E /* NOT USED */
#define IDS_E3_LINK_START       0x400F /* NOT USED */
#define IDS_E3_LINK_START_SYS   0x4010 /* NOT USED */
#define IDS_E3_LINK_WRITE       0x4011 /* NOT USED */
#define IDS_FAILED_TO_CREATE    0x4012
#define IDS_FAILED_TO_READ      0x4013
#define IDS_FAILED_TO_RESET     0x4014
#define IDS_FAILED_TO_SEND      0x4015
#define IDS_FAILED_TO_VERIFY    0x4016
#define IDS_LINK_ENTERED        0x4017
#define IDS_MORE_THAN_ONE_E     0x4018
#define IDS_NNDO_YOU_WANT_TO    0x4019
#define IDS_NO_E_DEVICES        0x401A
#define IDS_OPERATION           0x401B
#define IDS_PREPARING           0x401C
#define IDS_RESETTING_UNIT      0x401D
#define IDS_SAVE_UPLOADED       0x401E
#define IDS_TARGET_DEVICE       0x401F
#define IDS_TARGET_DEVICES      0x4020
#define IDS_TARGET_DEVICES_2    0x4021
#define IDS_TARGET_DEVICE_2     0x4022
#define IDS_TARGET_DEVICE_3     0x4023
#define IDS_TARGET_DEVICE_4     0x4024
#define IDS_TARGET_DEVICE_IS    0x4025
#define IDS_UNABLE_TO           0x4026
#define IDS_UNABLE_TO_OPEN      0x4027
#define IDS_UNABLE_TO_OPEN_2    0x4028
#define IDS_UNABLE_TO_OPEN_3    0x4029
#define IDS_UNABLE_TO_READ      0x402A
#define IDS_UNABLE_TO_WRITE     0x402B
#define IDS_UPGRADING_MODULE    0x402C
#define IDS_VERIFY_DATABASE     0x402D
#define IDS_VIA                 0x402E

// End of File

#endif
