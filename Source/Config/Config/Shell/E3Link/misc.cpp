
#include "intern.hpp"

#include "imports\udr.h"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Gateway Wrapper
//

// Constructor

CVers::CVers(PCSTR pConfig, char cFormat)
{
	m_pConfig  = pConfig;

	m_Format   = cFormat;

	m_uTimeout = 2000;

	m_nSeq     = 0;
	}
// Constructor

CVers::~CVers(void)
{
	}

// Operations

BOOL CVers::Send(void)
{
	INT nProductCode;
	INT nMajorVersion;
	INT nMinorVersion;

	INT nResult = vers( PVOID(m_pConfig),
			    m_uTimeout,
			    m_nSeq++,
			    m_Format,
			    &nProductCode,
			    &nMajorVersion,
			    &nMinorVersion, 
			    NULL, 
			    NULL, 
			    0, 
			    NULL
			    );

	return nResult == UDR_SUCCESS;
	}

// End of File
