
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Download Link
//
// Copyright (c) 1993-2017 Red Lion Controls Inc. Inc.
//
// All Rights Reserved
//

interface ILinkTransport
{
	public:
		// Deletion
		virtual void Release(void)	 = 0;

		// Management
		virtual BOOL Open(void)		= 0;
		virtual void Close(void)	= 0;
		virtual void Terminate(void)	= 0;

		// Attributes
		virtual CString GetErrorText(void) const	= 0;

		// Transport
		virtual BOOL Transact(IFileData *pData, UINT uCode)	= 0;
		virtual BOOL ResetStation(void)				= 0;

		// Notifications
		virtual void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)	= 0;
		virtual void OnBind(HWND hWnd)					= 0;
	};

// End of File
