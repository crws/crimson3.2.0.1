
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Download Link
//
// Copyright (c) 1993-2015 Red Lion Controls Inc. Inc.
//
// All Rights Reserved
//

#include "e3link.hpp"

#include "intern.hxx"

///////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <iphlpapi.h>

#include <shlobj.h>

#include "E3Master.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma	comment(lib, "iphlpapi")

#pragma comment(lib, "shell32")

#pragma comment(lib, "winmm.lib")

///////////////////////////////////////////////////////////////////////
//
// Byte Order Conversions
//

inline	WORD	HostToMotor(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
	}

inline	WORD	MotorToHost(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
	}

inline	DWORD	HostToMotor(DWORD Data)
{
	DWORD dwHi = HostToMotor(WORD(Data >> 16));

	DWORD dwLo = HostToMotor(WORD(Data)) << 16;

	return dwHi | dwLo;
	}

inline	DWORD	MotorToHost(DWORD Data)
{
	DWORD dwHi = HostToMotor(WORD(Data >> 16));

	DWORD dwLo = HostToMotor(WORD(Data)) << 16;

	return dwHi | dwLo;
	}

//////////////////////////////////////////////////////////////////////////
//
// Comms Commands
//

#define	IDINIT	500
#define	IDDONE	501
#define	IDFAIL	502
#define	IDKILL	503

//////////////////////////////////////////////////////////////////////////
//
// G3 Configuration Support
//

class CConfigLoader
{
	public:
		// Constructor
		CConfigLoader(void);

		// Attributes
		CString GetErrorText(void) const;

		// Attributes
		BOOL GetBaseModel(UINT &uBase, UINT &uModule);
		BOOL GetModuleFirmwareRevision(UINT &uRevision);
		BOOL GetBaseNetModeJumper(UINT &uMode);
		BOOL GetBaseSourceSinkJumper(UINT &uMode);
		BOOL GetBaseSerialNumber(UINT &uSerialNumber);
		BOOL CopyImageIntoFile(HANDLE hFile);

		// Operations
		BOOL ReadFirmwareRevision(void);
		BOOL CheckBaseModel(void);
		BOOL ReadCfgSetup(void);
		BOOL ReadImage(void);
		BOOL ReadFile(IFileData *pData);
		BOOL WriteFile(IFileData *pData);
		BOOL ClearFile(IFileData *pData);
		BOOL PrepareItem(void);
		BOOL ResetStation(void);

		// Binding
		void Bind(ILinkTransport *pTrans);

		// Comms Operations
		BOOL Transact(void);

	protected:
		enum {
			codeIdle,
			codeReadFile,
			codeWriteFile,
			codeClearFile,
			codeReadImage,
			codeSendReset,
			codePrepare,
			};

		// Data
		CString		 m_Error;
		UINT		 m_uCode;
		ILinkTransport * m_pTrans;
		IFileData      * m_pData;

		// Files
		CFileDataBaseArcFactory		m_BaseArcFactory;
		CFileDataModuleArcFactory	m_ModuleArcFactory;
		CFileDataBaseCfgSetup		m_BaseCfgSetup;
		CFileDataBaseTmpImage		m_BaseTmpImage;
		CFileDataBaseFirmware		m_BaseFirmware;
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Thread
//

class CCommsThread : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCommsThread(CDatabase *pDbase);

		// Destructor
		~CCommsThread(void);

		// Management
		void Bind(CWnd *pWnd);

		// Attributes
		CString GetErrorText(void) const;
		BOOL	GetBaseModel(UINT &uBase, UINT &uModule);
		BOOL	GetBaseSerialNumber(UINT &uSerialNumber);
		BOOL	GetBaseNetModeJumper(UINT &uMode);
		BOOL	GetBaseSourceSinkJumper(UINT &uMode);
		BOOL	CopyImageIntoFile(HANDLE hFile);
		BOOL	GetModuleFirmwareRevision(UINT &uVersion);

		// Configuration
		BOOL ReadFirmwareRevision(void);
		BOOL CheckBaseModel(void);
		BOOL ReadCfgSetup(void);
		BOOL ReadImage(void);
		BOOL ReadFile(IFileData *pData);
		BOOL PrepareItem(void);
		BOOL WriteFile(IFileData *pData);
		BOOL ClearFile(IFileData *pData);
		BOOL ResetStation(void);

		// Events
		void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);

	protected:
		// Data Members
		CDatabase        * m_pDbase;
		CEvent		   m_CommEvent;
		CWnd	         * m_pWnd;
		CString		   m_Error;
		CConfigLoader    * m_pService;
		ILinkTransport	 * m_pTrans;
		CString		   m_Method;
		CString		   m_Name;
		CFileData	   m_Firm;

		// Overridables
		BOOL OnInit(void);
		UINT OnExec(void);
		void OnTerm(void);

		// Implementation
		void Transact(void);
		BOOL CreateTransport(void);
		BOOL InitTransport(void);
		void LoadConfig(void);
		UINT FindUsbComPort(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Gateway Wrapper
//

class CGateway
{
	public:
		// Constructor
		CGateway(PCSTR pConfig, char cFormat);

		// Destructor
		~CGateway(void);

		// Operations
		BOOL Reset(BOOL fHard);

	protected:
		// Data
		UINT	m_uTimeout;		
		char	m_Format;
		PCSTR	m_pConfig;
		INT	m_nSeq;
	};

//////////////////////////////////////////////////////////////////////////
//
// Vers Wrapper
//

class CVers
{
	public:
		// Constructor
		CVers(PCSTR pConfig, char cFormat);

		// Destructor
		~CVers(void);

		// Operations
		BOOL Send(void);

	protected:
		// Data
		UINT	m_uTimeout;
		char	m_Format;
		PCSTR	m_pConfig;
		INT	m_nSeq;
	};

//////////////////////////////////////////////////////////////////////////
//
// Wide to Ansi String Conversion
//

extern LPSTR WideToAnsi(PCTXT pText);
extern LPSTR WidetoAnsi(CString Text);

//////////////////////////////////////////////////////////////////////////
//
// Ansi String Class
//

class CAnsiString
{
	public:
		// Constructor
		CAnsiString(PCTXT pText);

		// Destructor
		~CAnsiString(void);

		// Attributes
		UINT GetLength(void);

		// Conversion
		operator LPCSTR (void) const;

	protected:
		// Data
		LPSTR	m_pText;
	};

//////////////////////////////////////////////////////////////////////////
//
// Send Options
//

enum
{
	sendSend,
	sendUpdate,
	sendVerify,
	};

//////////////////////////////////////////////////////////////////////////
//
// Download Dialog Box
//

class CLinkSendDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CLinkSendDialog(CDatabase *pDbase, UINT uSend);

		// Destructor
		~CLinkSendDialog(void);

	public:
		// State Machine
		enum {
			stateCheckModel,
			stateCheckJumper1,
			stateCheckJumper2,
			stateReadConfig,
			statePrepareItem,
			stateWriteConfig,
			stateCheckConfig,
			stateWriteImage,
			stateCheckSerial,
			stateCheckRevision,
			stateWriteFirmware,
			stateCheckFirmware,
			stateResetStation,
			stateDone,
			};

		// Data Members
		CDatabase		* m_pDbase;
		UINT			  m_uState;
		UINT			  m_uSend;
		BOOL			  m_fDone;
		BOOL			  m_fError;
		BOOL			  m_fVerify;
		BOOL			  m_fChanges;
		CCommsThread		* m_pComms;
		INDEX			  m_Index;
		UINT			  m_uItem;
		UINT			  m_uSize;
		CItem			* m_pItem;
		CEt3CommsSystem		* m_pSystem;
		UINT			  m_uFile;
		IFileData		* m_pFile;
		CInitData		  m_Init;
		CFileDataBaseTmpImage	  m_Image;
		FILE			* m_pFirm;
		DWORD			  m_dwSize;
		CFileDataBaseFirmware     m_Firm;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnCommsInit(UINT uID);
		BOOL OnCommsDone(UINT uID);
		BOOL OnCommsFail(UINT uID);

		// Implementation
		void  SetDone(BOOL fError);
		void  SetError(PCTXT pText);
		void  ShowStatus(PCTXT pText);
		BOOL  GetContinue(PCTXT pText);
		void  TxFrame(void);
		BOOL  RxFrame(void);
		BOOL  Verify(void);
		void  Compare(IFileData *pFile, UINT uSize);
		PCTXT EnumNetModeJumper(UINT uMode);
		PCTXT EnumSourceSinkJumper(UINT uMode);
		BOOL  OpenFirmware(void);
		BOOL  OpenFile(PCTXT pFile);
		void  CloseFile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Upload Dialog Box
//

class CLinkUploadDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CLinkUploadDialog(CDatabase *pDbase);

		// Destructor
		~CLinkUploadDialog(void);

		// Attributes
		CString GetFilename(void) const;
	
	public:
		// States
		enum {
			stateReadImage,
			stateDone,
			};

		// Data Members
		CCommsThread		* m_pComms;
		BOOL		          m_fDone;
		BOOL		          m_fError;
		UINT		          m_uState;
		CDatabase		* m_pDbase;
		CFilename	          m_File1;
		CFilename	          m_File2;
		HANDLE		          m_hFile;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnCommsInit(UINT uID);
		BOOL OnCommsDone(UINT uID);
		BOOL OnCommsFail(UINT uID);

		// Implementation
		void SetDone(BOOL fError);
		void SetError(PCTXT pText);
		void ShowStatus(PCTXT pText);
		void TxFrame(void);
		BOOL RxFrame(void);
		BOOL OpenFile(void);
		BOOL CloseFile(void);
		BOOL SaveFile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Development Flags
//

#define	m_fTest		FALSE

// End of File
