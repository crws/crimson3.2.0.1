
#include "intern.hpp"

#include "file.hpp"

#include "imports\udr.h"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Required Libraries
//

#pragma	comment(lib, "ws2_32.lib")

//////////////////////////////////////////////////////////////////////////
//
// Eth3 Link Network Transport
//

class CUdrTransport : public ILinkTransport
{
	public:
		// Constructor
		CUdrTransport(DWORD IP, UINT uPort);
		CUdrTransport(UINT uPort, UINT uBaud);

		// Deletion
		void Release(void);

		// Management
		BOOL Open(void);
		void Close(void);
		void Terminate(void);

		// Attributes
		CString GetErrorText(void) const;

		// Transport
		BOOL Transact(IFileData *pData, UINT uCode);
		BOOL ResetStation(void);

		// Notifications
		void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
		void OnBind(HWND hWnd);

	protected:
		// Data
		DWORD		m_IP;
		UINT		m_uPort;
		BOOL		m_fOpen;
		CString		m_Error;
		CFileSystem	m_File;
		IFileData     * m_pData;
		BOOL		m_fRead;
		BOOL		m_fUSB;
		BOOL		m_uBaud;
		PCSTR		m_pConfig;
		char		m_Format;
		HANDLE		m_hHandle;
		BOOL		m_fHuman;

		// Implementation
		CString BuildAddr(void);
		CString BuildComm(void);
		void    BuildError(PCTXT pOperation);
	};

//////////////////////////////////////////////////////////////////////////
//
// Eth3 Link Network Transport
//

// Instantiator

ILinkTransport * Create_UdrTransport(DWORD IP, UINT uPort)
{
	return New CUdrTransport(IP, uPort);
	}

ILinkTransport * Create_UdrTransport(UINT uPort, UINT uBaud)
{
	return New CUdrTransport(uPort, uBaud);
	}

// Constructor

CUdrTransport::CUdrTransport(DWORD IP, UINT uPort)
{
	m_IP      = IP;

	m_uPort   = uPort;

	m_fOpen   = FALSE;

	m_fUSB    = FALSE;

	m_pConfig = NULL;

	m_fHuman  = FALSE;
	}

CUdrTransport::CUdrTransport(UINT uPort, UINT uBaud)
{
	m_uPort   = uPort;

	m_uBaud   = uBaud;

	m_fOpen   = FALSE;

	m_fUSB    = TRUE;

	m_pConfig = NULL;

	m_fHuman  = FALSE;
	}

// Deletion

void CUdrTransport::Release(void)
{
	if( m_pConfig ) {

		delete m_pConfig;

		m_pConfig = NULL;
		}

	delete this;
	}

// Management

BOOL CUdrTransport::Open(void)
{
	if( m_fUSB ) {

		CAnsiString Config(BuildComm());

		m_hHandle    = system_open_comm(Config);

		m_pConfig    = WideToAnsi(CPrintf(L"%d", m_hHandle));

		m_Format     = BIN_LEADER;

		m_File.SetConfig(m_pConfig, m_Format);

		m_fOpen = TRUE;

		return TRUE;
		}
	else {
		WORD Version = MAKEWORD(2, 0);

		WSADATA Data;

		if( WSAStartup(Version, &Data) != 0 ) {

			m_fOpen = FALSE;

			m_Error = CString(IDS_UNABLE_TO_OPEN_3);
			
			return FALSE;
			}

		m_pConfig = WideToAnsi(BuildAddr());

		m_Format  = NOCRC_LEADER;

		m_File.SetConfig(m_pConfig, m_Format);

		m_fOpen = TRUE;

		return TRUE;
		}
	}

void CUdrTransport::Close(void)
{
	if( m_fUSB ) {

		if( m_fOpen ) {
			
			system_close_comm(m_hHandle);
			}
		}
	else {
		if( m_fOpen ) {

			WSACleanup();
			}
		}
	}

void CUdrTransport::Terminate(void)
{
	// TODO -- implement
	}

// Attributes

CString CUdrTransport::GetErrorText(void) const
{
	return m_Error;
	}

// Transport

BOOL CUdrTransport::Transact(IFileData *pData, UINT uCode)
{
	m_File.Bind(pData);

	if( uCode == 0 ) {

		// clear

		if( m_File.DeletePath() ) {

			if( pData->GetDataSize() ) {

				CString Name = CString(pData->GetFile());

				if( m_File.Create() ) {

					for( UINT n = 0; n < 100; n ++ ) {

						if( m_File.Write() ) {							
					
							return TRUE;
							}
						}					

					m_Error = CPrintf(L"Failed to write file %s", Name);
				
					return FALSE;
					}

				m_Error = CPrintf(L"Failed to create file %s", Name);

				return FALSE;
				}

			return TRUE;
			}

		m_Error = L"Failed to delete path";
		
		return FALSE;
		}

	if( uCode == 1 ) {

		// read image

		if( m_File.Read() ) {
			
			return TRUE;
			}
		
		BuildError(L"Image Read");
		
		return FALSE;
		}

	if( uCode == 2 ) {

		if( m_File.Read() ) {
			
			return TRUE;
			}

		BuildError(L"Read");

		return FALSE;
		}

	if( uCode == 3 ) {

		if( m_File.Write() ) {
			
			return TRUE;
			} 

		BuildError(L"Write");

		return FALSE;
		}

	return FALSE;
	}

BOOL CUdrTransport::ResetStation(void)
{
	CGateway Gate(m_pConfig, m_Format);
	
	if( Gate.Reset(TRUE) ) {

		CVers Vers(m_pConfig, m_Format);
	
		for( UINT n = 0; n < 30; n ++ ) {

			if( Vers.Send() ) {
				
				return TRUE;
				}

			Sleep(10);
			}

		m_Error = CString(IDS_FAILED_TO_SEND);

		return FALSE;
		}

	m_Error = CString(IDS_FAILED_TO_RESET);

	return FALSE;
	}

// Notifications

void CUdrTransport::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	}

void CUdrTransport::OnBind(HWND hWnd)
{
	}

// Implementation

CString CUdrTransport::BuildAddr(void)
{
	BYTE b1 = PBYTE(&m_IP)[0];
	BYTE b2 = PBYTE(&m_IP)[1];
	BYTE b3 = PBYTE(&m_IP)[2];
	BYTE b4 = PBYTE(&m_IP)[3];

	return CPrintf("%u.%u.%u.%u", b4, b3, b2, b1);
	}

CString CUdrTransport::BuildComm(void)
{
	return CPrintf("COM%u", m_uPort);
	}

void CUdrTransport::BuildError(PCTXT pName)
{
	if( m_fHuman ) {

		m_Error = CPrintf( L"%s operation failed - %s",
				   pName,
				   m_File.EnumResult(m_File.GetResultCode())
				   );
		}
	else {
		m_Error = CPrintf( L"%s operation failed with error code %d, return %d (%s)", 
				   pName, 
				   m_File.GetErrorCode(), 
				   m_File.GetResultCode(),
				   m_File.EnumResult(m_File.GetResultCode())
				   );
		}
	}

// End of File
