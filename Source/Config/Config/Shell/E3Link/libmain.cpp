
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load E3Link\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading E3Link\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	if( uReason == DLL_THREAD_ATTACH ) {

		afxModule->ThreadAttach();

		return TRUE;
		}

	if( uReason == DLL_THREAD_DETACH ) {

		afxModule->ThreadDetach();

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Link APIs
//

BOOL DLLAPI Et3Link_Update(CDatabase *pDbase)
{
	CLinkSendDialog Dlg(pDbase, 1);

	//Dlg.SetMethod(Method);

	return Dlg.Execute(*afxMainWnd);
	}

BOOL DLLAPI Et3Link_Send(CDatabase *pDbase)
{
	CLinkSendDialog Dlg(pDbase, sendSend);

	return Dlg.Execute(*afxMainWnd);
	}

BOOL DLLAPI Et3Link_UsingIP(CDatabase *pDbase)
{
	return TRUE;
	}

void DLLAPI Et3Link_GetCalibrate(CDatabase *pDbase)
{
	// TODO -- implement
	}

void DLLAPI Et3Link_SetCalibrate(CDatabase *pDbase)
{
	// TODO -- implement
	}

BOOL DLLAPI Et3Link_HasVerify(CDatabase *pDbase)
{
	return TRUE;
	}

BOOL DLLAPI Et3Link_Verify(CDatabase *pDbase)
{
	CLinkSendDialog Dlg(pDbase, sendVerify);

	return Dlg.Execute(*afxMainWnd);
	}

// End of File
