
#include "intern.hpp"

#include "file.hpp"

#include <setupapi.h>

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications Thread
//

extern ILinkTransport * Create_DummyTransport(void);

extern ILinkTransport * Create_UdrTransport(DWORD IP, UINT uPort);

extern ILinkTransport * Create_UdrTransport(UINT uPort, UINT uBaud);

// Libraries

#pragma comment(lib, "shell32")

#pragma comment(lib, "setupapi.lib")

// Runtime Class

AfxImplementRuntimeClass(CCommsThread, CRawThread);

// Constructor

CCommsThread::CCommsThread(CDatabase *pDbase) : m_CommEvent(FALSE), m_Firm("/base0/temp/firmware.bin")
{
	m_pDbase    = pDbase;
	
	m_pWnd      = NULL;

	m_pTrans    = NULL;

	m_pService  = New CConfigLoader;

	LoadConfig();
	}

// Destructor

CCommsThread::~CCommsThread(void)
{
	delete m_pService;

	AfxRelease(m_pTrans);
	}

// Management

void CCommsThread::Bind(CWnd *pWnd)
{
	m_pWnd = pWnd;
	}

// Attributes

CString CCommsThread::GetErrorText(void) const
{
	return m_Error;
	}

BOOL CCommsThread::GetBaseModel(UINT &uBase, UINT &uModule)
{
	return m_pService->GetBaseModel(uBase, uModule);
	}

BOOL CCommsThread::GetBaseSerialNumber(UINT &uSerialNumber)
{
	return m_pService->GetBaseSerialNumber(uSerialNumber);
	}

BOOL CCommsThread::GetBaseNetModeJumper(UINT &uMode)
{
	return m_pService->GetBaseNetModeJumper(uMode);
	}

BOOL CCommsThread::GetBaseSourceSinkJumper(UINT &uMode)
{
	return m_pService->GetBaseSourceSinkJumper(uMode);
	}

BOOL CCommsThread::CopyImageIntoFile(HANDLE hFile)
{
	return m_pService->CopyImageIntoFile(hFile);
	}

BOOL CCommsThread::GetModuleFirmwareRevision(UINT &uVersion)
{
	return m_pService->GetModuleFirmwareRevision(uVersion);
	}

// Configuration

BOOL CCommsThread::ReadFirmwareRevision(void)
{
	m_pService->ReadFirmwareRevision();
	
	Transact();

	return TRUE;
	}

BOOL CCommsThread::CheckBaseModel(void)
{
	m_pService->CheckBaseModel();
	
	Transact();

	return TRUE;
	}

BOOL CCommsThread::ReadCfgSetup(void)
{
	m_pService->ReadCfgSetup();
	
	Transact();

	return TRUE;
	}

BOOL CCommsThread::ReadImage(void)
{
	m_pService->ReadImage();
	
	Transact();

	return TRUE;
	}

BOOL CCommsThread::ReadFile(IFileData *pData)
{
	m_pService->ReadFile(pData);

	Transact();

	return TRUE;
	}

BOOL CCommsThread::PrepareItem(void)
{
	m_pService->PrepareItem();

	Transact();

	return TRUE;
	}

BOOL CCommsThread::WriteFile(IFileData *pData)
{
	m_pService->WriteFile(pData);

	Transact();

	return TRUE;
	}

BOOL CCommsThread::ClearFile(IFileData *pData)
{
	m_pService->ClearFile(pData);

	Transact();

	return TRUE;
	}

BOOL CCommsThread::ResetStation(void)
{
	m_pService->ResetStation();

	Transact();

	return TRUE;
	}

// Events

void CCommsThread::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	}

// Overridables

BOOL CCommsThread::OnInit(void)
{
	return TRUE;
	}

UINT CCommsThread::OnExec(void)
{
	if( InitTransport() ) {

		CWaitableList List(m_CommEvent, m_TermEvent);

		BOOL fOpen = m_pTrans->Open();

		for(;;) {

			if( List.WaitForAnyObject(INFINITE) == waitSignal ) {

				if( List.GetObjectIndex() == 1 ) {

					break;
					}

				UINT ID = IDFAIL;

				if( !fOpen ) {

					m_Error = m_pTrans->GetErrorText();

					m_pWnd->PostMessage(WM_COMMAND, ID);
					}
				else {
					if( m_pService->Transact() ) {
						
						ID = IDDONE;
						}
					else
						m_Error = m_pTrans->GetErrorText();
					}

				m_pWnd->PostMessage(WM_COMMAND, ID);
				}
			}

		m_pTrans->Close();
		}

	if( m_Error.IsEmpty() ) {

		m_Error = CString(IDS_FAILED_TO_CREATE);
		}

	return 0;
	}

void CCommsThread::OnTerm(void)
{
	}

void CCommsThread::Transact(void)
{
	m_CommEvent.Set();
	}

BOOL CCommsThread::CreateTransport(void)
{
	m_Method.MakeUpper();

	if( m_Method.StartsWith(L"TCP") ) {

		DWORD IP   = m_pDbase->GetIPAddress();

		UINT uPort = m_pDbase->GetDownloadPort();

		m_Name     = "TCP/IP";

		m_pTrans   = Create_UdrTransport(IP, uPort);
		
		return TRUE;
		}

	if( m_Method.StartsWith(L"USB") ) {

		UINT uPort = FindUsbComPort();

		if( uPort < NOTHING ) {

			m_pTrans = Create_UdrTransport(uPort, 9600);

			m_Name   = "USB";
		
			return TRUE;
			}

		return FALSE;
		}

	if( m_Method.StartsWith(L"COM") ) {

		UINT uPort = watoi(m_Method.Mid(3));

		m_pTrans   = Create_UdrTransport(uPort, 9600);

		m_Name     = "Serial";

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsThread::InitTransport(void)
{
	if( CreateTransport() ) {

		m_pTrans ->OnBind(m_pWnd->GetHandle());

		m_pService->Bind(m_pTrans);

		CString Caption = m_pWnd->GetWindowText();

		if( Caption.GetLength() ) {
			
			Caption += CString(IDS_VIA);
			
			Caption += m_Name;

			C3OemStrings(Caption);

			m_pWnd->SetWindowText(Caption);
			}

		m_pWnd->PostMessage(WM_COMMAND, IDINIT);

		return TRUE;
		}

	m_pWnd->PostMessage(WM_COMMAND, IDFAIL);

	return FALSE;
	}

void CCommsThread::LoadConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"E3Link");

	Key.MoveTo(L"Category");

	Key.MoveTo(m_pDbase->GetDownloadConfig());

	UINT uMethod = Key.GetValue(L"Method", 0U);

	switch( uMethod ) {

		case 0:
			m_Method = "USB";
			break;

		case 1:
			m_Method = CPrintf("COM%u", Key.GetValue(L"COM", 8U));
			break;

		case 2:
			m_Method = "TCP";
			break;
		}
	}

UINT CCommsThread::FindUsbComPort(void)
{
	CLongArray Ports;

	HDEVINFO hDevInfo = SetupDiGetClassDevs( NULL,
						 L"USB",
						 NULL,
						 (DIGCF_PRESENT | DIGCF_ALLCLASSES)
						 );

	if( hDevInfo != INVALID_HANDLE_VALUE ) {

		SP_DEVINFO_DATA DevInfoData;

		memset(&DevInfoData, 0, sizeof(DevInfoData));

		DevInfoData.cbSize = sizeof(DevInfoData);

		for( UINT idx = 0; ; idx++ ) {

			if( !SetupDiEnumDeviceInfo(hDevInfo, idx, &DevInfoData) ) {

				break;
				}

			TCHAR Buffer[MAX_PATH];

			memset(Buffer, 0, sizeof(Buffer));

			DWORD dwSize = 0;

			if( SetupDiGetDeviceInstanceId(hDevInfo, &DevInfoData, Buffer, sizeof(Buffer), &dwSize) ) {

				CString InstanceId(Buffer);

				if( InstanceId.StartsWith(L"USB\\VID_1C59&PID_0100") ) {

					memset(Buffer, 0, sizeof(Buffer));

					if( SetupDiGetDeviceRegistryProperty(	hDevInfo,
										&DevInfoData,
										SPDRP_FRIENDLYNAME,
										NULL,
										PBYTE(Buffer),
										sizeof(Buffer),
										&dwSize
										) ) {

						CString Name(Buffer);

						UINT uFind = Name.Find(L"(COM");

						if( uFind != NOTHING ) {

							UINT uPort = watoi(Name.Mid(uFind + 4));

							Ports.Append(uPort);
							}
						}
					}
				}
			}

		SetupDiDestroyDeviceInfoList(hDevInfo);
		}

	UINT uCount = Ports.GetCount();
	
	if( uCount > 0 ) {

		if( uCount == 1 ) {

			return Ports.GetAt(0);
			}

		m_Error = CString(IDS_MORE_THAN_ONE_E);
		
		return NOTHING;
		}

	m_Error = CString(IDS_NO_E_DEVICES);

	return NOTHING;
	}

// End of File
