
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_AMEND_YOUR          0x4000 /* NOT USED */
#define IDS_APP_BUILD           0x4001 /* NOT USED */
#define IDS_AUTOSAVE_FILE       0x4002 /* NOT USED */
#define IDS_AUTOSAVING          0x4003 /* NOT USED */
#define IDS_BALLOON_HELP        0x4004 /* NOT USED */
#define IDS_BASE_FILTER         0x4005 /* NOT USED */
#define IDS_CANCEL              0x4006 /* NOT USED */
#define IDS_CD3                 0x4007 /* NOT USED */
#define IDS_CHECK_FOR_UPDATED   0x4008 /* NOT USED */
#define IDS_CHECK_SAVE          0x4009 /* NOT USED */
#define IDS_CHECK_UNTITLED      0x400A /* NOT USED */
#define IDS_CIRCULAR_1          0x400B /* NOT USED */
#define IDS_CIRCULAR_2          0x400C /* NOT USED */
#define IDS_CIRCULAR_3          0x400D /* NOT USED */
#define IDS_CMD_BAD_SWITCH      0x400E /* NOT USED */
#define IDS_CMD_EX_FILE         0x400F /* NOT USED */
#define IDS_CMD_NO_FILE         0x4010 /* NOT USED */
#define IDS_CONVERTED           0x4011 /* NOT USED */
#define IDS_CREATING_CRIMSON    0x4012 /* NOT USED */
#define IDS_CRIMSON_CAN         0x4013 /* NOT USED */
#define IDS_DATABASE_WAS        0x4014 /* NOT USED */
#define IDS_DBASE_UPSUPPORT     0x4015 /* NOT USED */
#define IDS_DID_YOU_ALREADY     0x4016 /* NOT USED */
#define IDS_DO_YOU_WANT_TO      0x4017 /* NOT USED */
#define IDS_DO_YOU_WANT_TO_2    0x4018 /* NOT USED */
#define IDS_ERRORS              0x4019 /* NOT USED */
#define IDS_ERRORS_IN           0x401A /* NOT USED */
#define IDS_ERRORS_STILL        0x401B /* NOT USED */
#define IDS_ERRORS_TEXT_1       0x401C /* NOT USED */
#define IDS_ERRORS_TEXT_2       0x401D /* NOT USED */
#define IDS_ERRORS_TEXT_3       0x401E /* NOT USED */
#define IDS_ERRORS_TEXT_4       0x401F /* NOT USED */
#define IDS_ERRORS_TEXT_5       0x4020 /* NOT USED */
#define IDS_FILE_COULD_NOT_BE   0x4021 /* NOT USED */
#define IDS_FILE_IS_OPEN        0x4022 /* NOT USED */
#define IDS_FILE_TARGET_SELECT  0x4023 /* NOT USED */
#define IDS_FILE_WAS            0x4024 /* NOT USED */
#define IDS_FILE_WAS_2          0x4025 /* NOT USED */
#define IDS_FILE_WAS_IMPORTED   0x4026 /* NOT USED */
#define IDS_F_KEY_TO_DISPLAY    0x4027 /* NOT USED */
#define IDS_ICON_AT_RIGHTHAND   0x4028 /* NOT USED */
#define IDS_IF_PRESSING_F_KEY   0x4029 /* NOT USED */
#define IDS_IMAGE_EXT           0x402A /* NOT USED */
#define IDS_IMAGE_FILTER        0x402B /* NOT USED */
#define IDS_IMAGE_SAVE          0x402C /* NOT USED */
#define IDS_IMPORT_CRIMSON      0x402D /* NOT USED */
#define IDS_IMPORT_EXT          0x402E /* NOT USED */
#define IDS_IMPORT_FILTER       0x402F /* NOT USED */
#define IDS_IMPORT_WARN_1       0x4030 /* NOT USED */
#define IDS_IMPORT_WARN_2       0x4031 /* NOT USED */
#define IDS_IMPORT_WARN_3       0x4032 /* NOT USED */
#define IDS_LOADING_CRIMSON     0x4033 /* NOT USED */
#define IDS_LOAD_ERROR          0x4034 /* NOT USED */
#define IDS_MODEL_UNSUPPORTED   0x4035 /* NOT USED */
#define IDS_NN                  0x4036 /* NOT USED */
#define IDS_OPENING_FILE        0x4037 /* NOT USED */
#define IDS_OPTIONS_ON_HELP     0x4038 /* NOT USED */
#define IDS_OR_IF_YOU_WOULD     0x4039 /* NOT USED */
#define IDS_PLEASE_BE_SURE_TO   0x403A /* NOT USED */
#define IDS_QUESTIONS_VIA       0x403B /* NOT USED */
#define IDS_READ                0x403C /* NOT USED */
#define IDS_REG_ABORT           0x403D /* NOT USED */
#define IDS_REG_AMEND           0x403E /* NOT USED */
#define IDS_REG_COMPANY         0x403F
#define IDS_REG_CONNECT         0x4040 /* NOT USED */
#define IDS_REG_EMAIL           0x4041 /* NOT USED */
#define IDS_REG_EMAIL_ERROR     0x4042 /* NOT USED */
#define IDS_REG_ERROR           0x4043 /* NOT USED */
#define IDS_REG_NAME            0x4044
#define IDS_REG_PATH            0x4045 /* NOT USED */
#define IDS_REG_PRODUCTS        0x4046 /* NOT USED */
#define IDS_REG_SENDING         0x4047 /* NOT USED */
#define IDS_REG_SERVER          0x4048 /* NOT USED */
#define IDS_REG_SKIP            0x4049 /* NOT USED */
#define IDS_REG_TITLE           0x404A /* NOT USED */
#define IDS_REG_UPDATES         0x404B /* NOT USED */
#define IDS_REG_UPGRADE         0x404C /* NOT USED */
#define IDS_REG_USERINFO        0x404D /* NOT USED */
#define IDS_REG_VERSION         0x404E
#define IDS_SAVE_CONVERSION     0x404F /* NOT USED */
#define IDS_SAVE_ERROR          0x4050 /* NOT USED */
#define IDS_SAVING_FILE         0x4051 /* NOT USED */
#define IDS_SUPPORT_URL         0x4052 /* NOT USED */
#define IDS_THANK_YOU_FOR       0x4053 /* NOT USED */
#define IDS_UNABLE_TO_READ      0x4054 /* NOT USED */
#define IDS_UNABLE_TO_WRITE     0x4055 /* NOT USED */
#define IDS_UNABLE_TO_WRITE_2   0x4056 /* NOT USED */
#define IDS_UNREGISTERED        0x4057 /* NOT USED */
#define IDS_UNTITLED_FILE       0x4058 /* NOT USED */
#define IDS_UPDATE_ABORT        0x4059 /* NOT USED */
#define IDS_UPDATE_AGENT        0x405A /* NOT USED */
#define IDS_UPDATE_AVAILABLE    0x405B /* NOT USED */
#define IDS_UPDATE_BUILD        0x405C /* NOT USED */
#define IDS_UPDATE_CLOSE        0x405D /* NOT USED */
#define IDS_UPDATE_ERROR        0x405E /* NOT USED */
#define IDS_UPDATE_FAILED       0x405F /* NOT USED */
#define IDS_UPDATE_FIND_DATA    0x4060 /* NOT USED */
#define IDS_UPDATE_FIND_SETUP   0x4061 /* NOT USED */
#define IDS_UPDATE_INVALID      0x4062 /* NOT USED */
#define IDS_UPDATE_NONE         0x4063 /* NOT USED */
#define IDS_UPDATE_OPEN_CONN    0x4064 /* NOT USED */
#define IDS_UPDATE_OP_OK        0x4065 /* NOT USED */
#define IDS_UPDATE_READ         0x4066 /* NOT USED */
#define IDS_UPDATE_SETUP        0x4067 /* NOT USED */
#define IDS_UPDATE_STATUS       0x4068 /* NOT USED */
#define IDS_VALIDATING          0x4069 /* NOT USED */
#define IDS_YOUR_INFORMATION    0x406A /* NOT USED */
#define IDS_YOU_MAY_ALSO        0x406B /* NOT USED */

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_FILE_SAVE_IMAGE	 0x8160
#define	IDM_FILE_CONVERT	 0x8161
#define	IDM_FILE_SECURITY	 0x8162
#define	IDM_FILE_EXTRA		 0x8170

#define	IDM_LINK		 0xB0
#define	IDM_LINK_TITLE		 0xB000
#define	IDM_LINK_SEND		 0xB001
#define	IDM_LINK_UPDATE		 0xB002
#define	IDM_LINK_SEND_IMAGE	 0xB003
#define	IDM_LINK_UPLOAD		 0xB004
#define	IDM_LINK_SET_TIME	 0xB005
#define	IDM_LINK_MOUNT		 0xB006
#define	IDM_LINK_DISMOUNT	 0xB007
#define	IDM_LINK_OPTIONS	 0xB008
#define IDM_LINK_FORMAT		 0xB009
#define	IDM_LINK_CALIBRATE	 0xB00A
#define	IDM_LINK_EMULATE	 0xB00C
#define	IDM_LINK_VERIFY		 0xB00D
#define	IDM_LINK_ONLINE		 0xB00E
#define IDM_LINK_SET_PFM_DROP    0xB00F
#define IDM_LINK_COMPRESS	 0xB010

#define	IDM_WARN		 0xB1
#define	IDM_WARN_TITLE		 0xB100
#define	IDM_WARN_SHOW_ERROR	 0xB101
#define	IDM_WARN_SHOW_CIRCLE     0xB102
#define	IDM_WARN_RECOMPILE	 0xB103
#define IDM_WARN_REBLOCK_COMMS	 0xB104
#define	IDM_WARN_REMAP_PERSIST	 0xB105

#define	IDM_HELP_UPDATE		 0x8610
#define	IDM_HELP_REGISTER	 0x8611
#define	IDM_HELP_NOTES		 0x8612
#define	IDM_HELP_REFERENCE	 0x8613

// End of File

#endif
