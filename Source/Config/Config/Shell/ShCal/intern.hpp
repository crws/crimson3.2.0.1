
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <shlink.hpp>

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Build Data
//

#include "..\..\build.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CCrimsonCalApp : public CThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCrimsonCalApp(void);

		// Destructor
		~CCrimsonCalApp(void);

		
	protected:
		
		// Overridables
		BOOL OnInitialize(void);
		void OnTerminate(void);
	};


// End of File

#endif
