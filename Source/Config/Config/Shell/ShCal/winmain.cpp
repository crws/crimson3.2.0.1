
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Test Harness
//

int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	afxModule = New CModule(modApplication);

	if( !afxModule->InitApp(hThis, GetCommandLine(), nCmdShow) ) {

		AfxTrace(L"ERROR: Failed to initialize application\n");

		delete afxModule;

		afxModule = NULL;

		return 1;
		}
	
	CString Name = CString(IDS_REG_COMPANY);

	UINT    uPos = Name.Find('/');

	afxModule->SetRegCompany(Name.Left(uPos));

	afxModule->SetRegName   (CString(IDS_REG_NAME));

	afxModule->SetRegVersion(CString(IDS_REG_VERSION));

	(New CCrimsonCalApp)->Execute();
	
	afxModule->Terminate();
	
	delete afxModule;

	afxModule = NULL;

	return 0;
	}

// End of File
