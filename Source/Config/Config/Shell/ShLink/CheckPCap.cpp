
#include "Intern.hpp"

#include "CheckPCap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Headers and Libraries
//

#include <setupapi.h>

#pragma  comment(lib, "shell32")

//////////////////////////////////////////////////////////////////////////
//
// WinPCap Checker
//

// Operations

BOOL CCheckPCap::Check(BOOL fInstall)
{
	if( TRUE ) {

		CRegKey Key(HKEY_LOCAL_MACHINE);

		Key = Key.Open(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\NpcapInst");

		if( Key.IsValid() ) {

			UINT uMajor = watoi(Key.GetValue(L"VersionMajor", L""));

			UINT uMinor = watoi(Key.GetValue(L"VersionMinor", L""));

			if( uMajor >= 0 ) {

				HMODULE hLib = LoadLibrary(L"wpcap.dll");

				if( hLib ) {

					FreeLibrary(hLib);

					return TRUE;
				}
			}

			AfxTouch(uMinor);
		}
	}

	if( TRUE ) {

		CRegKey Key(HKEY_LOCAL_MACHINE);

		Key = Key.Open(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\WinPcapInst");

		if( Key.IsValid() ) {

			UINT uMajor = watoi(Key.GetValue(L"VersionMajor", L""));

			UINT uMinor = watoi(Key.GetValue(L"VersionMinor", L""));

			if( uMajor > 4 || (uMajor == 4 && uMinor >= 1) ) {

				return TRUE;
			}
		}
	}

	if( fInstall ) {

		CString Text;

		Text += CString(IDS_WINPCAP_MUST_BE);

		Text += L"\n\n";

		Text += CString(IDS_DO_YOU_WISH_TO);

		if( CWnd::GetActiveWindow().YesNo(Text) == IDYES ) {

			afxThread->SetWaitMode(TRUE);

			CString Program = pccModule->GetFilename().WithName(L"Installers\\winpcap.exe");

			SHELLEXECUTEINFO Info;

			memset(&Info, 0, sizeof(Info));

			Info.cbSize       = sizeof(Info);

			Info.fMask        = SEE_MASK_NOCLOSEPROCESS;

			Info.lpFile       = Program;

			Info.lpParameters = L"";

			Info.nShow        = SW_SHOW;

			if( ShellExecuteEx(&Info) ) {

				for( ;;) {

					if( WaitForSingleObject(Info.hProcess, 500) == WAIT_OBJECT_0 ) {

						break;
					}

					afxMainWnd->UpdateWindow();
				}

				CloseHandle(Info.hProcess);

				afxMainWnd->UpdateWindow();

				afxThread->SetWaitMode(FALSE);

				return TRUE;
			}

			afxThread->SetWaitMode(FALSE);

			CWnd::GetActiveWindow().Error(CString(IDS_SETUP_PROGRAM));
		}
	}

	return FALSE;
}

// End of File
