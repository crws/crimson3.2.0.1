
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RC4 Encryption Support
//

// Context

struct rc4_key
{      
	BYTE state[256];
	BYTE x;
	BYTE y;
	};

// Prototypes

global	void	rc4(PBYTE pCode, PCBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);
global	void	rc4(PBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);
static	void	Swap(PBYTE a, PBYTE b);
static	void	rc4_prep(PCBYTE pData, UINT uSize, rc4_key *pKey);
static	void	rc4_work(PBYTE pData, UINT uSize, rc4_key *pKey);

// Code

global void rc4(PBYTE pCode, PCBYTE pData, UINT uData, PCBYTE pKey, UINT uKey)
{
	memcpy(pCode, pData, uData);

	rc4(pCode, uData, pKey, uKey);
	}

global void rc4(PBYTE pData, UINT uData, PCBYTE pKey, UINT uKey)
{
	rc4_key key;

	rc4_prep(pKey, uKey, &key);

	rc4_work(pData, uData, &key);
	}

static void Swap(PBYTE a, PBYTE b)
{
	BYTE sb;

	sb = *a;

	*a = *b;

	*b = sb;
}

static void rc4_prep(PCBYTE pData, UINT uSize, rc4_key *pKey)
{
	BYTE   index1;
	BYTE   index2;
	PBYTE  state;
	UINT   counter;     
	
	state = &pKey->state[0];

	for( counter = 0; counter < 256; counter++ ) {

		state[counter] = (BYTE) counter;
		}

	pKey->x = 0;     
	pKey->y = 0;     
	index1  = 0;     
	index2  = 0;

	for( counter = 0; counter < 256; counter++ ) {

		index2 = (BYTE) (pData[index1] + state[counter] + index2);

		Swap(&state[counter], &state[index2]);
		
		index1 = (BYTE) ((index1 + 1) % uSize);
		}
	}

static void rc4_work(PBYTE pData, UINT uSize, rc4_key *pKey)
{ 
	BYTE  x;
	BYTE  y;
	PBYTE state;
	BYTE  xorIndex;
	UINT  counter;
	
	x = pKey->x;     
	y = pKey->y;     
	
	state = &pKey->state[0];         

	for( counter = 0; counter < uSize; counter++ ) {

		x = (BYTE) (x        + 1);
		
		y = (BYTE) (state[x] + y);
		
		Swap(&state[x], &state[y]);
		
		xorIndex = (BYTE) (state[x] + state[y]);
		
		pData[counter] ^= state[xorIndex];
		}               
	
	pKey->x = x;     
	pKey->y = y;
	}

// End of File
