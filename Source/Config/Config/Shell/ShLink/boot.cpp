
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader Support
//

// Constructor

CBootLoader::CBootLoader(void)
{
	}

// Attributes

BOOL CBootLoader::IsSameVersion(void) const
{
	if( m_Req.GetOpcode() == bootCheckVersion ) {

		BYTE b = m_Rep.GetData()[0];

		return b ? TRUE : FALSE;
		}

	return FALSE;
	}

BOOL CBootLoader::CanUseDatabase(void) const
{
	if( m_Req.GetOpcode() == bootCheckVersion ) {

		if( m_Rep.GetDataSize() == 2 ) {

			BYTE b = m_Rep.GetData()[1];

			return b ? TRUE : FALSE;
		}
	}

	return FALSE;
}

BOOL CBootLoader::IsSameOem(BOOL fProm) const
{
	if( m_Rep.GetOpcode() == opNak ) {
		
		return TRUE;
		}
	
	if( m_Req.GetOpcode() == bootReadOem ) {

		PCSTR   pOem = PCSTR(m_Rep.GetData());

		CString Read = CString(pOem);

		if( Read == m_Config ) {

			return TRUE;
			}

		if( fProm ) {

			if( Read == L"Red Lion Controls" ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CBootLoader::GetModel(CString &Name) const
{
	if( m_Rep.GetOpcode() == opNak ) {
		
		return TRUE;
		}
	
	if( m_Req.GetOpcode() == bootReadModel ) {

		Name = PCSTR(m_Rep.GetData());

		return TRUE;
		}

	return FALSE;
	}

BOOL CBootLoader::GetRevision(DWORD &dwVer) const
{
	if( m_Rep.GetOpcode() == opNak ) {
		
		dwVer = 0;

		return TRUE;
		}

	if( m_Req.GetOpcode() == bootReadRevision ) {

		UINT uPtr = 0;

		dwVer = m_Rep.ReadLong(uPtr);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBootLoader::GetLevel(UINT &uLevel) const
{
	if( m_Rep.GetOpcode() == opNak ) {
		
		uLevel = 0;

		return TRUE;
		}

	if( m_Rep.GetOpcode() == opReply ) {

		UINT uPtr = 0;

		uLevel = m_Rep.ReadByte(uPtr);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBootLoader::GetAutoDetect(CString &Name) const
{
	if( m_Req.GetOpcode() == bootAutoDetect ) {

		Name = PCSTR(m_Rep.GetData());

		return TRUE;
	}

	return FALSE;
}

// Operations

BOOL CBootLoader::CheckModel(void)
{
	m_Req.StartFrame(servBoot, bootReadModel);

	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CBootLoader::CheckOem(CString Config)
{
	m_Config = Config;

	m_Req.StartFrame(servBoot, bootReadOem);

	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CBootLoader::CheckVersion(GUID Guid, DWORD Dbase)
{
	m_Req.StartFrame(servBoot, bootCheckVersion);

	m_Req.AddData(PBYTE(&Guid), sizeof(Guid));

	m_Req.AddLong(Dbase);

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CBootLoader::ForceReset(BOOL fTimeout)
{
	m_Req.StartFrame(servBoot, bootForceReset);

	m_Req.AddLong(fTimeout ? 0 : -1);

	m_Req.MarkVerySlow();

	return SyncTransact(opNull, TRUE, FALSE);
	}

BOOL CBootLoader::CheckHardware(void)
{
	m_Req.StartFrame(servBoot, bootCheckHardware);

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CBootLoader::CheckLoader(BOOL fSkip)
{
	m_Req.StartFrame(servBoot, bootReadRevision);

	if( fSkip ) {

		m_Req.MarkVerySlow();
		}
	
	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CBootLoader::ClearProgram(BYTE bBlocks)
{
	m_Req.StartFrame(servBoot, bootClearProgram);

	m_Req.AddByte(bBlocks);

	m_Req.MarkVerySlow();

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CBootLoader::WriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount, BOOL fLast)
{
	m_Req.StartFrame(servBoot, bootWriteProgram);

	m_Req.AddLong(dwAddr);

	m_Req.AddWord(wCount);

	if( !fLast ) m_Req.MarkAsync();

	m_Req.AddBulk(pData, wCount);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CBootLoader::WriteProgramEx(DWORD dwAddr, PBYTE pData, WORD wCount, BOOL fLast)
{
	m_Req.StartFrame(servBoot, bootWriteProgramEx);

	m_Req.AddLong(dwAddr);

	m_Req.AddWord(wCount);

	m_Req.AddWord(FindSum(pData, wCount));

	if( !fLast ) m_Req.MarkAsync();

	m_Req.AddBulk(pData, wCount);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CBootLoader::WriteVersion(GUID Guid)
{
	m_Req.StartFrame(servBoot, bootWriteVersion);

	m_Req.AddData(PBYTE(&Guid), sizeof(Guid));

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CBootLoader::StartProgram(void)
{
	m_Req.StartFrame(servBoot, bootStartProgram);

	m_Req.MarkVerySlow();

	return SyncTransact(opNull, TRUE, FALSE);
	}

BOOL CBootLoader::WriteMAC(PBYTE pMAC, UINT uCount)
{
	m_Req.StartFrame(servBoot, bootWriteMAC);

	m_Req.AddData(pMAC, uCount);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CBootLoader::WriteSerialNumber(PBYTE pSerial, UINT uCount)
{
	m_Req.StartFrame(servBoot, bootWriteSerialNum);

	m_Req.AddData(pSerial, uCount);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CBootLoader::CheckLevel(void)
{
	m_Req.StartFrame(servBoot, bootCheckLevel);

	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CBootLoader::AutoDetect(void)
{
	m_Req.StartFrame(servBoot, bootAutoDetect);

	return SyncTransact(opReply, FALSE, FALSE);
}

// Implementation

UINT CBootLoader::FindTransition(void)
{
	switch( m_Req.GetOpcode() ) {

		case bootForceReset:

			return transAppToBoot;
			
		case bootStartProgram:

			return transBootToApp; 
		}

	return transNone;
	}

WORD CBootLoader::FindSum(PBYTE pData, WORD wCount)
{
	WORD wSum = 0x5555;

	for( WORD i = 0; i < wCount; i++ ) {

		wSum = WORD(wSum + pData[i]);
		}

	return wSum;
	}

// End of File
