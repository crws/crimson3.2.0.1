
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Download Link
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

///////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "shlink.hpp"

#include "intern.hxx"

#include "g3master.hpp"

#include <iphlpapi.h>

#include <shlobj.h>

//////////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma	comment(lib, "iphlpapi")

#pragma comment(lib, "shell32")

#pragma comment(lib, "winmm.lib")

//////////////////////////////////////////////////////////////////////////
//
// Link Settings
//

extern	CString	g_sLinkEmModel;

extern	BOOL	g_fLinkEmulate;

extern	UINT	g_uLinkMethod;

extern	UINT	g_uLinkComPort;

extern	CString	g_sLinkUsbHost;

extern	CString	g_sLinkTcpHost;

extern	UINT	g_uLinkTcpPort;

extern	BOOL	g_fLinkTcpSlow;

///////////////////////////////////////////////////////////////////////
//
// Byte Order Conversions
//

inline	WORD	HostToMotor(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
	}

inline	WORD	MotorToHost(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
	}

inline	DWORD	HostToMotor(DWORD Data)
{
	DWORD dwHi = HostToMotor(WORD(Data >> 16));

	DWORD dwLo = HostToMotor(WORD(Data)) << 16;

	return dwHi | dwLo;
	}

inline	DWORD	MotorToHost(DWORD Data)
{
	DWORD dwHi = HostToMotor(WORD(Data >> 16));

	DWORD dwLo = HostToMotor(WORD(Data)) << 16;

	return dwHi | dwLo;
	}

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCommsThread;

class CLinkSendDialog;

class CLinkUploadDialog;

class CLinkExtractDialog;

//////////////////////////////////////////////////////////////////////////
//
// MC2 Service Codes
//

#define	SERV_CALIB	0x04

//////////////////////////////////////////////////////////////////////////
//
// MC2 Calibration Opcodes
//

#define	OP_ACK		0x01
#define	OP_REPLY	0x03
#define	OP_CALIB_REQ	0x10
#define	OP_CALIB_READ	0x11

//////////////////////////////////////////////////////////////////////////
//
// Communications Thread
//

class CCommsThread : public CRawThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Callback Type
		typedef BOOL (*PCALLBACK)(PVOID, UINT);

		// Constructor
		CCommsThread(void);

		// Destructor
		~CCommsThread(void);

		// Management
		void SetMethod(CString Method);
		void SetCredentials(CString User, CString Pass);
		void Bind(PCALLBACK pfnProc, PVOID pParam);
		void Bind(CWnd *pWnd);
		BOOL Terminate(DWORD Timeout);
		void SendComplete(void);
		void Recycle(UINT uTrans);
		void CycleSim(void);

		// Attributes
		BOOL    UsingTCPIP  (void) const;
		BOOL    UsingSim    (void) const;
		UINT    GetBulkLimit(BOOL fDown = TRUE) const;
		CString GetErrorText(void) const;

		// Credential Prompt
		BOOL AskForCredentials(void);

		// Boot Loader
		BOOL BootCheckModel(void);
		BOOL BootCheckOem(CString Config);
		BOOL BootCheckVersion(GUID Guid, DWORD Dbase);
		BOOL BootForceReset(BOOL fTimeout);
		BOOL BootCheckHardware(void);
		BOOL BootCheckLoader(BOOL fSkip);
		BOOL BootClearProgram(BYTE bBlocks);
		BOOL BootWriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount, BOOL fLast);
		BOOL BootWriteVersion(GUID Guid);
		BOOL BootStartProgram(void);
		BOOL BootCheckLevel(void);
		BOOL BootAutoDetect(void);
		BOOL BootIsSameVersion(void);
		BOOL BootCanUseDatabase(void);
		BOOL BootIsSameOem(void);
		BOOL BootGetModel(CString &Name);
		BOOL BootGetRevision(DWORD &dwVer);
		BOOL BootGetLevel(UINT &uLevel);
		BOOL BootGetAutoDetect(CString &Name);

		// Prom Loader
		BOOL PromCheckLoader(void);
		BOOL PromWriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount);
		BOOL PromUpdateProgram(UINT uProgram);
		BOOL PromStartProgram(void);
		BOOL PromIsRunningLoader(void);

		// Calibration
		void CalibTxReq (BYTE bSlot, BYTE bCode, WORD wData);
		void CalibTxRead(BYTE bSlot, BYTE bCode);
		BOOL CalibRxReq (void);
		BOOL CalibRxRead(WORD &wData);

		// Configuration
		BOOL ConfigCheckVersion(GUID Guid);
		BOOL ConfigClearConfig(void);
		BOOL ConfigClearGarbage(void);
		BOOL ConfigCheckItem(UINT uItem, UINT uClass, DWORD CRC);
		BOOL ConfigWriteItem(UINT uItem, UINT uClass, UINT uSize);
		BOOL ConfigWriteItemEx(UINT uItem, UINT uClass, UINT uSize, UINT uComp);
		BOOL ConfigWriteData(DWORD dwAddr, PBYTE pData, WORD wCount);
		BOOL ConfigWriteVersion(GUID Guid);
		BOOL ConfigHaltSystem(void);
		BOOL ConfigStartSystem(void);
		BOOL ConfigWriteTime(void);
		BOOL ConfigReadItem(UINT uItem);
		BOOL ConfigReadItem(UINT uItem, UINT uSubItem);
		BOOL ConfigReadData(UINT uItem, UINT uAddr, UINT uCount);
		BOOL ConfigFlashMount(void);
		BOOL ConfigFlashDismount(void);
		BOOL ConfigFlashVerify(BOOL fRaw);
		BOOL ConfigFlashFormat(void);
		BOOL ConfigCheckCompression(void);
		BOOL ConfigCheckExecution(void);
		BOOL ConfigCheckControl(void);
		BOOL ConfigCheckEditFlags(void);
		BOOL ConfigClearEditFlags(BYTE bMask);
		BOOL ConfigGetReply(void);
		UINT ConfigGetItemSize(void) const;
		void ConfigGetItemData(PBYTE pData, UINT uCount) const;
		BOOL ConfigGetCompression(BOOL &fCompress) const;
		BOOL ConfigGetExecution(BOOL &fExecute) const;
		BOOL ConfigGetControl(BOOL &fControl) const;
		BOOL ConfigGetSystemHalt(BOOL &fHalted) const;
		BOOL ConfigGetEditFlags(BYTE &bFlags, PDWORD pCheck) const;

		// Tunneling
		BOOL TunnelSendData(PCBYTE pData, UINT uSize);
		BOOL TunnelRecvData(PBYTE  pData, UINT uSize);
		
		// Data Access
		BOOL DataListRead(PCDWORD pRefs, UINT uCount);
		BOOL DataGetReadData(PDWORD pData, PBOOL pAvail, UINT uCount);
		
		// Straton
		BOOL StratonSend(PCBYTE pData, UINT uData);
		UINT StratonRecv(PBYTE  pData, UINT uData);

		// Events
		void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);

	protected:
		// Data Members
		CEvent		   m_CommEvent;
		CEvent		   m_CredEvent;
		CString		   m_Error;
		CString		   m_Name;
		ILinkTransport   * m_pTrans;
		CWnd		 * m_pWnd;
		PCALLBACK	   m_pfnProc;
		PVOID		   m_pParam;
		UINT		   m_uServ;
		CBootLoader      * m_pBoot;
		CPromLoader	 * m_pProm;
		CTunnelKing	 * m_pTunnel;
		CConfigLoader	 * m_pConfig;
		CDataAccess      * m_pData;
		CStratonOnline   * m_pStraton;
		CCrimsonAuth     * m_pAuth;
		BYTE		   m_bSeq;
		HWND		   m_hEmulator;
		BOOL		   m_fSim;
		CString		   m_Method;
		CString		   m_ForceUser;
		CString		   m_ForcePass;
		CByteArray         m_AuthDevice;
		CByteArray         m_AuthUser;
		CByteArray         m_AuthPass;
		CByteArray         m_AuthResponse;
		UINT		   m_uAuthState;
		UINT	           m_uAuthCount;
		BOOL	           m_fAuthSave;
		BOOL		   m_fAuthOkay;

		// Overridables
		BOOL OnInit(void);
		UINT OnExec(void);
		void OnTerm(void);
		
		// Implementation
		void    InitServices(void);
		BOOL    InitTransport(void);
		void    Transact(UINT uServ);
		UINT    CreateTransport(void);
		BOOL    RunEmulator(void);
		BOOL    KillEmulator(void);
		BOOL    Signal(UINT uCode);
		CString FindAeonHost(void);
		CString FindEmulator(void);
		CString FindComponent(CString Name);
		CString MakeEmulatorParams(CString Model, CString Ident, CString Client);
		CString GetEmulatorConfig(void);
		CString GetEmulatorMappings(void);
		CString GetEmulatorDataPath(void);

		// Authentication
		BOOL MakeAuthResponse(CByteArray const &Challenge);
		BOOL FindCachedCredentials(void);
		BOOL SaveCachedCredentials(void);
		BOOL ClearCachedCredentials(void);
		BOOL WaitForCredentials(void);
		BOOL Convert(CByteArray &Data, CString const &Text);
		BOOL Convert(CString &Text, CByteArray const &Data);

		// Window Callback
		static BOOL PostToWindow(PVOID pParam, UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Send Options
//

enum
{
	sendSend,
	sendUpdate,
	sendVerify,
	sendSilent,
	sendTime
	};

//////////////////////////////////////////////////////////////////////////
//
// Download Dialog Box
//

class CLinkSendDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CLinkSendDialog(CDatabase *pDbase, UINT uSend);

		// Destructor
		~CLinkSendDialog(void);

		// Operations
		void SetMethod(CString Method);
		void SetCredentials(CString User, CString Pass);

	public:
		// Data Members
		CDatabase    * m_pDbase;
		UINT	       m_uSend;
		CCommsThread * m_pComms;
		BOOL	       m_fDone;
		BOOL	       m_fError;
		BOOL  	       m_fDelay;
		UINT	       m_uState;
		UINT	       m_uReprom;
		BOOL	       m_fVerify;
		BOOL	       m_fChanges;
		BOOL	       m_fCompact;
		BOOL	       m_fHalted;
		BOOL	       m_fWarned;
		BOOL	       m_fControl;
		DWORD	       m_dwSize;
		DWORD	       m_dwAddr;
		DWORD	       m_dwTotal;
		FILE         * m_pFile;
		CGuid	       m_FirmGuid;
		DWORD	       m_FirmBoot;
		INDEX	       m_Index;
		UINT	       m_uAddr;
		UINT	       m_uItem;
		UINT	       m_uSize;
		UINT	       m_uComp;
		CItem        * m_pItem;
		CInitData      m_InitData;
		CString        m_FirmModel;
		PBYTE          m_pData;
		CString	       m_Method;
		CSystemItem  * m_pSystem;
		BOOL	       m_fForceReprom;
		BYTE	       m_bClear;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnDeviceChange(LONG dwEvent, LONG dwData);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnCommsInit(UINT uID);
		BOOL OnCommsDone(UINT uID);
		BOOL OnCommsFail(UINT uID);
		BOOL OnCommsCred(UINT uID);

		// Implementation
		void SetDone(BOOL fError);
		void SetError(PCTXT pText);
		void ShowStatus(PCTXT pText);
		void TxFrame(void);
		BOOL RxFrame(void);
		void PrepareData(void);
		BOOL Verify(BOOL fAllow, BOOL fFirmware);
		BOOL Warn(UINT uChange);

		// File Helpers
		BOOL OpenFirmware(BOOL &fWrong);
		BOOL OpenLoader(void);
		BOOL OpenProm(void);
		BOOL OpenPrimary(void);
		BOOL OpenFile(BOOL &fWrong, UINT uSlot);
		BOOL OpenFile(CString Name, BOOL fHasVersion);
		void CloseFile(void);

		// Reprom Filter
		BOOL CheckReprom(DWORD dwVersion) const;
		BOOL SkipLoader(void);
		BOOL SkipPrimary(void);
		void LoadConfig(void);

		// Debug
		PCTXT EnumState(UINT uState);
	};

//////////////////////////////////////////////////////////////////////////
//
// Upload Dialog Box
//

class CLinkUploadDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CLinkUploadDialog(void);

		// Destructor
		~CLinkUploadDialog(void);

		// Attributes
		CString GetFilename(void) const;
	
	public:
		// Data Members
		CCommsThread * m_pComms;
		BOOL	       m_fDone;
		BOOL	       m_fError;
		UINT	       m_uState;
		UINT	       m_uSize;
		UINT	       m_uLimit;
		UINT	       m_uAddr;
		UINT	       m_uCount;
		CFilename      m_File1;
		CFilename      m_File2;
		HANDLE	       m_hFile;
		BOOL	       m_fImport;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnDeviceChange(LONG dwEvent, LONG dwData);

		// Command Handlers
		BOOL OnCommandOkay(UINT uID);
		BOOL OnCommsInit(UINT uID);
		BOOL OnCommsDone(UINT uID);
		BOOL OnCommsFail(UINT uID);
		BOOL OnCommsCred(UINT uID);

		// Implementation
		void SetDone(BOOL fError);
		void SetError(PCTXT pText);
		void ShowStatus(PCTXT pText);
		void TxFrame(void);
		BOOL RxFrame(void);
		BOOL OpenFile(void);
		BOOL CloseFile(void);
		BOOL SaveFile(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Extract Dialog Box
//

class CLinkExtractDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CLinkExtractDialog(char cTag);

	// Destructor
	~CLinkExtractDialog(void);

	// Attributes
	CString GetData(void) const;

public:
	// Data Members
	CCommsThread * m_pComms;
	BOOL	       m_fDone;
	BOOL	       m_fError;
	UINT	       m_uState;
	UINT	       m_uSize;
	UINT	       m_uLimit;
	UINT	       m_uAddr;
	UINT	       m_uCount;
	UINT	       m_uItem;
	char	       m_cTag;
	CString	       m_Text;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	void OnDeviceChange(LONG dwEvent, LONG dwData);

	// Command Handlers
	BOOL OnCommandOkay(UINT uID);
	BOOL OnCommsInit(UINT uID);
	BOOL OnCommsDone(UINT uID);
	BOOL OnCommsFail(UINT uID);
	BOOL OnCommsCred(UINT uID);

	// Implementation
	void SetDone(BOOL fError);
	void SetError(PCTXT pText);
	void ShowStatus(PCTXT pText);
	void TxFrame(void);
	BOOL RxFrame(void);
};

// End of File

#endif
