
#include "intern.hpp"

#include "g3master.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MC2 Tunnel King
//

// Constructor

CTunnelKing::CTunnelKing(void) 
{
	}

// Attributes

BOOL CTunnelKing::RecvData(PBYTE pData, UINT uSize) const
{
	if( m_Req.GetOpcode() == tunnelTunnel ) {

		memcpy(pData, m_Rep.GetData(), uSize);

		return TRUE;
		}

	return FALSE;
	}

// Basic Operations

BOOL CTunnelKing::SendData(PCBYTE pData, UINT uSize)
{
	m_Req.StartFrame(servTunnel, tunnelTunnel);

	m_Req.AddBulk(pData, uSize);

	return SyncTransact(tunnelTunnel, FALSE, FALSE);
	}

// End of File
