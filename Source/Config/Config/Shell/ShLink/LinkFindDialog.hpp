
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinkFindDlg_HPP

#define INCLUDE_LinkFindDlg_HPP

//////////////////////////////////////////////////////////////////////////
//
// Link Find Dialog
//

class CLinkFindDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructors
	CLinkFindDialog(BOOL fAlone);

	// Attributes
	CString GetHost(void) const;
	CString GetProt(void) const;

	// Operations
	void PreSelect(CString Prot, CString Host);

protected:
	// Data Members
	BOOL         m_fAlone;
	CStringArray m_List;
	CListView *  m_pView;
	CString      m_Host;
	CString	     m_Prot;
	UINT	     m_uItem;
	CStringArray m_Fields;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

	// Command Handlers
	BOOL OnCommandOK(UINT uID);
	BOOL OnCommandCancel(UINT uID);
	BOOL OnCommandIdentify(UINT uID);
	BOOL OnCommandBrowse(UINT uID);
	BOOL OnCommandOpenFtp(UINT uID);
	BOOL OnCommandRefresh(UINT uID);
	BOOL OnCommandUseName(UINT uID);

	// Notification Handlers
	void OnItemChanged(UINT uID, NMLISTVIEW &Info);
	void OnDoubleClick(UINT uID, NMHDR &Info);

	// Implementation
	void    PerformLoad(void);
	void    ExecFind(void);
	void    ExecIdentify(CString Mac);
	void    CreateView(void);
	void    LoadView(void);
	CString FindName(void);
	void    FindUsb(void);
	void	LoadDns(void);
	void	SaveDns(void);
};

// End of File

#endif
