
//////////////////////////////////////////////////////////////////////////
//
// G3 MC2 Calibration Link Support
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CALIB_HPP

#define INCLUDE_CALIB_HPP

//////////////////////////////////////////////////////////////////////////
//
// Fixed Calibration Identifiers (bCode)
//

#define	CALIBNULL	 0
#define	CALIBCJ		10
#define	CALIBLIN0V	15
#define	CALIBLIN10V	16
#define	CALIBLIN0MA	17
#define	CALIBLIN4MA	18
#define	CALIBLIN20MA	19
#define	CALIBLINDONE	20
#define	CALIBSAVE	21

//////////////////////////////////////////////////////////////////////////
//
// Model ID's
//

#define ID_GMPID1	0x1006
#define ID_GMPID2	0x1007
#define ID_GMUIN4	0x1008
#define ID_GMOUT4	0x1009
#define ID_GMDIO14	0x100A
#define ID_GMTC8	0x100B
#define ID_GMINI8	0x100C
#define ID_GMINV8	0x100D
#define ID_GMRTD6	0x100E
#define ID_GMSG1	0x100F

//////////////////////////////////////////////////////////////////////////
//
// Model ID's
//

#define ID_CSPID1	1
#define ID_CSPID2	2
#define ID_CSTC8	3
#define ID_CSINI8	4
#define ID_CSINV8	5
#define ID_CSDIO14	6
#define ID_CSRTD6	7
#define ID_CSOPI8	8
#define ID_CSOPV8	9
#define ID_CSINI8L	10
#define ID_CSINV8L	11
#define ID_CSSG		12
#define ID_CSOUT4	13
#define ID_CSTC8ISO	14

//////////////////////////////////////////////////////////////////////////
//
// Calibration Dialog
//

class CCalibDlg : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CCalibDlg(BOOL fLinkOptions);

	// Destructor
	~CCalibDlg(void);

protected:
	// Data Members
	CCommsThread *m_pComms;
	BOOL	      m_fBusy;
	BOOL	      m_fRead;
	WORD	      m_wLinOut;
	WORD	      m_wCommit;
	BYTE	      m_bSlot;
	BYTE	      m_bCode;
	UINT	      m_uCmdsLoaded;
	UINT 	      m_uChansLoaded;
	BOOL	      m_fLinkOptions;
	CDatabase *   m_pDbase;
	BOOL	      m_fGraphite;
	UINT	      m_uModel;
	UINT	      m_uModule;
	CString	      m_Caption;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	void OnPreDestroy(void);

	// Notification Handlers
	void OnCmdSelChange(UINT uID, CWnd &Wnd);

	// Command Handlers
	BOOL OnCommandSend(UINT uID);
	BOOL OnCommandRead(UINT uID);
	BOOL OnCommandDone(UINT uID);
	BOOL OnCommsDone(UINT uID);
	BOOL OnCommsFail(UINT uID);
	BOOL OnCommsCred(UINT uID);
	BOOL OnCommandLink(UINT uID);

	// Implementation
	void SetCaption(void);
	void LoadModules(void);
	void LoadModel(void);
	void LoadChannels(UINT uModel, BYTE bSelect);
	void LoadCommands(UINT uModel, BYTE bSelect);
	void DoEnables(void);
	void DisableAll(void);
	BOOL IsPid1(UINT uModel);
	BOOL IsPid2(UINT uModel);
	BOOL IsSG(UINT uModel);
	BOOL IsOut4(UINT uModel);
	BOOL IsRtd6(UINT uModel);
	BOOL IsTC8(UINT uModel);
	BOOL IsTC8Iso(UINT uModel);
	BOOL IsInI8(UINT uModel);
	BOOL IsInI8L(UINT uModel);
	BOOL IsInV8(UINT uModel);
	BOOL IsInV8L(UINT uModel);
	BOOL IsUIn4(UINT uModel);
	BOOL IsColdJunc(UINT uCode);

	UINT GetSlot(void);
	UINT GetModule(void);
	UINT GetModel(void);
	BYTE GetChannel(void);
	BYTE GetCode(void);
	double GetFloat(void);
	WORD GetWord(void);

	void EncodeChannel(BYTE &bCode);
	void DecodeChannel(BYTE &bCode);

	void LoadConfig(void);
	void SaveConfig(void);

	BOOL Abort(CString Msg);
};

// End of File

#endif
