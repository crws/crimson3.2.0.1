
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Download Link
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SHLINK_HPP
	
#define	INCLUDE_SHLINK_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3look.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "shlink.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_SHLINK

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "shlink.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Comms Commands
//

#define	IDINIT	500
#define	IDDONE	501
#define	IDFAIL	502
#define	IDKILL	503
#define IDTERM  504
#define IDCRED  505

//////////////////////////////////////////////////////////////////////////
//								
// Link APIs
//

BOOL DLLAPI Link_Init(void);
BOOL DLLAPI Link_Term(void);
BOOL DLLAPI Link_SetDatabase(CDatabase *pDbase);
BOOL DLLAPI Link_AlwaysSend(void);
BOOL DLLAPI Link_HasVerify(void);
BOOL DLLAPI Link_HasSetTime(void);
BOOL DLLAPI Link_IsOnline(void);
BOOL DLLAPI Link_Update(CDatabase *pDbase);
BOOL DLLAPI Link_Update(CDatabase *pDbase, CString Method, CString User, CString Pass);
BOOL DLLAPI Link_Send(CDatabase *pDbase);
BOOL DLLAPI Link_Send(CDatabase *pDbase, CString Method, CString User, CString Pass);
BOOL DLLAPI Link_Verify(CDatabase *pDbase, BOOL fSilent);
BOOL DLLAPI Link_Upload(CFilename &File);
BOOL DLLAPI Link_ExtractDevCon(char cTag, CString &Text);
BOOL DLLAPI Link_SetTime(void);
BOOL DLLAPI Link_SetOptions(void);
BOOL DLLAPI Link_SetOptions(BOOL fEmulate);
BOOL DLLAPI Link_FindFirmware(CString &Path, CString const &Machine);
BOOL DLLAPI Link_HasEmulate(void);
BOOL DLLAPI Link_KillEmulate(void);
BOOL DLLAPI Link_GetEmulate(void);
BOOL DLLAPI Link_SetEmulate(BOOL fEmulate);
BOOL DLLAPI Link_WatchIsActive(void);
BOOL DLLAPI Link_WatchStart(CWnd *pWnd, PCDWORD pRefs, UINT uRefs);
BOOL DLLAPI Link_WatchUpdate(void);
BOOL DLLAPI Link_WatchGetData(PDWORD pData, PBOOL pAvail);
BOOL DLLAPI Link_WatchGetError(CString &Text);
BOOL DLLAPI Link_WatchAskForCredentials(void);
BOOL DLLAPI Link_WatchClose(void);
BOOL DLLAPI Link_StratonStart(BOOL (*pfnProc)(PVOID, UINT), PVOID pParam);
BOOL DLLAPI Link_StratonSend(PCBYTE pData, UINT uData);
UINT DLLAPI Link_StratonRecv(PBYTE pData, UINT uData);
BOOL DLLAPI Link_StratonAskForCredentials(void);
BOOL DLLAPI Link_StratonClose(void);
BOOL DLLAPI Link_GetCalibrate(void);
BOOL DLLAPI Link_SetCalibrate(void);
BOOL DLLAPI Link_GetCalibrateDlg(CWnd * pWnd, BOOL fLink);
BOOL DLLAPI Link_CanBrowse(void);
BOOL DLLAPI Link_Find(void);
BOOL DLLAPI Link_Browse(void);
BOOL DLLAPI Link_ShowFiles(void);
BOOL DLLAPI Link_DetectModel(CString &Model);

// End of File

#endif
