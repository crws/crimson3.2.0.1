
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinkAutoDetect_HPP

#define INCLUDE_LinkAutoDetect_HPP

//////////////////////////////////////////////////////////////////////////
//
// Auto Detect Dialog Box
//

class CLinkAutoDetectDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CLinkAutoDetectDialog(void);

	// Destructor
	~CLinkAutoDetectDialog(void);

	// Attributes
	CString GetModel(void) const;

public:
	// Data Members
	CCommsThread * m_pComms;
	BOOL	       m_fDone;
	BOOL	       m_fError;
	UINT	       m_uState;
	CString	       m_Model;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	void OnDeviceChange(LONG dwEvent, LONG dwData);

	// Command Handlers
	BOOL OnCommandOkay(UINT uID);
	BOOL OnCommsInit(UINT uID);
	BOOL OnCommsDone(UINT uID);
	BOOL OnCommsFail(UINT uID);
	BOOL OnCommsCred(UINT uID);

	// Implementation
	void SetDone(BOOL fError);
	void SetError(PCTXT pText);
	void ShowStatus(PCTXT pText);
	void TxFrame(void);
	BOOL RxFrame(void);
};

// End of File

#endif
