
#include "intern.hpp"

#include "../../../../Version/dbver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Download Dialog Box
//

// Externals

extern BOOL MismatchDialog(CWnd &Wnd, CDatabase *pDbase, BYTE &bFlags);

// Runtime Class

AfxImplementRuntimeClass(CLinkSendDialog, CStdDialog);

// Primary State Machine

enum
{
	stateInitCheckExecution,
	stateInitCheckControl,
	stateBootCheckLevel,
	stateBootCheckModel,
	stateBootCheckOem,
	stateBootCheckVersion,
	stateBootForceReset,
	stateBootCheckHardware,
	stateBootCheckLoader,
	stateBootClearMemory,
	stateBootWriteProgram,
	stateBootWriteVersion,
	stateBootStartProgram,
	statePromCheckVersion,
	statePromCheckLoader,
	statePromWriteProgram,
	statePromUpdateProgram,
	statePrimWriteProgram,
	statePrimUpdateProgram,
	stateConfigCheckVersion,
	stateConfigHaltSystem1,
	stateConfigClearConfig,
	stateConfigCheckEditFlags,
	stateConfigHaltSystem3,
	stateConfigClearEditFlags,
	stateConfigCheckItem,
	stateConfigHaltSystem2,
	stateConfigWriteItem,
	stateConfigWriteData,
	stateConfigClearGarbage,
	stateConfigWriteVersion,
	stateConfigWriteTime,
	stateConfigStartSystem,
	stateConfigHaltSystem,
	stateDone
};

// Reprom State Machine

enum
{
	stateRepromInit,
	stateRepromReq,
	stateRepromDone,
};

// Constructors

CLinkSendDialog::CLinkSendDialog(CDatabase *pDbase, UINT uSend)
{
	m_pDbase     = pDbase;

	m_pComms     = New CCommsThread;

	m_uSend      = uSend;

	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fVerify    = FALSE;

	m_fChanges   = TRUE;

	m_fCompact   = TRUE;

	m_fHalted    = FALSE;

	m_fWarned    = FALSE;

	m_fControl   = FALSE;

	m_fDelay     = FALSE;

	m_pFile      = NULL;

	m_uState     = stateDone;

	m_uReprom    = stateRepromInit;

	m_fAutoClose = FALSE;

	m_pSystem    = m_pDbase ? m_pDbase->GetSystemItem() : NULL;

	LoadConfig();

	SetName(L"LinkSendDlg");
}

// Destructor

CLinkSendDialog::~CLinkSendDialog(void)
{
	delete m_pComms;

	CloseFile();
}

// Operations

void CLinkSendDialog::SetMethod(CString Method)
{
	m_Method = Method;
}

void CLinkSendDialog::SetCredentials(CString User, CString Pass)
{
	m_pComms->SetCredentials(User, Pass);
}

// Message Map

AfxMessageMap(CLinkSendDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_DEVICECHANGE)

	AfxDispatchCommand(IDOK, OnCommandOkay)
	AfxDispatchCommand(IDINIT, OnCommsInit)
	AfxDispatchCommand(IDDONE, OnCommsDone)
	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDCRED, OnCommsCred)

	AfxMessageEnd(CLinkSendDialog)
};

// Message Handlers

BOOL CLinkSendDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	switch( m_uSend ) {

		case sendSend:

			m_uState   = stateInitCheckExecution;

			m_fChanges = FALSE;

			break;

		case sendUpdate:

			m_uState   = stateInitCheckExecution;

			m_fChanges = TRUE;

			break;

		case sendVerify:
		case sendSilent:

			m_uState  = stateBootStartProgram;

			m_fVerify = TRUE;

			m_fWarned = TRUE;

			SetWindowText(CString(IDS_VERIFY_DATABASE));

			GetDlgItem(101).SetWindowText(L"");

			break;

		case sendTime:

			m_uState = stateConfigWriteTime;

			SetWindowText(CString(IDS_DOWNLOAD_TIME));

			GetDlgItem(101).SetWindowText(L"");

			break;
	}

	m_pComms->SetMethod(m_Method);

	m_pComms->Bind(this);

	m_pComms->Create();

	return TRUE;
}

void CLinkSendDialog::OnDeviceChange(LONG uEvent, LONG uData)
{
	m_pComms->OnEvent(WM_DEVICECHANGE, uEvent, uData);
}

// Command Handlers

BOOL CLinkSendDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		#pragma warning(suppress: 6286)

		if( TRUE || NoYes(CString(IDS_LINK_ASK_ABORT)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_pComms->Terminate(2000);

			EndDialog(FALSE);
		}
	}
	else
		EndDialog(!m_fError);

	return TRUE;
}

BOOL CLinkSendDialog::OnCommsInit(UINT uID)
{
	TxFrame();

	return TRUE;
}

BOOL CLinkSendDialog::OnCommsDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
	}

	return TRUE;
}

BOOL CLinkSendDialog::OnCommsFail(UINT uID)
{
	if( !m_fDone ) {

		CString Text = m_pComms->GetErrorText();

		if( Text.Find('\n') < NOTHING ) {

			SetError(CString(IDS_UNABLE_TO_2));

			Error(Text);

			return TRUE;
		}

		SetError(Text);
	}

	return TRUE;
}

BOOL CLinkSendDialog::OnCommsCred(UINT uID)
{
	m_pComms->AskForCredentials();

	return TRUE;
}

// Implementation

void CLinkSendDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_pComms->Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			if( !m_fVerify || m_uSend == sendSilent ) {

				EndDialog(TRUE);

				return;
			}
		}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_LINK_CLOSE));
	}
}

void CLinkSendDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_LINK_REPORT_ERROR, pText));

	GetDlgItem(101).SetWindowText(L"");

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
}

void CLinkSendDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
}

void CLinkSendDialog::TxFrame(void)
{
	if( m_uState == stateInitCheckExecution ) {

		ShowStatus(CString(IDS_CHECKING));

		m_pComms->ConfigCheckExecution();

		return;
	}

	if( m_uState == stateInitCheckControl ) {

		ShowStatus(CString(IDS_CHECKING_CONTROL));

		m_pComms->ConfigCheckControl();

		return;
	}

	if( m_uState == stateBootCheckLevel ) {

		ShowStatus(CString(IDS_CHECKING_CURRENT));

		m_pComms->BootCheckLevel();

		return;
	}

	if( m_uState == stateBootCheckModel ) {

		ShowStatus(CString(IDS_LINK_CHECK_MODEL));

		m_pComms->BootCheckModel();

		return;
	}

	if( m_uState == stateBootCheckOem ) {

		ShowStatus(CString(IDS_LINK_CHECK_OEM));

		m_pComms->BootCheckOem(m_pDbase->GetConfig());

		return;
	}

	if( m_uState == stateBootCheckVersion ) {

		ShowStatus(CString(IDS_LINK_FIRM_VERSION));

		m_pComms->BootCheckVersion(m_FirmGuid, DBASE_FORMAT);

		return;
	}

	if( m_uState == stateBootForceReset ) {

		ShowStatus(CString(IDS_LINK_FORCE_RESET));

		m_pComms->BootForceReset(FALSE);

		return;
	}

	if( m_uState == stateBootCheckHardware ) {

		ShowStatus(CString(IDS_LINK_HARD_VERSION));

		m_pComms->BootCheckHardware();

		return;
	}

	if( m_uState == stateBootCheckLoader ) {

		ShowStatus(CString(IDS_LINK_BOOT_VER));

		m_pComms->BootCheckLoader(SkipLoader());

		return;
	}

	if( m_uState == stateBootClearMemory ) {

		m_dwAddr = 0;

		BYTE bBlocks = BYTE((m_dwSize + 0x10FFF) >> 16);

		ShowStatus(CString(IDS_LINK_CLEAR_FIRM));

		m_pComms->BootClearProgram(bBlocks);

		return;
	}

	if( m_uState == stateBootWriteProgram ) {

		UINT  uLimit = m_pComms->GetBulkLimit();

		PBYTE pData  = New BYTE[uLimit];

		UINT  uCount = fread(pData, 1, uLimit, m_pFile);

		BOOL  fLast  = (m_dwAddr + uCount >= m_dwSize);

		if( fLast ) {

			uCount = m_dwSize - m_dwAddr;
		}

		UINT  uThis  = 1 + m_dwAddr / uLimit;

		UINT  uInfo  = (m_uReprom == stateRepromReq) ? IDS_LINK_WLOAD : IDS_LINK_WFIRM;

		ShowStatus(CPrintf(uInfo, uThis));

		m_pComms->BootWriteProgram(m_dwAddr, pData, WORD(uCount), fLast);

		m_dwAddr += uCount;

		delete[] pData;

		return;
	}

	if( m_uState == stateBootWriteVersion ) {

		ShowStatus(CString(IDS_LINK_WFIRM_VERSION));

		m_pComms->BootWriteVersion(m_FirmGuid);

		return;
	}

	if( m_uState == stateBootStartProgram ) {

		ShowStatus(CString(IDS_LINK_START_FIRMWARE));

		if( m_uReprom == stateRepromDone ) {

			m_pComms->PromStartProgram();
		}
		else {
			m_pComms->BootStartProgram();
		}

		return;
	}

	if( m_uState == statePromCheckVersion ) {

		ShowStatus(CString(IDS_LINK_FIRM_VERSION));

		m_pComms->BootCheckVersion(m_FirmGuid, 0);

		return;
	}

	if( m_uState == statePromCheckLoader ) {

		ShowStatus(IDS("Checking boot loader status."));

		m_pComms->PromCheckLoader();

		return;
	}

	if( m_uState == statePromWriteProgram || m_uState == statePrimWriteProgram ) {

		UINT  uLimit = m_pComms->GetBulkLimit();

		PBYTE pData  = New BYTE[uLimit];

		UINT  uCount = fread(pData, 1, uLimit, m_pFile);

		UINT  uThis  = 1 + m_dwAddr / uLimit;

		ShowStatus(CPrintf(IDS_LINK_WBOOT, uThis));

		m_pComms->PromWriteProgram(m_dwAddr, pData, WORD(uCount));

		m_dwAddr += uCount;

		delete[] pData;

		return;
	}

	if( m_uState == statePromUpdateProgram ) {

		ShowStatus(CString(IDS_LINK_BOOT_UPDATE));

		m_pComms->PromUpdateProgram(1);

		return;
	}

	if( m_uState == statePrimUpdateProgram ) {

		ShowStatus(CString(IDS_LINK_BOOT_UPDATE));

		m_pComms->PromUpdateProgram(0);

		return;
	}

	if( m_uState == stateConfigCheckVersion ) {

		ShowStatus(CString(IDS_LINK_CONFIG_VERSION));

		CGuid Guid = m_pDbase->GetGuid();

		if( m_fDelay ) {

			Sleep(2000);
		}

		m_pComms->ConfigCheckVersion(Guid);

		return;
	}

	if( m_uState == stateConfigHaltSystem1 ) {

		ShowStatus(CString(IDS_LINK_STOP_FIRM));

		m_pComms->ConfigHaltSystem();

		return;
	}

	if( m_uState == stateConfigClearConfig ) {

		ShowStatus(CString(IDS_LINK_CLEAR_CONFIG));

		m_pComms->ConfigClearConfig();

		return;
	}

	if( m_uState == stateConfigCheckEditFlags ) {

		ShowStatus(IDS("Checking for configuration changes on device."));

		m_pComms->ConfigCheckEditFlags();

		return;
	}

	if( m_uState == stateConfigHaltSystem3 ) {

		ShowStatus(CString(IDS_LINK_STOP_FIRM));

		m_pComms->ConfigHaltSystem();

		return;
	}

	if( m_uState == stateConfigClearEditFlags ) {

		ShowStatus(IDS("Clearing configuration changes on device."));

		m_pComms->ConfigClearEditFlags(m_bClear);

		return;
	}

	if( m_uState == stateConfigCheckItem ) {

		m_uItem = m_pDbase->GetHandleValue(m_Index);

		m_pItem = m_pDbase->GetHandleItem(m_Index);

		m_InitData.SetAlign(TRUE);

		m_InitData.Empty();

		ShowStatus(CPrintf(IDS_LINK_CHECK_SIG, HANDLE_OFFSET + m_uItem));

		m_pItem->MakeInitData(m_InitData);

		m_uSize = m_InitData.GetCount();

		if( m_uItem ) {

			m_InitData.Compress();

			m_uComp = m_InitData.GetCount();
		}
		else
			m_uComp = m_uSize;

		m_pComms->ConfigCheckItem(m_uItem, 0x1234, m_InitData.GetCRC());

		m_dwTotal += m_InitData.GetCount();

		return;
	}

	if( m_uState == stateConfigHaltSystem2 ) {

		ShowStatus(CString(IDS_LINK_STOP_FIRM));

		m_pComms->ConfigHaltSystem();

		return;
	}

	if( m_uState == stateConfigWriteItem ) {

		ShowStatus(CPrintf(IDS_LINK_WHEADER, HANDLE_OFFSET + m_uItem));

		if( m_uSize == m_uComp ) {

			m_pComms->ConfigWriteItem(m_uItem, 0x1234, m_uSize);

			return;
		}

		m_pComms->ConfigWriteItemEx(m_uItem, 0x1234, m_uSize, m_uComp);

		return;
	}

	if( m_uState == stateConfigWriteData ) {

		UINT  uLimit = m_pComms->GetBulkLimit();

		UINT  uCount = min(uLimit, m_InitData.GetCount() - m_uAddr);

		PBYTE pData  = PBYTE(m_InitData.GetPointer() + m_uAddr);

		UINT  uThis  = 1 + m_uAddr / uLimit;

		ShowStatus(CPrintf(IDS_LINK_WDBLOCK, HANDLE_OFFSET + m_uItem, uThis));

		m_pComms->ConfigWriteData(m_uAddr, pData, WORD(uCount));

		m_uAddr += uCount;

		return;
	}

	if( m_uState == stateConfigClearGarbage ) {

		ShowStatus(CString(IDS_LINK_CCONFIG));

		m_pSystem->CompressData();

		m_pComms->ConfigClearGarbage();

		return;
	}

	if( m_uState == stateConfigWriteVersion ) {

		ShowStatus(CString(IDS_LINK_WCONFIG));

		CGuid Guid = m_pDbase->GetGuid();

		m_pComms->ConfigWriteVersion(Guid);

		return;
	}

	if( m_uState == stateConfigWriteTime ) {

		ShowStatus(CString(IDS_LINK_DATE_TIME));

		m_pComms->ConfigWriteTime();

		return;
	}

	if( m_uState == stateConfigStartSystem ) {

		ShowStatus(CString(IDS_LINK_START_FIRM));

		m_pComms->ConfigStartSystem();

		return;
	}

	if( m_uSend != sendTime ) {

		m_pComms->SendComplete();
	}

	if( m_fVerify ) {

		ShowStatus(CString(IDS_DONE_DEVICE));

		if( m_uSend == sendVerify ) {

			PlaySound(L"notify.wav", NULL, SND_FILENAME | SND_ASYNC);
		}

		SetDone(FALSE);

		return;
	}

	ShowStatus(CString(IDS_LINK_OP_OK));

	SetDone(FALSE);
}

BOOL CLinkSendDialog::RxFrame(void)
{
	if( m_uState == stateInitCheckExecution ) {

		BOOL fExecute;

		m_pComms->ConfigGetExecution(fExecute);

		if( fExecute ) {

			m_uState = stateInitCheckControl;
		}
		else {
			m_fControl = FALSE;

			m_uState   = stateBootCheckLevel;
		}

		return TRUE;
	}

	if( m_uState == stateInitCheckControl ) {

		BOOL fControl = m_pDbase->GetSystemItem()->HasControl();

		m_pComms->ConfigGetControl(fControl);

		m_fControl = fControl;

		m_uState   = stateBootCheckLevel;

		return TRUE;
	}

	if( m_uState == stateBootCheckLevel ) {

		UINT uLevel;

		if( m_pComms->BootGetLevel(uLevel) ) {

			if( uLevel < 1 ) {

				CString Config = m_pDbase->GetSystemItem()->GetDownloadConfig();

				if( Config == L"Colorado" && m_pComms->UsingTCPIP() ) {

					CString t;

					t += CString(IDS_THIS_UNIT_CANNOT);

					t += CString(IDS_PLEASE_CONNECT);

					Error(t);
				}
				else {
					m_uState = stateBootCheckModel;

					return TRUE;
				}
			}
			else {
				m_uState = stateBootCheckModel;

				return TRUE;
			}
		}

		SetError(CString(IDS_OPERATION_FAILED));

		return FALSE;
	}

	if( m_uState == stateBootCheckModel ) {

		m_FirmModel.Empty();

		CString DataModel = m_pDbase->GetSystemItem()->GetModel();

		if( m_pComms->BootGetModel(m_FirmModel) ) {

			BOOL fWrong = TRUE;

			if( m_FirmModel.IsEmpty() ) {

				m_FirmModel = DataModel;
			}

			if( OpenFirmware(fWrong) ) {

				if( m_pComms->UsingSim() ) {

					m_uState = stateBootCheckVersion;
				}
				else {
					m_pSystem->SetDownloadTarget(m_FirmModel);

					m_uState = stateBootCheckOem;
				}

				return TRUE;
			}

			if( m_pComms->UsingSim() ) {

				m_pComms->CycleSim();

				return TRUE;
			}

			if( !fWrong ) {

				SetError(CString(IDS_CANT_OPEN_FIRMWARE));

				return FALSE;
			}
		}

		SetError(CString(IDS_LINK_INV_MODEL));

		return FALSE;
	}

	if( m_uState == stateBootCheckOem ) {

		if( m_pComms->BootIsSameOem() ) {

			m_uState = stateBootCheckVersion;

			return TRUE;
		}

		SetError(CString(IDS_LINK_INV_MODEL));

		return FALSE;
	}

	if( m_uState == stateBootCheckVersion ) {

		if( m_pComms->BootIsSameVersion() ) {

			GetDlgItem(101).SetWindowText(L"");

			m_uState = stateBootStartProgram;

			return TRUE;
		}

		if( !m_fWarned ) {

			BOOL fUse   = m_pComms->BootCanUseDatabase();

			BOOL fAsked = m_pDbase->HasFlag(L"AskedAboutFirmware");

			if( fAsked || !Warn(fUse ? 4 : 1) ) {

				if( m_fDone ) {

					return FALSE;
				}

				if( !fAsked ) {

					m_pDbase->AddFlag(L"AskedAboutFirmware");
				}

				GetDlgItem(101).SetWindowText(L"");

				m_uState = stateBootStartProgram;

				return TRUE;
			}

			m_fWarned = TRUE;
		}

		if( Verify(FALSE, TRUE) ) {

			m_uState = stateBootForceReset;

			GetDlgItem(101).SetWindowText(L"");

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateBootForceReset ) {

		m_uState = stateBootCheckHardware;

		return TRUE;
	}

	if( m_uState == stateBootCheckHardware ) {

		if( SkipLoader() ) {

			m_uState = stateBootClearMemory;
		}
		else
			m_uState = stateBootCheckLoader;

		return TRUE;
	}

	if( m_uState == stateBootCheckLoader ) {

		DWORD dwVersion;

		if( m_pComms->BootGetRevision(dwVersion) ) {

			if( CheckReprom(dwVersion) ) {

				if( OpenProm() ) {

					if( SkipLoader() ) {

						m_uReprom = stateRepromReq;

						m_uState  = stateBootStartProgram;

						return TRUE;
					}

					if( OpenLoader() ) {

						m_uReprom = stateRepromReq;

						m_uState  = statePromCheckVersion;

						return TRUE;
					}
					else {
						OpenProm();

						m_uState = statePromCheckLoader;

						return TRUE;
					}
				}

				SetError(CString(IDS_CANT_OPEN_FIRMWARE));

				return FALSE;
			}
		}

		if( SkipLoader() ) {

			m_uReprom = stateRepromDone;

			m_uState  = stateBootStartProgram;

			GetDlgItem(101).SetWindowText(L"");

			return TRUE;
		}

		m_uState = stateBootClearMemory;

		GetDlgItem(101).SetWindowText(L"");

		return TRUE;
	}

	if( m_uState == stateBootClearMemory ) {

		m_uState = stateBootWriteProgram;

		return TRUE;
	}

	if( m_uState == stateBootWriteProgram ) {

		if( m_dwAddr >= m_dwSize ) {

			m_uState = stateBootWriteVersion;
		}

		return TRUE;
	}

	if( m_uState == stateBootWriteVersion ) {

		m_fDelay = FALSE;

		m_uState = stateBootStartProgram;

		return TRUE;
	}

	if( m_uState == stateBootStartProgram ) {

		if( SkipLoader() ) {

			if( m_uReprom == stateRepromInit ) {

				m_uState = stateBootCheckLoader;

				return TRUE;
			}

			if( m_uReprom == stateRepromReq ) {

				m_uState = statePromCheckLoader;

				return TRUE;
			}
		}
		else {
			if( m_uReprom == stateRepromReq ) {

				OpenProm();

				m_uState = statePromCheckLoader;

				return TRUE;
			}
		}

		if( m_fDelay ) {

			Sleep(3000);
		}

		m_uState = stateConfigCheckVersion;

		return TRUE;
	}

	if( m_uState == statePromCheckVersion ) {

		if( m_pComms->BootIsSameVersion() ) {

			m_uState = stateBootStartProgram;

			return TRUE;
		}

		m_uState = stateBootClearMemory;

		return TRUE;
	}

	if( m_uState == statePromCheckLoader ) {

		if( m_pComms->PromIsRunningLoader() ) {

			m_uState = statePromWriteProgram;

			m_dwAddr = 0;

			return TRUE;
		}

		if( m_pComms->UsingTCPIP() ) {

			CString t;

			t += CString(IDS_THIS_UNITS_BOOT);

			t += CString(IDS_PLEASE_CONNECT);

			Error(t);
		}

		SetError(CString(IDS_OPERATION_FAILED));

		return FALSE;
	}

	if( m_uState == statePromWriteProgram ) {

		if( m_dwAddr >= m_dwSize ) {

			m_uState = statePromUpdateProgram;
		}

		return TRUE;
	}

	if( m_uState == statePromUpdateProgram ) {

		if( SkipLoader() ) {

			m_uReprom = stateRepromDone;

			m_uState = stateBootStartProgram;

			return TRUE;
		}

		if( !SkipPrimary() ) {

			OpenPrimary();

			m_uState = statePrimWriteProgram;

			m_dwAddr = 0;

			return TRUE;
		}

		BOOL fWrong = TRUE;

		OpenFirmware(fWrong);

		m_uReprom = stateRepromDone;

		m_uState  = stateBootClearMemory;

		return TRUE;
	}

	if( m_uState == statePrimWriteProgram ) {

		if( m_dwAddr >= m_dwSize ) {

			m_uState = statePrimUpdateProgram;
		}

		return TRUE;
	}

	if( m_uState == statePrimUpdateProgram ) {

		if( SkipLoader() ) {

			m_uReprom = stateRepromDone;

			m_uState = stateBootStartProgram;

			return TRUE;
		}

		BOOL fWrong = TRUE;

		OpenFirmware(fWrong);

		if( Verify(FALSE, TRUE) ) {

			m_uReprom = stateRepromDone;

			m_uState  = stateBootClearMemory;

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateConfigCheckVersion ) {

		if( m_pComms->ConfigGetReply() ) {

			if( !m_fChanges ) {

				m_uState = stateConfigHaltSystem1;
			}
			else {
				m_uState = stateConfigCheckEditFlags;
			}

			return TRUE;
		}

		if( !m_fWarned ) {

			if( !Warn(2) ) {

				return FALSE;
			}

			m_fWarned = TRUE;
		}

		if( Verify(FALSE, FALSE) ) {

			m_uState = stateConfigHaltSystem1;

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateConfigHaltSystem1 ) {

		if( m_pComms->ConfigGetSystemHalt(m_fHalted) ) {

			if( !m_fHalted ) {

				SetError(CString(IDS_STOPPING_RUNTIME));

				return FALSE;
			}

			m_uState  = stateConfigClearConfig;
		}

		return TRUE;
	}

	if( m_uState == stateConfigClearConfig ) {

		m_bClear = 15;

		m_uState = stateConfigClearEditFlags;

		return TRUE;
	}

	if( m_uState == stateConfigCheckEditFlags ) {

		BYTE  bFlags;

		DWORD dwCheck[4];

		if( m_pComms->ConfigGetEditFlags(bFlags, dwCheck) ) {

			BYTE bDelta = bFlags;

			for( UINT n = 0; n < 4; n++ ) {

				if( bDelta & (1 << n) ) {

					CByteArray Data;

					if( m_pSystem->GetDeviceConfig(Data, n) ) {

						PCBYTE  pData = PCBYTE(Data.GetPointer());

						UINT    uData = Data.GetCount() - 1;

						if( CRC32(pData, uData) == dwCheck[n] ) {

							bDelta &= ~(1 << n);
						}
					}
					else {
						bFlags &= ~(1 << n);

						bDelta &= ~(1 << n);
					}
				}
			}

			if( bFlags ) {

				if( !m_fVerify ) {

					if( bDelta ) {

						if( !MismatchDialog(ThisObject, m_pDbase, bDelta) ) {

							m_pComms->Terminate(2000);

							EndDialog(FALSE);

							return FALSE;
						}

						if( bDelta ) {

							m_bClear = bDelta;

							m_uState = stateConfigHaltSystem3;

							return TRUE;
						}
					}
					else {
						m_bClear = bFlags;

						m_uState = stateConfigHaltSystem3;

						return TRUE;
					}
				}
			}

			PrepareData();

			m_Index   = m_pDbase->GetHeadHandle();

			m_dwTotal = 0;

			m_uState  = stateConfigCheckItem;

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateConfigHaltSystem3 ) {

		if( m_pComms->ConfigGetSystemHalt(m_fHalted) ) {

			if( !m_fHalted ) {

				SetError(CString(IDS_STOPPING_RUNTIME));

				return FALSE;
			}

			m_uState = stateConfigClearEditFlags;
		}

		return TRUE;
	}

	if( m_uState == stateConfigClearEditFlags ) {

		PrepareData();

		m_Index   = m_pDbase->GetHeadHandle();

		m_dwTotal = 0;

		m_uState  = stateConfigCheckItem;

		return TRUE;
	}

	if( m_uState == stateConfigCheckItem ) {

		if( (m_fVerify && m_uItem == HANDLE_IMAGE) || (m_fChanges && m_pComms->ConfigGetReply()) ) {

			if( m_pDbase->GetNextHandle(m_Index) ) {

				m_uState = stateConfigCheckItem;
			}
			else {
				if( m_pDbase->HasNewHandles() ) {

					PrepareData();

					m_fChanges = TRUE;

					m_Index    = m_pDbase->GetHeadHandle();

					m_dwTotal  = 0;

					m_uState   = stateConfigCheckItem;
				}
				else {
					if( !m_fHalted ) {

						m_uState = stateConfigStartSystem;
					}
					else
						m_uState = stateConfigWriteVersion;
				}
			}
		}
		else {
			m_uAddr = 0;

			if( !m_fHalted ) {

				if( FALSE ) {

					AfxTrace(L"Change on item %4.4X (%s)\n", WORD(m_uItem), m_pItem->GetClassName());

					AfxDump(m_InitData.GetPointer(), m_InitData.GetCount());
				}

				if( !m_fWarned ) {

					if( !Warn(3) ) {

						return FALSE;
					}

					m_fWarned = TRUE;
				}

				if( Verify(TRUE, FALSE) ) {

					if( m_fVerify ) {

						m_uState = stateConfigCheckVersion;
					}
					else
						m_uState = stateConfigHaltSystem2;

					return TRUE;
				}

				return FALSE;
			}

			m_uState = stateConfigWriteItem;
		}

		return TRUE;
	}

	if( m_uState == stateConfigHaltSystem2 ) {

		if( m_pComms->ConfigGetSystemHalt(m_fHalted) ) {

			if( !m_fHalted ) {

				SetError(CString(IDS_STOPPING_RUNTIME));

				return FALSE;
			}

			m_uState  = stateConfigWriteItem;
		}

		return TRUE;
	}

	if( m_uState == stateConfigWriteItem ) {

		if( !m_pComms->ConfigGetReply() ) {

			if( m_fCompact ) {

				m_fCompact = FALSE;

				m_uState   = stateConfigClearGarbage;

				return TRUE;
			}

			if( m_fChanges ) {

				m_fChanges = FALSE;

				m_uState   = stateConfigClearConfig;

				return TRUE;
			}

			SetError(CString(IDS_LINK_LARGE_DBASE));

			return FALSE;
		}

		m_uState = stateConfigWriteData;

		return TRUE;
	}

	if( m_uState == stateConfigWriteData ) {

		if( !m_pComms->ConfigGetReply() ) {

			SetError(CString(IDS_LINK_NOT_WRITTEN));

			return FALSE;
		}

		if( m_uAddr >= m_InitData.GetCount() ) {

			if( m_pDbase->GetNextHandle(m_Index) ) {

				m_uState = stateConfigCheckItem;
			}
			else {
				if( m_pDbase->HasNewHandles() ) {

					PrepareData();

					m_fChanges = TRUE;

					m_Index    = m_pDbase->GetHeadHandle();

					m_dwTotal  = 0;

					m_uState   = stateConfigCheckItem;
				}
				else
					m_uState = stateConfigWriteVersion;
			}
		}

		return TRUE;
	}

	if( m_uState == stateConfigClearGarbage ) {

		if( m_pComms->ConfigGetReply() ) {

			m_uState = stateConfigWriteItem;
		}
		else {
			m_fChanges = FALSE;

			m_uState   = stateConfigClearConfig;
		}

		return TRUE;
	}

	if( m_uState == stateConfigWriteVersion ) {

		m_uState = stateConfigStartSystem;

		return TRUE;
	}

	if( m_uState == stateConfigWriteTime ) {

		m_uState = stateConfigStartSystem;

		return TRUE;
	}

	if( m_uState == stateConfigStartSystem ) {

		m_uState = stateDone;

		CString Text;

		Text.Printf(CString(IDS_TARGET_DEVICE), m_dwTotal / 1024);

		afxThread->SetStatusText(Text);

		return TRUE;
	}

	SetError(CString(IDS_LINK_INVALID));

	return FALSE;
}

void CLinkSendDialog::PrepareData(void)
{
	m_pDbase->PrepareData();
}

BOOL CLinkSendDialog::Verify(BOOL fAllow, BOOL fFirmware)
{
	if( m_fVerify ) {

		if( fAllow ) {

			CString Text = CString(IDS_VERIFY_PROMPT_1) + CString(IDS_VERIFY_PROMPT_2);

			if( NoYes(Text) == IDYES ) {

				if( m_pComms->UsingSim() ) {

					m_pComms->Recycle(transAppToApp);
				}

				m_fVerify = FALSE;

				return TRUE;
			}
		}

		if( fFirmware ) {

			SetError(CString(IDS_DEVICE_DOES_NOT_2));
		}
		else
			SetError(CString(IDS_DEVICE_DOES_NOT));

		return FALSE;
	}

	return TRUE;
}

BOOL CLinkSendDialog::Warn(UINT uChange)
{
	if( m_Method.IsEmpty() ) {

		CString Prompt;

		if( uChange == 1 && (m_fControl || m_fChanges) ) {

			Prompt += CString(IDS_UNIT_DOES_NOT);
		}

		if( uChange == 4 && (m_fControl || m_fChanges) ) {

			Prompt += CString(IDS_UNIT_DOES_NOT_2);
		}

		if( uChange == 2 && (m_fControl || m_fChanges) ) {

			Prompt += CString(IDS_UNIT_CONTAINS);
		}

		if( uChange == 3 && m_fControl ) {

			Prompt += CString(IDS_UNIT_CONTAINS_2);
		}

		if( !Prompt.IsEmpty() ) {

			if( m_fControl ) {

				Prompt += L"\n\n";

				Prompt += CString(IDS_IEC_CONTROL);

				Prompt += L"\n\n";

				Prompt += CString(IDS_ENSURE_SYSTEM_IS);
			}

			Prompt += L"\n\n";

			if( uChange == 4 ) {

				Prompt += CString(IDS_DO_YOU_WISH_TO_3);

				UINT uCode = YesNoCancel(Prompt);

				if( uCode == IDYES ) {

					return TRUE;
				}

				if( uCode == IDNO ) {

					return FALSE;
				}
			}
			else {
				Prompt += CString(IDS_DO_YOU_WISH_TO_2);

				if( ((uChange == 3) ? YesNo(Prompt) : NoYes(Prompt)) == IDYES ) {

					return TRUE;
				}
			}

			SetDone(FALSE);

			return FALSE;
		}
	}

	return TRUE;
}

// File Helpers

BOOL CLinkSendDialog::OpenFirmware(BOOL &fWrong)
{
	return OpenFile(fWrong, 0);
}

BOOL CLinkSendDialog::OpenProm(void)
{
	BOOL fWrong = TRUE;

	return OpenFile(fWrong, 1);
}

BOOL CLinkSendDialog::OpenLoader(void)
{
	BOOL fWrong = TRUE;

	return OpenFile(fWrong, 2);
}

BOOL CLinkSendDialog::OpenPrimary(void)
{
	BOOL fWrong = TRUE;

	return OpenFile(fWrong, 3);
}

BOOL CLinkSendDialog::OpenFile(BOOL &fWrong, UINT uSlot)
{
	CStringArray Models;

	m_pSystem->GetModelList().Tokenize(Models, ',');

	for( UINT n = 0; n < Models.GetCount(); n++ ) {

		if( m_FirmModel == Models[n] ) {

			if( !m_pComms->UsingSim() ) {

				CStringArray Files;

				m_pSystem->GetModelInfo(m_FirmModel).Tokenize(Files, ',');

				if( !Files[uSlot].IsEmpty() ) {

					if( OpenFile(Files[uSlot], uSlot == 0) ) {

						return TRUE;
					}

					fWrong = FALSE;
				}
			}
			else {
				// We don't send firmware to Emulator.

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CLinkSendDialog::OpenFile(CString Name, BOOL fHasVersion)
{
	CloseFile();

	CString Machine = m_pSystem->GetProcessor();

	for( UINT p = 0; p < 2; p++ ) {

		CFilename Folder;

		if( p == 0 ) {

			Folder = afxModule->GetFilename().WithName(L"Firmware\\" + Machine);
		}
		else {
			#ifdef _DEBUG

			CString Build = L"Debug";

			#else

			CString Build = L"Release";

			#endif

			Folder = afxModule->GetFilename().WithName(L"..\\..\\..\\Runtime\\" + Machine + L"\\" + Build);
		}

		CPrintf File(L"%s\\%s.bin", Folder, Name);

		if( m_pFile = _wfopen(File, L"rb") ) {

			fseek(m_pFile, 0, SEEK_END);

			m_dwSize = ftell(m_pFile);

			if( fHasVersion ) {

				fseek(m_pFile, m_dwSize - sizeof(GUID), SEEK_SET);

				fread(&m_FirmGuid, sizeof(GUID), 1, m_pFile);

				m_dwSize -= 8;

				m_dwSize -= sizeof(GUID);

				char sBoot[12] = { 0 };

				fseek(m_pFile, m_dwSize - 32, SEEK_SET);

				fread(sBoot, sizeof(char), 8, m_pFile);

				if( !strcmp(sBoot, "BOOT-->") ) {

					fscanf(m_pFile, "%8d", PINT(&m_FirmBoot));

					m_dwSize  -= 32;
				}
				else {
					m_dwSize  -= 16;

					m_FirmBoot = 0;
				}
			}
			else {
				m_FirmGuid.CreateUnique();

				m_FirmBoot = 0;
			}

			fseek(m_pFile, 0, SEEK_SET);

			return TRUE;
		}
	}

	return FALSE;
}

void CLinkSendDialog::CloseFile(void)
{
	if( m_pFile ) {

		fclose(m_pFile);

		m_pFile = NULL;
	}
}

// Reprom Filter

BOOL CLinkSendDialog::CheckReprom(DWORD dwVersion) const
{
	if( m_fForceReprom ) {

		AfxTrace(L"Force Reprom!\n");

		return m_fForceReprom;
	}

	return dwVersion < m_FirmBoot;
}

BOOL CLinkSendDialog::SkipLoader(void)
{
	if( !m_pComms->UsingSim() ) {

		CStringArray Models;

		m_pSystem->GetModelList().Tokenize(Models, ',');

		for( UINT n = 0; n < Models.GetCount(); n++ ) {

			if( m_FirmModel == Models[n] ) {

				CStringArray Files;

				m_pSystem->GetModelInfo(m_FirmModel).Tokenize(Files, ',');

				return Files[2].IsEmpty();
			}
		}
	}

	return FALSE;
}

BOOL CLinkSendDialog::SkipPrimary(void)
{
	if( !m_pComms->UsingSim() ) {

		CStringArray Models;

		m_pSystem->GetModelList().Tokenize(Models, ',');

		for( UINT n = 0; n < Models.GetCount(); n++ ) {

			if( m_FirmModel == Models[n] ) {

				CStringArray Files;

				m_pSystem->GetModelInfo(m_FirmModel).Tokenize(Files, ',');

				return Files[3].IsEmpty();
			}
		}
	}

	return FALSE;
}

void CLinkSendDialog::LoadConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"ShLink");

	Key.MoveTo(L"Category");

	Key.MoveTo(L"Default");

	m_fForceReprom = Key.GetValue(L"Reprom", UINT(0));
}

// Debugging

PCTXT CLinkSendDialog::EnumState(UINT uState)
{
	switch( uState ) {

		case stateBootCheckLevel:	  return L"BootCheckLevel";
		case stateBootCheckModel:	  return L"BootCheckModel";
		case stateBootCheckOem:		  return L"BootCheckOem";
		case stateBootCheckVersion:	  return L"BootCheckVersion";
		case stateBootForceReset:	  return L"BootForceReset";
		case stateBootCheckHardware:	  return L"BootCheckHardware";
		case stateBootCheckLoader:	  return L"BootCheckLoader";
		case stateBootClearMemory:	  return L"BootClearMemory";
		case stateBootWriteProgram:	  return L"BootWriteProgram";
		case stateBootWriteVersion:	  return L"BootWriteVersion";
		case stateBootStartProgram:	  return L"BootStartProgram";
		case statePromCheckVersion:	  return L"PromCheckVersion";
		case statePromCheckLoader:	  return L"PromCheckLoader";
		case statePromWriteProgram:	  return L"PromWriteProgram";
		case statePromUpdateProgram:	  return L"PromUpdateProgram";
		case statePrimWriteProgram:	  return L"PrimWriteProgram";
		case statePrimUpdateProgram:	  return L"PrimUpdateProgram";
		case stateConfigCheckVersion:	  return L"ConfigCheckVersion";
		case stateConfigHaltSystem1:	  return L"ConfigHaltSystem1";
		case stateConfigClearConfig:	  return L"ConfigClearConfig";
		case stateConfigCheckItem:	  return L"ConfigCheckItem";
		case stateConfigHaltSystem2:	  return L"ConfigHaltSystem2";
		case stateConfigWriteItem:	  return L"ConfigWriteItem";
		case stateConfigWriteData:	  return L"ConfigWriteData";
		case stateConfigClearGarbage:	  return L"ConfigClearGarbage";
		case stateConfigWriteVersion:	  return L"ConfigWriteVersion";
		case stateConfigWriteTime:	  return L"ConfigWriteTime";
		case stateConfigStartSystem:	  return L"ConfigStartSystem";
		case stateConfigHaltSystem:	  return L"ConfigHaltSystem";
	}

	return L"<>";
}

// End of File
