
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Loader Base Class
//

// Constructor

CLoaderBase::CLoaderBase(void)
{
	m_pTrans     = NULL;

	m_fAsync     = FALSE;

	m_fAcceptNak = FALSE;

	m_fAcceptAck = FALSE;
}

// Attributes

CString CLoaderBase::GetErrorText(void) const
{
	return m_Error;
}

BOOL CLoaderBase::DidAuthFail(void) const
{
	return m_Rep.GetOpcode() == opAuthFailed;
}

// Binding

void CLoaderBase::Bind(ILinkTransport *pTrans, BOOL fAsync)
{
	m_pTrans = pTrans;

	m_fAsync = fAsync;
}

// Comms Operations

BOOL CLoaderBase::Transact(void)
{
	if( m_fAsync ) {

		if( m_pTrans->Transact(m_Req, m_Rep, TRUE) ) {

			if( m_fAcceptNak && m_Rep.GetOpcode() == opNak ) {

				return TRUE;
			}

			if( m_fAcceptAck && m_Rep.GetOpcode() == opAck ) {

				return TRUE;
			}

			if( m_bMatch && m_Rep.GetOpcode() != m_bMatch ) {

				UINT s1 = m_Req.GetService();

				UINT o1 = m_Req.GetOpcode();

				UINT o2 = m_Rep.GetOpcode();

				CString Text;

				Text.Printf(IDS_UNEXPECTED_REPLY, s1, o1, o2);

				m_Error = Text;

				return FALSE;
			}

			if( m_fCycle ) {

				if( m_bMatch || m_Rep.GetOpcode() == opAck ) {

					UINT uTrans = FindTransition();

					m_pTrans->Recycle(uTrans);
				}
			}

			return TRUE;
		}

		if( (m_Error = m_pTrans->GetErrorText()).IsEmpty() ) {

			m_Error = IDS("The operation failed.");
		}

		return FALSE;
	}

	return FALSE;
}

// Implementation

BOOL CLoaderBase::SyncTransact(BYTE bMatch, BOOL fCycle, BOOL fAcceptNak, BOOL fAcceptAck)
{
	if( m_fAsync ) {

		m_bMatch     = bMatch;

		m_fCycle     = fCycle;

		m_fAcceptNak = fAcceptNak;

		m_fAcceptAck = fAcceptAck;

		return TRUE;
	}
	else {
		if( m_pTrans->Transact(m_Req, m_Rep, TRUE) ) {

			if( fAcceptNak && m_Rep.GetOpcode() == opNak ) {

				return TRUE;
			}

			if( fAcceptAck && m_Rep.GetOpcode() == opAck ) {

				return TRUE;
			}

			if( bMatch && m_Rep.GetOpcode() != bMatch ) {

				UINT s1 = m_Req.GetService();

				UINT o1 = m_Req.GetOpcode();

				UINT o2 = m_Rep.GetOpcode();

				CString Text;

				Text.Printf(IDS_UNEXPECTED_REPLY, s1, o1, o2);

				return FALSE;
			}

			if( fCycle ) {

				if( bMatch || m_Rep.GetOpcode() == opAck ) {

					UINT uTrans = FindTransition();

					m_pTrans->Recycle(uTrans);
				}
			}

			return TRUE;
		}

		m_Error = m_pTrans->GetErrorText();

		return FALSE;
	}
}

UINT CLoaderBase::FindTransition(void)
{
	return transNone;
}

// End of File
