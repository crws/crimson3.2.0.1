
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Update Support
//

// Constructor

CPromLoader::CPromLoader(void)
{
	}

// Attributes

BOOL CPromLoader::IsRunningLoader(void) const
{
	return m_Rep.GetOpcode() == opAck;
	}

// Simple Operations

BOOL CPromLoader::CheckLoader(void)
{
	m_Req.StartFrame(servProm, promCheckLoader);

	return SyncTransact(opAck, FALSE, TRUE);
	}

BOOL CPromLoader::WriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount)
{
	m_Req.StartFrame(servProm, promWriteProgram);

	m_Req.AddLong(dwAddr);

	m_Req.AddWord(wCount);

	m_Req.AddBulk(pData, wCount);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CPromLoader::UpdateProgram(UINT uProm)
{
	m_Req.StartFrame(servProm, promUpdateProgram);

	m_Req.AddByte(0);

	m_Req.AddByte(BYTE(uProm));
	
	m_Req.MarkVerySlow();

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CPromLoader::StartProgram(void)
{
	m_Req.StartFrame(servBoot, bootStartProgram);

	m_Req.MarkVerySlow();

	return SyncTransact(opNull, TRUE, FALSE);
	}

// Implementation

UINT CPromLoader::FindTransition(void)
{
	switch( m_Req.GetOpcode() ) {

		case bootStartProgram:

			return transAppToApp; 
		}

	return transNone;
	}

// End of File
