
//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3LINK_HPP
	
#define	INCLUDE_G3LINK_HPP

////////////////////////////////////////////////////////////////////////
//
// G3 Link Command Flags
//

enum LinkFlags
{
	flagsAsync	= 0x02,

	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Service Codes
//

enum LinkService
{
	servBoot	= 0x01,
	servConfig	= 0x02,
	servTunnel	= 0x03,
	servProm	= 0x04,
	servData	= 0x05,
	servSysConsole  = 0x06,
	servStraton	= 0x07,
	servAuth	= 0x08
	
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Operation Codes
//

enum LinkCodes
{
	// General

	opNull			= 0x00,
	opAck			= 0x01,
	opNak			= 0x02,
	opReply			= 0x03,
	opReplyFalse		= 0x04,
	opReplyTrue		= 0x05,
	opAuthFailed		= 0x06,

	// Boot Loader

	bootCheckVersion	= 0x10,
	bootClearProgram	= 0x11,
	bootWriteProgram	= 0x12,
	bootWriteVersion	= 0x13,
	bootStartProgram	= 0x14,
	bootCheckHardware	= 0x15,
	bootForceReset		= 0x16,
	bootWriteMAC		= 0x17,
	bootWriteCalib		= 0x18,
	bootWriteModel		= 0x19,
	bootReadModel		= 0x1A,
	bootReadOem		= 0x1B,
	bootReadRevision	= 0x1C,
	bootWriteProgramEx	= 0x1D,
	bootReadSubModel	= 0x1E,
	bootWriteOem		= 0x1F,
	bootWriteIdentity	= 0x20,
	bootWriteSerialNum	= 0x21,
	bootCheckLevel          = 0x22,
	bootAutoDetect		= 0x23,

	// Configuration

	configCheckVersion	= 0x10,
	configClearConfig	= 0x11,
	configClearGarbage	= 0x12,
	configCheckItem		= 0x13,
	configWriteItem		= 0x14,
	configWriteData		= 0x15,
	configWriteVersion	= 0x16,
	configHaltSystem	= 0x17,
	configStartSystem	= 0x18,
	configWriteTime		= 0x19,
	configReadItem		= 0x1A,
	configReadData		= 0x1B,
	configFlashMount	= 0x1C,
	configFlashDismount	= 0x1D,
	configFlashVerify	= 0x1E,
	configFlashFormat	= 0x1F,
	configReadItemDP	= 0x20,
	configReadDataDP	= 0x21,
	configClearBufferRLC	= 0x22,
	configCheckModelRLC	= 0x23,
	configReadDataRLC	= 0x24,
	configWriteInitRLC	= 0x25,
	configWriteDataRLC	= 0x26,
	configReadIP		= 0x27,
	configWriteItemEx	= 0x2D,
	configCheckCompression  = 0x2E,
	configCheckControl      = 0x2F,
	configCheckEditFlags	= 0x30,
	configClearEditFlags	= 0x31,
	configIdentify		= 0x32,

	// Tunneling

	tunnelTunnel		= 0x10,

	// Reprom 
	
	promWriteProgram	= 0x10,
	promUpdateProgram	= 0x11,
	promCheckLoader		= 0x12,

	// Data Access

	dataListRead		= 0x10,

	// Straton

	stratonService		= 0x10,

	// System Console

	sysConsoleNetType	= 0x10,
	sysConsoleClearDB	= 0x11,
	sysConsoleReset		= 0x12,
	sysConsoleGetVer	= 0x13,
	sysConsoleGetAddr	= 0x14,
	sysConsoleGetPort	= 0x15,
	sysConsoleSetAddr	= 0x16,
	sysConsoleSetPort	= 0x17,
	sysConsoleSetDHCP	= 0x18,

	// Authentication

	authGetChallenge	= 0x10,
	authSendResponse	= 0x11
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CLinkFrame;

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Frame
//

class CLinkFrame
{
	public:
		// Constructor
		CLinkFrame(void);

		// Destructor
		~CLinkFrame(void);

		// Frame Creation
		void StartFrame(BYTE bService, BYTE bOpcode);

		// Operations
		void MarkAsync(void);
		void MarkVerySlow(void);
		void SetFlags(BYTE bFlags);

		// Simple Data
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(LONG lData);
		void AddGuid(GUID Guid);

		// Larger Data
		void AddData(PCBYTE pData, UINT uCount);
		void AddBulk(PCBYTE pData, UINT uCount);

		// Data Reads
		PCBYTE ReadData(UINT &uPtr, UINT uCount) const;
		GUID & ReadGuid(UINT &uPtr) const;
		BYTE   ReadByte(UINT &uPtr) const;
		WORD   ReadWord(UINT &uPtr) const;
		DWORD  ReadLong(UINT &uPtr) const;

		// Attributes
		BYTE  GetService(void) const;
		BYTE  GetOpcode(void) const;
		BYTE  GetFlags(void) const;
		BOOL  IsAsync(void) const;
		BOOL  IsVerySlow(void) const;
		UINT  GetDataSize(void) const;
		UINT  GetBulkSize(void) const;
		PBYTE GetData(void) const;
		PBYTE GetBulk(void) const;
		BYTE  GetAt(UINT uPos);

	protected:
		// Data Members	  
		BYTE  m_bService;
		BYTE  m_bOpcode;
		BYTE  m_bFlags;
		UINT  m_uDataCount;
		UINT  m_uBulkCount;
		UINT  m_uDataAlloc;
		UINT  m_uBulkAlloc;
		PBYTE m_pData;
		PBYTE m_pBulk;
		BOOL  m_fSlow;

		// Implementation
		void FreeData(void);
		void FreeBulk(void);
		void ExpandData(UINT uExtra);
		void ExpandBulk(UINT uExtra);
	};

// End of File

#endif
