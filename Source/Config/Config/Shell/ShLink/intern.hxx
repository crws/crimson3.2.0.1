
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Download Link
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_BAD_REPLY_FROM      0x4000
#define IDS_CANT_OPEN_FIRMWARE  0x4001
#define IDS_CANT_OPEN_PORT      0x4002
#define IDS_CHECKING            0x4003
#define IDS_CHECKING_CONTROL    0x4004
#define IDS_CHECKING_CURRENT    0x4005
#define IDS_CHECKING_DATABASE   0x4006
#define IDS_CLEARING_DATABASE   0x4007
#define IDS_CRIMSON_EMULATOR    0x4008
#define IDS_DEVICE_DOES_NOT     0x4009
#define IDS_DEVICE_DOES_NOT_2   0x400A
#define IDS_DHCP                0x400B
#define IDS_DISABLED            0x400C
#define IDS_DONE_DEVICE         0x400D
#define IDS_DOWNLOAD_TIME       0x400E
#define IDS_DO_YOU_REALLY       0x400F
#define IDS_DO_YOU_WISH_TO      0x4010 /* NOT USED */
#define IDS_DO_YOU_WISH_TO_2    0x4011
#define IDS_DRIVER_COULD_NOT    0x4012 /* NOT USED */
#define IDS_DRIVER_MUST_BE      0x4013 /* NOT USED */
#define IDS_EMULATOR            0x4014
#define IDS_EMULATOR_2          0x4015
#define IDS_EMULATOR_3          0x4016
#define IDS_ENSURE_SYSTEM_IS    0x4017
#define IDS_FILE_FILTER         0x4018
#define IDS_FLASH_CANNOT_FORMAT 0x4019
#define IDS_FLASH_CHECKING      0x401A
#define IDS_FLASH_DISMOUNT_CMD  0x401B
#define IDS_FLASH_ERROR         0x401C
#define IDS_FLASH_EXAMINE       0x401D
#define IDS_FLASH_FORMATTING    0x401E
#define IDS_FLASH_FORMAT_WARN   0x401F
#define IDS_FLASH_MOUNT_CMD     0x4020
#define IDS_FLASH_NONE          0x4021
#define IDS_FLASH_NOT_FORMATTED 0x4022
#define IDS_FLASH_STATE         0x4023
#define IDS_FLASH_VERIFY_FORMAT 0x4024
#define IDS_FROM_DOWNLOAD_TAB   0x4025
#define IDS_IEC_CONTROL         0x4026
#define IDS_IMPORT_FILTER       0x4027
#define IDS_INITIALIZING        0x4028
#define IDS_LINK_ABORT          0x4029
#define IDS_LINK_ASK_ABORT      0x402A
#define IDS_LINK_BOOT_UPDATE    0x402B
#define IDS_LINK_BOOT_VER       0x402C
#define IDS_LINK_CCONFIG        0x402D
#define IDS_LINK_CF_REQUIRED    0x402E
#define IDS_LINK_CHECK_MODEL    0x402F
#define IDS_LINK_CHECK_OEM      0x4030
#define IDS_LINK_CHECK_SIG      0x4031
#define IDS_LINK_CLEAR_CONFIG   0x4032
#define IDS_LINK_CLEAR_FIRM     0x4033
#define IDS_LINK_CLOSE          0x4034
#define IDS_LINK_CONFIG_VERSION 0x4035
#define IDS_LINK_DATE_TIME      0x4036
#define IDS_LINK_DISMOUNT       0x4037
#define IDS_LINK_DISMOUNTED     0x4038
#define IDS_LINK_FIRM_VERSION   0x4039
#define IDS_LINK_FORCE_RESET    0x403A
#define IDS_LINK_HARD_VERSION   0x403B
#define IDS_LINK_INVALID        0x403C
#define IDS_LINK_INV_MODEL      0x403D
#define IDS_LINK_LARGE_DBASE    0x403E
#define IDS_LINK_MOUNT          0x403F
#define IDS_LINK_MOUNTED        0x4040
#define IDS_LINK_NOT_WRITTEN    0x4041
#define IDS_LINK_NO_UP_IMAGE    0x4042
#define IDS_LINK_OPEN_TEMP_NOT  0x4043
#define IDS_LINK_OP_OK          0x4044
#define IDS_LINK_READ_DBLOCK    0x4045
#define IDS_LINK_READ_HEADER    0x4046
#define IDS_LINK_REPORT_ERROR   0x4047
#define IDS_LINK_START_FIRM     0x4048
#define IDS_LINK_START_FIRMWARE 0x4049
#define IDS_LINK_STOP_FIRM      0x404A
#define IDS_LINK_WBOOT          0x404B
#define IDS_LINK_WCONFIG        0x404C
#define IDS_LINK_WDBLOCK        0x404D
#define IDS_LINK_WFIRM          0x404E
#define IDS_LINK_WFIRM_VERSION  0x404F
#define IDS_LINK_WHEADER        0x4050
#define IDS_LINK_WLOAD          0x4051
#define IDS_LINK_YES            0x4052
#define IDS_LOCATED_IN          0x4053
#define IDS_MANUAL              0x4054
#define IDS_OPERATION_FAILED    0x4055
#define IDS_PLEASE_CONNECT      0x4056
#define IDS_PROCEED_SURE        0x4057
#define IDS_READING             0x4058
#define IDS_READING_GATEWAY     0x4059
#define IDS_READING_HOTFIX      0x405A
#define IDS_READING_IP          0x405B
#define IDS_READING_NETWORK     0x405C
#define IDS_READING_RUNTIME     0x405D
#define IDS_READING_SUBNET      0x405E
#define IDS_READING_TCP_PORT    0x405F
#define IDS_READY               0x4060
#define IDS_REBOOT_MAY_BE       0x4061 /* NOT USED */
#define IDS_RESETTING_UNIT      0x4062
#define IDS_SAME_COM_PORT       0x4063
#define IDS_SAME_NETWORK        0x4064
#define IDS_SAVE_UPLOADED       0x4065
#define IDS_SEND_CAL            0x4066
#define IDS_SEND_CAL_1          0x4067
#define IDS_SEND_CAL_2          0x4068
#define IDS_SEND_CAL_3          0x4069
#define IDS_SEND_CAL_4          0x406A
#define IDS_SEND_CAL_5          0x406B
#define IDS_SEND_CAL_6          0x406C
#define IDS_SEND_CAL_7          0x406D
#define IDS_SEND_CAL_8          0x406E
#define IDS_SEND_CAL_BAD        0x406F
#define IDS_SEND_CAL_CJ_COMP    0x4070
#define IDS_SEND_CAL_EEPROM     0x4071
#define IDS_SEND_CAL_HCM_DC     0x4072
#define IDS_SEND_CAL_INPUT      0x4073
#define IDS_SEND_CAL_INP_MV     0x4074
#define IDS_SEND_CAL_INP_PC     0x4075
#define IDS_SEND_CAL_INP_PV     0x4076
#define IDS_SEND_CAL_INP_RTD    0x4077
#define IDS_SEND_CAL_MODIFY     0x4078
#define IDS_SEND_CAL_OUT4       0x4079
#define IDS_SEND_CAL_OUT_DONE   0x407A
#define IDS_SEND_CAL_OUT_MA     0x407B
#define IDS_SEND_CAL_OUT_V      0x407C
#define IDS_SEND_CAL_READ_FAIL  0x407D
#define IDS_SEND_CAL_REQ_FAIL   0x407E
#define IDS_SEND_CAL_SAVE       0x407F
#define IDS_SEND_CAL_TRANS_FAIL 0x4080
#define IDS_SEND_EXIT_NOW       0x4081
#define IDS_SEND_INVALID        0x4082
#define IDS_SEND_LINOUT         0x4083
#define IDS_SEND_MODULE         0x4084
#define IDS_SEND_MODULE_CAL     0x4085
#define IDS_SEND_MODULE_LINOUT  0x4086
#define IDS_SEND_OP_BAD         0x4087
#define IDS_SETTING_NETWORK     0x4088
#define IDS_SETUP_PROGRAM       0x4089 /* NOT USED */
#define IDS_STOPPING_RUNTIME    0x408A
#define IDS_TARGET_ADDRESS      0x408B
#define IDS_TARGET_DEVICE       0x408C
#define IDS_THIS_OPERATION      0x408D
#define IDS_THIS_UNITS_BOOT     0x408E
#define IDS_THIS_UNIT_CANNOT    0x408F
#define IDS_UNABLE_TO           0x4090
#define IDS_UNABLE_TO_2         0x4091
#define IDS_UNABLE_TO_MODEL     0x4092
#define IDS_UNABLE_TO_WRITE     0x4093
#define IDS_UNEXPECTED_REPLY    0x4094
#define IDS_UNIT_CONTAINS       0x4095
#define IDS_UNIT_CONTAINS_2     0x4096
#define IDS_UNIT_DOES_NOT       0x4097
#define IDS_VERIFY_DATABASE     0x4098
#define IDS_VERIFY_PROMPT_1     0x4099
#define IDS_VERIFY_PROMPT_2     0x409A
#define IDS_VERIFY_PROMPT_3     0x409B /* NOT USED */
#define IDS_VERIFY_PROMPT_4     0x409C /* NOT USED */
#define IDS_VIA                 0x409D
#define IDS_WINPCAP_MUST_BE     0x409E /* NOT USED */
#define IDS_WRITING_GATEWAY     0x409F
#define IDS_WRITING_IP          0x40A0
#define IDS_WRITING_SUBNET      0x40A1
#define IDS_WRITING_TCP_PORT    0x40A2
#define IDS_XPSER_BAD_CHECK     0x40A3
#define IDS_XPSER_INVALID       0x40A4
#define IDS_XPSER_LARGE         0x40A5
#define IDS_XPSER_NAK           0x40A6
#define IDS_XPSER_NEG_ACK       0x40A7
#define IDS_XPSER_TO            0x40A8
#define IDS_XPUSB_BULK_NOT      0x40A9
#define IDS_XPUSB_CONFLICT_1    0x40AA /* NOT USED */
#define IDS_XPUSB_CONFLICT_2    0x40AB /* NOT USED */
#define IDS_XPUSB_REPLY_NOT     0x40AC
#define IDS_XPUSB_REQ_NOT       0x40AD
#define IDS_XPUSB_TRUNC         0x40AE
#define IDS_UNIT_DOES_NOT_2	0x40AF
#define IDS_DO_YOU_WISH_TO_3    0x40B0

// End of File

#endif
