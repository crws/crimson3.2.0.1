
#include "intern.hpp"

#include <setupapi.h>

#include "CheckPCap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CredentialsDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications Thread
//

// Libraries

#pragma comment(lib, "shell32")

// Externals

extern void md5(PCBYTE, UINT, PBYTE);

extern void rc4(PBYTE, UINT, PCBYTE, UINT);

// Runtime Class

AfxImplementRuntimeClass(CCommsThread, CRawThread);

// Constructor

CCommsThread::CCommsThread(void) : m_CommEvent(FALSE), m_CredEvent(FALSE)
{
	m_pWnd       = NULL;

	m_pfnProc    = NULL;

	m_pParam     = NULL;

	m_bSeq       = 0;

	m_fSim       = FALSE;

	m_pTrans     = NULL;

	m_hEmulator  = NULL;

	m_uAuthState = 0;

	m_uAuthCount = 0;

	m_fAuthSave  = TRUE;

	InitServices();
}

// Destructor

CCommsThread::~CCommsThread(void)
{
	delete m_pBoot;

	delete m_pProm;

	delete m_pConfig;

	delete m_pData;

	delete m_pTunnel;

	delete m_pStraton;

	delete m_pAuth;

	AfxRelease(m_pTrans);
}

// Attributes

BOOL CCommsThread::UsingTCPIP(void) const
{
	return m_Name == L"TCP/IP";
}

BOOL CCommsThread::UsingSim(void) const
{
	return m_fSim;
}

UINT CCommsThread::GetBulkLimit(BOOL fDown) const
{
	return m_pTrans->GetBulkLimit(fDown);
}

CString CCommsThread::GetErrorText(void) const
{
	return m_Error;
}

// Operations

void CCommsThread::SetMethod(CString Method)
{
	m_Method = Method;
}

void CCommsThread::SetCredentials(CString User, CString Pass)
{
	m_ForceUser = User;

	m_ForcePass = Pass;
}

void CCommsThread::Bind(CWnd *pWnd)
{
	if( g_fLinkEmulate ) {

		CCheckPCap::Check(TRUE);
	}

	Bind(PostToWindow, PVOID(m_pWnd = pWnd));
}

void CCommsThread::Bind(PCALLBACK pfnProc, PVOID pParam)
{
	m_pfnProc = pfnProc;

	m_pParam  = pParam;
}

BOOL CCommsThread::Terminate(DWORD Timeout)
{
	Signal(IDTERM);

	if( m_pTrans ) {

		m_pTrans->Terminate();
	}

	return CRawThread::Terminate(Timeout);
}

void CCommsThread::SendComplete(void)
{
	if( m_hEmulator ) {

		CWnd &Wnd = CWnd::FromHandle(m_hEmulator);

		if( Wnd.IsWindow() ) {

			CRect Rect = afxThread->GetMainWnd()->GetWindowRect();

			if( Wnd.IsIconic() ) {

				Wnd.ShowForeignWindow(SW_SHOWNOACTIVATE);
			}

			if( Rect.Overlaps(Wnd.GetWindowRect()) ) {

				SetForegroundWindow(Wnd);

				Wnd.ShowForeignWindow(SW_SHOW);
			}
		}
	}
}

void CCommsThread::Recycle(UINT uTrans)
{
	m_pTrans->Recycle(uTrans);
}

void CCommsThread::CycleSim(void)
{
	Transact(0);

	Sleep(100);
}

// Credentials Prompt

BOOL CCommsThread::AskForCredentials(void)
{
	if( m_Method.IsEmpty() ) {

		CString Dev, User, Pass;

		Convert(Dev, m_AuthDevice);

		Convert(User, m_AuthUser);

		Convert(Pass, m_AuthPass);

		CCredentialsDialog Dialog(Dev, User, Pass);

		if( Dialog.Execute(*afxMainWnd) ) {

			Convert(m_AuthUser, Dialog.GetUser());

			Convert(m_AuthPass, Dialog.GetPass());

			m_fAuthSave = Dialog.GetKeep();

			m_fAuthOkay = TRUE;

			m_CredEvent.Set();

			return TRUE;
		}
	}
	else {
		if( !m_ForceUser.IsEmpty() ) {

			Convert(m_AuthUser, m_ForceUser);

			Convert(m_AuthPass, m_ForcePass);

			m_fAuthSave = FALSE;

			m_fAuthOkay = TRUE;

			m_CredEvent.Set();

			return TRUE;
		}
	}

	m_fAuthOkay = FALSE;

	m_CredEvent.Set();

	return FALSE;
}

// Boot Loader

BOOL CCommsThread::BootCheckModel(void)
{
	m_pBoot->CheckModel();

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootCheckOem(CString Config)
{
	m_pBoot->CheckOem(Config);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootCheckVersion(GUID Guid, DWORD Dbase)
{
	m_pBoot->CheckVersion(Guid, Dbase);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootForceReset(BOOL fTimeout)
{
	m_pBoot->ForceReset(fTimeout);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootCheckHardware(void)
{
	m_pBoot->CheckHardware();

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootCheckLoader(BOOL fSkip)
{
	m_pBoot->CheckLoader(fSkip);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootClearProgram(BYTE bBlocks)
{
	m_pBoot->ClearProgram(bBlocks);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootWriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount, BOOL fLast)
{
	m_pBoot->WriteProgram(dwAddr, pData, wCount, fLast);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootWriteVersion(GUID Guid)
{
	m_pBoot->WriteVersion(Guid);

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootStartProgram(void)
{
	m_pBoot->StartProgram();

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootCheckLevel(void)
{
	m_pBoot->CheckLevel();

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootAutoDetect(void)
{
	m_pBoot->AutoDetect();

	Transact(servBoot);

	return TRUE;
}

BOOL CCommsThread::BootIsSameVersion(void)
{
	return m_pBoot->IsSameVersion();
}

BOOL CCommsThread::BootCanUseDatabase(void)
{
	return m_pBoot->CanUseDatabase();
}

BOOL CCommsThread::BootIsSameOem(void)
{
	return m_pBoot->IsSameOem(C3OemFeature(L"Promiscuous", FALSE));
}

BOOL CCommsThread::BootGetModel(CString &Name)
{
	return m_pBoot->GetModel(Name);
}

BOOL CCommsThread::BootGetRevision(DWORD &dwVer)
{
	return m_pBoot->GetRevision(dwVer);
}

BOOL CCommsThread::BootGetLevel(UINT &uLevel)
{
	return m_pBoot->GetLevel(uLevel);
}

BOOL CCommsThread::BootGetAutoDetect(CString &Name)
{
	return m_pBoot->GetAutoDetect(Name);
}

// Prom Loader

BOOL CCommsThread::PromCheckLoader(void)
{
	m_pProm->CheckLoader();

	Transact(servProm);

	return TRUE;
}

BOOL CCommsThread::PromWriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount)
{
	m_pProm->WriteProgram(dwAddr, pData, wCount);

	Transact(servProm);

	return TRUE;
}

BOOL CCommsThread::PromUpdateProgram(UINT uProgram)
{
	m_pProm->UpdateProgram(uProgram);

	Transact(servProm);

	return TRUE;
}

BOOL CCommsThread::PromStartProgram(void)
{
	m_pProm->StartProgram();

	Transact(servProm);

	return TRUE;
}

BOOL CCommsThread::PromIsRunningLoader(void)
{
	return m_pProm->IsRunningLoader();
}

// Calibration

void CCommsThread::CalibTxReq(BYTE bSlot, BYTE bCode, WORD wData)
{
	BYTE b[9];

	b[0] = bSlot;
	b[1] = 0;
	b[2] = SERV_CALIB;
	b[3] = BYTE(m_bSeq++);
	b[4] = OP_CALIB_REQ;
	b[5] = 0;
	b[6] = bCode;
	b[7] = HIBYTE(wData);
	b[8] = LOBYTE(wData);

	TunnelSendData(b, sizeof(b));
}

void CCommsThread::CalibTxRead(BYTE bSlot, BYTE bCode)
{
	BYTE b[7];

	b[0] = bSlot;
	b[1] = 0;
	b[2] = SERV_CALIB;
	b[3] = BYTE(m_bSeq++);
	b[4] = OP_CALIB_READ;
	b[5] = 0;
	b[6] = bCode;

	TunnelSendData(b, sizeof(b));
}

BOOL CCommsThread::CalibRxReq(void)
{
	BYTE b[38];

	if( TunnelRecvData(b, sizeof(b)) ) {

		if( b[1] == 1 ) {

			if( b[4] == OP_ACK ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsThread::CalibRxRead(WORD &wData)
{
	BYTE b[38];

	if( TunnelRecvData(b, sizeof(b)) ) {

		if( b[1] == 1 ) {

			if( b[4] == OP_REPLY ) {

				wData = MAKEWORD(b[7], b[6]);

				return TRUE;
			}
		}
	}

	return FALSE;
}


// Configuration

BOOL CCommsThread::ConfigCheckVersion(GUID Guid)
{
	m_pConfig->CheckVersion(Guid);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigClearConfig(void)
{
	m_pConfig->ClearConfig();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigClearGarbage(void)
{
	m_pConfig->ClearGarbage();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigCheckItem(UINT uItem, UINT uClass, DWORD CRC)
{
	m_pConfig->CheckItem(uItem, uClass, CRC);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigWriteItem(UINT uItem, UINT uClass, UINT uSize)
{
	m_pConfig->WriteItem(uItem, uClass, uSize);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigWriteItemEx(UINT uItem, UINT uClass, UINT uSize, UINT uComp)
{
	m_pConfig->WriteItemEx(uItem, uClass, uSize, uComp);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigWriteData(DWORD dwAddr, PBYTE pData, WORD wCount)
{
	m_pConfig->WriteData(dwAddr, pData, wCount);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigWriteVersion(GUID Guid)
{
	m_pConfig->WriteVersion(Guid);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigHaltSystem(void)
{
	m_pConfig->HaltSystem();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigStartSystem(void)
{
	m_pConfig->StartSystem();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigWriteTime(void)
{
	m_pConfig->WriteTime();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigReadItem(UINT uItem)
{
	m_pConfig->ReadItem(uItem);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigReadItem(UINT uItem, UINT uSubItem)
{
	m_pConfig->ReadItem(uItem, uSubItem);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigReadData(UINT uItem, UINT uAddr, UINT uCount)
{
	m_pConfig->ReadData(uItem, uAddr, uCount);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigFlashMount(void)
{
	m_pConfig->FlashMount();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigFlashDismount(void)
{
	m_pConfig->FlashDismount();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigFlashVerify(BOOL fRaw)
{
	m_pConfig->FlashVerify(fRaw);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigFlashFormat(void)
{
	m_pConfig->FlashFormat();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigCheckCompression(void)
{
	m_pConfig->CheckCompression();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigCheckExecution(void)
{
	m_pConfig->CheckExecution();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigCheckControl(void)
{
	m_pConfig->CheckControl();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigCheckEditFlags(void)
{
	m_pConfig->CheckEditFlags();

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigClearEditFlags(BYTE bMask)
{
	m_pConfig->ClearEditFlags(bMask);

	Transact(servConfig);

	return TRUE;
}

BOOL CCommsThread::ConfigGetReply(void)
{
	return m_pConfig->GetReply();
}

UINT CCommsThread::ConfigGetItemSize(void) const
{
	return m_pConfig->GetItemSize();
}

void CCommsThread::ConfigGetItemData(PBYTE pData, UINT uCount) const
{
	m_pConfig->GetItemData(pData, uCount);
}

BOOL CCommsThread::ConfigGetCompression(BOOL &fCompress) const
{
	return m_pConfig->GetCompression(fCompress);
}

BOOL CCommsThread::ConfigGetExecution(BOOL &fExecute) const
{
	return m_pConfig->GetExecution(fExecute);
}

BOOL CCommsThread::ConfigGetControl(BOOL &fControl) const
{
	return m_pConfig->GetControl(fControl);
}

BOOL CCommsThread::ConfigGetSystemHalt(BOOL &fHalted) const
{
	return m_pConfig->GetSystemHalt(fHalted);
}

BOOL CCommsThread::ConfigGetEditFlags(BYTE &bFlags, PDWORD pCheck) const
{
	return m_pConfig->GetEditFlags(bFlags, pCheck);
}

// Tunneling

BOOL CCommsThread::TunnelSendData(PCBYTE pData, UINT uSize)
{
	BYTE bData[64];

	memset(bData, 0, 64);

	memcpy(bData, pData, uSize);

	m_pTunnel->SendData(bData, 64);

	Transact(servTunnel);

	return TRUE;
}

BOOL CCommsThread::TunnelRecvData(PBYTE pData, UINT uSize)
{
	return m_pTunnel->RecvData(pData, uSize);
}

// Data Access

BOOL CCommsThread::DataListRead(PCDWORD pRefs, UINT uCount)
{
	m_pData->ListRead(pRefs, uCount);

	Transact(servData);

	return TRUE;
}

BOOL CCommsThread::DataGetReadData(PDWORD pData, PBOOL pAvail, UINT uCount)
{
	return m_pData->GetReadData(pData, pAvail, uCount);
}

// Straton

BOOL CCommsThread::StratonSend(PCBYTE pData, UINT uData)
{
	m_pStraton->Send(pData, uData);

	Transact(servStraton);

	return TRUE;
}

UINT CCommsThread::StratonRecv(PBYTE  pData, UINT uData)
{
	return m_pStraton->Recv(pData, uData);
}

// Events

void CCommsThread::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if( m_pTrans ) {

		m_pTrans->OnEvent(uMsg, wParam, lParam);
	}
}

// Overridables

BOOL CCommsThread::OnInit(void)
{
	return TRUE;
}

UINT CCommsThread::OnExec(void)
{
	if( InitTransport() ) {

		CWaitableList List(m_CommEvent, m_TermEvent);

		BOOL fOpen = m_pTrans->Open();

		for( ;;) {

			if( List.WaitForAnyObject(INFINITE) == waitSignal ) {

				if( List.GetObjectIndex() == 1 ) {

					break;
				}

				if( !fOpen ) {

					if( (m_Error = m_pTrans->GetErrorText()).IsEmpty() ) {

						m_Error = IDS("Failed to open transport.");
					}

					Signal(IDFAIL);

					continue;
				}

				for( ;;) {

					if( m_uAuthState == 0 ) {

						m_pAuth->GetChallenge();

						if( m_pAuth->Transact() ) {

							CByteArray Challenge;

							if( m_TermEvent.WaitForObject(0) == waitSignal ) {

								break;
							}

							if( m_pAuth->ReadChallenge(m_AuthDevice, Challenge) ) {

								if( MakeAuthResponse(Challenge) ) {

									m_uAuthState = 1;

									continue;
								}

								m_Error = IDS("The authentication process failed.");
							}
							else {
								m_uAuthState = 2;

								continue;
							}
						}

						if( (m_Error = m_pAuth->GetErrorText()).IsEmpty() ) {

							m_Error = IDS("The authentication process failed.");
						}

						Signal(IDFAIL);

						break;
					}

					if( m_uAuthState == 1 ) {

						m_pAuth->SendResponse(m_AuthUser, m_AuthResponse);

						if( m_pAuth->Transact() ) {

							if( m_TermEvent.WaitForObject(0) == waitSignal ) {

								break;
							}

							if( m_pAuth->CheckResponse() ) {

								if( m_fAuthSave ) {

									SaveCachedCredentials();
								}

								m_uAuthState = 2;

								continue;
							}

							if( m_uAuthCount == 0 ) {

								ClearCachedCredentials();
							}

							if( m_uAuthCount++ < 3 ) {

								m_uAuthState = 0;

								continue;
							}
						}

						m_Error = IDS("The authentication process failed.");

						Signal(IDFAIL);

						break;
					}

					if( m_uAuthState == 2 ) {

						if( m_uServ == 0 ) {

							if( m_fSim ) {

								KillEmulator();

								RunEmulator();
							}

							break;
						}

						CLoaderBase *pLoad = NULL;

						UINT         uCode = IDFAIL;

						switch( m_uServ ) {

							case servBoot:
								pLoad = m_pBoot;
								break;

							case servProm:
								pLoad = m_pProm;
								break;

							case servConfig:
								pLoad = m_pConfig;
								break;

							case servData:
								pLoad = m_pData;
								break;

							case servTunnel:
								pLoad = m_pTunnel;
								break;

							case servStraton:
								pLoad = m_pStraton;
								break;
						}

						if( !pLoad ) {

							m_Error = CString(IDS_SEND_INVALID);
						}
						else {
							if( pLoad->Transact() ) {

								uCode = IDDONE;
							}
							else {
								if( pLoad->DidAuthFail() ) {

									m_uAuthState = 0;

									continue;
								}

								m_Error = pLoad->GetErrorText();
							}

							if( m_TermEvent.WaitForObject(0) == waitSignal ) {

								break;
							}
						}

						Signal(uCode);

						break;
					}
				}
			}
		}

		if( fOpen ) {

			m_pTrans->Close();
		}
	}

	return 0;
}

void CCommsThread::OnTerm(void)
{
}

// Implementation

void CCommsThread::InitServices(void)
{
	m_pBoot    = New CBootLoader;

	m_pProm    = New CPromLoader;

	m_pConfig  = New CConfigLoader;

	m_pData    = New CDataAccess;

	m_pStraton = New CStratonOnline;

	m_pTunnel  = New CTunnelKing;

	m_pAuth    = New CCrimsonAuth;
}

BOOL CCommsThread::InitTransport(void)
{
	UINT uCreate = CreateTransport();

	if( uCreate == 1 ) {

		m_pTrans->OnBind(m_pWnd ? m_pWnd->GetHandle() : NULL);

		m_pBoot->Bind(m_pTrans, TRUE);

		m_pProm->Bind(m_pTrans, TRUE);

		m_pConfig->Bind(m_pTrans, TRUE);

		m_pData->Bind(m_pTrans, TRUE);

		m_pStraton->Bind(m_pTrans, TRUE);

		m_pTunnel->Bind(m_pTrans, TRUE);

		m_pAuth->Bind(m_pTrans, TRUE);

		if( m_pWnd ) {

			CString Caption = m_pWnd->GetWindowText();

			if( Caption.GetLength() ) {

				Caption += CString(IDS_VIA);

				Caption += m_Name;

				C3OemStrings(Caption);

				m_pWnd->SetWindowText(Caption);
			}
		}

		Signal(IDINIT);

		return TRUE;
	}
	else {
		if( m_fSim ) {

			if( uCreate ) {

				m_Error = CString(IDS_UNABLE_TO_MODEL);
			}
			else
				m_Error = CString(IDS_UNABLE_TO);
		}
		else
			m_Error = CString(IDS_UNABLE_TO_2);

		Signal(IDFAIL);

		return FALSE;
	}
}

void CCommsThread::Transact(UINT uServ)
{
	m_uServ = uServ;

	m_CommEvent.Set();
}

UINT CCommsThread::CreateTransport(void)
{
	if( !m_Method.IsEmpty() ) {

		m_Method.MakeUpper();

		if( m_Method.StartsWith(L"COM") ) {

			UINT com = watoi(m_Method.Mid(3));

			m_pTrans = Create_TransportSerial(com);

			m_Name   = CPrintf(L"COM%u", com);

			return 1;
		}

		if( m_Method.StartsWith(L"USB") ) {

			m_pTrans = Create_TransportUSB(C3OemGetUsbBase());

			m_Name   = "USB";

			return 1;
		}

		if( m_Method.StartsWith(L"TCP") ) {

			m_pTrans = Create_TransportNetwork(m_Method.Mid(4), 789, FALSE);

			m_Name   = "TCP/IP";

			return 1;
		}
	}
	else {
		if( g_fLinkEmulate ) {

			if( CCheckPCap::Check(FALSE) ) {

				if( RunEmulator() ) {

					m_pTrans = Create_TransportSerial(0);

					m_Name   = CString(IDS_EMULATOR);

					m_fSim   = TRUE;

					return 1;
				}
			}

			m_fSim = TRUE;

			return 0;
		}
		else {
			if( g_uLinkMethod == 0 ) {

				m_pTrans = Create_TransportSerial(g_uLinkComPort);

				m_Name   = CPrintf(L"COM%u", g_uLinkComPort);

				return 1;
			}

			if( g_uLinkMethod == 1 ) {

				m_pTrans = Create_TransportUSB(C3OemGetUsbBase());

				m_Name   = "USB";

				return 1;
			}

			if( g_uLinkMethod == 2 ) {

				m_pTrans = Create_TransportNetwork(g_sLinkTcpHost, g_uLinkTcpPort, g_fLinkTcpSlow);

				m_Name   = "TCP/IP";

				return 1;
			}
		}
	}

	return 0;
}

BOOL CCommsThread::RunEmulator(void)
{
	CString Ident = L"";

	CString Class = L"AeonWindow";

	m_hEmulator   = FindWindow(Class, NULL);

	if( !m_hEmulator ) {

		Ident	    = CPrintf(L"%8.8X", GetCurrentProcessId());

		Class	    = L"AeonWindow" + Ident;

		m_hEmulator = FindWindow(Class, NULL);

		if( !m_hEmulator ) {

			CFilename Program = FindAeonHost();

			CFilename Client  = FindEmulator();

			if( !Program.IsEmpty() && !Client.IsEmpty() ) {

				HANDLE  hEvent  = CreateEvent(NULL, FALSE, FALSE, L"AeonEvent" + Ident);

				CString Model   = g_sLinkEmModel;

				CString Params  = MakeEmulatorParams(Model, Ident, Client);

				CString Path    = Client.GetPath();

				SHELLEXECUTEINFO Info;

				memset(&Info, 0, sizeof(Info));

				Info.cbSize       = sizeof(Info);

				Info.fMask	  = SEE_MASK_UNICODE | SEE_MASK_NOCLOSEPROCESS;

				Info.lpFile       = Program;

				Info.lpParameters = Params;

				Info.lpDirectory  = Path;

				AfxTrace(L"Starting emulator with:\n[%s]\n", PCTXT(Params));

				Info.nShow        = SW_SHOW;

				if( ShellExecuteEx(&Info) ) {

					HANDLE hList[] = { Info.hProcess, hEvent };

					UINT   uCode   = WaitForMultipleObjects(2, hList, FALSE, 10000);

					CloseHandle(Info.hProcess);

					CloseHandle(hEvent);

					if( uCode == WAIT_OBJECT_0 + 1 ) {

						if( (m_hEmulator = FindWindow(Class, NULL)) ) {

							Sleep(500);

							CString Port = L"\\\\.\\pipe\\c32-program-" + Ident;

							for( UINT n = 0; n < 300; n++ ) {

								HANDLE hPort = CreateFile(Port,
											  GENERIC_READ | GENERIC_WRITE,
											  0,
											  NULL,
											  OPEN_EXISTING,
											  FILE_FLAG_OVERLAPPED,
											  NULL
								);

								if( hPort == INVALID_HANDLE_VALUE ) {

									Sleep(100);

									continue;
								}

								CloseHandle(hPort);

								Sleep(250);

								return TRUE;
							}
						}
					}

					return FALSE;
				}

				CloseHandle(hEvent);
			}

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CCommsThread::KillEmulator(void)
{
	::PostMessage(m_hEmulator, WM_COMMAND, 200, 0);

	Sleep(500);

	m_hEmulator = NULL;

	return TRUE;
}

BOOL CCommsThread::Signal(UINT uCode)
{
	if( m_pfnProc ) {

		return (*m_pfnProc)(m_pParam, uCode);
	}

	return FALSE;
}

CString CCommsThread::FindAeonHost(void)
{
	return FindComponent(L"WinHost.exe");
}

CString CCommsThread::FindEmulator(void)
{
	return FindComponent(L"CrimsonPxe.dll");
}

CString CCommsThread::FindComponent(CString Name)
{
	for( UINT p = 0; p < 2; p++ ) {

		CFilename Folder;

		if( p == 0 ) {

			Folder = afxModule->GetFilename().WithName(L"Firmware\\Win32");
		}
		else {
			#ifdef _DEBUG

			CString Build = L"Debug";

			#else

			CString Build = L"Release";

			#endif

			Folder = afxModule->GetFilename().WithName(L"..\\..\\..\\Runtime\\Win32\\" + Build);
		}

		CFilename File = Folder + L"\\" + Name;

		if( File.Exists() ) {

			return File;
		}
	}

	return L"";
}

CString CCommsThread::MakeEmulatorParams(CString Model, CString Ident, CString Client)
{
	CString   Params;

	#if !defined(_DEBUG)

	Params += L"-noconsole ";

	#endif

	Params += L"-emulate ";

	Params += CPrintf(L"-id=\"%s\" ", Ident);

	Params += CPrintf(L"-model=\"%s\" ", Model);

	Params += CPrintf(L"-emdata=\"%s\" ", GetEmulatorDataPath());

	Params += CPrintf(L"-caption=\"%s\" ", CString(IDS_CRIMSON_EMULATOR));

	Params += GetEmulatorConfig();

	Params += GetEmulatorMappings();

	Params += CPrintf("\"%s\"", Client);

	return Params;
}

CString CCommsThread::GetEmulatorConfig(void)
{
	CString   Params;

	CRegKey   Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"Emulator");

	if( Key.GetValue(L"Second", UINT(0)) ) {

		Params += L"-second ";
	}

	if( Key.GetValue(L"Options", L"").GetLength() ) {

		Params += Key.GetValue(L"Options", L"");

		Params += L" ";
	}

	return Params;
}

CString CCommsThread::GetEmulatorMappings(void)
{
	CString   Params;

	PCTXT     pName[] = { L"Port0", L"Port1", L"Port2", L"Port3", L"NIC0", L"NIC1" };

	PCTXT     pPort[] = { L"com0",  L"com1",  L"com2",  L"com3",  L"nic0", L"nic1" };

	CRegKey   Key     = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"Emulator");

	Key.MoveTo(L"Mappings");

	for( UINT n = 0; n < elements(pName); n++ ) {

		UINT m = Key.GetValue(pName[n], UINT(0));

		if( m ) {

			Params += CPrintf("-map=%s:%u ", pPort[n], m);
		}
	}

	return Params;
}

CString CCommsThread::GetEmulatorDataPath(void)
{
	CModule * pApp = afxModule->GetApp();

	CFilename Path = pApp->GetFolder(CSIDL_LOCAL_APPDATA, L"Emulator");

	return Path;
}

// Authentication

BOOL CCommsThread::MakeAuthResponse(CByteArray const &Challenge)
{
	if( (m_uAuthCount == 0 && FindCachedCredentials()) || WaitForCredentials() ) {

		CByteArray Data;

		Data.Append(BYTE(rand()));

		Data.Append(m_AuthPass);

		Data.Append(Challenge);

		BYTE hash[17];

		hash[0] = Data[0];

		md5(Data.GetPointer(), Data.GetCount(), hash + 1);

		m_AuthResponse.Empty();

		m_AuthResponse.Append(Challenge);

		m_AuthResponse.Append(hash, sizeof(hash));

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsThread::FindCachedCredentials(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	CString Dev;

	Convert(Dev, m_AuthDevice);

	Key.MoveTo(L"ShLink");

	Key.MoveTo(L"Credentials");

	Key.MoveTo(Dev);

	if( Key.GetValue(L"Valid", UINT(0)) ) {

		Key.GetValueAsBlob(L"User", m_AuthUser);

		Key.GetValueAsBlob(L"Pass", m_AuthPass);

		PCSTR pPass = "CausalSecurityAgainstPeeking";

		rc4(PBYTE(m_AuthPass.GetPointer()), m_AuthPass.GetCount(), PCBYTE(pPass), strlen(pPass));

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsThread::SaveCachedCredentials(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	CString Dev;

	Convert(Dev, m_AuthDevice);

	Key.MoveTo(L"ShLink");

	Key.MoveTo(L"Credentials");

	Key.MoveTo(Dev);

	CByteArray Hide(m_AuthPass);

	PCSTR pPass = "CausalSecurityAgainstPeeking";

	rc4(PBYTE(Hide.GetPointer()), Hide.GetCount(), PCBYTE(pPass), strlen(pPass));

	Key.SetValue(L"User", PVOID(m_AuthUser.GetPointer()), m_AuthUser.GetCount());

	Key.SetValue(L"Pass", PVOID(Hide.GetPointer()), Hide.GetCount());

	Key.SetValue(L"Valid", UINT(1));

	return TRUE;
}

BOOL CCommsThread::ClearCachedCredentials(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	CString Dev;

	Convert(Dev, m_AuthDevice);

	Key.MoveTo(L"ShLink");

	Key.MoveTo(L"Credentials");

	Key.Delete(Dev);

	return TRUE;
}

BOOL CCommsThread::WaitForCredentials(void)
{
	Signal(IDCRED);

	CWaitableList List(m_CredEvent, m_TermEvent);

	if( List.WaitForAnyObject(INFINITE) == waitSignal ) {

		if( List.GetObjectIndex() == 1 ) {

			return FALSE;
		}

		return m_fAuthOkay;
	}

	return FALSE;
}

BOOL CCommsThread::Convert(CByteArray &Data, CString const &Text)
{
	Data.Empty();

	Data.Expand(Text.GetLength());

	for( UINT n = 0; n < Text.GetLength(); n++ ) {

		Data.Append(BYTE(Text[n]));
	}

	return TRUE;
}

BOOL CCommsThread::Convert(CString &Text, CByteArray const &Data)
{
	Text.Empty();

	Text.Expand(Data.GetCount());

	for( UINT n = 0; n < Data.GetCount(); n++ ) {

		Text += WCHAR(Data[n]);
	}

	return TRUE;
}

// Window Callback

BOOL CCommsThread::PostToWindow(PVOID pParam, UINT uCode)
{
	if( pParam ) {

		CWnd *pWnd = (CWnd *) pParam;

		if( pWnd->IsWindow() ) {

			pWnd->PostMessage(WM_COMMAND, uCode);

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
