
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Extract Dialog Box
//

// Libraries

#pragma	comment(lib, "lz32.lib")

// Runtime Class

AfxImplementRuntimeClass(CLinkExtractDialog, CStdDialog);

// State Machine

enum
{
	stateBootStartProgram,
	stateConfigReadItem,
	stateConfigReadData,
	stateDone,
};

// Constructors

CLinkExtractDialog::CLinkExtractDialog(char cTag)
{
	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fAutoClose = FALSE;

	m_cTag       = cTag;

	m_pComms     = New CCommsThread;

	SetName(L"LinkExtractDlg");
}

// Destructor

CLinkExtractDialog::~CLinkExtractDialog(void)
{
	delete m_pComms;
}

// Attributes

CString CLinkExtractDialog::GetData(void) const
{
	return m_Text;
}

// Message Map

AfxMessageMap(CLinkExtractDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_DEVICECHANGE)

	AfxDispatchCommand(IDOK, OnCommandOkay)
	AfxDispatchCommand(IDINIT, OnCommsInit)
	AfxDispatchCommand(IDDONE, OnCommsDone)
	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDCRED, OnCommsCred)

	AfxMessageEnd(CLinkExtractDialog)
};

// Message Handlers

BOOL CLinkExtractDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_uState = stateBootStartProgram;

	m_pComms->Bind(this);

	m_pComms->Create();

	return TRUE;
}

void CLinkExtractDialog::OnDeviceChange(LONG uEvent, LONG uData)
{
	m_pComms->OnEvent(WM_DEVICECHANGE, uEvent, uData);
}

// Command Handlers

BOOL CLinkExtractDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		#pragma warning(suppress: 6286)

		if( TRUE || NoYes(CString(IDS_LINK_ABORT)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_pComms->Terminate(2000);

			EndDialog(!m_fError);
		}
	}

	else
		EndDialog(FALSE);

	return TRUE;
}

BOOL CLinkExtractDialog::OnCommsInit(UINT uID)
{
	TxFrame();

	return TRUE;
}

BOOL CLinkExtractDialog::OnCommsDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
	}

	return TRUE;
}

BOOL CLinkExtractDialog::OnCommsFail(UINT uID)
{
	SetError(m_pComms->GetErrorText());

	return TRUE;
}

BOOL CLinkExtractDialog::OnCommsCred(UINT uID)
{
	m_pComms->AskForCredentials();

	return TRUE;
}

// Implementation

void CLinkExtractDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_pComms->Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			EndDialog(TRUE);

			return;
		}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_LINK_CLOSE));
	}
}

void CLinkExtractDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_LINK_REPORT_ERROR, pText));

	GetDlgItem(101).SetWindowText(L"");

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
}

void CLinkExtractDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
}

void CLinkExtractDialog::TxFrame(void)
{
	if( m_uState == stateBootStartProgram ) {

		ShowStatus(CString(IDS_LINK_START_FIRMWARE));

		m_pComms->BootStartProgram();

		return;
	}

	if( m_uState == stateConfigReadItem ) {

		ShowStatus(CString(IDS_LINK_READ_HEADER));

		switch( m_cTag ) {

			case 'h': m_uItem = 0xFFFFFF01; break;
			case 'p': m_uItem = 0xFFFFFF02; break;
			case 's': m_uItem = 0xFFFFFF03; break;
			case 'u': m_uItem = 0xFFFFFF04; break;
		}

		m_pComms->ConfigReadItem(m_uItem);

		return;
	}

	if( m_uState == stateConfigReadData ) {

		UINT uLimit = m_pComms->GetBulkLimit(FALSE);

		ShowStatus(CPrintf(IDS_LINK_READ_DBLOCK, m_uAddr / uLimit + 1));

		m_uCount = min(m_uSize - m_uAddr, uLimit);

		m_pComms->ConfigReadData(m_uItem, m_uAddr, m_uCount);

		return;
	}

	ShowStatus(CString(IDS_LINK_OP_OK));

	SetDone(FALSE);
}

BOOL CLinkExtractDialog::RxFrame(void)
{
	if( m_uState == stateBootStartProgram ) {

		m_uState = stateConfigReadItem;

		return TRUE;
	}

	if( m_uState == stateConfigReadItem ) {

		if( m_uSize = m_pComms->ConfigGetItemSize() ) {

			m_uAddr  = 0;

			m_uState = stateConfigReadData;

			return TRUE;
		}

		SetError(IDS("The configuration could not be extracted."));

		return FALSE;
	}

	if( m_uState == stateConfigReadData ) {

		PBYTE pData = new BYTE[m_uCount];

		DWORD uSize = m_uCount;

		m_pComms->ConfigGetItemData(pData, m_uCount);

		m_Text += CString(PCSTR(pData), uSize);

		delete[] pData;

		if( (m_uAddr += m_uCount) == m_uSize ) {

			m_uState = stateDone;
		}

		return TRUE;
	}

	SetError(CString(IDS_LINK_INVALID));

	return FALSE;
}

// End of File
