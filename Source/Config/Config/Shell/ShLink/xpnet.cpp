
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Required Libraries
//

#pragma	comment(lib, "ws2_32.lib")

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Network Transport
//

class CNetworkTransport : public ILinkTransport
{
public:
	// Constructor
	CNetworkTransport(CString Host, UINT uPort, BOOL fSlow);

	// Destructor
	~CNetworkTransport(void);

	// Deletion
	void Release(void);

	// Management
	BOOL Open(void);
	void Recycle(UINT uType);
	void Close(void);
	void Terminate(void);

	// Attributes
	UINT    GetType(void) const;
	UINT    GetBulkLimit(BOOL fDown) const;
	CString GetErrorText(void) const;

	// Transport
	BOOL Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fExpectReply);

	// Notifications
	void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
	void OnBind(HWND hWnd);

protected:
	// Data Members
	CString m_Host;
	UINT    m_uPort;
	BOOL    m_fSlow;
	BYTE	m_bSeq;
	SOCKET  m_s;
	CString m_Error;
	BOOL	m_fFlash;
	BOOL    m_fRun;

	// Transport
	BOOL Connect(void);
	void Disconnect(void);
	BOOL PutFrame(CLinkFrame &Req);
	BOOL GetFrame(CLinkFrame &Rep, BOOL fSlow);
};

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Network Transport
//

// Instantiator

ILinkTransport * Create_TransportNetwork(CString Host, UINT uPort, BOOL fSlow)
{
	return new CNetworkTransport(Host, uPort, fSlow);
}

// Constructor

CNetworkTransport::CNetworkTransport(CString Host, UINT uPort, BOOL fSlow)
{
	m_Host   = Host;

	m_uPort  = uPort;

	m_fSlow  = fSlow;

	m_bSeq   = BYTE(rand());

	m_fRun   = FALSE;

	m_fFlash = FALSE;

	m_s      = INVALID_SOCKET;
}

// Destructor

CNetworkTransport::~CNetworkTransport(void)
{
}

// Deletion

void CNetworkTransport::Release(void)
{
	delete this;
}

// Management

BOOL CNetworkTransport::Open(void)
{
	m_fRun = TRUE;

	if( m_s == INVALID_SOCKET ) {

		if( !Connect() ) {

			m_Error = CString(IDS_CANT_OPEN_PORT);

			return FALSE;
		}

		return TRUE;
	}

	return TRUE;
}

void CNetworkTransport::Recycle(UINT uType)
{
	if( m_fFlash ) {

		Disconnect();

		Sleep(2000);

		for( UINT n = 0; n < 60; n++ ) {

			if( Connect() ) {

				return;
			}

			Sleep(1000);
		}
	}
}

void CNetworkTransport::Close(void)
{
	if( m_s != INVALID_SOCKET ) {

		Disconnect();
	}
}

void CNetworkTransport::Terminate(void)
{
	m_fRun = FALSE;
}

// Attributes

UINT CNetworkTransport::GetType(void) const
{
	return typeNetwork;
}

UINT CNetworkTransport::GetBulkLimit(BOOL fDown) const
{
	return 1280 * 8 - 12;
}

CString CNetworkTransport::GetErrorText(void) const
{
	return m_Error;
}

// Transport

BOOL CNetworkTransport::Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fExpectReply)
{
	if( PutFrame(Req) ) {

		if( GetFrame(Rep, Req.IsVerySlow()) ) {

			if( Req.GetService() == servBoot ) {

				if( Req.GetOpcode() == bootStartProgram ) {

					m_fFlash = TRUE;
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

// Notifications

void CNetworkTransport::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
}

void CNetworkTransport::OnBind(HWND hWnd)
{
}

// Transport

BOOL CNetworkTransport::Connect(void)
{
	sockaddr_in ip = { 0 };

	ip.sin_family  = AF_INET;

	ip.sin_port    = MAKEWORD(HIBYTE(m_uPort), LOBYTE(m_uPort));

	ADDRINFOW hint = { 0 };

	hint.ai_family   = AF_INET;

	hint.ai_socktype = SOCK_STREAM;

	hint.ai_protocol = IPPROTO_TCP;

	ADDRINFOW *result = NULL;

	if( GetAddrInfo(m_Host, NULL, &hint, &result) == 0 ) {

		for( ADDRINFOW *ptr=result; ptr != NULL; ptr=ptr->ai_next ) {

			if( ptr->ai_family == AF_INET ) {

				LPSOCKADDR sockaddr_ip = (LPSOCKADDR) ptr->ai_addr;

				ip.sin_addr.S_un.S_addr = (DWORD &) sockaddr_ip->sa_data[2];

				break;
			}
		}

		FreeAddrInfo(result);
	}

	if( ip.sin_addr.S_un.S_addr ) {

		m_s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		if( connect(m_s, (sockaddr *) &ip, sizeof(ip)) == 0 ) {

			u_long arg = 1;

			ioctlsocket(m_s, FIONBIO, &arg);

			return TRUE;
		}
	}

	return FALSE;
}

void CNetworkTransport::Disconnect(void)
{
	shutdown(m_s, 1);

	DWORD t0 = GetTickCount();

	DWORD tr = 5000;

	BYTE  bData[16];

	for( ;;) {

		// cppcheck-suppress uninitvar

		INT nRecv = recv(m_s, PSTR(bData), sizeof(bData), 0);

		if( nRecv == 0 ) {

			break;
		}

		if( nRecv <= 0 ) {

			if( WSAGetLastError() == WSAEWOULDBLOCK ) {

				if( GetTickCount() - t0 >= tr ) {

					break;
				}

				Sleep(5);

				continue;
			}

			break;
		}
	}

	closesocket(m_s);

	m_s = INVALID_SOCKET;
}

BOOL CNetworkTransport::PutFrame(CLinkFrame &Req)
{
	UINT  uData = Req.GetDataSize();

	UINT  uBulk = Req.GetBulkSize();

	UINT  uSize = uData + uBulk + 4;

	PBYTE pData = New BYTE[uSize + sizeof(WORD)];

	pData[0] = HIBYTE(uSize);

	pData[1] = LOBYTE(uSize);

	pData[2] = Req.GetService();

	pData[3] = ++m_bSeq;

	pData[4] = Req.GetOpcode();

	pData[5] = Req.GetFlags();

	memcpy(pData + 6, Req.GetData(), uData);

	memcpy(pData + 6 + uData, Req.GetBulk(), uBulk);

	if( send(m_s, PSTR(pData), uSize + 2, 0) > 0 ) {

		delete[] pData;

		return TRUE;
	}

	delete[] pData;

	m_Error = IDS("Unable to send request.");

	return FALSE;
}

BOOL CNetworkTransport::GetFrame(CLinkFrame &Rep, BOOL fSlow)
{
	DWORD t0 = GetTickCount();

	DWORD tr = fSlow ? 120000 : 20000;

	UINT  br = 0;

	BYTE  bData[16384];

	if( m_fSlow ) {

		tr *= 20;
	}

	for( ;;) {

		INT nRecv = recv(m_s, PSTR(bData + br), sizeof(bData) - br, 0);

		if( nRecv <= 0 ) {

			if( WSAGetLastError() == WSAEWOULDBLOCK ) {

				if( GetTickCount() - t0 >= tr ) {

					m_Error = IDS_XPUSB_REPLY_NOT;

					return FALSE;
				}

				continue;
			}

			m_Error = IDS_BAD_REPLY_FROM;

			return FALSE;
		}

		if( (br += nRecv) >= 2 ) {

			if( HIBYTE(br - 2) == bData[0] ) {

				if( LOBYTE(br - 2) == bData[1] ) {

					if( bData[3] == m_bSeq ) {

						Rep.StartFrame(bData[2], bData[4]);

						Rep.SetFlags(bData[5]);

						Rep.AddData(bData + 6, br - 6);

						return TRUE;
					}
				}
			}
		}
	}

	return FALSE;
}

// End of File
