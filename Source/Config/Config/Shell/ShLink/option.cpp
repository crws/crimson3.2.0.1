
#include "intern.hpp"

#include "LinkFindDialog.hpp"

#include "CheckPCap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Download Link
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Link Options Dialog
//

class CLinkOptionsDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructors
	CLinkOptionsDialog(BOOL fEmulate = TRUE);

protected:
	// Data Members
	BOOL	m_fSim;
	UINT	m_uMode;
	UINT	m_uCom;
	CString m_Host;
	UINT    m_uPPPP;
	BOOL	m_fSlow;
	BOOL	m_fEmulate;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

	// Command Handlers
	BOOL OnCommandOK(UINT uID);
	BOOL OnCommandCancel(UINT uID);
	BOOL OnCommandFind(UINT uID);
	BOOL OnSelect(UINT uID);
	BOOL OnSimSelect(UINT uID);
	BOOL OnSimConfig(UINT uID);

	// Implementation
	void LoadData(void);
	void SaveData(void);
	void LoadDialog(void);
	void ReadDialog(void);
	void DoEnables(void);
	void ShowTimeout(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Emulator Options Dialog
//

class CEmulatorOptionsDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructors
	CEmulatorOptionsDialog(void);

protected:
	// Data Members
	UINT    m_uPort[4];
	UINT    m_uNet[2];
	BOOL    m_fSecond;
	CString m_Options;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

	// Command Handlers
	BOOL OnCommandOK(UINT uID);
	BOOL OnCommandCancel(UINT uID);
	BOOL OnCommandClear(UINT uID);
	BOOL OnCommandShow(UINT uID);

	// Implementation
	void LoadData(void);
	void SaveData(void);
	BOOL ReadData(void);
	void LoadPorts(void);
	void LoadNet(void);
	void DeleteFrom(CFilename Path);
	BOOL IsWireless(CString Name);
	BOOL HasSecondMonitor(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Link Options Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CLinkOptionsDialog, CStdDialog);

// External API

BOOL DLLAPI Link_SetOptions(void)
{
	CLinkOptionsDialog Dlg;

	return Dlg.Execute(*afxMainWnd);
}

BOOL DLLAPI Link_SetOptions(BOOL fEmulate)
{
	CLinkOptionsDialog Dlg(fEmulate);

	return Dlg.Execute(*afxMainWnd);
}

// Constructors

CLinkOptionsDialog::CLinkOptionsDialog(BOOL fEmulate)
{
	m_fEmulate = fEmulate && Link_HasEmulate();

	SetName(L"LinkOptionsDlg");
}

// Message Map

AfxMessageMap(CLinkOptionsDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

		AfxDispatchCommand(IDOK, OnCommandOK)
		AfxDispatchCommand(IDCANCEL, OnCommandCancel)
		AfxDispatchCommand(2003, OnCommandFind)
		AfxDispatchCommand(1100, OnSelect)
		AfxDispatchCommand(1101, OnSelect)
		AfxDispatchCommand(1102, OnSelect)
		AfxDispatchCommand(1103, OnSelect)
		AfxDispatchCommand(1104, OnSelect)
		AfxDispatchCommand(1105, OnSelect)
		AfxDispatchCommand(1106, OnSelect)
		AfxDispatchCommand(3000, OnSimSelect)
		AfxDispatchCommand(3001, OnSimConfig)

		AfxMessageEnd(CLinkOptionsDialog)
};

// Message Handlers

BOOL CLinkOptionsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadData();

	LoadDialog();

	DoEnables();

	ShowTimeout();

	return TRUE;
}

// Command Handlers

BOOL CLinkOptionsDialog::OnCommandOK(UINT uID)
{
	ReadDialog();

	SaveData();

	EndDialog(TRUE);

	return TRUE;
}

BOOL CLinkOptionsDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
}

BOOL CLinkOptionsDialog::OnCommandFind(UINT uID)
{
	if( CCheckPCap::Check(TRUE) ) {

		CLinkFindDialog Dlg(FALSE);

		if( Dlg.Execute(*this) ) {

			CString Host = Dlg.GetHost();

			CString Prot = Dlg.GetProt().Mid(4);

			if( Prot != L"789" ) {

				Host += L':';

				Host += Prot;
			}

			GetDlgItem(2002).SetWindowText(Host);

			SetDlgFocus(2002);
		}
	}

	return TRUE;
}

BOOL CLinkOptionsDialog::OnSelect(UINT uID)
{
	DoEnables();

	return TRUE;
}

BOOL CLinkOptionsDialog::OnSimSelect(UINT uID)
{
	DoEnables();

	return TRUE;
}

BOOL CLinkOptionsDialog::OnSimConfig(UINT uID)
{
	CEmulatorOptionsDialog Dlg;

	if( Dlg.Execute(ThisObject) ) {

		ShowTimeout();
	}

	return TRUE;
}

// Implementation

void CLinkOptionsDialog::LoadData(void)
{
	switch( g_uLinkMethod ) {

		case 0:
			if( (m_uMode = g_uLinkComPort) > 4 ) {

				m_uCom  = m_uMode;

				m_uMode = 5;
			}
			break;

		case 1:
			m_uMode = 6;

			break;

		case 2:
			m_uMode = 7;

			m_fSlow = g_fLinkTcpSlow;

			break;
	}

	if( g_uLinkTcpPort == 789 ) {

		m_Host = g_sLinkTcpHost;
	}
	else {
		m_Host.Printf(L"%s:%u",
			      PCTXT(g_sLinkTcpHost),
			      g_uLinkTcpPort
		);
	}

	m_fSim  = g_fLinkEmulate;
}

void CLinkOptionsDialog::SaveData(void)
{
	switch( m_uMode ) {

		case 1:
		case 2:
		case 3:
		case 4:
		{
			g_uLinkMethod  = 0;

			g_uLinkComPort = m_uMode;
		}
		break;

		case 5:
		{
			g_uLinkMethod  = 0;

			g_uLinkComPort = m_uCom;
		}
		break;

		case 6:
		{
			g_uLinkMethod  = 1;

			g_sLinkUsbHost = L"";
		}
		break;

		case 7:
		{
			g_uLinkMethod  = 2;

			g_fLinkTcpSlow = m_fSlow;

			UINT uPos = m_Host.Find(':');

			if( uPos == NOTHING ) {

				g_sLinkTcpHost = m_Host;

				g_uLinkTcpPort = 789;
			}
			else {
				g_sLinkTcpHost = m_Host.Left(uPos);

				g_uLinkTcpPort = watoi(m_Host.Mid(uPos+1));
			}
		}
		break;
	}

	g_fLinkEmulate = m_fSim;
}

void CLinkOptionsDialog::LoadDialog(void)
{
	SetRadioGroup(1100, 1106, m_uMode - 1);

	CPrintf Port(L"%u", (m_uMode == 5) ? m_uCom : 5);

	GetDlgItem(2000).SetWindowText(Port);

	((CButton &) GetDlgItem(2001)).SetCheck(m_fSlow);

	GetDlgItem(2002).SetWindowText(m_Host);

	((CButton &) GetDlgItem(3000)).SetCheck(m_fSim && m_fEmulate);
}

void CLinkOptionsDialog::ReadDialog(void)
{
	m_uMode = GetRadioGroup(1100, 1106) + 1;

	m_uCom  = watoi(GetDlgItem(2000).GetWindowText());

	m_fSlow = ((CButton &) GetDlgItem(2001)).IsChecked();

	m_Host  = GetDlgItem(2002).GetWindowText();

	m_fSim  = ((CButton &) GetDlgItem(3000)).IsChecked();
}

void CLinkOptionsDialog::DoEnables(void)
{
	BOOL fSim = ((CButton &) GetDlgItem(3000)).IsChecked() && m_fEmulate;

	GetDlgItem(1100).EnableWindow(!fSim);
	GetDlgItem(1101).EnableWindow(!fSim);
	GetDlgItem(1102).EnableWindow(!fSim);
	GetDlgItem(1103).EnableWindow(!fSim);
	GetDlgItem(1104).EnableWindow(!fSim);
	GetDlgItem(1105).EnableWindow(!fSim && FALSE);
	GetDlgItem(1106).EnableWindow(!fSim);

	GetDlgItem(2000).EnableWindow(!fSim && GetRadioGroup(1100, 1106) == 4);
	GetDlgItem(2001).EnableWindow(!fSim && GetRadioGroup(1100, 1106) == 6);
	GetDlgItem(2002).EnableWindow(!fSim && GetRadioGroup(1100, 1106) == 6);
	GetDlgItem(2003).EnableWindow(!fSim && GetRadioGroup(1100, 1106) == 6);
	GetDlgItem(3000).EnableWindow(m_fEmulate);
	GetDlgItem(3001).EnableWindow(fSim);
	GetDlgItem(3002).EnableWindow(fSim);
}

void CLinkOptionsDialog::ShowTimeout(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"Emulator");

	Key.MoveTo(L"Mappings");

	BOOL fMap = FALSE;

	for( UINT s = 0; s < 4; s++ ) {

		if( Key.GetValue(CPrintf(L"Port%u", s), UINT(0)) ) {

			fMap = TRUE;
		}
	}

	for( UINT n = 0; n < 4; n++ ) {

		if( Key.GetValue(CPrintf(L"NIC%u", n), UINT(0)) ) {

			fMap = TRUE;
		}
	}

	CString Text = fMap ? CString(IDS_EMULATOR_2) : CString(IDS_EMULATOR_3);

	GetDlgItem(3002).SetWindowText(Text);
}

//////////////////////////////////////////////////////////////////////////
//
// Emulator Options Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEmulatorOptionsDialog, CStdDialog);

// Constructors

CEmulatorOptionsDialog::CEmulatorOptionsDialog(void)
{
	m_fAutoClose = FALSE;

	SetName(L"EmulatorOptionsDlg");
}

// Message Map

AfxMessageMap(CEmulatorOptionsDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

		AfxDispatchCommand(IDOK, OnCommandOK)
		AfxDispatchCommand(IDCANCEL, OnCommandCancel)
		AfxDispatchCommand(300, OnCommandClear)
		AfxDispatchCommand(301, OnCommandShow)

		AfxMessageEnd(CEmulatorOptionsDialog)
};

// Message Handlers

BOOL CEmulatorOptionsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadData();

	LoadPorts();

	LoadNet();

	CButton &Sec = (CButton &) GetDlgItem(400);

	Sec.EnableWindow(HasSecondMonitor());

	Sec.SetCheck(HasSecondMonitor() && m_fSecond);

	GetDlgItem(500).SetWindowText(m_Options);

	return TRUE;
}

// Command Handlers

BOOL CEmulatorOptionsDialog::OnCommandOK(UINT uID)
{
	if( ReadData() ) {

		Link_KillEmulate();

		SaveData();

		EndDialog(TRUE);
	}

	return TRUE;
}

BOOL CEmulatorOptionsDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
}

BOOL CEmulatorOptionsDialog::OnCommandClear(UINT uID)
{
	CString Text = CString(IDS_DO_YOU_REALLY);

	if( NoYes(Text) == IDYES ) {

		Link_KillEmulate();

		UpdateWindow();

		CModule * pApp = afxModule->GetApp();

		CFilename Path = pApp->GetFolder(CSIDL_LOCAL_APPDATA, L"Emulator");

		DeleteFrom(Path);
	}

	return TRUE;
}

BOOL CEmulatorOptionsDialog::OnCommandShow(UINT uID)
{
	CModule * pApp = afxModule->GetApp();

	CFilename Path = pApp->GetFolder(CSIDL_LOCAL_APPDATA, L"Emulator");

	ShellExecute(NULL, L"open", Path, NULL, NULL, SW_SHOWDEFAULT);

	return TRUE;
}

// Implementation

void CEmulatorOptionsDialog::LoadData(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"Emulator");

	CRegKey Map = Key;

	Map.MoveTo(L"Mappings");

	m_uPort[0] = Map.GetValue(L"Port0", UINT(0));

	m_uPort[1] = Map.GetValue(L"Port1", UINT(0));

	m_uPort[2] = Map.GetValue(L"Port2", UINT(0));

	m_uPort[3] = Map.GetValue(L"Port3", UINT(0));

	m_uNet[0] = Map.GetValue(L"NIC0", UINT(0));

	m_uNet[1] = Map.GetValue(L"NIC1", UINT(0));

	m_fSecond  = Key.GetValue(L"Second", UINT(0));

	m_Options  = Key.GetValue(L"Options", L"");
}

void CEmulatorOptionsDialog::SaveData(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"Emulator");

	CRegKey Map = Key;

	Map.MoveTo(L"Mappings");

	Map.SetValue(L"Port0", m_uPort[0]);

	Map.SetValue(L"Port1", m_uPort[1]);

	Map.SetValue(L"Port2", m_uPort[2]);

	Map.SetValue(L"Port3", m_uPort[3]);

	Map.SetValue(L"NIC0", m_uNet[0]);

	Map.SetValue(L"NIC1", m_uNet[1]);

	Key.SetValue(L"Second", m_fSecond);

	Key.SetValue(L"Options", m_Options);
}

BOOL CEmulatorOptionsDialog::ReadData(void)
{
	if( TRUE ) {

		DWORD dwMask = 0;

		for( UINT s = 0; s < elements(m_uPort); s++ ) {

			CComboBox &Combo = (CComboBox &) GetDlgItem(100+s);

			UINT      uPort  = Combo.GetCurSelData();

			if( uPort ) {

				if( dwMask & (1<<uPort) ) {

					CString Text = CString(IDS_SAME_COM_PORT);

					Error(Text);

					return FALSE;
				}

				dwMask |= (1<<uPort);
			}

			m_uPort[s] = uPort;
		}
	}

	if( TRUE ) {

		DWORD dwMask = 0;

		for( UINT n = 0; n < elements(m_uNet); n++ ) {

			CComboBox &Combo = (CComboBox &) GetDlgItem(200+n);

			UINT      uPort  = Combo.GetCurSelData();

			if( uPort ) {

				if( dwMask & (1<<uPort) ) {

					CString Text = CString(IDS_SAME_NETWORK);

					Error(Text);

					return FALSE;
				}

				dwMask |= (1<<uPort);
			}

			m_uNet[n] = uPort;
		}
	}

	m_fSecond = ((CButton &) GetDlgItem(400)).IsChecked();

	m_Options = GetDlgItem(500).GetWindowText();

	return TRUE;
}

void CEmulatorOptionsDialog::LoadPorts(void)
{
	for( UINT p = 0; p <= 8; p++ ) {

		CString COM;

		if( p )
			COM.Printf(L"COM%u", p);
		else
			COM = L"Disabled";

		for( UINT s = 0; s < elements(m_uPort); s++ ) {

			CComboBox &Combo = (CComboBox &) GetDlgItem(100+s);

			Combo.AddString(COM, p);
		}
	}

	for( UINT s = 0; s < elements(m_uPort); s++ ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(100+s);

		Combo.SelectData(DWORD(m_uPort[s]));
	}
}

void CEmulatorOptionsDialog::LoadNet(void)
{
	UINT             uIndex = 1;

	DWORD		 dwSize = 32768;

	IP_ADAPTER_INFO *pData = (IP_ADAPTER_INFO *) malloc(dwSize);

	IP_ADAPTER_INFO *pWalk = pData;

	if( TRUE ) {

		for( UINT n = 0; n < elements(m_uNet); n++ ) {

			CComboBox &Combo = (CComboBox &) GetDlgItem(200+n);

			Combo.AddString(CString(IDS_DISABLED), 0);
		}
	}

	if( GetAdaptersInfo(pData, &dwSize) == ERROR_SUCCESS ) {

		while( pWalk ) {

			BOOL    fUse = FALSE;

			CString Name = L"";

			if( pWalk->Type == MIB_IF_TYPE_ETHERNET ) {

				if( pWalk->AddressLength == 6 ) {

					Name = pWalk->Description;

					if( !IsWireless(Name) ) {

						fUse = TRUE;
					}
				}
			}

			if( fUse ) {

				UINT uPos = Name.Find(L" - ");

				if( uPos < NOTHING ) {

					Name = Name.Left(uPos);
				}

				Name.TrimBoth();

				for( UINT n = 0; n < elements(m_uNet); n++ ) {

					CComboBox &Combo = (CComboBox &) GetDlgItem(200+n);

					Combo.AddString(Name, uIndex);
				}
			}

			pWalk = pWalk->Next;

			uIndex++;
		}
	}

	for( UINT n = 0; n < elements(m_uNet); n++ ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(200+n);

		Combo.SelectData(m_uNet[n]);
	}
}

void CEmulatorOptionsDialog::DeleteFrom(CFilename Path)
{
	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(Path + L"\\*.*", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			if( Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) {

				if( Data.cFileName[0] != '.' ) {

					CFilename Name = Path + L'\\' + Data.cFileName;

					DeleteFrom(Name);

					RemoveDirectory(Name);
				}

				continue;
			}

			DeleteFile(Path + L'\\' + Data.cFileName);

		} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
	}
}

BOOL CEmulatorOptionsDialog::IsWireless(CString Name)
{
	return FALSE;
}

BOOL CEmulatorOptionsDialog::HasSecondMonitor(void)
{
	return GetSystemMetrics(80) > 1;
}

// End of File
