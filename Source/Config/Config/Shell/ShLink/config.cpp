
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Configuration Support
//

// Constructor

CConfigLoader::CConfigLoader(void)
{
	m_uSize = 0;
	}

// Attributes

BOOL CConfigLoader::GetReply(void) const
{
	if( m_Rep.GetOpcode() == opReply ) {

		BYTE b = m_Rep.GetData()[0];

		return b ? TRUE : FALSE;
		}

	return FALSE;
	}

UINT CConfigLoader::GetItemSize(void) const
{
	if( m_Rep.GetOpcode() == opReply ) {

		UINT uPtr = 0;

		return m_Rep.ReadLong(uPtr);
		}

	return 0;
	}

void CConfigLoader::GetItemData(PBYTE pData, UINT uCount) const
{
	if( m_Rep.GetOpcode() == opReply ) {

		UINT uPtr = 0;

		memcpy(pData, m_Rep.ReadData(uPtr, uCount), uCount);
		}
	}

BOOL CConfigLoader::GetCompression(BOOL &fCompress) const
{
	if( m_Rep.GetOpcode() == opNak ) {
		
		return TRUE;
		}

	if( m_Req.GetOpcode() == configCheckCompression ) {

		fCompress = m_Rep.GetData()[0] ? TRUE : FALSE;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetExecution(BOOL &fExecute) const
{
	if( m_Rep.GetOpcode() == opNak ) {

		fExecute = FALSE;

		return TRUE;
		}

	if( m_Req.GetOpcode() == configCheckCompression ) {

		fExecute = TRUE;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetControl(BOOL &fControl) const
{
	if( m_Rep.GetOpcode() == opNak ) {

		return TRUE;
		}

	if( m_Req.GetOpcode() == configCheckControl ) {

		fControl = m_Rep.GetData()[0] ? TRUE : FALSE;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetSystemHalt(BOOL &fHalted) const
{
	if( m_Req.GetOpcode() == configHaltSystem ) {

		if( m_Rep.GetDataSize() ) {

			fHalted = m_Rep.GetData()[0] ? TRUE : FALSE;

			return TRUE;
			}
		}

	fHalted = TRUE;

	return TRUE;
	}

BOOL CConfigLoader::GetEditFlags(BYTE &bFlags, PDWORD pCheck) const
{
	if( m_Req.GetOpcode() == configCheckEditFlags) {

		UINT uPtr = 0;

		for( UINT n = 0; n < 4; n++ ) {

			pCheck[n] = m_Rep.ReadLong(uPtr);
		}

		bFlags = m_Rep.ReadByte(uPtr);

		return TRUE;
	}

	return FALSE;
}

UINT CConfigLoader::GetIdentity(PBYTE pData, UINT uCount) const
{
	if( m_Rep.GetOpcode() == opReply ) {

		UINT uPtr  = 0;

		UINT uSize = m_Rep.ReadByte(uPtr);

		MakeMin(uSize, uCount);

		memcpy(pData, m_Rep.ReadData(uPtr, uSize), uSize);

		return uSize;
	}

	return 0;
}

// Operations

BOOL CConfigLoader::CheckVersion(GUID Guid)
{
	m_Req.StartFrame(servConfig, configCheckVersion);

	m_Req.AddData(PBYTE(&Guid), sizeof(Guid));

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::ClearConfig(void)
{
	m_Req.StartFrame(servConfig, configClearConfig);

	m_Req.MarkVerySlow();

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CConfigLoader::ClearGarbage(void)
{
	m_Req.StartFrame(servConfig, configClearGarbage);

	m_Req.MarkVerySlow();

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::CheckItem(UINT uItem, UINT uClass, DWORD CRC)
{
	m_Req.StartFrame(servConfig, configCheckItem);

	m_Req.AddWord(WORD(uItem));

	m_Req.AddWord(WORD(uClass));

	m_Req.AddLong(CRC);

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::WriteItem(UINT uItem, UINT uClass, UINT uSize)
{
	m_Req.StartFrame(servConfig, configWriteItem);

	m_Req.AddWord(WORD(uItem));

	m_Req.AddWord(WORD(uClass));

	m_Req.AddLong(uSize);

	m_uSize = uSize;

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::WriteItemEx(UINT uItem, UINT uClass, UINT uSize, UINT uComp)
{
	m_Req.StartFrame(servConfig, configWriteItemEx);

	m_Req.AddWord(WORD(uItem));

	m_Req.AddWord(WORD(uClass));

	m_Req.AddLong(uSize);

	m_Req.AddLong(uComp);

	m_uSize = uSize;

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::WriteData(DWORD dwAddr, PBYTE pData, WORD wCount)
{
	m_Req.StartFrame(servConfig, configWriteData);

	m_Req.AddLong(dwAddr);

	m_Req.AddWord(wCount);

	m_Req.AddBulk(pData, wCount);

	if( m_uSize >= 128 * 1024 ) {
		
		if( dwAddr + wCount == m_uSize ) {

			m_Req.MarkVerySlow();
			}
		}

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::WriteVersion(GUID Guid)
{
	m_Req.StartFrame(servConfig, configWriteVersion);

	m_Req.AddData(PBYTE(&Guid), sizeof(Guid));

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CConfigLoader::HaltSystem(void)
{
	m_Req.StartFrame(servConfig, configHaltSystem);

	m_Req.AddLong(30000);

	m_Req.MarkVerySlow();

	return SyncTransact(opReply, FALSE, FALSE, TRUE);
	}

BOOL CConfigLoader::StartSystem(void)
{
	m_Req.StartFrame(servConfig, configStartSystem);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CConfigLoader::WriteTime(void)
{
	m_Req.StartFrame(servConfig, configWriteTime);

	TIME_ZONE_INFORMATION Zone;

	SYSTEMTIME            Time;

	DWORD dInfo = GetTimeZoneInformation(&Zone);

	WORD  wZone = WORD((12*60)-Zone.Bias);
	
	BYTE  bSave = BYTE(dInfo == TIME_ZONE_ID_DAYLIGHT);

	GetLocalTime(&Time);

	m_Req.AddByte(BYTE(Time.wSecond));
	m_Req.AddByte(BYTE(Time.wMinute));
	m_Req.AddByte(BYTE(Time.wHour));
	m_Req.AddByte(BYTE(Time.wDay));
	m_Req.AddByte(BYTE(Time.wMonth));
	m_Req.AddByte(BYTE(Time.wYear%100));

	m_Req.AddWord(wZone);
	m_Req.AddByte(bSave);

	return SyncTransact(opAck, FALSE, FALSE);
	}

BOOL CConfigLoader::ReadItem(UINT uItem)
{
	m_Req.StartFrame(servConfig, configReadItem);

	m_Req.AddWord(WORD(uItem));

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::ReadItem(UINT uItem, UINT uSubItem)
{
	m_Req.StartFrame(servConfig, configReadItem);

	m_Req.AddWord(WORD(uItem));

	m_Req.AddWord(WORD(uSubItem));

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::ReadData(UINT uItem, UINT uAddr, UINT uCount)
{
	m_Req.StartFrame(servConfig, configReadData);

	m_Req.AddWord(WORD(uItem));
	m_Req.AddLong(LONG(uAddr));
	m_Req.AddWord(WORD(uCount));

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::ReadDataDP(UINT uItem, UINT uAddr, UINT uCount)
{
	m_Req.StartFrame(servConfig, configReadDataDP);

	m_Req.AddWord(WORD(uItem));
	m_Req.AddLong(LONG(uAddr));
	m_Req.AddWord(WORD(uCount));

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::FlashMount(void)
{
	m_Req.StartFrame(servConfig, configFlashMount);

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::FlashDismount(void)
{
	m_Req.StartFrame(servConfig, configFlashDismount);

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::FlashVerify(BOOL fRaw)
{
	m_Req.StartFrame(servConfig, configFlashVerify);

	m_Req.AddByte(BYTE(fRaw));

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::FlashFormat(void)
{
	m_Req.StartFrame(servConfig, configFlashFormat);

	m_Req.MarkVerySlow();

	return SyncTransact(opReply, FALSE, FALSE);
	}

BOOL CConfigLoader::CheckCompression(void)
{
	m_Req.StartFrame(servConfig, configCheckCompression);

	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CConfigLoader::CheckExecution(void)
{
	m_Req.StartFrame(servConfig, configCheckCompression);

	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CConfigLoader::CheckControl(void)
{
	m_Req.StartFrame(servConfig, configCheckControl);

	return SyncTransact(opReply, FALSE, TRUE);
	}

BOOL CConfigLoader::CheckEditFlags(void)
{
	m_Req.StartFrame(servConfig, configCheckEditFlags);

	return SyncTransact(opReply, FALSE, FALSE);
}

BOOL CConfigLoader::ClearEditFlags(BYTE bMask)
{
	m_Req.StartFrame(servConfig, configClearEditFlags);

	m_Req.AddByte(bMask);

	return SyncTransact(opReply, FALSE, FALSE);
}

BOOL CConfigLoader::Identify(BYTE bCode)
{
	m_Req.StartFrame(servConfig, configIdentify);

	m_Req.AddByte(bCode);

	return SyncTransact(opReply, FALSE, FALSE);
}

// End of File
