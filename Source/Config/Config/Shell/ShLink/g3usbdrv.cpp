
#include "intern.hpp"

#include "g3usbdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// WDM USB User Mode Driver
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CUSBDriver
//

// Constructor

CUSBDriver::CUSBDriver(void)
{
	Init();
	
	OpenOverlapped();
	}

// Destructor

CUSBDriver::~CUSBDriver(void)
{
	CloseOverlapped();

	Close();
	}

// Attributes

BOOL CUSBDriver::IsValid(void) const
{
	return m_hHandle != INVALID_HANDLE_VALUE;
	}

UINT CUSBDriver::GetVendor(void) const
{
	return m_pDevice ? m_pDevice->m_uVend : NOTHING;
	}

UINT CUSBDriver::GetProduct(void) const
{
	return m_pDevice ? m_pDevice->m_uProd : NOTHING;
	}

CString CUSBDriver::GetSN(void) const
{
	return m_pDevice ? m_pDevice->m_Serial : L"";
	}

// Operations

BOOL CUSBDriver::Open(void)
{
	return Open(PRODUCT_ANY, PRODUCT_ANY, SERIAL_ANY);
	}

BOOL CUSBDriver::Open(UINT uProd)
{
	return Open(uProd, 0xFFFF, SERIAL_ANY);
	}

BOOL CUSBDriver::Open(UINT uProd, UINT uMask)
{
	return Open(uProd, uMask, SERIAL_ANY);
	}

BOOL CUSBDriver::Open(CString Serial)
{
	return Open(PRODUCT_ANY, PRODUCT_ANY, Serial);
	}

BOOL CUSBDriver::Open(UINT uProd, UINT uMask, CString Serial)
{
	if( IsValid() ) {

		return FALSE;
		}

	if( !FindDevList() ) {

		return FALSE;
		}

	for( UINT i = 0; i < m_DevList.GetCount(); i ++ ) {

		CDevice *pDevice = m_DevList[i];
		
		if( (uProd & uMask) != (pDevice->m_uProd & uMask) ) {

			continue;
			}
		
		if( Serial != SERIAL_ANY && Serial != pDevice->m_Serial ) {

			continue;
			}

		m_hHandle = CreateFile( pDevice->m_Path,
					GENERIC_READ | GENERIC_WRITE,
					FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL,
					OPEN_EXISTING,
					FILE_FLAG_OVERLAPPED,
					NULL
					);

		if( m_hHandle != INVALID_HANDLE_VALUE ) {

			m_pDevice = pDevice;

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CUSBDriver::Send(PBYTE pData, UINT &uSize)
{
	AfxAssert(IsValid());

	if( !WriteFile(m_hHandle, pData, uSize, PDWORD(&uSize), &m_TxDataOS) ) {

		if( GetLastError() != ERROR_IO_PENDING ) {

			uSize = 0;

			return FALSE;
			}

		if( !GetOverlappedResult(m_hHandle, &m_TxDataOS, PDWORD(&uSize), TRUE) ) {

			uSize = 0;

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CUSBDriver::Recv(PBYTE pData, UINT &uSize, UINT uTimeout)
{
	AfxAssert(IsValid());

	if( !m_fRxPend ) {

		if( ReadFile(m_hHandle, PVOID(pData), uSize, PDWORD(&uSize), &m_RxDataOS) ) {

			return TRUE;
			}

		if( GetLastError() != ERROR_IO_PENDING ) {

			uSize = 0;
			
			return FALSE;
			}
		}

	if( WaitForSingleObject(m_RxDataOS.hEvent, uTimeout) != WAIT_TIMEOUT ) {

		m_fRxPend = FALSE;
		
		if( !GetOverlappedResult(m_hHandle, &m_RxDataOS, PDWORD(&uSize), TRUE) ) {

			uSize = 0;

			return FALSE;
			}

		return TRUE;
		}

	m_fRxPend = TRUE;

	uSize     = 0;

	return FALSE;
	}

void CUSBDriver::CancelIO(void)
{
	if( m_fRxPend ) {

		CancelIo(m_hHandle);

		m_fRxPend = FALSE;
		}
	}
	
void CUSBDriver::Close(void)
{
	CancelIO();

	ClearDevList();
	
	if( m_hHandle != INVALID_HANDLE_VALUE ) {
		
		CloseHandle(m_hHandle);

		m_hHandle = INVALID_HANDLE_VALUE;
		}
	}

void CUSBDriver::Refresh(void)
{
	AfxAssert(!IsValid());

	ClearDevList();

	FindDevList();
	}

// Device Enumerations

UINT CUSBDriver::GetDevCount(void)
{
	FindDevList();

	return m_DevList.GetCount();
	}

CDeviceList const & CUSBDriver::GetDevList(void)
{
	FindDevList();

	return m_DevList;
	}

// Device Notifications

HDEVNOTIFY CUSBDriver::RegEvents(HWND hWnd)
{
	DEV_BROADCAST_DEVICEINTERFACE dbd;

	ZeroMemory(&dbd, sizeof(dbd));
	
	dbd.dbcc_size	    = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	
	dbd.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	
	dbd.dbcc_classguid  = GUID_CLASS_IG3USB;

	return RegisterDeviceNotification(hWnd, &dbd, DEVICE_NOTIFY_WINDOW_HANDLE);
	}

void CUSBDriver::UnregEvents(HDEVNOTIFY hNotify)
{
	UnregisterDeviceNotification(hNotify);
	}

// Initialisation

void CUSBDriver::Init(void)
{
	m_hHandle = INVALID_HANDLE_VALUE;

	m_fRxPend = FALSE;

	m_pDevice = NULL;
	}

// Enumeration

BOOL CUSBDriver::FindDevList(void)
{
	if( m_DevList.IsEmpty() ) {
	
		HDEVINFO hDevInfo = INVALID_HANDLE_VALUE;

		__try {
			hDevInfo = SetupDiGetClassDevs( (GUID *) &GUID_CLASS_IG3USB,
							NULL,
							NULL,
							DIGCF_PRESENT |
							DIGCF_DEVICEINTERFACE
							);

			if( hDevInfo == INVALID_HANDLE_VALUE ) {

				__leave;
				}

			////////		
			
			for( DWORD dwIndex = 0;; dwIndex ++ ) {

				SP_DEVICE_INTERFACE_DATA DevData;

				DevData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
				
				if( !SetupDiEnumDeviceInterfaces( hDevInfo, 
								  NULL,
								  LPGUID(&GUID_CLASS_IG3USB),
								  dwIndex,
								  &DevData
								  ) ) {

					__leave;
					}

				////////
				
				ULONG uSize;
				
				SetupDiGetDeviceInterfaceDetail( hDevInfo, 
								 &DevData, 
								 NULL,
								 0,
								 &uSize,
								 NULL
								 );

				if( GetLastError() != ERROR_INSUFFICIENT_BUFFER ) {

					__leave;
					}

				PSP_INTERFACE_DEVICE_DETAIL_DATA pIData = NULL;

				__try {

					pIData = (PSP_INTERFACE_DEVICE_DETAIL_DATA) new BYTE [ uSize ];

					if( !pIData ) {

						__leave;
						}

					pIData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

					if( !SetupDiGetDeviceInterfaceDetail( hDevInfo,
									      &DevData,
									      pIData, 
									      uSize,
									      &uSize,
									      NULL
									      ) ) {

						__leave;
						}
					
					AppendDevList(pIData->DevicePath);
					}

				__finally {

					if( pIData ) {
						
						delete[] pIData;
						}
					}
				}		
			}

		__finally {

			if( hDevInfo != INVALID_HANDLE_VALUE  ) {

				SetupDiDestroyDeviceInfoList(hDevInfo);
				}
			}

		}
		
	return !m_DevList.IsEmpty();
	}

void CUSBDriver::AppendDevList(PCTXT pDevPath)
{
	CDevice *pDev = new CDevice;

	pDev->m_Path  = pDevPath;
	
	CStringArray Toks;

	CString      Ident;

	if( pDev->m_Path.Tokenize(Toks, L'#') == 4 ) {

		Ident          = Toks[1];

		pDev->m_Serial = Toks[2];
		}
	else {
		CString Path = pDevPath;
		
		Path.Replace(L'\\', L'#');

		CRegKey Key(HKEY_LOCAL_MACHINE);

		Key.MoveTo(L"System\\CurrentControlSet\\Control\\DeviceClasses");

		Key.MoveTo(L"{a028de24-1a67-4ad5-b8cf-19544a08215e}");

		Key.MoveTo(Path);

		CString Intance = Key.GetValueAsString(L"DeviceInstance");

		Toks.Empty();

		if( Intance.Tokenize(Toks, L'\\') == 3 ) {

			Ident          = Toks[1];

			pDev->m_Serial = Toks[2];
			}
		}

	Toks.Empty();

	if( Ident.Tokenize(Toks, L'&') >= 2 ) {;

		pDev->m_uVend = wcstoul(Toks[0].Mid(4, 4), NULL, 16	);

		pDev->m_uProd = wcstoul(Toks[1].Mid(4, 4), NULL, 16	);

		m_DevList.Append(pDev);

		return;
		}

	delete pDev;
	}

void CUSBDriver::ClearDevList(void)
{
	m_pDevice = NULL;
	
	for( UINT i = 0; i < m_DevList.GetCount(); i ++ ) {

		CDevice *p = m_DevList[i];

		delete p;
		}
	
	m_DevList.Empty();
	}

// Synch Objects

void CUSBDriver::OpenOverlapped(void)
{
	memset(&m_TxDataOS, 0, sizeof(m_TxDataOS));

	memset(&m_RxDataOS, 0, sizeof(m_RxDataOS));
	
	m_TxDataOS.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_RxDataOS.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	}

void CUSBDriver::CloseOverlapped(void)
{
	CloseHandle(m_TxDataOS.hEvent);

	CloseHandle(m_RxDataOS.hEvent);
	}

// End of File
