
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CredentialsDialog_HPP

#define INCLUDE_CredentialsDialog_HPP

//////////////////////////////////////////////////////////////////////////
//
// Credentials Dialog
//

class CCredentialsDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CCredentialsDialog(CString const &Device, CString const &User, CString const &Pass);

	// Attributes
	CString GetUser(void) const;
	CString GetPass(void) const;
	BOOL    GetKeep(void) const;

protected:
	// Data Members
	CString m_Device;
	CString m_User;
	CString m_Pass;
	BOOL    m_fKeep;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

	// Command Handlers
	BOOL OnCommandOK(UINT uID);
	BOOL OnCommandCancel(UINT uID);

	// Notification Handlers
	void OnEditChange(UINT uID, CWnd &Ctrl);

	// Implementation
	void DoEnables(void);
};

// End of File

#endif
