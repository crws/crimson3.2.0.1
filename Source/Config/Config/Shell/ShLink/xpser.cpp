
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ASCII Codes
//

#define	STX	0x02
#define	ETX	0x03
#define	ENQ	0x05
#define	ACK	0x06
#define	DLE	0x10
#define	NAK	0x15
#define	SYN	0x16

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Serial Transport
//

class CSerialTransport : public ILinkTransport
{
	public:
		// Constructor
		CSerialTransport(UINT uPort);

		// Destructor
		~CSerialTransport(void);

		// Deletion
		void Release(void);

		// Management
		BOOL Open(void);
		void Recycle(UINT uType);
		void Close(void);
		void Terminate(void);

		// Attributes
		UINT    GetType(void) const;
		UINT    GetBulkLimit(BOOL fDown) const;
		CString GetErrorText(void) const;

		// Transport
		BOOL Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fExpectReply);

		// Notifications
		void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
		void OnBind(HWND hWnd);

	protected:
		// Data Members
		UINT	   m_uPort;
		BOOL	   m_fPipe;
		BYTE	   m_bSeq;
		HANDLE	   m_hPort;
		CEvent	   m_TxEvent;
		CEvent	   m_RxEvent;
		OVERLAPPED m_TxOS;
		OVERLAPPED m_RxOS;
		BYTE	   m_bTxBuff[2048];
		BYTE	   m_bRxBuff[1280];
		UINT	   m_uPtr;
		UINT	   m_uCount;
		WORD	   m_wCheck;
		CString	   m_Error;
		BOOL	   m_fRun;

		// Port Access
		UINT SerialRead(void);
		BOOL SerialWrite(PBYTE pData, UINT uCount);
		BOOL SerialWrite(BYTE bData);

		// Packet Building
		void AddCtrl(BYTE bData);
		void AddData(PCBYTE pData, UINT uCount);
		void AddByte(BYTE bData);

		// Transport
		BOOL PutFrame(BOOL fSlow);
		BOOL GetAck(BOOL fSlow);
		BOOL GetFrame(BOOL fSlow);
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Serial Transport
//

// Instantiator

ILinkTransport * Create_TransportSerial(UINT uPort)
{
	return new CSerialTransport(uPort);
	}

// Constructor

CSerialTransport::CSerialTransport(UINT uPort) : m_TxEvent(TRUE), m_RxEvent(TRUE)
{
	m_uPort = uPort;

	m_bSeq  = BYTE(rand());

	m_hPort = INVALID_HANDLE_VALUE;

	memset(&m_TxOS, 0, sizeof(m_TxOS));

	memset(&m_RxOS, 0, sizeof(m_RxOS));

	m_TxOS.hEvent = m_TxEvent;

	m_RxOS.hEvent = m_RxEvent;
	}

// Destructor

CSerialTransport::~CSerialTransport(void)
{
	}

// Deletion

void CSerialTransport::Release(void)
{
	delete this;
	}

// Management

BOOL CSerialTransport::Open(void)
{
	if( m_hPort == INVALID_HANDLE_VALUE ) {

		TCHAR sPort[32];

		if( m_uPort ) {

			wsprintf(sPort, L"\\\\.\\COM%u", m_uPort);

			m_fPipe = FALSE;
			}
		else {
			wsprintf(sPort, L"\\\\.\\pipe\\c32-program");

			m_fPipe = TRUE;
			}

		m_hPort = CreateFile( sPort,
				      GENERIC_READ | GENERIC_WRITE,
				      0,
				      NULL,
				      OPEN_EXISTING,
				      FILE_FLAG_OVERLAPPED,
				      NULL
				      );

		if( m_hPort == INVALID_HANDLE_VALUE ) {

			if( m_fPipe ) {

				wsprintf(sPort, L"\\\\.\\pipe\\c32-program-%8.8X", GetCurrentProcessId());

				m_hPort = CreateFile( sPort,
						      GENERIC_READ | GENERIC_WRITE,
						      0,
						      NULL,
						      OPEN_EXISTING,
						      FILE_FLAG_OVERLAPPED,
						      NULL
						      );
				}
			}

		if( m_hPort != INVALID_HANDLE_VALUE ) {

			if( !m_fPipe ) {
							
				DCB Ctrl;

				memset(&Ctrl, 0, sizeof(Ctrl));

				Ctrl.DCBlength = sizeof(DCB);
				
				GetCommState(m_hPort, &Ctrl);

				Ctrl.BaudRate = 115200;
				Ctrl.Parity   = NOPARITY;
				Ctrl.ByteSize = 8;
				Ctrl.StopBits = ONESTOPBIT;
				
				Ctrl.fOutX           = FALSE;
				Ctrl.fInX            = FALSE;
				Ctrl.fOutxCtsFlow    = FALSE;
				Ctrl.fOutxDsrFlow    = FALSE;
				Ctrl.fDsrSensitivity = FALSE;
				Ctrl.fNull	     = FALSE;
				    
				SetCommState(m_hPort, &Ctrl);
				
				SetupComm(m_hPort, sizeof(m_bRxBuff), sizeof(m_bTxBuff));
				
				COMMTIMEOUTS TimeOut;
				
				TimeOut.ReadIntervalTimeout         = MAXDWORD;
				TimeOut.ReadTotalTimeoutMultiplier  = 0;
				TimeOut.ReadTotalTimeoutConstant    = 0;
				TimeOut.WriteTotalTimeoutMultiplier = 0;
				TimeOut.WriteTotalTimeoutConstant   = 0;
				
				SetCommTimeouts(m_hPort, &TimeOut);
				}

			m_fRun = TRUE;

			return TRUE;
			}

		m_Error = CString(IDS_CANT_OPEN_PORT);

		m_fRun = TRUE;

		return FALSE;
		}

	m_fRun = TRUE;

	return TRUE;
	}

void CSerialTransport::Recycle(UINT uType)
{
	Close();

	Sleep(3000);

	Open();
	}

void CSerialTransport::Close(void)
{
	if( m_hPort != INVALID_HANDLE_VALUE ) {

		CloseHandle(m_hPort);

		m_hPort = INVALID_HANDLE_VALUE;
		}
	}

void CSerialTransport::Terminate(void)
{
	m_fRun = FALSE;
	}

// Attributes

UINT CSerialTransport::GetType(void) const
{
	return typeSerial;
	}

UINT CSerialTransport::GetBulkLimit(BOOL fDown) const
{
	return fDown ? 1024 : 64;
	}

CString CSerialTransport::GetErrorText(void) const
{
	return m_Error;
	}

// Transport

BOOL CSerialTransport::Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fExpectReply)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	m_uPtr   = 0;

	m_wCheck = 0x5678;

	AddCtrl(STX);

	AddByte(0xFF);
	
	AddByte(0x00);
	
	AddByte(Req.GetService());
	
	AddByte(m_bSeq++);
	
	AddByte(Req.GetOpcode());
	
	AddByte(Req.GetFlags());

	AddData(Req.GetData(), Req.GetDataSize());

	AddData(Req.GetBulk(), Req.GetBulkSize());

	if( PutFrame(Req.IsVerySlow()) ) {

		if( GetFrame(Req.IsVerySlow()) ) {

			Rep.StartFrame(m_bRxBuff[2], m_bRxBuff[4]);

			Rep.AddData(m_bRxBuff + 6, m_uCount - 6);

			return TRUE;
			}
		}

	m_Error = IDS_XPUSB_REPLY_NOT;
		
	return FALSE;
	}

// Notifications

void CSerialTransport::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	}

void CSerialTransport::OnBind(HWND hWnd)
{
	}

// Port Access

UINT CSerialTransport::SerialRead(void)
{
	DWORD dwCount;

	BYTE  bData;

	if( !ReadFile(m_hPort, &bData, 1, &dwCount, &m_RxOS) ) {

		if( GetLastError() == ERROR_IO_PENDING ) {

			if( m_RxEvent.WaitForObject(100) ) {

				GetOverlappedResult(m_hPort, &m_RxOS, &dwCount, FALSE);
		
				return dwCount ? bData : NOTHING;
				}

			CancelIo(m_hPort);

			return NOTHING;
			}

		return NOTHING;
		}

	return dwCount ? bData : NOTHING;
	}

BOOL CSerialTransport::SerialWrite(PBYTE pData, UINT uCount)
{
	DWORD dwCount;

	if( !WriteFile(m_hPort, pData, uCount, &dwCount, &m_TxOS) ) {

		if( GetLastError() == ERROR_IO_PENDING ) {

			GetOverlappedResult(m_hPort, &m_TxOS, &dwCount, TRUE);

			return dwCount ? TRUE : FALSE;
			}

		return FALSE;
		}

	return dwCount ? TRUE : FALSE;
	}

BOOL CSerialTransport::SerialWrite(BYTE bData)
{
	DWORD dwCount;

	if( !WriteFile(m_hPort, &bData, 1, &dwCount, &m_TxOS) ) {

		if( GetLastError() == ERROR_IO_PENDING ) {

			GetOverlappedResult(m_hPort, &m_TxOS, &dwCount, TRUE);

			return dwCount ? TRUE : FALSE;
			}

		return FALSE;
		}

	return dwCount ? TRUE : FALSE;
	}

// Packet Building

void CSerialTransport::AddCtrl(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = DLE;

	m_bTxBuff[m_uPtr++] = bData;
	}

void CSerialTransport::AddData(PCBYTE pData, UINT uCount)
{
	while( uCount-- ) AddByte(*(pData++));
	}

void CSerialTransport::AddByte(BYTE bData)
{
	if( bData == DLE ) {

		m_bTxBuff[m_uPtr++] = DLE;

		m_bTxBuff[m_uPtr++] = DLE;
		}
	else
		m_bTxBuff[m_uPtr++] = bData;

	m_wCheck = WORD(m_wCheck + bData);
	}

// Transport

BOOL CSerialTransport::PutFrame(BOOL fSlow)
{
	WORD wCheck = m_wCheck;

	AddCtrl(ETX);

	AddByte(LOBYTE(wCheck));
	
	AddByte(HIBYTE(wCheck));

	UINT uRetry = fSlow ? 5 : 10;

	for( UINT n = 0; n < uRetry; n++ ) {

		if( !m_fPipe ) {

			PurgeComm(m_hPort, PURGE_RXCLEAR);

			PurgeComm(m_hPort, PURGE_TXCLEAR);

			for(;;) {
			
				COMSTAT ComStat;

				DWORD dwFlags = 0;

				ClearCommError(m_hPort, &dwFlags, &ComStat);
				
				if( dwFlags == 0 ) {

					break;
					}
				}
			}

		SerialWrite(m_bTxBuff, m_uPtr);

		if( GetAck(fSlow) ) {

			return TRUE;
			}

		if( m_fPipe ) {

			Close();

			Open();

			continue;
			}
		}

	return FALSE;
	}

BOOL CSerialTransport::GetAck(BOOL fSlow)
{
	DWORD t0 = GetTickCount();

	DWORD tr = fSlow ? 5000 : 500;

	while( m_fRun ) {

		UINT w = SerialRead();

		if( w == NOTHING ) {

			if( GetTickCount() - t0 > tr ) {

				m_Error = CString(IDS_XPSER_NAK);

				return FALSE;
				}

			Sleep(10);

			continue;
			}

		if( w == ACK ) {

			return TRUE;
			}

		if( w == NAK ) {

			m_Error = CString(IDS_XPSER_NEG_ACK);

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CSerialTransport::GetFrame(BOOL fSlow)
{
	DWORD t0 = GetTickCount();

	DWORD tr = fSlow ? 120000 : 20000;

	UINT uState = 0;

	UINT uCount = 0;
	
	UINT uDelay = 0;

	WORD wCheck = 0;
	
	BOOL fDLE   = FALSE;
	
	while( m_fRun ) {
	
		UINT w = SerialRead();
		
		if( w == NOTHING ) {

			if( GetTickCount() - t0 > tr ) {

				m_Error = CString(IDS_XPSER_TO);

				return FALSE;
				}	
		
			if( ++uDelay >= 10 ) {

				uState = 0;
				}

			Sleep(10);
			
			continue;
			}
		else
			uDelay = 0;

		if( w == DLE ) {
		
			if( !fDLE ) {
			
				fDLE = TRUE;
				
				continue;
				}
			else
				fDLE = FALSE;
			}
			
		switch( uState ) {
		
			case 0:
				if( fDLE ) {
				
					if( w == STX ) {
	
						wCheck = 0x5678;
						
						uCount = 0;
										
						uState = 1;
						}
					}
				break;
				
			case 1:
				if( fDLE ) {
				
					if( w == ETX ) {

						uState = 2;
						}
					else {
						m_Error = CString(IDS_XPSER_INVALID);

						return FALSE;
						}
					}
				else {
					m_bRxBuff[uCount] = BYTE(w);
				
					if( ++uCount == sizeof(m_bRxBuff) ) {

						m_Error = CString(IDS_XPSER_LARGE);

						return FALSE;
						}
						
					wCheck = WORD(wCheck + w);
					}
				
				break;
				
			case 2:
				if( fDLE ) {
					
					m_Error = CString(IDS_XPSER_INVALID);

					return FALSE;
					}
				else {
					wCheck = WORD(wCheck - 0x001 * w);
					
					uState = 3;
					}
				
				break;
				
			case 3:
				if( fDLE ) {
					
					m_Error = CString(IDS_XPSER_INVALID);

					return FALSE;
					}
				else {
					wCheck = WORD(wCheck - 0x100 * w);
					
					if( wCheck ) {
					
						SerialWrite(NAK);

						m_Error = CString(IDS_XPSER_BAD_CHECK);

						return FALSE;
						}
					else {
						SerialWrite(ACK);

						m_uCount = uCount;
						
						return TRUE;
						}
					}
				break;
			}
		
		fDLE = FALSE;
		}

	return FALSE;
	}

// End of File
