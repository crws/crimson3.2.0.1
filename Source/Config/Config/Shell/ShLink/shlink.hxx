
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Download Link
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SHLINK_HXX
	
#define	INCLUDE_SHLINK_HXX

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3look.hxx>

// End of File

#endif
