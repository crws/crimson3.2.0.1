
#include "intern.hpp"

#include "g3master.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Access
//

// Constructor

CDataAccess::CDataAccess(void)
{
	}

// Attributes

BOOL CDataAccess::GetReadData(PDWORD pData, PBOOL pAvail, UINT uCount) const
{
	if( m_Rep.GetOpcode() == dataListRead ) {

		if( m_Rep.GetDataSize() ) {

			UINT uPtr = 0;

			if( m_Rep.ReadWord(uPtr) == uCount ) {

				for( UINT n = 0; n < uCount; n++ ) {

					if( m_Rep.ReadByte(uPtr) ) {

						pAvail[n] = TRUE;

						pData [n] = m_Rep.ReadLong(uPtr);
						}
					else
						pAvail[n] = FALSE;
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Basic Operations

BOOL CDataAccess::ListRead(PCDWORD pRefs, UINT uCount)
{
	m_Req.StartFrame(servData, dataListRead);

	m_Req.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD Ref = HostToMotor(pRefs[n]);

		m_Req.AddBulk(PCBYTE(&Ref), sizeof(Ref));
		}

	return SyncTransact(dataListRead, FALSE, FALSE);
	}

// End of File
