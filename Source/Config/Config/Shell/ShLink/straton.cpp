
#include "intern.hpp"

#include "g3master.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Online Debug
//

// Constructor

CStratonOnline::CStratonOnline(void)
{
	}

// Attributes

UINT CStratonOnline::Recv(PBYTE pData, UINT uData) const
{
	if( m_Rep.GetOpcode() == stratonService ) {

		UINT uSize = m_Rep.GetDataSize();

		UINT uPtr  = 0;

		memcpy(pData, m_Rep.ReadData(uPtr, uSize), uSize);

		return uSize;
		}

	return 0;
	}

//  Operations

BOOL CStratonOnline::Send(PCBYTE pData, UINT uData)
{
	m_Req.StartFrame(servStraton, stratonService);

	m_Req.AddBulk(pData, uData);

	return SyncTransact(stratonService, FALSE, FALSE);
	}

// End of File
