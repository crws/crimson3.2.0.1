
#include "intern.hpp"

#include "LinkAutoDetect.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Auto Detect Dialog Box
//

// Runtime Class

AfxImplementRuntimeClass(CLinkAutoDetectDialog, CStdDialog);

// State Machine

enum
{
	stateBootAutoDetect,
	stateDone,
};

// Constructors

CLinkAutoDetectDialog::CLinkAutoDetectDialog(void)
{
	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fAutoClose = FALSE;

	m_pComms     = New CCommsThread;

	SetName(L"LinkAutoDetectDlg");
}

// Destructor

CLinkAutoDetectDialog::~CLinkAutoDetectDialog(void)
{
	delete m_pComms;
}

// Attributes

CString CLinkAutoDetectDialog::GetModel(void) const
{
	return m_Model;
}

// Message Map

AfxMessageMap(CLinkAutoDetectDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_DEVICECHANGE)

	AfxDispatchCommand(IDOK, OnCommandOkay)
	AfxDispatchCommand(IDINIT, OnCommsInit)
	AfxDispatchCommand(IDDONE, OnCommsDone)
	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDCRED, OnCommsCred)

	AfxMessageEnd(CLinkAutoDetectDialog)
};

// Message Handlers

BOOL CLinkAutoDetectDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_uState = stateBootAutoDetect;

	m_pComms->Bind(this);

	m_pComms->Create();

	return TRUE;
}

void CLinkAutoDetectDialog::OnDeviceChange(LONG uEvent, LONG uData)
{
	m_pComms->OnEvent(WM_DEVICECHANGE, uEvent, uData);
}

// Command Handlers

BOOL CLinkAutoDetectDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		#pragma warning(suppress: 6286)

		if( TRUE || NoYes(CString(IDS_LINK_ABORT)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_pComms->Terminate(2000);

			EndDialog(!m_fError);
		}
	}

	else
		EndDialog(FALSE);

	return TRUE;
}

BOOL CLinkAutoDetectDialog::OnCommsInit(UINT uID)
{
	TxFrame();

	return TRUE;
}

BOOL CLinkAutoDetectDialog::OnCommsDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
	}

	return TRUE;
}

BOOL CLinkAutoDetectDialog::OnCommsFail(UINT uID)
{
	SetError(m_pComms->GetErrorText());

	return TRUE;
}

BOOL CLinkAutoDetectDialog::OnCommsCred(UINT uID)
{
	m_pComms->AskForCredentials();

	return TRUE;
}

// Implementation

void CLinkAutoDetectDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_pComms->Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			EndDialog(TRUE);

			return;
		}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_LINK_CLOSE));
	}
}

void CLinkAutoDetectDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_LINK_REPORT_ERROR, pText));

	GetDlgItem(101).SetWindowText(L"");

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
}

void CLinkAutoDetectDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
}

void CLinkAutoDetectDialog::TxFrame(void)
{
	if( m_uState == stateBootAutoDetect ) {

		ShowStatus(IDS("Requesting Model Information"));

		m_pComms->BootAutoDetect();

		return;
	}

	ShowStatus(CString(IDS_LINK_OP_OK));

	SetDone(FALSE);
}

BOOL CLinkAutoDetectDialog::RxFrame(void)
{
	if( m_uState == stateBootAutoDetect ) {

		if( m_pComms->BootGetAutoDetect(m_Model) ) {

			m_uState = stateDone;

			return TRUE;
		}
	}

	SetError(CString(IDS_LINK_INVALID));

	return FALSE;
}

// End of File
