
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// MD5 Hash Support
//

// Functions

#define F1(x, y, z) (z ^ (x & (y ^ z)))

#define F2(x, y, z) F1(z, x, y)

#define F3(x, y, z) (x ^ y ^ z)

#define F4(x, y, z) (y ^ (x | ~z))

// Mixing Round

#define MD(f, w, x, y, z, d, s)	(w += f(x, y, z) + d, w = w << s | w >> (32 - s), w += x)

// Hash Context

struct CCtx
{
	DWORD Data[4];
	DWORD Bits[2];
	BYTE  In[64];
};

// Prototypes

global	void	 md5(PCBYTE pData, UINT uSize, PBYTE pHash);
static	void	 Init(CCtx *pCtx);
static	void	 Update(CCtx *pCtx, PCBYTE pData, UINT uSize);
static	void	 Final(PBYTE pHash, CCtx *pCtx);
static	void	 Transform(PDWORD pData, PDWORD pIn);
static	void	 Reverse(PBYTE pData, UINT uLongs);

// Code

global void md5(PCBYTE pData, UINT uSize, PBYTE pHash)
{
	CCtx ctx;

	Init(&ctx);

	Update(&ctx, pData, uSize);

	Final(pHash, &ctx);
}

static void Init(CCtx *pCtx)
{
	pCtx->Data[0] = 0x67452301;
	pCtx->Data[1] = 0xefcdab89;
	pCtx->Data[2] = 0x98badcfe;
	pCtx->Data[3] = 0x10325476;

	pCtx->Bits[0] = 0;
	pCtx->Bits[1] = 0;
}

static void Update(CCtx *pCtx, PCBYTE pData, UINT uSize)
{
	DWORD t;

	t = pCtx->Bits[0];

	if( (pCtx->Bits[0] = t + (DWORD(uSize) << 3)) < t ) {

		pCtx->Bits[1]++;
	}

	pCtx->Bits[1] += uSize >> 29;

	t = (t >> 3) & 0x3F;

	if( t ) {

		PBYTE p = PBYTE(pCtx->In + t);

		t = 64 - t;

		if( uSize < t ) {

			memcpy(p, pData, uSize);

			return;
		}

		memcpy(p, pData, t);

		Reverse(pCtx->In, 16);

		Transform(pCtx->Data, PDWORD(pCtx->In));

		pData += t;

		uSize -= t;
	}

	while( uSize >= 64 ) {

		memcpy(pCtx->In, pData, 64);

		Reverse(pCtx->In, 16);

		Transform(pCtx->Data, PDWORD(pCtx->In));

		pData += 64;

		uSize -= 64;
	}

	memcpy(pCtx->In, pData, uSize);
}

static void Final(PBYTE pHash, CCtx *pCtx)
{
	UINT  n = (pCtx->Bits[0] >> 3) & 0x3F;

	PBYTE p = pCtx->In + n;

	*p++ = 0x80;

	n    = 64 - 1 - n;

	if( n < 8 ) {

		memset(p, 0, n);

		Reverse(pCtx->In, 16);

		Transform(pCtx->Data, PDWORD(pCtx->In));

		memset(pCtx->In, 0, 56);
	}
	else {
		memset(p, 0, n - 8);
	}

	Reverse(pCtx->In, 14);

	PDWORD(pCtx->In)[14] = pCtx->Bits[0];

	PDWORD(pCtx->In)[15] = pCtx->Bits[1];

	Transform(pCtx->Data, PDWORD(pCtx->In));

	Reverse(PBYTE(pCtx->Data), 4);

	memcpy(pHash, pCtx->Data, 16);

	memset(pCtx, 0, sizeof(*pCtx));
}

static void Transform(PDWORD pData, PDWORD pIn)
{
	register DWORD a, b, c, d;

	a = pData[0];
	b = pData[1];
	c = pData[2];
	d = pData[3];

	MD(F1, a, b, c, d, pIn[0] + 0xD76AA478, 7);
	MD(F1, d, a, b, c, pIn[1] + 0xE8C7B756, 12);
	MD(F1, c, d, a, b, pIn[2] + 0x242070DB, 17);
	MD(F1, b, c, d, a, pIn[3] + 0xC1BDCEEE, 22);
	MD(F1, a, b, c, d, pIn[4] + 0xF57C0FAF, 7);
	MD(F1, d, a, b, c, pIn[5] + 0x4787C62A, 12);
	MD(F1, c, d, a, b, pIn[6] + 0xA8304613, 17);
	MD(F1, b, c, d, a, pIn[7] + 0xFD469501, 22);
	MD(F1, a, b, c, d, pIn[8] + 0x698098D8, 7);
	MD(F1, d, a, b, c, pIn[9] + 0x8B44F7AF, 12);
	MD(F1, c, d, a, b, pIn[10] + 0xFFFF5BB1, 17);
	MD(F1, b, c, d, a, pIn[11] + 0x895CD7BE, 22);
	MD(F1, a, b, c, d, pIn[12] + 0x6B901122, 7);
	MD(F1, d, a, b, c, pIn[13] + 0xFD987193, 12);
	MD(F1, c, d, a, b, pIn[14] + 0xA679438E, 17);
	MD(F1, b, c, d, a, pIn[15] + 0x49B40821, 22);

	MD(F2, a, b, c, d, pIn[1] + 0xF61E2562, 5);
	MD(F2, d, a, b, c, pIn[6] + 0xC040B340, 9);
	MD(F2, c, d, a, b, pIn[11] + 0x265E5A51, 14);
	MD(F2, b, c, d, a, pIn[0] + 0xE9B6C7AA, 20);
	MD(F2, a, b, c, d, pIn[5] + 0xD62F105D, 5);
	MD(F2, d, a, b, c, pIn[10] + 0x02441453, 9);
	MD(F2, c, d, a, b, pIn[15] + 0xD8A1E681, 14);
	MD(F2, b, c, d, a, pIn[4] + 0xE7D3FBC8, 20);
	MD(F2, a, b, c, d, pIn[9] + 0x21E1CDE6, 5);
	MD(F2, d, a, b, c, pIn[14] + 0xC33707D6, 9);
	MD(F2, c, d, a, b, pIn[3] + 0xF4D50D87, 14);
	MD(F2, b, c, d, a, pIn[8] + 0x455A14ED, 20);
	MD(F2, a, b, c, d, pIn[13] + 0xA9E3E905, 5);
	MD(F2, d, a, b, c, pIn[2] + 0xFCEFA3F8, 9);
	MD(F2, c, d, a, b, pIn[7] + 0x676F02D9, 14);
	MD(F2, b, c, d, a, pIn[12] + 0x8D2A4C8A, 20);

	MD(F3, a, b, c, d, pIn[5] + 0xFFFA3942, 4);
	MD(F3, d, a, b, c, pIn[8] + 0x8771F681, 11);
	MD(F3, c, d, a, b, pIn[11] + 0x6D9D6122, 16);
	MD(F3, b, c, d, a, pIn[14] + 0xFDE5380C, 23);
	MD(F3, a, b, c, d, pIn[1] + 0xA4BEEA44, 4);
	MD(F3, d, a, b, c, pIn[4] + 0x4BDECFA9, 11);
	MD(F3, c, d, a, b, pIn[7] + 0xF6BB4B60, 16);
	MD(F3, b, c, d, a, pIn[10] + 0xBEBFBC70, 23);
	MD(F3, a, b, c, d, pIn[13] + 0x289B7EC6, 4);
	MD(F3, d, a, b, c, pIn[0] + 0xEAA127FA, 11);
	MD(F3, c, d, a, b, pIn[3] + 0xD4EF3085, 16);
	MD(F3, b, c, d, a, pIn[6] + 0x04881D05, 23);
	MD(F3, a, b, c, d, pIn[9] + 0xD9D4D039, 4);
	MD(F3, d, a, b, c, pIn[12] + 0xE6DB99E5, 11);
	MD(F3, c, d, a, b, pIn[15] + 0x1FA27CF8, 16);
	MD(F3, b, c, d, a, pIn[2] + 0xC4AC5665, 23);

	MD(F4, a, b, c, d, pIn[0] + 0xF4292244, 6);
	MD(F4, d, a, b, c, pIn[7] + 0x432AFF97, 10);
	MD(F4, c, d, a, b, pIn[14] + 0xAB9423A7, 15);
	MD(F4, b, c, d, a, pIn[5] + 0xFC93A039, 21);
	MD(F4, a, b, c, d, pIn[12] + 0x655B59C3, 6);
	MD(F4, d, a, b, c, pIn[3] + 0x8F0CCC92, 10);
	MD(F4, c, d, a, b, pIn[10] + 0xFFEFF47D, 15);
	MD(F4, b, c, d, a, pIn[1] + 0x85845DD1, 21);
	MD(F4, a, b, c, d, pIn[8] + 0x6FA87E4F, 6);
	MD(F4, d, a, b, c, pIn[15] + 0xFE2CE6E0, 10);
	MD(F4, c, d, a, b, pIn[6] + 0xA3014314, 15);
	MD(F4, b, c, d, a, pIn[13] + 0x4E0811A1, 21);
	MD(F4, a, b, c, d, pIn[4] + 0xF7537E82, 6);
	MD(F4, d, a, b, c, pIn[11] + 0xBD3AF235, 10);
	MD(F4, c, d, a, b, pIn[2] + 0x2AD7D2BB, 15);
	MD(F4, b, c, d, a, pIn[9] + 0xEB86D391, 21);

	pData[0] += a;
	pData[1] += b;
	pData[2] += c;
	pData[3] += d;
}

static void Reverse(PBYTE pData, UINT uLongs)
{
}

// End of File
