@echo off

if not exist L:\cer.pfx goto skip

set TimeServer=/t http://timestamp.digicert.com

signtool.exe sign /q /f L:\cer.pfx /p opensesame %TimeServer% %2\..\..\..\..\..\Build\Bin\Config\Win32\%1\oem_redlion.dll

signtool.exe sign /q /f L:\cer.pfx /p opensesame %TimeServer% %2\..\..\..\..\..\Build\Bin\Config\Win32\%1\c3.exe

:skip

exit 0
