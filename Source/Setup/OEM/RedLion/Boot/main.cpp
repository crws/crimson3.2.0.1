
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Setup Boot Strapper
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

// Static Data

static	HWND		m_hWnd;

static	HINSTANCE	m_hThis;

// Prototypes

int	WINAPI	WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow);
static	BOOL	SaveToFile(HRSRC hRes, PCTSTR pFile, BOOL fBar);
static	BOOL	Delete(PCTSTR pFile);
static	void	GetTemp(PTSTR pName, PCTSTR pPath, PCTSTR pType);
static	void	MakeWindow(void);
static	void	KillWindow(void);
static	void	PumpWindow(void);
LRESULT	WINAPI	WndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
static	PCTSTR	GetString(UINT id);
static	void	ParseCommandLine(void);
static	void	SetLanguage(PCTSTR pLang);
static	BOOL	IsVista(void);

// Code

int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	m_hThis = hThis;

	ParseCommandLine();

	INITCOMMONCONTROLSEX Init;

	Init.dwSize = sizeof(Init);

	Init.dwICC  = ICC_PROGRESS_CLASS;

	InitCommonControlsEx(&Init);

	TCHAR sTemp[MAX_PATH] = { 0 };

	TCHAR sVcLib[MAX_PATH] = { 0 };

	TCHAR sSetup[MAX_PATH] = { 0 };

	TCHAR sPatch[MAX_PATH] = { 0 };

	TCHAR sParam[MAX_PATH] = { 0 };

	GetTempPath(MAX_PATH, sTemp);

	MakeWindow();

	HRSRC hVcLib = FindResource(hThis, L"vcr", L"exe");

	HRSRC hSetup = FindResource(hThis, L"msi", L"msi");

	if( hVcLib && hSetup ) {

		GetTemp(sVcLib, sTemp, L"exe");

		GetTemp(sSetup, sTemp, L"msi");

		if( SaveToFile(hVcLib, sVcLib, TRUE) && SaveToFile(hSetup, sSetup, TRUE) ) {

			lstrcat(sParam, L"/i \"");

			lstrcat(sParam, sSetup);

			lstrcat(sParam, L"\"");

			BOOL   fOkay = FALSE;

			PCTSTR pLang = NULL;

			switch( PRIMARYLANGID(GetThreadLocale()) ) {

				case LANG_FRENCH:

					pLang = L"fr";

					break;

				case LANG_GERMAN:

					pLang = L"de";

					break;

				case LANG_SPANISH:

					pLang = L"es";

					break;

				case LANG_CHINESE:

					pLang = L"zh";

					break;

				default:

					fOkay = TRUE;

					break;
			}

			if( pLang ) {

				HRSRC hPatch = FindResource(hThis, pLang, L"mst");

				if( hPatch ) {

					GetTemp(sPatch, sTemp, L"mst");

					if( SaveToFile(hPatch, sPatch, FALSE) ) {

						lstrcat(sParam, L" transforms=\"");

						lstrcat(sParam, sPatch);

						lstrcat(sParam, L"\"");

						fOkay = TRUE;
					}
				}
			}

			if( fOkay ) {

				SHELLEXECUTEINFO Info;

				memset(&Info, 0, sizeof(Info));

				Info.cbSize       = sizeof(Info);

				Info.fMask	  = SEE_MASK_NOCLOSEPROCESS;

				Info.lpFile       = sVcLib;

				Info.lpParameters = L"/quiet";

				Info.lpDirectory  = sTemp;

				Info.nShow        = SW_HIDE;

				if( ShellExecuteEx(&Info) ) {

					WaitForSingleObject(Info.hProcess, INFINITE);

					KillWindow();

					Info.lpFile       = L"msiexec.exe";

					Info.lpParameters = sParam;

					Info.lpDirectory  = sTemp;

					Info.nShow        = SW_SHOW;

					if( ShellExecuteEx(&Info) ) {

						WaitForSingleObject(Info.hProcess, INFINITE);

						Delete(sVcLib);

						Delete(sSetup);

						Delete(sPatch);

						return 0;
					}
				}
			}

			Delete(sVcLib);

			Delete(sSetup);

			Delete(sPatch);
		}
	}

	KillWindow();

	return 1;
}

static BOOL SaveToFile(HRSRC hRes, PCTSTR pFile, BOOL fBar)
{
	HANDLE hFile = CreateFile(pFile,
				  GENERIC_WRITE,
				  FILE_SHARE_WRITE,
				  NULL,
				  CREATE_ALWAYS,
				  FILE_ATTRIBUTE_NORMAL,
				  NULL
	);

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT    uSize = SizeofResource(NULL, hRes);

		HGLOBAL hData = LoadResource(NULL, hRes);

		PVOID   pData = LockResource(hData);

		UINT    uFrom = 0;

		while( uFrom < uSize ) {

			UINT  uLump = min(uSize - uFrom, 256 * 1024);

			DWORD uDone = 0;

			WriteFile(hFile, PBYTE(pData) + uFrom, uLump, &uDone, NULL);

			if( uDone == uLump ) {

				uFrom += uDone;

				if( fBar ) {

					HWND hBar = GetDlgItem(m_hWnd, 100);

					UINT uPos = 110 * (uFrom / 16) / (uSize / 16);

					SendMessage(hBar, PBM_SETPOS, uPos, 0);
				}

				PumpWindow();

				continue;
			}

			FreeResource(hData);

			CloseHandle(hFile);

			return FALSE;
		}

		FreeResource(hData);

		CloseHandle(hFile);

		return TRUE;
	}

	return FALSE;
}

static BOOL Delete(PCTSTR pFile)
{
	if( pFile[0] ) {

		for( int n = 0; n < 20; n++ ) {

			if( DeleteFile(pFile) ) {

				return TRUE;
			}

			Sleep(500);
		}
	}

	return FALSE;
}

static void GetTemp(PTSTR pName, PCTSTR pPath, PCTSTR pType)
{
	GetTempFileName(pPath, L"c3-", 0, pName);

	DeleteFile(pName);

	lstrcpy(pName + lstrlen(pName) - 3, pType);
}

static	void	MakeWindow(void)
{
	WNDCLASS Class;

	Class.style		= 0;
	Class.lpfnWndProc	= WndProc;
	Class.cbClsExtra	= 0;
	Class.cbWndExtra	= 0;
	Class.hInstance		= m_hThis;
	Class.hCursor		= LoadCursor(NULL, IDC_WAIT);
	Class.hIcon		= LoadIcon(m_hThis, L"app");
	Class.hbrBackground	= HBRUSH(1 + COLOR_BTNFACE);
	Class.lpszMenuName	= NULL;
	Class.lpszClassName	= L"BootClass";

	if( RegisterClass(&Class) ) {

		int cx = 300;

		int cy = 120;

		int xp = (GetSystemMetrics(SM_CXSCREEN) - cx) / 2;

		int yp = (GetSystemMetrics(SM_CYSCREEN) - cy) / 2;

		m_hWnd = CreateWindow(L"BootClass",
				      GetString(1),
				      WS_OVERLAPPED | WS_CAPTION | WS_CLIPCHILDREN,
				      xp, yp,
				      cx, cy,
				      NULL,
				      NULL,
				      m_hThis,
				      NULL
		);

		ShowWindow(m_hWnd, SW_SHOW);

		UpdateWindow(m_hWnd);
	}
}

static	void	PumpWindow(void)
{
	MSG Msg;

	while( PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE) ) {

		TranslateMessage(&Msg);

		DispatchMessage(&Msg);
	}
}

static	void	KillWindow(void)
{
	DestroyWindow(m_hWnd);
}

LRESULT	WINAPI	WndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_CREATE ) {

		RECT Rect;

		GetClientRect(hWnd, &Rect);

		HWND hBar = CreateWindow(PROGRESS_CLASS,
					 L"",
					 WS_CHILD,
					 Rect.left   + 10,
					 Rect.bottom - 40,
					 Rect.right  - Rect.left - 25,
					 30,
					 hWnd,
					 HMENU(100),
					 m_hThis,
					 NULL
		);

		ShowWindow(hBar, SW_SHOW);

		SendMessage(hBar, PBM_SETRANGE, 0, MAKELPARAM(0, 100));
	}

	if( uMessage == WM_PAINT ) {

		PAINTSTRUCT ps;

		HDC    hDC   = BeginPaint(hWnd, &ps);

		PCTSTR pText = GetString(2);

		UINT   nSize = lstrlen(pText);

		SIZE   Size;

		RECT   Rect;

		SelectObject(hDC, GetStockObject(ANSI_VAR_FONT));

		GetTextExtentPoint(hDC, pText, nSize, &Size);

		GetClientRect(hWnd, &Rect);

		int xp = (Rect.left + Rect.right - Size.cx) / 2;

		int yp = 15;

		SetBkMode(hDC, TRANSPARENT);

		TextOut(hDC, xp, yp, pText, nSize);

		EndPaint(hWnd, &ps);

		return 0;
	}

	return DefWindowProc(hWnd, uMessage, wParam, lParam);
}

static	PCTSTR	GetString(UINT id)
{
	static TCHAR sText[256];

	LoadString(m_hThis, id, sText, sizeof(sText));

	return sText;
}

static	void	ParseCommandLine(void)
{
	INT     nArg = 0;

	LPTSTR *pArg = CommandLineToArgvW(GetCommandLine(), &nArg);

	if( nArg == 3 ) {

		if( pArg[1][0] == '-' || pArg[1][0] == '/' ) {

			if( !lstrcmpi(pArg[1] + 1, L"lang") ) {

				SetLanguage(pArg[2]);
			}
		}
	}
}

static	void	SetLanguage(PCTSTR pLang)
{
	WORD l = LANG_ENGLISH;

	WORD s = SUBLANG_ENGLISH_US;

	struct CList { PCTSTR p; WORD l; WORD s; };

	static CList const List[] = {

		{	L"en",		LANG_ENGLISH,	SUBLANG_ENGLISH_US		},
		{	L"fr",		LANG_FRENCH,	SUBLANG_FRENCH			},
		{	L"de",		LANG_GERMAN,	SUBLANG_GERMAN			},
		{	L"es",		LANG_SPANISH,	SUBLANG_SPANISH			},
		{	L"zh",		LANG_CHINESE,	SUBLANG_CHINESE_SIMPLIFIED	},
		{	NULL,		0,		0				},

	};

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; List[n].p; n++ ) {

			if( !lstrcmpi(List[n].p, pLang) ) {

				l = List[n].l;

				s = List[n].s;

				break;
			}
		}
	}

	if( IsVista() ) {

		HMODULE hModule = LoadLibrary(L"kernel32");

		FARPROC pfnProc = GetProcAddress(hModule, "SetThreadUILanguage");

		if( pfnProc ) {

			typedef void(__stdcall * PSETLANG)(LANGID);

			PSETLANG pfnSetLang = PSETLANG(pfnProc);

			(*pfnSetLang)(LANGID(MAKELANGID(l, s)));
		}

		FreeLibrary(hModule);
	}

	SetThreadLocale(MAKELCID(MAKELANGID(l, s), SORT_DEFAULT));
}

static BOOL IsVista(void)
{
	return TRUE;
}

// End of File
