@echo off

cd "%~2"

call ..\..\..\..\..\Build\Bin\Version\%1\setup.bat

if not exist L:\cer.pfx goto skip

set TimeServer=/t http://timestamp.digicert.com

signtool.exe sign /q /f L:\cer.pfx /p opensesame %TimeServer% "%~3\c32.exe"

:skip

mkdir "%~3\..\..\..\..\Bin\Artifacts"    2>nul
mkdir "%~3\..\..\..\..\Bin\Artifacts\%1" 2>nul

set tag=dev

rem 12345678901234567890
rem C3.2-Local-0.xxxx
rem C3.2-Daily-0.xxxx
rem C3.2-Main-0.xxxx
rem C3.2-Gold-Red-Lion-0.xxxx

if "%branch:~0,11%" == "C3.2-Local-" set tag=local

if "%branch:~0,11%" == "C3.2-Daily-" set tag=daily

if "%branch:~0,10%" == "C3.2-Main-"  set tag=main

if "%branch:~0,10%" == "C3.2-Gold-"  set tag=gold

copy "%~3\c32.exe" "%~3\..\..\..\..\Bin\Artifacts\%1\c32-%tag%-%build%.exe" >nul
